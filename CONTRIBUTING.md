# Contributing to Moore

Contributions to Moore are accepted in the form of merge requests. Code should
conform to the C++ and Python formatting standards define by the CI. The
checker will run automatically when you push new commits to your branch, and
will generate a patch you can apply if the checks fail.

To simplify the workflow, you can format and lint locally using

```shell
lb-format --reference origin/master
flake8 --exclude '*.opts.py' --select=F,E71,E9,W1,W6 $(find Hlt -name '*.py')
```

Check [Git4LHCb](https://twiki.cern.ch/twiki/bin/view/LHCb/Git4LHCb) for more details.

Opening a merge request (MR) is a good way to discuss your changes with others.
If your code is not yet ready to be reviewed by a Moore responsible, you can
[mark it 'work in
progress'](https://docs.gitlab.com/ee/user/project/merge_requests/work_in_progress_merge_requests.html)
by starting the title with `WIP: `. This can be useful for when you want to ask
others about specific details your unsure of. Non-WIP MRs will be reviewed
following the [RTA shifter guidelines][shifterpage].

Each MR should have a short title summarising the changes you've made, such as
"Add two-body prompt D0 HLT2 lines" or "Fix HLT2 forward tracking bug", and a
description with more detail. It is useful to see [what Moore responsibles look
for in a MR before you open your first one][shifterpage].

## Code owners

The [`CODEOWNERS` file](./CODEOWNERS) lists 'owners' of files and folders.
These are people who might be interested in changes to those objects. If
you're adding new files, please make sure to put yourself, and perhaps
others, down as owners in the owners file. You can also update the entries if
you notice things are out of date.

## Branches

The `master` branch is used for Run 3 developments.
Dedicated branches exist for the data-taking in a specific year, eg `2024-patches`. How to know which branch your MR should target:
- Fixes and additions for the data-taking in the current year should target the specific `{year}-patches` branch.
By default, it is understood that a MR targeting the current year branch should also be included in `master`,
so the `master` branch is regularly synchronised with the current year branch.
If you are putting in a temporary fix that is not meant as a long-term solution and should not be included in `master`, please state it clearly in the MR description.
- If you are working on a long-term development that is not meant for immediate inclusion in the data taking (e.g., for end-of-year Sprucing), your MR should target the `master` branch.

In case of doubt, you are encouraged to contact the coordinators of the WP you work within.


[shifterpage]: https://twiki.cern.ch/twiki/bin/viewauth/LHCb/RealTimeAnalysisShifterChecklist
