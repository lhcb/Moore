# Merge request for line _LineName_

<!-- please follow the naming convention at https://gitlab.cern.ch/lhcb/Moore/-/issues/60 -->

## Description of the line

Description of channel and of the builders used in the development.

## MC samples used in the development

1. Minbias
2. Channel-specific MC (report size of sample).

## Contact people

Use the `/assign @[name]` [gitlab quick action](https://docs.gitlab.com/ee/user/project/quick_actions.html) with the `@` handle to tag all authors.

## Report

- Efficiency tables obtained through the wizard.
- Cuts that may have changed since Run 1/2.
- Plots.
- Any other comment.
- Report on use of TOPO, whether in sprucing or Turbo.

## Check-list

- [ ] Close line-specific issue. This should be done using the [gitlab quick action](https://docs.gitlab.com/ee/user/project/quick_actions.html) `closes #[issue number]`. This will close the issue automatically when the MR is merged.

/assign me

/assign_reviewer @bldelane @sklaver @jugarcia

/target_branch SLB
