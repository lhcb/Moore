# Development issue for the line `LineName`

<!-- Please follow the line naming convention at https://gitlab.cern.ch/lhcb/Moore/-/issues/60 -->


## Line Name: `Hlt2LineName`

## Line description

Physics channel(s). Mention whether there are plans to hone the selection when porting over from Run 1/2.

## Ported HLT2 & stripping lines

List the stripping & HLT2 (lines) ported.

## Contact people

Author(s), listed with `@` handle.

## Checklist when opening:

- [ ] Milestone selected (look up the milestones starting with `SLB -` [here](https://gitlab.cern.ch/lhcb/Moore/-/milestones)).

## Checklist before closing:

- [ ] Assign other authors, if any.
- [ ] MR to the `SLB` branch with report section complete.
- [ ] Approved by reviewers.

/label ~"SLB Line"

/assign me
