###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV
from PyConf.packing import persistable_location
from SelAlgorithms.monitoring import histogram_1d, monitor

from RecoConf import monitoring_particles
from RecoConf.config import Reconstruction
from RecoConf.event_filters import require_pvs
from RecoConf.legacy_rec_hlt1_tracking import make_pvs
from RecoConf.rdbuilder_thor import make_rd_detached_dielectron
from RecoConf.reconstruction_objects import reconstruction


def _gg_mass_mon():
    return monitor(
        name="DiPhotonMassMon",
        data=monitoring_particles.diphoton(),
        histograms=[
            histogram_1d(
                name="gg_m",
                title="DiPhotonMassMon",
                label="MASS",
                functor=F.MASS,
                bins=240,
                range=(0 * MeV, 1200 * MeV),
            )
        ],
    )


def _ks_ll_mass_mon():
    return monitor(
        name="KShortLLMassMon",
        data=monitoring_particles.prompt_ks_ll(),
        histograms=[
            histogram_1d(
                name="ks_m",
                title="KShortLLMassMon",
                label="MASS",
                functor=F.MASS,
                bins=180,
                range=(410 * MeV, 590 * MeV),
            )
        ],
    )


def _ks_uu_mass_mon():
    return monitor(
        name="KShortUUMassMon",
        data=monitoring_particles.prompt_ks_uu(),
        histograms=[
            histogram_1d(
                name="ks_m",
                title="KShortUUMassMon",
                label="MASS",
                functor=F.MASS,
                bins=180,
                range=(300 * MeV, 700 * MeV),
            )
        ],
    )


def _lambda_ll_mass_mon():
    return monitor(
        name="LambdaLLMassMon",
        data=monitoring_particles.prompt_lambda_ll(),
        histograms=[
            histogram_1d(
                name="lambda_m",
                title="LambdaMassMon",
                label="MASS",
                functor=F.MASS,
                bins=120,
                range=(1080 * MeV, 1140 * MeV),
            ),
            histogram_1d(
                name="lambda0_m",
                title="Lambda0MassMon",
                label="MASS",
                functor=F.MASS * (F.PARTICLE_ID > 0),
                bins=120,
                range=(1080 * MeV, 1140 * MeV),
            ),
            histogram_1d(
                name="lambda0bar_m",
                title="Lambda0barMassMon",
                label="MASS",
                functor=F.MASS * (F.PARTICLE_ID < 0),
                bins=120,
                range=(1080 * MeV, 1140 * MeV),
            ),
        ],
    )


def _xi_lll_mass_mon():
    return monitor(
        name="XiMassMon",
        data=monitoring_particles.prompt_xi_lll(),
        histograms=[
            histogram_1d(
                name="xi_m",
                title="XiMassMon",
                label="MASS",
                functor=F.MASS,
                bins=120,
                range=(1260 * MeV, 1380 * MeV),
            )
        ],
    )


def _omega_lll_mass_mon():
    return monitor(
        name="OmegaMassMon",
        data=monitoring_particles.prompt_omega_lll(),
        histograms=[
            histogram_1d(
                name="omega_m",
                title="OmegaMassMon",
                label="MASS",
                functor=F.MASS,
                bins=80,
                range=(1600 * MeV, 1760 * MeV),
            )
        ],
    )


def _d0_mass_mon():
    return monitor(
        name="D0MassMon",
        data=monitoring_particles.prompt_d0(),
        histograms=[
            histogram_1d(
                name="d0_m",
                title="D0MassMon",
                label="MASS",
                functor=F.MASS,
                bins=150,
                range=(1715 * MeV, 2015 * MeV),
            )
        ],
    )


def _dst_mass_mon():
    return monitor(
        name="DstMassMon",
        data=monitoring_particles.prompt_dst(),
        histograms=[
            histogram_1d(
                name="dst_m",
                title="DstMassMon",
                label="MASS",
                functor=(F.MASS - F.CHILD(1, F.MASS)),
                bins=80,
                range=(140 * MeV, 180 * MeV),
            )
        ],
    )


def _dp_mass_mon():
    return monitor(
        name="DpMassMon",
        data=monitoring_particles.prompt_dp(),
        histograms=[
            histogram_1d(
                name="dp_m",
                title="DpMassMon",
                label="MASS",
                functor=F.MASS,
                bins=100,
                range=(1670 * MeV, 2070 * MeV),
            ),
            histogram_1d(
                name="dplus_m",
                title="DplusMassMon",
                label="MASS",
                functor=F.MASS * (F.PARTICLE_ID > 0),
                bins=100,
                range=(1670 * MeV, 2070 * MeV),
            ),
            histogram_1d(
                name="dminus_m",
                title="DminusMassMon",
                label="MASS",
                functor=F.MASS * (F.PARTICLE_ID < 0),
                bins=100,
                range=(1670 * MeV, 2070 * MeV),
            ),
        ],
    )


def _ds_mass_mon():
    return monitor(
        name="DsMassMon",
        data=monitoring_particles.prompt_ds(),
        histograms=[
            histogram_1d(
                name="ds_m",
                title="DsMassMon",
                label="MASS",
                functor=F.MASS,
                bins=100,
                range=(1700 * MeV, 2100 * MeV),
            )
        ],
    )


def _lc_mass_mon():
    return monitor(
        name="LcMassMon",
        data=monitoring_particles.prompt_lc(),
        histograms=[
            histogram_1d(
                name="lc_m",
                title="LcMassMon",
                label="MASS",
                functor=F.MASS,
                bins=125,
                range=(2130 * MeV, 2630 * MeV),
            )
        ],
    )


def _jpsi_mass_mon():
    return monitor(
        name="JpsiMassMon",
        data=monitoring_particles.prompt_jpsi_to_mumu(),
        histograms=[
            histogram_1d(
                name="jpsi_m",
                title="JpsiMassMon",
                label="MASS",
                functor=F.MASS,
                bins=120,
                range=(2.7 * GeV, 3.9 * GeV),
            )
        ],
    )


def _jpsi_ee():
    return make_rd_detached_dielectron(am_min=2000 * MeV, am_max=3500 * MeV)


def _jpsi_ee_mass_mon():
    return monitor(
        name="JpsiEEMassMon",
        data=_jpsi_ee(),
        histograms=[
            histogram_1d(
                name="jpsi_ee_m",
                title="JpsiEEMassMon",
                label="MASS",
                functor=F.MASS,
                bins=150,
                range=(2.0 * GeV, 3.5 * GeV),
            )
        ],
    )


def make_mass_monitors():
    with persistable_location.bind(force=False), reconstruction.bind(from_file=False):
        ks_ll_mon_reco = Reconstruction(
            "KShortLLMassMonCF",
            [_ks_ll_mass_mon()],
            filters=[monitoring_particles.prompt_ks_ll()],
        )
        ks_uu_mon_reco = Reconstruction(
            "KShortUUMassMonCF",
            [_ks_uu_mass_mon()],
            filters=[monitoring_particles.prompt_ks_uu()],
        )
        gg_mon_reco = Reconstruction(
            "DiPhotonMassMonCF",
            [_gg_mass_mon()],
            filters=[monitoring_particles.diphoton()],
        )
        lambda_ll_mon_reco = Reconstruction(
            "LambdaMassMonCF",
            [_lambda_ll_mass_mon()],
            filters=[monitoring_particles.prompt_lambda_ll()],
        )
        xi_lll_mon_reco = Reconstruction(
            "XiMassMonCF",
            [_xi_lll_mass_mon()],
            filters=[monitoring_particles.prompt_xi_lll()],
        )
        omega_lll_mon_reco = Reconstruction(
            "OmegaMassMonCF",
            [_omega_lll_mass_mon()],
            filters=[monitoring_particles.prompt_omega_lll()],
        )
        d0_mon_reco = Reconstruction(
            "D0MassMonCF", [_d0_mass_mon()], filters=[monitoring_particles.prompt_d0()]
        )
        dst_mon_reco = Reconstruction(
            "DstMassMonCF",
            [_dst_mass_mon()],
            filters=[monitoring_particles.prompt_dst()],
        )
        dp_mon_reco = Reconstruction(
            "DpMassMonCF", [_dp_mass_mon()], filters=[monitoring_particles.prompt_dp()]
        )
        ds_mon_reco = Reconstruction(
            "DsMassMonCF", [_ds_mass_mon()], filters=[monitoring_particles.prompt_ds()]
        )
        lc_mon_reco = Reconstruction(
            "LcMassMonCF", [_lc_mass_mon()], filters=[monitoring_particles.prompt_lc()]
        )
        jpsi_mon_reco = Reconstruction(
            "JpsiMassMonCF",
            [_jpsi_mass_mon()],
            filters=[monitoring_particles.prompt_jpsi_to_mumu()],
        )
        jpsiee_mon_reco = Reconstruction(
            "JpsiEEMassMonCF", [_jpsi_ee_mass_mon()], filters=[_jpsi_ee()]
        )
        reco_with_require_pvs = Reconstruction(
            "MassMonWithPVsCF",
            [
                ks_ll_mon_reco.node,
                ks_uu_mon_reco.node,
                lambda_ll_mon_reco.node,
                xi_lll_mon_reco.node,
                omega_lll_mon_reco.node,
                d0_mon_reco.node,
                dst_mon_reco.node,
                dp_mon_reco.node,
                ds_mon_reco.node,
                lc_mon_reco.node,
                jpsi_mon_reco.node,
                jpsiee_mon_reco.node,
            ],
            filters=[require_pvs(make_pvs())],
        )
        return [gg_mon_reco.node, reco_with_require_pvs.node]
