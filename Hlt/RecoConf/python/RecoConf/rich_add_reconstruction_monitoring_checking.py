###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import (
    TracksToSelection,
)
from PyConf.tonic import (
    configurable,
)

from .rich_data_monitoring import (
    alignment_rich_monitoring_options,
    alignment_rich_reco_options,
    default_rich_monitoring_options,
    make_rich_pixel_monitors,
    make_rich_track_monitors,
)
from .rich_mc_checking import (
    default_rich_checking_options,
    make_rich_checkers,
)
from .rich_reconstruction import (
    default_rich_reco_options,
    make_all_rich_pids,
    make_merged_rich_pids,
    make_rich_pids,
    make_rich_pixels,
)


@configurable
def add_hlt2_rich(
    best_tracks,
    light_reco=False,
    do_mc_checking=False,
    do_data_monitoring=False,
    do_data_monitoring_rich=False,  # can switch it on individually
    do_data_monitoring_allow_rich=True,  # can protect from global switch
    with_UT=True,
    moni_set="Standard",
    mirror_align_tasks=["Produce"],
    radiator="Rich2Gas",
    track_types=["Long", "Downstream", "Upstream"],
):
    """Returns the nodes to add for the RICH HLT2 reco."""

    # To monitor or not to monitor?
    do_data_moni = False
    # There should be at least one permission of two: RICH-only, global, and by
    # default global data monitoring (do_data_monitoring) will include RICH as
    # well, because by default do_data_monitoring_allow_rich=True.
    # But the latter can be changed in an external bind call.
    if do_data_monitoring_rich or (
        do_data_monitoring and do_data_monitoring_allow_rich
    ):
        do_data_moni = True

    reco_opts = default_rich_reco_options()

    # More reconstruction options for Cherenkov photons
    # in case of the RICH alignment monitoring.
    if do_data_moni and moni_set == "MirrorAlign":
        more_reco_opts = alignment_rich_reco_options(radiator=radiator)
        reco_opts.update(more_reco_opts)

    if light_reco:
        selections = {}
        richRecConfs = {}
        for key in track_types:
            selections[key] = TracksToSelection(
                InputLocation=best_tracks[key]["v1"]
            ).OutputLocation
            richRecConfs[key] = make_rich_pids(
                track_name=key, input_tracks=selections[key], options=reco_opts
            )
        pidMerge = {key: richRecConfs[key]["RichPIDs"] for key in richRecConfs}
    else:
        richRecConfs = make_all_rich_pids(
            best_tracks, reco_opts, track_types=track_types
        )
        # Merge them for now to be compatible with Brunel.
        pidMerge = make_merged_rich_pids(richRecConfs)

    # RICH data monitoring.
    moni_nodes = None
    if do_data_moni:
        moni_nodes = []
        # Data monitoring options
        moni_opts = default_rich_monitoring_options()
        moni_opts["UseUT"] = with_UT
        # More monitoring options in case of the RICH alignment monitoring.
        if moni_set == "MirrorAlign":
            more_moni_opts = alignment_rich_monitoring_options(radiator=radiator)
            moni_opts.update(more_moni_opts)
        # Rich pixel-level data monitoring
        richPixelConf = make_rich_pixels(options=reco_opts)
        richPixelMonitors = make_rich_pixel_monitors(
            conf=richPixelConf,
            reco_opts=reco_opts,
            moni_opts=moni_opts,
            moni_set=moni_set,
        )
        moni_nodes += [v for _, v in sorted(richPixelMonitors.items())]
        # Rich track level data monitoring
        for key, conf in sorted(richRecConfs.items()):
            riTkMoni = make_rich_track_monitors(
                conf=conf,
                reco_opts=reco_opts,
                moni_opts=moni_opts,
                moni_set=moni_set,
                mirror_align_tasks=mirror_align_tasks,
            )
            moni_nodes += [v for _, v in sorted(riTkMoni.items())]

    # RICH MC checking.
    mc_nodes = None
    if do_mc_checking:
        mc_nodes = []
        # Default MC checking options.
        check_opts = default_rich_checking_options()
        check_opts["UseUT"] = with_UT
        for key, conf in sorted(richRecConfs.items()):
            # MC checkers require original 'v1' container before filtering.
            # To Be removed...
            if light_reco:
                conf["OriginalV1Tracks"] = best_tracks[key]["v1"]
            else:
                conf["OriginalV1Tracks"] = best_tracks["v1"]
            riMCcheck = make_rich_checkers(
                conf=conf, reco_opts=reco_opts, check_opts=check_opts
            )
            mc_nodes += [v for _, v in sorted(riMCcheck.items())]

    return {"rich_pids": pidMerge, "monitoring": moni_nodes, "mcchecking": mc_nodes}
