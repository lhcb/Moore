###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Uncomment when Rec!587 is merged
# from PyConf.Algorithms import VeloKalmanTrackV1
import logging

import Functors as F

## Algorithms for PV translations / rotations
# Algorithms to get the Pr hits
# Algorithms to take care of decay-tree refitting
## Algorithms for PV refitting
# Optional workflow on PV association
from PyConf.Algorithms import (
    FlattenDecayTree,
    LHCbIDOverlapRelationTableParticleToTrack,
    LHCbIDOverlapRelationTableTrackToTrack,
    PVsEmptyProducer,
    RecombineDecayTrees,
    ReplaceTracksInRecVertex,
    SelectFTClustersFromTracks,
    SelectTracksForParticles,
    SelectTracksForRecVertices,
    SelectUTClustersFromTracks,
    SelectVPMicroClustersFromTracks,
    SharedTrackEventFitter,
    UpdateVertexCoordinatesOffline,
    UTHitClustersToPrUTHitsConverter,
    UTHitClustersToUTHitHandlerConverter,
    V1V1PrKalmanFilter,
    VPLightClustersToVPHitsConverter,
    VPMicroClustersToVPLightClustersConverter,
)

from RecoConf.algorithms_thor import ParticleFilter

# Uncomment when Rec!587 is merged
# from PyConf.Algorithms import VeloKalmanTrackV1
# Algotihms to perform the fit
from RecoConf.hlt2_tracking import (
    get_global_measurement_provider,
    get_track_master_fitter,
)
from RecoConf.legacy_rec_hlt1_tracking import (
    get_global_clusters_on_track_tool,
    make_FTRawBankDecoder_clusters,
    make_PrStoreSciFiHits_hits,
    make_PrStoreUTHit_hits,
    make_ut_hit_clusters,
    make_velo_micro_clusters,
    make_VPClus_hits,
)

log = logging.getLogger(__name__)

TRACK_FIT_TYPE_PRKALMAN = "PrKalman"
TRACK_FIT_TYPE_VELOKALMAN = "VeloKalman"
TRACK_FIT_TYPE_TRACKMASTERFITTER = "TrackMasterFitter"


def refit_tracks(tracks, get_clusters_from_track, track_fitter):
    if get_clusters_from_track:
        velo_micro_clusters_on_tracks = SelectVPMicroClustersFromTracks(Inputs=[tracks])
        scifi_clusters_on_tracks = SelectFTClustersFromTracks(Inputs=[tracks])
        ut_clusters_on_tracks = SelectUTClustersFromTracks(Inputs=[tracks])
        # add geometry dependent information to the clusters
        vp_light_clusters_on_tracks = VPMicroClustersToVPLightClustersConverter(
            InputClusters=velo_micro_clusters_on_tracks
        )
        converted_track_ut_hits = UTHitClustersToUTHitHandlerConverter(
            InputClusters=ut_clusters_on_tracks
        )
    else:
        ut_clusters_on_tracks = make_ut_hit_clusters()
        velo_micro_clusters_on_tracks = make_velo_micro_clusters()
        scifi_clusters_on_tracks = make_FTRawBankDecoder_clusters()

        # add geometry dependent information to the clusters
        converted_track_ut_hits = make_PrStoreUTHit_hits()
        vp_light_clusters_on_tracks = make_VPClus_hits()

    ### To configure the track fit, two things have to be changed:
    ### (1) the Measurement Provider need the right pointers to
    ### the locations from the cluters on the track.
    ###
    ### (2) the clusters-on-track-tool needs to be pointed to the
    ### micro/light clusters that we just got from the track
    clusters_on_track_tool = get_global_clusters_on_track_tool(
        velo_micro_clusters=(lambda: velo_micro_clusters_on_tracks),
        ft_clusters=(lambda: scifi_clusters_on_tracks),
        ut_hits=(lambda: ut_clusters_on_tracks),
    )

    if track_fitter == TRACK_FIT_TYPE_TRACKMASTERFITTER:
        measurement_provider = get_global_measurement_provider(
            velo_hits=(lambda: vp_light_clusters_on_tracks),
            ut_hits=(lambda: converted_track_ut_hits),
            ft_clusters=(lambda: scifi_clusters_on_tracks),
        )

        track_refitting = SharedTrackEventFitter(
            TracksInContainer=tracks,
            Fitter=get_track_master_fitter(
                clusters_on_track_tool=(lambda: clusters_on_track_tool),
                get_measurement_provider=(lambda: measurement_provider),
            ),
        )

        refitted_tracks = track_refitting.TracksOutContainer

    elif track_fitter == TRACK_FIT_TYPE_PRKALMAN:
        pr_hits_vp = VPLightClustersToVPHitsConverter(
            InputClusters=vp_light_clusters_on_tracks
        )
        pr_hits_ft = make_PrStoreSciFiHits_hits(
            make_ft_clusters=(lambda: scifi_clusters_on_tracks)
        )
        pr_hits_ut = UTHitClustersToPrUTHitsConverter(
            InputClusters=ut_clusters_on_tracks
        )

        from PyConf.Tools import KalmanFilterTool, TrackMasterExtrapolator

        from RecoConf.core_algorithms import make_unique_id_generator
        from RecoConf.hlt2_tracking import get_global_materiallocator

        track_refitter = V1V1PrKalmanFilter(
            HitsVP=pr_hits_vp,
            HitsUT=pr_hits_ut,
            HitsFT=pr_hits_ft,
            TrackFitter=KalmanFilterTool(
                ReferenceExtrapolator=TrackMasterExtrapolator(
                    MaterialLocator=get_global_materiallocator()
                ),
                TrackAddClusterTool=clusters_on_track_tool,
                MaxChi2PreOutlierRemoval=9999999,  # disable - try to keep as many tracks as possible
                MaxChi2=9999999,  # disable - try to keep as many tracks as possible
                UniqueIDGenerator=make_unique_id_generator(),
            ),
            Input=tracks,
        ).OutputTracks

        refitted_tracks = track_refitter

    elif track_fitter == TRACK_FIT_TYPE_VELOKALMAN:
        if not get_clusters_from_track:
            log.error(
                "Configuration error: VeloKalman only supports clusters on track."
            )

        log.error("Functionality pending - see Rec!587 and Rec MR 4113")
        # refitted_tracks = VeloKalmanTrackV1(
        #    TracksLocation=tracks).OutputTracksLocation
        refitted_tracks = None

    return refitted_tracks


def refit_decay_tree(
    input_particles,
    input_pvs,
    get_clusters_from_track=True,
    track_fitter=TRACK_FIT_TYPE_PRKALMAN,
    **kwargs,
):
    """Takes an input DataHandle that contains LHCb::Particles (such as the
    output of a trigger line), and:
     (1) finds out which particles are basic particles, and calls the
         track refitter on those
     (2) takes the refitted tracks, along with the relation table,
          to then call the DecayTree recombiner which re-combines the
          particles together, and fits again the decay vertices.

      The output contains particles, the new vertices, the new protoparticles,
      and again a relation table between the old particles and the new ones.

      When get_clusters_from_track is false, the clusters are instead searched
      for in the standard raw event - this only works when this is persisted.
    """

    if "use_PrKF" in kwargs:
        log.warning(
            "Detected 'use_PrKF' as part of the configuration of refit_decay_tree."
        )
        log.warning(
            "Please update your configuration - this has been changed in November 2024!"
        )

        log.warning("( when using PrKF: remove the argument (default),")
        log.warning(
            "when not using the PrKF: use track_fitter=track_refitting.TRACK_FIT_TYPE_TRACKMASTERFITTER )"
        )

        if kwargs["use_PrKF"]:
            track_fitter = TRACK_FIT_TYPE_PRKALMAN
        else:
            track_fitter = TRACK_FIT_TYPE_TRACKMASTERFITTER

    flat_particles = FlattenDecayTree(InputParticles=input_particles)
    basic_particles = ParticleFilter(
        flat_particles.OutputParticles,
        F.FILTER(F.ISBASICPARTICLE),  # ideally swap to some F.HASTRACK functor
        name="SelectBasicParticlesForRefit_{hash}",
    )

    tracks_from_particles = SelectTracksForParticles(
        Inputs=[basic_particles]
    ).OutputTracks

    tracks_from_dst = refit_tracks(
        tracks_from_particles, get_clusters_from_track, track_fitter=track_fitter
    )

    relation_table_match_by_veloid_to_track = LHCbIDOverlapRelationTableParticleToTrack(
        MatchFrom=basic_particles,
        MatchTo=tracks_from_dst,
        IncludeVP=True,
        IncludeFT=True,
        IncludeUT=True,
    ).OutputRelations

    if input_pvs is None:
        input_pvs = PVsEmptyProducer()

    recombined_trees = RecombineDecayTrees(
        InputParticles=input_particles,
        InputPVs=input_pvs,
        InputTracks=tracks_from_dst,  # the refitted tracks
        InputTrackRelations=relation_table_match_by_veloid_to_track,
    )  # the relation table

    return recombined_trees


def refit_pvs(
    *, input_pvs, track_fitter=TRACK_FIT_TYPE_PRKALMAN, keep_unconverged_fits=True
):
    """Takes an input DataHandle that contains LHCb::RecVertices, and:
    (1) takes the tracks from the primary vertices
    (2) refits those tracks, using the arguments provided in the kwargs to
        pass as arguments to refit_tracks()
    (3) creates the new PV container, with swapped tracks and runs the PV fit
         again. The new PVs have a relation to the old ones.

     The output contains the new vertices and a relation table between
     the old particles and the new ones (2-way).

     The track_fitter agrgument is a string (see the options defined on top
     of this file) to distinguish between different Kalman filter implementations,
     with the standard being also the standard in HLT2.

     In HLT2, also PVs whose fit does *not* converge are kept. This behaviour
     is reproduced by running with keep_unconverged_fits as True. If you wish
     you be stricter, turn it off.
    """
    pv_tracks = SelectTracksForRecVertices(Inputs=[input_pvs])

    refitted_pv_tracks = refit_tracks(
        tracks=pv_tracks, track_fitter=track_fitter, get_clusters_from_track=True
    )

    relation_table_match_by_veloid_to_track = LHCbIDOverlapRelationTableTrackToTrack(
        MatchFrom=pv_tracks, MatchTo=refitted_pv_tracks, IncludeVP=True
    ).OutputRelations

    return ReplaceTracksInRecVertex(
        name="RefitPVs_{hash}",
        InputVertices=input_pvs,
        InputTracks=refitted_pv_tracks,
        InputTrackRelations=relation_table_match_by_veloid_to_track,
        OnlyKeepConvergedFits=(not keep_unconverged_fits),
    )


def update_vertex_positions(*, input_pvs):
    """Takes as input the DataHandle of RecVertices, and applies
    a coordinate transform as defined in the conditions (and
    accessible via in `DeLHCb::offlineCoordinateTransformation`).

    These vertices are *not* fitted again, but merely shifted / rotated
    around to account for a change in global coordinate transformation.

    This is safe to be used on all data types, including those without
    the PV tracks persisted.

    The output contains the new vertices and a relation table between
    the old particles and the new ones (2-way).
    """

    return UpdateVertexCoordinatesOffline(
        name="UpdateVertexCoordinatesOffline_{hash}", InputVertices=input_pvs
    )
