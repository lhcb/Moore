###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Load data from files and set up unpackers.

There are two things we have to deal with:

1. Loading the data from the file in to the TES, done by PyConf.input_from_root_file
2. Unpacking and preparing packed containers, if the 'reconstruction' is
   defined as the objects already present in the file.

In most LHCb applications, step 2 is done for you behind the scenes. The
DataOnDemandSvc is configured in LHCb/GaudiConf/DstConf.py to unpack containers
when they are requested. It also configures adding RICH, MUON, and combined PID
information to ProtoParticles when they're unpacked. Because we don't have the
DataOnDemandSvc here, we have to do this by hand.

The interesting 'user-facing' exports of this module are:
* [reco,mc]_unpacker(): get Algorithm that produces a container of given unpacked object name.
"""

import collections

from Configurables import (
    MCRichDigitSummaryUnpacker,
    MCRichHitUnpacker,
    MCRichOpticalPhotonUnpacker,
    MCRichSegmentUnpacker,
    MCRichTrackUnpacker,
    UnpackCaloHypo,
    UnpackMCCaloHit,
    UnpackMCHit,
    UnpackMuonPIDs,
    UnpackProtoParticle,
    UnpackRecVertex,
    UnpackRichPIDs,
    UnpackTrack,
)

#####
# New unpackers can not unpack old dst files from brunel
# so here we are using old unpackers which unpack from packed objects
# while new unpackers will unpack from packed data buffers
#####
from Gaudi.Configuration import ERROR
from PyConf.application import input_from_root_file
from PyConf.components import Algorithm, force_location
from PyConf.reading import get_mc_particles, get_mc_vertices
from PyConf.Tools import (
    ChargedProtoParticleAddCaloHypos,
    ChargedProtoParticleAddCombineDLLs,
    ChargedProtoParticleAddMuonInfo,
    ChargedProtoParticleAddRichInfo,
)


def __packed_location_for_(unpacked):
    m = {"/Event/MC": "/Event/pSim", "/Event/Rec": "/Event/pRec"}
    for k, v in m.items():
        if unpacked.startswith(k):
            return v + unpacked.removeprefix(k)
    raise KeyError(unpacked)


def _unpacker(forced_type, unpacked_loc, **kwargs):
    """Return unpacker that reads from file and unpacks to a forced output location."""
    assert "name" not in kwargs

    _unpack_configurable = {
        "LHCb::PackedRecVertices": UnpackRecVertex,
        "LHCb::PackedCaloHypos": UnpackCaloHypo,
        "LHCb::PackedMuonPID": UnpackMuonPIDs,
        "LHCb::PackedRichPID": UnpackRichPIDs,
        "LHCb::PackedTracks": UnpackTrack,
        "LHCb::PackedProtoParticles": UnpackProtoParticle,
        "LHCb::PackedMCHit": UnpackMCHit,
        "LHCb::PackedMCCaloHits": UnpackMCCaloHit,
        "LHCb::PackedMCRichHits": MCRichHitUnpacker,
        "LHCb::PackedMCRichSegments": MCRichSegmentUnpacker,
        "LHCb::PackedMCRichTracks": MCRichTrackUnpacker,
        "LHCb::PackedMCRichOpticalPhotons": MCRichOpticalPhotonUnpacker,
        "LHCb::PackedMCRichDigitSummarys": MCRichDigitSummaryUnpacker,
    }
    return Algorithm(
        _unpack_configurable[forced_type],
        name=f"Unpack_{unpacked_loc.replace('/', '_')}",
        outputs={"OutputName": force_location(unpacked_loc)},
        InputName=input_from_root_file(
            __packed_location_for_(unpacked_loc), forced_type=forced_type
        ),
        **kwargs,
    )


def _get_reco_unpackers():
    # for each key, specify both the location in the TES and the type that is
    # expected to be made available by reading it from a file, the dependencies, and
    # any extra arguments
    _packed_reco_from_file = {
        "PVs": ("LHCb::PackedRecVertices", "Vertex/Primary", None, None),
        "CaloElectrons": ("LHCb::PackedCaloHypos", "Calo/Electrons", None, None),
        "CaloPhotons": ("LHCb::PackedCaloHypos", "Calo/Photons", None, None),
        "CaloMergedPi0s": ("LHCb::PackedCaloHypos", "Calo/MergedPi0s", None, None),
        "CaloSplitPhotons": ("LHCb::PackedCaloHypos", "Calo/SplitPhotons", None, None),
        "MuonPIDs": ("LHCb::PackedMuonPID", "Muon/MuonPID", ("Tracks",), None),
        "RichPIDs": (
            "LHCb::PackedRichPID",
            "Rich/PIDs",
            ("Tracks",),
            {"OutputLevel": ERROR},
        ),
        "Tracks": ("LHCb::PackedTracks", "Track/Best", None, None),
        "NeutralProtos": (
            "LHCb::PackedProtoParticles",
            "ProtoP/Neutrals",
            ("CaloPhotons", "CaloMergedPi0s", "CaloSplitPhotons"),
            None,
        ),
        "ChargedProtos": (
            "LHCb::PackedProtoParticles",
            "ProtoP/Charged",
            ("Tracks", "RichPIDs", "MuonPIDs"),
            None,
        ),
    }

    def _reco_unpacker(key, **kwargs):
        """Return unpacker that reads from file and unpacks to a forced output location."""
        packed_type, location, deps, extra = _packed_reco_from_file[key]
        if extra:
            kwargs.update(extra)
        if deps:
            kwargs.update({"ExtraInputs": [_reco_unpacker(i) for i in deps]})
        # The SmartRefs held by the unpacked objects only work if we unpack to these specific locations
        return _unpacker(packed_type, "/Event/Rec/" + location, **kwargs)

    muonPIDs = _reco_unpacker("MuonPIDs")
    richPIDs = _reco_unpacker("RichPIDs")
    electrons = _reco_unpacker("CaloElectrons")
    photons = _reco_unpacker("CaloPhotons")
    charged_protos = _reco_unpacker(
        "ChargedProtos",
        AddInfo=[
            # only overwrite, not remove old info to not invalidate global pid object
            ChargedProtoParticleAddRichInfo(
                InputRichPIDLocation=richPIDs, RemoveOldInfo=False
            ),
            ChargedProtoParticleAddMuonInfo(
                InputMuonPIDLocation=muonPIDs, RemoveOldInfo=False
            ),
            # NOTE missing CaloHypo links to CellID due to missing Cluster persistency is not reported
            ChargedProtoParticleAddCaloHypos(
                ElectronHypos=electrons, PhotonHypos=photons, ReportMissing=False
            ),
            ChargedProtoParticleAddCombineDLLs(RemoveOldInfo=False),
        ],
    )

    return collections.OrderedDict(
        [
            ("PVs", _reco_unpacker("PVs")),
            ("CaloElectrons", electrons),
            ("CaloPhotons", photons),
            ("CaloMergedPi0s", _reco_unpacker("CaloMergedPi0s")),
            ("CaloSplitPhotons", _reco_unpacker("CaloSplitPhotons")),
            ("MuonPIDs", muonPIDs),
            ("RichPIDs", richPIDs),
            ("Tracks", _reco_unpacker("Tracks")),
            ("NeutralProtos", _reco_unpacker("NeutralProtos")),
            ("ChargedProtos", charged_protos),
        ]
    )


# precomputed dict of reco unpackers, will be filled on first use
_reco_unpackers = None


def _get_mc_unpackers():
    # for each key, specify both the location in the TES and the type that is
    # expected to be made available by reading it from a file
    _packed_mc_from_file = {
        "MCVPHits": ("LHCb::PackedMCHit", "VP/Hits"),
        "MCUTHits": ("LHCb::PackedMCHit", "UT/Hits"),
        "MCFTHits": ("LHCb::PackedMCHit", "FT/Hits"),
        "MCRichHits": ("LHCb::PackedMCRichHits", "Rich/Hits"),
        "MCRichSegments": ("LHCb::PackedMCRichSegments", "Rich/Segments"),
        "MCRichTracks": ("LHCb::PackedMCRichTracks", "Rich/Tracks"),
        "MCRichOpticalPhotons": (
            "LHCb::PackedMCRichOpticalPhotons",
            "Rich/OpticalPhotons",
        ),
        "MCEcalHits": ("LHCb::PackedMCCaloHits", "Ecal/Hits"),
        "MCHcalHits": ("LHCb::PackedMCCaloHits", "Hcal/Hits"),
        "MCMuonHits": ("LHCb::PackedMCHit", "Muon/Hits"),
        "MCRichDigitSummaries": (
            "LHCb::PackedMCRichDigitSummarys",
            "Rich/DigitSummaries",
        ),
    }

    def _mc_unpacker(key, **kwargs):
        """Return unpacker that reads from file and unpacks to a forced output location."""
        packed_type, location = _packed_mc_from_file[key]
        # The SmartRefs held by the unpacked MC objects only work if we unpack to these specific locations
        return _unpacker(
            packed_type, "/Event/MC/" + location.removeprefix("MC"), **kwargs
        )

    # Ordered so that dependents are unpacked first
    # Make sure that MC particles and MC vertices are unpacked together,
    # see https://gitlab.cern.ch/lhcb/LHCb/issues/57 for details.
    return collections.OrderedDict(
        [
            ("MCRichDigitSummaries", _mc_unpacker("MCRichDigitSummaries")),
            ("MCParticles", get_mc_particles("/Event/MC/Particles")),
            ("MCVertices", get_mc_vertices("/Event/MC/Vertices")),
            ("MCVPHits", _mc_unpacker("MCVPHits")),
            ("MCUTHits", _mc_unpacker("MCUTHits")),
            ("MCFTHits", _mc_unpacker("MCFTHits")),
            ("MCRichHits", _mc_unpacker("MCRichHits")),
            ("MCRichSegments", _mc_unpacker("MCRichSegments")),
            ("MCRichTracks", _mc_unpacker("MCRichTracks")),
            ("MCRichOpticalPhotons", _mc_unpacker("MCRichOpticalPhotons")),
            ("MCEcalHits", _mc_unpacker("MCEcalHits")),
            ("MCHcalHits", _mc_unpacker("MCHcalHits")),
            ("MCMuonHits", _mc_unpacker("MCMuonHits")),
        ]
    )


# precomputed dict of mc unpackers, will be filled on first use
_mc_unpackers = None


def reco_unpacker(key):
    """Returns the adequate unpacker for the given key"""
    global _reco_unpackers
    if _reco_unpackers is None:
        _reco_unpackers = _get_reco_unpackers()
    return _reco_unpackers[key]


def reco_unpackers():
    global _reco_unpackers
    if _reco_unpackers is None:
        _reco_unpackers = _get_reco_unpackers()
    return _reco_unpackers


def mc_unpacker(key):
    """Returns the adequate unpacker for the given key"""
    global _mc_unpackers
    if _mc_unpackers is None:
        _mc_unpackers = _get_mc_unpackers()
    return _mc_unpackers[key]


def mc_unpackers():
    global _mc_unpackers
    if _mc_unpackers is None:
        _mc_unpackers = _get_mc_unpackers()
    return _mc_unpackers


def boole_links_digits_mcparticles(key):
    """Return a dict of locations for MC linker tables (to mcparticles) created by Boole."""
    locations = {
        "EcalDigitsV1": "Ecal/Digits",
        "EcalDigits": "Ecal/Digits2MCParticles",
        "FTLiteClusters": "FT/LiteClusters",
        "HcalDigitsV1": "Hcal/Digits",
        "HcalDigits": "Hcal/Digits2MCParticles",
        "MuonDigits": "Muon/Digits",
        "UTClusters": "UT/Clusters",
        "UTDigits": "UT/TightDigits",
        "VPDigits": "VP/Digits",
    }
    return input_from_root_file(
        "/Event/Link/Raw/" + locations[key], forced_type="LHCb::LinksByKey"
    )


def boole_links_digits_mchits(key):
    """Return a dict of locations for MC linker tables (to mchits) created by Boole.

    These locations are only propagated out of Boole for extendend DIGI and DST types.
    """
    locations = {
        "FTLiteClusters": "FT/LiteClusters2MCHits",
        "UTClusters": "UT/Clusters2MCHits",
        "UTDigits": "UT/TightDigits2MCHits",
        "VPDigits": "VP/Digits2MCHits",
    }
    return input_from_root_file(
        "/Event/Link/Raw/" + locations[key], forced_type="LHCb::LinksByKey"
    )


def brunel_links(key):
    """Return a dict of locations for MC linker tables created by Brunel."""
    locations = {
        "CaloElectrons": "Calo/Electrons",
        "CaloMergedPi0s": "Calo/MergedPi0s",
        "CaloPhotons": "Calo/Photons",
        "CaloSplitPhotons": "Calo/SplitPhotons",
        "Tracks": "Track/Best",
    }
    return input_from_root_file(
        "/Event/Link/Rec/" + locations[key], forced_type="LHCb::LinksByKey"
    )
