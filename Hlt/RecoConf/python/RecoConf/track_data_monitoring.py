###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable
from PyConf.Algorithms import (
    DeterministicPrescaler,
    FTTrackMonitor,
    GaudiAllenPVsToPrimaryVertexContainer,
    PrSciFiHitsMonitor,
    RandomTrackContainerSplitter,
    RandomVeloTrackContainerSplitter,
    TrackCorrelationsMonitor,
    TrackFitMatchMonitor,
    TrackMonitor,
    TrackPV2HalfMonitor,
    TrackVertexMonitor,
    TrackVPOverlapMonitor,
    VertexCompare,
)
from PyConf.application import make_odin
from PyConf.control_flow import CompositeNode, NodeLogic

from RecoConf.hlt2_tracking import make_PrStoreSciFiHits_hits
from RecoConf.legacy_rec_hlt1_tracking import (
    make_PatPV3DFuture_pvs,
    make_reco_pvs,
    make_TrackBeamLineVertexFinderSoA_pvs,
)


def get_default_monitoring_track_types_for_light_reco():
    """Returns default monitoring track types of light reco sequence.

    Returns:
        Dict with lists of strings.
    """
    return [
        "BestLong",
        "BestSeed",
        "BestVelo",
        "BestVeloBackward",
        "BestDownstream",
        "BestUpstream",
    ]


@configurable
def get_monitoring_track_types_for_light_reco(
    skip_UT=False, track_types=get_default_monitoring_track_types_for_light_reco()
):
    """Returns monitoring track types of light reco sequence.

    Returns:
        Dict with lists of strings.
    """
    if skip_UT:
        if "BestDownstream" in track_types:
            track_types.remove("BestDownstream")
        if "BestUpstream" in track_types:
            track_types.remove("BestUpstream")

    return {"Best": track_types}


@configurable
def monitor_tracking(
    tracks, velo_tracks, tracks_for_pvs, pvs, use_pr_kf=False, prescale=1.0
):
    prescaler = DeterministicPrescaler(
        name="TrackMonitoringPrescaler",
        AcceptFraction=prescale,
        SeedName="TrackMonitoringPrescaler",
        ODINLocation=make_odin(),
    )
    scifihits = make_PrStoreSciFiHits_hits()
    vertex_moni = TrackVertexMonitor(
        name="TrackVertexMonitor", PVContainer=pvs, TrackContainer=tracks
    )

    fitmatch_moni = TrackFitMatchMonitor(
        name="TrackFitMatchMonitor", TrackContainer=tracks
    )

    track_corr_moni = TrackCorrelationsMonitor(
        name="TrackCorrelationsMonitor",
        TracksInContainer=tracks,
        SciFiHits=scifihits,
        StatesFor2DHits=["VELO", "UT", "FT"],
    )

    track_moni = TrackMonitor(name="TrackMonitor", TracksInContainer=tracks)

    velo_track_moni = TrackMonitor(
        name="VeloTrackMonitor", TracksInContainer=velo_tracks
    )

    vpoverlap_moni = TrackVPOverlapMonitor(
        name="TrackVPOverlapMonitor", TrackContainer=tracks
    )

    ft_track_moni = FTTrackMonitor(name="FTTrackMonitor", TracksInContainer=tracks)

    pv2half_moni = TrackPV2HalfMonitor(
        name="TrackPV2HalfMonitor",
        TrackContainer=tracks_for_pvs,
        ODINLocation=make_odin(),
    )
    scifi_hits_moni = PrSciFiHitsMonitor(
        name="PrSciFiHitsMonitor",
        SciFiHits=make_PrStoreSciFiHits_hits(),
        ODINLocation=make_odin(),
    )

    return CompositeNode(
        "Monitoring",
        [
            prescaler,
            vertex_moni,
            fitmatch_moni,
            track_moni,
            track_corr_moni,
            velo_track_moni,
            vpoverlap_moni,
            pv2half_moni,
            scifi_hits_moni,
            ft_track_moni,
        ],
        combine_logic=NodeLogic.LAZY_AND,
        force_order=True,
    )


@configurable
def monitor_vertex_resolution(tracks_for_pvs):
    # Do the imports here such that the build of the Moore function
    # cache still works on +cuda platforms, where the Allen algorithm
    # wrappers are not built.
    from AllenConf.primary_vertex_reconstruction import make_pvs
    from AllenConf.velo_reconstruction import decode_velo, make_velo_tracks_ACsplit

    # ------------------- Allen PV reconstruction alghorhithm (TBLV) -------------------
    # reconstruct Allen velo tracks
    decoded_velo = decode_velo()
    # Split of velo tracks to two random sets - RS1 and RS2
    velo_tracks_RS1, velo_tracks_RS2 = make_velo_tracks_ACsplit(
        decoded_velo, split_alg="random"
    )

    # reconstruct Allen PVs
    pvs_RS1 = make_pvs(velo_tracks_RS1, pv_name="_RS1", velo_open=False)

    # convert Allen PVs to PrimaryVertexContainer object
    pv_container_RS1 = GaudiAllenPVsToPrimaryVertexContainer(
        number_of_multivertex=pvs_RS1["dev_number_of_multi_final_vertices"],
        reconstructed_multi_pvs=pvs_RS1["dev_multi_final_vertices"],
    ).OutputPVs

    pvs_RS2 = make_pvs(velo_tracks_RS2, pv_name="_RS2", velo_open=False)

    # convert Allen PVs to PrimaryVertexContainer object
    pv_container_RS2 = GaudiAllenPVsToPrimaryVertexContainer(
        number_of_multivertex=pvs_RS2["dev_number_of_multi_final_vertices"],
        reconstructed_multi_pvs=pvs_RS2["dev_multi_final_vertices"],
    ).OutputPVs

    vertex_compare_Allen = VertexCompare(
        name="vertex_resolution_Allen",
        inputVerticesName1=pv_container_RS1,
        inputVerticesName2=pv_container_RS2,
    )

    # ------------------- Moore PV reconstruction alghorhithms  (PatPV3D, TBLV)-------------------
    random_splitter_forward = RandomVeloTrackContainerSplitter(
        name="RandomVeloTrackContainerSplitter_Forward",
        TracksInContainer=tracks_for_pvs["Pr"],
    )

    random_splitter_backward = RandomVeloTrackContainerSplitter(
        name="RandomVeloTrackContainerSplitter_Backward",
        TracksInContainer=tracks_for_pvs["Pr::backward"],
    )

    random_splitter = RandomTrackContainerSplitter(
        name="RandomTrackContainerSplitter", TracksInContainer=tracks_for_pvs["v1"]
    )

    RS1_velo = {
        "v1": random_splitter.PassedContainer,
        "Pr": random_splitter_forward.PassedContainer,
        "Pr::backward": random_splitter_backward.PassedContainer,
    }
    RS2_velo = {
        "v1": random_splitter.RejectedContainer,
        "Pr": random_splitter_forward.RejectedContainer,
        "Pr::backward": random_splitter_backward.RejectedContainer,
    }

    pvs1_TBLV = make_reco_pvs(
        RS1_velo, make_pvs_from_velo_tracks=make_TrackBeamLineVertexFinderSoA_pvs
    )
    pvs2_TBLV = make_reco_pvs(
        RS2_velo, make_pvs_from_velo_tracks=make_TrackBeamLineVertexFinderSoA_pvs
    )

    vertex_compare_Moore_TBLV = VertexCompare(
        name="vertex_resolution_Moore_TBLV",
        inputVerticesName1=pvs1_TBLV["v3"],
        inputVerticesName2=pvs2_TBLV["v3"],
    )

    pvs1_PatPV3D = make_reco_pvs(
        RS1_velo, None, make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs
    )
    pvs2_PatPV3D = make_reco_pvs(
        RS2_velo, None, make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs
    )

    vertex_compare_Moore_PatPV3D = VertexCompare(
        name="vertex_resolution_Moore_PatPV3D",
        inputVerticesName1=pvs1_PatPV3D["v3"],
        inputVerticesName2=pvs2_PatPV3D["v3"],
    )

    return [
        vertex_compare_Allen,
        vertex_compare_Moore_TBLV,
        vertex_compare_Moore_PatPV3D,
    ]
