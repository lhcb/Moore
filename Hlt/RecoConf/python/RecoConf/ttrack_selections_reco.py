###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define the necessary reconstruction chain to go from Ttracks to particles
"""

from PyConf import configurable
from PyConf.Algorithms import (
    TrackAncestorAssociatorByKey,
    TtrackAncestorAssociatorByTableIndex,
)
from PyConf.Tools import (
    ParticleVertexFitter,
    TrackInterpolator,
    TrackRungeKuttaExtrapolator,
    TrackStateProvider,
)

from RecoConf.hlt2_tracking import (
    convert_tracks_to_v3_from_v1,
    get_GhostProbabilityTools,
    make_TrackBestTrackCreator_tracks,
)
from RecoConf.protoparticles import make_charged_protoparticles
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.rich_reconstruction import default_rich_reco_options, make_rich_pids


def make_fitted_ttracks(global_reco=reconstruction, skip_UT=True):
    tracks = global_reco()["AllTrackHandles"]
    decloned_seed_fitted = tracks["SeedDeclonedFitted"]["v1"]
    decloned_seed = global_reco()["Ttracks"]
    decloned_seed_fitted_anc = TrackAncestorAssociatorByKey(
        InputTracks=decloned_seed_fitted, AncestorTracks=decloned_seed
    )

    best_decloned_seed = make_TrackBestTrackCreator_tracks(
        tracks=[decloned_seed_fitted_anc],
        name="TBTC_Seed_{hash}",
        get_ghost_tools=get_GhostProbabilityTools(
            track_type="Ttrack", without_UT=skip_UT
        ),
        do_not_refit=True,
        fit_tracks=False,
        skip_UT=skip_UT,
    )["Best"]

    return {"v1": best_decloned_seed}


def make_mva_ttracks(global_reco=reconstruction, skip_UT=True):
    tracks = global_reco()["AllTrackHandles"]
    decloned_seed = global_reco()["Ttracks"]
    decloned_filtered_seed_fitted = tracks["FilteredSeedDeclonedFitted"]["v1"]
    decloned_filtered_seed_rels = tracks["Extras"]["FilteredSeedRels"]

    decloned_filtered_seed_anc = TtrackAncestorAssociatorByTableIndex(
        InputTracks=decloned_filtered_seed_fitted,
        InputTracksTable=decloned_filtered_seed_rels,
        AncestorTracks=decloned_seed,
    ).OutputLocation

    best_decloned_filtered_seed = make_TrackBestTrackCreator_tracks(
        tracks=[decloned_filtered_seed_anc],
        name="TBTC_Seed_{hash}",
        get_ghost_tools=get_GhostProbabilityTools(
            track_type="Ttrack", without_UT=skip_UT
        ),
        do_not_refit=True,
        fit_tracks=False,
        skip_UT=skip_UT,
    )["Best"]

    return {"v1": best_decloned_filtered_seed}


# RICH PIDs from T tracks
def make_ttrack_rich_pids(ttracks):
    return make_rich_pids(
        "Ttrack", ttracks["v1"], default_rich_reco_options(), location=None
    )["RichPIDs"]


def make_ttrack_muon_pids(ttracks):
    from RecoConf.muon_reconstruction import make_all_muon_pids, make_conv_muon_pids

    tracks_v3, tracks_rels = convert_tracks_to_v3_from_v1({"Ttrack": ttracks["v1"]})

    muonRecConfs = make_all_muon_pids(tracks=tracks_v3, muon_pid_track_types=["Ttrack"])
    muon_pids, muon_tracks = make_conv_muon_pids(
        muonRecConfs, tracks_rels, track_types=["Ttrack"]
    )

    return muon_pids["Ttrack"]


# All the T track reco
@configurable
def make_ttrack_reco(global_reco, make_ttracks, skipRich=False, skipCalo=False):
    good_ttracks = make_ttracks()
    rich_pids = make_ttrack_rich_pids(good_ttracks) if not skipRich else None
    # TODO calo reco was configured wrong (to be added appropriately)
    calo_pids = None
    muon_pids = make_ttrack_muon_pids(good_ttracks)

    return {
        "good_ttracks": good_ttracks,
        "rich_pids": rich_pids,
        "calo_pids": calo_pids,
        "muon_pids": muon_pids,
    }


# define protoparticle maker for T tracks that uses the above reconstruction
@configurable
def make_ttrack_protoparticles(
    track_type="Ttrack",
    name="TTrackSel_ProtoParticleMaker",
    make_ttrack_reco=make_ttrack_reco,
    make_ttracks=make_fitted_ttracks,
    make_global_reco=reconstruction,
):
    """ttrack protoparticle maker
    This is used to make ttrack protoparticles
    """

    global_reco = make_global_reco()
    ttrack_reco = make_ttrack_reco(global_reco, make_ttracks)

    return make_charged_protoparticles(
        tracks=ttrack_reco["good_ttracks"]["v1"],
        rich_pids=ttrack_reco["rich_pids"],
        calo_pids=ttrack_reco["calo_pids"],
        muon_pids=ttrack_reco["muon_pids"],
        track_types=["Ttrack"],
        use_track_ancestor_calo=True,
    )


def make_ttrack_MVAfiltered_protoparticles():
    return make_ttrack_protoparticles(
        name="TTrackSel_MVAfiltered_ProtoParticleMaker", make_ttracks=make_mva_ttracks
    )


def TrackStateProvider_with_TrackRungeKuttaExtrapolator():
    RK_extrapolator = TrackRungeKuttaExtrapolator(name="ttracks_RKextrapolator")
    RK_interpolator = TrackInterpolator(
        name="ttracks_RKinterpolator", Extrapolator=RK_extrapolator
    )
    return TrackStateProvider(
        name="ttracks_TrackStateProvider",
        Extrapolator=RK_extrapolator,
        Interpolator=RK_interpolator,
        public=True,
    )


def PVF_with_single_extrapolation():
    return ParticleVertexFitter(
        name="RKParticleVertexFitter",
        StateProvider=TrackStateProvider_with_TrackRungeKuttaExtrapolator(),
        extrapolateTtracks=True,
    )
