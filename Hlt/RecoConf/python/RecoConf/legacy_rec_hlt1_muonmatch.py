###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable
from PyConf.Algorithms import (
    MuonMatchVeloUTSoA,
    PrFilterIPSoA,
    PrVeloUT,
    SciFiTrackForwarding,
    fromPrUpstreamTracksV1Tracks,
    fromPrVeloTracksV1Tracks,
)

from RecoConf.muonid import make_muon_hits

from .legacy_rec_hlt1_tracking import (
    all_legacy_rec_hlt1_forward_track_types,
    all_upstream_track_types,
    all_velo_track_types,
    get_global_clusters_on_track_tool_only_velo,
    make_pvs,
    make_VeloClusterTrackingSIMD_hits,
)


@configurable
def make_IPselected_tracks(
    tracks,
    pvs,
    ip_cut,
    make_velo_hits=make_VeloClusterTrackingSIMD_hits,
    clusters_to_track_tool=get_global_clusters_on_track_tool_only_velo,
):
    output_tracks = PrFilterIPSoA(
        IPcut=ip_cut, Input=tracks["Pr"], InputVertices=pvs
    ).Output

    output_tracks_v1 = fromPrVeloTracksV1Tracks(
        InputTracksLocation=output_tracks, TrackAddClusterTool=clusters_to_track_tool()
    ).OutputTracksLocation

    return {"Pr": output_tracks, "v1": output_tracks_v1}


@configurable
def make_muon_match_tracks(velo_tracks, upstream_tracks, make_muon_hits=make_muon_hits):
    muon_match_tracks = MuonMatchVeloUTSoA(
        InputTracks=upstream_tracks["Pr"], InputMuonHits=make_muon_hits()
    ).OutputTracks

    muon_match_tracks_v1 = fromPrUpstreamTracksV1Tracks(
        InputTracksLocation=muon_match_tracks, VeloTracksLocation=velo_tracks["v1"]
    ).OutputTracksLocation

    return {"Pr": muon_match_tracks, "v1": muon_match_tracks_v1}


def make_tracks_with_muonmatch():
    velo_tracks = all_velo_track_types()
    velo_ut_tracks = all_upstream_track_types(velo_tracks)
    muon_match_tracks = make_muon_match_tracks(velo_tracks, velo_ut_tracks)
    forward_tracks = all_legacy_rec_hlt1_forward_track_types(muon_match_tracks)
    return {
        "Velo": velo_tracks,
        "Upstream": velo_ut_tracks,
        "MuonMatch": muon_match_tracks,
        "Forward": forward_tracks,
    }


def make_tracks_with_muonmatch_ipcut(velo_track_min_ip, tracking_min_pt):
    velo_tracks = all_velo_track_types()
    pvs = make_pvs()
    ipselected_tracks = make_IPselected_tracks(velo_tracks, pvs, velo_track_min_ip)

    with PrVeloUT.bind(MinPT=tracking_min_pt, MinPTFinal=tracking_min_pt):
        velo_ut_tracks = all_upstream_track_types(ipselected_tracks)
    muon_match_tracks = make_muon_match_tracks(ipselected_tracks, velo_ut_tracks)

    with SciFiTrackForwarding.bind(MinPt=tracking_min_pt):
        forward_tracks = all_legacy_rec_hlt1_forward_track_types(muon_match_tracks)

    return {
        "Velo": velo_tracks,
        "Upstream": velo_ut_tracks,
        "MuonMatch": muon_match_tracks,
        "Forward": forward_tracks,
    }
