###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from collections import namedtuple

from PyConf.application import configure, configure_input
from PyConf.control_flow import CompositeNode, NodeLogic


class Reconstruction(namedtuple("Reconstruction", ["node"])):  # noqa
    """Immutable object fully qualifiying the output of a reconstruction data flow with prefilters.

    Attributes:
        node (CompositeNode): the control flow of the reconstruction.

    """

    __slots__ = ()  # do not add __dict__ (preserve immutability)

    def __new__(cls, name, data_producers, filters=None):
        """Initialize a Reconstruction from name, data_producers and filters.

        Creates two control flow `CompositeNode` with the given `data`
        combined with `NONLAZY_OR` to execute  and a `CompositeNode`.

        Args:
            name (str): name of the reconstruction
            data_producers (list): iterable list of algorithms to produce data
            filters (list): list of filters to apply before running reconstruction
        """
        data_producers_node = CompositeNode(
            name + "_data",
            data_producers,
            combine_logic=NodeLogic.NONLAZY_OR,
            force_order=True,
        )
        if filters is None:
            filters = []
        control_flow = filters + [data_producers_node]
        cf_node = CompositeNode(
            name + "_decision",
            control_flow,
            combine_logic=NodeLogic.LAZY_AND,
            force_order=True,
        )

        return super(Reconstruction, cls).__new__(cls, cf_node)

    @property
    def name(self):
        """Reconstruction name"""
        return self.node.name


def run_reconstruction(options, make_reconstruction, public_tools=[]):
    """Configure the reconstruction data flow with a simple control flow.

    Convenience function that configures all services and creates a data flow.

    Args:
        options (ApplicationOptions): holder of application options
        make_reconstruction: function returning a single CompositeNode object
        public_tools (list): list of public `Tool` instances to configure

    """

    config = configure_input(options)
    reconstruction = make_reconstruction()
    config.update(configure(options, reconstruction.node, public_tools=public_tools))
    # TODO pass config to gaudi explicitly when that is supported
    return config
