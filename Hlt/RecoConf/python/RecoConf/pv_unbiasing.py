###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Helper functions related to unbiasing PVs are bundled together here.
"""

from PyConf.Algorithms import ParticleUnbiasedPVAdder, RecV1ToPVConverter
from PyConf.reading import get_extended_pvs


# root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp3/tests/logs/spruce_bsjpsiphi_passthrough_with_pv_pointer..dst
def unbias_decay_tree(*, input_particles, input_extended_pvs=None):
    """Removes tracks from the decay tree from the provided set of
    'extended' PVs. When these are not provided, an attempt is made
    to get them via the stanard reading method. Note that, this is only
    available in case the PVTracks have been stored on the DST.

    Returns both the vertices, and the changed Particles.
    """

    if input_extended_pvs is None:
        input_extended_pvs = get_extended_pvs()

    return ParticleUnbiasedPVAdder(
        InputParticles=input_particles, PrimaryVertices=input_extended_pvs
    )


def unbiased_decay_tree_from_v1_pvs(*, input_particles, v1_pvs):
    """Removes tracks from the decay tree from the provided set of
    v1 PVs *with tracks*. This is only available in case the
    PVTracks have been stored on the DST.
    """

    input_extended_pvs = RecV1ToPVConverter(InputVertices=v1_pvs).OutputVertices

    return unbias_decay_tree(
        input_particles=input_particles, input_extended_pvs=input_extended_pvs
    )
