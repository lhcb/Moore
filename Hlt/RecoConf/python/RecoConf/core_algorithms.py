###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Core algorithms for HLT processing"""

from Functors import FILTER
from PyConf.Algorithms import (
    PrFilter__PrFittedForwardTracksWithMuonID,
    PrFilter__PrFittedForwardTracksWithPVs,
    PrFilter__PrVeloTracks,
    UniqueIDGeneratorAlg,
)


def make_unique_id_generator():
    """Create the instance in charge of generating unique IDs.

    Ensure that the location of the ID generator remains always constant (it would
    be better if we keep it as the default location). Otherwise algorithms might
    end up using and comparing IDs from different instances.
    """
    return UniqueIDGeneratorAlg().Output


def Filter(objects, functor):
    """Return filters with the same output types as the inputs"""
    output = {}
    for key, alg_t in [
        ("PrFittedForwardWithPVs", PrFilter__PrFittedForwardTracksWithPVs),
        ("PrFittedForwardWithMuonID", PrFilter__PrFittedForwardTracksWithMuonID),
        ("PrVeloTracks", PrFilter__PrVeloTracks),
        ("PrVeloBackwardTracks", PrFilter__PrVeloTracks),
    ]:
        if key not in objects:
            continue
        filtered = alg_t(Input=objects[key], Cut=FILTER(functor)).Output
        output[key] = filtered
    return output
