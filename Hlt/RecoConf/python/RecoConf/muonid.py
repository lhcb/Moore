###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable
from PyConf.Algorithms import (
    MuonIDHlt1Alg,
    MuonRawInUpgradeToHits,
    MuonRawToHits,
)
from PyConf.application import default_raw_banks


@configurable
def make_muon_hits(geometry_version=3):
    if geometry_version == 2:
        return MuonRawToHits(RawBanks=default_raw_banks("Muon")).HitContainer
    elif geometry_version == 3:
        return MuonRawInUpgradeToHits(
            RawBanks=default_raw_banks("Muon"),
            ErrorRawBanks=default_raw_banks("MuonError"),
        ).HitContainer
    raise ValueError(f"Unsupported muon decoding version {geometry_version}")


@configurable
def make_muon_id(tracks, make_muon_hits=make_muon_hits):
    return MuonIDHlt1Alg(
        runMVA=False, InputTracks=tracks["Pr"], InputMuonHits=make_muon_hits()
    ).OutputMuonPID
