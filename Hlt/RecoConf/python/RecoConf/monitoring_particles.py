###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from GaudiKernel.SystemOfUnits import micrometer as um
from PyConf.Algorithms import FunctionalDiElectronMaker
from PyConf.Tools import ParticleVertexFitter

from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.reconstruction_objects import make_pvs

from .standard_particles import (
    make_has_rich_down_kaons,
    make_has_rich_down_pions,
    make_has_rich_down_protons,
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
    make_has_rich_long_protons,
    make_ismuon_long_muon,
    make_long_electrons_no_brem,
    make_photons,
    make_up_pions,
)


def _MIPCHI2_MIN(cut, pvs=make_pvs):
    return F.MINIPCHI2CUT(IPChi2Cut=cut, Vertices=pvs())


def _DZ_CHILD(i):
    return F.CHILD(i, F.END_VZ) - F.END_VZ


def _DRHO2_CHILD(i):
    return (F.CHILD(i, F.END_VX) - F.END_VX) * (F.CHILD(i, F.END_VX) - F.END_VX) + (
        F.CHILD(i, F.END_VY) - F.END_VY
    ) * (F.CHILD(i, F.END_VY) - F.END_VY)


def _filter_photons():
    return ParticleFilter(
        make_photons(),
        F.FILTER(
            F.require_all(
                F.PT > 200 * MeV,
                F.CALO_NEUTRAL_1TO9_ENERGY_RATIO > 0.9,
                F.IS_PHOTON > 0.8,
                F.IS_NOT_H > 0.7,
            )
        ),
    )


def _filter_long_pions_for_strange():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(F.require_all(F.PT > 80 * MeV, _MIPCHI2_MIN(9), F.PID_K < 5)),
    )


def _filter_down_pions_for_strange():
    return ParticleFilter(
        make_has_rich_down_pions(),
        F.FILTER(F.require_all(F.PT > 100 * MeV, F.PID_K < 10)),
    )


def _filter_upstream_pions_for_strange():
    return ParticleFilter(
        make_up_pions(), F.FILTER(F.require_all(F.PT > 80 * MeV, _MIPCHI2_MIN(9)))
    )


def _filter_long_pions_for_charm():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 180 * MeV,
                F.PID_K < 3,
                _MIPCHI2_MIN(6),
            ),
        ),
    )


def _filter_long_kaons_for_strange():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(F.require_all(F.PT > 150 * MeV, _MIPCHI2_MIN(6), F.PID_K > 2)),
    )


def _filter_long_kaons_for_charm():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > 300 * MeV,
                F.P > 2.3 * GeV,
                F.PID_K > 0,
                _MIPCHI2_MIN(4),
            ),
        ),
    )


def diphoton():
    return ParticleCombiner(
        name="Monitoring_DiPhoton",
        Inputs=[_filter_photons(), _filter_photons()],
        ParticleCombiner="ParticleAdder",
        DecayDescriptor="eta -> gamma gamma",
        CombinationCut=F.require_all(
            F.math.in_range(0 * MeV, F.MASS, 1200 * MeV), F.PT > 1 * GeV
        ),
        CompositeCut=F.ALL,
    )


def prompt_ks_ll():
    return ParticleCombiner(
        [_filter_long_pions_for_strange(), _filter_long_pions_for_strange()],
        DecayDescriptor="KS0 -> pi+ pi-",
        name="Monitoring_KS0_LL",
        CombinationCut=F.require_all(
            F.math.in_range(380 * MeV, F.MASS, 610 * MeV),
            F.MAXDOCACUT(300 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(410 * MeV, F.MASS, 590 * MeV),
            F.CHI2DOF < 24,
            F.math.in_range(-200 * mm, F.END_VZ, 640 * mm),
            F.OWNPVVDZ > 3 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 6,
            F.OWNPVDIRA > 0.999,
            F.OWNPVIPCHI2 < 9,
        ),
    )


def prompt_ks_dd():
    return ParticleCombiner(
        [_filter_down_pions_for_strange(), _filter_down_pions_for_strange()],
        DecayDescriptor="KS0 -> pi+ pi-",
        name="Monitoring_KS0_DD",
        CombinationCut=F.require_all(
            F.math.in_range(380 * MeV, F.MASS, 610 * MeV),
            F.PT > 300 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(410 * MeV, F.MASS, 590 * MeV),
            F.PT > 350 * MeV,
            F.CHI2DOF < 24,
            F.math.in_range(250 * mm, F.END_VZ, 2485 * mm),
            F.OWNPVDIRA > 0.999,
            F.OWNPVIPCHI2 < 12,
        ),
    )


def prompt_ks_uu():
    return ParticleCombiner(
        [_filter_upstream_pions_for_strange(), _filter_upstream_pions_for_strange()],
        DecayDescriptor="KS0 -> pi+ pi-",
        name="Monitoring_KS0_UU",
        CombinationCut=F.require_all(
            F.math.in_range(300 * MeV, F.MASS, 700 * MeV),
            F.PT > 50 * MeV,
            F.P > 0.4 * GeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(300 * MeV, F.MASS, 700 * MeV),
            F.PT > 100 * MeV,
            F.P > 0.5 * GeV,
            F.CHI2DOF < 12.0,
            F.BPVVDZ(make_pvs()) > 4 * mm,
            F.BPVVDRHO(make_pvs()) > 1.5 * mm,
            F.BPVFDCHI2(make_pvs()) > 80.0,
            F.BPVDIRA(make_pvs()) < 0.9995,
        ),
    )


def _lambda_ll():
    protons = ParticleFilter(
        make_has_rich_long_protons(),
        F.FILTER(
            F.require_all(
                F.PT > 350 * MeV, F.P > 2.3 * GeV, _MIPCHI2_MIN(6.0), F.PID_P > 3
            )
        ),
    )
    return ParticleCombiner(
        [protons, _filter_long_pions_for_strange()],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name="Monitoring_Lambda_LL",
        CombinationCut=F.require_all(
            F.MASS < 1160 * MeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.MASS < 1140 * MeV,
            F.CHI2DOF < 24,
            F.math.in_range(-200 * mm, F.END_VZ, 640 * mm),
            F.OWNPVVDZ > 4 * mm,
            F.OWNPVVDRHO > 0.6 * mm,
            F.OWNPVDLS > 6,
        ),
    )


def prompt_lambda_ll():
    return ParticleFilter(
        _lambda_ll(), F.FILTER(F.require_all(F.OWNPVDIRA > 0.999, F.OWNPVIPCHI2 < 9))
    )


def _lambda_ll_for_strange():
    return ParticleFilter(
        _lambda_ll(),
        F.FILTER(
            F.require_all(
                F.OWNPVVDZ > 6 * mm,
                F.OWNPVVDRHO > 1 * mm,
                F.OWNPVDLS > 8,
                F.math.in_range(1109 * MeV, F.MASS, 1123 * MeV),
            )
        ),
    )


def _lambda_dd():
    protons = ParticleFilter(
        make_has_rich_down_protons(),
        F.FILTER(F.require_all(F.PT > 500 * MeV, F.P > 2.3 * GeV)),
    )
    return ParticleCombiner(
        [protons, _filter_down_pions_for_strange()],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name="Monitoring_Lambda_DD",
        CombinationCut=F.require_all(
            F.MASS < 1160 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 1140 * MeV,
            F.CHI2DOF < 24,
            F.math.in_range(250 * mm, F.END_VZ, 2485 * mm),
        ),
    )


def prompt_lambda_dd():
    return ParticleFilter(
        _lambda_dd(), F.FILTER(F.require_all(F.OWNPVDIRA > 0.99, F.OWNPVIPCHI2 < 12))
    )


def _lambda_dd_for_strange():
    return ParticleFilter(
        _lambda_dd(),
        F.FILTER(F.require_all(F.math.in_range(1105 * MeV, F.MASS, 1128 * MeV))),
    )


def prompt_xi_lll():
    return ParticleCombiner(
        [_lambda_ll_for_strange(), _filter_long_pions_for_strange()],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        name="Monitoring_Xim_LLL",
        CombinationCut=F.require_all(
            F.MASS < 1400 * MeV,
            F.MAXDOCACUT(250 * um),
        ),
        CompositeCut=F.require_all(
            F.MASS < 1380 * MeV,
            F.CHI2DOF < 24,
            _DZ_CHILD(1) > 4 * mm,
            _DRHO2_CHILD(1) > 20 * 20 * um * um,
            F.OWNPVVDZ > 4 * mm,
            F.OWNPVDLS > 6,
            F.math.in_range(-200 * mm, F.END_VZ, 630 * mm),
            F.OWNPVDIRA > 0.999,
            F.OWNPVIPCHI2 < 9,
        ),
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
    )


def prompt_xi_ddl():
    return ParticleCombiner(
        [_lambda_dd_for_strange(), _filter_long_pions_for_strange()],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        name="Monitoring_Xim_DDL",
        CombinationCut=F.require_all(
            F.MASS < 1400 * MeV,
            F.MAXDOCACUT(750 * um),
        ),
        CompositeCut=F.require_all(
            F.MASS < 1380 * MeV,
            F.CHI2DOF < 12.0,
            F.OWNPVVDZ > 4 * mm,
            F.OWNPVDLS > 16,
            F.math.in_range(-200 * mm, F.END_VZ, 630 * mm),
            F.OWNPVDIRA > 0.999,
            F.OWNPVIPCHI2 < 12,
        ),
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
    )


def prompt_xi_ddd():
    return ParticleCombiner(
        [_lambda_dd_for_strange(), _filter_down_pions_for_strange()],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        name="Monitoring_Xim_DDD",
        CombinationCut=F.require_all(
            F.MASS < 1400 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 1380 * MeV,
            F.CHI2DOF < 24.0,
            _DZ_CHILD(1) > 4 * mm,
            F.math.in_range(250 * mm, F.END_VZ, 2485 * mm),
            F.OWNPVDIRA > 0.99,
        ),
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
    )


def prompt_omega_lll():
    return ParticleCombiner(
        [_lambda_ll_for_strange(), _filter_long_kaons_for_strange()],
        DecayDescriptor="[Omega- -> Lambda0 K-]cc",
        name="Monitoring_Omega_LLL",
        CombinationCut=F.require_all(
            F.MASS < 1760 * MeV,
            F.MAXDOCACUT(250 * um),
        ),
        CompositeCut=F.require_all(
            F.MASS < 1740 * MeV,
            F.CHI2DOF < 24,
            _DZ_CHILD(1) > 1 * mm,
            _DRHO2_CHILD(1) > 20 * 20 * um * um,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVDLS > 5,
            F.math.in_range(-200 * mm, F.END_VZ, 630 * mm),
            F.OWNPVDIRA > 0.999,
            F.OWNPVIPCHI2 < 9,
        ),
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
    )


def prompt_omega_ddd():
    kaon = ParticleFilter(
        make_has_rich_down_kaons(),
        F.FILTER(F.require_all(F.PT > 300 * MeV, F.P > 2.3 * GeV)),
    )
    return ParticleCombiner(
        [_lambda_dd_for_strange(), kaon],
        DecayDescriptor="[Omega- -> Lambda0 K-]cc",
        name="Monitoring_Omega_DDD",
        CombinationCut=F.require_all(
            F.MASS < 1780 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 1740 * MeV,
            F.CHI2DOF < 24.0,
            _DZ_CHILD(1) > 4 * mm,
            F.math.in_range(250 * mm, F.END_VZ, 2485 * mm),
            F.OWNPVDIRA > 0.99,
        ),
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
    )


def prompt_d0():
    return ParticleCombiner(
        [_filter_long_kaons_for_charm(), _filter_long_pions_for_charm()],
        DecayDescriptor="[D0 -> K- pi+]cc",
        name="Monitoring_D0ToKmPip",
        CombinationCut=F.require_all(
            F.math.in_range(1665 * MeV, F.MASS, 2065 * MeV),
            F.PT > 1.3 * GeV,
            F.CHILD(2, F.PT) > 300 * MeV,
            F.CHILD(1, _MIPCHI2_MIN(6)),
            F.MAXSDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1700 * MeV, F.MASS, 2030 * MeV),
            F.PT > 1.5 * GeV,
            F.CHI2DOF < 7,
            F.OWNPVVDZ > 0.8 * mm,
            F.OWNPVDLS > 4.5,
            F.OWNPVIPCHI2 < 9,
            F.OWNPVDIRA > 0.999,
        ),
    )


def prompt_dst():
    slow_pi = ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 120 * MeV, F.MINIPCHI2(make_pvs()) < 6, F.PID_K < 16)
        ),
    )
    return ParticleCombiner(
        [prompt_d0(), slow_pi],
        DecayDescriptor="[D*(2010)+ -> D0 pi+]cc",
        name="Monitoring_Dstar",
        CombinationCut=F.require_all(
            F.math.in_range(1845 * MeV, F.CHILD(1, F.MASS), 1885 * MeV),
            F.MASS - F.CHILD(1, F.MASS) < 195 * MeV,
            F.PT > 1.8 * GeV,
            F.MAXSDOCACUT(180 * um),
        ),
        CompositeCut=F.require_all(
            F.MASS - F.CHILD(1, F.MASS) < 180 * MeV, F.CHI2DOF < 6, F.PT > 2 * GeV
        ),
    )


def prompt_dp():
    return ParticleCombiner(
        [
            _filter_long_kaons_for_charm(),
            _filter_long_pions_for_charm(),
            _filter_long_pions_for_charm(),
        ],
        DecayDescriptor="[D+ -> K- pi+ pi+]cc",
        name="Monitoring_DpToKmPipPip",
        Combination12Cut=F.require_all(
            F.MASS < 1960 * MeV, F.MAXSDOCACUT(120 * um), F.CHILD(1, _MIPCHI2_MIN(6))
        ),
        CombinationCut=F.require_all(
            F.math.in_range(1640 * MeV, F.MASS, 2100 * MeV),
            F.PT > 1.3 * GeV,
            F.MAXSDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1670 * MeV, F.MASS, 2070 * MeV),
            F.PT > 1.5 * GeV,
            F.CHI2DOF < 6,
            F.OWNPVVDZ > 2 * mm,
            F.OWNPVDLS > 8,
            F.OWNPVDIRA > 0.999,
            F.OWNPVIPCHI2 < 9,
        ),
    )


def prompt_ds():
    return ParticleCombiner(
        [
            _filter_long_kaons_for_charm(),
            _filter_long_kaons_for_charm(),
            _filter_long_pions_for_charm(),
        ],
        DecayDescriptor="[D_s+ -> K+ K- pi+]cc",
        name="Monitoring_DspToPhiPip",
        Combination12Cut=F.require_all(F.MASS < 1050 * MeV, F.MAXSDOCACUT(120 * um)),
        CombinationCut=F.require_all(
            F.math.in_range(1665 * MeV, F.MASS, 2165 * MeV),
            F.PT > 1.4 * GeV,
            F.MAXSDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1700 * MeV, F.MASS, 2100 * MeV),
            F.PT > 1.6 * GeV,
            F.CHI2DOF < 6,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVDLS > 6,
            F.OWNPVDIRA > 0.999,
            F.OWNPVIPCHI2 < 9,
        ),
    )


def prompt_lc():
    protons = ParticleFilter(
        make_has_rich_long_protons(),
        F.FILTER(
            F.require_all(
                F.PT > 500 * MeV, _MIPCHI2_MIN(6), F.PID_P > 10, F.PID_P - F.PID_K > 5
            )
        ),
    )
    return ParticleCombiner(
        [protons, _filter_long_kaons_for_charm(), _filter_long_pions_for_charm()],
        DecayDescriptor="[Lambda_c+ -> p+ K- pi+]cc",
        name="Monitoring_LcpToPpKmPip",
        Combination12Cut=F.MASS < 2420 * MeV,
        CombinationCut=F.require_all(
            F.math.in_range(2200 * MeV, F.MASS, 2560 * MeV),
            F.PT > 1.7 * GeV,
            F.MAXSDOCACUT(280 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2130 * MeV, F.MASS, 2630 * MeV),
            F.PT > 1.9 * GeV,
            F.CHI2DOF < 6,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVDLS > 5,
            F.OWNPVDIRA > 0.999,
            F.OWNPVIPCHI2 < 9,
        ),
    )


def prompt_jpsi_to_mumu():
    mu = ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.PT > 700 * MeV,
                F.MINIP(make_pvs()) < 80 * um,
                F.MINIPCHI2(make_pvs()) < 6,
                F.PID_MU > -3,
            )
        ),
    )
    return ParticleCombiner(
        [mu, mu],
        name="Monitoring_JpsiToMumMup",
        DecayDescriptor="J/psi(1S) -> mu+ mu-",
        CombinationCut=F.require_all(
            F.math.in_range(2.7 * GeV, F.MASS, 3.5 * GeV),
            F.PT > 1.3 * GeV,
            F.MAXSDOCACUT(80 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2.8 * GeV, F.MASS, 3.4 * GeV),
            F.PT > 1.6 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVIP < 100 * um,
        ),
    )


def prompt_jpsi_to_ee():
    pvs = make_pvs()
    electrons = ParticleFilter(
        make_long_electrons_no_brem(),
        F.FILTER(
            F.require_all(
                F.PT > 1 * GeV,
                F.MINIP(pvs) < 80 * um,
                F.MINIPCHI2(pvs) < 6,
                F.PROBNN_E > 0.7,
            )
        ),
    )

    dielectrons = FunctionalDiElectronMaker(
        InputParticles=electrons,
        PrimaryVertices=pvs,
        MinDiElecPT=2 * GeV,
        MinDiElecMass=2.2 * GeV,
        MaxDiElecMass=3.6 * GeV,
        DiElecID="J/psi(1S)",
        DistMaxpair=1 * mm,
    ).Particles
    return ParticleFilter(
        dielectrons,
        F.FILTER(
            F.require_all(
                F.math.in_range(2.3 * GeV, F.MASS, 3.5 * GeV),
                F.MAXSDOCACUT(80 * um),
                F.CHI2DOF < 6.0,
                F.OWNPVIPCHI2 < 9.0,
                F.OWNPVIP < 100 * um,
            )
        ),
    )
