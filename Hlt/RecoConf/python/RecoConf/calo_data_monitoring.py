###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from DDDB.CheckDD4Hep import UseDD4Hep
from GaudiKernel.SystemOfUnits import GeV
from PyConf import configurable
from PyConf.Algorithms import (
    CaloChargedPIDsMonitor,
    CaloClusterECALMonitor,
    CaloDigitECALMonitor,
    CaloDigitHCALMonitor,
    CaloECALPedestal,
    CaloECALTimeAlignment,
    CaloFutureRawToDigits,
    CaloHCALPedestal,
    CaloHCALTimeAlignment,
    CaloLEDECALMonitor,
    CaloLEDHCALMonitor,
    CaloTAEData,
)
from PyConf.application import default_raw_banks, make_odin


@configurable
def monitor_calo_clusters(
    clusters, split_clusters=None, name="ClusterMonitorEcalClusters"
):
    nodes = [
        CaloClusterECALMonitor(
            name=name,
            Input=clusters,
            HistoMultiplicity=(0.0, 400.0, 100),
            HistoEnergy=(0.0 * GeV, 50.0 * GeV, 100),
            HistoEt=(0.0 * GeV, 5.0 * GeV, 100),
            HistoSize=(0, 30, 25),
        ),
    ]
    if split_clusters:
        nodes.append(
            CaloClusterECALMonitor(
                name="ClusterMonitorEcalSplitClusters",
                Input=split_clusters,
                HistoMultiplicity=(0.0, 50.0, 100),
                HistoEnergy=(0.0 * GeV, 100.0 * GeV, 100),
                HistoEt=(0.0 * GeV, 10.0 * GeV, 100),
            )
        )
    return nodes


@configurable
def monitor_calo_digits(
    calo,
    adcFilterEcal=-100,
    adcFilterHcal=-100,
    eFilter=-9999.0,
    etFilter=-9999.0,
    histoAdcMin=-100.5,
    histoAdcMax=4095.5,
    histoAdcBin=4196,
    HistoMultiplicityMaxEcal=8000,
    HistoMultiplicityBinEcal=8000,
    HistoMultiplicityMaxHcal=2000,
    HistoMultiplicityBinHcal=2000,
    Calib_BXIDsA=[],
    Calib_BXIDsB=[],
):
    ecal_digit_moni = CaloDigitECALMonitor(
        name="DigitMonitorEcal",
        Input=calo["digitsEcal"],
        ODINLocation=make_odin(),
        HistoMultiplicity=(0.0, HistoMultiplicityMaxEcal, HistoMultiplicityBinEcal),
        HistoEt=(0.0 * GeV, 5.0 * GeV, 100),
        HistoAdc=(histoAdcMin, histoAdcMax, histoAdcBin),
        HistoX=(-3900.8, 3900.8, 64),
        HistoY=(-3169.4, 3169.4, 52),
        Calib_BXIDsA=Calib_BXIDsA,
        Calib_BXIDsB=Calib_BXIDsB,
        ADCFilter=adcFilterEcal,
        EnergyFilter=eFilter,
        EtFilter=etFilter,
    )
    hcal_digit_moni = CaloDigitHCALMonitor(
        name="DigitMonitorHcal",
        Input=calo["digitsHcal"],
        ODINLocation=make_odin(),
        HistoMultiplicity=(0.0, HistoMultiplicityMaxHcal, HistoMultiplicityBinHcal),
        HistoEt=(0.0 * GeV, 5.0 * GeV, 100),
        HistoAdc=(histoAdcMin, histoAdcMax, histoAdcBin),
        HistoX=(-4201.6, 4201.6, 32),
        HistoY=(-3413.8, 3413.8, 26),
        Calib_BXIDsA=Calib_BXIDsA,
        Calib_BXIDsB=Calib_BXIDsB,
        ADCFilter=adcFilterHcal,
        EnergyFilter=eFilter,
        EtFilter=etFilter,
    )
    return [ecal_digit_moni, hcal_digit_moni]


@configurable
def monitor_calo_led(calo):
    ecalpath = (
        "/world/DownstreamRegion/Ecal:DetElement-Info-IOV"
        if UseDD4Hep
        else "/dd/Structure/LHCb/DownstreamRegion/Ecal"
    )
    hcalpath = (
        "/world/DownstreamRegion/Hcal:DetElement-Info-IOV"
        if UseDD4Hep
        else "/dd/Structure/LHCb/DownstreamRegion/Hcal"
    )

    rawEventCalo = default_raw_banks("Calo")
    rawEventCaloError = default_raw_banks("CaloError")

    adc_alg_ecal = CaloFutureRawToDigits(
        name="FutureEcalZSup_withPinDiodes",
        RawBanks=rawEventCalo,
        ErrorRawBanks=rawEventCaloError,
        DetectorLocation=ecalpath,
        DecodePinData=True,
    )

    ecal_led_moni = CaloLEDECALMonitor(
        name="LEDMonitorEcal",
        InputODIN=make_odin(),
        Input=adc_alg_ecal.OutputDigitData,
        CheckNCell=5,  # Number of Cells to be looked for a LED to be declared as fired
        PedestalRate=64,  # Accept rate (%)
        ADCThreshold=30,
    )

    adc_alg_hcal = CaloFutureRawToDigits(
        name="FutureHcalZSup_withPinDiodes",
        RawBanks=rawEventCalo,
        ErrorRawBanks=rawEventCaloError,
        DetectorLocation=hcalpath,
        DecodePinData=True,
    )

    hcal_led_moni = CaloLEDHCALMonitor(
        name="LEDMonitorHcal",
        InputODIN=make_odin(),
        Input=adc_alg_hcal.OutputDigitData,
        CheckNCell=5,  # Number of Cells to be looked for a LED to be declared as fired
        PedestalRate=64,  # Accept rate (%)
        ADCThreshold=30,  # ADC threshold for a PIN to be declared as fired
    )

    return [ecal_led_moni, hcal_led_moni]


@configurable
def monitor_calo_time_alignment(
    calo,
    adcFilterEcal=-100,
    adcFilterHcal=-100,
    histoAdcMax=4000,
    taHistoYBin=100,
    TAE_BXIDs=[1000, 2000],
):
    ecal_ta_moni = CaloECALTimeAlignment(
        Input=calo["digitsEcal"],
        ODINLocation=make_odin(),
        ADCFilter=adcFilterEcal,
        TAE_BXIDs=TAE_BXIDs,
    )
    hcal_ta_moni = CaloHCALTimeAlignment(
        Input=calo["digitsHcal"],
        ODINLocation=make_odin(),
        ADCFilter=adcFilterHcal,
        TAE_BXIDs=TAE_BXIDs,
    )
    tae_data = CaloTAEData(
        name="CaloTAEData", Input=calo["digitsEcal"], ODINLocation=make_odin()
    )
    return [ecal_ta_moni, hcal_ta_moni, tae_data]


@configurable
def monitor_calo_pedestal(
    calo,
    adcFilterEcal=-100,
    adcFilterHcal=-100,
    histoAdcMin=-100.5,
    histoAdcMax=100.5,
    histoAdcBin=201,
):
    ecal_pedestal_moni = CaloECALPedestal(
        Input=calo["digitsEcal"],
        ODINLocation=make_odin(),
        ADCFilter=adcFilterEcal,
        HistoAdc=(histoAdcMin, histoAdcMax, histoAdcBin),
    )
    hcal_pedestal_moni = CaloHCALPedestal(
        Input=calo["digitsHcal"],
        ODINLocation=make_odin(),
        ADCFilter=adcFilterHcal,
        HistoAdc=(histoAdcMin, histoAdcMax, histoAdcBin),
    )
    return [ecal_pedestal_moni, hcal_pedestal_moni]


@configurable
def monitor_calo_charged(
    calo, tracks, name="CaloChargedPIDsMonitor", accepted_track_types=["Long"]
):
    cpids = calo["v2_chargedpids"]
    brems = calo["v2_breminfos"]
    track_types = list(
        set(cpids.keys()) & set(brems.keys()) & set(accepted_track_types)
    )
    nodes = ()
    for tt in track_types:
        nodes += (
            CaloChargedPIDsMonitor(
                name=name + "_" + tt,
                Tracks=tracks[tt],
                ChargedPID=cpids[tt],
                BremInfo=brems[tt],
                Clusters=calo["ecalClusters"],
            ),
        )
    return nodes


def monitor_calo(calo, tracks=None):
    monitors = (
        monitor_calo_clusters(calo["ecalClusters"], calo["ecalSplitClusters"])
        + monitor_calo_digits(calo)
        + monitor_calo_time_alignment(calo)
        + monitor_calo_pedestal(calo)
    )
    if tracks:
        monitors += monitor_calo_charged(calo, tracks)
    return monitors
