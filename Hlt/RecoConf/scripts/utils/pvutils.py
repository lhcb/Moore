###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# 1;95;0c Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# The utils for processing output of the PrimaryVertexChecker output
#
# author: Agnieszka Dziurda (agnieszka.dziurda@cern.ch)
# date:   02/2020
#

from array import array

from pvconfig import (
    basic_cut,
    get_default_tree_name,
    get_text_cor,
    get_x_axis,
    get_y_axis,
    set_style,
    transfer_variable,
)
from ROOT import (
    TCanvas,
    TFile,
    TGraphAsymmErrors,
    TLatex,
    TPaveText,
    gDirectory,
    gPad,
    gStyle,
    kGray,
)


def get_files(tf, label, files):
    i = 0
    for f in files:
        tf[label[i]] = TFile(f)
        i += 1
    return tf


def get_trees(tf, tr, label, trees, is_checker):
    i = 0
    for lab in label:
        if len(tf) == len(trees):
            tr[lab] = tf[lab].Get(trees[i])
        else:
            tr[lab] = tf[lab].Get(get_default_tree_name(is_checker))
        i += 1
    return tr


def get_eff(eff, hist, trees, dependence, style, ranges, categories, label, offset):
    for cat in categories:
        eff[cat] = {}
        hist[cat] = {}
        i = offset
        for lab in label:
            var_den = "{dependence}>>hist{code}_{dep}_{cat}_{lab}_denom{i}({bins},{mins},{maxs})".format(
                dependence=dependence,
                code="den",
                dep=dependence,
                cat=cat,
                lab=lab,
                bins=ranges[dependence]["bins"],
                mins=ranges[dependence]["min"],
                maxs=ranges[dependence]["max"],
                i=i,
            )
            var_nom = "{dependence}>>hist{code}_{dep}_{cat}_{lab}_num{i}({bins},{mins},{maxs})".format(
                dependence=dependence,
                code="nom",
                dep=dependence,
                cat=cat,
                lab=lab,
                bins=ranges[dependence]["bins"],
                mins=ranges[dependence]["min"],
                maxs=ranges[dependence]["max"],
                i=i,
            )

            cut_den = "nrectrmc>=4 && dz < 2.0 {cuts}".format(
                cuts=categories[cat]["cut"]
            )
            cut_nom = cut_den + " && reco == 1"

            trees[lab].Draw(var_nom, cut_nom)
            trees[lab].Draw(var_den, cut_den)

            h_nom = gDirectory.Get(
                "hist{code}_{dep}_{cat}_{lab}_num{i}".format(
                    code="nom", dep=dependence, cat=cat, lab=lab, i=i
                )
            )
            h_den = gDirectory.Get(
                "hist{code}_{dep}_{cat}_{lab}_denom{i}".format(
                    code="den", dep=dependence, cat=cat, lab=lab, i=i
                )
            )

            g_eff = TGraphAsymmErrors()
            g_eff.Divide(h_nom, h_den, "cl=0.683 b(1,1) mode")

            set_style(
                h_nom,
                style["color"][i] - 7,
                style["marker"][i] + 4,
                get_x_axis(dependence),
                "Efficiency",
                "",
            )
            set_style(
                h_den,
                kGray,
                style["marker"][i],
                get_x_axis(dependence),
                "Efficiency",
                "",
            )
            hist[cat][lab] = {}
            hist[cat][lab]["nom"] = h_nom
            hist[cat][lab]["den"] = h_den

            set_style(
                g_eff,
                style["color"][i],
                style["marker"][i],
                get_x_axis(dependence),
                "Efficiency",
                "",
            )
            eff[cat][lab] = {}
            eff[cat][lab]["eff"] = g_eff
            i += 1

    return eff, hist


def find_max(gr, gr_type, label):
    m = -999999.0
    for lab in label:
        if gr[lab][gr_type].GetYaxis().GetXmax() > m:
            m = gr[lab][gr_type].GetYaxis().GetXmax()

    return m


def find_min(gr, gr_type, label):
    m = 999999.0
    for lab in label:
        if gr[lab][gr_type].GetYaxis().GetXmin() < m:
            m = gr[lab][gr_type].GetYaxis().GetXmin()
    return m


def plot(
    gr,
    gr_type,
    prefix,
    dependence,
    categories,
    label,
    legend=None,
    hist=None,
    dist=False,
    lhcbtextpos=(0.9, 0.6),
):
    for cat in categories:
        can = TCanvas(
            "canvas_{depen}_{prefix}_{gr_type}_{cat}".format(
                depen=dependence, prefix=prefix, gr_type=gr_type, cat=cat
            ),
            "cR",
            1200,
            800,
        )
        can.SetBottomMargin(0.15)
        can.SetLeftMargin(0.12)
        can.SetTopMargin(0.15)
        if dist:
            can.SetTopMargin(0.20)
        can.SetRightMargin(0.05)
        can.cd()

        maximum = find_max(gr[cat], gr_type, label)
        minimum = find_min(gr[cat], gr_type, label)
        if abs(maximum) > abs(minimum) and minimum < 0:
            minimum = -maximum

        gr[cat][label[0]][gr_type].GetYaxis().SetRangeUser(minimum * 1.1, maximum * 1.1)
        if gr_type == "eff":
            gr[cat][label[0]][gr_type].GetYaxis().SetRangeUser(0.0, 1.1)
        if gr_type == "sigma":
            gr[cat][label[0]][gr_type].GetYaxis().SetRangeUser(0.0, maximum * 1.1)
        gr[cat][label[0]][gr_type].Draw("AP")

        for lab in label:
            gr[cat][lab][gr_type].Draw("SAME P")

        if dist:
            histmax_den = 1.1 * hist[cat][label[0]]["den"].GetMaximum()
            scale = gPad.GetUymax() / histmax_den
            hist[cat][label[0]]["den"].Scale(scale * 0.75)
            hist[cat][label[0]]["den"].Draw("hist SAME")

            for lab in label:
                hist[cat][lab]["nom"].Scale(scale * 0.75)
                hist[cat][lab]["nom"].Draw("ep SAME")

                gr[cat][lab][gr_type].Draw("SAME P")

        if legend:
            legend.Draw("SAME")

        lhcbtext = TLatex()
        lhcbtext.SetTextFont(132)
        lhcbtext.SetTextColor(1)
        lhcbtext.SetTextSize(0.07)
        lhcbtext.SetTextAlign(132)

        lhcbtext.DrawTextNDC(lhcbtextpos[0], lhcbtextpos[1], "LHCb simulation")

        saveName = "{prefix}_{dependence}_{cat}.pdf".format(
            prefix=prefix, dependence=dependence, cat=cat
        )
        can.SaveAs(saveName)


def get_global(
    hist,
    trees,
    dependence,
    x_axis,
    y_axis,
    style,
    ranges,
    categories,
    label,
    offset,
    simple=False,
):
    if dependence.find("pull") > -1:
        dep = transfer_variable(dependence)
    else:
        dep = dependence

    for cat in categories:
        hist[cat] = {}
        i = offset
        for lab in label:
            var = (
                "{dependence}>>hist_{dep}_{cat}_{lab}_{i}({bins},{mins},{maxs})".format(
                    dependence=dep,
                    dep=dependence,
                    cat=cat,
                    lab=lab,
                    bins=ranges[dependence]["bins"],
                    mins=ranges[dependence]["min"],
                    maxs=ranges[dependence]["max"],
                    i=i,
                )
            )

            cut = "nrectrmc>=4 && dz < 2.0 && reco == 1 {cuts}".format(
                cuts=categories[cat]["cut"]
            )

            trees[lab].Draw(var, cut)
            h = gDirectory.Get(
                "hist_{dep}_{cat}_{lab}_{i}".format(
                    dep=dependence, cat=cat, lab=lab, i=i
                )
            )
            if i == 0 and not simple:
                col = style["color"][i] - 10
            else:
                col = style["color"][i]
            set_style(h, col, style["marker"][i], x_axis, y_axis, "")

            hist[cat][lab] = h
            i += 1

    return hist


def set_text(text, color, x, y, lab, mean, mean_err, rms, rms_err, scale, units):
    s = 1.0
    if scale:
        s = 1000.0

    ur = ""
    um = "#times 10^{-3}"
    if units:
        ur = "[#mu m]"
        um = "[#mu m]"

    text.SetNDC()
    text.SetTextFont(132)
    text.SetTextColor(color)
    text.DrawLatex(x, y * 1.0, lab)

    text.DrawLatex(
        x,
        y * 0.95,
        "#mu = ({0:0.2f} #pm {1:0.2f}) {unit}".format(
            mean * 1000.0, mean_err * 1000.0, unit=um
        ),
    )
    text.DrawLatex(
        x,
        y * 0.90,
        "#sigma = ({0:0.2f} #pm {1:0.2f}) {unit}".format(rms * s, rms_err * s, unit=ur),
    )
    return text


def plot_comparison(
    hist,
    prefix,
    dependence,
    categories,
    label,
    style,
    norm,
    offset,
    simple=False,
    legend=None,
):
    for cat in categories:
        can = TCanvas(
            "canvas_{depen}_{cat}".format(depen=dependence, cat=cat), "cR", 1200, 800
        )
        can.SetBottomMargin(0.15)
        can.SetLeftMargin(0.15)
        can.SetTopMargin(0.20)
        can.SetRightMargin(0.05)

        can.cd()
        cor = get_text_cor()

        hist[cat][label[0]].GetYaxis().SetRangeUser(
            0.0, hist[cat][label[0]].GetMaximum() * 1.1
        )
        scale = True
        units = True
        if "pull" in dependence:
            scale = False
            units = False
        if norm:
            if not simple:
                hist[cat][label[0]].DrawNormalized("hist")
            else:
                hist[cat][label[0]].DrawNormalized("PE")
            i = offset
            for lab in label:
                hist[cat][lab].DrawNormalized("SAME PE")
                if not simple:
                    text = TLatex()
                    text = set_text(
                        text,
                        style["color"][i],
                        cor["x"][i],
                        cor["y"][i],
                        lab,
                        hist[cat][lab].GetMean(),
                        hist[cat][lab].GetMeanError(),
                        hist[cat][lab].GetRMS(),
                        hist[cat][lab].GetRMSError(),
                        scale,
                        units,
                    )
                i += 1

        else:
            hist[cat][label[0]].Draw("hist")
            for lab in label:
                hist[cat][lab].Draw("SAME PE")
        if legend:
            legend.Draw("SAME")

        pavetext = TPaveText(
            0.72,
            0.77 - gStyle.GetPadTopMargin(),
            0.87,
            0.89 - gStyle.GetPadTopMargin(),
            "NBNDC",
        )
        pavetext.AddText("LHCb simulation")
        pavetext.SetFillColor(0)
        pavetext.SetTextSize(0.06)
        pavetext.SetTextFont(132)
        pavetext.Draw()

        can.Update()
        saveName = "{prefix}_{dependence}_{cat}.pdf".format(
            prefix=prefix, dependence=dependence, cat=cat
        )
        can.SaveAs(saveName)


def get_robust_sigma(hist, resol):
    y = array("d", [0.0] * 3)
    x = array("d", [0.0] * 3)
    x[0] = 0.25
    x[1] = 0.50
    x[2] = 0.75

    hist.GetQuantiles(3, y, x)

    _median = y[1]
    _approxstdev = (y[2] - y[0]) / 1.34898
    # factor gives correspondence between IQR and stdev for a Gaussian
    mult = 4.0

    histclone = hist.Clone()
    nb = histclone.GetNbinsX()

    for i in range(1, nb + 1):
        if (histclone.GetBinCenter(i) < (_median - _approxstdev * mult)) or (
            histclone.GetBinCenter(i) > (_median + _approxstdev * mult)
        ):
            histclone.SetBinContent(i, 0)

    if resol:
        robustsigma = {
            "sigma": {
                "var": histclone.GetRMS() * 1000.0,
                "err": histclone.GetRMSError() * 1000.0,
            },
            "mean": {
                "var": histclone.GetMean() * 1000.0,
                "err": histclone.GetMeanError() * 1000.0,
            },
        }

    else:
        robustsigma = {
            "sigma": {
                "var": histclone.GetRMS() * 1.0,
                "err": histclone.GetRMSError() * 1.0,
            },
            "mean": {"var": histclone.GetMean(), "err": histclone.GetMeanError()},
        }

    return robustsigma


def get_dependence(
    graph, trees, variable, dependence, ranges, style, categories, label, offset
):
    if variable.find("pull") > -1:
        var = transfer_variable(dependence)
    else:
        var = variable
    bin_width = (ranges[dependence]["max"] - ranges[dependence]["min"]) / ranges[
        dependence
    ]["bins"]

    resol = True
    mean = "Mean [#mu m]"
    if "pull" in variable:
        resol = False
        mean = "Mean"

    for cat in categories:
        graph[cat] = {}
        i = offset
        for lab in label:
            graph[cat][lab] = {}
            graph_rms = TGraphAsymmErrors(ranges[dependence]["bins"])
            graph_mean = TGraphAsymmErrors(ranges[dependence]["bins"])

            bin_min = ranges[dependence]["min"]
            for b in range(0, ranges[dependence]["bins"]):
                cut_bin = "{variable1}>={mins} && {variable2}<{maxs}".format(
                    variable1=dependence,
                    mins=bin_min,
                    variable2=dependence,
                    maxs=bin_min + bin_width,
                )

                hist_string = "{dependence}>>hist_{dep}_{cat}_{i}_{number}".format(
                    dependence=var,
                    dep=variable,
                    cat=cat,
                    i=i,
                    number=int(b),
                )

                cut = "{basic} && reco == 1 && {bin_cut} {cuts}".format(
                    basic=basic_cut(), bin_cut=cut_bin, cuts=categories[cat]["cut"]
                )

                trees[lab].Draw(hist_string, cut)

                bin_min += bin_width

                h = gDirectory.Get(
                    "hist_{dep}_{cat}_{i}_{number}".format(
                        dep=variable, cat=cat, i=i, number=int(b)
                    )
                )

                robust_sigma = get_robust_sigma(h, resol)

                graph_mean.SetPoint(
                    b, bin_min + bin_width / 2.0, robust_sigma["mean"]["var"]
                )
                graph_mean.SetPointError(
                    b,
                    bin_width / 2.0,
                    bin_width / 2.0,
                    robust_sigma["mean"]["err"],
                    robust_sigma["mean"]["err"],
                )

                graph_rms.SetPoint(
                    b, bin_min + bin_width / 2.0, robust_sigma["sigma"]["var"]
                )
                graph_rms.SetPointError(
                    b,
                    bin_width / 2.0,
                    bin_width / 2.0,
                    robust_sigma["sigma"]["err"],
                    robust_sigma["sigma"]["err"],
                )

                set_style(
                    graph_rms,
                    style["color"][i],
                    style["marker"][i],
                    get_x_axis(dependence),
                    get_y_axis(variable),
                    "",
                )
                set_style(
                    graph_mean,
                    style["color"][i],
                    style["marker"][i],
                    get_x_axis(dependence),
                    mean,
                    "",
                )

            graph[cat][lab]["sigma"] = graph_rms
            graph[cat][lab]["mean"] = graph_mean
            i += 1

    return graph
