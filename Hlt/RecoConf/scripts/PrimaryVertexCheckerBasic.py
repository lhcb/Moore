###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#!/usr/bin/python

# The script for plotting PV efficinecy as the function
# of various distributions: nTracks, z, r.
# As input the NTuple created by hlt1_reco_pvchecker.py
# is needed.
#
# The efficency is calculated usig TGraphAsymmErrors
# and Bayesian error bars
#
# author: Agnieszka Dziurda (agnieszka.dziurda@cern.ch)
# date:   02/2020
#
# Example of usage:
# ../../../run python PrimaryVertexCheckerPull.py
# --file file1.root file2.root --label name1 name2
#

import argparse
import os
import sys

from ROOT import TLegend, gROOT

parser = argparse.ArgumentParser()
parser.add_argument(
    "--file", dest="fileName", default="", nargs="+", help="filename to plot"
)
parser.add_argument(
    "--label", dest="label", default="", nargs="+", help="labels for files"
)
parser.add_argument(
    "--tree",
    dest="treeName",
    default="",
    nargs="+",
    help="tree name to plot",
)
parser.add_argument(
    "--smog", dest="smog", default=False, action="store_true", help="set true for SMOG"
)
parser.add_argument(
    "--multi",
    dest="multi",
    default=False,
    action="store_true",
    help="add multiplicity plots",
)
parser.add_argument(
    "--isol",
    dest="isol",
    default=False,
    action="store_true",
    help="add isolated/closed plots",
)
parser.add_argument(
    "--dist",
    dest="dist",
    default=False,
    action="store_true",
    help="plot distributions in the canvas",
)
parser.add_argument(
    "--prefix",
    dest="prefix",
    default="pv_basic",
    help="prefix for the plot name",
)
parser.add_argument(
    "--dir",
    dest="directory",
    default=os.getcwd(),
    help="tree name to plot",
)

parser.add_argument(
    "--offset",
    dest="offset",
    default=0,
    help="offset for plot colors",
)


def get_labels(number_of_files):
    label = []
    for i in range(0, number_of_files):
        label.append("PV Checker {number}".format(number=str(i + 1)))
    return label


if __name__ == "__main__":
    args = parser.parse_args()
    path = args.directory + "/utils/"
    sys.path.append(os.path.abspath(path))
    offset = int(args.offset)
    gROOT.SetBatch()

    from pvconfig import (
        get_categories,
        get_style,
        get_variable_ranges,
        set_legend_simple,
    )
    from pvutils import get_files, get_global, get_trees, plot_comparison

    ranges = get_variable_ranges(args.smog)
    style = get_style()

    cat = get_categories(args.multi, args.isol, args.smog)
    label = args.label
    if args.label == "":
        label = get_labels(len(args.fileName))

    tr = {}
    tf = {}
    tf = get_files(tf, label, args.fileName)
    tr = get_trees(tf, tr, label, args.treeName, True)

    hist_trueMCPVs = {}
    hist_visMCPVs = {}
    hist_zMC = {}
    hist_xMC = {}
    hist_yMC = {}
    hist_part = {}

    norm = True  # to-do
    hist_trueMCPVs = get_global(
        hist_trueMCPVs,
        tr,
        "mtruemcpv",
        "All MC PVs",
        "Candidates Normalized",
        style,
        ranges,
        cat,
        label,
        offset,
        True,
    )
    hist_visMCPVs = get_global(
        hist_visMCPVs,
        tr,
        "nmcpv",
        "Reconstructible MC PVs}",
        "Candidates Normalized",
        style,
        ranges,
        cat,
        label,
        offset,
        True,
    )
    hist_zMC = get_global(
        hist_zMC,
        tr,
        "zMC",
        "PV z [mm]",
        "Candidates Normalized",
        style,
        ranges,
        cat,
        label,
        offset,
        True,
    )
    hist_xMC = get_global(
        hist_xMC,
        tr,
        "xMC",
        "PV x [mm]",
        "Candidates Normalized",
        style,
        ranges,
        cat,
        label,
        offset,
        True,
    )
    hist_yMC = get_global(
        hist_yMC,
        tr,
        "yMC",
        "PV y [mm]",
        "Candidates Normalized",
        style,
        ranges,
        cat,
        label,
        offset,
        True,
    )
    hist_part = get_global(
        hist_part,
        tr,
        "nrectrmc",
        "MC Particles in MC PVs",
        "Candidates Normalized",
        style,
        ranges,
        cat,
        label,
        offset,
        True,
    )

    legend = TLegend(0.15, 0.86, 0.88, 0.98)
    legend = set_legend_simple(legend, label, hist_trueMCPVs)

    plot_comparison(
        hist_trueMCPVs,
        args.prefix,
        "mtruemcpv",
        cat,
        label,
        style,
        norm,
        offset,
        True,
        legend,
    )
    plot_comparison(
        hist_visMCPVs,
        args.prefix,
        "nmcpv",
        cat,
        label,
        style,
        norm,
        offset,
        True,
        legend,
    )

    plot_comparison(
        hist_zMC, args.prefix, "zMC", cat, label, style, norm, offset, True, legend
    )
    plot_comparison(
        hist_xMC, args.prefix, "xMC", cat, label, style, norm, offset, True, legend
    )
    plot_comparison(
        hist_yMC, args.prefix, "yMC", cat, label, style, norm, offset, True, legend
    )

    plot_comparison(
        hist_part,
        args.prefix,
        "nrectrmc",
        cat,
        label,
        style,
        norm,
        offset,
        True,
        legend,
    )
