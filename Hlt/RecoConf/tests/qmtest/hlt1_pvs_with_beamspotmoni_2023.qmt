<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="program"><text>gaudirun.py</text></argument>
<argument name="args"><set>
  <text>$MOOREROOT/tests/options/mdf_input_and_conds_data_2023.py</text>
  <text>$RECOCONFROOT/options/hlt1_pvs_with_beamspotmoni_2023.py</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="reference"><text>../refs/hlt1_pvs_with_beamspotmoni_2023.ref</text></argument>
<argument name="error_reference"><text>../refs/empty.ref</text></argument>
<argument name="validator"><text>
from Moore.qmtest.exclusions import ref_preprocessor, counter_preprocessor, sensitivities
validateWithReference(
    preproc=ref_preprocessor,
    counter_preproc=counter_preprocessor,
    sensitivities=sensitivities)

from Moore.qmtest.exclusions import remove_known_warnings
countErrorLines({
    "FATAL": 0,
    "ERROR": 0,
    "WARNING": 0
},
                stdout=remove_known_warnings(stdout))

# Parse pathname of YAML files from stdout
# Verify that at least one YAML file has been written
import re
from os.path import isfile

pattern = re.compile(
    r'BeamSpotMonitor\s+INFO Writing new conditions file (.+)')
written = 0
found = 0
for line in stdout.split('\n'):
    m = re.match(pattern, line)
    if m:
        written += 1
        yfile = m.group(1)
        if isfile(yfile): found += 1

if written == 0:
    causes.append('No output YAML file written')

if found != written:
    causes.append('One or more output YAML files not found')
</text></argument>
</extension>
