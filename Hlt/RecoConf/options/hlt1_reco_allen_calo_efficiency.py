###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Allen.config import run_allen_reconstruction
from AllenCore.gaudi_allen_generator import allen_runtime_options
from RecoConf.calo_data_monitoring import monitor_calo_clusters
from RecoConf.calorimeter_mc_checking import check_calo_cluster_efficiency
from RecoConf.calorimeter_reconstruction import make_calo_reduced
from RecoConf.config import Reconstruction
from RecoConf.hlt1_allen import allen_gaudi_config, make_allen_calo_clusters
from RecoConf.options import options


def make_reconstruction():
    # Get Allen calo clusters
    allen_calo_clusters = make_allen_calo_clusters()
    ecal_clusters_allen = allen_calo_clusters["ecal_clusters"]

    # Get calo digits
    calo = make_calo_reduced()

    # Build Calo::ClusterMonitor
    data_producers = monitor_calo_clusters(
        ecal_clusters_allen, name="CaloClusterMonitor"
    )

    # Build CaloClusterEfficiency
    calo["ecalClusters"] = ecal_clusters_allen  # for mc checking
    calo_clusters_efficiency = check_calo_cluster_efficiency(calo)
    data_producers += calo_clusters_efficiency

    return Reconstruction("calo_cluster_efficiency", data_producers)


options.evt_max = 1000
options.histo_file = "Hlt1RecoAllenCaloEfficiencyHistos.root"
options.ntuple_file = "Hlt1RecoAllenCaloEfficiencyNTuple.root"
options.histo_calo = True

with (
    allen_gaudi_config.bind(sequence="hlt1_cosmics"),
    allen_runtime_options.bind(filename="allen_calo_eff_monitor.root"),
):
    run_allen_reconstruction(options, make_reconstruction)
