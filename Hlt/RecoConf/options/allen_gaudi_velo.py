###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Allen.config import run_allen_reconstruction
from AllenCore.gaudi_allen_generator import allen_runtime_options
from PyConf.Algorithms import VPRetinaFullClustering
from RecoConf.config import Reconstruction
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.hlt1_allen import allen_gaudi_config, make_allen_velo_tracks
from RecoConf.legacy_rec_hlt1_tracking import make_velo_full_clusters
from RecoConf.mc_checking import (
    check_tracking_efficiency,
    make_links_lhcbids_mcparticles_VP,
    make_links_tracks_mcparticles,
)
from RecoConf.mc_checking_categories import get_hit_type_mask, get_mc_categories
from RecoConf.options import options


def make_reconstruction():
    velo_tracks = make_allen_velo_tracks()

    # make links to lhcb id for mc matching
    # ms: this uses the CPU version of the retina decoding/clustering, but we test that Allen and this gives the same clusters/LHCbIDs
    links_to_lhcbids = make_links_lhcbids_mcparticles_VP()

    # build the PrChecker algorihm for velo track
    links_to_velo_tracks = make_links_tracks_mcparticles(
        InputTracks=velo_tracks["v1keyed"], LinksToLHCbIDs=links_to_lhcbids
    )

    pr_checker_for_velo_track = check_tracking_efficiency(
        TrackType="Velo",
        InputTracks=velo_tracks["v1keyed"],
        LinksToTracks=links_to_velo_tracks,
        LinksToLHCbIDs=links_to_lhcbids,
        MCCategories=get_mc_categories("Velo"),
        HitTypesToCheck=get_hit_type_mask("Velo"),
    )

    return Reconstruction("track_efficiency", [pr_checker_for_velo_track])


options.evt_max = 100
options.histo_file = "Hlt1VeloTrackingResolutionAllen.root"

with (
    make_velo_full_clusters.bind(make_full_cluster=VPRetinaFullClustering),
    allen_gaudi_config.bind(sequence="velo", node_name="config"),
    default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),
    allen_runtime_options.bind(filename="allen_gaudi_velo_monitor.root"),
):
    run_allen_reconstruction(options, make_reconstruction)
