###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from RecoConf.config import run_reconstruction
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.options import options
from RecoConf.standalone import standalone_hlt1_reco

# The suffix is needed to remove a race condition between tests
options.histo_file = "MCMatching_decode_retina_MiniBias.root"

with (
    standalone_hlt1_reco.bind(do_mc_checking=True),
    default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),
):
    run_reconstruction(options, standalone_hlt1_reco)

options.histo_file = "MCMatching_decode_retina_MiniBias.root"
