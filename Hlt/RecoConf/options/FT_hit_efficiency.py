###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from PyConf.Algorithms import (
    FTHitEfficiencyMonitor,
    PrKalmanFilter_noUT,
    PrKalmanFilter_Seed,
    PrKalmanFilter_Velo,
    PrStoreSciFiHits,
    TrackListMCTruthRefiner,
)
from PyConf.application import make_odin
from RecoConf import mc_checking
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.hlt2_tracking import (
    make_hlt2_tracks_without_UT,
    make_PrStoreSciFiHits_hits,
)
from RecoConf.legacy_rec_hlt1_tracking import make_FTRawBankDecoder_clusters
from RecoConf.options import options

# save output file with histograms
options.histo_file = options.getProp("histo_file") or "hlt2_FT_hit_efficiency.root"


def hit_efficiency_sequence():
    odin = make_odin()
    data = [odin]

    for layer_under_study in range(4):  # histograms for the first station
        with (
            PrKalmanFilter_noUT.bind(FillFitResult=True),
            PrKalmanFilter_Seed.bind(FillFitResult=True),
            PrKalmanFilter_Velo.bind(FillFitResult=True),
            make_PrStoreSciFiHits_hits.bind(disabled_layers=[layer_under_study]),
        ):
            hlt2_tracks = make_hlt2_tracks_without_UT(
                light_reco=True, fast_reco=False, use_pr_kf=True
            )

            my_enabled_layers = [j not in [layer_under_study] for j in range(12)]
            all_FT_pr_hits = PrStoreSciFiHits(
                HitsLocation=make_FTRawBankDecoder_clusters(),
                LayerMasks=tuple(my_enabled_layers),
            ).Output

            my_ft_efficiency_alg = FTHitEfficiencyMonitor(
                name="FTHitEfficiencyLayer{}".format(layer_under_study),
                TrackLocation=hlt2_tracks["BestLong"]["v1"],
                PrFTHitsLocation=all_FT_pr_hits,
                LayerUnderStudy=layer_under_study,
            )

            data += [my_ft_efficiency_alg]

            # Add MC-only information, as illustration of
            # debugging options
            mc_particles = mc_checking.mc_unpacker("MCParticles")

            links_to_lhcbids = (
                mc_checking.make_links_lhcbids_mcparticles_tracking_system()
            )
            links_to_mcp = mc_checking.make_links_tracks_mcparticles(
                InputTracks=hlt2_tracks["BestLong"]["v1"],
                LinksToLHCbIDs=links_to_lhcbids,
            )

            my_filtered_hlt2_tracks = TrackListMCTruthRefiner(
                InputTracks=hlt2_tracks["BestLong"]["v1"],
                TrackToMCPRelations=links_to_mcp,
                MCParticles=mc_particles,
                MCCut=F.ALL,
            ).OutputPassTracks

            my_ft_efficiency_alg_no_ghosts = FTHitEfficiencyMonitor(
                name="FTHitEfficiencyNoGhostsLayer{}".format(layer_under_study),
                TrackLocation=my_filtered_hlt2_tracks,
                PrFTHitsLocation=all_FT_pr_hits,
                LayerUnderStudy=layer_under_study,
            )
            data += [my_ft_efficiency_alg_no_ghosts]

    return Reconstruction("hlt2_hit_eff_reco", data, [])


run_reconstruction(options, hit_efficiency_sequence)
