###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import mm, mm2
from PyConf.Algorithms import BeamSpotMonitor
from PyConf.application import make_odin
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.legacy_rec_hlt1_tracking import (
    make_all_pvs,
    make_PatPV3DFuture_pvs,
    make_reco_pvs,
    make_VeloClusterTrackingSIMD,
)
from RecoConf.options import options
from RecoConf.standalone import reco_prefilters


#####
## Pared down reconstruction sequence
#####
def run_hlt1_pvs_with_beamspotmoni():
    """Run the Hlt2 track reconstruction optimized for not having the UT detector

    Args:

    Returns:
        Reconstruction: Data and control flow of Hlt2 track reconstruction.

    """
    pvs = make_all_pvs()

    from PyConf.Algorithms import VertexListRefiner

    selected_pvs = {
        "v1": VertexListRefiner(
            InputLocation=pvs["v1"], MinNumTracks=15, MinZ=-200 * mm, MaxZ=500 * mm
        ).OutputLocation
    }

    beam_spot_moni = BeamSpotMonitor(
        name="BeamSpotMonitor",
        ODINLocation=make_odin(),
        PVContainer=selected_pvs["v1"],
        MinPVsForCalib=200,
        LogConditions=True,
        MakeDBRunFile=True,
        conditionsDbPath=".",
        VeloOpenUpdates=True,
        MaxAbsDeltaMap={
            "x": 0.0 * mm,
            "y": 0.0 * mm,
            "xx": 0.0 * mm2,
            "xy": 0.0 * mm2,
            "yy": 0.0 * mm2,
        },
    )
    data = [pvs["v1"], selected_pvs["v1"], beam_spot_moni]

    return Reconstruction("pvs_reco", data, reco_prefilters(skipUT=True))


options.histo_file = "hlt1_pvs_with_beamspotmoni_2023.root"

with (
    default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),
    make_VeloClusterTrackingSIMD.bind(SkipForward=4),
    make_PatPV3DFuture_pvs.bind(
        velo_open=True, use_3D_seeding=True, use_beam_spot_cut=False
    ),
    make_reco_pvs.bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs),
):
    run_reconstruction(options, run_hlt1_pvs_with_beamspotmoni)
