###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from functools import partial

from PyConf import configurable
from PyConf.Algorithms import (
    GhostProb_Downstream_TrainingTupleAlg,
    GhostProb_Long_noUT_TrainingTupleAlg,
    GhostProb_Long_TrainingTupleAlg,
    GhostProb_Ttrack_TrainingTupleAlg,
    GhostProb_Upstream_TrainingTupleAlg,
    GhostProb_Velo_TrainingTupleAlg,
)
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.data_from_file import mc_unpackers
from RecoConf.event_filters import require_gec
from RecoConf.hlt2_global_reco import (
    make_fastest_reconstruction,
    make_light_reco_pr_kf_without_UT,
    reconstruction,
)
from RecoConf.mc_checking import (
    make_links_lhcbids_mcparticles_tracking_system,
    make_links_tracks_mcparticles,
)
from RecoConf.options import options


def get_tuple_alg(track_type, with_UT=False):
    if track_type == "Long":
        return (
            GhostProb_Long_TrainingTupleAlg
            if with_UT
            else GhostProb_Long_noUT_TrainingTupleAlg
        )
    elif track_type == "Downstream":
        return GhostProb_Downstream_TrainingTupleAlg
    elif track_type == "Upstream":
        return GhostProb_Upstream_TrainingTupleAlg
    elif track_type == "Ttrack":
        return GhostProb_Ttrack_TrainingTupleAlg
    elif track_type == "Velo":
        return GhostProb_Velo_TrainingTupleAlg
    return None


@configurable
def standalone_hlt2_tracks(
    models={"Long_noUT": {"type": "Long", "weights": ""}},
    make_reconstruction=make_fastest_reconstruction,
):
    reco = reconstruction(make_reconstruction=make_reconstruction)
    mc_parts = mc_unpackers()["MCParticles"]
    links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()

    data = []
    for key, value in models.items():
        reco_name = value["reco_obj"]
        tracks = reco.get(reco_name, None)
        if tracks is None:
            tracks = reco["AllTrackHandles"][reco_name]["v1"]
        links_to_tracks = make_links_tracks_mcparticles(
            tracks, LinksToLHCbIDs=links_to_lhcbids
        )

        tuple_alg = get_tuple_alg(track_type=value["type"], with_UT="noUT" not in key)
        training_tuple = tuple_alg(
            name=f"TrainingTupleAlg_GhostProb_{key}",
            InputObjects=tracks,
            MCParticles=mc_parts,
            LinksToMC=links_to_tracks,
            WeightsFileName=value["weights"],
        )

        data.append(training_tuple)

    prefilters = [require_gec()]

    return Reconstruction("hlt2_tracks", data, prefilters)


options.ntuple_file = "ghostprob_test_training_tuple.root"
options.evt_max = 1000

models = {
    "Long_noUT": {
        "type": "Long",
        "reco_obj": "LongTracks",
        "weights": "paramfile:///data/GhostProbability/hlt2_ghostprob_long_noUT.json",
    },
    "Ttrack": {
        "type": "Ttrack",
        "reco_obj": "BestSeed",
        "weights": "paramfile:///data/GhostProbability/hlt2_ghostprob_ttrack_noUT.json",
    },
    "Velo": {
        "type": "Velo",
        "reco_obj": "BestVelo",
        "weights": "paramfile:///data/GhostProbability/hlt2_ghostprob_velo_noUT.json",
    },
}

with standalone_hlt2_tracks.bind(make_reconstruction=make_light_reco_pr_kf_without_UT):
    run_reconstruction(options, partial(standalone_hlt2_tracks, models=models))
