###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Allen.config import setup_allen_non_event_data_service
from AllenConf.ut_reconstruction import decode_ut
from AllenCore.generator import get_constants
from PyConf.Algorithms import CompareRecAllenUTHits, GaudiAllenTransformAllenRecUTHits
from PyConf.application import configure, configure_input
from PyConf.control_flow import CompositeNode, NodeLogic
from RecoConf.legacy_rec_hlt1_tracking import make_PrStorePrUTHits_hits
from RecoConf.options import options

# from Gaudi.Configuration import DEBUG
# options.output_level = DEBUG

config = configure_input(options)
non_event_data_node = setup_allen_non_event_data_service(bank_types=["UT"])

max_cluster_size = 128

# Get Allen constants for UT hits dxdy
constants = get_constants()

allen_decoded_ut = decode_ut(
    cluster_ut_hits=True,
    position_method=0,  # AdcWeighting
    max_cluster_size=max_cluster_size,
)

rec_ut_hits = make_PrStorePrUTHits_hits(
    isCluster=True, positionMethod="AdcWeighting", stripMax=max_cluster_size
)

transformed_ut_hits = GaudiAllenTransformAllenRecUTHits(
    ut_hit_offsets=allen_decoded_ut["dev_ut_hit_offsets"],
    ut_hits=allen_decoded_ut["dev_ut_hits"],
    UTHitsLocation=rec_ut_hits,
    allen_constants=constants,
)

compare_allen_rec_hits = CompareRecAllenUTHits(
    allen_ut_hits=transformed_ut_hits.allen_ut_hits,
    rec_ut_hits=transformed_ut_hits.rec_ut_hits,
    WeightingMethod="Adc",
    MaxClusterSize=max_cluster_size,
)

cf_node = CompositeNode(
    "compare_ut_hits",
    combine_logic=NodeLogic.NONLAZY_OR,
    children=[non_event_data_node, compare_allen_rec_hits],
    force_order=True,
)
config.update(configure(options, cf_node))
