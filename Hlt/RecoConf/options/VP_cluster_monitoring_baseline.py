###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.options import options
from RecoConf.velo_cluster_data_monitoring import monitor_velo_clusters

options.histo_file = "VP_cluster_MiniBias_baseline.root"


def my_monitor():
    return Reconstruction("vp_cluster_monitoring_baseline", [monitor_velo_clusters()])


run_reconstruction(options, my_monitor)
