###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
from RecoConf.config import run_reconstruction
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.options import options
from RecoConf.standalone import phoenix_data_dump_hlt2

dd4hepSvc = DD4hepSvc()
dd4hepSvc.DetectorList = [
    "/world",
    "VP",
    "FT",
    "UT",
    "Magnet",
    "Rich1",
    "Rich2",
    "Ecal",
    "Hcal",
    "Muon",
]

options.evt_max = 20
options.event_store = "EvtStoreSvc"
options.phoenix_filename = "LHCb_EventDataset.json"
options.set_input_and_conds_from_testfiledb("2023_07_04_run268773")

with default_VeloCluster_source.bind(bank_type="VPRetinaCluster"):
    run_reconstruction(options, phoenix_data_dump_hlt2)
