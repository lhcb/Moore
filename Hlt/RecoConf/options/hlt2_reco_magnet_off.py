###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F

# magnetic field treatment DetDesc vs DD4HEP
from DDDB.CheckDD4Hep import UseDD4Hep
from GaudiKernel.SystemOfUnits import GeV, MeV
from PyConf.Algorithms import (
    PrKalmanFilter,
    PrKalmanFilter_noUT,
    TrackCorrelationsMonitor,
    TrackMonitor,
    TracksFromProtoParticleSelection,
)
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.Tools import TrackLinearExtrapolator
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.hlt2_global_reco import (
    make_light_reco_pr_kf,
    make_light_reco_pr_kf_without_UT,
)
from RecoConf.hlt2_tracking import (
    make_PrKalmanFilter_noUT_tracks,
    make_PrKalmanFilter_tracks,
    make_PrStoreSciFiHits_hits,
)
from RecoConf.mc_checking import (
    check_tracking_efficiency,
    get_hit_type_mask,
    get_mc_categories,
    make_links_lhcbids_mcparticles_tracking_system,
    make_links_tracks_mcparticles,
)
from RecoConf.options import options
from RecoConf.standalone import reco_prefilters

if not UseDD4Hep:
    from Configurables import MagneticFieldSvc

    MagneticFieldSvc().UseSetCurrent = True
    MagneticFieldSvc().NominalCurrent = 0.0
else:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as dd4hepsvc

    dd4hepsvc().NominalMagnetCurrent = 0.0


def my_reco_data(with_UT=True):
    make_reco = make_light_reco_pr_kf if with_UT else make_light_reco_pr_kf_without_UT
    make_tracks = (
        make_PrKalmanFilter_tracks if with_UT else make_PrKalmanFilter_noUT_tracks
    )
    my_kalmanfilter = PrKalmanFilter if with_UT else PrKalmanFilter_noUT

    # data
    with (
        make_tracks.bind(
            max_chi2ndof=4.0, reference_extrapolator=TrackLinearExtrapolator
        ),
        my_kalmanfilter.bind(MagOff=True, VeloTrackPT=400.0 * MeV),
    ):
        reco = make_reco()

    protos_long = reco["LongProtos"]
    ecal_filter = F.require_all(
        F.INECAL, F.CLUSTERMATCH_CHI2 < 16, F.ELECTRONENERGY > 10.0 * GeV
    )
    my_filtered_tracks = TracksFromProtoParticleSelection(
        Input=protos_long, TrackPredicate=F.ALL, ProtoParticlePredicate=ecal_filter
    ).OutputTracks

    data = [reco["LongTracks"], reco["VeloTracks"], reco["PVs"], my_filtered_tracks]

    scifihits = make_PrStoreSciFiHits_hits()

    # monitoring
    for tracks, name in [(reco["LongTracks"], "Long"), (reco["VeloTracks"], "Velo")]:
        data += [
            TrackMonitor(name=f"TrackMonitor_{name}", TracksInContainer=tracks),
            TrackCorrelationsMonitor(
                name=f"TrackCorrelationsMonitor_{name}",
                TracksInContainer=tracks,
                SciFiHits=scifihits,
                StatesFor2DHits=["VELO", "UT", "FT"],
                Extrapolator=TrackLinearExtrapolator(),
            ),
        ]

    # mc checking
    if options.simulation:
        links_to_lhcbids = make_links_lhcbids_mcparticles_tracking_system()
        for tracks, tt in [(reco["LongTracks"], "BestLong")]:
            links_to_tracks = make_links_tracks_mcparticles(
                InputTracks={"v1": tracks}, LinksToLHCbIDs=links_to_lhcbids
            )
            data += [
                check_tracking_efficiency(
                    InputTracks=tracks,
                    TrackType=tt,
                    LinksToTracks=links_to_tracks,
                    LinksToLHCbIDs=links_to_lhcbids,
                    MCCategories=get_mc_categories(tt),
                    HitTypesToCheck=get_hit_type_mask(tt),
                )
            ]

    # control logic
    data_producers_node = CompositeNode(
        "data_with_gec", data, combine_logic=NodeLogic.NONLAZY_OR, force_order=True
    )

    with_gec_node = CompositeNode(
        "with_gec",
        reco_prefilters(skipUT=True) + [data_producers_node],
        combine_logic=NodeLogic.LAZY_AND,
        force_order=True,
    )

    return Reconstruction("hlt2_reco_magoff", [with_gec_node])


# main
run_reconstruction(options, my_reco_data)
