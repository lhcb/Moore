###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from RecoConf.config import run_reconstruction
from RecoConf.hlt2_tracking import make_hlt2_tracks
from RecoConf.options import options
from RecoConf.standalone import standalone_hlt2_reco

options.ntuple_file = "hlt2_fast_reco_with_mcchecking.root"

with (
    standalone_hlt2_reco.bind(do_mc_checking=True, do_data_monitoring=True),
    make_hlt2_tracks.bind(fast_reco=True),
):
    run_reconstruction(options, standalone_hlt2_reco)
