###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Allen.config import setup_allen_non_event_data_service
from AllenConf.calo_reconstruction import decode_calo
from PyConf.Algorithms import CompareRecAllenCaloDigits
from PyConf.application import configure, configure_input
from PyConf.control_flow import CompositeNode, NodeLogic
from RecoConf.calorimeter_reconstruction import make_calo_reduced
from RecoConf.options import options

options.evt_max = 500
config = configure_input(options)
non_event_data_node = setup_allen_non_event_data_service()

# Get Allen calo digits
calo_allen = decode_calo()
calo_digits_allen = calo_allen["dev_ecal_digits"]

# Get HLT2 calo digits
calo = make_calo_reduced()
calo_digits = calo["digitsEcal"]

test_digits = CompareRecAllenCaloDigits(
    ecal_digits=calo_digits_allen, EcalDigits=calo_digits
)

cf_node = CompositeNode(
    "compare_calo_digits",
    combine_logic=NodeLogic.NONLAZY_OR,
    children=[non_event_data_node, test_digits],
    force_order=True,
)
config.update(configure(options, cf_node))
