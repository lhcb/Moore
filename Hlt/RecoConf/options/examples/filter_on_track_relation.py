###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from PyConf.Algorithms import (
    PrFilterTracks2CaloClusters,
    PrFilterTracks2ElectronMatch,
    PrFilterTracks2ElectronShower,
    PrMatchNNv3,
)
from RecoConf.calorimeter_reconstruction import (
    make_acceptance,
    make_clusters,
    make_digits,
    make_photons_and_electrons,
    make_track_cluster_matching,
    make_track_electron_and_brem_matching,
    make_trackbased_eshower,
)
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.event_filters import require_gec
from RecoConf.hlt2_tracking import (
    convert_tracks_to_v3_from_v1,
    get_ignore_ut_hits_tool,
    make_hlt2_tracks,
)
from RecoConf.legacy_rec_hlt1_tracking import make_all_pvs
from RecoConf.options import options


def reco():
    hlt2_tracks = make_hlt2_tracks(light_reco=True, fast_reco=False, use_pr_kf=True)

    digisEcal = make_digits()["digitsEcal"]
    ecalClusters = make_clusters(digisEcal)
    tracks_v3, trackrels = convert_tracks_to_v3_from_v1(
        hlt2_tracks["Seed"]["v1"],
        track_types=["Ttrack"],
    )

    tracks_incalo = make_acceptance(tracks_v3)
    tcmatches = make_track_cluster_matching(ecalClusters, tracks_incalo)
    PhElOutput = make_photons_and_electrons(
        ecalClusters,
        tcmatches["combined"],
        make_all_pvs()["v3"],
    )
    photons = PhElOutput["photons"]
    electrons = PhElOutput["electrons"]

    eshower = make_trackbased_eshower(tracks_incalo, digisEcal)

    tcmatches_e = make_track_electron_and_brem_matching(
        tracks_incalo, tcmatches, digisEcal, electrons, photons
    )

    # filter seed tracks according to their first weight, i.e. cluster chi2
    # this is a 2D relation, so the weight function returns a tuple of vectors
    # we take the first argument from the tuple and forward it (the vector) to
    # a function that finds the smallest chi2 larger than zero (zero is the default
    # initialised value and means the same as no relation).
    calo_matched_seeds = PrFilterTracks2CaloClusters(
        Relation=tcmatches["Ttrack"],
        Cut=F.FILTER((F.MIN_ELEMENT_NOTZERO @ F.FORWARDARG0 @ F.WEIGHT) < 20),
    ).Output

    electron_matched_seeds = PrFilterTracks2ElectronMatch(
        Relation=tcmatches_e["Ttrack"]["ElectronMatch"],
        Cut=F.FILTER(F.MIN_ELEMENT_NOTZERO @ F.FORWARDARG0 @ F.WEIGHT < 20),
    ).Output

    # tracks and electron shower have a 1D relation, so the weight function returns
    # a tuple of values. E/p is the first value, accessed using std::get via the
    # functor.
    shower_matched_seeds = PrFilterTracks2ElectronShower(
        Relation=eshower["Ttrack"], Cut=F.FILTER((F.GET(0) @ F.WEIGHT) > 0.7)
    ).Output

    # the output of above's filters is a v3 track container that can be
    # passed to approriate algos like the one below
    calo_long = PrMatchNNv3(
        VeloInput=hlt2_tracks["Velo"]["Pr"],
        SeedInput=calo_matched_seeds,
        AddUTHitsToolName=get_ignore_ut_hits_tool(),
    ).MatchOutput

    data = [calo_long, electron_matched_seeds, shower_matched_seeds]

    return Reconstruction("hlt2_reco", data, [require_gec()])


run_reconstruction(options, reco)
