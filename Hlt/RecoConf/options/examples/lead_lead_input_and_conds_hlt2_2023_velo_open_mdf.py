###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# To be removed once data is updated to use a tag including
# https://gitlab.cern.ch/lhcb-conddb/DDDB/-/merge_requests/107
from DDDB.CheckDD4Hep import UseDD4Hep
from RecoConf.options import options

if UseDD4Hep:
    from Configurables import DDDBConf

    DDDBConf().GeometryVersion = "before-rich1-geom-update-26052022"

options.set_input_and_conds_from_testfiledb(
    "upgrade-202305-PbPb-EPOS-b-8_22-VELO-10mm-mdf"
)

options.evt_max = 300
