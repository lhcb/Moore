###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from RecoConf.config import run_reconstruction
from RecoConf.options import options
from RecoConf.standalone import standalone_hlt2_seeding

options.ntuple_file = "hlt2_reco_seeding_UT_with_mcchecking_tuple.root"
options.histo_file = "hlt2_reco_seeding_UT_with_mcchecking.root"
options.histo_calo = True

with standalone_hlt2_seeding.bind(do_mc_checking=True, do_data_monitoring=True):
    run_reconstruction(options, standalone_hlt2_seeding)
