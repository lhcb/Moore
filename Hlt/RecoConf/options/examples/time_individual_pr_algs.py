###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import (
    PrForwardTrackingVelo,
    PrHybridSeeding,
    PrMatchNN,
    TimingTuple,
)
from PyConf.application import make_odin
from PyConf.Tools import SequencerTimerTool
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.hlt2_global_reco import make_light_reco_pr_kf, make_rec_summary
from RecoConf.options import options

options.ntuple_file = "Moore/timings.root"

options.set_input_and_conds_from_testfiledb("upgrade_Sept2022_BsPhiPhi_0fb_md_xdigi")
options.evt_max = 100


def time_algos():
    timer_tool = SequencerTimerTool(public=True)
    with (
        PrForwardTrackingVelo.bind(
            TimerTool=timer_tool, name="TimedPrForwardTrackingVelo"
        ),
        PrMatchNN.bind(TimerTool=timer_tool, name="TimedPrMatchNN"),
        PrHybridSeeding.bind(TimerTool=timer_tool, name="TimedPrHybridSeeding"),
    ):
        reco_output = make_light_reco_pr_kf()
        rec_summary = make_rec_summary(reco_output)

    timing = TimingTuple(
        ODINLocation=make_odin(),
        RecSummaryLocation=rec_summary,
        TimeAlgos=[
            "TimedPrForwardTrackingVelo",
            "TimedPrMatchNN",
            "TimedPrHybridSeeding",
        ],
    )
    return Reconstruction("rec", [timing], [])


run_reconstruction(options, time_algos)
