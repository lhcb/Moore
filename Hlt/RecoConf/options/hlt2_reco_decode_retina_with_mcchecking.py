###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
This set of options is used for reconstruction development purposes,
and assumes that the input contains MCHits (i.e. is of `Exended`
DST/digi type).
"""

from RecoConf.config import run_reconstruction
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.mc_checking import check_track_resolution
from RecoConf.options import options
from RecoConf.standalone import standalone_hlt2_full_track_reco

options.histo_file = "hlt2_track_monitoring_with_mc_decode_retina_histos.root"
options.ntuple_file = "hlt2_track_monitoring_with_mc_decode_retina_ntuples.root"

with (
    standalone_hlt2_full_track_reco.bind(do_mc_checking=True),
    default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),
    check_track_resolution.bind(per_hit_resolutions=False, split_per_type=True),
):
    run_reconstruction(options, standalone_hlt2_full_track_reco)
