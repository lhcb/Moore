###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Allen.config import run_allen_reconstruction
from AllenCore.gaudi_allen_generator import allen_runtime_options
from PyConf.Algorithms import VPRetinaFullClustering
from RecoConf.config import Reconstruction
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.event_filters import require_gec
from RecoConf.hlt1_allen import make_allen_velo_ut_tracks
from RecoConf.legacy_rec_hlt1_tracking import (
    get_global_clusters_on_track_tool,
    make_FTRawBankDecoder_empty_clusters,
    make_velo_full_clusters,
)
from RecoConf.mc_checking import (
    check_tracking_efficiency,
    make_links_lhcbids_mcparticles_VP_UT,
    make_links_tracks_mcparticles,
)
from RecoConf.mc_checking_categories import get_hit_type_mask, get_mc_categories
from RecoConf.options import options


def make_reconstruction():
    velo_ut_tracks = make_allen_velo_ut_tracks()

    # make links to lhcb id for mc matching
    links_to_lhcbids = make_links_lhcbids_mcparticles_VP_UT()

    # build the PrChecker algorihm for forward track
    links_to_velo_ut_tracks = make_links_tracks_mcparticles(
        InputTracks=velo_ut_tracks["v1keyed"], LinksToLHCbIDs=links_to_lhcbids
    )

    pr_checker_for_velo_ut_track = check_tracking_efficiency(
        TrackType="VeloUT",
        InputTracks=velo_ut_tracks["v1keyed"],
        LinksToTracks=links_to_velo_ut_tracks,
        LinksToLHCbIDs=links_to_lhcbids,
        MCCategories=get_mc_categories("Upstream"),
        HitTypesToCheck=get_hit_type_mask("Upstream"),
    )

    return Reconstruction(
        "track_efficiency", [pr_checker_for_velo_ut_track], [require_gec()]
    )


options.evt_max = 100
options.histo_file = "Hlt1VeloUTTrackingResolutionAllen.root"

with (
    make_velo_full_clusters.bind(make_full_cluster=VPRetinaFullClustering),
    allen_runtime_options.bind(filename="allen_gaudi_velo_ut_monitor.root"),
    default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),
    get_global_clusters_on_track_tool.bind(
        ft_clusters=make_FTRawBankDecoder_empty_clusters
    ),
):
    run_allen_reconstruction(options, make_reconstruction)
