###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Pay attention on this specific configuration as it is just an integration test.

Options file to test the tracking in the VELO. At the moment only the VPHitEfficiencyMonitor is included, but this can be extended to include other algorithms.

In the VPHitEfficiencyMonitor, the hit efficiency of sensors is determined and stored in an output ROOT file. Note: This is done independantly fromt the tracking efficiency.
The VPHitEfficiencyMonitor will produce the hit efficiency per sensor. To cover the entire VELO, a loop over sensor_under_study is needed (but tracking is ran only once). At the moment this loop is
kept short for integration tests, but all 208 sensors should be used when the pseudo hit efficiencies are the desired output.
The tracks are fitted using the TrackMasterFitter and all extrapolators used are LinearExtrapolators. This includes the extrapolator used in the TrackInterpolator.
As input for the VPHitEfficiencyMonitor, data is intended. To have 1 entry in all sensors requires on O(10^3) events at avg_mu=2.2. A more detailed map of the hit efficiencies,
for instance to use in Boole, needs an order of events of ~O(10^10). (assuming 16x16 pixel bins)
For the unbiassed hit efficiencies, an example options file can be found under ./examples/VP_hit_efficiency.py
"""

import Functors as F
from PyConf.Algorithms import (
    TrackEventFitter,
    TrackListRefiner,
    TrackSelectionToContainer,
    VeloRetinaClusterTrackingSIMDFull,
    VPHitEfficiencyMonitor,
    fromPrVeloTracksV1TracksMerger,
)
from PyConf.application import default_raw_banks, make_odin
from PyConf.Tools import (
    SimplifiedMaterialLocator,
    TrackInterpolator,
    TrackLinearExtrapolator,
    TrackMasterExtrapolator,
    TrackMasterFitter,
)
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.hlt2_tracking import (
    TrackBestTrackCreator,
    get_global_clusters_on_track_tool_no_ut,
    get_global_measurement_provider,
    make_PrStoreUTHit_empty_hits,
)
from RecoConf.legacy_rec_hlt1_tracking import (
    make_PatPV3DFuture_pvs,
    make_reco_pvs,
    make_RetinaClusters,
)
from RecoConf.options import options

make_reco_pvs.global_bind(make_pvs_from_velo_tracks=make_PatPV3DFuture_pvs)


def make_my_sequence():
    odin = make_odin()
    data = [odin]

    with get_global_measurement_provider.bind(
        ignoreUT=True,
        velo_hits=make_RetinaClusters,
        ignoreFT=True,
        ignoreMuon=True,
        ut_hits=make_PrStoreUTHit_empty_hits,
    ):
        # Make a mask to check if unbiassed hit efficiencies are also calculated correctly
        # If proper pseudo efficiencies are intended, remove the mask (and extend range of sensor in loop)
        my_mask = [
            sensor in [99] for sensor in range(208)
        ]  # sensor 99 is chosen arbitrarily

        vpClustering = VeloRetinaClusterTrackingSIMDFull(
            RawBanks=default_raw_banks("VPRetinaCluster"),
            SensorMasks=tuple(my_mask),
            MaxScatterSeeding=0.1,
            MaxScatterForwarding=0.1,
            MaxScatter3hits=0.02,
            SkipForward=3,
        )
        clusters = vpClustering.HitsLocation
        vpTracks = vpClustering.TracksLocation
        vpTracks_backwards = vpClustering.TracksBackwardLocation

        vpTracks_v1 = fromPrVeloTracksV1TracksMerger(  # converts Pr -> v1 tracks and merges forward/backward
            InputTracksLocation1=vpTracks,
            InputTracksLocation2=vpTracks_backwards,
            TrackAddClusterTool=get_global_clusters_on_track_tool_no_ut(),
        ).OutputTracksLocation

        my_TrackMasterFitter = TrackMasterFitter(
            MeasProvider=get_global_measurement_provider(),
            MaterialLocator=SimplifiedMaterialLocator(),
            Extrapolator=TrackMasterExtrapolator(
                MaterialLocator=SimplifiedMaterialLocator()
            ),
            TrackAddClusterTool=get_global_clusters_on_track_tool_no_ut(),
            MaxNumberOutliers=2,
            NumberFitIterations=10,
            FastMaterialApproximation=True,
            MaxUpdateTransports=10,
        )

        fittedTracks = TrackEventFitter(
            TracksInContainer=vpTracks_v1,
            Fitter=(my_TrackMasterFitter),
            MaxChi2DoF=2.8,
            name="TrackEventFitter_{hash}",
        ).TracksOutContainer

        bestTracks = TrackBestTrackCreator(
            name="TrackBestTrackCreator_{hash}",
            TracksInContainers=[fittedTracks],
            DoNotRefit=True,
            AddGhostProb=False,
            FitTracks=False,
            MaxChi2DoF=2.8,
        ).TracksOutContainer

        trackExtrapolator = TrackLinearExtrapolator()
        trackInterpolator = TrackInterpolator(Extrapolator=trackExtrapolator)

        # do track selection
        tracks_selection = TrackListRefiner(
            inputLocation=bestTracks,
            Code=F.require_all(
                F.NVPHITS >= 4,
                F.P >= 0,  # Here to make it explicit
                F.PT >= 0,
            ),
        ).outputLocation
        filtered_tracks = TrackSelectionToContainer(
            name="TrackSelectionToContainer_{hash}", InputLocation=tracks_selection
        ).OutputLocation

        # Loop over sensors. Subset of all sensors at the moment for testing
        for sensor_under_study in [20, 21, 22, 23, 96, 97, 98, 99, 100]:
            my_vp_efficiency_alg_TMF = VPHitEfficiencyMonitor(
                name="VPHitEfficiencyMonitorSensor_{0}".format(sensor_under_study),
                TrackLocation=filtered_tracks,
                PrVPHitsLocation=clusters,
                MaxTrackCov=100.0,
                SensorUnderStudy=sensor_under_study,
                Interpolator=trackInterpolator,
                Extrapolator=trackExtrapolator,
            )

            data += [my_vp_efficiency_alg_TMF]

    return Reconstruction("hlt2_hit_eff_reco", data, [])


with default_VeloCluster_source.bind(bank_type="VPRetinaCluster"):
    run_reconstruction(options, make_my_sequence)
