###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Allen.config import run_allen_reconstruction
from AllenConf.primary_vertex_reconstruction import make_pvs
from AllenConf.velo_reconstruction import decode_velo, make_velo_tracks
from PyConf.Algorithms import GaudiAllenPVsToPrimaryVertexContainer, VertexCompare
from RecoConf.config import Reconstruction
from RecoConf.legacy_rec_hlt1_tracking import (
    all_velo_track_types,
    make_PatPV3DFuture_pvs,
    make_TrackBeamLineVertexFinderSoA_pvs,
)
from RecoConf.options import options


def run_VertexCompare():
    # reconstruct Allen velo tracks
    decoded_velo = decode_velo()
    velo_tracks = make_velo_tracks(decoded_velo)

    # reconstruct Allen PVs
    pvs = make_pvs(velo_tracks, velo_open=False)

    # convert Allen PVs to PrimaryVertexContainer object
    pv_container_allen = GaudiAllenPVsToPrimaryVertexContainer(
        number_of_multivertex=pvs["dev_number_of_multi_final_vertices"],
        reconstructed_multi_pvs=pvs["dev_multi_final_vertices"],
    ).OutputPVs

    hlt1_tracks = all_velo_track_types()
    pvs_tblv = make_TrackBeamLineVertexFinderSoA_pvs(hlt1_tracks, None)
    pvs_patpv = make_PatPV3DFuture_pvs(hlt1_tracks, None)

    with VertexCompare.bind(produceNtuple=False):
        vertex_compare = [
            VertexCompare(
                inputVerticesName1=pv_container_allen, inputVerticesName2=pvs_tblv["v3"]
            ),
            VertexCompare(
                inputVerticesName1=pv_container_allen,
                inputVerticesName2=pvs_patpv["v3"],
            ),
            VertexCompare(
                inputVerticesName1=pvs_tblv["v3"], inputVerticesName2=pvs_patpv["v3"]
            ),
        ]
    return Reconstruction("vertex_compare", vertex_compare)


run_allen_reconstruction(options, run_VertexCompare)
