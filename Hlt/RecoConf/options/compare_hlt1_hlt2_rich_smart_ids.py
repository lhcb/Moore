###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Allen.config import setup_allen_non_event_data_service
from AllenConf.rich_reconstruction import decode_rich
from PyConf.Algorithms import TestAllenRichPixels
from PyConf.application import configure, configure_input
from PyConf.control_flow import CompositeNode, NodeLogic
from RecoConf.options import options
from RecoConf.rich_reconstruction import default_rich_reco_options, make_rich_pixels

options.evt_max = 100
config = configure_input(options)
non_event_data_node = setup_allen_non_event_data_service()
allen_rich1_decoding_rich = decode_rich(rich=1)
allen_rich2_decoding_rich = decode_rich(rich=2)
reco_opts = default_rich_reco_options()
rec_rich_pixels = make_rich_pixels(options=reco_opts)

test_hits = TestAllenRichPixels(
    rich1_smart_ids=allen_rich1_decoding_rich["dev_smart_ids"],
    rich2_smart_ids=allen_rich2_decoding_rich["dev_smart_ids"],
    RichDecodedData=rec_rich_pixels["RichDecodedData"],
)

cf_node = CompositeNode(
    "compare_rich_smart_ids",
    combine_logic=NodeLogic.NONLAZY_OR,
    children=[non_event_data_node, test_hits],
    force_order=True,
)

config.update(configure(options, cf_node))
