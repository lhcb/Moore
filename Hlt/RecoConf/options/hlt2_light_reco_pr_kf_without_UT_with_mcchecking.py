###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os

from Allen.config import run_allen_reconstruction
from PyConf.Algorithms import VertexCompare
from RecoConf.mc_checking import check_track_resolution
from RecoConf.options import options
from RecoConf.standalone import standalone_hlt2_light_reco_without_UT

isQMTTest = "QMTTEST_NAME" in os.environ
myName = (
    os.environ["QMTTEST_NAME"]
    if isQMTTest
    else "hlt2_light_reco_pr_without_UT_with_mcchecking"
)

options.histo_file = options.getProp("histo_file") or myName + "_histos.root"
options.ntuple_file = options.getProp("ntuple_file") or myName + "_ntuples.root"

with (
    standalone_hlt2_light_reco_without_UT.bind(
        do_mc_checking=True, do_data_monitoring=True, monitor_all_tracks=True
    ),
    check_track_resolution.bind(per_hit_resolutions=True, split_per_type=True),
    VertexCompare.bind(produceNtuple=False, produceHistogram=True, monitoring=True),
):
    run_allen_reconstruction(options, standalone_hlt2_light_reco_without_UT)
