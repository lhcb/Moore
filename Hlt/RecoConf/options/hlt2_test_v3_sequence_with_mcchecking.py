###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from functools import partial

from PyConf.Algorithms import (
    GhostProbabilityAlg_Long,
    GhostProbabilityMerger,
    PrCloneKillerLongV3,
    PrCloneKillerSeedV3,
    PrKalmanFitResultMerger,
    TrackContainersMergerV3,
    fromPrSeedingTracksV1Tracks,
)
from PyConf.Algorithms import (
    PrKalmanFilter_noUT_V3Full as PrKalmanFilter,
)
from PyConf.Algorithms import (
    fromV3TrackFullWithGhostProbV1Track as fromV3TrackV1Track,
)
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.core_algorithms import make_unique_id_generator
from RecoConf.hlt2_tracking import (
    TrackMasterExtrapolator,
    get_default_hlt2_tracks_without_UT,
    get_default_out_track_types_for_light_reco,
    get_global_clusters_on_track_tool_no_ut,
    get_global_materiallocator,
    make_PrStoreSciFiHits_hits,
)
from RecoConf.legacy_rec_hlt1_tracking import (
    make_all_pvs,
    make_VeloClusterTrackingSIMD_hits,
)
from RecoConf.mc_checking import (
    get_fitted_tracks_checkers,
    get_track_checkers,
    hits_resolution_checkers,
    make_links_lhcbids_mcparticles_VP_FT,
)
from RecoConf.options import options


def test_v3_sequence(fit_forward_first=True):
    fast_reco = False

    vp_hits = make_VeloClusterTrackingSIMD_hits()
    ft_hits = make_PrStoreSciFiHits_hits()

    tracks = get_default_hlt2_tracks_without_UT()

    if fit_forward_first:
        first, name_1 = tracks["Forward"]["Pr"], "Forward"
        second, name_2 = tracks["Match"]["Pr"], "Match"

        first_ft_hits = tracks["FTHitsRes"] if fast_reco else ft_hits
        second_ft_hits = ft_hits

    else:
        first, name_1 = tracks["Match"]["Pr"], "Match"
        second, name_2 = tracks["Forward"]["Pr"], "Forward"

        second_ft_hits = tracks["FTHitsRes"] if fast_reco else ft_hits
        first_ft_hits = ft_hits

    ## not sure we should hardcode this here...
    max_chi2 = 2.8

    kf_template = partial(
        PrKalmanFilter,
        MaxChi2=max_chi2,
        MaxChi2PreOutlierRemoval=20,
        HitsVP=vp_hits,
        TrackAddClusterTool=get_global_clusters_on_track_tool_no_ut(),
        InputUniqueIDGenerator=make_unique_id_generator(),
        ReferenceExtrapolator=TrackMasterExtrapolator(
            MaterialLocator=get_global_materiallocator()
        ),
    )

    pr_kf_1 = kf_template(
        name="PrKalmanFilter" + name_1, Input=first, HitsFT=first_ft_hits
    )

    fitted_tracks_1 = pr_kf_1.OutputTracks
    partial_chi2s_1 = pr_kf_1.OutputPartialChi2s
    track_fit_results_1 = pr_kf_1.OutputTrackFitResults

    # Calculate the ghost prob
    ghostProb_1 = GhostProbabilityAlg_Long(
        InputTracksName=fitted_tracks_1,
        InputInfoName=partial_chi2s_1,
        WeightsFileName="paramfile://data/GhostProbability/hlt2_ghostprob_long.json",
    ).OutputGhostProbName

    decloned_2 = PrCloneKillerLongV3(
        name="CloneKiller" + name_2,
        TracksInContainer=second,
        TracksRefContainer=fitted_tracks_1,
    ).TracksOutContainer

    pr_kf_2 = kf_template(
        name="PrKalmanFilter" + name_2, Input=decloned_2, HitsFT=second_ft_hits
    )

    fitted_tracks_2 = pr_kf_2.OutputTracks
    partial_chi2s_2 = pr_kf_2.OutputPartialChi2s
    track_fit_results_2 = pr_kf_2.OutputTrackFitResults

    # Calculate the ghost prob
    ghostProb_2 = GhostProbabilityAlg_Long(
        InputTracksName=fitted_tracks_2,
        InputInfoName=partial_chi2s_2,
        WeightsFileName="paramfile://data/GhostProbability/hlt2_ghostprob_long.json",
    ).OutputGhostProbName

    # now merge all containers
    best_long_tracks = TrackContainersMergerV3(
        InputLocations=[fitted_tracks_1, fitted_tracks_2],
        InputUniqueIDGenerators=[make_unique_id_generator()],
    ).OutputLocation

    best_long_ghostProb = GhostProbabilityMerger(
        InputLocations=[ghostProb_1, ghostProb_2]
    ).OutputLocation

    best_long_fit_results = PrKalmanFitResultMerger(
        InputLocations=[track_fit_results_1, track_fit_results_2]
    ).OutputLocation

    seed_decloned = PrCloneKillerSeedV3(
        name="CloneKillerSeed",
        TracksInContainer=tracks["Seed"]["Pr"],
        TracksRefContainer=best_long_tracks,
    ).TracksOutContainer

    seed_decloned_v1 = fromPrSeedingTracksV1Tracks(
        InputTracksLocation=seed_decloned,
        TrackAddClusterTool=get_global_clusters_on_track_tool_no_ut(),
    ).OutputTracksLocation

    best_long_v1 = fromV3TrackV1Track(
        InputTracks=best_long_tracks,
        InputGhostProbs=best_long_ghostProb,
        InputTrackFitResults=best_long_fit_results,
        TrackAddClusterTool=get_global_clusters_on_track_tool_no_ut(),
    ).OutputTracks

    hlt2_tracks = {
        "BestLong": best_long_v1,
        "SeedDecloned": seed_decloned_v1,
        "BestLongGhostProb": best_long_ghostProb,
        "BestLongFitResults": best_long_fit_results,
    }

    track_dict = get_default_hlt2_tracks_without_UT()

    for trType in hlt2_tracks.keys():
        track_dict[trType] = {"v1": hlt2_tracks[trType]}

    # ######################################################################
    # ## Do some MC checking
    # ## copied from standalone.py
    # ######################################################################
    out_track_types = get_default_out_track_types_for_light_reco(skip_UT=True)

    best_tracks = {
        track_type: track_dict[track_type] for track_type in out_track_types["Best"]
    }

    data = [tracks["v1"] for tracks in best_tracks.values()]

    data += [
        track_dict[track_type]["v1"] for track_type in out_track_types["Unfitted4Calo"]
    ]

    pvs = make_all_pvs()
    data += [pvs["v3"]]

    types_and_locations_for_checkers = {
        "Velo": track_dict["Velo"],
        "VeloFull": track_dict["Velo"],
        "Forward": track_dict["Forward"],
        "Seed": track_dict["Seed"],
        "Match": track_dict["Match"],
    }
    data += get_track_checkers(
        types_and_locations_for_checkers,
        uthit_efficiency_types=[],
        make_links_lhcbids_mcparticles=make_links_lhcbids_mcparticles_VP_FT,
    )
    data += get_fitted_tracks_checkers(
        best_tracks,
        fitted_track_types=["BestLong"],
        make_links_lhcbids_mcparticles=make_links_lhcbids_mcparticles_VP_FT,
        with_UT=False,
    )
    data += hits_resolution_checkers(with_UT=False)

    return Reconstruction("test_v3_sequence", data)


run_reconstruction(options, test_v3_sequence)
