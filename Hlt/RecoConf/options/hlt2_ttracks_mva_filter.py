###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.event_filters import require_gec
from RecoConf.hlt2_global_reco import make_fastest_reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction
from RecoConf.options import options
from RecoConf.reconstruction_objects import (
    reconstruction as reco_objects_reconstruction,
)


def standalone_mva_ttracks():
    prefilters = [require_gec()]
    filtered_tracks = reco_objects_reconstruction()["AllTrackHandles"][
        "FilteredSeedDeclonedFitted"
    ]["v1"]
    data = [filtered_tracks]
    return Reconstruction("standalone_mva_ttracks", data, prefilters)


# Run with serial processing to allow muon ID
options.n_event_slots = 1
options.n_threads = 1
with (
    reco_objects_reconstruction.bind(from_file=False),
    hlt2_reconstruction.bind(make_reconstruction=make_fastest_reconstruction),
):
    run_reconstruction(options, standalone_mva_ttracks)
