###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from DDDB.CheckDD4Hep import UseDD4Hep
from RecoConf.config import run_reconstruction
from RecoConf.options import options
from RecoConf.standalone import standalone_hlt1_reco_velo_only

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc

    dd4hepSvc = DD4hepSvc()
    dd4hepSvc.DetectorList = ["/world", "VP"]

run_reconstruction(options, standalone_hlt1_reco_velo_only)
