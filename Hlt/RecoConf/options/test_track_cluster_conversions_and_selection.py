###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from functools import partial

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from PyConf.Algorithms import (
    FunctionalChargedProtoParticleMaker,
    PrStorePrUTHits,
    SelectFTClustersFromParticles,
    SelectFTClustersFromTracks,
    SelectVPMicroClustersFromParticles,
    SelectVPMicroClustersFromTracks,
    TestOverlappingFTLiteClusters,
    TestOverlappingUTHits,
    TestOverlappingVPLightClusters,
    VPLightClustersToVPMicroClustersConverter,
    VPMicroClustersToVPLightClustersConverter,
)
from PyConf.application import default_raw_banks
from RecoConf.algorithms_thor import ParticleCombiner
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.hlt2_tracking import make_hlt2_tracks_without_UT
from RecoConf.legacy_rec_hlt1_tracking import (
    make_FTRawBankDecoder_clusters,
    make_PrStorePrUTHits_hits,
    make_VPClus_hits,
)
from RecoConf.options import options
from RecoConf.reconstruction_objects import make_pvs, reconstruction
from RecoConf.standard_particles import _make_particles, get_all_track_selector


def make_protos(tracks):
    charged_protos = FunctionalChargedProtoParticleMaker(Inputs=[tracks])

    return charged_protos.Output


def make_muons(make_protos_function):
    """creates LHCb::Particles from LHCb::ProtoParticles of tracks"""
    return _make_particles(
        species="muon",
        get_track_selector=get_all_track_selector,
        make_protoparticles=make_protos_function,
    )


def make_dimuons(
    muons,
    name="test_dimuon_selection",
    descriptor="J/psi(1S) -> mu+ mu-",
    am_min=1700.0 * MeV,
    am_max=5000.0 * MeV,
    pt_min=500.0 * MeV,
    doca_max=0.15 * mm,
):
    pvs = make_pvs()

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.DOCA(1, 2) < doca_max,
        (
            (F.CHILD(1, F.PT) > 500 * MeV)
            & (F.CHILD(1, F.P) > 5 * GeV)
            & (F.CHILD(1, F.MINIP(pvs)) > 0.05 * mm)
        ),
        (
            (F.CHILD(2, F.PT) > 500 * MeV)
            & (F.CHILD(2, F.P) > 5 * GeV)
            & (F.CHILD(1, F.MINIP(pvs)) > 0.05 * mm)
        ),
        F.PT > pt_min,
    )

    vertex_code = F.require_all(F.PT > pt_min, F.MASS < am_max, F.MASS > am_min)

    return ParticleCombiner(
        name=name,
        Inputs=[muons, muons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def test_cluster_selection_and_conversion_machinery():
    # Start with producing some particles
    hlt2_tracks = make_hlt2_tracks_without_UT()
    my_long_tracks = hlt2_tracks["BestLong"]["v1"]
    muons = make_muons(partial(make_protos, my_long_tracks))

    data = []

    velo_micro_clusters_on_tracks = SelectVPMicroClustersFromTracks(
        Inputs=[my_long_tracks]
    )
    scifi_clusters_on_tracks = SelectFTClustersFromTracks(Inputs=[my_long_tracks])
    vp_light_clusters_on_tracks = VPMicroClustersToVPLightClustersConverter(
        InputClusters=velo_micro_clusters_on_tracks
    )

    # now convert the tracks into *basic* particles, and get the clusters from them.
    velo_micro_clusters_on_particles = SelectVPMicroClustersFromParticles(
        Inputs=[muons]
    )
    scifi_clusters_on_particles = SelectFTClustersFromParticles(Inputs=[muons])
    vp_light_clusters_on_particles = VPMicroClustersToVPLightClustersConverter(
        InputClusters=velo_micro_clusters_on_particles
    )

    # First, we check the containers we get are identical.
    test_VP_converter_consistency_track_particle = TestOverlappingVPLightClusters(
        name="TestVPTrackToParticle",
        VPLightClusters_A=vp_light_clusters_on_tracks,
        VPLightClusters_B=vp_light_clusters_on_particles,
    )
    test_VP_converter_consistency_particle_track = TestOverlappingVPLightClusters(
        name="TestVPParticleToTrack",
        VPLightClusters_A=vp_light_clusters_on_particles,
        VPLightClusters_B=vp_light_clusters_on_tracks,
    )
    data += [
        test_VP_converter_consistency_track_particle,
        test_VP_converter_consistency_particle_track,
    ]

    test_FT_converter_consistency_track_particle = TestOverlappingFTLiteClusters(
        name="TestFTTrackToParticle",
        FTLiteClusters_A=scifi_clusters_on_tracks,
        FTLiteClusters_B=scifi_clusters_on_particles,
    )
    test_FT_converter_consistency_particle_track = TestOverlappingFTLiteClusters(
        name="TestFTParticleToTrack",
        FTLiteClusters_A=scifi_clusters_on_particles,
        FTLiteClusters_B=scifi_clusters_on_tracks,
    )
    data += [
        test_FT_converter_consistency_track_particle,
        test_FT_converter_consistency_particle_track,
    ]

    # Now the second phase: test whether the clusters found on the tracks
    # indeed are also part of the normal cluster containers (i.e. they are not
    # filled with bogus)
    """ Check the SciFi clusters """
    scifi_clusters = make_FTRawBankDecoder_clusters()

    test_scifi_clusters_converter = TestOverlappingFTLiteClusters(
        FTLiteClusters_A=scifi_clusters_on_particles, FTLiteClusters_B=scifi_clusters
    )
    data += [test_scifi_clusters_converter]
    """ VP clusters """
    vp_light_clusters = make_VPClus_hits()

    test_vp_consistency = TestOverlappingVPLightClusters(
        VPLightClusters_A=vp_light_clusters_on_particles,
        VPLightClusters_B=vp_light_clusters,
    )
    data += [test_vp_consistency]

    # test as well how it handles composites
    with reconstruction.bind(from_file=False):
        flat_dimuons = make_dimuons(muons)

    velo_clusters_on_dimuons = SelectVPMicroClustersFromParticles(Inputs=[flat_dimuons])
    scifi_clusters_on_dimuons = SelectFTClustersFromParticles(Inputs=[flat_dimuons])
    data += [velo_clusters_on_dimuons, scifi_clusters_on_dimuons]

    # now test the converters
    # consistency check: are the doubly converted clusters the same?
    # test it by running over *all* clusters
    all_vp_micro_clusters = VPLightClustersToVPMicroClustersConverter(
        InputClusters=vp_light_clusters
    )
    doubly_converted_clusters = VPMicroClustersToVPLightClustersConverter(
        InputClusters=all_vp_micro_clusters
    )
    test_converter_consistency_A = TestOverlappingVPLightClusters(
        VPLightClusters_A=doubly_converted_clusters, VPLightClusters_B=vp_light_clusters
    )
    test_converter_consistency_B = TestOverlappingVPLightClusters(
        VPLightClusters_A=vp_light_clusters, VPLightClusters_B=doubly_converted_clusters
    )

    data += [test_converter_consistency_A, test_converter_consistency_B]

    # test the direct decoding of Pr UT Hits vs the
    # path via the converters
    default_pr_ut_hits = make_PrStorePrUTHits_hits()
    make_raw = default_raw_banks

    original_hits = PrStorePrUTHits(
        UTRawBank=make_raw("UT"),
        UTErrorRawBank=make_raw("UTError"),
        UseUTBanks=True,
        UseUTErrorBanks=True,
        isCluster=True,
        positionMethod="AdcWeighting",
        stripMax=128,
        AssumeSorted=True,
    )

    hit_comparison = TestOverlappingUTHits(
        UTHits_A=default_pr_ut_hits, UTHits_B=original_hits, OutputLevel=1
    )

    data += [hit_comparison]

    return Reconstruction("test_clusters", data)


from RecoConf.decoders import default_VeloCluster_source

with (
    default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),
    reconstruction.bind(from_file=False),
):
    run_reconstruction(options, test_cluster_selection_and_conversion_machinery)
