###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os

from Allen.config import allen_non_event_data_config, run_allen_reconstruction
from DDDB.CheckDD4Hep import UseDD4Hep
from PyConf.Algorithms import VPRetinaFullClustering
from RecoConf.config import Reconstruction
from RecoConf.hlt1_allen import (
    combine_raw_banks_with_MC_data_for_standalone_Allen_checkers,
)
from RecoConf.legacy_rec_hlt1_tracking import (
    make_RetinaCluster_raw_bank,
    make_RetinaClusters,
    make_velo_full_clusters,
)
from RecoConf.mc_checking import tracker_dumper
from RecoConf.options import options

base_dir = os.getcwd() + "/dump/"
conddb_tag = options.conddb_tag.replace("/", "_")
if not UseDD4Hep:
    dddb_tag = options.dddb_tag.replace("/", "_")
    geom_dir = base_dir + "geometry_" + dddb_tag + "_" + conddb_tag
else:
    geom_dir = base_dir + "geometry_" + conddb_tag
mdf_dir = base_dir + "mdf_" + conddb_tag + "/"

for idir in [mdf_dir, geom_dir]:
    if not os.path.exists(idir):
        os.makedirs(idir)
# cleanup possible remnants of previous runs of the test
# without this, the test may fail for missing counters as
# the dump won't actually be ran
for f in os.listdir(geom_dir):
    fullpath = os.path.join(geom_dir, f)
    if os.path.isfile(fullpath) and f.endswith(".bin"):
        os.remove(fullpath)

mdf_file = mdf_dir + "dumped_file.mdf"

options.output_type = "MDF"
options.output_file = mdf_file
with_retina_clusters = True
options.evt_max = 500


def dump_mdf():
    algs = combine_raw_banks_with_MC_data_for_standalone_Allen_checkers(
        output_file=options.output_file
    )
    return Reconstruction("write_mdf", algs)


if with_retina_clusters:
    with (
        allen_non_event_data_config.bind(dump_geometry=True, out_dir=geom_dir),
        make_RetinaClusters.bind(make_raw=make_RetinaCluster_raw_bank),
        make_velo_full_clusters.bind(make_full_cluster=VPRetinaFullClustering),
        tracker_dumper.bind(velo_hits=make_RetinaClusters),
    ):
        run_allen_reconstruction(options, dump_mdf)
else:
    with allen_non_event_data_config.bind(dump_geometry=True, out_dir=geom_dir):
        run_allen_reconstruction(options, dump_mdf)
