###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Allen.config import setup_allen_non_event_data_service
from AllenConf.ut_reconstruction import decode_ut
from AllenCore.generator import get_constants
from PyConf.Algorithms import (
    CompareRecAllenUTHits,
    GaudiAllenTransformAllenRecUTHits,
    MCCheckRecAllenUTHits,
)
from PyConf.application import configure, configure_input
from PyConf.control_flow import CompositeNode, NodeLogic
from RecoConf.data_from_file import mc_unpacker
from RecoConf.legacy_rec_hlt1_tracking import make_PrStorePrUTHits_hits
from RecoConf.options import options

# from Gaudi.Configuration import DEBUG
# options.output_level = DEBUG

config = configure_input(options)
non_event_data_node = setup_allen_non_event_data_service()

# 0 = AdcWeighting, 1 = GeoWeighting for Allen
settings = [
    ("Adc", "AdcWeighting", 0, 4),
    ("Adc", "AdcWeighting", 0, 128),
    ("Geo", "GeoWeighting", 1, 4),
    ("Geo", "GeoWeighting", 1, 128),
]

test_hits_nodes = list()
# Get Allen constants for UT hits layer offset
constants = get_constants()
for weighting_method, rec_weighting, allen_weighting, max_cluster_size in settings:
    allen_decoded_ut = decode_ut(
        cluster_ut_hits=True,
        position_method=allen_weighting,
        max_cluster_size=max_cluster_size,
    )

    rec_ut_hits = make_PrStorePrUTHits_hits(
        isCluster=True, positionMethod=rec_weighting, stripMax=max_cluster_size
    )

    transformed_ut_hits = GaudiAllenTransformAllenRecUTHits(
        ut_hit_offsets=allen_decoded_ut["dev_ut_hit_offsets"],
        ut_hits=allen_decoded_ut["dev_ut_hits"],
        UTHitsLocation=rec_ut_hits,
        allen_constants=constants,
    )

    mc_check_hits = MCCheckRecAllenUTHits(
        allen_ut_hits=transformed_ut_hits.allen_ut_hits,
        rec_ut_hits=transformed_ut_hits.rec_ut_hits,
        UnpackedUTHits=mc_unpacker("MCUTHits"),
        WeightingMethod=weighting_method,
        MaxClusterSize=max_cluster_size,
    )

    compare_allen_rec_hits = CompareRecAllenUTHits(
        allen_ut_hits=transformed_ut_hits.allen_ut_hits,
        rec_ut_hits=transformed_ut_hits.rec_ut_hits,
        WeightingMethod=weighting_method,
        MaxClusterSize=max_cluster_size,
    )

    test_hits_nodes.append(mc_check_hits)
    test_hits_nodes.append(compare_allen_rec_hits)

# No clustering checker

allen_decoded_ut = decode_ut(cluster_ut_hits=False)
rec_ut_hits = make_PrStorePrUTHits_hits(isCluster=False)

transformed_ut_hits = GaudiAllenTransformAllenRecUTHits(
    ut_hit_offsets=allen_decoded_ut["dev_ut_hit_offsets"],
    ut_hits=allen_decoded_ut["dev_ut_hits"],
    UTHitsLocation=rec_ut_hits,
    allen_constants=constants,
)

mc_check_hits = MCCheckRecAllenUTHits(
    allen_ut_hits=transformed_ut_hits.allen_ut_hits,
    rec_ut_hits=transformed_ut_hits.rec_ut_hits,
    UnpackedUTHits=mc_unpacker("MCUTHits"),
    WeightingMethod="No",
    MaxClusterSize="1",
)

compare_allen_rec_hits = CompareRecAllenUTHits(
    allen_ut_hits=transformed_ut_hits.allen_ut_hits,
    rec_ut_hits=transformed_ut_hits.rec_ut_hits,
    WeightingMethod="No",
    MaxClusterSize="1",
)

test_hits_nodes.append(mc_check_hits)
test_hits_nodes.append(compare_allen_rec_hits)

cf_node = CompositeNode(
    "compare_ut_hits_with_mcchecking",
    combine_logic=NodeLogic.NONLAZY_OR,
    children=[non_event_data_node] + test_hits_nodes,
    force_order=True,
)
config.update(configure(options, cf_node))
