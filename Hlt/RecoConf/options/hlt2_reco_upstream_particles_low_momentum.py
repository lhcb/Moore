###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf.Algorithms import PrVeloUT
from PyConf.application import configure, configure_input
from PyConf.control_flow import CompositeNode, NodeLogic
from RecoConf.hlt2_global_reco import make_light_reco_pr_kf
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction
from RecoConf.options import options
from RecoConf.reconstruction_objects import reconstruction, upfront_reconstruction
from RecoConf.standard_particles import (
    make_has_rich_up_kaons,
    make_has_rich_up_pions,
    make_has_rich_up_protons,
    make_up_electrons_no_brem,
    make_up_kaons,
    make_up_muons,
    make_up_pions,
    make_up_protons,
)


def make_upstream_particles():
    upstream_electrons = make_up_electrons_no_brem()
    upstream_pions = make_up_pions()
    upstream_pions_with_rich = make_has_rich_up_pions()
    upstream_kaons = make_up_kaons()
    upstream_kaons_with_rich = make_has_rich_up_kaons()
    upstream_protons = make_up_protons()
    upstream_protons_with_rich = make_has_rich_up_protons()
    upstream_muons = make_up_muons()
    recoChildren = upfront_reconstruction()
    recoChildren += [
        upstream_electrons,
        upstream_pions,
        upstream_pions_with_rich,
        upstream_kaons,
        upstream_kaons_with_rich,
        upstream_protons,
        upstream_protons_with_rich,
        upstream_muons,
    ]

    return CompositeNode(
        "everything",
        children=recoChildren,
        combine_logic=NodeLogic.LAZY_AND,
        force_order=True,
    )


with (
    hlt2_reconstruction.bind(make_reconstruction=make_light_reco_pr_kf),
    reconstruction.bind(from_file=False),
):
    config = configure_input(options)
    top_cf_node = make_upstream_particles()
    config.update(configure(options, top_cf_node))
