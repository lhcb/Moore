###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Configurables import LHCbApp
from DDDB.CheckDD4Hep import UseDD4Hep

if UseDD4Hep:
    # https://gitlab.cern.ch/lhcb-conddb/lhcb-conditions-database/-/merge_requests/26
    # from RecoConf.options import options
    # options.conddb_tag = ...
    # does not work with DD4Hep
    # see https://gitlab.cern.ch/lhcb/LHCb/-/issues/234
    LHCbApp().CondDBtag = "jonrob/all-pmts-active"
