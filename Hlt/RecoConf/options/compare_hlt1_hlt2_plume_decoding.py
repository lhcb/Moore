###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Allen.config import setup_allen_non_event_data_service
from AllenConf.plume_reconstruction import decode_plume
from Moore import options
from PyConf.Algorithms import CompareRecAllenPlume, PlumeRawToDigits
from PyConf.application import configure, configure_input, default_raw_banks, make_odin
from PyConf.control_flow import CompositeNode, NodeLogic

config = configure_input(options)
non_event_data_node = setup_allen_non_event_data_service()
allen_decoded_plume = decode_plume()
read_all_channels = True

odin = make_odin()

rec_plume_adc = PlumeRawToDigits(
    ReadAllChannels=read_all_channels, RawBankLocation=default_raw_banks("Plume")
).Output

test_hits = CompareRecAllenPlume(
    plume_digits_Allen=allen_decoded_plume["dev_plume"],
    plume_digits_Moore=rec_plume_adc,
)

cf_node = CompositeNode(
    "compare_plume_adc",
    combine_logic=NodeLogic.NONLAZY_OR,
    children=[non_event_data_node, test_hits],
    force_order=True,
)
config.update(configure(options, cf_node))
