###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Allen.config import run_allen_reconstruction
from RecoConf.hlt2_global_reco import (
    make_fastest_reconstruction,
    reconstruction,
)
from RecoConf.options import options
from RecoConf.standalone import standalone_hlt2_global_reco

with reconstruction.bind(make_reconstruction=make_fastest_reconstruction):
    run_allen_reconstruction(options, standalone_hlt2_global_reco)
