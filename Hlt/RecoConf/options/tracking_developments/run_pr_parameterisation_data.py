###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PRConfig.TestFileDB import test_file_db
from PyConf.Algorithms import PrParameterisationData
from PyConf.reading import get_mc_track_info
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.data_from_file import mc_unpacker
from RecoConf.options import options

"""This is an example of how to run the data producer for pattern reconstruction
parameterisations.
See https://gitlab.cern.ch/gunther/prforwardtracking-parametrisation-tuner/-/tree/master for
more info on the parametersiations.
"""

options.evt_max = -1
n_files_per_cat = 1
polarity = "MU"
options.ntuple_file = f"data/param_data_{polarity}.root"
input_files = (
    (
        test_file_db["upgrade_DC19_01_Bs2JPsiPhi_MD"].filenames[:n_files_per_cat]
        if polarity == "MD"
        else test_file_db["upgrade_DC19_01_Bs2JpsiPhiMU"].filenames[:n_files_per_cat]
    )
    + test_file_db[f"upgrade_DC19_01_Bs2PhiPhi{polarity}"].filenames[:n_files_per_cat]
    + test_file_db[f"upgrade_DC19_01_Z2mumu{polarity}"].filenames[:n_files_per_cat]
    + test_file_db[f"upgrade_DC19_01_Bd2Dstmumu{polarity}"].filenames[:n_files_per_cat]
    + test_file_db[f"upgrade_DC19_01_Dst2D0pi{polarity}"].filenames[:n_files_per_cat]
    + test_file_db[f"upgrade_DC19_01_Bd2Kstee{polarity}"].filenames[:n_files_per_cat]
    + test_file_db[f"upgrade_DC19_01_Dp2KSPip_{polarity}"].filenames[:n_files_per_cat]
)
options.input_files = input_files
options.input_type = "ROOT"
options.set_conds_from_testfiledb(f"upgrade_DC19_01_Dst2D0pi{polarity}")


def run_tracking_param_debug(options):
    param_data = PrParameterisationData(
        MCParticles=mc_unpacker("MCParticles"),
        MCVPHits=mc_unpacker("MCVPHits"),
        MCFTHits=mc_unpacker("MCFTHits"),
        zRef=8520.0,
        TrackInfo=get_mc_track_info(),
    )

    data = [param_data]

    return Reconstruction("run_tracking_debug", data)


run_reconstruction(options, run_tracking_param_debug)
