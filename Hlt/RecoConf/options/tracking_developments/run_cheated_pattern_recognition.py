###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import (
    LHCb__Converters__Track__v1__fromV2TrackV1Track as FromV2TrackV1Track,
)
from PyConf.Algorithms import (
    PrCheatedLongTracking,
    PrCheatedSciFiTracking,
    TrackBestTrackCreator,
    fromPrSeedingTracksV1Tracks,
)
from PyConf.reading import get_mc_header, get_mc_track_info
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.data_from_file import boole_links_digits_mcparticles, mc_unpacker
from RecoConf.hlt2_cheated_tracking import make_cheatedUpstream_tracks
from RecoConf.hlt2_tracking import (
    get_global_clusters_on_track_tool_no_ut,
    get_track_master_fitter,
    make_PrStoreSciFiHits_hits,
)
from RecoConf.legacy_rec_hlt1_tracking import (
    all_upstream_track_types,
    make_legacy_rec_hlt1_tracks,
)
from RecoConf.mc_checking import (
    get_track_checkers,
    make_default_IdealStateCreator,
    make_links_lhcbids_mcparticles_tracking_system,
    make_links_tracks_mcparticles,
)
from RecoConf.options import options


def run_cheated():
    links_to_hits = make_links_lhcbids_mcparticles_tracking_system()

    cheated_long_tracking = PrCheatedLongTracking(
        LHCbIdLinkLocation=links_to_hits,
        FTHitsLocation=make_PrStoreSciFiHits_hits(),
        MCParticleLocation=mc_unpacker("MCParticles"),
        MCVerticesLocation=mc_unpacker("MCVertices"),
        MCPropertyLocation=get_mc_track_info(),
        IdealStateCreator=make_default_IdealStateCreator(),
        AddIdealStates=True,
    )

    cheated_long_tracks_v2 = cheated_long_tracking.OutputName
    cheated_long_tracks_v1 = FromV2TrackV1Track(
        InputTracksName=cheated_long_tracks_v2
    ).OutputTracksName
    # Add the seeding

    cheated_seed_tracking_pr = PrCheatedSciFiTracking(
        FTHitsLocation=make_PrStoreSciFiHits_hits(),  # from legacy_rec_hlt1_tracking
        MCParticleLocation=mc_unpacker("MCParticles"),
        MCPropertyLocation=get_mc_track_info(),
        LinkLocation=boole_links_digits_mcparticles("FTLiteClusters"),
    ).OutputName

    cheated_seed_tracking_v1 = fromPrSeedingTracksV1Tracks(
        InputTracksLocation=cheated_seed_tracking_pr,
        TrackAddClusterTool=get_global_clusters_on_track_tool_no_ut(),
    ).OutputTracksLocation

    cheated_upstream_tracks = all_upstream_track_types(
        velo_tracks=make_legacy_rec_hlt1_tracks()["Velo"],
        make_velo_ut_tracks=make_cheatedUpstream_tracks,
    )

    best_long_tracks = TrackBestTrackCreator(
        TracksInContainers=[cheated_long_tracks_v1],
        Fitter=get_track_master_fitter(),
        DoNotRefit=False,
        AddGhostProb=False,
        FitTracks=True,
    ).TracksOutContainer

    best_seed_tracks = TrackBestTrackCreator(
        TracksInContainers=[cheated_seed_tracking_v1],
        Fitter=get_track_master_fitter(),
        DoNotRefit=False,
        AddGhostProb=False,
        FitTracks=True,
    ).TracksOutContainer

    # add MCLinking to the (fitted) V1 tracks
    links_to_long_tracks = make_links_tracks_mcparticles(
        InputTracks=best_long_tracks, LinksToLHCbIDs=links_to_hits
    )
    links_to_seed_tracks = make_links_tracks_mcparticles(
        InputTracks=best_seed_tracks, LinksToLHCbIDs=links_to_hits
    )

    # Now it's showtime: resolutions
    from PyConf.Algorithms import TrackResChecker
    from PyConf.Tools import VisPrimVertTool

    mcpart = mc_unpacker("MCParticles")
    mchead = get_mc_header()
    mcprop = get_mc_track_info()
    res_long_checker = TrackResChecker(
        TracksInContainer=best_long_tracks,
        MCParticleInContainer=mc_unpacker("MCParticles"),
        LinkerInTable=links_to_long_tracks,
        StateCreator=make_default_IdealStateCreator(),
        VisPrimVertTool=VisPrimVertTool(
            public=True, MCHeader=mchead, MCParticles=mcpart, MCProperties=mcprop
        ),
    )
    res_seed_checker = TrackResChecker(
        TracksInContainer=best_seed_tracks,
        MCParticleInContainer=mc_unpacker("MCParticles"),
        LinkerInTable=links_to_seed_tracks,
        StateCreator=make_default_IdealStateCreator(),
        VisPrimVertTool=VisPrimVertTool(
            public=True, MCHeader=mchead, MCParticles=mcpart, MCProperties=mcprop
        ),
    )

    data = [res_long_checker, res_seed_checker]
    types_and_locations_for_checkers = {
        "Seed": cheated_seed_tracking_v1,
        "Upstream": cheated_upstream_tracks,
    }
    data += get_track_checkers(types_and_locations_for_checkers)

    return Reconstruction("hlt2_cheated_pattern_reco", data)


run_reconstruction(options, run_cheated)
