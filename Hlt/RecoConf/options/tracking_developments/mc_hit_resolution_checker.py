###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.event_filters import require_gec
from RecoConf.mc_checking import (
    fthits_resolution_checker,
    uthits_resolution_checker,
    vphits_resolution_checker,
)
from RecoConf.options import options

options.histo_file = "PrHitsChecker.root"


def hits_checker():
    return Reconstruction(
        "prhitschecker",
        [
            vphits_resolution_checker(),
            uthits_resolution_checker(),
            fthits_resolution_checker(),
        ],
        [require_gec()],
    )


run_reconstruction(options, hits_checker)
