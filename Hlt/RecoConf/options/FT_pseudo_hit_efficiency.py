###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf.Algorithms import (
    FTHitEfficiencyMonitor,
    FTPseudoHitEfficiencyMonitor,
    PrKalmanFilter_noUT,
    PrKalmanFilter_Seed,
    PrKalmanFilter_Velo,
    PrStoreSciFiHits,
)
from PyConf.application import make_odin
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.hlt2_tracking import make_hlt2_tracks_without_UT
from RecoConf.legacy_rec_hlt1_tracking import make_FTRawBankDecoder_clusters
from RecoConf.muonid import make_muon_hits
from RecoConf.options import options

# save output file with histograms
options.histo_file = (
    options.getProp("histo_file") or "bias_hlt2_FT_pseudo_hit_efficiency.root"
)


def hit_efficiency_sequence():
    odin = make_odin()
    data = [odin]
    with (
        PrKalmanFilter_noUT.bind(FillFitResult=True),
        PrKalmanFilter_Seed.bind(FillFitResult=True),
        PrKalmanFilter_Velo.bind(FillFitResult=True),
    ):
        hlt2_tracks = make_hlt2_tracks_without_UT(
            light_reco=True, fast_reco=False, use_pr_kf=True
        )

        all_FT_pr_hits = PrStoreSciFiHits(
            HitsLocation=make_FTRawBankDecoder_clusters()
        ).Output

        my_ft_efficiency_alg = FTPseudoHitEfficiencyMonitor(
            name="FTPseudoHitEfficiency",
            TrackLocation=hlt2_tracks["BestLong"]["v1"],
            PrFTHitsLocation=all_FT_pr_hits,
            LayersUnderStudy=list(range(12)),
            MaxDoca=2.0,
            MinTrackP=3000,
            MinTrackPT=300,
        )

        data += [my_ft_efficiency_alg]

    return Reconstruction("hlt2_pseudo_hit_eff_reco", data, [])


with (
    default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),
    make_muon_hits.bind(geometry_version=3),
):
    run_reconstruction(options, hit_efficiency_sequence)
