###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Hlt/Moore
---------
#]=======================================================================]

gaudi_install(PYTHON)

gaudi_add_tests(pytest ${CMAKE_CURRENT_SOURCE_DIR}/python)
gaudi_add_tests(QMTest)

if(NOT USE_DD4HEP)
    # disable DD4HEP specific tests
    set_property(
        TEST
            Moore.test_lbexec_hlt2_pp_commissioning_data
            Moore.test_lbexec_spruce_pp_2024_noflagging_mc
            Moore.starterkit.interactive-dst
        PROPERTY
            DISABLED TRUE
    )
endif()

# these 3 tests are ran in a sequence. The first one is broken in the sense that
# it provides an empty output. The last one thus fails now with PyConf. At the
# time they were written they did not use PyConf and thus the error went unnoticed
# as the last test was happily running on 0 events. They need to be fixed and
# reenabled
set_property(
    TEST
        Moore.test_lbexec_hlt2_pp_2022_reprocessing_data
        Moore.test_lbexec_hlt2_pp_2022_reprocessing_data_check
        Moore.test_lbexec_hlt2_pp_2022_reprocessing_data_check_output
    PROPERTY
        DISABLED TRUE
)

if (BINARY_TAG MATCHES ".*cuda.*")
    # When compiling for GPU, Gaudi-Allen algorithm wrappers call the device algorithms
    # since these are incompatible with calling from Moore / Gaudi, disable Gaudi-Allen tests
    get_property(moore_tests DIRECTORY PROPERTY TESTS)
    foreach(test IN ITEMS ${moore_tests})
        if(test MATCHES "")
            set_property(TEST ${test} PROPERTY DISABLED TRUE)
        endif()
    endforeach()
endif()
