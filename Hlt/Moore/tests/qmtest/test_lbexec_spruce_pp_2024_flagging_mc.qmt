<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
Test that Spruce can run from LHCbDirac and that it produces the expected outputs in flagging mode
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="prerequisites"><set>
  <tuple><text>test_lbexec_hlt2_pp_2024_flagging_mc</text><enumeral>PASS</enumeral></tuple>
</set></argument>
<argument name="program"><text>lbexec</text></argument>
<argument name="timeout"><integer>1000</integer></argument>
<argument name="args"><set>
  <text>Moore.production:spruce</text>
  <text>$HLT2CONFROOT/options/spruce_pp_2024_flagging_mc.yaml</text>
  <text>--</text>
  <text>--settings=Sprucing_production_physics_pp_Collision24c2</text>
  <text>--flagging</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="validator"><text>

from Moore.qmtest.exclusions import remove_known_warnings
countErrorLines({"FATAL": 0, "ERROR": 0, "WARNING": 0},
                stdout=remove_known_warnings(stdout))


findReferenceBlock("""
HLTControlFlowMgr                      INFO StateTree: CFNode   #executed  #passed
LAZY_AND: moore                                                                                                    #=20      Sum=20          Eff=|( 100.0000 +- 0.00000 )%|
 NONLAZY_OR: lines                                                                                                 #=20      Sum=20          Eff=|( 100.0000 +- 0.00000 )%|
  NONLAZY_OR: hlt_decision                                                                                         #=20      Sum=20          Eff=|( 100.0000 +- 0.00000 )%|
   LAZY_AND: SpruceB2CC_BsToJpsiKstarWideDecisionWithOutput                                                        #=20      Sum=0           Eff=|( 0.000000 +- 0.00000 )%|
    LAZY_AND: SpruceB2CC_BsToJpsiKstarWide                                                                         #=20      Sum=0           Eff=|( 0.000000 +- 0.00000 )%|
 """)


import xml.etree.ElementTree as ET
from pathlib import Path

# Ensure the summary XML is as expected
tree = ET.parse(Path.cwd() / "spruce_pp_2024_summary.xml")
out_files = tree.findall("./output/file")
in_files = tree.findall("./input/file")
assert len(in_files) == 1, in_files
assert len(out_files) == 1, out_files
in_file = in_files[0]
out_file = out_files[0]
print(f"sizes : {int(in_file.text)}, {int(out_file.text)}")
assert int(in_file.text) ==  int(out_file.text), "Should retain all events"
assert out_file.attrib["name"] == "PFN:spruce_pp_2024_flagging_mc.dst"

# Ensure the pool XML was updated to contain the new output file
tree = ET.parse(Path.cwd() / "hlt2_pool_xml_catalog.xml")
catalog_output = tree.findall(f'./File/physical/pfn[@name="spruce_pp_2024_flagging_mc.dst"]')
assert len(catalog_output) == 1, catalog_output

</text></argument>
</extension>
