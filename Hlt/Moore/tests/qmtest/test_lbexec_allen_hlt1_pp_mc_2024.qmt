<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
Check that Allen can be called from lb-exec successfully
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="prerequisites"><set>
  <tuple><text>test_prepare_lbexec_hlt</text><enumeral>PASS</enumeral></tuple>
</set></argument>
<argument name="program"><text>lbexec</text></argument>
<argument name="args"><set>
  <text>--override-option-class=Moore.LbExec:TestOptions</text>
  <text>Moore.production:hlt1</text>
  <text>$HLT1CONFROOT/options/allen_hlt1_pp_2024_production_options.yaml</text>
  <text>--</text>
  <text>--sequence=hlt1_pp_matching</text>
  <text>--flagging</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="validator"><text>

from Moore.qmtest.exclusions import remove_known_warnings
countErrorLines({"FATAL": 0, "ERROR": 0, "WARNING": 0},
                stdout=remove_known_warnings(stdout))

import xml.etree.ElementTree as ET
from pathlib import Path

# Ensure the summary XML is as expected
tree = ET.parse(Path.cwd() / "hlt1_pp_2024_summary.xml")
out_files = tree.findall("./output/file")
assert len(out_files) == 1, out_files
out_file = out_files[0]
assert int(out_file.text) &gt; 0, "at least one event should be selected"
assert int(out_file.text) &lt; 30, "at least one event should be rejected"
assert out_file.attrib["name"] == "PFN:allen_hlt1_pp_2024_production.dst"

# Ensure the pool XML was updated to contain the new output file
tree = ET.parse(Path.cwd() / "hlt1_pool_xml_catalog.xml")
catalog_output = tree.findall(f'./File/physical/pfn[@name="allen_hlt1_pp_2024_production.dst"]')
assert len(catalog_output) == 1, catalog_output

</text></argument>
</extension>
