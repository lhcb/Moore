###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os

from Moore import options

options.evt_max = 100
# for meaningfull resolution plot larger statistics is needed: comment out the line above (the test will process ~40K events and take a few hours)

options.set_input_and_conds_from_testfiledb("expected_2024_BdToKstgamma_xdigi")

isQMTTest = "QMTTEST_NAME" in os.environ
myName = (
    os.environ["QMTTEST_NAME"]
    if isQMTTest
    else "default_input_and_conds_hlt2_BdToKstgamma"
)

options.histo_file = os.path.expandvars("histos_" + myName + ".root")
options.ntuple_file = os.path.expandvars("ntuple_" + myName + ".root")
