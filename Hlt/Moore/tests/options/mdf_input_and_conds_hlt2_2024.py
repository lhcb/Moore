###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from RecoConf.decoders import default_VeloCluster_source

# Use velo retina decoding:
default_VeloCluster_source.global_bind(bank_type="VPRetinaCluster")

options.set_conds_from_testfiledb("expected_2024_minbias_xdigi")
# As dddb_tag a branch with a newer particle table version is used here,
# as some new hlt2 lines rely on new/renamed particles only introduced in dddb-20240427.
# This workaround is temporary and can be removed, once newer simulation samples,
# based on dddb-20240427 directly, are available for these tests.
options.dddb_tag = "upgrade/dddb-20231017-new-particle-table"

options.event_store = "EvtStoreSvc"

options.input_files = ["allen_mdf_write_for_hlt2.mdf"]
options.input_type = "MDF"
options.persistreco_version = 1.0
