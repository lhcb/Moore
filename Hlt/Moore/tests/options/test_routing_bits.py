###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse

from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    default_raw_event,
)
from PyConf.control_flow import CompositeNode


def routing_bits(rbbanks):
    """Return a list with the 96 routing bit values."""
    assert rbbanks.size() == 1  # stop if we have multiple rb banks
    d = rbbanks[0].data()  # get only bank
    bits = "{:032b}{:032b}{:032b}".format(d[2], d[1], d[0])
    return list(map(int, reversed(bits)))


parser = argparse.ArgumentParser()
parser.add_argument("--input-mdf", help="Input MDF file")
args = parser.parse_args()

options = ApplicationOptions(_enabled=False)
options.gaudipython_mode = True
options.input_files = [args.input_mdf]
options.input_type = "MDF"
options.data_type = "Upgrade"
options.simulation = True
options.dddb_tag = "dddb-20231017"
options.conddb_tag = "sim-20231017-vc-mu100"
options.geometry_version = "run3/trunk"
options.conditions_version = "jonrob/all-pmts-active"
config = configure_input(options)

rawEvent = default_raw_event("HltDecReports")
config.update(configure(options, CompositeNode("TopSeq", children=[rawEvent])))

import GaudiPython

gaudi = GaudiPython.AppMgr()
TES = gaudi.evtsvc()
gaudi.run(1)
LHCb = GaudiPython.gbl.LHCb

found_events = bool(TES["/Event"])
found_set_routing_bit = False
found_routing_bits_rawbank = False
while TES["/Event/DAQ"]:
    rawevent = TES["/Event/DAQ/RawEvent"]
    rbbanks = rawevent.banks(LHCb.RawBank.HltRoutingBits)
    if rbbanks.size() > 0:
        found_routing_bits_rawbank = True

        if any(rb == 1 for rb in routing_bits(rbbanks)):
            found_set_routing_bit = True

    gaudi.run(1)

if not found_events:
    print("Test ERROR No events found.")
if not found_routing_bits_rawbank:
    print("Test ERROR No routing bits raw bank found in any event.")
if not found_set_routing_bit:
    print("Test ERROR No routing bit set in any event.")
