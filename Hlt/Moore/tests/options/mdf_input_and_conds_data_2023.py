###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from PyConf.application import metainfo_repos

options.set_input_and_conds_from_testfiledb("2023_raw_hlt1_269939")
options.evt_max = 500

metainfo_repos.global_bind(extra_central_tags=["commissioning"])
