###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Check the contents of DstData, DecReports and SelReports rawbanks

Check that the same sourceID does not appear twice.

"""

import argparse

from PyConf.Algorithms import HltDecReportsDecoder, HltRoutingBitsFilter
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    default_raw_banks,
    default_raw_event,
    make_odin,
)
from PyConf.control_flow import CompositeNode

parser = argparse.ArgumentParser()
parser.add_argument("--type", help="Input type")
parser.add_argument("--input", help="Input file")
parser.add_argument("--events", default=10000, help="Number of events")

args = parser.parse_args()

options = ApplicationOptions(_enabled=False)
options.gaudipython_mode = True
options.input_files = [args.input]
options.input_type = args.type
options.data_type = "Upgrade"
options.simulation = True
options.dddb_tag = "dddb-20231017"
options.conddb_tag = "sim-20231017-vc-mu100"
options.geometry_version = "run3/trunk"
options.conditions_version = "jonrob/all-pmts-active"
options.histo_file = "hlt1_hlt2_decreports.root"
config = configure_input(options)

raw_event = default_raw_event("ODIN")
raw_odin = default_raw_banks("ODIN")
odin = make_odin()
decReport = default_raw_banks("HltDecReports")
dstData = default_raw_banks("DstData")
selReport = default_raw_banks("HltSelReports")
routingbits = default_raw_banks("HltRoutingBits")
hlt1_decreports = HltDecReportsDecoder(
    name="Hlt1DecReportsDecoder", RawBanks=decReport, SourceID="Hlt1"
)
hlt2_decreports = HltDecReportsDecoder(
    name="Hlt2DecReportsDecoder", RawBanks=decReport, SourceID="Hlt2"
)
phys_filter = HltRoutingBitsFilter(
    name="PhysFilter", RawBanks=routingbits, RequireMask=(0, 0, 1 << (95 - 64))
)

afterPhyFilter = CompositeNode(
    "UnpackAfterPhysFilter",
    [
        phys_filter,
        decReport,
        selReport,
        dstData,
        hlt1_decreports,
        hlt2_decreports,
    ],
)
alg = [raw_event, raw_odin, odin, routingbits, afterPhyFilter]
config.update(configure(options, CompositeNode("TopSeq", children=alg)))

import GaudiPython

LHCb = GaudiPython.gbl.LHCb

gaudi = GaudiPython.AppMgr()
TES = gaudi.evtSvc()

n = 0


def check_unique_sourceids(location):
    source_ids = []
    if TES[location].getData().size() == 0:
        error(f"{location} has 0 banks")
        return False

    for rb in TES[location].getData():
        source_ids.append(rb.sourceID())

    has_unique_sourceids = 0 == (len(source_ids) - len(set(source_ids)))
    return has_unique_sourceids


def routing_bits(rbbanks):
    """Return a list with the routing bits that are set."""
    (rbbank,) = rbbanks  # stop if we have multiple rb banks
    d = rbbank.data()
    bits = "{:032b}{:032b}{:032b}".format(d[2], d[1], d[0])
    ordered = list(map(int, reversed(bits)))
    return [bit for bit, value in enumerate(ordered) if value]


def error(msg):
    print("CheckOutput ERROR", msg)


while n < int(args.events):
    gaudi.run(1)
    if not TES["/Event/DAQ"]:
        break
    raw_event = TES["/Event/DAQ/RawEvent"]

    if not TES[odin.location]:
        error("No ODIN")

    on_bits = routing_bits(raw_event.banks(LHCb.RawBank.HltRoutingBits))
    if 95 not in on_bits:  # this must be a lumi exclusive event
        if 94 not in on_bits:
            error("Bit 94 must be set when 95 is not set")
        # there are no other banks, so nothing more to check
        continue

    unique_decreports_ids = check_unique_sourceids(decReport.location)
    if not unique_decreports_ids:
        error("Multiple DecReports banks with same SourceID")

    unique_dstdata_ids = check_unique_sourceids(dstData.location)
    if not unique_dstdata_ids:
        error("Multiple DstData banks with same SourceID")

    unique_selreports_ids = check_unique_sourceids(selReport.location)
    if not unique_selreports_ids:
        error("Multiple SelReports banks with same SourceID")

    unique_odin = check_unique_sourceids(raw_odin.location)
    if not unique_odin:
        error("Multiple ODIN banks with same SourceID")

    if TES[routingbits.location].getData().size() != 1:
        error("There should be exactly one routing bits bank")

    n = n + 1
