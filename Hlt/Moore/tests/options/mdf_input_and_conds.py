###############################################################################
# (c) Copyright 2019-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options

options.set_input_and_conds_from_testfiledb("expected_2024_min_bias_mdf")
options.input_files = sorted(list(set(options.input_files)))  # remove dups
options.evt_max = 1000
options.event_store = "EvtStoreSvc"
