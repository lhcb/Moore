###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#####
## part I
#####
from DDDB.CheckDD4Hep import UseDD4Hep
from Moore import options
from PyConf.application import metainfo_repos

options.set_input_and_conds_from_testfiledb("2022_raw_hlt1_253597")

# Note DetDesc requires simulation to be false.
# As dddb_tag a branch with a newer particle table version is used here,
# as some new hlt2 lines rely on new/renamed particles only introduced in dddb-20240427.
# This workaround is temporary and can be removed, once newer simulation samples,
# based on dddb-20240427 directly, are available for these tests.
options.simulation = not UseDD4Hep
metainfo_repos.global_bind(extra_central_tags=["commissioning"])

options.evt_max = 500
