###############################################################################
# (c) Copyright 2022-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from contextlib import contextmanager
from enum import Enum
from typing import Optional

from GaudiConf.LbExec import Options as DefaultOptions
from GaudiConf.LbExec import TestOptionsBase
from PyConf.application import ROOT_KEY
from PyConf.packing import persistreco_version
from PyConf.reading import reconstruction as reconstruction_reading
from PyConf.reading import tes_root
from PyConf.reading import upfront_reconstruction as upfront_reconstruction_reading
from RecoConf.reconstruction_objects import reconstruction


class ProcessTypes(str, Enum):
    Hlt1 = "Hlt1"
    Hlt2 = "Hlt2"
    TurboPass = "TurboPass"
    TurboSpruce = "TurboSpruce"
    Spruce = "Spruce"
    ReconstructionOnly = "ReconstructionOnly"
    Monitoring = "Monitoring"
    UserDefined = "UserDefined"


class Options(DefaultOptions):
    tck: Optional[int] = None
    process: Optional[ProcessTypes] = None

    @contextmanager
    def apply_binds(self):
        """
        This function configures the following functions, where their keyword
        arguments are globally bound to the user-specified values.
        This way users do not have to manually configure these functions themselves.
        - reconstruction() from reconstruction_objects
        - reconstruction() from PyConf.reading
        - upfront_reconstruction() from PyConf.reading

        For reference the Sprucing is configured through 3 options using lbexec
        - `options.input_process` is set to "Hlt2" for the FSR sink https://gitlab.cern.ch/lhcb/LHCb/-/blob/master/PyConf/python/PyConf/application.py?ref_type=heads#L1156
        - `options.process` is set to "Spruce" to control the reconstruction control flow (ie. get it from input file). This is not required if set to "TurboPass"
        - `process` is set to "spruce" or "pass" based on the lines to run for overall Moore control flow in config.py https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Moore/python/Moore/config.py#L298
        """
        if self.input_process:
            reconstruction_reading.global_bind(input_process=self.input_process)
            tes_root.global_bind(input_process=self.input_process)

            reconstruction_reading.global_bind(
                simulation=self.simulation and self.input_type == ROOT_KEY
            )
            upfront_reconstruction_reading.global_bind(
                simulation=self.simulation and self.input_type == ROOT_KEY
            )

        persistreco_version.global_bind(version=self.persistreco_version)

        if (
            self.process == ProcessTypes.Spruce
            or self.process == ProcessTypes.TurboSpruce
            or self.process == ProcessTypes.TurboPass
        ):
            reconstruction.global_bind(spruce=True, from_file=True)

        with super().apply_binds():
            yield


class TestOptions(Options, TestOptionsBase):
    """Specialized Options class for Moore Tests"""

    pass
