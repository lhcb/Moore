###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test persistreco when using real time reco."""

from Hlt2Conf.lines.topological_b import threebody_line, twobody_line
from Moore import Options, run_moore
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.hlt2_global_reco import (
    make_light_reco_pr_kf,
    make_light_reco_pr_kf_without_UT,
)
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction
from RecoConf.reconstruction_objects import reconstruction


def make_lines():
    return [twobody_line(persistreco=True), threebody_line(persistreco=True)]


def run_hlt2(options: Options):
    skipUT = False
    my_reco = make_light_reco_pr_kf_without_UT if skipUT else make_light_reco_pr_kf

    public_tools = [stateProvider_with_simplified_geom()]
    with (
        hlt2_reconstruction.bind(make_reconstruction=my_reco),
        reconstruction.bind(from_file=False),
        default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),
    ):
        return run_moore(options, make_lines, public_tools, exclude_incompatible=False)
