###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Configuration to run LHCbIntegrationTest's FSR_DecodingKeys.
"""

from Hlt2Conf.lines.topological_b import threebody_line, twobody_line
from Moore import Options, run_moore
from RecoConf.decoders import default_ft_decoding_version
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

ft_decoding_version = 2
default_ft_decoding_version.global_bind(value=ft_decoding_version)

from RecoConf.calorimeter_reconstruction import make_digits

make_digits.global_bind(calo_raw_bank=False)


def hlt2_2_or_3body_topo(options: Options):
    def make_lines():
        return [twobody_line(persistreco=True), threebody_line(persistreco=True)]

    public_tools = [stateProvider_with_simplified_geom()]
    with reconstruction.bind(from_file=False):
        return run_moore(options, make_lines, public_tools, exclude_incompatible=False)
