###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Hlt2Conf.lines.b_to_charmonia.hlt2_b2cc import all_lines
from Moore import Options, run_moore
from RecoConf.reconstruction_objects import reconstruction


def main(options: Options):
    def make_lines():
        return [builder() for builder in all_lines.values()]

    with reconstruction.bind(from_file=False):
        return run_moore(options, make_lines)
