###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import logging

log = logging.getLogger(__name__)

import copy
from collections import OrderedDict
from itertools import chain

from PyConf.control_flow import CompositeNode
from PyConf.dataflow import DataHandle

from .tools import getInputs, isSelectionAlg


def loopLineAlgIterations(oldAlgs):
    """
    This function takes algorithms as input and creates a list with all level of iteration through the data flow where the algorithm occurs.

    Inputs: oldAlgs
    Outputs: lineAlgorithmIterations
    """

    lineAlgorithmIterations = {}
    loopLine = True
    currentList = oldAlgs
    iteration = 0
    while loopLine:
        nextList = []
        for algorithm in currentList:
            if isinstance(algorithm, DataHandle):
                algorithm = algorithm.producer

            lineAlgorithmIterations[algorithm] = iteration

            if isinstance(algorithm, CompositeNode):
                for i in algorithm.children:
                    nextList.append(i)
            else:
                inputs = getInputs(algorithm)
                for i in inputs:
                    nextList.append(i)

        if sum([val == 0 for val in lineAlgorithmIterations.values()]) == 1:
            if all([alg in lineAlgorithmIterations.keys() for alg in oldAlgs]):
                loopLine = False
        if not nextList:
            loopLine = False
        currentList = list(OrderedDict.fromkeys(nextList))
        iteration += 1

    return lineAlgorithmIterations


def separateAlgs(oldAlgs):
    """
    This function takes old algorithm list as input and creates three lists,
    one with all algorithms before selections,
    one with all selection algorithms,
    one with all algorithms after selections.
    Current implementation assumes only selection algorithms are DataHandles.
    """

    # Separating selection algorithms from pre and post filters
    preSelAlgs = []
    oldSelAlgs = []
    postSelAlgs = []
    beforeSel = True
    afterSel = False
    particleAlgNames = [
        "ParticleContainerMerger",
        "BodyCombiner",
        "ParticleRangeFilter",
    ]
    # TODO Create a flag in all selection objects so filtering on typename is not needed.
    for alg in oldAlgs:
        if isinstance(alg, DataHandle):
            alg = alg.producer

        if not isinstance(alg, CompositeNode) and any(
            [name in alg.typename for name in particleAlgNames]
        ):
            if afterSel:
                # All selection algorithms are not together as one in the control flow
                # This case is currently not handled
                # Returning empty lists to stop control flow optimization
                return [], [], []
            beforeSel = False
            oldSelAlgs.append(alg)
        else:
            if beforeSel:
                preSelAlgs.append(alg)
            else:
                afterSel = True
                postSelAlgs.append(alg)

    return preSelAlgs, oldSelAlgs, postSelAlgs


def loopDataFlow(name, oldSelAlgs, optimization="default"):
    """
    This functions loops through the data flow and picks one algorithm per iteration to add to the control flow
    """
    particleAlgNames = [
        "ParticleContainerMerger",
        "BodyCombiner",
        "ParticleRangeFilter",
    ]

    mainAlg = getFinalSelAlg(oldSelAlgs)

    if not mainAlg:
        return []

    loopVar = True
    if any(algName in mainAlg.typename for algName in particleAlgNames):
        newAlgs = [mainAlg]
        currentAlg = mainAlg

        # Looping back through data flow
        iteration = 0
        while loopVar:
            iteration += 1
            inputs = [alg for alg in getInputs(currentAlg) if isSelectionAlg(alg)]
            if not inputs:
                loopVar = False
                break
            inputAlgs = [
                alg.producer if isinstance(alg, DataHandle) else alg for alg in inputs
            ]

            # If algorithm is a ContainerMerger we want to loop past its input to see if we can find common algorithm further into the line
            # otherwise we stop here
            if currentAlg.typename == "ParticleContainerMerger":
                inputAlgs, loopVar = loopPastMerger(inputAlgs, loopVar)
                if loopVar is False:
                    break
            if optimization == "default":
                if inputAlgs:
                    currentAlg = inputAlgs[0]
                    newAlgs = [currentAlg] + newAlgs
                else:
                    loopVar = False

    else:
        print(f"Skipping line: {name}, main algorithm type not recognised")
        return oldSelAlgs
    return newAlgs


def newAlgsChecks(newCFlowAlgs, oldAlgs):
    """
    This function performs checks on optimized control flow and returns False in case the old one should be used.
    """
    particleAlgNames = [
        "ParticleContainerMerger",
        "BodyCombiner",
        "ParticleRangeFilter",
    ]

    # Check if algs differ that are not selection algs: Filter, Combiner, Merger
    newCFlowAlgsProducers = [
        alg.producer if isinstance(alg, DataHandle) else alg for alg in newCFlowAlgs
    ]
    oldAlgsProducers = [
        alg.producer if isinstance(alg, DataHandle) else alg for alg in oldAlgs
    ]
    oldAlgsInNew = [alg in newCFlowAlgsProducers for alg in oldAlgsProducers]
    if sum(oldAlgsInNew) < len(oldAlgsProducers):
        diffAlgs = [alg for (alg, b) in zip(oldAlgsProducers, oldAlgsInNew) if not b]
        for alg in diffAlgs:
            if not any([algName in alg.typename for algName in particleAlgNames]):
                print(
                    f"Difference in control flow that is not a nominal selection alg: {alg}, Skipping line"
                )
                return False

    return True


def loopPastMerger(inputAlgs, loopVar):
    """
    This function loops past ParticleContainerMergers to try to find common algorithms further into the data flow.
    """
    inputAlgsList = []
    # Looping past Merger inputs
    # inputAlgsList will be a list of inputs for all of the Merger's inputs
    for i in inputAlgs:
        if i.typename == "ParticleContainerMerger":
            # If a merger has a merger as input we can treat its inputs as the intial merger's
            inputs2 = [alg for alg in getInputs(i)]
            for alg in inputs2:
                inputAlgsList.append(tuple(getInputs(alg)))
        else:
            inputAlgsList.append(tuple(getInputs(i)))
    # If the inputs are all the same
    if len(set(inputAlgsList)) == 1:
        inputAlgs = [
            alg.producer if isinstance(alg, DataHandle) else alg
            for alg in inputAlgsList[0]
        ]
    else:
        # If the inputs are not the same we loop deeper in the data flow to find common alg
        loopThroughMerger = True
        loopThroughList = [list(algtuple) for algtuple in inputAlgsList]
        newLoopAlgs = loopThroughList
        while loopThroughMerger:
            inputAlgs = []

            # Flattened and filtered list of input algorithms
            # chain.from_iterable flattens the list
            flatFilteredInputAlgs = list(
                OrderedDict.fromkeys((chain.from_iterable(loopThroughList)))
            )
            for alg in flatFilteredInputAlgs:
                # If algorithm is shared between all inputs we can add it to the control flow
                if all([alg in algList for algList in loopThroughList]):
                    if isinstance(alg, DataHandle):
                        inputAlgs.append(alg.producer)
                    else:
                        inputAlgs.append(alg)
                    loopThroughMerger = False

            if not inputAlgs:
                # This section loops back and searches for a common algorithm shared between all data flow paths

                tempNewLoopAlgs = [[] for i in loopThroughList]
                tempLoopThroughList = copy.copy(loopThroughList)
                for n in range(len(loopThroughList)):
                    if not newLoopAlgs[n]:
                        continue
                    for alg in list(OrderedDict.fromkeys(newLoopAlgs[n])):
                        if isSelectionAlg(alg):
                            if isinstance(alg, DataHandle):
                                alg = alg.producer
                            if alg.typename == "ParticleContainerMerger":
                                mergerInputAlgs = getInputs(alg)
                                for m in mergerInputAlgs:
                                    pastInputAlgs = getInputs(m)
                                    tempLoopThroughList.append(
                                        loopThroughList[n] + pastInputAlgs
                                    )
                                    tempNewLoopAlgs.append(pastInputAlgs)
                            else:
                                tempLoopThroughList[n].extend(getInputs(alg))
                                tempNewLoopAlgs[n].extend(getInputs(alg))

                loopThroughList = tempLoopThroughList
                newLoopAlgs = tempNewLoopAlgs
                if all([not algs for algs in newLoopAlgs]):
                    loopThroughMerger = False
    return inputAlgs, loopVar


def getFinalSelAlg(oldSelAlgs):
    """
    This function is used to ensure that someone has not misplaced their final selection algorithm,
    it happened for a line in the past.
    """

    # The maximum iteration an algorithm occurs at is used to find the final deciding algorithm
    if len(oldSelAlgs) > 1:
        lineAlgIters = loopLineAlgIterations(oldSelAlgs)
        mainAlg = [key for key, val in lineAlgIters.items() if val == 0]
        if mainAlg:
            return mainAlg[0]
        else:
            return []
    else:
        mainAlg = oldSelAlgs[0]
        return mainAlg
