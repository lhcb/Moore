###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import logging

log = logging.getLogger(__name__)

from PyConf import configurable

from .controlFlowOptimization import loopDataFlow, newAlgsChecks, separateAlgs
from .tools import AlgToDH


@configurable
def default_controlflow(lineName, algs):
    """
    Default control flow without using counters, will pick 0th input at every decision point.
    """

    preSelAlgs, oldSelAlgs, postSelAlgs = separateAlgs(algs)

    if not oldSelAlgs:
        log.debug("No old selection algs")
        return algs

    newAlgs = loopDataFlow(lineName, oldSelAlgs, optimization="default")
    if (not newAlgs) or (newAlgs == oldSelAlgs):
        return algs

    newAlgs = [AlgToDH(alg) for alg in newAlgs]

    newCFlowAlgs = preSelAlgs + newAlgs + postSelAlgs

    if not newAlgsChecks(newCFlowAlgs, algs):
        return algs
    else:
        return newCFlowAlgs
