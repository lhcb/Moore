###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import logging

log = logging.getLogger(__name__)
from collections import OrderedDict

from PyConf.control_flow import CompositeNode
from PyConf.dataflow import DataHandle


def AlgToDH(algorithm):
    """Function to go from algorithm to its datahandle.
    Input: DataHandle
    Output: Algorithm"""
    if not hasattr(algorithm, "outputs"):
        return algorithm
    outputs = algorithm.outputs
    if any(["Output" == o for o in outputs.keys()]):
        return outputs["Output"]
    elif "OutputParticles" in outputs.keys():
        return outputs["OutputParticles"]
    elif "Particles" in outputs.keys():
        return outputs["Particles"]
    elif "OutputContainer" in outputs.keys():
        return outputs["OutputContainer"]
    else:
        return algorithm


def getInputs(alg):
    if isinstance(alg, DataHandle):
        alg = alg.producer
    l = []
    for key, values in alg.inputs.items():
        if "Input" not in key:
            continue
        if isinstance(values, list):
            l = l + values
        else:
            l.append(values)

    return list(OrderedDict.fromkeys(l))


def isSelectionAlg(line_alg):
    if isinstance(line_alg, CompositeNode):
        return False
    producer = line_alg.producer if isinstance(line_alg, DataHandle) else line_alg
    return (
        ("ParticleContainerMerger" in producer.typename)
        or ("Combiner" in producer.typename)
        or ("ParticleRangeFilter" in producer.typename)
    )
