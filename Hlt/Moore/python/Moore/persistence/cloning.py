###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Logic for cloning MC information under ``/Event/HLT2(Spruce/)``.

This is largely a port of the Run 2 logic, up until the serialisation of the
packed containers into raw banks.

The Run 2 logic is defined in the Hlt project. Reading the `_persistRecoSeq`
method in the `HltAfterburner.py` file is a good place to start if you want to
understand more.
"""

from PyConf.Algorithms import CopyMCRelationsTargets, CopySignalMCParticles
from PyConf.location_prefix import prefix
from PyConf.reading import get_mc_particles
from PyConf.Tools import MCParticleCloner


def mc_cloners(stream, protoparticle_relations):
    """Copy signal _and_ associated MC particles to microDST-like locations.

    In the Stripping, the microDST machinery only saves a subset of the
    input MC particles and vertices. This subset contains two possibly
    overlapping contributions:

    1. The MCParticle objects in the signal decay tree; and
    2. The MCParticle objects which can be associated to the reconstructed
    objects selected by at least one stripping line.

    Args:
        stream (str): TES root under which objects will be cloned.
        protoparticle_relations (list of DataHandle): ProtoParticle-to-MCParticle relations.
    """

    #
    # The reason this is here is to get the targets of the table to be added to /Event/HLT2/MC...
    #
    # Algorithm to clone the 'to' entries in the relations tables
    # from their original location to the same location prefixed by /Event/{stream}
    copy_mcp_targets = CopyMCRelationsTargets(
        name="Copy_MC_For_"
        + "__".join(i.location.replace("/", "_") for i in protoparticle_relations),
        InputLocations=protoparticle_relations,
        MCCloner=MCParticleCloner(OutputPrefix=stream),
    )

    # Algorithm to clone all MC particles and vertices that are associated
    # to the simulated signal process (using LHCb::MCParticle::fromSignal)
    if "Spruce" in stream:
        MCParticlesLocation = "/Event/HLT2/MC/Particles"
    else:
        MCParticlesLocation = "/Event/MC/Particles"

    mc_location_mapping = dict(
        (l, prefix(l, stream))
        for l in (
            MCParticlesLocation,
            MCParticlesLocation.replace("Particles", "Vertices"),
        )
    )
    # TODO  must add those MC particles which appear in the 'to' side of the protoparticle_relations!!!
    #      and then remove the need for CopyMCRelationsTargets...
    copy_signal_mcp = CopySignalMCParticles(
        MCCloner=MCParticleCloner(OutputPrefix=stream),
        MCParticlesLocation=get_mc_particles(MCParticlesLocation),
        ExtraOutputs=list(mc_location_mapping.values()),
    )

    return ((copy_mcp_targets, copy_signal_mcp), mc_location_mapping)
