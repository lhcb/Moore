###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import logging
import os
from functools import lru_cache

from GaudiConf.reading import type_map
from PyConf.components import force_location

#: Final component of a TES location holding Particle objects
PARTICLES_LOCATION_SUFFIX = "Particles"
# Container type we match against to define 'a container of Particle objects'
PARTICLE_CONTAINER_TYPES = {
    "KeyedContainer<LHCb::Particle,Containers::KeyedObjectManager<Containers::hashmap> >",
    "SharedObjectsContainer<LHCb::Particle>",
}
# Set of locations we've forced to specific values; must keep track of these to
# ensure we never force the same location twice
_FORCED_LOCATIONS = set()

log = logging.getLogger(__name__)


def __get_type(dh):
    types = type_map()
    if dh.type in types.keys():
        return types[dh.type].replace("Selection", "")

    return None


def cloning_map():
    from PyConf.Algorithms import (
        CopyCaloClusters,
        CopyCaloHypos,
        CopyFlavourTags,
        CopyP2InfoRelations,
        CopyP2IntRelations,
        CopyP2MCPRelations,
        CopyP2VRelations,
        CopyParticles,
        CopyPP2MCPRelations,
        CopyProtoParticles,
        CopyRecVertices,
        CopyTracks,
        CopyVertices,
    )

    return {
        "Particles": CopyParticles,
        "FlavourTags": CopyFlavourTags,
        "Tracks": CopyTracks,
        "CaloHypos": CopyCaloHypos,
        "CaloClusters": CopyCaloClusters,
        "PVs": CopyRecVertices,
        "ProtoParticles": CopyProtoParticles,
        "Vertices": CopyVertices,
        "P2VRelations": CopyP2VRelations,
        "P2MCPRelations": CopyP2MCPRelations,
        "P2IntRelations": CopyP2IntRelations,
        "P2InfoRelations": CopyP2InfoRelations,
        "PP2MCPRelations": CopyPP2MCPRelations,
    }


def copy_to_final_location(input, path_components):
    """Return an algorithm that copies Particle objects.

    Args:
        input (DataHandle): input container to be copied.
        path_components (list of str): Definition of TES path, relative to
        '/Event', under which to store copied objects.
    """
    input_type = __get_type(input)
    if not input_type:
        log.warning(
            "Unsupported type {} for line output {} (for location {}); this will not be persisted".format(
                input.type, input, path_components
            )
        )
        return None
    # Path components should not contain slashes
    assert all("/" not in pc for pc in path_components)
    # We will add the suffix in this method to ensure consistency
    base = os.path.join("/Event", *path_components)
    output_loc = os.path.join(base, input_type)

    # Keep track of forced output locations to make sure we don't write to the
    # same place twice
    assert output_loc not in _FORCED_LOCATIONS, (
        "Duplicate Copy" + input_type + " instantiation"
    )
    _FORCED_LOCATIONS.add(output_loc)

    return cloning_map()[input_type](
        Input=input, outputs=dict(Output=force_location(output_loc))
    )


@lru_cache(maxsize=None)
def is_particle_producer(producer):
    """Return True if `producer` outputs an `LHCb::Particle` container.

    If an output cannot be introspected to have a type in
    `PARTICLE_CONTAINER_TYPES` a fallback is used to detect a common type of
    particle producer: descendents of `DVCommonBase`. In this case the output
    property is called `Particles` and output type cannot be deduced by the
    Python configuration framework, so is marked as "unknown_t".

    Note:
        The `Particles` property name is a convention used with Moore and
        Hlt2Conf. It is the argument name in output transforms used to
        decorate various `DVCommonBase` subclasses; see `Hlt2Conf.algorithms`
        for examples.

        If a `DVCommonBase` descendent does not follow these conventions, it
        will not be recognised by this method as a `LHCb::Particle` producer.
    """
    # First, check if the producer has outputs of the correct C++ type
    output_types = {dh.type for dh in producer.outputs.values()}
    if PARTICLE_CONTAINER_TYPES & output_types:
        return True
    # Otherwise, use a heuristic to determine if producer derives from
    # DVCommonBase, in which case it is a Particle producer
    try:
        particles = producer.Particles
        return particles.type == "unknown_t"
    except AttributeError:
        return False


def particle_output(producer):
    """Return the LHCb::Particle output of the producer algorithm.

    Raises:
        ValueError: If `producer` does not output a LHCb::Particle container.
    """
    # First assume we have a functional algorithm and can introspect the
    # output types, such that we just pick the output with the right type
    particles = [
        dh for dh in producer.outputs.values() if dh.type in PARTICLE_CONTAINER_TYPES
    ]
    if not particles:
        try:
            # The output transform for DVCommonBase algorithms in
            # Hlt2Conf.algorithms assigns the `Particles` name to the particle
            # output property, so try that next
            particles = [producer.Particles]
        except AttributeError:
            particles = []

    #    Check was first introduced by apearce here:
    #    https://gitlab.cern.ch/lhcb/Moore/-/commit/6f50581315efac1188f38fdbd4eda3b2a82d6c81
    #    But it's unclear why, so the check was removed in Moore!967 to make that MR work.
    #    Moore#320 is to follow up and investigate if check is needed.
    #    if len(particles) != 1:
    #        raise ValueError("Could not find unique LHCb::Particle outputs of {}".
    #                         format(producer))
    return particles.pop()
