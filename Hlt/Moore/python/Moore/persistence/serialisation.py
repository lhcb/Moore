###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Configuration for serialising packed containers to raw banks.

As the output format when running online, MDF, only supports writing a raw
event comprised of raw banks, serialisation is a necessary step in persisting
HLT2 output online. For the sake of uniformity we also perform the
serialisation when writing out ROOT files, e.g. during simulation productions.
"""

from Gaudi.Configuration import WARNING as OUTPUTLEVEL
from PyConf.Algorithms import HltPackedBufferWriter
from PyConf.control_flow import CompositeNode


def serialise_packed_containers(packed_handles, source_id):
    """Return CF node that serialises a set of packed containers to a raw bank.

    Args:
        packer_locations (list of str): Packed object containers to serialise.

    Returns:
        serialisation_node (CompositeNode).
        output_raw_data (DataHandle): Raw event with serialised data,
            i.e. the DstData bank.

    """
    bank_writer = HltPackedBufferWriter(
        PackedContainers=packed_handles, OutputLevel=OUTPUTLEVEL, SourceID=source_id
    )

    serialisation_cf = CompositeNode(
        "serialisation",
        children=[bank_writer],
    )

    return serialisation_cf, bank_writer
