###############################################################################
# (c) Copyright 2019-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import argparse
import json
import re
from contextlib import contextmanager

from PyConf.utilities import ConfigurationError

from Moore import Options
from Moore.lines import SpruceLine
from Moore.streams import Stream, Streams, make_default_streams


def hlt1(options: Options, *raw_args):
    """Setup Hlt1 with given options

    Arguments:
    sequence(default="hlt1_pp_default")
    flagging(default=False)
    """
    from AllenCore.gaudi_allen_generator import allen_runtime_options
    from RecoConf.hlt1_allen import allen_gaudi_config

    from Moore import run_allen
    from Moore.config import allen_control_flow

    args = _parse_args(raw_args)
    with (
        allen_gaudi_config.bind(sequence=args.sequence),
        allen_runtime_options.bind(filename="allen_monitor" + args.sequence + ".root"),
        allen_control_flow.bind(flagging=args.flagging),
    ):
        return run_allen(options)


def hlt2(options: Options, *raw_args):
    """Setup Hlt2 with given options

    Arguments:
    settings(default=None, will run all lines in one stream): to set the streams and reconstruction options
    flagging(default=False): to run MC in flagging mode
    require_deployed_trigger_key(default=False)
    flagging(default=False): to run MC in flagging mode
    persistreco(default=False): to run MC in persistreco mode (with flaggging only)
    rawbanks(default=None): to run MC persisting rawbanks (with flaggging only)
    reco_only(default=False): to run MC in reconstruction only mode (with flaggging only)
    line_regex(default=None): to select lines based on regex match
    velo_source(default="VPRetinaCluster"): Velo bank type, either VPRetinaCluster or VeloSP
    """

    args = _parse_args(raw_args)

    if args.settings:
        import importlib

        module = importlib.import_module(
            "Hlt2Conf.settings." + args.settings, package=__package__
        )
        make_streams = module.make_streams
    elif options.lines_maker:
        make_streams = options.lines_maker
    else:
        from Hlt2Conf.settings.defaults import make_all_lines

        make_streams = make_all_lines

    return _hlt2(
        options,
        make_streams,
        args.lines_regex,
        args.require_deployed_trigger_key,
        args.flagging,
        args.persistreco,
        args.rawbanks,
        args.reco_only,
        args.velo_source,
        args.without_ut,
    )


def _parse_args(raw_args):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--lines-regex",
        type=re.compile,
        help="Regex pattern to select which lines to run. ",
    )
    parser.add_argument(
        "--require-deployed-trigger-key",
        action="store_true",
        help="When enabled, it is checked that the trigger configuration has been deployed on cvmfs. "
        "In case of failure, the job will crash.",
    )
    parser.add_argument(
        "--settings",
        default=None,
        help="Which settings file to import make_streams from",
    )

    parser.add_argument(
        "--sequence", default="hlt1_pp_default", help="AllenSequence to import"
    )

    parser.add_argument(
        "--flagging", action="store_true", help="Run MC in flagging mode"
    )

    parser.add_argument(
        "--persistreco",
        action="store_true",
        help="To save reconstruction of all events. Only to be used with --flagging",
    )

    parser.add_argument(
        "--rawbanks",
        action="store_true",
        help="To keep all detector banks of all events. Only to be used with --flagging",
    )

    parser.add_argument(
        "--reco-only",
        action="store_true",
        help="To run only the reconstruction for all events. Only to be used with --flagging",
    )

    parser.add_argument("--velo-source", default=None, help="Velo cluster type")

    parser.add_argument(
        "--without-ut",
        action="store_true",
        help="if UT should be excluded for MC linking",
    )

    return parser.parse_args(raw_args)


def _hlt2(
    options,
    make_lines,
    lines_regex,
    require_deployed_trigger_key=False,
    flagging=False,
    persistreco=False,
    rawbanks=False,
    reco_only=False,
    velo_source=None,
    without_ut=False,
):
    from PyConf.application import metainfo_repos, retrieve_encoding_dictionary
    from RecoConf import mc_checking
    from RecoConf.decoders import default_VeloCluster_source
    from RecoConf.reconstruction_objects import reconstruction

    from Moore import run_moore

    if require_deployed_trigger_key:
        metainfo_repos.global_bind(repos=[])  # only use repos on cvmfs
        retrieve_encoding_dictionary.global_bind(
            require_key_present=True
        )  # require key is in repo
    if options.simulation:
        mc_checking.make_links_lhcbids_mcparticles_tracking_and_muon_system.global_bind(
            with_ut=not without_ut
        )

    # Do not configure simplified geometry as it does not exist for DD4HEP
    from RecoConf.global_tools import (
        stateProvider_with_simplified_geom,
        trackMasterExtrapolator_with_simplified_geom,
    )

    public_tools = [
        trackMasterExtrapolator_with_simplified_geom(),
        stateProvider_with_simplified_geom(),
    ]

    if velo_source:
        if velo_source in ["VeloSP", "VPRetinaCluster"]:
            default_VeloCluster_source.global_bind(bank_type=velo_source)
        else:
            raise ConfigurationError(
                "Velo bank type needs to "
                + "VeloSP or VPRetinaCluster but got "
                + velo_source
            )

    def _my_line_maker():
        return _line_maker(
            options,
            make_lines,
            lines_regex,
            flagging,
            persistreco,
            rawbanks,
            reco_only,
            process="Hlt2",
        )

    with reconstruction.bind(from_file=False), _apply_track_binds():
        config = run_moore(options, _my_line_maker, public_tools)

    if options.simulation:
        from Configurables import DeterministicPrescaler
        from Gaudi.Configuration import allConfigurables

        for n, c in allConfigurables.items():
            if isinstance(c, DeterministicPrescaler) and n.endswith("_Prescaler"):
                c.AcceptFraction = 1.0

    return config


@contextmanager
def _apply_track_binds():
    from DDDB.CheckDD4Hep import UseDD4Hep

    if UseDD4Hep:
        from PyConf.Tools import TrackMasterExtrapolator, TrackMasterFitter

        # This needs to happen before the public tools are instantiated,
        # which means we cannot put it inside make_streams().
        with (
            TrackMasterExtrapolator.bind(
                ApplyMultScattCorr=False,
                ApplyEnergyLossCorr=False,
                ApplyElectronEnergyLossCorr=False,
            ),
            TrackMasterFitter.bind(ApplyMaterialCorrections=False),
        ):
            yield
    else:
        yield


def _line_maker(
    options,
    make_lines,
    lines_regex,
    flagging=False,
    persistreco=False,
    rawbanks=None,
    reco_only=False,
    process="Hlt2",
):
    streams = make_lines()

    if lines_regex:
        for stream in streams.streams:
            filtered_lines = [
                line for line in stream.lines if lines_regex.fullmatch(line.name)
            ]
            if len(filtered_lines) < len(stream.lines):
                print(
                    f"Reduced {len(stream.lines)} lines to {len(filtered_lines)} using {lines_regex}"
                )
                stream.update(filtered_lines)

        streams.update()

    if options.simulation:
        streams = _flatten_and_deduplicate_lines(streams)
        if flagging:
            if persistreco and rawbanks:
                from Hlt2Conf.lines.mc.mc_lines import (
                    make_hlt2_passthrough_persistreco_rawbanks_line as hlt2passline,
                )
                from Hlt2Conf.lines.mc.mc_lines import (
                    make_spruce_passthrough_persistreco_rawbanks_line as sprucepassline,
                )
            elif persistreco:
                from Hlt2Conf.lines.mc.mc_lines import (
                    make_hlt2_passthrough_persistreco_line as hlt2passline,
                )
                from Hlt2Conf.lines.mc.mc_lines import (
                    make_spruce_passthrough_persistreco_line as sprucepassline,
                )
            elif rawbanks:
                from Hlt2Conf.lines.mc.mc_lines import (
                    make_hlt2_passthrough_rawbanks_line as hlt2passline,
                )
                from Hlt2Conf.lines.mc.mc_lines import (
                    make_spruce_passthrough_rawbanks_line as sprucepassline,
                )
            else:
                from Hlt2Conf.lines.mc.mc_lines import (
                    make_hlt2_passthrough_line as hlt2passline,
                )
                from Hlt2Conf.lines.mc.mc_lines import (
                    make_spruce_passthrough_line as sprucepassline,
                )

            if process == "Spruce":
                streams += [sprucepassline()]
            else:
                if not reco_only:
                    streams += [make_lines(hlt2passline)]
                else:
                    if reco_only and not persistreco:
                        raise ConfigurationError(
                            "You have requested reco_only but not persistreco. This will not give you what you want"
                        )

                    streams = [make_lines(hlt2passline)]

        if (persistreco or rawbanks or reco_only) and not flagging:
            raise ConfigurationError(
                "You have requested to use persistreco and/or rawbanks and/or reco_only but you are not running in flagging mode"
            )

    if isinstance(streams, list):
        streams = make_default_streams(streams)

    return streams


def _flatten_and_deduplicate_lines(streams):
    # Use a set comprehension to deduplicate lines
    if isinstance(streams, Streams):
        return streams.lines
    elif isinstance(streams, list):
        return streams
    else:
        raise ConfigurationError(
            "The streams object is neither an instance of Streams nor a list of lines, check configuration"
        )


def spruce(options: Options, *raw_args):
    """Setup Sprucing with given options

    Arguments:
    settings(default=None, will run all default booked lines in one stream): to set the sprucing streams from SprucingConfig
    flagging(default=False): to run MC in flagging mode
    require_deployed_trigger_key(default=False)
    line_regex(default=None): to select lines based on regex match
    persistreco(default=False): to run MC in persistreco mode (with flaggging only)
    rawbanks(default=None): to run MC persisting rawbanks (with flaggging only)
    reco_only(default=False): to run MC in reconstruction only mode (with flaggging onl
    """

    args = _parse_args(raw_args)

    def _import_module(module_name):
        import importlib

        spec = importlib.util.find_spec(module_name)
        if spec is None:
            raise ConfigurationError(
                f"The module {module_name} does not exist in SprucingConfig."
            )
        print(f"Importing {module_name}")
        module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module)
        return module

    if args.settings:
        import re

        config_match = re.findall("Collision([0-9]{2}c[0-9])", args.settings)
        if config_match:
            config = config_match[0]
        else:
            raise ConfigurationError(
                "The settings provided are invalid. A valid example is 'Sprucing_production_physics_pp_Collision24c2_MC'"
            )
        year = config[0:2]

        module_name = f"SprucingConfig.Spruce{year}.{args.settings}"
        module = _import_module(module_name)
        make_streams = module.make_excl_spruce_prod_streams

        module_name = (
            f"SprucingConfig.Spruce{year}.fullline_config.pp_Collision{config}"
        )
        module = _import_module(module_name)
        full_lines_for_TISTOS = module.full_lines

    else:
        module_name = "Hlt2Conf.sprucing_settings.Sprucing_production_pp"
        module = _import_module(module_name)
        make_streams = module.make_excl_spruce_prod_streams
        full_modules_for_TISTOS = module.full_modules_for_TISTOS
        full_lines_for_TISTOS = module.lines_for_TISTOS(full_modules_for_TISTOS)

    return _spruce(
        options,
        make_streams,
        full_lines_for_TISTOS,
        args.lines_regex,
        args.require_deployed_trigger_key,
        args.flagging,
        args.persistreco,
        args.rawbanks,
        args.reco_only,
    )


def _spruce(
    options,
    make_lines,
    full_hlt2_lines,
    lines_regex,
    require_deployed_trigger_key=False,
    flagging=False,
    persistreco=False,
    rawbanks=None,
    reco_only=False,
):
    import Functors as F
    from PyConf.Algorithms import VoidFilter
    from PyConf.application import (
        configure,
        configure_input,
        metainfo_repos,
        retrieve_encoding_dictionary,
    )
    from PyConf.control_flow import CompositeNode, NodeLogic
    from PyConf.reading import get_decreports

    from Moore import moore_control_flow, run_moore
    from Moore.persistence.hlt2_tistos import list_of_full_stream_lines

    if require_deployed_trigger_key:
        metainfo_repos.global_bind(repos=[])  # only use repos on cvmfs
        retrieve_encoding_dictionary.global_bind(
            require_key_present=True
        )  # require key is in repo

    # Do not configure simplified geometry as it does not exist for DD4HEP
    from RecoConf.global_tools import (
        stateProvider_with_simplified_geom,
        trackMasterExtrapolator_with_simplified_geom,
    )

    def _my_line_maker():
        return _line_maker(
            options,
            make_lines,
            lines_regex,
            flagging,
            persistreco,
            rawbanks,
            reco_only,
            process="Spruce",
        )

    if options.simulation:
        config = configure_input(options)
        ## Implement a filter such that exclusive Sprucing only runs on FULL stream HLT2 lines

        line_regex = "|".join(
            line for line in [line + "Decision" for line in full_hlt2_lines]
        )
        full_stream_filter = VoidFilter(
            name="full_stream_filter",
            Cut=F.DECREPORTS_RE_FILTER(
                Regex=line_regex, DecReports=get_decreports("Hlt2")
            ),
        )

        with (
            list_of_full_stream_lines.bind(lines=full_hlt2_lines),
            _apply_track_binds(),
        ):
            public_tools = [
                trackMasterExtrapolator_with_simplified_geom(),
                stateProvider_with_simplified_geom(),
            ]

            moore_control_node = moore_control_flow(
                options, _my_line_maker(), process="spruce"
            )

            ## Add this filter before `moore_control_node` using `LAZY_AND` logic IF flagging
            moore_withfilter_node = CompositeNode(
                "MC_spruce_control_node",
                combine_logic=NodeLogic.LAZY_AND,
                children=[full_stream_filter] + [moore_control_node],
                force_order=True,
            )

            if not flagging:
                moore_control_node = moore_withfilter_node
            config.update(
                configure(options, moore_control_node, public_tools=public_tools)
            )

        ## Remove any line prescales
        from Configurables import DeterministicPrescaler
        from Gaudi.Configuration import allConfigurables

        for n, c in allConfigurables.items():
            if isinstance(c, DeterministicPrescaler) and n.endswith("_Prescaler"):
                c.AcceptFraction = 1.0

        return config

    else:
        with (
            list_of_full_stream_lines.bind(lines=full_hlt2_lines),
            _apply_track_binds(),
        ):
            public_tools = [
                trackMasterExtrapolator_with_simplified_geom(),
                stateProvider_with_simplified_geom(),
            ]
            config = run_moore(options, _my_line_maker, public_tools)

        return config


def turbo_spruce(line_config, custom_prescales={}, use_regex=True, spruce_streams=None):
    """
    Creates and configures sprucing streams for HLT2 TURBO output.

    This function reads the attributes of Hlt2Lines from a dict made at the HLT2 stage. It streams these Hlt2Lines as SpruceLines based on their "stream" attribute OR by regex. If required it can apply custom prescales.

    Args:
        line_config (dict): dict made at the HLT2 stage with line attributes. This dict must have the following structure: {"line_name": {"name": "line_name", "stream": "stream_name", "prescale": 1.0} ...}. See Sprucing_tests:spruce_overlap for an example
        custom_prescales (dict, optional): A dictionary mapping line names to
            prescale factors. Defaults to empty dictionary.
        use_regex (bool, optional): If True, stream lines according to spruce_streams regex. Defaults to True.
        spruce_streams (dict, optional): A dictionary mapping stream names to regex patterns. Defaults to None.

    Returns:
        Streams: An object containing the configured sprucing streams for passthrough of TURBO output.
    """
    turbo_lines = list(line_config.keys())
    # print(f"Lines in HLT2 stream requested: {turbo_lines}")

    if use_regex:
        assert all(
            isinstance(regex_list, list) for regex_list in spruce_streams.values()
        ), (
            "The `spruce_streams` dict for use_regex=True must map stream names to regex pattern LISTS"
        )
        # Can stream lines according to regex
        streaming = {
            stream_name: [
                line
                for line in turbo_lines
                if any(re.match(regex, line) for regex in regex_list)
            ]
            for stream_name, regex_list in spruce_streams.items()
        }

    else:  # Using stream attribute in line_config to stream lines
        list_of_streams = set(
            line_config[line]["stream"] for line in line_config.keys()
        )

        ### Check that all lines have a stream
        if None in list_of_streams:
            raise ValueError("At least one line has no stream attribute assigned!!!")
        print(f"Streams found in line attributes file are : {list_of_streams}")

        # Streaming is done via exact "regex" list of lines
        streaming = {
            stream_name: [
                line
                for line in turbo_lines
                if line_config[line].get("stream") == stream_name
            ]
            for stream_name in list_of_streams
        }

    for stream_name in streaming.keys():
        print(f"Lines in {stream_name} to be spruced: {streaming[stream_name]}")

    assert sum(map(len, streaming.values())) == len(line_config.keys()), (
        f"There are {len(line_config.keys())} lines to be streamed but only {sum(map(len, streaming.values()))} lines in the streams config"
    )

    streams = [
        Stream(
            stream_name,
            lines=[
                _make_pass_spruceline(
                    line_config[line], custom_prescales=custom_prescales
                )
                for line in streaming[stream_name]
            ],
            detectors=[],
        )
        for stream_name in streaming
    ]

    my_streams = Streams(streams=streams)
    # my_streams.print()
    return my_streams


def _make_pass_spruceline(line_attributes, custom_prescales={}):
    """
    Create a SpruceLine for a given Hlt2Line.

    Args:
        line_attributes (dict): Attributes for line.
        custom_prescales (dict, optional): A dictionary mapping lines to custom prescales. Defaults to empty dictionary.

    Returns:
        SpruceLine: The SpruceLine with the attributes of the matching Hlt2Line.
    """
    from PyConf.reading import get_particles, upfront_decoder

    linename = line_attributes["name"]
    filter = f"{linename}Decision"
    location = f"/Event/HLT2/{linename}/Particles"

    # print(
    #     f"Line {linename} produces candidates at /Event/Turbo/{spruce_linename}/Particles"
    # )

    line_attributes["prescale"] = custom_prescales.get(linename, 1.0)

    with upfront_decoder.bind(source="Hlt2"):
        hlt2_particles = get_particles(location)
        algs = []
        # Some lines do not output anything to `/Event/HLT2/{hlt2_linename}/Particles`
        if line_attributes["produces_particles"]:
            algs.append(hlt2_particles)
        else:
            print(
                f"Line {linename} did not produce location `/Event/HLT2/{linename}/Particles`, skipping this location"
            )

        extra_outputs_tuplelist = []
        # get extra_outputs - note might be several of them
        if line_attributes["extra_outputs"]:
            for loc in line_attributes["extra_outputs"]:
                # print(f"Adding extra_output {loc} for line {spruce_linename}")
                extra_outputs_tuplelist.append(
                    (loc, get_particles(f"/Event/HLT2/{linename}/{loc}/Particles"))
                )
        pass_spruceline = SpruceLine(
            name=linename,
            hlt2_filter_code=filter,
            algs=algs,
            extra_outputs=extra_outputs_tuplelist,
            prescale=line_attributes["prescale"],
            persistreco=line_attributes["persistreco"],
            tagging_particles=line_attributes["tagging_particles"],
            calo_digits=line_attributes["calo_digits"],
            calo_clusters=line_attributes["calo_clusters"],
            pv_tracks=line_attributes["pv_tracks"],
            track_ancestors=line_attributes["track_ancestors"],
            raw_banks=line_attributes["raw_banks"],
            stream=line_attributes["stream"],
        )
    return pass_spruceline
