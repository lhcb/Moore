###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import logging

from PyConf import configurable
from PyConf.Algorithms import (
    ExecutionReportsWriter,
    Hlt__RoutingBitsCombiner,
    Hlt__RoutingBitsWriter,
    HltDecReportsWriter,
    HltSelReportsWriter,
)
from PyConf.application import make_odin
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.filecontent_metadata import (
    generate_encoding_dictionary,
    register_encoding_dictionary,
)

from .selreports import make_selreports
from .streams import get_bank_by_sourceid, get_default_routing_bits

log = logging.getLogger(__name__)


@configurable
def report_writers_nodes(streams, process):
    """Return the control flow node for report writers

    Args:
        streams (dict of stream : DecisionLines): control flow nodes of lines
        process (str): "hlt1", "hlt2", "spruce" or "pass"
    Returns:
        node (CompositeNode): Output creation control flow.
        dec_reports(HltDecReports): Decision reports
        hlt_report_banks (list): Raw data to write out per bank type.
        routing_bits(dict stream name: list of routing bit banks):  Routing bit bank for Hlt1/2
    """
    algs = []

    # We will write the reports to raw banks at these locations
    source_id = {"hlt1": "Hlt1", "hlt2": "Hlt2", "spruce": "Spruce", "pass": "Spruce"}[
        process
    ]
    major_name = {
        "hlt1": "Hlt1SelectionID",
        "hlt2": "Hlt2SelectionID",
        "spruce": "SpruceSelectionID",
        "pass": "SpruceSelectionID",
    }[process]

    dec_key = int(
        register_encoding_dictionary(
            major_name,
            generate_encoding_dictionary(
                major_name, [l.decision_name for l in streams.lines]
            ),
        ),
        16,
    )  # TODO unsigned? Stick to hex string?

    ##spruce and passthrough jobs will write a Spruce report
    erw = ExecutionReportsWriter(
        Persist=[line.name for line in streams.lines],
        ANNSvcKey=major_name,
        TCK=dec_key,  # TODO unsigned? Stick to hex string?
    )
    drw = HltDecReportsWriter(
        SourceID=source_id,
        InputHltDecReportsLocation=erw.DecReportsLocation,
        EncodingKey=dec_key,
    )

    algs.extend([erw, drw])
    # this is the new dec report for this process
    hlt_report_banks = [drw.OutputView]

    # There can be only one routing bits bank per stream, so need to combine hlt1 and hlt2
    routing_bits = {}
    if process == "hlt1" or process == "hlt2":
        for stream in streams.streams:
            rbw_writer = Hlt__RoutingBitsWriter(
                RoutingBits=get_default_routing_bits(stream),
                DecReports=erw.DecReportsLocation,
                ODIN=make_odin(),
            )

            routing_bits[stream.name] = [rbw_writer.OutputView]
            if process == "hlt2":
                rbw_merger = Hlt__RoutingBitsCombiner(
                    Hlt1Bits=get_bank_by_sourceid("Hlt1", "HltRoutingBits"),
                    Hlt2Bits=rbw_writer.OutputView,
                )
                routing_bits[stream.name] = [rbw_merger.OutputView]

    # For hlt1, make sel reports
    if process == "hlt1":
        srm = make_selreports(process, streams.lines, erw)
        srw = HltSelReportsWriter(
            SourceID=source_id,
            DecReports=erw.DecReportsLocation,
            SelReports=srm.SelReports,
            ObjectSummaries=srm.ObjectSummaries,
            EncodingKey=srm.properties["EncodingKey"],
        )
        algs.append(srw)
        hlt_report_banks += [srw.OutputView]

    node = CompositeNode(
        "report_writers",
        combine_logic=NodeLogic.NONLAZY_OR,
        children=algs,
        force_order=True,
    )

    return node, erw, hlt_report_banks, routing_bits
