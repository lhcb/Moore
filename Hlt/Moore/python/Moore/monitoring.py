###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re
from math import pi

import Functors as F
from GaudiKernel.SystemOfUnits import GeV
from GaudiKernel.SystemOfUnits import picosecond as ps
from PyConf import configurable
from RecoConf.reconstruction_objects import make_pvs, make_rec_summary
from SelAlgorithms.monitoring import histogram_1d, monitor


@configurable
def run_default_monitoring(run=True):
    return run


def monitoring(algs, name, mon_vars):
    """Create a list of monitoring algorithms from the control flow (algs).
       The last item in the CF whose producer is a Combiner or ParticleContainerMerger is taken as input to monitor.
       From that particle, mass, pt, eta, (decay time), vertex chi2 and ip chi2 histograms will be filled.

    Args:
        algs (list of Algorithms): Control flow of this line.
        name (str): Name of the calling line.
        monitoring_variables (tuple(str)): Variables to create default monitoring plots for.
           These variables need to be implemented in .monitoring.py

    Returns:
        List of monitoring algorithms.
    """
    producer = None

    def get_producer_with_selection(input_producer):
        if "BodyCombiner" in input_producer.typename:
            last_combiner = input_producer
            producer_output = input_producer.OutputParticles
            return producer_output, last_combiner
        elif "FunctionalDiElectronMaker" in input_producer.typename:
            last_combiner = input_producer
            producer_output = input_producer.Particles
            return producer_output, last_combiner
        if "Filter" in input_producer.typename and "Input" in input_producer.inputs:
            filter_input = input_producer.inputs["Input"].producer
            if "BodyCombiner" in filter_input.typename:
                last_combiner = filter_input
            elif "FunctionalDiElectronMaker" in filter_input.typename:
                last_combiner = filter_input
            elif "ParticleMaker" in filter_input.typename:
                last_combiner = input_producer
            elif "FastJetBuilder" in filter_input.typename:
                last_combiner = input_producer
            elif "Filter" in filter_input.typename and "Input" in input_producer.inputs:
                producer_output, last_combiner = get_producer_with_selection(
                    filter_input
                )
            elif "ParticleContainerMerger" in filter_input.typename:
                # sometimes there are "InputContainers", but there is also plain "Input"...
                # ... also lists can be nested, so they need to be flattened
                flatten_mergers = (
                    lambda merger_inputs: merger_inputs
                    if hasattr(merger_inputs, "producer")
                    else flatten_mergers(merger_inputs[0])
                )
                merger_input = flatten_mergers(
                    [v for k, v in input_producer.inputs.items() if "Input" in k][0]
                )
                producer_output, last_combiner = get_producer_with_selection(
                    merger_input.producer
                )
            else:
                last_combiner = None
            # overwrite the recursion result
            producer_output = input_producer.Output
            return producer_output, last_combiner
        if input_producer.typename == "ParticleContainerMerger":
            flatten_mergers = (
                lambda merger_inputs: merger_inputs
                if hasattr(merger_inputs, "producer")
                else flatten_mergers(merger_inputs[0])
            )
            merger_input = flatten_mergers(
                [v for k, v in input_producer.inputs.items() if "Input" in k][0]
            )
            producer_output, last_combiner = get_producer_with_selection(
                merger_input.producer
            )
            producer_output = input_producer.OutputContainer
            return producer_output, last_combiner
        else:
            return None, None

    for alg in reversed(algs):
        if hasattr(alg, "producer"):
            producer = alg.producer
            producer_output, last_combiner = get_producer_with_selection(producer)
            if producer_output is not None and last_combiner is not None:
                break
    if producer is None or last_combiner is None:
        return []

    # parse ranges from the combiner code. we need to get the cuts in parseable format (code_repr)
    prop_dict = last_combiner.properties
    comp_cutstring = (
        "" if "CompositeCut" not in prop_dict else prop_dict["CompositeCut"].code_repr()
    )
    comb_cutstring = (
        ""
        if "CombinationCut" not in prop_dict
        else prop_dict["CombinationCut"].code_repr()
    )
    # concatenate filter cut if it sits on top of a combiner. for the parsing it doesn't really matter that this is not a real cut string
    if (
        producer.typename == "ParticleRangeFilter"
        or producer.typename == "ChargedBasicsFilter"
    ):
        fltr = producer.properties["Cut"].code_repr()
        comp_cutstring += "" if "MVA" in fltr else fltr

    def _parse_range(functor, cut):
        # match the use of in_range with a given functor, and store the numbers in a capturing group (.*?)
        # to find the end of a functor, go to the last closing bracket and match either the AND with the nex functor, or the end of line "?( &|$)"
        m = re.search(rf"in_range\( (.\S*?), {functor}, (.\S*?) \)?( &|$)", cut)
        return None if m is None else (float(m.group(1)), float(m.group(2)))

    def _parse_limits(functor, cut, default_lo, default_hi):
        # example ( CHI2DOF < 10 ); match the brackets that separate cuts in code_repr() and capture the cut value
        lom = re.search(rf"{functor} > (.*?) \)", cut)
        him = re.search(rf"{functor} < (.*?) \)", cut)
        if lom is None and him is None:
            return None
        elif lom is None:
            parsed_max = float(him.group(1))
            return (default_lo, parsed_max) if parsed_max > default_lo else None
        elif him is None:
            parsed_min = float(lom.group(1))
            return (
                (float(lom.group(1)), default_hi) if parsed_min < default_hi else None
            )
        else:
            return (float(lom.group(1)), float(him.group(1)))

    def _parse_all(functor, default_lo, default_hi):
        # try to parse histogram ranges from combiner cuts. range takes precedence over parsing individual limits,
        # composite over combination, defaults are taken as last resort
        rg = _parse_range(functor, comp_cutstring)
        if rg is None:
            rg = _parse_limits(functor, comp_cutstring, default_lo, default_hi)
        if rg is None:
            rg = _parse_range(functor, comb_cutstring)
        if rg is None:
            rg = _parse_limits(functor, comb_cutstring, default_lo, default_hi)
        if rg is None:
            rg = (default_lo, default_hi)
        return rg

    def _parse_mass_functor_from_cut(cut):
        # extrawurst for delta mass and corrected mass
        if "MASS - CHILD(Index=1, Functor=MASS)" in cut:
            return {
                "functor": F.MASS - F.CHILD(1, F.MASS),
                "regex": r"\( MASS - CHILD\(Index=1, Functor=MASS\) \)",
            }
        if re.search(r"MASS( >| <|,)", cut):
            return {"functor": F.MASS, "regex": r"MASS"}
        if "BPVCORRM" in cut:
            return {
                "functor": F.BPVCORRM(make_pvs()),
                "regex": r"_BPVCORRM(?!ERR).*? _FORWARDARGS\(\) \), _FORWARDARGS\(\) \)",
            }
        else:
            return None

    def _parse_mass_functor():
        # parse the mass functor that is used. composite cut takes precedence over combination cut, delta mass over mass over (corrected mass).
        mass_functor_dict = _parse_mass_functor_from_cut(comp_cutstring)
        if mass_functor_dict is None:
            mass_functor_dict = _parse_mass_functor_from_cut(comb_cutstring)
        if mass_functor_dict is None:
            mass_functor_dict = {"functor": F.MASS, "regex": r"MASS"}
        return mass_functor_dict

    def _pv_filter_in_algs():
        for alg in reversed(algs):
            if "VoidFilter" in str(alg.__dict__.get("_alg_type", None)) and str(
                alg.__dict__.get("_name")
            ).startswith("require_pvs"):
                return True
        return False

    # parse the monitoring variable names and define the configuration of the histograms
    monitoring_histograms, global_monitoring_histograms = [], []

    for mon_var in mon_vars:
        # pt
        if mon_var.lower() == "pt":
            xmin, xmax = _parse_all(
                r"\( RHO_COORDINATE @ THREEMOMENTUM \)", 0.0, 40.0 * GeV
            )
            monitoring_histograms.append(
                histogram_1d(
                    f"/{name}/pt", f"Monitor_{name}_pt", F.PT, 80, (xmin, xmax), "PT"
                )
            )
        # eta
        elif mon_var.lower() == "eta":
            monitoring_histograms.append(
                histogram_1d(
                    f"/{name}/eta", f"Monitor_{name}_eta", F.ETA, 40, (1.5, 5.5), "ETA"
                )
            )
        # phi
        elif mon_var.lower() == "phi":
            monitoring_histograms.append(
                histogram_1d(
                    f"/{name}/phi", f"Monitor_{name}_phi", F.PHI, 90, (-pi, pi), "PHI"
                )
            )
        # mass
        elif mon_var.lower() == "m":
            mass_functor = _parse_mass_functor()
            # TODO: parse decay descriptor to set defaults in _parse_all. Might need https://gitlab.cern.ch/lhcb/Moore/-/issues/289
            xmin, xmax = _parse_all(mass_functor["regex"], 0.0, 7.0 * GeV)
            monitoring_histograms.append(
                histogram_1d(
                    f"/{name}/m",
                    f"Monitor_{name}_m",
                    mass_functor["functor"],
                    100,
                    (xmin, xmax),
                    "MASS",
                )
            )
        # decay time
        elif mon_var.lower() == "dectime":
            xmin, xmax = _parse_all(
                r"VTX_LTIME.*? _FORWARDARGS\(\) \), _FORWARDARGS\(\) \)", 0 * ps, 8 * ps
            )
            monitoring_histograms.append(
                histogram_1d(
                    f"/{name}/dectime",
                    f"Monitor_{name}_dectime",
                    F.BPVLTIME(make_pvs()),
                    100,
                    (xmin, xmax),
                    "DECAYTIME",
                )
            )
        # vertex chi2
        elif mon_var.lower() == "vchi2":
            xmin, xmax = _parse_all(
                r"\( VALUE_OR\(Value=nan\) \@ _CHI2DOF \)", 0.0, 32.0
            )
            monitoring_histograms.append(
                histogram_1d(
                    f"/{name}/vchi2",
                    f"Monitor_{name}_vchi2",
                    F.CHI2DOF,
                    64,
                    (xmin, xmax),
                    "CHI2DOF",
                )
            )
        # ip chi2
        elif mon_var.lower() == "ipchi2":
            if _pv_filter_in_algs():
                xmin, xmax = _parse_all(r"IPCHI2\)?\.bind.*?", 0.0, 1000.0)
                monitoring_histograms.append(
                    histogram_1d(
                        f"/{name}/ipchi2",
                        f"Monitor_{name}_ipchi2",
                        F.BPVIPCHI2(make_pvs()),
                        64,
                        (xmin, xmax),
                        "IPCHI2",
                    )
                )
        # # ip
        elif mon_var.lower() == "ip":
            if _pv_filter_in_algs():
                xmin, xmax = _parse_all(r"IP\)?\.bind.*?", 0.0, 10.0)
                monitoring_histograms.append(
                    histogram_1d(
                        f"/{name}/ip",
                        f"Monitor_{name}_ip",
                        F.OWNPVIP,
                        50,
                        (xmin, xmax),
                        "IP",
                    )
                )
        # number of candidates
        elif mon_var.lower() == "n_candidates":
            global_monitoring_histograms.append(
                histogram_1d(
                    f"/{name}/n_candidates",
                    f"Monitor_{name}_n_candidates",
                    F.SIZE(producer_output),
                    100,
                    (0, 100),
                    "NCANDIDATES",
                )
            )
        elif mon_var.lower() == "nlongtracks":
            global_monitoring_histograms.append(
                histogram_1d(
                    f"/{name}/nLongTracks",
                    f"Monitor_{name}_nLongTracks",
                    F.VALUE_OR(-1)
                    @ F.RECSUMMARY_INFO(make_rec_summary(), "nLongTracks"),
                    100,
                    (0, 1000),
                    "NLONGTRACKS",
                )
            )
        else:
            raise ValueError(
                f"{mon_var} is not a supported default monitoring histogram"
            )

    # define the monitoring algorithm for a container
    if monitoring_histograms:
        container_monitor = monitor(
            name=f"Monitor__{name}",
            data=producer_output,
            histograms=monitoring_histograms,
        )
    else:
        container_monitor = None

    # define the monitoring algorithm for global information
    if global_monitoring_histograms:
        global_monitor = monitor(
            name=f"GlobalMonitor__{name}", histograms=global_monitoring_histograms
        )
    else:
        global_monitor = None

    return list(filter(lambda m: m is not None, (container_monitor, global_monitor)))
