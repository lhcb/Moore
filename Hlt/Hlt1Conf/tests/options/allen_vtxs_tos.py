###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Allen.config import setup_allen_non_event_data_service
from AllenConf.persistency import build_decision_ids
from AllenConf.persistency import line_names as allen_line_names
from Moore import options
from PyConf.application import configure, configure_input
from PyConf.control_flow import CompositeNode, NodeLogic
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.hlt1_allen import (
    allen_gaudi_config,
    call_allen_decision_logger,
    make_allen_decision,
    make_allen_secondary_vertices,
    make_allen_sel_reports,
)
from RecoConf.legacy_rec_hlt1_tracking import get_default_ut_clusters


def make_tos_filter(line_name):
    from PyConf.Algorithms import TOSFilter__v1__RecVertex as TOSFilter
    from PyConf.Tools import ParticleTisTos

    return TOSFilter(
        name=line_name + "TOS",
        InputContainer=make_allen_secondary_vertices()["v1_dihadrons"],
        SelReports=make_allen_sel_reports(),
        DecisionPattern=line_name,
        ParticleTisTosTool=ParticleTisTos(
            TOSFracVP=1.0, TOSFracFT=1.0, TOSFracMuon=1.0
        ),
    )


def setup_tos_node(gather_selections):
    line_names = allen_line_names(gather_selections)
    hlt1_line_ids = build_decision_ids(line_names)
    filt_algs = [make_tos_filter(line_name) for line_name in hlt1_line_ids.keys()]

    return CompositeNode(
        "tosfilters", combine_logic=NodeLogic.NONLAZY_OR, children=filt_algs
    )


def run_test():
    """
    Final configuration for the test application
    """
    config = configure_input(options)

    # Get Allen configuration
    hlt1_config = allen_gaudi_config()
    gather_selections = hlt1_config["gather_selections"]
    allen_node = hlt1_config["control_flow_node"]
    non_event_data_node = setup_allen_non_event_data_service()

    # Get the configured TOS filters
    tos_node = setup_tos_node(gather_selections)

    # Make a complete control flow
    test_node = CompositeNode(
        "TestAllenSVsTOS",
        combine_logic=NodeLogic.LAZY_AND,
        children=[
            non_event_data_node,
            allen_node,
            call_allen_decision_logger(),
            make_allen_decision(),
            tos_node,
        ],
        force_order=True,
    )

    # Finalize configuration
    config.update(configure(options, test_node))

    return config


# Expand algorithm name field
options.msg_svc_format = "% F%70W%S %7W%R%T %0W%M"

# Remove the UT bind once the UT conditions are
# integrated in DD4Hep
with (
    allen_gaudi_config.bind(sequence="hlt1_pp_no_ut_dihadrons"),
    default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),
    get_default_ut_clusters.bind(disable_ut=True),
):
    run_test()
