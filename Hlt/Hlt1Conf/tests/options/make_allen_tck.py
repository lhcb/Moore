###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from AllenConf.persistency import register_allen_encoding_table

# raw_event_format must be configured to successfully configure Allen
# Configure with the same input as the default for Allen tests.
from hlt1_filtered_mdf_input import options
from PyConf.application import configure_input
from RecoConf.hlt1_allen import allen_gaudi_config

config = configure_input(options)

hlt1_config = allen_gaudi_config()
gather_selections = hlt1_config["gather_selections"]

key = register_allen_encoding_table(gather_selections)
print(key)
if key:
    print("PASSED")
