###############################################################################
# (c) Copyright 2023-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Test option for the Hlt1 bandwidth test in LHCbPR
The streaming configuration in this test is `streamless`, i.e. in a single stream to match data-taking.
To ensure the appropriate environment and input configurations, this should only be called from within DBASE/PRConfig/scripts/benchmark_scripts/Moore_bandwidth_test.sh
"""

import json

from AllenConf.persistency import line_names as hlt1_line_names
from AllenCore.gaudi_allen_generator import allen_runtime_options
from Configurables import DDDBConf
from DDDB.CheckDD4Hep import UseDD4Hep
from Hlt2Conf.tests.bandwidth.bandwidth_helpers import FileNameHelper
from Moore import options
from Moore.config import allen_control_flow
from PyConf.application import configure, configure_input
from RecoConf.hlt1_allen import allen_gaudi_config

STREAM_CONFIG = "streamless"
fname_helper = FileNameHelper(process="hlt1", stream_config=STREAM_CONFIG)
fname_helper.make_tmp_dirs()
options.output_file = fname_helper.mdfdst_fname_for_Moore(ext=".mdf").format(
    stream=STREAM_CONFIG
)
options.output_type = "MDF"
options.output_manifest_file = fname_helper.tck()

if UseDD4Hep:
    DDDBConf().GeometryVersion = "run3/before-rich1-geom-update-26052022"

config = configure_input(options)
# choice of sequence from https://mattermost.web.cern.ch/lhcb/pl/ewxcgeppm7gxdejdcsm6ko6xky
with (
    allen_gaudi_config.bind(sequence="hlt1_pp_matching_no_ut_1000KHz"),
    allen_runtime_options.bind(filename="allen_write_hlt1_bandwidth_test_monitor.root"),
):
    allen_node = allen_control_flow(options)
    hlt1_config = allen_gaudi_config()
    line_names = hlt1_line_names(hlt1_config["gather_selections"])
    config.update(configure(options, allen_node))

# Write out stream configuration to JSON file for use later in the test
with open(fname_helper.stream_config_json_path(), "w") as f:
    hlt1_linenames = [linename.replace("Decision", "") for linename in line_names]
    json.dump({STREAM_CONFIG: hlt1_linenames}, f)
