###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Write an HLT1-filtered DST file."""

from Moore import options

options.output_file = "test_allen_hlt1_persistence_dst_write.dst"
options.output_type = "ROOT"
options.root_ioalg_name = "RootIOAlgExt"
options.root_ioalg_opts = {
    "IgnorePaths": [
        "/Event/DAQ",
        "/Event/Gen/Collisions",
        "/Event/PrevPrev/Gen/Collisions",
        "/Event/Prev/Gen/Collisions",
        "/Event/Next/Gen/Collisions",
        "/Event/NextNext/Gen/Collisions",
    ]
}  # as DumpGeometry will potentially create it first

# there is no rejection, so avoid writing out too many events
options.evt_max = 100
