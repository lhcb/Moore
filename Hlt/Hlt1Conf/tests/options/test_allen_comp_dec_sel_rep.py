###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Check the consistency of the line decisions and presence of SelReport

Takes two outputs from previously-run jobs:
  1. The MDF
  2. The TCK in TCKData/config.cdb
For each line with a decision in the DecReport, the presence of a
corresponding SelReport is checked.
Two error conditions are recognized and considered failures of the test:
  - A positive line decision without a corresponding SelReport
  - A SelReport for a line with a negative decision
"""

import argparse
from collections import defaultdict

from PyConf.Algorithms import HltDecReportsDecoder, HltSelReportsDecoder
from PyConf.application import (
    ApplicationOptions,
    configure,
    configure_input,
    default_raw_banks,
    default_raw_event,
)
from PyConf.control_flow import CompositeNode

parser = argparse.ArgumentParser()
parser.add_argument("--input-mdf", help="Input MDF file")
args = parser.parse_args()

options = ApplicationOptions(_enabled=False)
options.gaudipython_mode = True
options.input_files = [args.input_mdf]
options.input_type = "MDF"
options.data_type = "Upgrade"
options.simulation = True
options.dddb_tag = "dddb-20231017"
options.conddb_tag = "sim-20231017-vc-mu100"
options.geometry_version = "run3/trunk"
options.conditions_version = "jonrob/all-pmts-active"
config = configure_input(options)

rawEvent = default_raw_event("HltDecReports")
decReport = default_raw_banks("HltDecReports")
selReport = default_raw_banks("HltSelReports")
decDec = HltDecReportsDecoder(
    name="Hlt1DecReportsDecoder", RawBanks=decReport, SourceID="Hlt1"
)
selDec = HltSelReportsDecoder(
    name="Hlt1SelReportsDecoder",
    RawBanks=selReport,
    DecReports=decReport,
    SourceID="Hlt1",
)

config.update(
    configure(
        options,
        CompositeNode(
            "TopSeq", children=[rawEvent, decReport, selReport, decDec, selDec]
        ),
    )
)

# decoderDB wants TCKANNSvc as name...
# Set up counters for recording decisions and selreport existence from MDF
counts_from_mdf = defaultdict(lambda: defaultdict(int))

import GaudiPython

gaudi = GaudiPython.AppMgr()
TES = gaudi.evtSvc()
gaudi.run(1)

error = False
while TES["/Event/DAQ"]:
    decs = TES[decDec.OutputHltDecReportsLocation.location]
    print(decs)
    if not decs:
        print("DecReports TES location not found")
        error = True
        break

    sels = TES[selDec.OutputHltSelReportsLocation.location]
    if not sels:
        print("SelReports TES location not found")
        error = True
        break

    for key in decs.decisionNames():
        report = decs.decReport(key)
        decision = report.decision()
        hassel = sels.hasSelectionName(key)
        counts_from_mdf[key][(decision, hassel)] += 1

    gaudi.run(1)

# Check for any inconsistent instances:
missing_sel = (1, False)  # Event accepted by line without a SelReport
extra_sel = (0, True)  # SelReport for a line that did not accept the event
for key in counts_from_mdf.keys():
    counters = counts_from_mdf[key]
    line_name = key
    if missing_sel in counters.keys():
        error = True
        print("Test ERROR: Missing SelReport for {}".format(line_name))
    if extra_sel in counters.keys():
        error = True
        print("Test ERROR: Spurious SelReport for {}".format(line_name))

    print("Coincidence counts for {}:  {}".format(line_name, counters))

if error:
    exit("Test failed")  # exit with a non-zero code
