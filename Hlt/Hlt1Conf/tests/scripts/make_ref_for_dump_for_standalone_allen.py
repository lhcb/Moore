###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Script to produce reference for dump_for_standalone_allen.qmt
#
# From the directory, in which the dumped directory resides, call this scripts with the main dump directory as first argument and the names of sub-directories as second argument, separated by comma
# Example: python path/to/make_ref_for_dump_for_standalone_allen.py MiniBrunel_2018_MinBias_FTv4_DIGI/ geometry_dddb-20180815_sim-20180530-vc-md100,mdf
#
# authors: Roel Aaij (roel.aaij@cern.ch), Dorothea vom Bruch (dorothea.vom.bruch@cern.ch)
# date:   07/2021

import os
import sys

base_dir = sys.argv[1]
dump_directories = sys.argv[2].split(",")
files = {}
for dd in dump_directories:
    dump_dir = os.path.join(base_dir, dd)
    files[dd] = sorted(
        [
            os.path.join(dump_dir, f)
            for f in os.listdir(dump_dir)
            if not f.endswith(".root")
        ]
    )

sizes = [(f, os.stat(os.path.join(f)).st_size) for v in files.values() for f in v]
sizes = sorted(sizes, key=lambda e: e[0])

with open("ref", "w") as ref:
    for f, s in sizes:
        ref.write("%s %d\n" % (f, s))
