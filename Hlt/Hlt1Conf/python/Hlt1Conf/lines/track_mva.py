###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import math

import Functors.math as fmath
from Functors import (
    BPVIPCHI2,
    CHI2DOF,
    PT,
)
from GaudiKernel.SystemOfUnits import GeV
from Moore.lines import Hlt1Line
from PyConf import configurable
from RecoConf.core_algorithms import Filter
from RecoConf.event_filters import require_gec, require_pvs
from RecoConf.legacy_rec_hlt1_tracking import (
    make_fitted_forward_tracks_with_pv_relations,
    make_legacy_rec_hlt1_fitted_tracks,
    make_legacy_rec_hlt1_tracks,
    make_pvs,
)


def make_tracks_mva_tracks():
    tracks = make_legacy_rec_hlt1_fitted_tracks(make_legacy_rec_hlt1_tracks())
    return make_fitted_forward_tracks_with_pv_relations(tracks)


@configurable
def track_mva_prefilters(pvs):
    return [require_gec(), require_pvs(pvs)]


@configurable
def one_track_mva_line(
    name="Hlt1TrackMVA",
    prescale=1,
    make_input_tracks=make_tracks_mva_tracks,
    make_pvs=make_pvs,
    # TrackMVALoose cuts from ZombieMoore
    max_chi2dof=2.5,
    min_pt=2.0 * GeV,
    max_pt=26 * GeV,
    min_ipchi2=7.4,
    param1=1.0,
    param2=2.0,
    param3=1.248,
):
    pvs = make_pvs()
    pre_sel = CHI2DOF < max_chi2dof
    hard_sel = (PT > max_pt) & (BPVIPCHI2(pvs) > min_ipchi2)
    bulk_sel = fmath.in_range(min_pt, PT, max_pt) & (
        fmath.log(BPVIPCHI2(pvs))
        > (
            param1 / ((PT / GeV - param2) * (PT / GeV - param2))
            + (param3 / max_pt) * (max_pt - PT)
            + math.log(min_ipchi2)
        )
    )
    full_sel = pre_sel & (hard_sel | bulk_sel)
    track_filter = Filter(make_input_tracks(), full_sel)["PrFittedForwardWithPVs"]
    return Hlt1Line(
        name=name, algs=track_mva_prefilters(pvs) + [track_filter], prescale=prescale
    )
