###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable
from PyConf.Algorithms import (
    CombineTracksSIMD__2Body__PrFittedForwardTracksWithMuonID,
    CombineTracksSIMD__2Body__PrFittedForwardTracksWithMuonID_Scalar,
    CombineTracksSIMD__2Body__PrFittedForwardTracksWithPVs,
    CombineTracksSIMD__2Body__PrFittedForwardTracksWithPVs_Scalar,
    CombineTracksSIMD__3Body__PrFittedForwardTracksWithMuonID,
    CombineTracksSIMD__3Body__PrFittedForwardTracksWithMuonID_Scalar,
    CombineTracksSIMD__3Body__PrFittedForwardTracksWithPVs,
    CombineTracksSIMD__3Body__PrFittedForwardTracksWithPVs_Scalar,
)
from RecoConf.core_algorithms import make_unique_id_generator


def require_all(*cuts):
    """Return a cut string that requires each of the argument strings.

    Example usage:

        >>> require_all('M < 8*GeV', 'PT > 3*GeV')
        '((M < 8*GeV) & (PT > 3*GeV))'
    """
    return "({})".format(" & ".join("({})".format(cut) for cut in cuts))


def require_any(*cuts):
    """Return a cut string that requires at least one of the argument strings.

    Example usage:

        >>> require_any('M < 8*GeV', 'PT > 3*GeV')
        '((M < 8*GeV) | (PT > 3*GeV))'
    """
    return "({})".format(" | ".join("({})".format(cut) for cut in cuts))


@configurable
def CombineTracks(
    InputTracks,
    AssumedMass="K+",
    Backend="Best",
    CombinationCut=None,
    NBodies=2,
    VertexCut=None,
    **kwargs,
):
    """Return a configured CombineTracks instance.

    Parameters
    ----------
    AssumedMass : float or str
        Mass hypothesis assigned to the tracks when calculating the parent
        4-momentum. This can either be a floating point value or a particle
        name.
    Backend : str
        Level of vectorisation to target in the combiner. Valid values are
        'Best', and 'Scalar'.
    CombinationCut : Functors.Functor
        Functor to be applied to the N-track combination object.
    Combination12Cut : Functors.Functor
        Functor to be applied to the 2-track sub-combination iff NBodies > 2
    NBodies : int
        The number of tracks entering each combination
    VertexCut : Functors.Functor
        Functor to be applied to the new composite object after the vertex fit.
    """
    # possible algorithms, indexed by string representing the C++ input type and other parameters
    candidate_configurables = [
        # LHCb::Pr::Fitted::Forward::Tracks zipped with PV relations
        (CombineTracksSIMD__2Body__PrFittedForwardTracksWithPVs, 2, "Best"),
        (CombineTracksSIMD__2Body__PrFittedForwardTracksWithPVs_Scalar, 2, "Scalar"),
        (CombineTracksSIMD__3Body__PrFittedForwardTracksWithPVs, 3, "Best"),
        (CombineTracksSIMD__3Body__PrFittedForwardTracksWithPVs_Scalar, 3, "Scalar"),
        # LHCb::Pr::Fitted::Forward::Tracks zipped with muon PIDs
        (CombineTracksSIMD__2Body__PrFittedForwardTracksWithMuonID, 2, "Best"),
        (CombineTracksSIMD__2Body__PrFittedForwardTracksWithMuonID_Scalar, 2, "Scalar"),
        (CombineTracksSIMD__3Body__PrFittedForwardTracksWithMuonID, 3, "Best"),
        (CombineTracksSIMD__3Body__PrFittedForwardTracksWithMuonID_Scalar, 3, "Scalar"),
    ]
    candidate_configurables = {
        (alg.getDefaultProperties()["InputTracks"].type(), n_bodies, backend): alg
        for alg, n_bodies, backend in candidate_configurables
    }
    # Get the configurable type that matches our inputs
    configurable = candidate_configurables[(InputTracks.type, NBodies, Backend)]
    return configurable(
        InputUniqueIDGenerator=make_unique_id_generator(),
        InputTracks=InputTracks,
        AssumedMass=str(AssumedMass),
        CombinationCut=CombinationCut,
        VertexCut=VertexCut,
        **kwargs,
    )
