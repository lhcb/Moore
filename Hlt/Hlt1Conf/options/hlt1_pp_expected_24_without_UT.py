###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Options file for running on expected-2024 signal simulation

If you copy this code to a file with path
    Moore/hlt1_pp_expected_24_without_UT.py

Then you'll be able to run it with
    Moore/run gaudirun.py Moore/hlt1_pp_expected_24_without_UT.py
    or
    lb-run -c <a detdesc binaryTag> Moore/<latest 2024-patches release> gaudirun.py Moore/hlt1_pp_expected_24_without_UT.py
"""

from DDDB.CheckDD4Hep import UseDD4Hep

if UseDD4Hep:
    # https://mattermost.web.cern.ch/lhcb/pl/tz5eowsiytrpfxi3br8oy1k3xy
    raise RuntimeError("""
        "Sorry, you'll need a detdesc stack to run over this MC. Please see https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/running_over_mc.html#switching-to-a-detdesc-compatible-platform".
        DD4HEP is suggested to be used on '2024.Q1.2' MC onwards. This input is 'expected-2024' and thus not suitable.
        """)
    # This output is used in Hlt/Hlt2Conf/options/hlt2_pp_expected_24_with_UT.py,
    # if this script gets updated to use 2024.Q1.2 MC as input than that comment can be removed too.

from Moore import options
from Moore.production import hlt1

# Input-specific options
# Arbitrary example input
options.set_input_and_conds_from_testfiledb("expected_2024_BsToJpsiPhi_xdigi")

# Output options
options.output_file = "hlt1_output.dst"
options.output_type = "ROOT"
options.root_ioalg_name = "RootIOAlgExt"
options.root_ioalg_opts = {"IgnorePaths": ["/Event/DAQ"]}

# Misc options
options.scheduler_legacy_mode = False
options.evt_max = 50

hlt1(options, "--sequence=hlt1_pp_matching_no_ut_1000KHz", "--flagging")
