###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test if FlavourTags objects are persisted and the pointers to the taggedB()
point to valid candidates.

Runs over the output file passed as the last argument to this script.

"""

import argparse

import GaudiPython as GP
from GaudiConf.reading import do_unpacking
from Moore import options
from PyConf.application import configure, configure_input

parser = argparse.ArgumentParser()
parser.add_argument("input", help="Input filename")
parser.add_argument("hlt2_manifest", help="HLT2 JSON manifest dump")
args = parser.parse_args()

# Change the tags if required
options.data_type = "Upgrade"
options.simulation = True
options.dddb_tag = "dddb-20201211"
options.conddb_tag = "sim-20201218-vc-md100"
options.input_files = [args.input]
options.input_type = "ROOT"
options.gaudipython_mode = True

config = configure_input(options)

cf_node = do_unpacking(input_process="Hlt2")

config.update(configure(options, cf_node))

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()
appMgr.run(1)

flavour_tags = ["FlavourTags/FlavourTags"]

locations = (
    [
        "/Event/HLT2/HltDecReports",
        "/Event/HLT2/Hlt2Test_B2JpsiK_withFlavourTagging/Particles",
        "/Event/HLT2/Hlt2Test_B2JpsiKModified_withFlavourTagging/Particles",
    ]
    + [
        f"/Event/HLT2/Hlt2Test_B2JpsiKModified_withFlavourTagging/{ft}"
        for ft in flavour_tags
    ]
    + [f"/Event/HLT2/Hlt2Test_B2JpsiK_withFlavourTagging/{ft}" for ft in flavour_tags]
)

while TES["/Event/HLT2"]:
    dec_rep = TES["/Event/Hlt2/DecReports"]
    if not dec_rep:
        print("could not obtain Hlt2 decisions")
    for i in (
        "Hlt2Test_B2JpsiK_withFlavourTaggingDecision",
        "Hlt2Test_B2JpsiKModified_withFlavourTagging",
    ):
        if not dec_rep.hasDecisionName(i):
            print("Missing decision {} in Hlt2DecReports".format(i))
    l = dec_rep.decReport("Hlt2Test_B2JpsiK_withFlavourTaggingDecision")
    l_mod = dec_rep.decReport("Hlt2Test_B2JpsiKModified_withFlavourTagging")
    if l.decision() or l_mod.decision():
        particles_default = TES[
            "/Event/HLT2/Hlt2Test_B2JpsiK_withFlavourTagging/Particles"
        ]
        particles_modified = TES[
            "/Event/HLT2/Hlt2Test_B2JpsiKModified_withFlavourTagging/Particles"
        ]

        for flavour_tag in flavour_tags:
            fts_default = TES[
                f"/Event/HLT2/Hlt2Test_B2JpsiK_withFlavourTagging/{flavour_tag}"
            ]
            if particles_default:
                if fts_default:
                    if particles_default.size() != fts_default.size():
                        print(particles_default.size(), fts_default.size())
                        print(
                            "ERROR: different size of particle container and flavourtag container for Hlt2Test_B2JpsiK_withFlavourTagging"
                        )
                    for i in range(len(fts_default)):
                        if not fts_default[i].taggedB():
                            print(
                                "ERROR: FlavourTag.taggedB() does not exist for Hlt2Test_B2JpsiK_withFlavourTagging"
                            )
                        elif fts_default[i].taggedB() != particles_default[i]:
                            print(
                                "ERROR: FlavourTag.taggedB() does not match corresponding B in particles container for Hlt2Test_B2JpsiK_withFlavourTagging"
                            )
                        for t in fts_default[i].taggers():
                            print("type", t.type())
                            print("omega", t.omega())
                            print("mvaValue", t.mvaValue())
                            print("N tag particles", t.taggerParts().size())
                else:
                    print(
                        "ERROR: no FlavourTags in Hlt2Test_B2JpsiK_withFlavourTagging"
                    )
            else:
                print("ERROR: no particles in Hlt2Test_B2JpsiK_withFlavourTagging")

            fts_modified = TES[
                f"/Event/HLT2/Hlt2Test_B2JpsiKModified_withFlavourTagging/{flavour_tag}"
            ]
            if particles_modified:
                if fts_modified:
                    if particles_modified.size() != fts_modified.size():
                        print(
                            "ERROR: different size of particle container and flavourtag container for Hlt2Test_B2JpsiKModified_withFlavourTagging"
                        )
                    for i in range(len(fts_modified)):
                        if not fts_default[i].taggedB():
                            print(
                                "ERROR: FlavourTag.taggedB() does not exist for Hlt2B2JpsiModifiedK"
                            )
                        elif fts_modified[i].taggedB() != particles_modified[i]:
                            print(
                                "ERROR: FlavourTag.taggedB() does not match corresponding B in particles container for Hlt2Test_B2JpsiKModified_withFlavourTagging"
                            )
                        for t in fts_modified[i].taggers():
                            print("type", t.type())
                            print("omega", t.omega())
                            print("mvaValue", t.mvaValue())
                            print("N tag particles", t.taggerParts().size())
                else:
                    print(
                        "ERROR: no FlavourTags in Hlt2Test_B2JpsiKModified_withFlavourTagging"
                    )
    appMgr.run(1)
