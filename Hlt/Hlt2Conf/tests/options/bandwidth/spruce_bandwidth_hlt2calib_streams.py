###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Test options for the Sprucing bandwidth test in LHCbPR performing a sprucing-pass job on hlt2Calib output
To ensure the appropriate environment and input configurations, this should only be called from within DBASE/PRConfig/scripts/benchmark_scripts/Moore_bandwidth_test.sh
"""

import json
import logging

from Hlt2Conf.sprucing_settings.Sprucing_production_pp import (
    make_hlt2calib_spruce_prod_streams,
)
from Hlt2Conf.tests.bandwidth.bandwidth_helpers import FileNameHelper
from Moore import options, run_moore
from Moore.monitoring import run_default_monitoring
from RecoConf.reconstruction_objects import reconstruction

log = logging.getLogger()

options.input_process = "Hlt2"

fname_helper = FileNameHelper(process="spruce", stream_config="hlt2calib")
fname_helper.make_tmp_dirs()
options.output_file = fname_helper.mdfdst_fname_for_Moore(ext=".dst")
options.output_type = "ROOT"
options.output_manifest_file = fname_helper.tck()


def make_streams():
    real_streams = make_hlt2calib_spruce_prod_streams()

    # Write out stream configuration to JSON file for use later in the test
    with open(fname_helper.stream_config_json_path(), "w") as f:
        json.dump(
            {
                stream.name: [line.name for line in stream.lines]
                for stream in real_streams.streams
            },
            f,
        )

    # Write out persistency options to JSON to be printed in the html pages
    with open(fname_helper.line_descriptor_json_path(), "w") as f:
        json.dump(
            {
                stream.name: {
                    line.name: {
                        "PersistReco": line.persistreco
                        if hasattr(line, "persistreco")
                        else False,
                        "ExtraOutputs": bool(len(line.extra_outputs))
                        if hasattr(line, "extra_outputs")
                        else False,
                    }
                    for line in stream.lines
                }
                for stream in real_streams.streams
            },
            f,
        )

    return real_streams


with (
    reconstruction.bind(from_file=True, spruce=True),
    run_default_monitoring.bind(run=False),
):
    config = run_moore(options, make_streams, public_tools=[])
