###############################################################################
# (c) Copyright 2000-2025 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Test options for the Hlt2 bandwidth test in LHCbPR performing a hlt2-production job on hlt1-filtered output.
To ensure the appropriate environment and input configurations, this should only be called from within DBASE/PRConfig/scripts/benchmark_scripts/Moore_bandwidth_test.sh
"""

import json

from DDDB.CheckDD4Hep import UseDD4Hep
from Hlt2Conf.settings.hlt2_pp_2025 import make_streams
from Hlt2Conf.tests.bandwidth.bandwidth_helpers import FileNameHelper
from Moore import options, run_moore
from Moore.lines import Hlt2LuminosityLine
from RecoConf.global_tools import (
    stateProvider_with_simplified_geom,
    trackMasterExtrapolator_with_simplified_geom,
)
from RecoConf.reconstruction_objects import reconstruction

fname_helper = FileNameHelper(process="hlt2", stream_config="production")
fname_helper.make_tmp_dirs()
options.output_file = fname_helper.mdfdst_fname_for_Moore(ext=".mdf")
options.output_type = "MDF"
options.output_manifest_file = fname_helper.tck()
options.output_streams_attributes_file = fname_helper.streams_attributes_file()


def make_main_streams():
    streams = make_streams()

    # HACK Remove lumi nano lines for bandwidth tests
    # See PRConfig #38
    for stream in streams.streams:
        filtered = [
            line for line in stream.lines if not isinstance(line, Hlt2LuminosityLine)
        ]
        stream.update(filtered)
    streams.update()

    # Write out stream configuration to JSON file for use later in the test
    with open(fname_helper.stream_config_json_path(), "w") as f:
        json.dump(
            {
                stream.name: [line.name for line in stream.lines]
                for stream in streams.streams
            },
            f,
        )

    # Write out persistency options to JSON to be printed in the html pages
    with open(fname_helper.line_descriptor_json_path(), "w") as f:
        json.dump(
            {
                stream.name: {
                    line.name: {
                        "PersistReco": line.persistreco
                        if hasattr(line, "persistreco")
                        else False,
                        "ExtraOutputs": bool(len(line.extra_outputs))
                        if hasattr(line, "extra_outputs")
                        else False,
                    }
                    for line in stream.lines
                }
                for stream in streams.streams
            },
            f,
        )

    return streams


options.scheduler_legacy_mode = False

if UseDD4Hep:
    # This needs to happen before the public tools are instantiated,
    # which means we cannot put it inside make_streams().
    from PyConf.Tools import TrackMasterExtrapolator, TrackMasterFitter

    TrackMasterExtrapolator.global_bind(
        ApplyMultScattCorr=False,
        ApplyEnergyLossCorr=False,
        ApplyElectronEnergyLossCorr=False,
    )
    TrackMasterFitter.global_bind(ApplyMaterialCorrections=False)

public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom(),
]
with reconstruction.bind(from_file=False):
    config = run_moore(options, make_main_streams, public_tools=public_tools)
