###############################################################################
# (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import json
from collections import defaultdict

import PyConf
from Hlt2Conf.settings.hlt2_pp_2024 import make_streams
from Moore import options, run_moore
from RecoConf.global_tools import (
    stateProvider_with_simplified_geom,
    trackMasterExtrapolator_with_simplified_geom,
)
from RecoConf.reconstruction_objects import reconstruction


def _datadependencies(algs):
    def __walk(visited, top):
        if top.name in visited:
            return
        yield top
        visited.add(top.name)

        if isinstance(top, PyConf.control_flow.CompositeNode):
            for c in top.children:
                for p in __walk(visited, c):
                    yield p
        else:
            for handles in top.inputs.values():
                handles = handles if isinstance(handles, list) else [handles]
                for handle in handles:
                    for p in __walk(visited, handle.producer):
                        yield p

    visited = set()
    for alg in algs:
        yield from __walk(visited, alg)


def mapAlgToLines(lines):
    """
    Process a list of Hlt lines to extract and categorize algorithms and combiners.

    This function iterates over a collection of lines, examining each line's
    algorithms and collect a dictionary with mappings between algorithms, lines and inputs.

    Args:
        lines (list): A list of line objects to be processed.

    Returns:
        AlgorithmToLines (defaultdict): Maps algorithm names to a list of line names.
    """

    # Defaultdict(list) means that the default initialization is a list
    # meaning one can append directly even if the key does not exist
    AlgorithmToLines = defaultdict(list)

    for line in lines:
        for c in line.node.children:
            if isinstance(c, PyConf.components.Algorithm):
                datadeps = list(_datadependencies([c]))
            else:
                datadeps = list(_datadependencies(c.children))

            for alg in datadeps:
                if hasattr(alg, "typename"):
                    if "BodyCombiner" in alg.typename:
                        AlgorithmToLines[alg.name].append(line.name)

    return AlgorithmToLines


def make_streams_max_candidates():
    """
    Picks up production stream settings as in hlt2_pp_2024.py
    """
    from DDDB.CheckDD4Hep import UseDD4Hep

    if UseDD4Hep:
        # This needs to happen before the public tools are instantiated,
        # which means we cannot put it inside make_streams().
        from PyConf.Tools import TrackMasterExtrapolator, TrackMasterFitter

        TrackMasterExtrapolator.global_bind(
            ApplyMultScattCorr=False,
            ApplyEnergyLossCorr=False,
            ApplyElectronEnergyLossCorr=False,
        )
        TrackMasterFitter.global_bind(ApplyMaterialCorrections=False)

    streams = make_streams()
    all_lines = []
    for stream in streams.streams:
        all_lines += stream.lines

    AlgToLines = mapAlgToLines(all_lines)

    with open("AlgToLines.json", "w") as f:
        json.dump(AlgToLines, f)

    return streams


public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom(),
]

with reconstruction.bind(from_file=False):
    config = run_moore(options, make_streams_max_candidates, public_tools=public_tools)
