###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Options file to test the monitoring
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.reconstruction_objects import (
    make_pvs,
    upfront_reconstruction,
)
from RecoConf.standard_particles import make_long_pions
from SelAlgorithms.monitoring import histogram_1d, histogram_2d, histogram_axis, monitor


@configurable
def filter_decay_products(
    particles,
    pvs,
    *,
    max_track_chi2dof=9.0,
    max_ghostprob=0.4,
    ip_min=0.5 * mm,
    mipchi2_min=25.0,
):
    cut = F.require_all(
        F.CHI2DOF < max_track_chi2dof,
        F.GHOSTPROB < max_ghostprob,
        F.MINIP(pvs) > ip_min,
        F.MINIPCHI2(pvs) > mipchi2_min,
    )
    return ParticleFilter(particles, F.FILTER(cut))


@configurable
def make_kaons(
    decay_descriptor,
    inputs,
    pvs,
    *,
    mass_min=400.0 * MeV,
    mass_max=600.0 * MeV,
    two_body_comb_maxdocachi2=9.0,
    comb_maxdoca=0.4 * mm,
    vchi2pdof_max=9.0,
):
    combination_code = F.require_all(
        in_range(mass_min, F.MASS, mass_max),
        F.MAXDOCACUT(comb_maxdoca),
        F.MAXSDOCACHI2CUT(two_body_comb_maxdocachi2),
    )
    vertex_code = F.CHI2DOF < vchi2pdof_max
    return ParticleCombiner(
        inputs,
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


all_lines = {}


@register_line_builder(all_lines)
@configurable
def ks02pipi_line(name="Hlt2KS02pipi", prescale=1):
    pvs = make_pvs()
    pions = filter_decay_products(make_long_pions(), pvs)
    kaons = make_kaons("KS0 -> pi+ pi-", [pions, pions], pvs)

    mass_min, mass_max = 400.0 * MeV, 600.0 * MeV

    # this will monitor all the pions satisfying the basic requirements
    # from events triggered by this line
    precomb_pions_pt_mon = histogram_1d(
        functor=F.PT,
        name="pion_pt",
        title="pion_pt",
        label="PT",
        bins=100,
        range=(0.0, 1e3 * MeV),
    )
    precomb_pions_pt_over_p_mon = histogram_2d(
        name="pion_pt_over_p",
        title="pion_pt_over_p",
        xaxis=histogram_axis(functor=F.P, bins=100, range=(0.0, 10.0 * GeV), label="P"),
        yaxis=histogram_axis(
            functor=F.PT, bins=100, range=(0.0, 1e3 * MeV), label="PT"
        ),
    )
    kaons_mass_mon = histogram_1d(
        functor=F.MASS,
        name="mass",
        title="mass",
        label="MASS",
        bins=100,
        range=(mass_min, mass_max),
    )
    kaons_momenta_sum_mon = histogram_1d(
        functor=F.MASS**2 + F.PT**2,
        name="energy squared",
        title="energy squared",
        label="energy squared",
        bins=100,
        range=(mass_min, mass_max),
    )

    pions_monitor = monitor(
        name=f"{name}PionsMonitor",
        data=pions,
        histograms=[precomb_pions_pt_mon, precomb_pions_pt_over_p_mon],
    )
    kaons_monitor = monitor(
        name=f"{name}KaonsMonitor",
        data=kaons,
        histograms=[kaons_mass_mon, kaons_momenta_sum_mon],
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [kaons, pions_monitor, kaons_monitor],
        prescale=prescale,
    )


# Moore configuration
from Moore import options, run_moore

# Temporary workaround for TrackStateProvider
from RecoConf.global_tools import stateProvider_with_simplified_geom

options.set_input_and_conds_from_testfiledb("Upgrade_MinBias_LDST")
options.input_raw_format = 4.3
options.persistreco_version = 0.0
options.evt_max = 1000
options.histo_file = "hlt2_histos.root"

run_moore(options, lambda: [ks02pipi_line()], [stateProvider_with_simplified_geom()])
