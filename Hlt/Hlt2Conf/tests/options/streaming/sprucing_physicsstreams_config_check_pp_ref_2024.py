###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Verifies whether all the HLT2 TURCAL and TURBO lines are to be streamed in passthrough Sprucing and the HLT2 FULL lines to be included for TISTOS persistency in exclusive Sprucing

NB: This test should use the sprucing configuration for the 2024 pp reference run.
"""

import ast
import re
from pathlib import Path

# This needs to be kept up to date for the test to make sense!!
from Hlt2Conf.sprucing_settings.Sprucing_production_pp_ref_2024 import (
    full_modules_for_TISTOS,
    lines_for_TISTOS,
    nominal_lines,
    persist_reco_lines,
    persist_reco_rawbank_lines,
    rawbank_lines,
    turbolinedict,
)


def get_hlt2_streaming(hlt2optsdumpfile):
    """Takes job opts dump of HLT2 job and returns the streaming configuration. Million thanks to Chris Burr for this!"""
    app_config = ast.literal_eval(Path(hlt2optsdumpfile).read_text())
    decisions = dict()
    for name, _, nodes, _ in app_config["HLTControlFlowMgr"]["CompositeCFNodes"]:
        if not name.endswith("_writer"):
            continue
        if name in decisions:
            raise NotImplementedError("Found two nodes named '{name}'")
        if len(nodes) != 2 or not nodes[0].startswith(
            "HltDecReportsFilter/HltDecReportsFilter_"
        ):
            raise NotImplementedError(f"Unexpected nodes for {name}: {nodes}")
        decisions[name.removesuffix("_writer")] = [
            line.removesuffix("Decision")
            for line in app_config[nodes[0].split("/", 1)[1]]["Lines"]
        ]
    for name, lines in decisions.items():
        print(f"First 3 lines of stream {name} are ({len(lines)}) :", *lines[:3], "...")
    return decisions


# Lumi line not to be streamed to disk
omit_lines = ["Hlt2Lumi"]
# Monitoring and DQ lines not to be streamed to disk
turcal_omit_lines = ["Hlt2Monitoring", "Hlt2DQ", "Hlt2CalibMon"] + omit_lines

##Get HLT2 lines by stream ("Decision" suffix added for TURBO for regex compatibility)
decisions = get_hlt2_streaming("hlt2_pp_ref_2024_optsdump.py")
turbo_hlt2_lines = [
    line + "Decision"
    for line in decisions["turbo"]
    if not any(x in line for x in omit_lines)
]
turcal_hlt2_lines = [
    line
    for line in decisions["turcal"]
    if not any(x in line for x in turcal_omit_lines)
]
full_hlt2_lines = [
    line for line in decisions["full"] if not any(x in line for x in omit_lines)
]

##Add together all WG regexes
turbo_regex = [
    item
    for sublist in [list(turbolinedict[wg]) for wg in turbolinedict]
    for item in sublist
]
r = re.compile("|".join(turbo_regex))
###print(f"r : {r.pattern}")

# TURBO
##Turbo lines to be spruced are
turbo_spruce_lines = list(filter(r.match, turbo_hlt2_lines))
##Turbo lines running in HLT2 but not being streamed by Sprucing are
turbo_hlt2_not_spruce = list(set(turbo_hlt2_lines) - set(turbo_spruce_lines))

# TURCAL
##Turcal lines to be spruced are
turcal_spruce_lines = (
    nominal_lines + persist_reco_lines + rawbank_lines + persist_reco_rawbank_lines
)
##Turcal lines running in HLT2 but not being streamed by Sprucing are
turcal_hlt2_not_spruce = list(set(turcal_hlt2_lines) - set(turcal_spruce_lines))
##Turcal lines set to be Spruced but not running in HLT2 are
turcal_spruce_not_hlt2 = list(set(turcal_spruce_lines) - set(turcal_hlt2_lines))

# FULL
##Full lines for TISTOS in exclusive sprucing are
full_spruce_lines = lines_for_TISTOS(full_modules_for_TISTOS)
##Full lines running in HLT2 but not with TISTOS persistency in sprucing are
full_hlt2_not_spruce = list(set(full_hlt2_lines) - set(full_spruce_lines))
##Full lines set for TISTOS persistency but not running in HLT2 are
full_spruce_not_hlt2 = list(set(full_spruce_lines) - set(full_hlt2_lines))

has_failed = False

turbo = (turbo_hlt2_not_spruce, [])
turcal = (turcal_hlt2_not_spruce, turcal_spruce_not_hlt2)
full = (full_hlt2_not_spruce, full_spruce_not_hlt2)
streams = {"TURBO": turbo, "TURCAL": turcal, "FULL": full}

for stream, lines in streams.items():
    hlt2_not_spruce, spruce_not_hlt2 = lines
    if hlt2_not_spruce:
        print(
            f"ERROR:: Lines {hlt2_not_spruce} of {stream} stream will not be spruced and therefore will not be avaliable on disk. In the case of FULL lines you will not have TISTOS persistency. Please update the Sprucing configuration.\n"
        )
        has_failed = True

    if spruce_not_hlt2:
        print(
            f"ERROR:: Lines {spruce_not_hlt2} of {stream} stream are due to be spruced but did not run in HLT2. Please update the Sprucing configuration.\n"
        )
        has_failed = True
    if not has_failed:
        print(f"{stream} stream correctly configured \n")

if has_failed:
    raise RuntimeError("Some lines failed one of the line tests.\n")

print("SUCCESS:: All lines passed the tests.")
