###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options

options.set_input_and_conds_from_testfiledb("hltlumisummarytupling")

options.input_type = "MDF"

options.output_type = "ROOT"
options.ntuple_file = "LumiTuple.root"

options.evt_max = 1000

options.output_file = "Lumi.root"
