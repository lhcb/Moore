###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test for overlap in sprucing passthrough."""

import argparse

import GaudiPython as GP
from Configurables import HistogramPersistencySvc
from GaudiConf.reading import do_unpacking
from Hlt2Conf.check_output import check_banks, check_not_banks
from Moore import options
from PyConf.application import configure, configure_input

parser = argparse.ArgumentParser()
parser.add_argument("-i", type=str, help="Input MDF or DST")
parser.add_argument("-t", type=str, help=".tck.json file from the job")
parser.add_argument("-p", type=str, help="input_process can be Spruce or Turbo")
parser.add_argument("-s", type=str, help="Stream to test")

args = parser.parse_args()

##Prepare application
options.data_type = "Upgrade"
options.simulation = True
options.geometry_version = "run3/trunk"
options.conditions_version = "jonrob/all-pmts-active"
options.input_files = [args.i]
if args.t:
    options.input_manifest_file = args.t
options.input_type = "ROOT"
options.root_ioalg_name = "RootIOAlgExt"
options.evt_max = -1
options.gaudipython_mode = True
options.input_stream = args.s
config = configure_input(options)

assert args.p == "Spruce" or args.p == "Turbo", (
    "input_process is Turbo (passthrough sprucing) or Spruce"
)

if args.p == "Turbo":
    RECO_ROOT = "/Event/HLT2"
else:
    RECO_ROOT = "/Event/Turbo/HLT2"

cf_node = do_unpacking(input_process=args.p, has_mc_data=options.simulation)

config.update(configure(options, cf_node))

# Disable warning about histogram saving not being required
HistogramPersistencySvc(OutputLevel=5)

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()

# MonkeyPatch for the fact that RegistryEntry.__bool__
# changed in newer cppyy. Proper fix should go into Gaudi
import cppyy

cppyy.gbl.DataSvcHelpers.RegistryEntry.__bool__ = lambda x: True

nevents = 100
if args.p == "Turbo":
    lineone = "Hlt2Topo2Body"
    linetwo = "Hlt2Topo3Body"
else:
    lineone = "Hlt2Lineone_extraoutputs"
    linetwo = "Hlt2Linetwo"

lineone_extraoutputs = ["LongTracks", "LongerTracks"]
reco_locs = ["Calo/Electrons", "ProtoP/Long"]


def check_particles(TES, prefix):
    container = TES[prefix + "/Particles"]
    if not container:
        raise RuntimeError(
            "Check ERROR - No particles propagated contrary to DecReports"
        )
    if not container.size() > 0:
        raise RuntimeError(
            "Check ERROR - No particles propagated contrary to DecReports"
        )
    print(prefix + "/Particles has size ", container.size())


for ii in range(nevents):
    print(f"Checking event {ii}...")
    appMgr.run(1)
    if not TES["/Event"]:
        if ii == 0:
            raise Exception("No events found. Something has gone wrong!!")
        else:
            raise Exception(
                f"No events with {lineone} and {linetwo} found. Something has gone wrong!!"
            )

    hlt2reportloc = TES["/Event/Hlt2/DecReports"]
    if (
        hlt2reportloc.decReport(f"{lineone}Decision").decision()
        and hlt2reportloc.decReport(f"{linetwo}Decision").decision()
    ):
        print(f"This event passed both {lineone} and {linetwo}")
        TES.dump()

        if args.p == "Turbo":
            for line in [lineone, linetwo]:
                check_particles(TES, f"/Event/HLT2/{line}")
            print("Overlap is present")
            break
        else:  # Spruce
            if options.input_stream == "streamone":
                # Check particles and rb
                check_particles(TES, f"/Event/Turbo/{lineone}")
                check_banks(TES, options.input_stream, [9])

                # Check persistreco
                for reco in reco_locs:
                    persistreco = TES[f"/Event/Turbo/HLT2/Rec/{reco}"]
                    if not persistreco or persistreco.size() <= 0:
                        raise RuntimeError(
                            "Check ERROR - Reco locations not propagated"
                        )
                    else:
                        print(
                            f"Reco locations propagated. There are {persistreco.size()} {reco}"
                        )

                # Check track ancestors
                ## Note that track ancstors are not present for HLT2 stage to begin with
                ## proto().track().ancestors().size()

                # Check CALO clusters
                caloclusters = TES["/Event/Turbo/HLT2/Rec/Calo/Electrons"][0].clusters()
                if not caloclusters or caloclusters.size() <= 0:
                    raise RuntimeError("Check ERROR - Calo clusters not propagated")
                else:
                    print(
                        f"Calo clusters propagated. There are {caloclusters.size()} for the first electron"
                    )

                # Check CALO digits
                calodigits = TES["/Event/Turbo/HLT2/Rec/Calo/Electrons"][0].digits()
                if not calodigits or calodigits.size() <= 0:
                    raise RuntimeError("Check ERROR - Calo digits not propagated")
                else:
                    print(
                        f"Calo digits propagated. There are {calodigits.size()} for the first electron"
                    )

                # Check PV tracks
                pvtracks = TES["/Event/Turbo/HLT2/Rec/Vertex/Primary"][0].tracks()
                if not pvtracks or pvtracks.size() <= 0:
                    raise RuntimeError("Check ERROR - PV tracks not propagated")
                else:
                    print(
                        f"PV tracks propagated. There are {pvtracks.size()} for the first vertex"
                    )

                # Check extra_outputs
                for extraoutput in lineone_extraoutputs:
                    check_particles(TES, f"/Event/Turbo/{lineone}/{extraoutput}")

                # Check no overlap
                try:
                    check_particles(TES, f"/Event/Turbo/{linetwo}")
                except RuntimeError:
                    print(f"Check - No particles from {linetwo} AS EXPECTED")

                else:
                    print("Check ERROR - Overlap is present")
                finally:
                    print(f"Done with {lineone}")
                    break

            else:  # streamtwo
                # Check particles and no Rich rb
                check_particles(TES, f"/Event/Turbo/{linetwo}")
                check_not_banks(TES, options.input_stream, [9])

                # Check persistreco
                for reco in reco_locs:
                    persistreco = TES[f"/Event/Turbo/HLT2/Rec/{reco}"]
                    if not persistreco or persistreco.size() <= 0:
                        print(f"Reco ({reco}) not propagated as expected")
                    else:
                        raise RuntimeError(
                            f"Check ERROR - reco locations propagated. There are {persistreco.size()} {reco} - this is wrong"
                        )

                # Cannot check absense of CALO objs with linetwo

                # Check PV tracks
                pvtracks = TES["/Event/Turbo/HLT2/Rec/Vertex/Primary"][0].tracks()
                if not pvtracks or pvtracks.size() == 0:
                    print("PV tracks not propagated as expected")
                else:
                    raise RuntimeError(
                        f"Check ERROR - PV tracks propagated - NOT expected, gives {pvtracks.size()}"
                    )

                # Check for no overlap
                try:
                    check_particles(TES, f"/Event/Turbo/{lineone}")
                except RuntimeError:
                    print(f"Check - No particles from {lineone} AS EXPECTED")
                else:
                    print("Check ERROR - Overlap is present")
                finally:
                    print(f"Done with {linetwo}")
                    break
