###############################################################################
# (c) Copyright 2021-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test exclusive and passthrough (Turbo) Sprucing output.

Can check if RawBanks are present and not present
Find number->RawBank info at https://gitlab.cern.ch/lhcb/LHCb/-/blob/master/Event/DAQEvent/include/Event/RawBank.h

./run python Hlt/Hlt2Conf/tests/options/sprucing/spruce_check_neutral.py -i spruce_example_realtimereco_neutral.test.dst -t spruce_example_realtime_neutral.tck.json
-p Spruce -s test -rb 9 21 -notrb 13 73
"""

# FIXME (MCl) I'm artificially disabling tests using this script because I do
# not understand enough of PyConf information flow and of how IOAlg integrates
# with it to be able to make it run.
exit(77)

import argparse

import GaudiPython as GP
from Configurables import (
    ApplicationMgr,
    HistogramPersistencySvc,
    IODataManager,
    LHCbApp,
    createODIN,
)
from GaudiConf import IOExtension
from GaudiConf.reading import __unpack_rawevent, do_unpacking
from Hlt2Conf.check_output import (
    check_banks,
    check_decreports,
    check_hlt2_topo_candidates,
    check_MCoutput,
    check_not_banks,
    check_particles,
)
from PyConf.application import configured_ann_svc


def error(msg):
    print("Check ERROR", msg)


parser = argparse.ArgumentParser()
parser.add_argument("-i", type=str, help="Input MDF or DST")
parser.add_argument("-t", type=str, help=".tck.json file from the job")
parser.add_argument("-p", type=str, help="input_process can be Spruce or Turbo")
parser.add_argument("-s", type=str, help="Stream to test")
parser.add_argument("-n", type=int, required=False, help="Number of events to check")
parser.add_argument("-rb", type=int, nargs="+", help="RawBanks to check")
parser.add_argument(
    "-notrb", type=int, nargs="+", help="RawBanks to check are NOT in output"
)
parser.add_argument(
    "-topo",
    action="store_true",
    help="Check that Topo candidates from HLT2 are present",
)

args = parser.parse_args()

assert args.p == "Spruce" or args.p == "Turbo", (
    "input_process is Turbo (passthrough sprucing) or Spruce"
)

##Prepare application
LHCbApp(
    DataType="Upgrade",
    Simulation=True,
    DDDBtag="dddb-20210617",
    CondDBtag="sim-20210617-vc-mu100",
)

input_process = args.p
stream = args.s

if input_process == "Spruce":
    RECO_ROOT = "/Event/Spruce/HLT2"
    dec_to_check = ["SpruceTest_BdToKstGamma"]

if input_process == "Turbo":
    RECO_ROOT = "/Event/HLT2"
    dec_to_check = "Passthrough"

algs = do_unpacking(
    input_process=input_process, stream=stream, simulation=True, raw_event_format=0.3
)

unpack = [
    __unpack_rawevent(
        bank_types=["ODIN", "HltDecReports", "DstData"],
        input_process=input_process,
        stream=stream,
    )
]

odin = createODIN()

algs = unpack + [odin] + algs[1:]

app = ApplicationMgr(TopAlg=algs)
app.ExtSvc += [configured_ann_svc(json_file=args.t)]

IOExtension().inputFiles([args.i], clear=True)
# Disable warning about not being able to navigate ancestors
IODataManager(DisablePFNWarning=True)
# Disable warning about histogram saving not being required
HistogramPersistencySvc(OutputLevel=5)

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()

# MonkeyPatch for the fact that RegistryEntry.__bool__
# changed in newer cppyy. Proper fix should go into Gaudi
import cppyy

cppyy.gbl.DataSvcHelpers.RegistryEntry.__bool__ = lambda x: True

nevents = args.n if args.n else 1

nevents = 1

for ii in range(nevents):
    print("Checking next event.")
    appMgr.run(1)
    if not TES["/Event"]:
        if ii == 0:
            error("No events found. Something has gone very wrong!!")
        break
    if ii == 0:
        TES.dump()

    hlt2reportloc = TES["/Event/Hlt2/DecReports"]
    hlt2_decs_to_check = hlt2reportloc.decReports().keys()
    ##Note this ^^ will not check streaming only that you can access hlt2 decisions
    print("hlt2_decs_to_check ", hlt2_decs_to_check)

    # Check dec reports
    if input_process == "Turbo":
        decisions = check_decreports(TES, decs=hlt2_decs_to_check)
        check_decreports(TES, decs=[dec_to_check], stage="Spruce")
    elif input_process == "Spruce":
        decisions = check_decreports(TES, decs=dec_to_check, stage="Spruce")
        check_decreports(TES, decs=hlt2_decs_to_check)

    # Check particles and relations between them
    if input_process == "Turbo":
        for k, v in decisions.items():
            if v and "Lumi" not in k:
                prefix = "/Event/HLT2/" + k
                check_particles(TES, prefix.removesuffix("Decision"))
    elif input_process == "Spruce":
        for k, v in decisions.items():
            if v and "Lumi" not in k:
                prefix = "/Event/Spruce/" + k
                check_particles(TES, prefix.removesuffix("Decision"))

    # Check MC locations if simulation like
    if "_dstinput" in args.i:
        check_MCoutput(TES, RECO_ROOT, fs=1)

    # Check persistency of neutral particles
    if "_neutral" in args.i:
        MC_neutral_rel = TES[RECO_ROOT + "/Relations/NeutralPP2MCP"]
        if not MC_neutral_rel:
            error("MC relations table for neutral particles not propagated")
        if not MC_neutral_rel.relations().size() >= 4:
            error("MC relations table for neutral particles not correctly propagated")
        print(
            "MC relations table for neutral particles size is ",
            MC_neutral_rel.relations().size(),
        )

    # Check rawbanks specific to this stream
    print("args.rb ", args.rb)

    if args.rb:
        check_banks(TES, args.s, banks=args.rb)

    print("args.notrb ", args.notrb)

    if args.notrb:
        check_not_banks(TES, args.s, banks=args.notrb)

    if args.topo:
        check_hlt2_topo_candidates(TES)
