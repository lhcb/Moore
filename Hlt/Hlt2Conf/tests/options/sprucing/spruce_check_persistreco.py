###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test Sprucing output of fully persisted (persistreco) line."""

import argparse

import GaudiPython as GP
from Configurables import HistogramPersistencySvc
from GaudiConf.reading import do_unpacking
from Hlt2Conf.check_output import (
    check_banks,
    check_decreports,
    check_particles,
    check_persistreco,
)
from Moore import options
from PyConf.application import configure, configure_input


def error(msg):
    print("CheckOutput ERROR", msg)


parser = argparse.ArgumentParser()
parser.add_argument("i", type=str, help="Input MDF or DST")
parser.add_argument("t", type=str, help=".tck.json file from the job")

args = parser.parse_args()

# Prepare application
options.data_type = "Upgrade"
options.simulation = True
options.dddb_tag = "dddb-20171126"
options.conddb_tag = "sim-20171127-vc-md100"
options.input_files = [args.i]
options.input_type = "ROOT"
options.input_stream = "default"
options.gaudipython_mode = True
config = configure_input(options)

cf_node = do_unpacking(input_process="Spruce", has_mc_data=False)

config.update(configure(options, cf_node))

# Disable warning about histogram saving not being required
HistogramPersistencySvc(OutputLevel=5)

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()

# MonkeyPatch for the fact that RegistryEntry.__bool__
# changed in newer cppyy. Proper fix should go into Gaudi
import cppyy

cppyy.gbl.DataSvcHelpers.RegistryEntry.__bool__ = lambda x: True

nevents = 1
for ii in range(nevents):
    print("Checking next event.")
    appMgr.run(1)
    if not TES["/Event/Hlt2/DecReports"]:
        if ii == 0:
            error("No events found. Something has gone very wrong!!")
        break
    if ii == 0:
        TES.dump()

    # Check dec reports
    hlt2decisions = check_decreports(TES, decs=["Hlt2Topo2Body", "Hlt2Topo3Body"])
    sprucedecisions = check_decreports(
        TES, decs=["Spruce_Test_line_persistreco"], stage="Spruce"
    )

    for k, v in sprucedecisions.items():
        if v:
            prefix = "/Event/Spruce/" + k
            check_particles(TES, prefix.removesuffix("Decision"))

    # Check persistency of packed containers
    locations = ["Rec/ProtoP/Long", "Rec/ProtoP/Neutrals", "Rec/Calo/Electrons"]
    # default_persistable_locations(stream="/Event/Spruce/HLT2")
    check_persistreco(
        TES, ["/Event/Spruce/HLT2/" + loc for loc in locations], N=2, unexpected_locs=[]
    )

    # Specific check for persistreco
    persistedCharged = TES["/Event/Spruce/HLT2/Rec/ProtoP/Long"].size()
    persistedTracks = TES["/Event/Spruce/HLT2/Rec/Track/BestLong"].size()
    if persistedTracks < 20 or persistedCharged < 20:
        error("persistreco objects not being saved correctly.")

    # Check the Rich (=9) RawBank is present
    check_banks(TES, "default", banks=[9])
