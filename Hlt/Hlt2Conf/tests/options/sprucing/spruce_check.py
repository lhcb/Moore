###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test exclusive and passthrough (Turbo) Sprucing output.

Can check if RawBanks are present and not present
Find number->RawBank info at https://gitlab.cern.ch/lhcb/LHCb/-/blob/master/Event/DAQEvent/include/Event/RawBank.h

./run python Hlt/Hlt2Conf/tests/options/sprucing/spruce_check.py -i spruce_example_realtimereco.test.dst -t spruce_example_realtime.tck.json
-p Spruce -s test -rb 9 21 -notrb 13 73
"""

import argparse

import GaudiPython as GP
from Configurables import HistogramPersistencySvc
from GaudiConf.reading import do_unpacking
from Hlt2Conf.check_output import (
    check_banks,
    check_decreports,
    check_hlt2_topo_candidates,
    check_MCoutput,
    check_not_banks,
    check_particles,
)
from Moore import options
from PyConf.application import configure, configure_input


def error(msg):
    print("CheckOutput ERROR", msg)


parser = argparse.ArgumentParser()
parser.add_argument("-i", type=str, help="Input MDF or DST")
parser.add_argument("-t", type=str, help=".tck.json file from the job")
parser.add_argument("-p", type=str, help="input_process can be Spruce or Turbo")
parser.add_argument("-s", type=str, help="Stream to test")
parser.add_argument("-n", type=int, required=False, help="Number of events to check")
parser.add_argument("-rb", type=int, nargs="+", help="RawBanks to check")
parser.add_argument(
    "-notrb", type=int, nargs="+", help="RawBanks to check are NOT in output"
)
parser.add_argument(
    "-topo",
    action="store_true",
    help="Check that Topo candidates from HLT2 are present",
)
parser.add_argument(
    "-usemc", action="store_true", help="Use MC data from the inpu tfile"
)

args = parser.parse_args()

assert args.p == "Spruce" or args.p == "Turbo", (
    "input_process is Turbo (passthrough sprucing) or Spruce"
)

##Prepare application
options.data_type = "Upgrade"
options.simulation = True
options.dddb_tag = "dddb-20171126"
options.conddb_tag = "sim-20171127-vc-md100"
options.input_files = [args.i]
options.input_manifest_file = args.t
options.input_type = "ROOT"
options.root_ioalg_name = "RootIOAlgExt"
options.evt_max = args.n if args.n else -1
options.gaudipython_mode = True
options.input_stream = args.s
config = configure_input(options)

input_process = args.p

if input_process == "Spruce":
    RECO_ROOT = "/Event/Spruce/HLT2"
    dec_to_check = "Spruce_Test_line"

if input_process == "Turbo":
    RECO_ROOT = "/Event/HLT2"
    dec_to_check = "Passthrough"

cf_node = do_unpacking(input_process=input_process, has_mc_data=args.usemc)

config.update(configure(options, cf_node))

# Disable warning about histogram saving not being required
HistogramPersistencySvc(OutputLevel=5)

appMgr = GP.AppMgr()
TES = appMgr.evtsvc()

# MonkeyPatch for the fact that RegistryEntry.__bool__
# changed in newer cppyy. Proper fix should go into Gaudi
import cppyy

cppyy.gbl.DataSvcHelpers.RegistryEntry.__bool__ = lambda x: True

nevents = args.n if args.n else 1

for ii in range(nevents):
    print("Checking next event.")
    appMgr.run(1)
    if not TES["/Event"]:
        if ii == 0:
            error("No events found. Something has gone very wrong!!")
        break
    if ii == 0:
        TES.dump()

    hlt2reportloc = TES["/Event/Hlt2/DecReports"]
    hlt2_decs_to_check = hlt2reportloc.decReports().keys()
    ##Note this ^^ will not check streaming only that you can access hlt2 decisions
    print("hlt2_decs_to_check ", hlt2_decs_to_check)

    # Check dec reports
    if input_process == "Turbo":
        decisions = check_decreports(TES, decs=hlt2_decs_to_check)
        check_decreports(TES, decs=[dec_to_check], stage="Spruce")
    elif input_process == "Spruce":
        decisions = check_decreports(TES, decs=[dec_to_check], stage="Spruce")
        check_decreports(TES, decs=hlt2_decs_to_check)

    # Check particles and relations between them
    if input_process == "Turbo":
        for k, v in decisions.items():
            if v and "Lumi" not in k:
                prefix = "/Event/HLT2/" + k
                check_particles(TES, prefix.removesuffix("Decision"))
    elif input_process == "Spruce":
        for k, v in decisions.items():
            if v and "Lumi" not in k:
                prefix = "/Event/Spruce/" + k
                check_particles(TES, prefix.removesuffix("Decision"))

    # Check MC locations if simulation like
    if "_dstinput" in args.i:
        check_MCoutput(TES, RECO_ROOT)

    # Check rawbanks specific to this stream
    print("args.rb ", args.rb)

    if args.rb:
        check_banks(TES, args.s, banks=args.rb)

    print("args.notrb ", args.notrb)

    if args.notrb:
        check_not_banks(TES, args.s, banks=args.notrb)

    if args.topo:
        check_hlt2_topo_candidates(TES)
