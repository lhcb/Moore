###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Minimal configuration to test lumi counters and "nanofication".

For VDM processing:

we keep the raw banks for the 1 KHz line only (in a separated stream?) for debugging purposes. If something goes wrong with the lumi summary we can use those to perform the measurement with less statistics reading the raw banks.

For Physics processing:

We remove raw banks completely from now on for 30 kHz line.
We keep the raw banks for 1 kHz line. Note: 1 kHz of raw banks is equivalent to : 1kHz * 40 kBytes* 1e7 = 400 TBytes (for 1/3 of a year, which is a reasonable data taking time).
Lumi stream will have odin + hlt lumi summary
In physics streams, we can remove also the HLT lumi summary because the only thing we need in there is the counting of the lumi events to be used downstream in the FSR.
"""

from Hlt2Conf.lines.luminosity.luminosity import lumi_counters_line, lumi_nanofy_line
from Moore import options, run_moore
from Moore.lines import Hlt2Line
from Moore.streams import Stream, Streams
from PyConf.Algorithms import HltLumiSummaryMonitor
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

options.output_file = "output_{stream}.mdf"
options.output_type = "MDF"
options.evt_max = 2000
options.set_input_and_conds_from_testfiledb("2023_raw_hlt1_269939")
options.monitoring_file = "monitoring.json"
options.histo_file = "histograms.root"

from RecoConf.decoders import default_VeloCluster_source

# Use velo retina decoding:
default_VeloCluster_source.global_bind(bank_type="VPRetinaCluster")


def physics_line():
    return Hlt2Line("Hlt2Physics", algs=[], prescale=0.1)


def make_streams():
    return Streams(
        streams=[
            Stream("full", lines=[physics_line(), lumi_nanofy_line()]),
            Stream("lumi", lines=[lumi_counters_line()]),
        ]
    )


# Print the counter values
HltLumiSummaryMonitor.global_bind(OutputLevel=1)

with reconstruction.bind(from_file=False):
    config = run_moore(
        options, make_streams, public_tools=[stateProvider_with_simplified_geom()]
    )
