###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import filecmp
import os
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument("qmttest", help="Test to run")
parser.add_argument("process", help="process")
args = parser.parse_args()

options_file = args.qmttest

filetest = open(args.qmttest, "r")
lines = filetest.readlines()
filetest.close()

ntest = 5
old_op = "options.output_manifest_file"
git_op = "options.write_decoding_keys_to_git = False"

for i in range(ntest):
    options_file = f"{args.process}_{i}_test_options.py"
    if os.path.exists(options_file):
        os.remove(options_file)

    new_op = f"options.output_manifest_file = '{args.process}_{i}_configuration_options.tck.json'"
    for j in range(len(lines)):
        lines[j] = lines[j].rstrip()
        if old_op not in lines[j]:
            continue
        else:
            lines[j] = new_op + "\n" + git_op

    with open(options_file, "w") as filetest:
        for line in lines:
            filetest.write(line + "\n")

    test_file = f"./{args.process}_{i}_configuration_check.sh"

    if os.path.exists(test_file):
        os.remove(test_file)
    with open(test_file, "w") as f:
        f.write("#!/bin/bash\n")
        f.write(f"gaudirun.py -n -o {args.process}_check{i}.opts {options_file}")
        f.close()
    subprocess.call(["chmod", "+x", test_file])
    subprocess.call(test_file)

diffs = 0
compare_file = f"./{args.process}_configuration_compare.sh"
for i in range(ntest):
    for ii in range(i + 1, ntest):
        result = filecmp.cmp(
            f"{args.process}_check{i}.opts", f"{args.process}_check{ii}.opts"
        )
        print(f"{args.process}_check{i}.opts", f"{args.process}_check{ii}.opts", result)

        if not result:
            if os.path.exists(compare_file):
                os.remove(compare_file)
            with open(compare_file, "w") as f:
                f.write("#!/bin/bash\n")
                f.write(
                    f"diff -I 'FileCatalog.*' {args.process}_check{i}.opts {args.process}_check{ii}.opts"
                )
                f.close()

                subprocess.call(["chmod", "+x", compare_file])

                subprocess.call(compare_file)
                diffs += subprocess.call(compare_file)

if diffs == 0:
    print("Test Passed for configuration")
else:
    print("Test Failed for configuration")

diffs = 0
for i in range(ntest):
    for ii in range(i + 1, ntest):
        result = filecmp.cmp(
            f"{args.process}_{i}_configuration_options.tck.json",
            f"{args.process}_{ii}_configuration_options.tck.json",
        )
        print(
            f"{args.process}_{i}_configuration_options.tck.json",
            f"{args.process}_{ii}_configuration_options.tck.json",
            result,
        )

        if not result:
            if os.path.exists(compare_file):
                os.remove(compare_file)
            with open(compare_file, "w") as f:
                f.write("#!/bin/bash\n")
                f.write(
                    f"diff {args.process}_{i}_configuration_options.tck.json {args.process}_{ii}_configuration_options.tck.json"
                )
                f.close()

                subprocess.call(["chmod", "+x", compare_file])

                subprocess.call(compare_file)
                diffs += subprocess.call(compare_file)

if diffs == 0:
    print("Test Passed for json file")
else:
    print("Test Failed for json file")
