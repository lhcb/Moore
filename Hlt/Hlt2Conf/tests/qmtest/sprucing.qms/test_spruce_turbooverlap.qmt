<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->

<!--
Runs test for overlap effect of HLT2 lines in Sprucing passthrough
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="prerequisites"><set>
  <tuple><text>sprucing.test_hlt2_foroverlapcheck</text><enumeral>PASS</enumeral></tuple>
</set></argument>
<argument name="program"><text>lbexec</text></argument>
<argument name="timeout"><integer>3000</integer></argument>
<argument name="args"><set>
  <text>Hlt2Conf.Sprucing_tests:spruce_overlap</text>
  <text>$HLT2CONFROOT/options/sprucing/lbexec_yamls/spruce_overlap.yaml</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="validator"><text>

from Moore.qmtest.exclusions import remove_known_warnings
countErrorLines({"FATAL": 0, "ERROR": 0, "WARNING": 0},
                stdout=remove_known_warnings(stdout))

import re
ref_file=str(open("test_hlt2_foroverlapcheck.stdout", "r").read())

ref_matches_lineone = re.findall('LAZY_AND: (Hlt2Lineone_extraoutputs) .*Sum=(\d+)', ref_file)
matches_lineone = re.findall('(VoidFilter/Hlt2Lineone_extraoutputs_Hlt2Filter) .*Sum=(\d+)', stdout)

ref_matches_linetwo = re.findall('LAZY_AND: (Hlt2Linetwo) .*Sum=(\d+)', ref_file)
matches_linetwo = re.findall('(VoidFilter/Hlt2Linetwo_Hlt2Filter) .*Sum=(\d+)', stdout)

print(f"ref_matches_lineone is {ref_matches_lineone} and matches_lineone is {matches_lineone}")
print(f"ref_matches_linetwo is {ref_matches_linetwo} and matches_linetwo is {matches_linetwo}")

if not len(ref_matches_lineone)==len(matches_lineone)==len(ref_matches_linetwo)==len(matches_linetwo)==1:
  causes.append('No events passed line one and/or line two... something is wrong.')

if not ref_matches_lineone[0][1]==matches_lineone[0][1]:
  causes.append((f'Expected {ref_matches_lineone[0][1]} events post Sprucing for line {ref_matches_lineone[0][0]} but got {matches_lineone[0][1]}'))
else:
  print(f"HLT2 found {ref_matches_lineone[0][1]} events and passthrough Sprucing found {matches_lineone[0][1]} for line {ref_matches_lineone[0][0]} - all good")


prescale_linetwo = re.findall('(DeterministicPrescaler/Hlt2Linetwo_Prescaler) .*Eff=..\s(\d+.\d+)', stdout)
print(f"Line two prescale is {prescale_linetwo}")

if not (float(prescale_linetwo[0][1]) &gt; 40. and float(prescale_linetwo[0][1]) &lt; 60.):
  causes.append((f'Expected prescale on line two to be approx 0.5 but got {prescale_linetwo[0][1]}'))
else:
  print(f"The prescaling in Sprucing is working - got {prescale_linetwo[0][1]}% expecting approx. 50% - all good")




</text></argument>
</extension>
