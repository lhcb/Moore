<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
Test that the HLT2 and sprucing bandwidth test runs without errors.
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="program"><text>bash</text></argument>
<argument name="timeout"><integer>3600</integer></argument>
<argument name="args"><set>
  <text>$HLT2CONFROOT/tests/options/bandwidth/run_spruce_bandwidth__override_evtmax_100.sh</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="validator"><text>
from GaudiConf.QMTest.BaseTest import LineSkipper
from Moore.qmtest.exclusions import remove_known_warnings

_remove_known_warnings = remove_known_warnings + LineSkipper([
    "WARNING Lifetime fit did not converge. Aborting.",
    "WARNING Negative variance produced in lifetime fit iteration.",
    "WARNING Empty Buffers -- presumably DstData bank has no payload"
])
countErrorLines({"FATAL": 0, "ERROR": 0, "WARNING": 0},
                stdout=_remove_known_warnings(stdout))

from Hlt2Conf.tests.bandwidth.bandwidth_helpers import bandwidth_qmtest_validator
causes += bandwidth_qmtest_validator()

</text></argument>
</extension>
