###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Configuration file of a throughput test using ThOr-based selection algorithms."""

from DDDB.CheckDD4Hep import UseDD4Hep
from Hlt2Conf.settings.defaults import make_all_lines
from Moore import options, run_moore
from RecoConf.global_tools import (
    stateProvider_with_simplified_geom,
    trackMasterExtrapolator_with_simplified_geom,
)
from RecoConf.hlt2_global_reco import make_light_reco_pr_kf_without_UT
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction
from RecoConf.legacy_rec_hlt1_tracking import get_default_ut_clusters
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.ttrack_selections_reco import make_ttrack_reco

## explicitly remove tracking efficiency lines, as they call the reconstruction in the subdetectors
to_remove = [
    "Hlt2TrackEff_DiMuon_.*",
    "Hlt2TrackEff_ZToMuMu_.*",
    "Hlt2IFT_SMOG2KshortVeloLong.*",
    "Hlt2IFT_SMOG2TrackEff_DiMuon_.*",
    "Hlt2CalibMon_DiMuon.*",
    "Hlt2CalibMon_KshortVelo.*",
]

public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom(),
]

options.scheduler_legacy_mode = False

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc

    dd4hepSvc = DD4hepSvc()
    dd4hepSvc.DetectorList = [
        "/world",
        "VP",
        "FT",
        "Magnet",
        "Rich1",
        "Rich2",
        "Ecal",
        "Hcal",
        "Muon",
    ]


with (
    reconstruction.bind(from_file=False),
    make_all_lines.bind(
        lines_to_remove=to_remove, remove_lines_with_hlt1_filter_code=False
    ),
    make_light_reco_pr_kf_without_UT.bind(
        skipRich=False, skipCalo=False, skipMuon=False
    ),
    make_ttrack_reco.bind(skipCalo=False),
    get_default_ut_clusters.bind(disable_ut=True),
    hlt2_reconstruction.bind(make_reconstruction=make_light_reco_pr_kf_without_UT),
):
    config = run_moore(options, make_all_lines, public_tools)
