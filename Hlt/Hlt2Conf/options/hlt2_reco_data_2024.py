###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Hlt2Conf.settings.hlt2_binds import config_pp_2024
from Moore import options
from Moore.config import run_allen_reconstruction
from RecoConf.standalone import reco_prefilters, standalone_hlt2_global_reco

with config_pp_2024(), reco_prefilters.bind(gec=False):
    run_allen_reconstruction(options, standalone_hlt2_global_reco)
