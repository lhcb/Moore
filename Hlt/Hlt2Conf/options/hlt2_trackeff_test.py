###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running HLT2 lines.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_trackeff_test.py
"""

from Configurables import HiveDataBrokerSvc
from Hlt2Conf.lines.trackeff.DiMuonTrackEfficiency import all_lines as jpsi_lines
from Hlt2Conf.lines.trackeff.ZTrackEfficiency import all_lines as Z_lines
from Moore import options, run_moore
from RecoConf.calorimeter_reconstruction import make_digits
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.hlt2_global_reco import make_fastest_reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction
from RecoConf.reconstruction_objects import reconstruction

HiveDataBrokerSvc().OutputLevel = 5

make_digits.global_bind(calo_raw_bank=False)

options.root_ioalg_name = "RootIOAlgExt"
options.output_file = "hlt2_trackeff_alllines.dst"
options.output_type = "ROOT"
options.output_manifest_file = "hlt2_trackeff_withUT.tck.json"


def make_lines():
    return [
        builder()
        for line_dict in [jpsi_lines, Z_lines]
        for builder in line_dict.values()
    ]


public_tools = [stateProvider_with_simplified_geom()]

with (
    reconstruction.bind(from_file=False),
    hlt2_reconstruction.bind(make_reconstruction=make_fastest_reconstruction),
    make_fastest_reconstruction.bind(skipUT=False),
):
    config = run_moore(options, make_lines, public_tools)
