###############################################################################
# (c) Copyright 2025 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV
from Hlt2Conf.lines import charmonium_to_dimuon
from Hlt2Conf.lines.bandq.builders import b_hadrons
from Hlt2Conf.lines.test.hlt2_test import (
    default_bs2jpsiphi_line_with_flavourtagging,
    modified_bs2jpsiphi_line_with_flavourtagging,
)
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction


def make_lines():
    return [
        default_bs2jpsiphi_line_with_flavourtagging(),
        modified_bs2jpsiphi_line_with_flavourtagging(),
    ]


public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    config = run_moore(options, make_lines, public_tools)
