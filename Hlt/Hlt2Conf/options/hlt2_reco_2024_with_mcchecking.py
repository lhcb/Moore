###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os

from Hlt2Conf.settings.hlt2_binds import config_pp_2024_with_monitoring
from Moore import options
from Moore.config import run_allen_reconstruction
from RecoConf.mc_checking import check_track_resolution, mc_check_tracking
from RecoConf.standalone import reco_prefilters, standalone_hlt2_global_reco

isQMTTest = "QMTTEST_NAME" in os.environ
myName = os.environ["QMTTEST_NAME"] if isQMTTest else "hlt2_reco_2024_with_mcchecking"

options.histo_file = options.getProp("histo_file") or myName + "_histos.root"
options.ntuple_file = options.getProp("ntuple_file") or myName + "_ntuples.root"

with (
    config_pp_2024_with_monitoring(),
    reco_prefilters.bind(gec=False),
    standalone_hlt2_global_reco.bind(do_mc_checking=True, do_data_monitoring=True),
    check_track_resolution.bind(per_hit_resolutions=True, split_per_type=True),
    mc_check_tracking.bind(skip_UT=False),
):
    run_allen_reconstruction(options, standalone_hlt2_global_reco)
