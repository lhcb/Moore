###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options
from PyConf.Algorithms import HltLumiSummaryDecoder, LumiSummaryTuple
from PyConf.application import default_raw_banks, make_odin
from RecoConf.config import Reconstruction, run_reconstruction


def tuple_maker():
    ## odin
    odin = make_odin()

    ## lumi summary
    lumi_sum = HltLumiSummaryDecoder(
        RawBanks=default_raw_banks("HltLumiSummary"), SourceID="Hlt1"
    ).OutputContainerName

    make_tuple = LumiSummaryTuple(InputLumi=lumi_sum, ODIN=odin)

    return Reconstruction("LumiTuple", [make_tuple])


run_reconstruction(options, tuple_maker)
