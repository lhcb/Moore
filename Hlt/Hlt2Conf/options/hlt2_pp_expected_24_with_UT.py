###############################################################################
# (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Options file for running on output of Moore/Hlt/Hlt1Conf/options/hlt1_pp_expected_24_with_UT.py
Configured for full-stream lines. Can also add turbo lines as shown (commented out).
Output should be spruced before being tupled.

If you copy this code to a file with path
    Moore/hlt2_pp_expected_24_with_UT.py

Then you'll be able to run it with
    Moore/run gaudirun.py Moore/hlt2_pp_expected_24_with_UT.py
    or
    lb-run -c <a detdesc binaryTag> Moore/<latest 2024-patches release> gaudirun.py Moore/hlt2_pp_expected_24_with_UT.py

Note that if you want to use this snippet for a bandwidth estimate, you should write to .mdf, not .dst.
Note also that the output .mdf is written without compression, whereas in data-taking we
compress the HLT2 output separately in the data movers. You can compress yourselves with e.g.:
    zstd -3 <filepath>

and with this compressed output, you should get a representative file size for a bandwidth estimate.
"""

from DDDB.CheckDD4Hep import UseDD4Hep

if UseDD4Hep:
    # https://mattermost.web.cern.ch/lhcb/pl/tz5eowsiytrpfxi3br8oy1k3xy
    raise RuntimeError("""
        "Sorry, you'll need a detdesc stack to run over this MC. Please see https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/running_over_mc.html#switching-to-a-detdesc-compatible-platform".
        DD4HEP is suggested to be used on '2024.Q1.2' MC onwards. This input is 'expected-2024' and thus not suitable.
        """)
    # This input is from Hlt/Hlt1Conf/options/hlt1_pp_expected_24_without_UT.py,
    # if that script gets updated to use 2024.Q1.2 MC as input than this comment can be removed.

from typing import Callable

from Hlt2Conf.lines.semileptonic import all_lines as full_lines  # all full-stream lines
from Moore import options
from Moore.streams import Stream, Streams
from RecoConf.reconstruction_objects import reconstruction

# Input-specific options
options.conddb_tag = "sim-20231017-vc-md100"
options.dddb_tag = "dddb-20231017"
options.input_files = ["hlt1_output.dst"]
options.input_type = "ROOT"
options.input_raw_format = 0.5
options.simulation = True
options.data_type = "Upgrade"

# Output options
options.output_file = (
    "hlt2_output__full.dst"  # change full->turbo if running turbo lines
)
options.output_type = "ROOT"
options.root_ioalg_name = "RootIOAlgExt"
options.root_ioalg_opts = {"IgnorePaths": ["/Event/DAQ/RawEvent/moved/aside"]}

# Misc options
options.scheduler_legacy_mode = False
options.n_threads = 1  # can be multi-threaded
options.evt_max = -1


def _make_streams():
    streams = [
        Stream(
            "full",
            lines=[builder() for builder in full_lines.values()],
            routing_bit=98,
            detectors=[],
            # detectors=DETECTORS - import from Moore.streams
        ),  # Turbo or Full case - no detector raw banks, for TurCal add them as above.
        # Add turbo lines as well if you like:
        # Would need to import e.g. from Hlt2Conf.lines.b2oc import all_lines as turbo_lines
        # Stream(
        #    "turbo",
        #    lines=[builder() for builder in turbo_lines.values()],
        #    routing_bit=99,
        #    detectors=[]
        # )
    ]
    return Streams(streams=streams)


def make_streams_with_binds(real_make_streams: Callable = _make_streams) -> Streams:
    from Hlt2Conf.settings.hlt2_binds import config_pp_2024

    # This is implicitly _with_UT.
    # `Import config_pp_2024_without_UT` instead to allow running Hlt2 without the UT.
    with config_pp_2024():
        return real_make_streams()


with reconstruction.bind(from_file=False):
    options.lines_maker = make_streams_with_binds
    from Moore.production import hlt2

    # Rather than processing via Moore:run_moore, using Moore.production:hlt2 emulates MC centralised processing.
    # This enables removal of prescales+postscales for all lines + adds a passthrough line + instantiates public_tools
    config = hlt2(options, "--flagging")
