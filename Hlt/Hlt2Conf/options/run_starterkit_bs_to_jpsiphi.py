###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
With a stack set up, run as
Moore/run gaudirun.py '$HLT2CONFROOT/options/run_starterkit_bs_to_jpsiphi.py' 2>&1 | tee starterkit_bs_to_jpsiphi_minbias.log
"""

# For production modules, we would use
# from Hlt2Conf.lines.<wg>.<module> import <line>
from Hlt2Conf.lines.starterkit import all_lines
from Moore import options, run_moore
from RecoConf.decoders import default_ft_decoding_version
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.hlt2_global_reco import make_fastest_reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction
from RecoConf.reconstruction_objects import reconstruction

# Minimum bias means that collisions are simulated as they would happen during data-taking and all events are kept.
# This is contrary to generator settings in which a certain decay is required to happen, as in our signal MC.
minbias = True
hadronic = False

if minbias:
    default_ft_decoding_version.global_bind(value=2)
    # hlt1_filtered means that only events that passed hlt1 have been persisted in this minbias sample.
    options.set_input_and_conds_from_testfiledb("upgrade_minbias_hlt1_filtered")
    options.input_raw_format = 4.3
else:
    default_ft_decoding_version.global_bind(value=6)
    options.input_type = "ROOT"
    if hadronic:
        options.input_files = [
            f"root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00143671/0000/00143671_000000{i}_1.xdigi"
            for i in [
                "14",
                "30",
                "26",
                "34",
                "33",
                "32",
                "35",
                "37",
                "38",
                "43",
                "40",
                "44",
                "46",
                "42",
                "51",
                "50",
                "53",
                "52",
                "54",
                "55",
                "56",
                "58",
                "59",
                "65",
                "66",
                "72",
                "80",
                "87",
                "84",
                "88",
                "89",
                "90",
                "94",
                "98",
                "67",
                "45",
                "95",
                "82",
                "77",
                "47",
                "36",
            ]
        ]
    else:
        options.input_files = [
            f"root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/Upgrade/XDIGI/00143675/0000/00143675_000000{i}_1.xdigi"
            for i in [
                "15",
                "26",
                "37",
                "34",
                "41",
                "44",
                "47",
                "43",
                "46",
                "49",
                "45",
                "51",
                "55",
                "56",
                "57",
                "59",
                "61",
                "63",
                "65",
                "66",
                "70",
                "76",
                "79",
                "81",
                "82",
                "85",
                "88",
                "91",
                "96",
                "97",
                "98",
                "99",
                "64",
                "23",
                "95",
                "33",
                "84",
                "48",
                "80",
                "87",
                "54",
            ]
        ]
    options.conddb_tag = "sim-20210617-vc-md100"
    options.dddb_tag = "dddb-20210617"
    # We only need to register an output file when running on signal MC, as we don't expect to find anything in minbias.
    options.output_file = "hlt2_starterkit_bs_to_jpsiphi.dst"
    options.output_type = "ROOT"

options.evt_max = 3600
options.print_freq = 360
options.input_type = "ROOT"
options.msg_svc_format = "% F%56W%S%7W%R%T %0W%M"


def get_lines():
    return [line_builder() for line_builder in all_lines.values()]


public_tools = [stateProvider_with_simplified_geom()]

if minbias:
    run_moore(options, get_lines, public_tools)
else:
    options.output_manifest_file = "hlt2_starterkit_bs_to_jpsiphi.tck.json"
    with (
        reconstruction.bind(from_file=False),
        hlt2_reconstruction.bind(make_reconstruction=make_fastest_reconstruction),
    ):
        config = run_moore(options, get_lines, public_tools)
