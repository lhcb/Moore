###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Configuration file of a throughput test using ThOr-based selection algorithms."""

from Hlt2Conf.settings.defaults import make_all_lines
from Moore import options, run_moore
from Moore.lines import optimize_controlflow
from RecoConf.global_tools import (
    stateProvider_with_simplified_geom,
    trackMasterExtrapolator_with_simplified_geom,
)
from RecoConf.hlt2_global_reco import make_fastest_reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction
from RecoConf.reconstruction_objects import reconstruction

public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom(),
]

options.scheduler_legacy_mode = False
options.output_file = "hlt2_pp_thor.mdf"
options.output_type = "MDF"

with (
    reconstruction.bind(from_file=False),
    make_all_lines.bind(remove_lines_with_hlt1_filter_code=False),
    optimize_controlflow.bind(optimization="default"),
    hlt2_reconstruction.bind(make_reconstruction=make_fastest_reconstruction),
):
    config = run_moore(options, make_all_lines, public_tools)
