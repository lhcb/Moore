###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Example option on how to test on any specific Hlt2 line in B2OC.

# Run like any other options file with:

    ./Moore/run gaudirun.py hlt2_b2oc_example.py

# To run on a specific line, BdToDsmK_DsmToKpKmPim in this example, that
# is booked by unique function in hlt2_b2oc.py:

from Hlt2Conf.lines.b_to_open_charm.hlt2_b2oc import BdToDsmK_DsmToKpKmPim

all_lines = {}
from Hlt2Conf.lines.b_to_open_charm.hlt2_b2oc import make_hlt2_lines
default_lines = [
     'BdToDsmK_DsmToKpKmPim',
]
extra_config = {
    'flavour_tagging': [
        'BdToDsmK_DsmToKpKmPim',
    ]
}
make_hlt2_lines(
    line_dict=all_lines,
    all_lines=default_lines,
    extra_config=extra_config)
"""

###############################################################################
# configure which lines to test
###############################################################################
# Run on ALL B2OC lines
from DDDB.CheckDD4Hep import UseDD4Hep
from Hlt2Conf.lines.b_to_open_charm import all_lines
from Hlt2Conf.settings.hlt2_binds import config_pp_2024
from Moore import options, run_moore
from Moore.streams import DETECTORS, Stream, Streams
from RecoConf.global_tools import (
    stateProvider_with_simplified_geom,
    trackMasterExtrapolator_with_simplified_geom,
)
from RecoConf.reconstruction_objects import reconstruction

###############################################################################
# configure input data set
###############################################################################

options.set_input_and_conds_from_testfiledb("hlt2_bandwidth_input_2024")
options.input_raw_format = 0.5
options.ioalg_buffer_nb_events = 300

# Set a reasonable number of events
options.evt_max = 1000

###############################################################################
# configure output files
###############################################################################

# Write the output file
options.output_file = "hlt2_{stream}.mdf"
options.output_type = "MDF"
options.output_manifest_file = "hlt2_b2oc_lines.tck.json"
# Hlt2 monitoring histos
options.histo_file = "hlt2_b2oc_monitoring.root"

###############################################################################
# run Moore
###############################################################################


def make_streams() -> Streams:
    streams = [
        Stream(
            "b2oc",
            lines=[builder() for builder in all_lines.values()],  # whole module
            routing_bit=85,  # some dummy value != 94 or 95
            # lines=[line_to_run()] # single line
            detectors=[],
        )  # Turbo and Full case - no detector raw banks
        # detectors=DETECTORS) # if persisting detector raw banks i.e. special cases or TurCal lines
    ]
    return Streams(streams=streams)


options.scheduler_legacy_mode = False
if UseDD4Hep:
    # This needs to happen before the public tools are instantiated,
    # which means we cannot put it inside make_streams().
    from PyConf.Tools import TrackMasterExtrapolator, TrackMasterFitter

    TrackMasterExtrapolator.global_bind(
        ApplyMultScattCorr=False,
        ApplyEnergyLossCorr=False,
        ApplyElectronEnergyLossCorr=False,
    )
    TrackMasterFitter.global_bind(ApplyMaterialCorrections=False)


public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom(),
]
with reconstruction.bind(from_file=False), config_pp_2024():
    config = run_moore(options, make_streams, public_tools, exclude_incompatible=True)
