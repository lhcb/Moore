###############################################################################
# (c) Copyright 2019-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Example option on how to test on any specific Sprucing line in B2OC.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_spruce_example.py

To run on a specific line that is booked by unique function (in spruce_b2oc.py):

from Hlt2Conf.lines.b_to_open_charm.spruce_b2oc import BdToDsmK_DsmToKpKmPim
sprucing_lines = {}
from Hlt2Conf.lines.b_to_open_charm.spruce_b2oc import make_sprucing_lines
default_lines = [
    'BdToDsmK_DsmToKpKmPim',
]
extra_config = {
    'flavour_tagging': [
        'BdToDsmK_DsmToKpKmPim',
    ]
}
make_sprucing_lines(
    line_dict=all_lines,
    all_lines=default_lines,
    extra_config=extra_config)
"""

###############################################################################
# configure which lines to test
###############################################################################
# Run on ALL B2OC lines:
from Hlt2Conf.lines.b_to_open_charm import sprucing_lines
from Moore import options, run_moore
from Moore.lines import optimize_controlflow
from Moore.monitoring import run_default_monitoring
from Moore.persistence.hlt2_tistos import list_of_full_stream_lines
from Moore.streams import Stream, Streams
from PyConf.Algorithms import VeloRetinaClusterTrackingSIMD
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.global_tools import (
    stateProvider_with_simplified_geom,
    trackMasterExtrapolator_with_simplified_geom,
)
from RecoConf.legacy_rec_hlt1_tracking import make_VeloClusterTrackingSIMD
from RecoConf.reconstruction_objects import reconstruction
from SprucingConfig.Spruce24.fullline_config.pp_Collision24c4 import (
    full_lines as full_lines_for_TISTOS,
)

###############################################################################
# configure input data set
###############################################################################

# Configure input data from testfiledb
input_files = [
    "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/EoY-spruce-2024/307781_00090010_0105.raw",
    "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/EoY-spruce-2024/307781_00090011_0074.raw",
    "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/EoY-spruce-2024/307781_00090012_0106.raw",
    "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/EoY-spruce-2024/307781_00090015_0004.raw",
    "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/dpa/wp1/data/EoY-spruce-2024/307781_00090015_0073.raw",
]
options.input_files = input_files

options.input_raw_format = 0.5
options.input_type = "RAW"
options.simulation = False
options.data_type = "Upgrade"
options.geometry_version = "run3/2024.Q1.2-v00.00"
options.conditions_version = "master"
options.input_process = "Hlt2"
options.scheduler_legacy_mode = False

# Set a reasonable number of events
options.evt_max = 100

###############################################################################
# configure output files
###############################################################################

# Write the output file
options.output_file = "spruce_b2oc_lines.dst"
options.output_type = "ROOT"
options.output_manifest_file = "spruce_b2oc_lines.tck.json"
# Hlt2 monitoring histos
options.histo_file = "spruce_b2oc_monitoring.root"

sprucing_lines.pop("SpruceB2OC_BdToDsmK_DsmToHHH_FEST")


def make_streams() -> Streams:
    streams = [
        Stream(
            "b2oc",
            lines=[builder() for builder in sprucing_lines.values()],  # whole module
            routing_bit=85,  # some dummy value != 94 or 95
            # lines=[line_to_run()] # single line
            detectors=[],
        )  # Turbo and Full case - no detector raw banks
        # detectors=DETECTORS) # if persisting detector raw banks i.e. special cases or TurCal lines
    ]
    return Streams(streams=streams)


from DDDB.CheckDD4Hep import UseDD4Hep

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc

    dd4hepSvc = DD4hepSvc()
    dd4hepSvc.ConditionsLocation = (
        "git:/cvmfs/lhcb.cern.ch/lib/lhcb/git-conddb/lhcb-conditions-database.git"
    )
    # This needs to happen before the public tools are instantiated,
    # which means we cannot put it inside make_streams().
    from PyConf.Tools import TrackMasterExtrapolator, TrackMasterFitter

    TrackMasterExtrapolator.global_bind(
        ApplyMultScattCorr=False,
        ApplyEnergyLossCorr=False,
        ApplyElectronEnergyLossCorr=False,
    )
    TrackMasterFitter.global_bind(ApplyMaterialCorrections=False)

default_VeloCluster_source.global_bind(bank_type="VPRetinaCluster")
make_VeloClusterTrackingSIMD.global_bind(algorithm=VeloRetinaClusterTrackingSIMD)

public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom(),
]

with (
    list_of_full_stream_lines.bind(lines=full_lines_for_TISTOS),
    reconstruction.bind(from_file=True, spruce=True),
    optimize_controlflow.bind(optimization="default"),
    run_default_monitoring.bind(run=False),
):
    config = run_moore(options, make_streams, public_tools=public_tools)
