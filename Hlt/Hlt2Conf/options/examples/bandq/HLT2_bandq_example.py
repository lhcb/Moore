###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running a set of HLT2 lines -> mdf to calculate BW on high-mu HLT1-filtered real data.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_bandwidth_real_data_example.py
    file_size = ls -lh --si hlt2_bandwidth_real_data_example.mdf
    b/w (GB/s) = input rate (1290 kHz for this min bias) x file_size (MB) / options.evt_max

Note also that the output .mdf is written without compression, whereas in data-taking we
compress the HLT2 output separately in the data movers. You can do the same compression with e.g.:
    zstd -3 <filepath>

and with this compressed output, you should get a representative file size for a bandwidth estimate.
"""

from DDDB.CheckDD4Hep import UseDD4Hep
from Hlt2Conf.lines.bandq import all_lines
from Hlt2Conf.lines.charmonium_to_dimuon import all_lines as charmonium_to_dimuon_lines
from Hlt2Conf.lines.charmonium_to_dimuon_detached import (
    all_lines as charmonium_to_dimuon_detached_lines,
)

# Current reconstruction configuration for 2024
from Hlt2Conf.settings.hlt2_binds import config_pp_2024
from Moore import options, run_moore
from Moore.streams import Stream, Streams
from RecoConf.global_tools import (
    stateProvider_with_simplified_geom,
    trackMasterExtrapolator_with_simplified_geom,
)
from RecoConf.reconstruction_objects import reconstruction

if not UseDD4Hep:
    raise RuntimeError(
        "Sorry, you'll need a DD4HEP stack to run over real data. Please see https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/running_over_mc.html#switching-to-a-detdesc-compatible-platform"
    )

options.set_input_and_conds_from_testfiledb("hlt2_bandwidth_input_2024")

options.evt_max = 50
options.simulation = False
options.scheduler_legacy_mode = False
options.input_raw_format = 0.5
options.input_type = "MDF"

options.geometry_version = "run3/2024.Q1.2-v00.00"
options.conditions_version = "master"

options.output_file = "hlt2_bandwidth_real_data_example_dummystream.mdf"
options.output_type = "MDF"
options.output_manifest_file = "hlt2_bandwidth_real_data_example.tck.json"


def make_streams():
    all_bandq_lines = [builder() for builder in all_lines.values()]
    all_bandq_lines += [
        builder() for builder in charmonium_to_dimuon_detached_lines.values()
    ]
    all_bandq_lines += [builder() for builder in charmonium_to_dimuon_lines.values()]
    streams = [
        Stream(
            "dummystream",
            lines=all_bandq_lines,  # whole module
            routing_bit=85,  # some dummy value != 94 or 95
            # lines=[line_to_run()] # single line
            detectors=[],
        )  # Turbo and Full case - no detector raw banks
        # detectors=DETECTORS) # if persisting detector raw banks i.e. special cases or TurCal lines
    ]
    return Streams(streams=streams)


public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom(),
]
with reconstruction.bind(from_file=False), config_pp_2024():
    config = run_moore(options, make_streams, public_tools)
