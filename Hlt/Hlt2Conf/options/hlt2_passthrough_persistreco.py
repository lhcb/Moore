###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test PersistReco with pass-through line (defined with `algs=[]`). Produces hlt2_passthrough_persistreco.mdf

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_passthrough_persistreco.py
"""

from Moore import options, run_moore
from Moore.lines import Hlt2Line
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

options.set_input_and_conds_from_testfiledb("expected_2024_BdToKstgamma_xdigi")

options.evt_max = 100
options.output_file = "hlt2_passthrough_persistreco.mdf"
options.output_type = "MDF"
options.output_manifest_file = "hlt2_passthrough_persistreco.tck.json"


def pass_through_line(name="Hlt2Passthrough"):
    """Return a HLT2 line that performs no selection but runs and persists the reconstruction"""
    return Hlt2Line(name=name, prescale=1, algs=[], persistreco=True)


def make_lines():
    return [pass_through_line()]


public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    config = run_moore(options, make_lines, public_tools)
