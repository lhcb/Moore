###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Options file for running on HLT1-filtered *real data* from after the June Technical Stop (with UT in reconstruction).
Configured for full-stream lines. Can also add turbo lines as shown (commented out).
Output should be spruced before being tupled.

If you copy this code to a file with path
    Moore/hlt2_pp_realdata_without_UT.py

Then you'll be able to run it with
    Moore/run gaudirun.py Moore/hlt2_pp_realdata_without_UT.py

REQUIRES A DD4HEP STACK TO RUN OVER REAL DATA

The HLT1 output rate corresponding to this sample was 1 MHz, and mu=4.9.
Feel free to keep checking under /eos/lhcb/wg/rta/WP3/bandwidth_division/ for new samples!

Note that the output .mdf is written without compression, whereas in real life it will be
compressed separately by the data movers. You can compress yourselves with e.g.:
    zstd -3 <filepath>

and with this compressed output, you should get a representative file size.
"""

from DDDB.CheckDD4Hep import UseDD4Hep
from Hlt2Conf.lines.semileptonic import all_lines as full_lines  # all full-stream lines
from Moore import options, run_moore
from Moore.streams import Stream, Streams
from RecoConf.reconstruction_objects import reconstruction

if not UseDD4Hep:
    raise RuntimeError(
        "Sorry, you'll need a DD4HEP stack (default stack platform will do) to run over real data. Check lb-stack-setup README.md for instructions."
    )

# Input-specific options
options.set_input_and_conds_from_testfiledb("hlt2_pp_realdata_with_UT")
options.input_raw_format = 0.5

# Output options
options.output_file = "hlt2_output__{stream}.mdf"
options.output_type = "MDF"

# Misc options
options.scheduler_legacy_mode = False
options.n_threads = 1  # can be multi-threaded
options.evt_max = 50  # Around 100k in the files


def make_streams():
    streams = [
        Stream(
            "full",
            lines=[builder() for builder in full_lines.values()],
            routing_bit=98,
            detectors=[],
            # detectors=DETECTORS - import from Moore.streams
        ),  # Turbo or Full case - no detector raw banks, for TurCal add them as above.
        # Add turbo lines as well if you like:
        # Would need to import e.g. from Hlt2Conf.lines.qee import hlt2_turbo_lines as turbo_lines
        # Stream(
        #    "turbo",
        #    lines=[builder() for builder in turbo_lines.values()],
        #    routing_bit=99,
        #    detectors=[]
        # )
    ]
    return Streams(streams=streams)


# This needs to happen before the public tools are instantiated,
# which means we cannot put it inside make_streams().
from PyConf.Tools import TrackMasterExtrapolator, TrackMasterFitter

TrackMasterExtrapolator.global_bind(
    ApplyMultScattCorr=False,
    ApplyEnergyLossCorr=False,
    ApplyElectronEnergyLossCorr=False,
)
TrackMasterFitter.global_bind(ApplyMaterialCorrections=False)

public_tools = []
from Hlt2Conf.settings.hlt2_binds import config_pp_2024

# This is implicitly _with_UT.
# `Import config_pp_2024_without_UT` instead to allow running Hlt2 without the UT.
with reconstruction.bind(from_file=False), config_pp_2024():
    config = run_moore(options, make_streams, public_tools)
