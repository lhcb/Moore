###############################################################################
# (c) Copyright 2021-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test checking PV tracks relations to (original) tracks on output of topo{2,3} persistreco hlt2 lines (original reco real time)."""

from Moore import options
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.reconstruction_objects import reconstruction

input_files = ["hlt2_2or3bodytopo_realtime.mdf"]

options.input_raw_format = 0.3
options.input_process = "Hlt2"
options.input_files = input_files
options.input_manifest_file = "hlt2_2or3bodytopo_realtime.tck.json"
options.input_type = "MDF"

options.evt_max = -1
options.simulation = True
options.dddb_tag = "dddb-20171126"
options.conddb_tag = "sim-20171127-vc-md100"


def monitor_algorithms() -> Reconstruction:
    # main reco
    reco_handles = reconstruction()
    data = []

    # monitors
    from PyConf.Algorithms import PVToTracksRelationsMonitor

    data += [
        PVToTracksRelationsMonitor(
            PVs=reco_handles["PVs"], Tracks=reco_handles["VeloTracks"], ErrorMode=True
        )
    ]

    return Reconstruction("monitor_algorithms", data)


with reconstruction.bind(from_file=True, spruce=True):
    run_reconstruction(options, monitor_algorithms)
