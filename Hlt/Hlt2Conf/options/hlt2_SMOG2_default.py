###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import mm
from Hlt2Conf.lines.ift import all_lines
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.legacy_rec_hlt1_tracking import make_TrackBeamLineVertexFinderSoA_pvs
from RecoConf.reconstruction_objects import reconstruction

options.output_file = "hlt2_pp_SMOG2_default.dst"
options.output_type = "ROOT"
options.output_manifest_file = "hlt2_pp_SMOG2_default.tck.json"


def make_lines():
    return [builder() for builder in all_lines.values()]


public_tools = [stateProvider_with_simplified_geom()]

with reconstruction.bind(from_file=False):
    with make_TrackBeamLineVertexFinderSoA_pvs.bind(minz=-541 * mm):
        config = run_moore(options, make_lines, public_tools)
