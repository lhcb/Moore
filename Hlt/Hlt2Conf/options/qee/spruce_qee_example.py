###############################################################################
# (c) Copyright 2019-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Run the QEE sprucing lines on output of single- and di-muon HLT persistreco lines from Z->MuMu. Produces spruce_fromfilereco.dst

Run on the output of hlt2_qee_example_persistreco.py

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_qee_spruce_example.py
"""

from Hlt2Conf.lines.qee import sprucing_lines
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

input_files = ["hlt2_qee_zmumu.mdf"]

options.input_files = input_files
options.input_type = "MDF"
options.input_manifest_file = "hlt2_qee_zmumu.tck.json"
options.evt_max = -1
options.simulation = True
options.dddb_tag = "dddb-20201211"
options.conddb_tag = "sim-20201218-vc-mu100"
options.output_file = "qee_zmumu_spruce.dst"
options.output_type = "ROOT"
options.output_manifest_file = "qee_zmumu_spruce.tck.json"


def make_lines():
    return [line() for line in sprucing_lines.values()]


public_tools = [stateProvider_with_simplified_geom()]

with reconstruction.bind(
    from_file=True, spruce=True, manifest_file=options.input_manifest_file
):
    config = run_moore(options, make_lines, public_tools)
