###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Hlt2Conf.settings.hlt2_VdM import make_streams
from Moore import options, run_moore
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.reconstruction_objects import reconstruction

options.lines_maker = make_streams

with (
    reconstruction.bind(from_file=False),
    default_VeloCluster_source.bind(bank_type="VP"),
):
    config = run_moore(options, public_tools=[])
