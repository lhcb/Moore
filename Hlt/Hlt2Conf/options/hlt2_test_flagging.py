###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running HLT2 lines using hlt2 function with MC flagging.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_test_flagging.py
"""

from Hlt2Conf.lines.test.hlt2_test import hlt2_test_lines
from Moore import options
from Moore.production import hlt2
from RecoConf.hlt2_global_reco import make_light_reco_pr_kf
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction
from RecoConf.reconstruction_objects import reconstruction

options.evt_max = 200
options.output_file = "hlt2_test_production.dst"
options.output_manifest_file = "hlt2_test_production.tck.json"
options.output_type = "ROOT"
options.root_ioalg_name = "RootIOAlgExt"


def make_lines(make_lines=None):
    if make_lines:
        return make_lines()
    return [builder() for builder in hlt2_test_lines.values()]


options.lines_maker = make_lines

# find all arguments in Moore.production module
hlt2_mc_args = ("--flagging", "--velo-source=VPRetinaCluster")

with (
    reconstruction.bind(from_file=False),
    hlt2_reconstruction.bind(make_reconstruction=make_light_reco_pr_kf),
):
    config = hlt2(options, *hlt2_mc_args)
