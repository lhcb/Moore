###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test persistreco when using real time reco. Produces hlt2_2or3bodytopo_realtime_neutral_dst.dst for further spruce check of the persistency of neutral particles.

HLT2 writes a DST as will be the case for simulation
"""

from Hlt2Conf.lines.topological_b import threebody_line, twobody_line
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

options.root_ioalg_name = "RootIOAlgExt"
options.output_file = "hlt2_2or3bodytopo_realtime_neutral_dst.dst"
options.output_type = "ROOT"
options.output_manifest_file = "hlt2_2or3bodytopo_realtime_neutral_dst.tck.json"


def make_lines():
    return [twobody_line(persistreco=True), threebody_line(persistreco=True)]


public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    config = run_moore(options, make_lines, public_tools, exclude_incompatible=False)
