###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for testing the streaming of HLT2 lines.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_streaming.py
"""

from Moore import options, run_moore
from Moore.config import get_allen_hlt1_decision_ids
from Moore.lines import Hlt2Line
from Moore.streams import Stream, Streams
from Moore.tcks import dump_hlt2_configuration
from RecoConf.global_tools import stateProvider_with_simplified_geom

# options.set_input_and_conds_from_testfiledb('MiniBrunel_2018_MinBias_FTv4_MDF')
options.input_files = [
    "/scratch/rmatev/Run_0000238820_20220720-084749-135_HCEB01_0001.mdf"
]
options.dddb_tag = "upgrade/master"
options.conddb_tag = "upgrade/master"
options.input_type = "MDF"
options.evt_max = 200
options.output_file = "hlt2_test_data.{stream}.mdf"
options.output_type = "MDF"
options.simulation = True

import Functors as F
from PyConf.Algorithms import VoidFilter
from RecoConf.calorimeter_reconstruction import make_calo_reduced
from RecoConf.reconstruction_objects import reconstruction


def calo_activity_line(name="Hlt2_Calo_activity", prescale=1):
    calo_clusters = make_calo_reduced()
    # digits_ecal = calo_clusters["digitsEcal"]
    clusters_ecal = calo_clusters["ecalClusters"]
    # activity_filter = VoidFilter(name="CaloActivityFilter", Cut = F.SIZE(digits_ecal)>500)
    activity_filter = VoidFilter(
        name="CaloActivityFilter", Cut=F.SIZE(clusters_ecal) > 100
    )
    return Hlt2Line(name=name, algs=[activity_filter], prescale=prescale)


def hlt1_nobias_line(name="Hlt2_Hlt1nobias", prescale=1):
    # intentionally have some overlap with the other line in order to test
    # the routing bits in events going to both streams.
    return Hlt2Line(
        name=name, algs=[], hlt1_filter_code="Hlt1_ActivityDecision", prescale=prescale
    )


# def lumi_line():
# pass through lumi


def make_streams():
    stream_A = Stream(
        name="stream_A",
        lines=[calo_activity_line()],
        routing_bit=87,
        detectors=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )

    stream_B = Stream(
        name="stream_B",
        lines=[hlt1_nobias_line()],
        routing_bit=88,
        detectors=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )

    return Streams(streams=[stream_A, stream_B])


def make_lines():
    my_lines = [calo_activity_line(), hlt1_nobias_line()]
    return my_lines


public_tools = [stateProvider_with_simplified_geom()]


def hack_hlt1_decision_ids():
    decs = {"Hlt1_ActivityDecision": 1}
    print("I actually use this")
    return decs


with reconstruction.bind(from_file=False):
    with get_allen_hlt1_decision_ids.substitute(hack_hlt1_decision_ids):
        config = run_moore(options, make_streams, public_tools)
dump_hlt2_configuration(config, "hlt2_activity_trigger_2022.tck.json")
