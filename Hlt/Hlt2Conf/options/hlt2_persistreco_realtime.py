###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test persistreco when using real time reco. Produces hlt2_testpersistreco_realtime.dst

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_persistreco_realtime.py
"""

import Functors as F
from Functors import require_all
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from GaudiKernel.SystemOfUnits import micrometer as um
from Moore import options, run_moore
from Moore.lines import Hlt2Line
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.event_filters import require_pvs
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import (
    make_pvs,
    reconstruction,
    upfront_reconstruction,
)
from RecoConf.standard_particles import make_has_rich_long_kaons

###Define 2 orthoganol lines (PIDK cut) with and without persistreco


def make_charm_kaons(pidk_cut):
    """Return kaons maker for D0 decay selection."""
    pvs = make_pvs()
    code = require_all(
        F.MINIPCUT(IPCut=60 * um, Vertices=pvs),
        F.PT > 800 * MeV,
        F.P > 5 * GeV,
        pidk_cut,
    )
    return ParticleFilter(make_has_rich_long_kaons(), F.FILTER(code))


def make_dzeros(particle1, particle2, name, descriptor):
    """Return D0 maker with selection tailored for two-body hadronic final
    states.

    Args:
        particles (list of DataHandles): Input particles used in the
                                         combination.
        name (string): Combiner name for identification.
        descriptor (string): Decay descriptor to be reconstructed.
    """
    pvs = make_pvs()
    combination_cut = require_all(
        F.MASS > 1685 * MeV,
        F.MASS < 2045 * MeV,
        F.PT > 2 * GeV,
        F.MAX(F.PT) > 1000 * MeV,
        F.MAXDOCACUT(0.1 * mm),
    )
    vertex_cut = require_all(
        F.MASS > 1715 * MeV,
        F.MASS < 2015 * MeV,
        F.CHI2DOF < 10.0,
        F.BPVFDCHI2(pvs) > 25.0,
        F.BPVDIRA(pvs) > 0.99985,
    )

    return ParticleCombiner(
        [particle1, particle2],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_cut,
        CompositeCut=vertex_cut,
    )


def test_persistreco_line(name="Hlt2_test_persistreco", prescale=1, persistreco=True):
    kaons = make_charm_kaons(pidk_cut=F.PID_K > 5)
    dzeros = make_dzeros(
        particle1=kaons,
        particle2=kaons,
        name="Charm_D0ToHH_D0ToKmKp_persistreco",
        descriptor="D0 -> K- K+",
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs())] + [dzeros],
        prescale=prescale,
        persistreco=persistreco,
    )


def test_nopersistreco_line(
    name="Hlt2_test_nopersistreco", prescale=1, persistreco=False
):
    kaons = make_charm_kaons(pidk_cut=F.PID_K < 5)
    dzeros = make_dzeros(
        particle1=kaons,
        particle2=kaons,
        name="Charm_D0ToHH_D0ToKmKp_nopersistreco",
        descriptor="D0 -> K- K+",
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs())] + [dzeros],
        prescale=prescale,
        persistreco=persistreco,
    )


options.root_ioalg_name = "RootIOAlgExt"
options.root_ioalg_opts = {"IgnorePaths": ["/Event/Rec/Summary"]}

options.evt_max = 1000


def make_lines():
    return [test_persistreco_line(), test_nopersistreco_line()]


public_tools = [stateProvider_with_simplified_geom()]

with reconstruction.bind(from_file=False):
    config = run_moore(options, make_lines, public_tools)
