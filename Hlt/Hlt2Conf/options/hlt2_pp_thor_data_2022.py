###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Configuration file to test running on data.
"""

from Hlt2Conf.lines import all_lines
from Moore import options, run_moore
from Moore.config import filter_lines as remove_lines
from PyConf.Tools import TrackMasterFitter
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.hlt2_global_reco import make_light_reco_pr_kf_without_UT
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction
from RecoConf.hlt2_tracking import (
    make_PrKalmanFilter_noUT_tracks,
    make_PrKalmanFilter_Seed_tracks,
    make_PrKalmanFilter_Velo_tracks,
    make_TrackBestTrackCreator_tracks,
)
from RecoConf.legacy_rec_hlt1_tracking import get_default_ut_clusters
from RecoConf.reconstruction_objects import reconstruction

options.histo_file = "histos_hlt2_pp_thor_data_2022.root"
options.output_file = "data_hlt2_pp_thor_data_2022.mdf"
options.output_type = "MDF"

options.event_store = "EvtStoreSvc"

# Explicitly remove tracking efficiency lines using the UT.
# Track eff lines use the TrackMasterFitter which used the DetailedMaterialLocator.
# The latter is not supported in DD4HEP and leads to crashes, see https://gitlab.cern.ch/lhcb/Rec/-/issues/429.
to_remove = ["Hlt2TrackEff_.*_MuonUT.*", "Hlt2TrackEff_.*_Downstream.*"]


def make_lines():
    trunc_lines = all_lines
    for remove in to_remove:
        trunc_lines = remove_lines(trunc_lines, remove)
    print(
        "Manually removed lines due to DD4HEP: ", all_lines.keys() - trunc_lines.keys()
    )
    return [builder() for builder in trunc_lines.values()]


# Note the following lines are commented out as the DD4HEP build would automatically change to the detailed geometry (11/2022)
# from RecoConf.global_tools import stateProvider_with_simplified_geom, trackMasterExtrapolator_with_simplified_geom
# public_tools = [
#     trackMasterExtrapolator_with_simplified_geom(),
#     stateProvider_with_simplified_geom()
# ]
public_tools = []

options.scheduler_legacy_mode = False

with (
    reconstruction.bind(from_file=False),
    make_light_reco_pr_kf_without_UT.bind(
        skipRich=False, skipCalo=False, skipMuon=False
    ),
    make_TrackBestTrackCreator_tracks.bind(max_chi2ndof=8.0),
    make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=8.0),
    make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=8.0),
    make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=8.0),
    default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),
    get_default_ut_clusters.bind(disable_ut=True),
    TrackMasterFitter.bind(FastMaterialApproximation=True),
    hlt2_reconstruction.bind(make_reconstruction=make_light_reco_pr_kf_without_UT),
):
    config = run_moore(options, make_lines, public_tools)
