###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for running HLT2 lines.

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_noUT_trackeff_test.py
"""

from Configurables import HiveDataBrokerSvc
from Hlt2Conf.lines.trackeff.DiMuonTrackEfficiency import all_lines as jpsi_lines
from Hlt2Conf.lines.trackeff.ZTrackEfficiency import all_lines as Z_lines
from Moore import options, run_moore
from Moore.config import filter_lines as remove_lines
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction as reconstruction

HiveDataBrokerSvc().OutputLevel = 5

options.output_file = "hlt2_trackeff_noUT.dst"
options.output_type = "ROOT"
options.output_manifest_file = "hlt2_trackeff_noUT.tck.json"

# Remove lines which need UT
muonut_to_remove = "Hlt2TrackEff_.*_MuonUT.*"
downstream_to_remove = "Hlt2TrackEff_.*_Downstream.*"

all_lines = {**jpsi_lines, **Z_lines}
hlt2_lines = remove_lines(all_lines, muonut_to_remove)
trackeff_lines = remove_lines(hlt2_lines, downstream_to_remove)

print("Removed lines: ", all_lines.keys() - trackeff_lines.keys())

print("Number of HLT2 lines {}".format(len(trackeff_lines)))


def make_lines():
    return [builder() for builder in trackeff_lines.values()]


public_tools = [stateProvider_with_simplified_geom()]

with reconstruction.bind(from_file=False):
    config = run_moore(options, make_lines, public_tools)
