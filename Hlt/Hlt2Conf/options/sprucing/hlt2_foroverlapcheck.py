###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""

HLT2 options to test overlap of HLT2 lines and the solution to this using Sprucing

"""

from Hlt2Conf.lines.test.spruce_test import Test_extraoutputs_hlt2_line
from Hlt2Conf.lines.topological_b import threebody_line
from Moore import options, run_moore
from Moore.lines import Hlt2Line
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

options.set_input_and_conds_from_testfiledb("expected_2024_BdToKstgamma_xdigi")

options.evt_max = 1000

options.output_file = "hlt2_foroverlapcheck.mdf"
options.output_type = "MDF"
options.output_manifest_file = "hlt2_foroverlapcheck.tck.json"
# following line makes the dict of all lines in all streams output to a json using the `get_line_attributes` of `Stream` object
options.write_streams_attributes_to_json = True


def make_lines():
    return [
        # Bespoke version of `b2kstgamma_line` that has extra_outputs and all the other persistency options set to true
        Test_extraoutputs_hlt2_line("Hlt2Lineone_extraoutputs"),
        # Normal 3body TOPO line that should have good efficiency on BdToKstgamma
        threebody_line("Hlt2Linetwo", persistreco=False),
        # Line that does not make any output at `/Particles`
        Hlt2Line(name="Hlt2Linethree", persistreco=True, algs=[], raw_banks=["Muon"]),
    ]


public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    config = run_moore(options, make_lines, public_tools, exclude_incompatible=False)
