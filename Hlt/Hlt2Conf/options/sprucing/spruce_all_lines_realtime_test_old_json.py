###############################################################################
# (c) Copyright 2021-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test running all Sprucing lines on topo{2,3} persistreco hlt2 output (use real time reco). Produces spruce_all_lines_realtimereco_newPacking.dst

Use this for rate tests

Run like any other options file:

    ./Moore/run gaudirun.py spruce_all_lines_realtime.py
"""

from Hlt2Conf.lines import sprucing_lines
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

##Run over HLT1 filtered Min bias sample that has been processed by TOPO{2, 3} HLT2 lines.
##To produce this see `Hlt/Hlt2Conf/options/Sprucing/hlt2_2or3bodytopo_realtime.py`

options.input_raw_format = 0.3
options.input_process = "Hlt2"
options.set_input_and_conds_from_testfiledb("spruce_all_lines_realtime_test_old_json")
options.input_manifest_file = "root://eoslhcb.cern.ch//eos/lhcb/wg/rta/samples/mc/Hlt1Hlt2filtered_MinBias_sprucing/hlt2_2or3bodytopo_realtime_newPacking.tck.json"

options.output_file = "spruce_all_lines_realtimereco_newPacking.dst"
options.output_type = "ROOT"
options.output_manifest_file = "spruce_all_lines_realtime_newPacking.tck.json"


def make_lines():
    return [builder() for builder in sprucing_lines.values()]


public_tools = [stateProvider_with_simplified_geom()]

with reconstruction.bind(from_file=True, spruce=True):
    config = run_moore(options, make_lines, public_tools)
