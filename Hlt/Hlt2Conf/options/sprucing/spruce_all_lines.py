###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Configuration file of a Sprucing throughput test on all Sprucing lines."""

import logging

from Hlt2Conf.lines import sprucing_lines
from Moore import options, run_moore
from Moore.monitoring import run_default_monitoring
from RecoConf.global_tools import (
    stateProvider_with_simplified_geom,
    trackMasterExtrapolator_with_simplified_geom,
)
from RecoConf.reconstruction_objects import reconstruction

log = logging.getLogger()

from Hlt2Conf.sprucing_settings.fixed_line_configs import (
    lines_for_TISTOS_BW_March2024 as lines_for_TISTOS,
)
from Moore.persistence.hlt2_tistos import list_of_full_stream_lines

options.input_raw_format = 0.5
options.input_process = "Hlt2"
options.output_file = "spruce_all_lines.dst"
options.output_type = "ROOT"

lines_to_remove = [
    "SpruceB2OC_BdToDsmK_DsmToHHH_FEST",
    "SpruceIFT_SMOG2TrackEff_DiMuon_SeedMuon_mum_Match",
    "SpruceIFT_SMOG2TrackEff_DiMuon_SeedMuon_mum_Tag",
    "SpruceIFT_SMOG2TrackEff_DiMuon_SeedMuon_mup_Match",
    "SpruceIFT_SMOG2TrackEff_DiMuon_SeedMuon_mup_Tag",
    "SpruceIFT_SMOG2TrackEff_DiMuon_VeloMuon_mum_Match",
    "SpruceIFT_SMOG2TrackEff_DiMuon_VeloMuon_mum_Tag",
    "SpruceIFT_SMOG2TrackEff_DiMuon_VeloMuon_mup_Match",
    "SpruceIFT_SMOG2TrackEff_DiMuon_VeloMuon_mup_Tag",
]


def make_lines():
    for line in lines_to_remove:
        if line in sprucing_lines.keys():
            print("Removing {line_name}".format(line_name=line))
            sprucing_lines.pop(line)
    return [builder() for builder in sprucing_lines.values()]


public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom(),
]

options.scheduler_legacy_mode = False

with (
    reconstruction.bind(from_file=True, spruce=True),
    list_of_full_stream_lines.bind(lines=lines_for_TISTOS),
    run_default_monitoring.bind(run=False),
):
    config = run_moore(options, make_lines, public_tools)
