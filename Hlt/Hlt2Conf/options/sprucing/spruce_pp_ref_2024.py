###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Job option to make exclusive Sprucing cache.
"""

from Configurables import ApplicationMgr, Gaudi__RootCnvSvc
from Gaudi.Configuration import EventPersistencySvc
from Hlt2Conf.sprucing_settings.Sprucing_production_pp_ref_2024 import (
    excl_spruce_production,
)
from Moore import options
from RecoConf.reconstruction_objects import reconstruction

options.output_file = "spruce_example_realtimereco_{stream}.dst"
options.output_type = "ROOT"
##Next line needed as Hlt/Moore/tests/options/mdf_input_and_conds_hlt2.py fixes it to 0.0
options.persistreco_version = 1.0

# Next few lines are needed for building functor caches from this option file
ApplicationMgr().ExtSvc += ["Gaudi::IODataManager/IODataManager"]
rootSvc = Gaudi__RootCnvSvc("RootCnvSvc")
EventPersistencySvc().CnvServices += [rootSvc]
ApplicationMgr().ExtSvc += [rootSvc]

with reconstruction.bind(from_file=True, spruce=True):
    config = excl_spruce_production(options)
