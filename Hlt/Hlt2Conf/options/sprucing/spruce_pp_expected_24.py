###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Options file for running on Full-stream output of Moore/Hlt/Hlt2Conf/options/hlt2_pp_expected_24_with_UT.py

.dst output can be tupled with FunTuple.

If you copy this code to a file with path
    Moore/spruce_pp_expected_24.py

Then you'll be able to run it with
    Moore/run gaudirun.py Moore/spruce_pp_expected_24.py
"""

from Hlt2Conf.lines.semileptonic import sprucing_lines
from Moore import options, run_moore
from Moore.streams import Stream, Streams
from PyConf.reading import reconstruction as reco_spruce
from PyConf.reading import upfront_reconstruction as upfront_spruce
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

# Input-specific options
options.conddb_tag = "sim-20231017-vc-md100"
options.dddb_tag = "dddb-20231017"
options.input_files = ["hlt2_output__full.dst"]
options.input_type = "ROOT"
options.input_raw_format = 0.5
options.simulation = True
options.data_type = "Upgrade"

# Output options
options.output_file = "spruce_full_stream_output__{stream}.dst"
options.output_type = "ROOT"

# Misc options
options.scheduler_legacy_mode = False
options.input_process = "Hlt2"
options.evt_max = -1


def make_streams():
    streams = [
        Stream(
            "semileptonic",
            lines=[builder() for builder in sprucing_lines.values()],
            detectors=[],
        )
    ]
    return Streams(streams=streams)


public_tools = [stateProvider_with_simplified_geom()]
with (
    reconstruction.bind(from_file=True, spruce=True),
    reco_spruce.bind(simulation=True),
    upfront_spruce.bind(simulation=True),
):
    config = run_moore(options, make_streams, public_tools)
