###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Options file for running on Turbo-stream output of Moore/Hlt/Hlt2Conf/options/hlt2_pp_early2024_realdata_without_UT.py

.dst output can be tupled with FunTuple.

If you copy this code to a file with path
    Moore/spruce_pp_early2024_realdata.py

Then you'll be able to run it with
    Moore/run gaudirun.py Moore/spruce_pp_early2024_realdata.py

REQUIRES A DD4HEP STACK TO RUN OVER REAL DATA
"""

from DDDB.CheckDD4Hep import UseDD4Hep
from Hlt2Conf.lines.semileptonic import sprucing_lines
from Moore import options, run_moore
from Moore.streams import Stream, Streams
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction

if not UseDD4Hep:
    raise RuntimeError(
        "Sorry, you'll need a DD4HEP stack (default stack platform will do) to run over real data. Check lb-stack-setup README.md for instructions."
    )

# Input-specific options
options.geometry_version = "run3/2024.Q1.2-v00.00"
options.conditions_version = "master"
options.dddb_tag = "run3/2024.Q1.2-v00.00"
options.simulation = False
options.input_files = ["hlt2_output__full.mdf"]
options.input_type = "MDF"
options.input_raw_format = 0.5
options.data_type = "Upgrade"

# Output options
options.output_file = "spruce_full_stream_output__{stream}.dst"
options.output_type = "ROOT"

# Misc options
options.scheduler_legacy_mode = False
options.input_process = "Hlt2"
options.evt_max = -1


def make_streams():
    streams = [
        Stream(
            "semileptonic",
            lines=[builder() for builder in sprucing_lines.values()],
            detectors=[],
        )
    ]
    return Streams(streams=streams)


public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=True, spruce=True):
    config = run_moore(options, make_streams, public_tools)
