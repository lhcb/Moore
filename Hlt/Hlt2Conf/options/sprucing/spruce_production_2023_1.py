###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Job option specifically to make 2023_1 (7-8th July) exclusive Sprucing cache.
"""

from Configurables import ApplicationMgr, Gaudi__RootCnvSvc
from Gaudi.Configuration import EventPersistencySvc
from Hlt2Conf.sprucing_settings.Sprucing_2023_1_production import excl_spruce_production
from Moore import options
from RecoConf.reconstruction_objects import reconstruction

options.output_file = "spruce_example_realtimereco_{stream}.dst"
options.output_type = "ROOT"

# Next few lines are needed for building functor caches from this option file
ApplicationMgr().ExtSvc += ["Gaudi::IODataManager/IODataManager"]
rootSvc = Gaudi__RootCnvSvc("RootCnvSvc")
EventPersistencySvc().CnvServices += [rootSvc]
ApplicationMgr().ExtSvc += [rootSvc]

with reconstruction.bind(from_file=True, spruce=True):
    config = excl_spruce_production(options)
