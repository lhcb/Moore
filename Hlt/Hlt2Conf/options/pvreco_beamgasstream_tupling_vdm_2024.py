###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options
from PyConf.Algorithms import HltRoutingBitsFilter, LumiPVs_nobeamline
from PyConf.application import default_raw_banks, make_odin
from PyConf.reading import get_decreports
from RecoConf.config import Reconstruction, run_reconstruction
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.legacy_rec_hlt1_tracking import all_velo_track_types


def tuple_maker():
    phys_filter = HltRoutingBitsFilter(
        RawBanks=default_raw_banks("HltRoutingBits"), RequireMask=(0x0, 0x0, 0x80000000)
    )
    tuple_maker = LumiPVs_nobeamline(
        ODIN=make_odin(),
        InputVeloTracks=all_velo_track_types()["v1"],
        InputHlt=get_decreports("Hlt1"),
    )
    return Reconstruction("BeamGasTuple", [tuple_maker], [phys_filter])


with default_VeloCluster_source.bind(bank_type="VPRetinaCluster"):
    run_reconstruction(options, tuple_maker)
