###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test persistreco when using real time reco. Produces hlt2_2or3bodytopo_realtime.mdf

Run like any other options file:

    ./Moore/run gaudirun.py hlt2_2or3bodytopo_realtime_with_extras.py
"""

from Hlt2Conf.lines.topological_b import (
    line_prefilters,
    make_filtered_topo_threebody,
    make_filtered_topo_twobody,
)
from Moore import options, run_moore
from Moore.lines import Hlt2Line
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction, upfront_reconstruction

options.set_input_and_conds_from_testfiledb("expected_2024_BdToKstgamma_xdigi")


def twobody_with_extras_line(
    name="Hlt2_Topo2Body_with_extras", prescale=1, persistreco=True
):
    candidates = make_filtered_topo_twobody(MVACut=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + line_prefilters() + [candidates],
        prescale=prescale,
        calo_clusters=True,
        calo_digits=True,
        pv_tracks=True,
        track_ancestors=True,
        persistreco=persistreco,
    )


def threebody_with_extras_line(
    name="Hlt2_Topo3Body_with_extras", prescale=1, persistreco=True
):
    candidates = make_filtered_topo_threebody(MVACut=0.0)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + line_prefilters() + [candidates],
        prescale=prescale,
        calo_clusters=True,
        calo_digits=True,
        pv_tracks=True,
        track_ancestors=True,
        persistreco=persistreco,
    )


def make_lines():
    return [
        twobody_with_extras_line(persistreco=True),
        threebody_with_extras_line(persistreco=True),
    ]


options.evt_max = 100

options.output_file = "hlt2_2or3bodytopo_realtime_with_extras.mdf"
options.output_type = "MDF"
options.output_manifest_file = "hlt2_2or3bodytopo_realtime_with_extras.tck.json"

public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False):
    config = run_moore(options, make_lines, public_tools, exclude_incompatible=False)
