###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import ApplicationMgr, Gaudi__RootCnvSvc
from Gaudi.Configuration import EventPersistencySvc
from Hlt2Conf.lines import all_lines
from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom

# Following 2 lines needed to create cache for streaming HDRFilters
options.output_file = "hlt2_pp_default.dst"
options.output_type = "ROOT"

# Next few lines are needed for building functor caches from this option file
ApplicationMgr().ExtSvc += ["Gaudi::IODataManager/IODataManager"]
rootSvc = Gaudi__RootCnvSvc("RootCnvSvc")
EventPersistencySvc().CnvServices += [rootSvc]
ApplicationMgr().ExtSvc += [rootSvc]


def make_lines():
    return [builder() for builder in all_lines.values()]


public_tools = [stateProvider_with_simplified_geom()]
run_moore(options, make_lines, public_tools)
