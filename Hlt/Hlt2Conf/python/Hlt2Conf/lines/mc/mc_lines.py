###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.lines import Hlt2Line, SpruceLine
from Moore.streams import DETECTORS

MC_HLT2_PASSTHROUGH_LINE_NAME = "Hlt2MCPassThrough"
MC_HLT2_PASSTHROUGH_PERSISTRECO_LINE_NAME = "Hlt2MCPassThroughPersistReco"
MC_HLT2_PASSTHROUGH_RAWBANKS_LINE_NAME = "Hlt2MCPassThroughRawBanks"
MC_HLT2_PASSTHROUGH_PERSISTRECO_RAWBANKS_LINE_NAME = (
    "Hlt2MCPassThroughPersistRecoRawBanks"
)

MC_SPRUCE_PASSTHROUGH_LINE_NAME = "SpruceMCPassThrough"
MC_SPRUCE_PASSTHROUGH_PERSISTRECO_LINE_NAME = "SpruceMCPassThroughPersistReco"
MC_SPRUCE_PASSTHROUGH_RAWBANKS_LINE_NAME = "SpruceMCPassThroughRawBanks"
MC_SPRUCE_PASSTHROUGH_PERSISTRECO_RAWBANKS_LINE_NAME = (
    "SpruceMCPassThroughPersistRecoRawBanks"
)


def make_hlt2_passthrough_line(name=MC_HLT2_PASSTHROUGH_LINE_NAME):
    """Line to select all events in flagging mode."""
    print(f"Adding {name} for simulation")
    return Hlt2Line(name=name, algs=[], prescale=1)


def make_hlt2_passthrough_persistreco_line(
    name=MC_HLT2_PASSTHROUGH_PERSISTRECO_LINE_NAME,
):
    """Line to select all events in flagging mode.

    persistreco=True emulates Brunel.

    """
    print(f"Adding {name} for simulation")
    return Hlt2Line(name=name, algs=[], prescale=1, persistreco=True)


def make_hlt2_passthrough_rawbanks_line(name=MC_HLT2_PASSTHROUGH_RAWBANKS_LINE_NAME):
    """Line to select all events in flagging mode.

    persistreco=True emulates Brunel.

    """
    print(f"Adding {name} for simulation")
    return Hlt2Line(name=name, algs=[], prescale=1, raw_banks=DETECTORS)


def make_hlt2_passthrough_persistreco_rawbanks_line(
    name=MC_HLT2_PASSTHROUGH_PERSISTRECO_RAWBANKS_LINE_NAME,
):
    """Line to select all events in flagging mode.

    persistreco=True emulates Brunel.

    """
    print(f"Adding {name} for simulation")
    return Hlt2Line(
        name=name, algs=[], prescale=1, persistreco=True, raw_banks=DETECTORS
    )


def make_spruce_passthrough_line(name=MC_SPRUCE_PASSTHROUGH_LINE_NAME):
    """Line to select all events in flagging mode."""
    print(f"Adding {name} for simulation")
    return SpruceLine(name=name, algs=[], prescale=1)


def make_spruce_passthrough_persistreco_line(
    name=MC_SPRUCE_PASSTHROUGH_PERSISTRECO_LINE_NAME,
):
    """Line to select all events in flagging mode."""
    print(f"Adding {name} for simulation")
    return SpruceLine(name=name, algs=[], prescale=1, persistreco=True)


def make_spruce_passthrough_rawbanks_line(
    name=MC_SPRUCE_PASSTHROUGH_RAWBANKS_LINE_NAME,
):
    """Line to select all events in flagging mode."""
    print(f"Adding {name} for simulation")
    return SpruceLine(name=name, algs=[], prescale=1, raw_banks=DETECTORS)


def make_spruce_passthrough_persistreco_rawbanks_line(
    name=MC_SPRUCE_PASSTHROUGH_PERSISTRECO_RAWBANKS_LINE_NAME,
):
    """Line to select all events in flagging mode."""
    print(f"Adding {name} for simulation")
    return SpruceLine(
        name=name, algs=[], prescale=1, persistreco=True, raw_banks=DETECTORS
    )
