###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV, MeV

from .builders.b_builder import make_b2hhmunu
from .builders.base_builder import (
    make_fake_muons_from_b_notIsMuon,
    make_fake_protons_from_b,
    make_kaons_from_b,
    make_muons_from_b,
    make_pions_from_b,
    make_protons_from_b,
)
from .builders.hh_builder import make_hh

"""
SL lines for the B->pplnu, B->(rho->pipi)lnu, and B->(phi->KK)lnu decays. The decay descriptors use intermediate states with short lifetimes to have the hadrons come from the B vertex
"""


def make_b2ppbarmunu(process):
    """
    Selection for the B->ppmunu and muonic B->pptaunu decays
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    protons = make_protons_from_b(pt_min=800 * MeV, mipchi2_min=9.0)

    ppbar = make_hh([protons, protons], "J/psi(1S) -> p+ p~-")
    muons = make_muons_from_b(p_min=3.0 * GeV)
    line_alg = make_b2hhmunu([ppbar, muons], "[B+ -> J/psi(1S) mu+]cc")

    return line_alg


def make_b2ppbarmunu_ss(process):
    """
    Selection for the B->ppmunu and muonic B->pptaunu decays with same-sign protons
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    protons = make_protons_from_b(pt_min=800 * MeV, mipchi2_min=9.0)
    ppbar = make_hh([protons, protons], "[J/psi(1S) -> p+ p+]cc")
    muons = make_muons_from_b(p_min=3.0 * GeV)
    line_alg = make_b2hhmunu([ppbar, muons], "[B+ -> J/psi(1S) mu+]cc")

    return line_alg


def make_b2ppbarmunu_fakep(process):
    """
    Selection for the B->ppmunu and muonic B->pptaunu decays with a fake proton of opposite sign to muon
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    protons = make_protons_from_b(pt_min=800 * MeV, mipchi2_min=9.0)
    fake_protons = make_fake_protons_from_b(pt_min=800 * MeV, mipchi2_min=9.0)

    ppbar = make_hh([protons, fake_protons], "J/psi(1S) -> p+ p~-")
    muons = make_muons_from_b(p_min=3.0 * GeV)
    line_alg = make_b2hhmunu([ppbar, muons], "[B+ -> J/psi(1S) mu+]cc")

    return line_alg


def make_b2ppbarmunu_fakemu(process):
    """
    Selection for the B->ppmunu and muonic B->pptaunu decays with a fake muon
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    protons = make_protons_from_b(pt_min=800 * MeV, mipchi2_min=9.0)

    ppbar = make_hh([protons, protons], "J/psi(1S) -> p+ p~-")
    muons = make_fake_muons_from_b_notIsMuon(p_min=3.0 * GeV)
    line_alg = make_b2hhmunu([ppbar, muons], "[B+ -> J/psi(1S) mu+]cc")

    return line_alg


def make_b2pipimunu(process):
    """
    Selection for the B->pipimunu and muonic B->pipitaunu decays
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    pions = make_pions_from_b(pt_min=400.0 * MeV, p_min=3000.0 * MeV, mipchi2_min=9.0)

    pipi = make_hh(
        [pions, pions],
        "rho(770)0 -> pi+ pi-",
        m_min=0,
        m_max=2200.0 * MeV,
        pt_min=1000.0 * MeV,
        fdchi2_min=50.0,
        bpvdira_min=0.98,
        comb_pt_any_min=900.0 * MeV,
        vtx_chi2pdof_max=4.0,
    )
    muons = make_muons_from_b()
    line_alg = make_b2hhmunu([pipi, muons], "[B+ -> rho(770)0 mu+]cc")

    return line_alg


def make_b2pipimunu_ss(process):
    """
    Selection for the B->pipimunu and muonic B->pipitaunu decays with same-sign pions
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    pions = make_pions_from_b(pt_min=400.0 * MeV, p_min=3000.0 * MeV, mipchi2_min=9.0)
    pipi = make_hh(
        [pions, pions],
        "[rho(770)0 -> pi+ pi+]cc",
        m_min=0,
        m_max=2200.0 * MeV,
        pt_min=1000.0 * MeV,
        fdchi2_min=50.0,
        bpvdira_min=0.98,
        comb_pt_any_min=900.0 * MeV,
        vtx_chi2pdof_max=4.0,
    )
    muons = make_muons_from_b()
    line_alg = make_b2hhmunu([pipi, muons], "[B+ -> rho(770)0 mu+]cc")

    return line_alg


def make_b2pipimunu_fakemu(process):
    """
    Selection for the B->pipimunu and muonic B->pipitaunu decays with a fake muon
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    pions = make_pions_from_b(pt_min=400.0 * MeV, p_min=3000.0 * MeV, mipchi2_min=9.0)

    pipi = make_hh(
        [pions, pions],
        "rho(770)0 -> pi+ pi-",
        m_min=0,
        m_max=2200.0 * MeV,
        pt_min=1000.0 * MeV,
        fdchi2_min=50.0,
        bpvdira_min=0.98,
        comb_pt_any_min=900.0 * MeV,
        vtx_chi2pdof_max=4.0,
    )
    muons = make_fake_muons_from_b_notIsMuon()
    line_alg = make_b2hhmunu([pipi, muons], "[B+ -> rho(770)0 mu+]cc")

    return line_alg


def make_b2pipimunu_fakepi(process):
    """
    Selection for the B->pipimunu and muonic B->pipitaunu decays with fake pions
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    pions = make_pions_from_b(
        pt_min=400.0 * MeV, p_min=3000.0 * MeV, mipchi2_min=9.0, pid=None
    )

    pipi = make_hh(
        [pions, pions],
        "rho(770)0 -> pi+ pi-",
        m_min=0,
        m_max=2200.0 * MeV,
        pt_min=1000.0 * MeV,
        fdchi2_min=50.0,
        bpvdira_min=0.98,
        comb_pt_any_min=900.0 * MeV,
        vtx_chi2pdof_max=4.0,
    )
    muons = make_fake_muons_from_b_notIsMuon()
    line_alg = make_b2hhmunu([pipi, muons], "[B+ -> rho(770)0 mu+]cc")

    return line_alg


def make_b2kkmunu(process):
    """
    Selection for the B->KKmunu and muonic B->KKtaunu decays
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    kaons = make_kaons_from_b(pt_min=400.0 * MeV, p_min=3000.0 * MeV, mipchi2_min=9.0)

    kk = make_hh(
        [kaons, kaons],
        "phi(1020) -> K+ K-",
        m_min=0,
        m_max=2200.0 * MeV,
        pt_min=1000.0 * MeV,
        fdchi2_min=50.0,
        bpvdira_min=0.98,
        vtx_chi2pdof_max=4.0,
    )
    muons = make_muons_from_b()
    line_alg = make_b2hhmunu([kk, muons], "[B+ -> phi(1020) mu+]cc")

    return line_alg


def make_b2kkmunu_ss(process):
    """
    Selection for the B->KKmunu and muonic B->KKtaunu decays with same-sign kaons
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    kaons = make_kaons_from_b(pt_min=400.0 * MeV, p_min=3000.0 * MeV, mipchi2_min=9.0)
    kk = make_hh(
        [kaons, kaons],
        "[phi(1020) -> K+ K+]cc",
        m_min=0,
        m_max=2200.0 * MeV,
        pt_min=1000.0 * MeV,
        fdchi2_min=50.0,
        bpvdira_min=0.98,
        vtx_chi2pdof_max=4.0,
    )
    muons = make_muons_from_b()
    line_alg = make_b2hhmunu([kk, muons], "[B+ -> phi(1020) mu+]cc")

    return line_alg


def make_b2kkmunu_fakemu(process):
    """
    Selection for the B->KKmunu and muonic B->KKtaunu decays with a fake muon
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    kaons = make_kaons_from_b(pt_min=400.0 * MeV, p_min=3000.0 * MeV, mipchi2_min=9.0)

    kk = make_hh(
        [kaons, kaons],
        "phi(1020) -> K+ K-",
        m_min=0,
        m_max=2200.0 * MeV,
        pt_min=1000.0 * MeV,
        fdchi2_min=50.0,
        bpvdira_min=0.98,
        vtx_chi2pdof_max=4.0,
    )
    muons = make_fake_muons_from_b_notIsMuon()
    line_alg = make_b2hhmunu([kk, muons], "[B+ -> phi(1020) mu+]cc")

    return line_alg
