###############################################################################
# (C) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Booking of semileptonic hlt2 lines, notice PROCESS = 'hlt2'

Output:
updated dictionary of hlt2_lines

Note:
    Line builders have ``PROCESS`` as argument to allow ad hoc settings.
"""

from Moore.config import Hlt2Line, register_line_builder

from Hlt2Conf.lines.semileptonic.sl_monitoring import generate_default_monitoring_vars

from .builders import sl_line_prefilter
from .HbToHbprimeLNu import make_bctobsx
from .HbToHcLNu import (
    make_b0todplnu_dptokkpi,
    make_b0todplnu_dptokkpi_fakelepton,
    make_b0todplnu_dptokpipi,
    make_b0todplnu_dptokpipi_fakelepton,
    make_bctojpsilnu_jpsitomumu,
    make_bctojpsilnu_jpsitomumu_fakelepton,
    make_bstodslnu_dstokkpi,
    make_bstodslnu_dstokkpi_fakelepton,
    make_bstodstlnu_dsttodsgamma_dstokkpi_gammatoee,
    make_butod0lnu_d0tok3pi,
    make_butod0lnu_d0tok3pi_fakelepton,
    make_butod0lnu_d0tokpi,
    make_butod0lnu_d0tokpi_fakelepton,
    make_lbtolclnu_lctopkpi,
    make_lbtolclnu_lctopkpi_fakelepton,
    make_lbtolclnu_lctoV0h,
    make_lbtolclnu_lctoV0h_fakelepton,
    make_omegabtoomegacmunu_omegactopkkpi,
    make_omegabtoomegacmunu_omegactopkkpi_fakelepton,
    make_xib0toxicplusmunu_xicplustopkpi,
    make_xib0toxicplusmunu_xicplustopkpi_fakelepton,
    make_xibminustoxic0munu_xic0topkkpi,
    make_xibminustoxic0munu_xic0topkkpi_fakelepton,
)
from .HbToHcTauNu_TauToLNuNu import (
    make_b0todptaunu_dptokpipi_tautolnunu,
    make_b0todptaunu_dptokpipi_tautolnunu_fakelepton,
    make_bctojpsitaunu_jpsitomumu_tautolnunu,
    make_bctojpsitaunu_jpsitomumu_tautolnunu_fakelepton,
    make_bstodstaunu_dstokkpi_tautolnunu,
    make_bstodstaunu_dstokkpi_tautolnunu_fakelepton,
    make_butod0taunu_d0tok3pi_tautolnunu,
    make_butod0taunu_d0tok3pi_tautolnunu_fakelepton,
    make_butod0taunu_d0tokpi_tautolnunu,
    make_butod0taunu_d0tokpi_tautolnunu_fakelepton,
    make_lbtolctaunu_lctopkpi_tautolnu,
    make_lbtolctaunu_lctopkpi_tautolnu_fakelepton,
    make_lbtolctaunu_lctoV0h_tautolnu,
    make_lbtolctaunu_lctoV0h_tautolnu_fakelepton,
    make_omegabtoomegactaunu_omegactopkkpi_tautomununu,
    make_omegabtoomegactaunu_omegactopkkpi_tautomununu_fakelepton,
    make_xib0toxicplustaunu_xicplustopkpi_tautomununu,
    make_xib0toxicplustaunu_xicplustopkpi_tautomununu_fakelepton,
    make_xibminustoxic0taunu_xic0topkkpi_tautomununu,
    make_xibminustoxic0taunu_xic0topkkpi_tautomununu_fakelepton,
)
from .HbToHcTauNu_TauToPiPiPiNu import (
    make_b0todptaunu_dptokpipi_tautopipipinu,
    make_bctojpsitaunu_jpsitomumu_tautopipipinu,
    make_bptod0taunu_d0tok3pi_tautopipipinu,
    make_bptod0taunu_d0tokpi_tautopipipinu,
    make_bstodstaunu_dstokkpi_tautopipipinu,
    make_lbtolctaunu_lctopkpi_tautopipipinu,
    make_lbtolctaunu_lctopphi_tautopipipinu,
    make_lbtolctaunu_lctoV0h_tautopipipinu,
    make_omegabtoomegactaunu_omegactopkkpi_tautopipipinu,
    make_xibminustoxic0taunu_xic0topkkpi_tautopipipinu,
)
from .HbToHHLNu import (
    make_b2kkmunu,
    make_b2kkmunu_fakemu,
    make_b2kkmunu_ss,
    make_b2pipimunu,
    make_b2pipimunu_fakemu,
    make_b2pipimunu_ss,
    make_b2ppbarmunu,
    make_b2ppbarmunu_fakemu,
    make_b2ppbarmunu_fakep,
    make_b2ppbarmunu_ss,
)
from .HbToHuLNu import (
    make_b0topimunu,
    make_b0topimunu_fakek,
    make_b0topimunu_fakemu,
    make_bstokenu,
    make_bstokenu_fakeelectron,
    make_bstokenu_fakek,
    make_bstokmunu,
    make_bstokmunu_fakek,
    make_bstokmunu_fakemu,
    make_lbtopmunu,
    make_lbtopmunu_fakemu,
    make_lbtopmunu_fakep,
    make_lbtopmunu_ss,
    make_lbtopmunu_ss_fakemu,
    make_lbtopmunu_ss_fakep,
)
from .HbToHuTauNu_TauToLNuNu import (
    make_bstoktaunu_muonic,
    make_bstoktaunu_muonic_fakek,
    make_bstoktaunu_muonic_fakemu,
)
from .HbToHuTauNu_TauToPiPiPiNu import (
    make_b0topitaunu_hadronic,
    make_bstoktaunu_hadronic,
    make_lbtoptaunu_tautopipipinu,
)
from .HbToLLLNu import (
    make_b2eeenu,
    make_b2eeenu_ss,
    make_b2egammanu_cnvdd,
    make_b2egammanu_cnvll,
    make_b2emumunu,
    make_b2emumunu_ss,
    make_b2mueenu,
    make_b2mueenu_ss,
    make_b2mugammanu_cnvdd,
    make_b2mugammanu_cnvll,
    make_b2mumumunu,
    make_b2mumumunu_ss,
)
from .HbToTauNu_BTracking import (
    get_btracking_extra_outputs,
    get_btracking_raw_banks,
    get_mc_checker,
    make_b2dpipi_d2kpipi_with_btracking,
    make_b2taunu_tau2pipipi_with_btracking,
)

PROCESS = "hlt2"
hlt2_lines = {}
all_lines = hlt2_lines
_MONITORING_VARIABLES = ("pt", "nLongTracks", "eta", "n_candidates", "vchi2", "ipchi2")


@register_line_builder(hlt2_lines)
def hlt2_bptod0taunu_d0tokpipipi_tautopipipinu_line(
    name="Hlt2SLB_BuToD0TauNu_D0ToKPiPiPi_TauToPiPiPiNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay B+ -> D~0 tau+, D~0 -> K- Pi- Pi+ Pi+, hadronic tau decay.
    """
    line_alg = make_bptod0taunu_d0tok3pi_tautopipipinu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctojpsitaunu_jpsitomumu_tautomununu_line(
    name="Hlt2SLB_BcToJpsiTauNu_JpsiToMuMu_TauToMuNuNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Bc -> J/psi(1S)(-> mu mu) tau(-> mu nu nu) nu.
    """
    line_alg = make_bctojpsitaunu_jpsitomumu_tautolnunu(process=PROCESS, lepton="mu")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctojpsitaunu_jpsitomumu_fakemuon_line(
    name="Hlt2SLB_BcToJpsiTauNu_JpsiToMuMu_TauToMuNuNu_FakeMuon",
    prescale=0.1,
    persistreco=True,
):
    """
    SL Hlt2 line for the decay Bc -> J/psi(1S)(-> mu mu) tau(-> mu nu nu) nu, with a fake muon.
    """
    line_alg = make_bctojpsitaunu_jpsitomumu_tautolnunu_fakelepton(
        process=PROCESS, lepton="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctojpsitaunu_jpsitomumu_tautoenunu_line(
    name="Hlt2SLB_BcToJpsiTauNu_JpsiToMuMu_TauToENuNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Bc -> J/psi(1S)(-> mu mu) tau(-> e nu nu) nu.
    """
    line_alg = make_bctojpsitaunu_jpsitomumu_tautolnunu(process=PROCESS, lepton="e")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctojpsitaunu_jpsitomumu_fakeelectron_line(
    name="Hlt2SLB_BcToJpsiTauNu_JpsiToMuMu_TauToENuNu_FakeElectron",
    prescale=0.1,
    persistreco=True,
):
    """
    SL Hlt2 line for the decay Bc -> J/psi(1S)(-> mu mu) tau(-> e nu nu) nu, with a fake electron.
    """
    line_alg = make_bctojpsitaunu_jpsitomumu_tautolnunu_fakelepton(
        process=PROCESS, lepton="e"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b0todptaunu_dptokpipi_tautomununu_line(
    name="Hlt2SLB_B0ToDpTauNu_DpToKPiPi_TauToMuNuNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay B0 -> D+(-> K pi pi) tau(-> mu nu nu) nu.
    """
    line_alg = make_b0todptaunu_dptokpipi_tautolnunu(process=PROCESS, lepton="mu")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b0todptaunu_dptokpipi_fakemuon_line(
    name="Hlt2SLB_B0ToDpTauNu_DpToKPiPi_TauToMuNuNu_FakeMuon",
    prescale=0.1,
    persistreco=True,
):
    """
    SL Hlt2 line for the decay B0 -> D+(-> K pi pi) tau(-> mu nu nu) nu, with a fake muon.
    """
    line_alg = make_b0todptaunu_dptokpipi_tautolnunu_fakelepton(
        process=PROCESS, lepton="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b0todptaunu_dptokpipi_tautoenunu_line(
    name="Hlt2SLB_B0ToDpTauNu_DpToKPiPi_TauToENuNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay B0 -> D+(-> K pi pi) tau(-> e nu nu) nu.
    """
    line_alg = make_b0todptaunu_dptokpipi_tautolnunu(process=PROCESS, lepton="e")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b0todptaunu_dptokpipi_fakeelectron_line(
    name="Hlt2SLB_B0ToDpTauNu_DpToKPiPi_TauToENuNu_FakeElectron",
    prescale=0.1,
    persistreco=True,
):
    """
    SL Hlt2 line for the decay B0 -> D+(-> K pi pi) tau(-> e nu nu) nu, with a fake electron.
    """
    line_alg = make_b0todptaunu_dptokpipi_tautolnunu_fakelepton(
        process=PROCESS, lepton="e"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_butod0taunu_d0tokpi_tautomununu_line(
    name="Hlt2SLB_BuToD0TauNu_D0ToKPi_TauToMuNuNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi) tau(-> mu nu nu) nu.
    """
    line_alg = make_butod0taunu_d0tokpi_tautolnunu(process=PROCESS, lepton="mu")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_butod0taunu_d0tokpi_fakemuon_line(
    name="Hlt2SLB_BuToD0TauNu_D0ToKPi_TauToMuNuNu_FakeMuon",
    prescale=0.1,
    persistreco=True,
):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi) tau(-> mu nu nu) nu, with a fake muon.
    """
    line_alg = make_butod0taunu_d0tokpi_tautolnunu_fakelepton(
        process=PROCESS, lepton="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_butod0taunu_d0tok3pi_tautomununu_line(
    name="Hlt2SLB_BuToD0TauNu_D0ToKPiPiPi_TauToMuNuNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi pi pi) tau(-> mu nu nu) nu.
    """
    line_alg = make_butod0taunu_d0tok3pi_tautolnunu(process=PROCESS, lepton="mu")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_butod0taunu_d0tok3pi_fakemuon_line(
    name="Hlt2SLB_BuToD0TauNu_D0ToKPiPiPi_TauToMuNuNu_FakeMuon",
    prescale=0.1,
    persistreco=True,
):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi pi pi) tau(-> mu nu nu) nu, with a fake muon.
    """
    line_alg = make_butod0taunu_d0tok3pi_tautolnunu_fakelepton(
        process=PROCESS, lepton="mu"
    )
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_butod0taunu_d0tok3pi_tautoenunu_line(
    name="Hlt2SLB_BuToD0TauNu_D0ToKPiPiPi_TauToENuNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi pi pi) tau(-> e nu nu) nu.
    """
    line_alg = make_butod0taunu_d0tok3pi_tautolnunu(process=PROCESS, lepton="e")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_butod0taunu_d0tok3pi_fakeelectron_line(
    name="Hlt2SLB_BuToD0TauNu_D0ToKPiPiPi_TauToENuNu_FakeElectron",
    prescale=0.1,
    persistreco=True,
):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi pi pi) tau(-> e nu nu) nu, with a fake electron.
    """
    line_alg = make_butod0taunu_d0tok3pi_tautolnunu_fakelepton(
        process=PROCESS, lepton="e"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bstodstaunu_dstokkpi_tautomununu_line(
    name="Hlt2SLB_BsToDsTauNu_DsToKKPi_TauToMuNuNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Bs0 -> Ds-(-> K K pi) tau+(-> mu nu nu) nu, and combinatorial (same sign).
    """
    line_alg = make_bstodstaunu_dstokkpi_tautolnunu(process=PROCESS, lepton="mu")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bstodstaunu_dstokkpi_fakemuon_line(
    name="Hlt2SLB_BsToDsTauNu_DsToKKPi_TauToMuNuNu_FakeMuon",
    prescale=0.1,
    persistreco=True,
):
    """
    SL Hlt2 line for the decay Bs0 -> Ds-(-> K K pi) tau+(-> mu nu nu) nu and combinatorial (same sign), with a fake muon.
    """
    line_alg = make_bstodstaunu_dstokkpi_tautolnunu_fakelepton(
        process=PROCESS, lepton="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bstodstaunu_dstokkpi_tautoenunu_line(
    name="Hlt2SLB_BsToDsTauNu_DsToKKPi_TauToENuNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Bs0 -> Ds-(-> K K pi) tau+(-> e nu nu) nu.
    """
    line_alg = make_bstodstaunu_dstokkpi_tautolnunu(process=PROCESS, lepton="e")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bstodstaunu_dstokkpi_fakeelectron_line(
    name="Hlt2SLB_BsToDsTauNu_DsToKKPi_TauToENuNu_FakeElectron",
    prescale=0.1,
    persistreco=True,
):
    """
    SL Hlt2 line for the decay Bs0 -> Ds-(-> K K pi) tau+(-> e nu nu) nu, with a fake electron.
    """
    line_alg = make_bstodstaunu_dstokkpi_tautolnunu_fakelepton(
        process=PROCESS, lepton="e"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolcmunu_lctopkpi_line(
    name="Hlt2SLB_LbToLcMuNu_LcToPKPi", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p K pi) mu nu.
    """
    line_alg = make_lbtolclnu_lctopkpi(process=PROCESS, lepton="mu")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolcmunu_lctopkpi_fakemuon_line(
    name="Hlt2SLB_LbToLcMuNu_LcToPKPi_FakeMuon", prescale=0.1, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p K pi) mu nu, with a fake muon.
    """
    line_alg = make_lbtolclnu_lctopkpi_fakelepton(process=PROCESS, lepton="mu")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolcenu_lctopkpi_line(
    name="Hlt2SLB_LbToLcENu_LcToPKPi", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p K pi) e nu.
    """
    line_alg = make_lbtolclnu_lctopkpi(process=PROCESS, lepton="e")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopkpi_tautomunu_line(
    name="Hlt2SLB_LbToLcTauNu_LcToPKPi_TauToMuNuNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p K pi) tau-(-> mu nu nu) nu.
    """
    line_alg = make_lbtolctaunu_lctopkpi_tautolnu(process=PROCESS, lepton="mu")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopkpi_tautomunu_fakemuon_line(
    name="Hlt2SLB_LbToLcTauNu_LcToPKPi_TauToMuNuNu_FakeMuon",
    prescale=0.1,
    persistreco=True,
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p K pi) tau-(-> mu nu nu) nu, with a fake muon.
    """
    line_alg = make_lbtolctaunu_lctopkpi_tautolnu_fakelepton(
        process=PROCESS, lepton="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctojpsimunu_jpsitomumu_line(
    name="Hlt2SLB_BcToJpsiMuNu_JpsiToMuMu", persistreco=True, prescale=1
):
    """
    SL Hlt2 line for the decay Bc+ -> Jpsi(-> mu mu) mu nu.
    """
    line_alg = make_bctojpsilnu_jpsitomumu(process=PROCESS, lepton="mu")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctojpsimunu_jpsitomumu_fakemuon_line(
    name="Hlt2SLB_BcToJpsiMuNu_JpsiToMuMu_FakeMuon", prescale=0.1, persistreco=True
):
    """
    SL Hlt2 line for the decay Bc+ -> Jpsi(-> mu mu) mu nu, with a fake muon.
    """
    line_alg = make_bctojpsilnu_jpsitomumu_fakelepton(process=PROCESS, lepton="mu")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctojpsienu_jpsitomumu_line(
    name="Hlt2SLB_BcToJpsiENu_JpsiToMuMu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Bc+ -> Jpsi(-> mu mu) e nu.
    """
    line_alg = make_bctojpsilnu_jpsitomumu(process=PROCESS, lepton="e")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctojpsienu_jpsitomumu_fakeelectron_line(
    name="Hlt2SLB_BcToJpsiENu_JpsiToMuMu_FakeElectron", prescale=0.1, persistreco=True
):
    """
    SL Hlt2 line for the decay Bc+ -> Jpsi(-> mu mu) e nu, with a fake electron.
    """
    line_alg = make_bctojpsilnu_jpsitomumu_fakelepton(process=PROCESS, lepton="e")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_butod0munu_d0tokpi_line(
    name="Hlt2SLB_BuToD0MuNu_D0ToKPi", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay B(c)+ -> D0(-> K pi) mu nu.
    """
    line_alg = make_butod0lnu_d0tokpi(process=PROCESS, lepton="mu")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_butod0munu_d0tokpi_fakemuon_line(
    name="Hlt2SLB_BuToD0MuNu_D0ToKPi_FakeMuon", prescale=0.1, persistreco=True
):
    """
    SL Hlt2 line for the decay B(c)+ -> D0(-> K pi) mu nu, with a fake muon.
    """
    line_alg = make_butod0lnu_d0tokpi_fakelepton(process=PROCESS, lepton="mu")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_butod0enu_d0tokpi_line(
    name="Hlt2SLB_BuToD0ENu_D0ToKPi", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay B(c)+ -> D0(-> K pi) e nu.
    """
    line_alg = make_butod0lnu_d0tokpi(process=PROCESS, lepton="e")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def butod0enu_d0tokpi_fakeelectron_line(
    name="Hlt2SLB_BuToD0ENu_D0ToKPi_FakeElectron", prescale=0.1, persistreco=True
):
    """
    SL Hlt2 line for the decay B(c)+ -> D0(-> K pi) e nu, with a fake electron.
    """
    line_alg = make_butod0lnu_d0tokpi_fakelepton(process=PROCESS, lepton="e")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_butod0munu_d0tok3pi_line(
    name="Hlt2SLB_BuToD0MuNu_D0ToKPiPiPi", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi pi pi) mu nu.
    """
    line_alg = make_butod0lnu_d0tok3pi(process=PROCESS, lepton="mu")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_butod0munu_d0tok3pi_fakemuon_line(
    name="Hlt2SLB_BuToD0MuNu_D0ToKPiPiPi_FakeMuon", prescale=0.1, persistreco=True
):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi pi pi) mu nu, with a fake muon.
    """
    line_alg = make_butod0lnu_d0tok3pi_fakelepton(process=PROCESS, lepton="mu")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_butod0enu_d0tok3pi_line(
    name="Hlt2SLB_BuToD0ENu_D0ToKPiPiPi", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi pi pi) e nu.
    """
    line_alg = make_butod0lnu_d0tok3pi(process=PROCESS, lepton="e")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_butod0enu_d0tok3pi_fakeelectron_line(
    name="Hlt2SLB_BuToD0ENu_D0ToKPiPiPi_FakeElectron", prescale=0.1, persistreco=True
):
    """
    SL Hlt2 line for the decay B+ -> D0(-> K pi pi pi) e nu, with a fake electron.
    """
    line_alg = make_butod0lnu_d0tok3pi_fakelepton(process=PROCESS, lepton="e")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b0todpmunu_dptokpipi_line(
    name="Hlt2SLB_B0ToDpMuNu_DpToKPiPi", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay B0 -> D+(-> K pi pi) mu nu.
    """
    line_alg = make_b0todplnu_dptokpipi(process=PROCESS, lepton="mu")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b0todpmunu_dptokpipi_fakemuon_line(
    name="Hlt2SLB_B0ToDpMuNu_DpToKPiPi_FakeMuon", prescale=0.1, persistreco=True
):
    """
    SL Hlt2 line for the decay B0 -> D+(-> K pi pi) mu nu, with a fake muon.
    """
    line_alg = make_b0todplnu_dptokpipi_fakelepton(process=PROCESS, lepton="mu")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b0todpenu_dptokpipi_line(
    name="Hlt2SLB_B0ToDpENu_DpToKPiPi", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay B0 -> D+(-> K pi pi) e nu.
    """
    line_alg = make_b0todplnu_dptokpipi(process=PROCESS, lepton="e")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b0todpenu_dptokpipi_fakeelectron_line(
    name="Hlt2SLB_B0ToDpENu_DpToKPiPi_FakeElectron", prescale=0.1, persistreco=True
):
    """
    SL Hlt2 line for the decay B0 -> D+(-> K pi pi) e nu, with a fake electron.
    """
    line_alg = make_b0todplnu_dptokpipi_fakelepton(process=PROCESS, lepton="e")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b0todpmunu_dptokkpi_line(
    name="Hlt2SLB_B0ToDpMuNu_DpToKKPi", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay B0 -> D+(-> K K pi) mu nu.
    """
    line_alg = make_b0todplnu_dptokkpi(process=PROCESS, lepton="mu")
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b0todpmunu_dptokkpi_fakemuon_line(
    name="Hlt2SLB_B0ToDpMuNu_DpToKKPi_FakeMuon", prescale=0.1, persistreco=True
):
    """
    SL Hlt2 line for the decay B0 -> D+(-> K K pi) mu nu, with a fake muon.
    """
    line_alg = make_b0todplnu_dptokkpi_fakelepton(process=PROCESS, lepton="mu")
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b0todpenu_dptokkpi_line(
    name="Hlt2SLB_B0ToDpENu_DpToKKPi", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay B0 -> D+(-> K K pi) e nu.
    """
    line_alg = make_b0todplnu_dptokkpi(process=PROCESS, lepton="e")
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b0todpenu_dptokkpi_fakeelectron_line(
    name="Hlt2SLB_B0ToDpENu_DpToKKPi_FakeElectron", prescale=0.1, persistreco=True
):
    """
    SL Hlt2 line for the decay B0 -> D+(-> K K pi) e nu, with a fake electron.
    """
    line_alg = make_b0todplnu_dptokpipi_fakelepton(process=PROCESS, lepton="e")
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bstodsmunu_dstokkpi_line(
    name="Hlt2SLB_BsToDsMuNu_DsToKKPi", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay B_s0 -> Ds(-> K K pi) mu nu.
    """
    line_alg = make_bstodslnu_dstokkpi(process=PROCESS, lepton="mu")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bstodstlnu_dsttodsgamma_dstokkpi_gammatoee_line(
    name="Hlt2SLB_BsToDsstMuNu_DsstToDsGamma_DsToKKPi_GammaToEE",
    prescale=1,
    persistreco=True,
):
    """
    SL Hlt2 line for the decay  B_s0 -> D*s(-> Ds(-> K K pi) gamma (-> ee) ) mu nu.
    """
    line_alg = make_bstodstlnu_dsttodsgamma_dstokkpi_gammatoee(
        process=PROCESS, lepton="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bstodsmunu_dstokkpi_fakemuon_line(
    name="Hlt2SLB_BsToDsMuNu_DsToKKPi_FakeMuon", prescale=0.1, persistreco=True
):
    """
    SL Hlt2 line for the decay B_s0 -> Ds(-> K K pi) mu nu, with a fake muon.
    """
    line_alg = make_bstodslnu_dstokkpi_fakelepton(process=PROCESS, lepton="mu")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bstodsenu_dstokkpi_line(
    name="Hlt2SLB_BsToDsENu_DsToKKPi", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay B_s0 -> Ds(-> K K pi) e nu.
    """
    line_alg = make_bstodslnu_dstokkpi(process=PROCESS, lepton="e")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bstodsenu_dstokkpi_fakeelectron_line(
    name="Hlt2SLB_BsToDsENu_DsToKKPi_FakeElectron", prescale=0.1, persistreco=True
):
    """
    SL Hlt2 line for the decay B_s0 -> Ds(-> K K pi) e nu, with a fake electron.
    """
    line_alg = make_bstodslnu_dstokkpi_fakelepton(process=PROCESS, lepton="e")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_xib0toxicplusmunu_xicplustopkpi_line(
    name="Hlt2SLB_Xib0ToXicplusMuNu_XicplusToPKPi", prescale=1.0, persistreco=True
):
    """
    SL Hlt2 line for the decays:
        Xi_b0 -> Xi_c+ (-> p K- pi+) mu- nu (Right sign)
        Xi_b0 -> Xi_c+ (-> p K- pi+) mu+ nu (Wrong sign)
    """
    line_alg = make_xib0toxicplusmunu_xicplustopkpi(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_xib0toxicplustaunu_xicplustopkpi_tautomununu_line(
    name="Hlt2SLB_Xib0ToXicplusTauNu_XicplusToPKPi_TauToMuNuNu",
    prescale=1.0,
    persistreco=True,
):
    """
    SL Hlt2 line for the decays:
        Xi_b0 -> Xi_c+ (-> p K- pi+) tau- (-> mu- nu nu) nu (Right sign)
        Xi_b0 -> Xi_c+ (-> p K- pi+) tau+ (-> mu+ nu nu) nu (Wrong sign)
    """
    line_alg = make_xib0toxicplustaunu_xicplustopkpi_tautomununu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_xib0toxicplusmunu_xicplustopkpi_fakemuon_line(
    name="Hlt2SLB_Xib0ToXicplusMuNu_XicplusToPKPi_FakeMuon",
    prescale=0.05,
    persistreco=True,
):
    """
    SL Hlt2 line for the decays with fake muon:
        Xi_b0 -> Xi_c+ (-> p K- pi+) mu- nu (Right sign)
        Xi_b0 -> Xi_c+ (-> p K- pi+) mu+ nu (Wrong sign)
    """
    line_alg = make_xib0toxicplusmunu_xicplustopkpi_fakelepton(process=PROCESS)
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_xib0toxicplustaunu_xicplustopkpi_tautomununu_fakemuon_line(
    name="Hlt2SLB_Xib0ToXicplusTauNu_XicplusToPKPi_FakeMuon",
    prescale=0.05,
    persistreco=True,
):
    """
    SL Hlt2 line for the decays with a fake muon:
        Xi_b0 -> Xi_c+ (-> p K- pi+) tau- (-> mu- nu nu) nu (Right sign)
        Xi_b0 -> Xi_c+ (-> p K- pi+) tau+ (-> mu+ nu nu) nu (Wrong sign)
    """
    line_alg = make_xib0toxicplustaunu_xicplustopkpi_tautomununu_fakelepton(
        process=PROCESS
    )
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_xibminustoxic0munu_xic0topkkpi_line(
    name="Hlt2SLB_XibminusToXic0MuNu_Xic0ToPKKPi", prescale=1.0, persistreco=True
):
    """
    SL Hlt2 line for the decays:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) mu- nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) mu+ nu (Wrong sign)
    """
    line_alg = make_xibminustoxic0munu_xic0topkkpi(process=PROCESS)
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_xibminustoxic0taunu_xic0topkkpi_tautomununu_line(
    name="Hlt2SLB_XibminusToXic0TauNu_Xic0ToPKKPi_TauToMuNuNu",
    prescale=1.0,
    persistreco=True,
):
    """
    SL Hlt2 line for the decays:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau- (-> mu- nu nu) nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau+ (-> mu+ nu nu) nu (Wrong sign)
    """
    line_alg = make_xibminustoxic0taunu_xic0topkkpi_tautomununu(process=PROCESS)
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_xibminustoxic0munu_xic0topkkpi_fakemuon_line(
    name="Hlt2SLB_XibminusToXic0MuNu_Xic0ToPKKPi_FakeMuon",
    prescale=0.05,
    persistreco=True,
):
    """
    SL Hlt2 line for the decays with fake muon:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) mu- nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) mu+ nu (Wrong sign)
    """
    line_alg = make_xibminustoxic0munu_xic0topkkpi_fakelepton(process=PROCESS)
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_xibminustoxic0taunu_xic0topkkpi_tautomununu_fakemuon_line(
    name="Hlt2SLB_XibminusToXic0TauNu_Xic0ToPKKPi_FakeMuon",
    prescale=0.05,
    persistreco=True,
):
    """
    SL Hlt2 line for the decays with fake muon:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau- (-> mu- nu nu) nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau+ (-> mu+ nu nu) nu (Wrong sign)
    """
    line_alg = make_xibminustoxic0taunu_xic0topkkpi_tautomununu_fakelepton(
        process=PROCESS
    )
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b0todptaunu_dptokpipi_tautopipipinu_line(
    name="Hlt2SLB_B0ToDpTauNu_DpToKPiPi_TauToPiPiPiNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay B0 -> D- tau+, hadronic tau decay.
    """
    line_alg = make_b0todptaunu_dptokpipi_tautopipipinu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctojpsitaunu_jpsitomumu_tautopipipinu_line(
    name="Hlt2SLB_BcToJpsiTauNu_JpsiToMuMu_TauToPiPiPiNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay B_c+ -> J/psi(1S) tau+, hadronic tau decay.
    """
    line_alg = make_bctojpsitaunu_jpsitomumu_tautopipipinu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bptod0taunu_d0tokpi_tautopipipinu_line(
    name="Hlt2SLB_BuToD0TauNu_D0ToKPi_TauToPiPiPiNu", prescale=0.59, persistreco=True
):
    """
    SL Hlt2 line for the decay B+ -> D~0 tau+, hadronic tau decay.
    """
    line_alg = make_bptod0taunu_d0tokpi_tautopipipinu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bstodstaunu_dstokkpi_tautopipipinu_line(
    name="Hlt2SLB_BsToDsTauNu_DsToKKPi_TauToPiPiPiNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay B_s0 -> D_s- tau+, hadronic tau decay.
    """
    line_alg = make_bstodstaunu_dstokkpi_tautopipipinu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopkpi_tautopipipinu_line(
    name="Hlt2SLB_LbToLcTauNu_LcToPKPi_TauToPiPiPiNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Lambda_b0 -> Lambda_c+ tau-, hadronic tau decay.
    """
    line_alg = make_lbtolctaunu_lctopkpi_tautopipipinu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtoptaunu_tautopipipinu_line(
    name="Hlt2SLB_LbTopTauNu_TauToPiPiPiNu", prescale=0.43, persistreco=True
):
    """
    SL Hlt2 line for the decay Lambda_b0 -> p+ tau-, hadronic tau decay.
    """
    line_alg = make_lbtoptaunu_tautopipipinu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b0topimunu_line(name="Hlt2SLB_B0ToPiMuNu", prescale=1, persistreco=True):
    """
    SL Hlt2 line for the decay B0 -> pi mu nu.
    """
    line_alg = make_b0topimunu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b0topimunu_fakemu_line(
    name="Hlt2SLB_B0ToPiMuNu_NoPIDMu", prescale=0.02, persistreco=True
):
    """
    SL Hlt2 line for the decay B0 -> pi mu nu, with a fake muon.
    """
    line_alg = make_b0topimunu_fakemu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b0topimunu_fakek_line(
    name="Hlt2SLB_B0ToPiMuNu_NoPIDPi", prescale=0.02, persistreco=True
):
    """
    SL Hlt2 line for the decay B0 -> pi mu, with a fake pions.
    """
    line_alg = make_b0topimunu_fakek(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b0topitaunu_hadronic_line(
    name="Hlt2SLB_B0ToPiTauNu_TauToPiPiPiNu", prescale=0.1, persistreco=True
):
    """
    SL Hlt2 line for the decay B0 -> pi tau (->pi+pi+pi-)
    """
    line_alg = make_b0topitaunu_hadronic(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtopmunu_line(name="Hlt2SLB_LbToPMuNu", prescale=1, persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> p mu nu.
    """
    line_alg = make_lbtopmunu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtopmunu_fakep_line(
    name="Hlt2SLB_LbToPMuNu_FakeP", prescale=0.020, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> p mu nu, with a fake proton.
    """
    line_alg = make_lbtopmunu_fakep(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtopmunu_fakemu_line(
    name="Hlt2SLB_LbToPMuNu_FakeMuon", prescale=0.05, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> p mu nu, with a fake muon.
    """
    line_alg = make_lbtopmunu_fakemu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtopmunu_ss_line(name="Hlt2SLB_LbToPMuNu_WS", prescale=1, persistreco=True):
    """
    SL Hlt2 line for the decay Lb0 -> p mu nu (same-sign).
    """
    line_alg = make_lbtopmunu_ss(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtopmunu_ss_fakep_line(
    name="Hlt2SLB_LbToPMuNu_WS_FakeP", prescale=0.020, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> p mu nu (same-sign), with a fake proton.
    """
    line_alg = make_lbtopmunu_ss_fakep(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtopmunu_ss_fakemu_line(
    name="Hlt2SLB_LbToPMuNu_WS_FakeMuon", prescale=0.05, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> p mu nu (same-sign), with a fake muon.
    """
    line_alg = make_lbtopmunu_ss_fakemu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bstokmunu_line(name="Hlt2SLB_BsToKMuNu", prescale=1, persistreco=True):
    """
    SL Hlt2 line for the decay Bs0 -> K mu nu.
    """
    line_alg = make_bstokmunu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bstokmunu_fakemu_line(
    name="Hlt2SLB_BsToKMuNu_NoPIDMu", prescale=0.02, persistreco=True
):
    """
    SL Hlt2 line for the decay Bs0 -> K mu nu, with a fake muon.
    """
    line_alg = make_bstokmunu_fakemu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bstokmunu_fakek_line(
    name="Hlt2SLB_BsToKMuNu_NoPIDK", prescale=0.02, persistreco=True
):
    """
    SL Hlt2 line for the decay Bs0 -> K mu, with a fake kaon.
    """
    line_alg = make_bstokmunu_fakek(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bstokenu_line(name="Hlt2SLB_BsToKENu", prescale=1, persistreco=True):
    """
    SL Hlt2 line for the decay Bs0 -> K e nu.
    """
    line_alg = make_bstokenu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bstokenu_fakeelectron_line(
    name="Hlt2SLB_BsToKENu_FakeElectron", prescale=0.02, persistreco=True
):
    """
    SL Hlt2 line for the decay Bs0 -> K e nu, with a fake electron.
    """
    line_alg = make_bstokenu_fakeelectron(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bstokenu_fakek_line(
    name="Hlt2SLB_BsToKENu_NoPIDK", prescale=0.02, persistreco=True
):
    """
    SL Hlt2 line for the decay Bs0 -> K e, with a fake kaon.
    """
    line_alg = make_bstokenu_fakek(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bstoktaunu_hadronic_line(
    name="Hlt2SLB_BsToKTauNu_TauToPiPiPiNu", prescale=0.1, persistreco=True
):
    """
    SL Hlt2 line for the decay Bs0 -> K tau (->pi+pi+pi-)
    """
    line_alg = make_bstoktaunu_hadronic(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bstoktaunu_muonic_line(
    name="Hlt2SLB_BsToKTauNu_TauToMuNuNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Bs0 -> K tau(mu nu nu ) nu.
    """
    line_alg = make_bstoktaunu_muonic(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bstoktaunu_muonic_fakemu_line(
    name="Hlt2SLB_BsToKTauNu_TauToMuNuNu_NoPIDMu", prescale=0.02, persistreco=True
):
    """
    SL Hlt2 line for the decay Bs0 -> K tau(mu nu nu) nu, with a fake muon.
    """
    line_alg = make_bstoktaunu_muonic_fakemu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bstoktaunu_muonic_fakek_line(
    name="Hlt2SLB_BsToKTauNu_TauToMuNuNu_NoPIDK", prescale=0.02, persistreco=True
):
    """
    SL Hlt2 line for the decay Bs0 -> K tau(mu nu nu) , with a fake kaon.
    """
    line_alg = make_bstoktaunu_muonic_fakek(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2ppbarmunu_line(name="Hlt2SLB_BuToPPbarMuNu", prescale=1, persistreco=True):
    """
    SL Hlt2 line for the B->ppmunu and muonic B->pptaunu decays
    """
    line_alg = make_b2ppbarmunu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2ppbarmunu_ss_line(
    name="Hlt2SLB_BuToPPbarMuNu_SS", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the B->ppmunu and muonic B->pptaunu decays with same-sign protons
    """
    line_alg = make_b2ppbarmunu_ss(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2ppbarmunu_fakep_line(
    name="Hlt2SLB_BuToPPbarMuNu_FakeP", prescale=0.1, persistreco=True
):
    """
    SL Hlt2 line for the B->ppmunu and muonic B->pptaunu decays with a fake proton of opposite sign to muon
    """
    line_alg = make_b2ppbarmunu_fakep(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2ppbarmunu_fakemu_line(
    name="Hlt2SLB_BuToPPbarMuNu_FakeMu", prescale=0.1, persistreco=True
):
    """
    SL Hlt2 line for the B->ppmunu and muonic B->pptaunu decays with a fake muon
    """
    line_alg = make_b2ppbarmunu_fakemu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2pipimunu_line(name="Hlt2SLB_BuToPiPiMuNu", prescale=1, persistreco=True):
    """
    SL Hlt2 line for the B->pipimunu and muonic B->pipitaunu decays
    """
    line_alg = make_b2pipimunu(process=PROCESS)
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2pipimunu_ss_line(
    name="Hlt2SLB_BuToPiPiMuNu_SS", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the B->pipimunu and muonic B->pipitaunu decays with same-sign pions
    """
    line_alg = make_b2pipimunu_ss(process=PROCESS)
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2pipimunu_fakemu_line(
    name="Hlt2SLB_BuToPiPiMuNu_FakeMu", prescale=0.1, persistreco=True
):
    """
    SL Hlt2 line for the B->pipimunu and muonic B->pipitaunu decays with a fake muon
    """
    line_alg = make_b2pipimunu_fakemu(process=PROCESS)
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2kkmunu_line(name="Hlt2SLB_BuToKKMuNu", prescale=1, persistreco=True):
    """
    SL Hlt2 line for the B->KKmunu and muonic B->KKtaunu decays
    """
    line_alg = make_b2kkmunu(process=PROCESS)
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2kkmunu_ss_line(name="Hlt2SLB_BuToKKMuNu_SS", prescale=1, persistreco=True):
    """
    SL Hlt2 line for the B->KKmunu and muonic B->KKtaunu decays with same-sign kaons
    """
    line_alg = make_b2kkmunu_ss(process=PROCESS)
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2kkmunu_fakemu_line(
    name="Hlt2SLB_BuToKKMuNu_FakeMu", prescale=0.1, persistreco=True
):
    """
    SL Hlt2 line for the B->KKmunu and muonic B->KKtaunu decays with a fake muon
    """
    line_alg = make_b2kkmunu_fakemu(process=PROCESS)
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2mumumunu_line(name="Hlt2SLB_BuToMuMuMuNu", prescale=1, persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> mu+mu-mu+nu decays
    """
    line_alg = make_b2mumumunu(name=name, process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2emumunu_line(name="Hlt2SLB_BuToEMuMuNu", prescale=1, persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> e+mu-mu+nu decays
    """
    line_alg = make_b2emumunu(name=name, process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2mueenu_line(name="Hlt2SLB_BuToMuEENu", prescale=1, persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> mu+e-e+nu decays
    """
    line_alg = make_b2mueenu(name=name, process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2mugammanu_cnvll_line(
    name="Hlt2SLB_BuToMuGammaNu_CNVLL", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the B(c)+ -> mu+gamma(->e+e-)nu decays with long electrons
    """
    line_alg = make_b2mugammanu_cnvll(name=name, process=PROCESS)
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2mugammanu_cnvdd_line(
    name="Hlt2SLB_BuToMuGammaNu_CNVDD", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the B(c)+ -> mu+gamma(->e+e-)nu decays with downstream electrons
    """
    line_alg = make_b2mugammanu_cnvdd(name=name, process=PROCESS)
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2egammanu_cnvll_line(
    name="Hlt2SLB_BuToEGammaNu_CNVLL", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the B(c)+ -> mu+gamma(->e+e-)nu decays with long conversion electrons
    """
    line_alg = make_b2egammanu_cnvll(name=name, process=PROCESS)
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2egammanu_cnvdd_line(
    name="Hlt2SLB_BuToEGammaNu_CNVDD", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the B(c)+ -> mu+gamma(->e+e-)nu decays with downstream conversion electrons
    """
    line_alg = make_b2egammanu_cnvdd(name=name, process=PROCESS)
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2eeenu_line(name="Hlt2SLB_BuToEEENu", prescale=1, persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> e+e-e+nu decays
    """
    line_alg = make_b2eeenu(name=name, process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2mumumunu_ss_line(
    name="Hlt2SLB_BuToMuMuMuNu_SS", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the B(c)+ -> mu+mu+mu+nu decays: same-sign combinations
    """
    line_alg = make_b2mumumunu_ss(name=name, process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2emumunu_ss_line(name="Hlt2SLB_BuToEMuMuNu_SS", prescale=1, persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> e+mu+mu+nu decays: same-sign combinations
    """
    line_alg = make_b2emumunu_ss(name=name, process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2mueenu_ss_line(name="Hlt2SLB_BuToMuEENu_SS", prescale=1, persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> mu+e+e+nu decays: same-sign combinations
    """
    line_alg = make_b2mueenu_ss(name=name, process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_b2eeenu_ss_line(name="Hlt2SLB_BuToEEENu_SS", prescale=1, persistreco=True):
    """
    SL Hlt2 line for the B(c)+ -> e+e+e+nu decays: same-sign combinations
    """
    line_alg = make_b2eeenu_ss(name=name, process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_omegabtoomegacmunu_omegactopkkpi_line(
    name="Hlt2SLB_OmegabToOmegacMuNu_OmegacToPKKPi", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay [Omegab- -> Omegac0(-> p K- K- pi+) mu nu]cc.
    """
    line_alg = make_omegabtoomegacmunu_omegactopkkpi(process=PROCESS)
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_omegabtoomegacmunu_omegactopkkpi_fakemuon_line(
    name="Hlt2SLB_OmegabToOmegacMuNu_OmegacToPKKPi_FakeMuon",
    prescale=0.05,
    persistreco=True,
):
    """
    SL Hlt2 line for the decay [Omegab- -> Omegac0(-> p K- K- pi+) mu nu]cc with a fake muon.
    """
    line_alg = make_omegabtoomegacmunu_omegactopkkpi_fakelepton(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_omegabtoomegactaunu_omegactopkkpi_tautomunu_line(
    name="Hlt2SLB_OmegabToOmegacTauNu_OmegacToPKKPi_TauToMuNuNu",
    prescale=1,
    persistreco=True,
):
    """
    SL Hlt2 line for the decay Omegab- -> Omegac0(-> p K- K- pi+) tau-(-> mu nu nu) nu.
    """
    line_alg = make_omegabtoomegactaunu_omegactopkkpi_tautomununu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_omegabtoomegactaunu_omegactopkkpi_tautomunu_fakemuon_line(
    name="Hlt2SLB_OmegabToOmegacTauNu_OmegacToPKKPi_FakeMuon",
    prescale=0.05,
    persistreco=True,
):
    """
    SL Hlt2 line for the decay Omegab- -> Omegac0(-> p K- K- pi+) tau-(-> mu nu nu) nu, with a fake muon.
    """
    line_alg = make_omegabtoomegactaunu_omegactopkkpi_tautomununu_fakelepton(
        process=PROCESS
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolcmunu_lctopksLL_line(
    name="Hlt2SLB_LbToLcMuNu_LcToPKSLL", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) mu nu.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolclnu_lctoV0h(
        process=PROCESS, V0_name="KS0", V0_type="LL", lepton_name="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolcmunu_lctopksDD_line(
    name="Hlt2SLB_LbToLcMuNu_LcToPKSDD", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) mu nu.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'downstream' tracks ('DD' category).
    """
    line_alg = make_lbtolclnu_lctoV0h(
        process=PROCESS, V0_name="KS0", V0_type="DD", lepton_name="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolcenu_lctopksLL_line(
    name="Hlt2SLB_LbToLcENu_LcToPKSLL", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) e nu.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolclnu_lctoV0h(
        process=PROCESS, V0_name="KS0", V0_type="LL", lepton_name="e"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolcenu_lctopksDD_line(
    name="Hlt2SLB_LbToLcENu_LcToPKSDD", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) e nu.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'downstream' tracks ('DD' category).
    """
    line_alg = make_lbtolclnu_lctoV0h(
        process=PROCESS, V0_name="KS0", V0_type="DD", lepton_name="e"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolcmunu_lctopksLL_fakemuon_line(
    name="Hlt2SLB_LbToLcMuNu_LcToPKSLL_FakeMuon", prescale=0.05, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) mu nu, with a fake muon.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolclnu_lctoV0h_fakelepton(
        process=PROCESS, V0_name="KS0", V0_type="LL", lepton_name="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolcmunu_lctopksDD_fakemuon_line(
    name="Hlt2SLB_LbToLcMuNu_LcToPKSDD_FakeMuon", prescale=0.05, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) mu nu, with a fake muon.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'downstream' tracks ('DD' category).
    """
    line_alg = make_lbtolclnu_lctoV0h_fakelepton(
        process=PROCESS, V0_name="KS0", V0_type="DD", lepton_name="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopksLL_tautomunu_line(
    name="Hlt2SLB_LbToLcTauNu_LcToPKSLL_TauToMuNuNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) tau (-> mu nu nu) nu.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu(
        process=PROCESS, V0_name="KS0", V0_type="LL", lepton_name="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopksDD_tautomunu_line(
    name="Hlt2SLB_LbToLcTauNu_LcToPKSDD_TauToMuNuNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) tau (-> mu nu nu) nu.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('DD' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu(
        process=PROCESS, V0_name="KS0", V0_type="DD", lepton_name="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopksLL_tautomunu_fakemuon_line(
    name="Hlt2SLB_LbToLcTauNu_LcToPKSLL_FakeMuon", prescale=0.05, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) tau (-> mu nu nu) nu, with a fake muon.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu_fakelepton(
        process=PROCESS, V0_name="KS0", V0_type="LL", lepton_name="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopksDD_tautomunu_fakemuon_line(
    name="Hlt2SLB_LbToLcTauNu_LcToPKSDD_FakeMuon", prescale=0.05, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) tau (-> mu nu nu) nu, with a fake muon.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('DD' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu_fakelepton(
        process=PROCESS, V0_name="KS0", V0_type="DD", lepton_name="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolcmunu_lctolambdapiLL_line(
    name="Hlt2SLB_LbToLcMuNu_LcToLambdaPiLL", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) mu nu.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolclnu_lctoV0h(
        process=PROCESS, V0_name="Lambda0", V0_type="LL", lepton_name="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolcmunu_lctolambdapiDD_line(
    name="Hlt2SLB_LbToLcMuNu_LcToLambdaPiDD", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) mu nu.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'downstream' tracks ('DD' category).
    """
    line_alg = make_lbtolclnu_lctoV0h(
        process=PROCESS, V0_name="Lambda0", V0_type="DD", lepton_name="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolcenu_lctolambdapiLL_line(
    name="Hlt2SLB_LbToLcENu_LcToLambdaPiLL", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) e nu.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolclnu_lctoV0h(
        process=PROCESS, V0_name="Lambda0", V0_type="LL", lepton_name="e"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolcenu_lctolambdapiDD_line(
    name="Hlt2SLB_LbToLcENu_LcToLambdaPiDD", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) e nu.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'downstream' tracks ('DD' category).
    """
    line_alg = make_lbtolclnu_lctoV0h(
        process=PROCESS, V0_name="Lambda0", V0_type="DD", lepton_name="e"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolcmunu_lctolambdapiLL_fakemuon_line(
    name="Hlt2SLB_LbToLcMuNu_LcToLambdaPiLL_FakeMuon", prescale=0.05, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) mu nu, with a fake muon.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolclnu_lctoV0h_fakelepton(
        process=PROCESS, V0_name="Lambda0", V0_type="LL", lepton_name="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolcmunu_lctolambdapiDD_fakemuon_line(
    name="Hlt2SLB_LbToLcMuNu_LcToLambdaPiDD_FakeMuon", prescale=0.05, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) mu nu, with a fake muon.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'downstream' tracks ('DD' category).
    """
    line_alg = make_lbtolclnu_lctoV0h_fakelepton(
        process=PROCESS, V0_name="Lambda0", V0_type="DD", lepton_name="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctolambdapiLL_tautomunu_line(
    name="Hlt2SLB_LbToLcTauNu_LcToLambdaPiLL_TauToMuNuNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) tau (-> mu nu nu) nu.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu(
        process=PROCESS, V0_name="Lambda0", V0_type="LL", lepton_name="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctolambdapiDD_tautomunu_line(
    name="Hlt2SLB_LbToLcTauNu_LcToLambdaPiDD_TauToMuNuNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) tau (-> mu nu nu) nu.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('DD' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu(
        process=PROCESS, V0_name="Lambda0", V0_type="DD", lepton_name="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctolambdapiLL_tautomunu_fakemuon_line(
    name="Hlt2SLB_LbToLcTauNu_LcToLambdaPiLL_FakeMuon", prescale=0.05, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) tau (-> mu nu nu) nu, with a fake muon.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu_fakelepton(
        process=PROCESS, V0_name="Lambda0", V0_type="LL", lepton_name="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        persistreco=persistreco,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctolambdapiDD_tautomunu_fakemuon_line(
    name="Hlt2SLB_LbToLcTauNu_LcToLambdaPiDD_FakeMuon", prescale=0.05, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) tau (-> mu nu nu) nu, with a fake muon.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('DD' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautolnu_fakelepton(
        process=PROCESS, V0_name="Lambda0", V0_type="DD", lepton_name="mu"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobsmunu_bstodspi_line(
    name="Hlt2SLB_BcToBsMuNu_BsToDsPi", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs mu nu with Bs -> Ds pi
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsMuNu", BsDecay="BsToDsPi")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobsmunu_bstodsk_line(
    name="Hlt2SLB_BcToBsMuNu_BsToDsK", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs mu nu with Bs -> Ds K
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsMuNu", BsDecay="BsToDsK")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobsmunu_bstojpsiphi_line(
    name="Hlt2SLB_BcToBsMuNu_BsToJpsiPhi", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs mu nu with Bs -> J/psi phi
    """

    line_alg = make_bctobsx(
        process=PROCESS, BcDecay="BcToBsMuNu", BsDecay="BsToJpsiPhi"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobsmunu_bstokk_line(
    name="Hlt2SLB_BcToBsMuNu_BsToKK", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs mu nu with Bs -> K- K+
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsMuNu", BsDecay="BsToKK")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobsmunu_bstokpi_line(
    name="Hlt2SLB_BcToBsMuNu_BsToKPi", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs mu nu with Bs -> K+ pi- and CC
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsMuNu", BsDecay="BsToKPi")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobsmunu_bstopipi_line(
    name="Hlt2SLB_BcToBsMuNu_BsToPiPi", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs mu nu with Bs -> pi+ pi-
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsMuNu", BsDecay="BsToPiPi")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobsenu_bstodspi_line(
    name="Hlt2SLB_BcToBsENu_BsToDsPi", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs e nu with Bs -> Ds pi
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsENu", BsDecay="BsToDsPi")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobsenu_bstodsk_line(
    name="Hlt2SLB_BcToBsENu_BsToDsK", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs e nu with Bs -> Ds K
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsENu", BsDecay="BsToDsK")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobsenu_bstojpsiphi_line(
    name="Hlt2SLB_BcToBsENu_BsToJpsiPhi", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs e nu with Bs -> J/psi phi
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsENu", BsDecay="BsToJpsiPhi")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobsenu_bstokk_line(
    name="Hlt2SLB_BcToBsENu_BsToKK", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs e nu with Bs -> K- K+
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsENu", BsDecay="BsToKK")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobsenu_bstokpi_line(
    name="Hlt2SLB_BcToBsENu_BsToKPi", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs e nu with Bs -> K+ pi- and CC
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsENu", BsDecay="BsToKPi")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobsenu_bstopipi_line(
    name="Hlt2SLB_BcToBsENu_BsToPiPi", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs e nu with Bs -> pi+ pi-
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsENu", BsDecay="BsToPiPi")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobspi_bstodspi_line(
    name="Hlt2SLB_BcToBsPi_BsToDsPi", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs pi with Bs -> Ds pi
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsPi", BsDecay="BsToDsPi")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobspi_bstodsk_line(
    name="Hlt2SLB_BcToBsPi_BsToDsK", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs pi with Bs -> Ds K
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsPi", BsDecay="BsToDsK")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobspi_bstojpsiphi_line(
    name="Hlt2SLB_BcToBsPi_BsToJpsiPhi", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs pi with Bs -> J/psi phi
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsPi", BsDecay="BsToJpsiPhi")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobspi_bstokk_line(
    name="Hlt2SLB_BcToBsPi_BsToKK", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs pi with Bs -> K+ K-
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsPi", BsDecay="BsToKK")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobspi_bstokpi_line(
    name="Hlt2SLB_BcToBsPi_BsToKPi", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs pi with Bs -> K+ pi-
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsPi", BsDecay="BsToKPi")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobspi_bstopipi_line(
    name="Hlt2SLB_BcToBsPi_BsToPiPi", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs pi with Bs -> pi+ pi-
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsPi", BsDecay="BsToPiPi")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobsk_bstodspi_line(
    name="Hlt2SLB_BcToBsK_BsToDsPi", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs K with Bs -> Ds pi
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsK", BsDecay="BsToDsPi")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobsk_bstodsk_line(
    name="Hlt2SLB_BcToBsK_BsToDsK", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs K with Bs -> Ds K
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsK", BsDecay="BsToDsK")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobsk_bstojpsiphi_line(
    name="Hlt2SLB_BcToBsK_BsToJpsiPhi", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs K with Bs -> J/psi phi
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsK", BsDecay="BsToJpsiPhi")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobsk_bstokk_line(
    name="Hlt2SLB_BcToBsK_BsToKK", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs K with Bs -> K+ K-
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsK", BsDecay="BsToKK")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobsk_bstokpi_line(
    name="Hlt2SLB_BcToBsK_BsToKPi", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs K with Bs -> K+ pi- and CC
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsK", BsDecay="BsToKPi")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_bctobsk_bstopipi_line(
    name="Hlt2SLB_BcToBsK_BsToPiPi", persistreco=True, prescale=1.0
):
    """
    SL Hlt2 line for the decay Bc -> Bs K with Bs -> pi+ pi-
    """

    line_alg = make_bctobsx(process=PROCESS, BcDecay="BcToBsK", BsDecay="BsToPiPi")

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=False
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_btotaunu_tautopipipinu_btracking_line(
    name="Hlt2SLB_BuToTauNu_TauToPiPiPiNu_BTracking",
    prescale=1,
    persistreco=True,
    persist_raw_banks=True,
    include_mcchecker=False,
):
    """
    SL Hlt2 line for B+(c)->Tau+(->Pi+Pi+Pi-Nu)Nu + c.c. with B+(c) tracking
    option to include mc checker for line testing, not meant for online running
    """
    parts_with_btracking = make_b2taunu_tau2pipipi_with_btracking(process=PROCESS)
    line_mon = generate_default_monitoring_vars(
        name, parts_with_btracking["LineAlg"], charged=True, charmed=False
    )

    line = Hlt2Line(
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [parts_with_btracking["LineAlg"]] + line_mon,
        extra_outputs=get_btracking_extra_outputs(parts_with_btracking),
        persistreco=persistreco,
        raw_banks=get_btracking_raw_banks(persist_raw_banks),
        monitoring_variables=_MONITORING_VARIABLES,
    )

    return (
        line
        if not include_mcchecker
        else {"line": line, "checker": get_mc_checker(parts_with_btracking)}
    )


@register_line_builder(hlt2_lines)
def hlt2_btodpipi_dtokpipi_btracking_line(
    name="Hlt2SLB_BuToDPiPi_DToKPiPi_BTracking",
    prescale=1,
    persistreco=True,
    persist_raw_banks=True,
):
    """
    SL Hlt2 line for B+->D-(->K+Pi-Pi-)pi+pi+ + c.c. with ability for B+ tracking,
    control channel for 'Hlt2SLB_BToTauNu_TauToPiPiPiNu_BTracking_Line'
    """
    parts_with_btracking = make_b2dpipi_d2kpipi_with_btracking(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, parts_with_btracking["LineAlg"], charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [parts_with_btracking["LineAlg"]] + line_mon,
        extra_outputs=get_btracking_extra_outputs(parts_with_btracking),
        persistreco=persistreco,
        raw_banks=get_btracking_raw_banks(persist_raw_banks),
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopksLL_tautopipipinu_line(
    name="Hlt2SLB_LbToLcTauNu_LcToPKSLL_TauToPiPiPiNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) tau-, hadronic tau decay.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautopipipinu(
        process=PROCESS, V0_name="KS0", V0_type="LL"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopksDD_tautopipipinu_line(
    name="Hlt2SLB_LbToLcTauNu_LcToPKSDD_TauToPiPiPiNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> p KS0) tau-, hadronic tau decay.
    Here (KS0 -> pi+ pi-) is reconstructed using two 'long' tracks ('DD' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautopipipinu(
        process=PROCESS, V0_name="KS0", V0_type="DD"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctolambdapiLL_tautopipipinu_line(
    name="Hlt2SLB_LbToLcTauNu_LcToLambdaPiLL_TauToPiPiPiNu",
    prescale=1,
    persistreco=True,
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) tau-, hadronic tau decay.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('LL' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautopipipinu(
        process=PROCESS, V0_name="Lambda0", V0_type="LL"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctolambdapiDD_tautopipipinu_line(
    name="Hlt2SLB_LbToLcTauNu_LcToLambdaPiDD_TauToPiPiPiNu",
    prescale=1,
    persistreco=True,
):
    """
    SL Hlt2 line for the decay Lb0 -> Lc+(-> Lambda0 pi+) tau-, hadronic tau decay.
    Here (Lambda0 -> pi+ pi-) is reconstructed using two 'long' tracks ('DD' category).
    """
    line_alg = make_lbtolctaunu_lctoV0h_tautopipipinu(
        process=PROCESS, V0_name="Lambda0", V0_type="DD"
    )

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        persistreco=persistreco,
        name=name,
        prescale=prescale,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_lbtolctaunu_lctopphi_tautopipipinu_line(
    name="Hlt2SLB_LbToLcTauNu_LcToPPhi_TauToPiPiPiNu", prescale=1, persistreco=True
):
    """
    SL Hlt2 line for the decay Lambda_b0 -> Lambda_c+ tau-, hadronic tau decay.
    """
    line_alg = make_lbtolctaunu_lctopphi_tautopipipinu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=False, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_xibminustoxic0taunu_xic0topkkpi_tautopipipinu_line(
    name="Hlt2SLB_XibminusToXic0TauNu_Xic0ToPKKPi_TauToPiPiPiNu",
    prescale=1.0,
    persistreco=True,
):
    """
    SL Hlt2 line for the decays:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau- nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau+ nu (Wrong sign)
    """
    line_alg = make_xibminustoxic0taunu_xic0topkkpi_tautopipipinu(process=PROCESS)
    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )


@register_line_builder(hlt2_lines)
def hlt2_omegabtoomegactaunu_omegactopkkpi_tautopipipinu_line(
    name="Hlt2SLB_OmegabToOmegacTauNu_OmegacToPKKPi_TauToPiPiPiNu",
    prescale=1,
    persistreco=True,
):
    """
    SL Hlt2 line for the decay Omegab- -> Omegac0(-> p K- K- pi+) tau- nu.
    """
    line_alg = make_omegabtoomegactaunu_omegactopkkpi_tautopipipinu(process=PROCESS)

    line_mon = generate_default_monitoring_vars(
        name, line_alg, charged=True, charmed=True
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=sl_line_prefilter() + [line_alg] + line_mon,
        monitoring_variables=_MONITORING_VARIABLES,
    )
