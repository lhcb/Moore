###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from GaudiKernel.SystemOfUnits import MeV, mm
from PyConf import ConfigurationError
from RecoConf.algorithms_thor import ParticleContainersMerger

from .builders.b_builder import make_b2xtaunu
from .builders.base_builder import make_tauons_hadronic_decay
from .builders.charm_hadron_builder import (
    make_d0_tok3pi,
    make_d0_tokpi,
    make_dplus_tokpipi,
    make_ds_tokkpi,
    make_Hc_to_nbody,
    make_jpsi_tomumu,
    make_lambdac_tolambda0pi,
    make_lambdac_topkpi,
    make_lambdac_topks,
    make_lambdac_topphi,
    make_omegac_topkkpi,
    make_xic0_topkkpi,
)

_B0_M = 5279.65 * MeV
_Bp_M = 5279.34 * MeV
_Bc_M = 6274.47 * MeV
_Bs_M = 5366.88 * MeV
_Lb_M = 5619.60 * MeV


def make_bptod0taunu_d0tok3pi_tautopipipinu(process):
    """
    Selection for the decay B+ -> D~0 tau+, hadronic tau decay, D~0 -> K- pi- pi+ pi+.
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    tauons = make_tauons_hadronic_decay()

    cuts = {
        "comb_docachi2_max": 5.0,
        "kaon_pid": (F.PID_K > 4.0),
        "pion_pid": (F.PID_K < 2.0),
    }  # Tighter D0 cut

    d0s = make_d0_tok3pi(**cuts)

    m_min = _Bp_M - 3000 * MeV
    m_max = _Bp_M + 2000 * MeV
    comb_m_min = _Bp_M - 3000 * MeV
    comb_m_max = _Bp_M + 2000 * MeV
    bpvdira_min = 0.995
    vchi2pdof_max = 100.0
    bpvfdchi2_min = 0.0

    b_rs = make_b2xtaunu(
        particles=[d0s, tauons],
        descriptor="[B+ -> D~0 tau+]cc",
        name="SLB_BToD0TauNuCombiner_{hash}",
        m_min=m_min,
        m_max=m_max,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min,
    )
    b_ws = make_b2xtaunu(
        particles=[d0s, tauons],
        descriptor="[B- -> D~0 tau-]cc",
        name="SLB_BToD0TauNuCombiner_{hash}",
        m_min=m_min,
        m_max=m_max,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min,
    )
    line_alg = ParticleContainersMerger([b_rs, b_ws])
    return line_alg


def make_b0todptaunu_dptokpipi_tautopipipinu(process):
    """
    Selection for the decay B0 -> D- tau+, hadronic tau decay.
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    tauons = make_tauons_hadronic_decay()
    dps = make_dplus_tokpipi(daughter_pt_min=400 * MeV)
    # IMPORTANT NOTE: we need to check if the cut trpchi2 > 0.01 is needed for the Dp children.
    b0s_rightsign = make_b2xtaunu(
        particles=[dps, tauons],
        descriptor="[B0 -> D- tau+]cc",
        name="B0ToDpTauNuCombiner_RS_{hash}",
    )
    b0s_wrongsign = make_b2xtaunu(
        particles=[dps, tauons],
        descriptor="[B0 -> D- tau-]cc",
        name="B0ToDpTauNuCombiner_WS_{hash}",
    )
    line_alg = ParticleContainersMerger([b0s_rightsign, b0s_wrongsign])

    return line_alg


def make_bctojpsitaunu_jpsitomumu_tautopipipinu(process):
    """
    Selection for the decay B_c+ -> J/psi(1S) tau+, hadronic tau decay.
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    tauons = make_tauons_hadronic_decay()
    jpsis = make_jpsi_tomumu()

    m_min = _Bc_M - 3000 * MeV
    m_max = _Bc_M + 2000 * MeV
    comb_m_min = _Bc_M - 3000 * MeV
    comb_m_max = _Bc_M + 2000 * MeV
    comb_doca_max = 0.2 * mm
    bpvdira_min = 0.995
    vchi2pdof_max = 100.0
    bpvfdchi2_min = 0.0

    line_alg = make_b2xtaunu(
        particles=[jpsis, tauons],
        descriptor="[B_c+ -> J/psi(1S) tau+]cc",
        name="SLB_BToJpsiTauNuCombiner_{hash}",
        m_min=m_min,
        m_max=m_max,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min,
    )

    return line_alg


def make_bptod0taunu_d0tokpi_tautopipipinu(process):
    """
    Selection for the decay B+ -> D~0 tau+, hadronic tau decay.
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    tauons = make_tauons_hadronic_decay()

    cuts = {
        "comb_docachi2_max": 5.0,
        "daughter_pt_min": 400 * MeV,
        "kaon_pid": (F.PID_K > 4.0),
        "pion_pid": (F.PID_K < 2.0),
    }  # Tighter D0 cut, following butod0enu

    dzeros = make_d0_tokpi(**cuts)

    m_min = _Bp_M - 3000 * MeV
    m_max = _Bp_M + 2000 * MeV
    comb_m_min = _Bp_M - 3000 * MeV
    comb_m_max = _Bp_M + 2000 * MeV
    comb_doca_max = 0.2 * mm
    bpvdira_min = 0.995
    vchi2pdof_max = 100.0
    bpvfdchi2_min = 0.0

    b_rs = make_b2xtaunu(
        particles=[dzeros, tauons],
        descriptor="[B+ -> D~0 tau+]cc",
        name="SLB_BToD0TauNuCombiner_{hash}",
        m_min=m_min,
        m_max=m_max,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min,
    )
    b_ws = make_b2xtaunu(
        particles=[dzeros, tauons],
        descriptor="[B- -> D~0 tau-]cc",
        name="SLB_BToD0TauNuCombiner_{hash}",
        m_min=m_min,
        m_max=m_max,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min,
    )
    line_alg = ParticleContainersMerger([b_rs, b_ws])
    return line_alg


def make_bstodstaunu_dstokkpi_tautopipipinu(process):
    """
    Selection for the decay B_s0 -> D_s- tau+, hadronic tau decay.
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    tauons = make_tauons_hadronic_decay()
    dss = make_ds_tokkpi(
        # comb_m_min=1760 * MeV,
        # comb_m_max=2080 * MeV,
        daughter_pt_min=400 * MeV
    )

    m_min = _Bs_M - 3000 * MeV
    m_max = _Bs_M + 2000 * MeV
    comb_mb_min = _Bs_M - 3000 * MeV
    comb_mb_max = _Bs_M + 2000 * MeV
    comb_doca_max = 0.2 * mm
    bpvdira_min = 0.995
    vchi2pdof_max = 100.0
    bpvfdchi2_min = 0.0

    bss_rightsign = make_b2xtaunu(
        particles=[dss, tauons],
        descriptor="[B_s0 -> D_s- tau+]cc",
        name="SLB_BsToDsTauNuCombiner_RS_{hash}",
        m_min=m_min,
        m_max=m_max,
        comb_m_min=comb_mb_min,
        comb_m_max=comb_mb_max,
        comb_doca_max=comb_doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min,
    )

    bss_wrongsign = make_b2xtaunu(
        particles=[dss, tauons],
        descriptor="[B_s0 -> D_s- tau-]cc",
        name="SLB_BsToDsTauNuCombiner_WS_{hash}",
        m_min=m_min,
        m_max=m_max,
        comb_m_min=comb_mb_min,
        comb_m_max=comb_mb_max,
        comb_doca_max=comb_doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min,
    )

    line_alg = ParticleContainersMerger([bss_rightsign, bss_wrongsign])

    return line_alg


def make_lbtolctaunu_lctopkpi_tautopipipinu(process):
    """
    Selection for the decay Lambda_b0 -> Lambda_c+ tau-, hadronic tau decay.
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    tauons = make_tauons_hadronic_decay()

    lcs = make_lambdac_topkpi()

    m_min = _Lb_M - 3000 * MeV
    m_max = _Lb_M + 2000 * MeV
    comb_m_min = _Lb_M - 3000 * MeV
    comb_m_max = _Lb_M + 2000 * MeV
    comb_doca_max = 0.2 * mm
    bpvdira_min = 0.995
    vchi2pdof_max = 100.0
    bpvfdchi2_min = 0.0
    # AMAXDOCA<0.15

    Lbs_rightsign = make_b2xtaunu(
        particles=[lcs, tauons],
        descriptor="[Lambda_b0 -> Lambda_c+ tau-]cc",
        name="SLB_LbToLcTauNuCombiner_RS_{hash}",
        m_min=m_min,
        m_max=m_max,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min,
    )

    Lbs_wrongsign = make_b2xtaunu(
        particles=[lcs, tauons],
        descriptor="[Lambda_b0 -> Lambda_c+ tau+]cc",
        name="SLB_LbToLcTauNuCombiner_WS_{hash}",
        m_min=m_min,
        m_max=m_max,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min,
    )

    line_alg = ParticleContainersMerger([Lbs_rightsign, Lbs_wrongsign])

    return line_alg


def make_lbtolctaunu_lctoV0h_tautopipipinu(process, V0_name, V0_type):
    """
    Selection for the decay Lambda_b0 -> Lambda_c+ tau-, hadronic tau decay.
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    Lc_mass = 2286.46 * MeV
    delta_Lc_comb = 100.0 * MeV
    delta_Lc_mother = 80.0 * MeV

    if V0_name == "Lambda0":
        lc_maker = make_lambdac_tolambda0pi
    elif V0_name == "KS0":
        lc_maker = make_lambdac_topks
    else:
        raise ConfigurationError(
            f"V0_name must be either 'Lambda0' or 'KS'. Got '{V0_name}' instead. Please check."
        )

    with make_Hc_to_nbody.bind(vchi2pdof_max=6.0, bpvfdchi2_min=None, bpvdira_min=None):
        lcs = lc_maker(
            V0_type,
            mother_m_min=(Lc_mass - delta_Lc_mother) * MeV,
            mother_m_max=(Lc_mass + delta_Lc_mother) * MeV,
            comb_m_min=(Lc_mass - delta_Lc_comb) * MeV,
            comb_m_max=(Lc_mass + delta_Lc_comb) * MeV,
        )

    tauons = make_tauons_hadronic_decay()
    descriptor_rs = "[Lambda_b0 -> Lambda_c+ tau-]cc"
    descriptor_ws = "[Lambda_b0 -> Lambda_c+ tau+]cc"

    with make_Hc_to_nbody.bind(vchi2pdof_max=6.0, bpvfdchi2_min=None, bpvdira_min=None):
        lcs = lc_maker(
            V0_type,
            mother_m_min=(Lc_mass - delta_Lc_mother) * MeV,
            mother_m_max=(Lc_mass + delta_Lc_mother) * MeV,
            comb_m_min=(Lc_mass - delta_Lc_comb) * MeV,
            comb_m_max=(Lc_mass + delta_Lc_comb) * MeV,
        )

    m_min = _Lb_M - 3000 * MeV
    m_max = _Lb_M + 2000 * MeV
    comb_m_min = _Lb_M - 3000 * MeV
    comb_m_max = _Lb_M + 2000 * MeV
    comb_doca_max = 0.5 * mm
    bpvdira_min = 0.995
    vchi2pdof_max = 100.0
    bpvfdchi2_min = 0.0

    lb_rightsign = make_b2xtaunu(
        particles=[lcs, tauons],
        descriptor=descriptor_rs,
        m_min=m_min,
        m_max=m_max,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min,
    )

    lb_wrongsign = make_b2xtaunu(
        particles=[lcs, tauons],
        descriptor=descriptor_ws,
        m_min=m_min,
        m_max=m_max,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min,
    )

    line_alg = ParticleContainersMerger([lb_rightsign, lb_wrongsign])

    return line_alg


def make_lbtolctaunu_lctopphi_tautopipipinu(process):
    """
    Selection for the decay Lambda_b0 -> Lambda_c+ tau-, hadronic tau decay.
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    tauons = make_tauons_hadronic_decay()

    Lc_mass = 2286.46 * MeV
    delta_Lc_comb = 100.0 * MeV
    delta_Lc_mother = 80.0 * MeV

    lcs = make_lambdac_topphi(
        mother_m_min=(Lc_mass - delta_Lc_mother),
        mother_m_max=(Lc_mass + delta_Lc_mother),
        mother_pt_min=2500 * MeV,
        comb_m_min=(Lc_mass - delta_Lc_comb),
        comb_m_max=(Lc_mass + delta_Lc_comb),
        comb_pt_min=None,
        comb_pt_any_min=None,
        comb_pt_sum_min=2500 * MeV,
        daughter_p_min=2000 * MeV,
        daughter_pt_min=400 * MeV,
        daughter_mipchi2_min=4.0,
        proton_pid=F.require_all(F.PID_P > 0.0, (F.PID_P - F.PID_K) > 0.0),
        comb_docachi2_max=20.0,
    )

    m_min = _Lb_M - 3000 * MeV
    m_max = _Lb_M + 2000 * MeV
    comb_m_min = _Lb_M - 3000 * MeV
    comb_m_max = _Lb_M + 2000 * MeV
    comb_doca_max = 0.5 * mm
    bpvdira_min = 0.995
    vchi2pdof_max = 100.0
    bpvfdchi2_min = 0.0
    # AMAXDOCA<0.15

    Lbs_rightsign = make_b2xtaunu(
        particles=[lcs, tauons],
        descriptor="[Lambda_b0 -> Lambda_c+ tau-]cc",
        name="SLB_LbToLcTauNuCombiner_RS_{hash}",
        m_min=m_min,
        m_max=m_max,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min,
    )

    Lbs_wrongsign = make_b2xtaunu(
        particles=[lcs, tauons],
        descriptor="[Lambda_b0 -> Lambda_c+ tau+]cc",
        name="SLB_LbToLcTauNuCombiner_WS_{hash}",
        m_min=m_min,
        m_max=m_max,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvdira_min=bpvdira_min,
        bpvfdchi2_min=bpvfdchi2_min,
    )

    line_alg = ParticleContainersMerger([Lbs_rightsign, Lbs_wrongsign])

    return line_alg


def make_xibminustoxic0taunu_xic0topkkpi_tautopipipinu(process):
    """
    SL line for the decays:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau- nu (Right sign), hadronic tau decay
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau+ nu (Wrong sign), hadronic tau decay
    """
    # make taus
    taus = make_tauons_hadronic_decay()

    descriptor_rs = "[Xi_b- -> Xi_c0 tau-]cc"
    descriptor_ws = "[Xi_b- -> Xi_c0 tau+]cc"

    # make Xi_c0 candidates
    xic0 = make_xic0_topkkpi()

    # make Xib- candidates
    xibs_rs = make_b2xtaunu(
        name="XibminusToXic0tauBuilder_TauToPiPiPiNu_RS",
        descriptor=descriptor_rs,
        particles=[xic0, taus],
    )
    xibs_ws = make_b2xtaunu(
        name="XibminusToXic0tauBuilder_TauToPiPiPiNu_WS",
        descriptor=descriptor_ws,
        particles=[xic0, taus],
    )
    return ParticleContainersMerger([xibs_rs, xibs_ws])


def make_omegabtoomegactaunu_omegactopkkpi_tautopipipinu(process):
    """
    Selection for the decay [Omegab- -> Omegac0(-> p K- K- pi+) tau- nu]cc, hadronic tau decay
    """

    tauons = make_tauons_hadronic_decay()
    descriptor_rs = "[Omega_b- -> Omega_c0 tau-]cc"
    descriptor_ws = "[Omega_b- -> Omega_c0 tau+]cc"

    omegacs = make_omegac_topkkpi()

    omegab_rightsign = make_b2xtaunu(
        particles=[omegacs, tauons],
        descriptor=descriptor_rs,
        name="Omegab2OmegacTauNu_TauToPiPiPiNu_OmegacTopKKPi_combiner",
    )
    omegab_wrongsign = make_b2xtaunu(
        particles=[omegacs, tauons],
        descriptor=descriptor_ws,
        name="Omegab2OmegacTauNu_TauToPiPiPiNu_OmegacTopKKPi_WS_combiner",
    )
    linealg = ParticleContainersMerger([omegab_rightsign, omegab_wrongsign])

    return linealg
