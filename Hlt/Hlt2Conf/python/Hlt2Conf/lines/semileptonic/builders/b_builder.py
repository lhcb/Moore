###############################################################################
# (c) Copyright 2020-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Semileptonic B builder, based on B2OC code."""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner


def make_b2xclnu(
    particles,
    descriptor,
    name="BToXcLNuCombiner_{hash}",
    comb_m_min=2200 * MeV,
    comb_m_max=8000 * MeV,
    comb_docachi2_max=10.0,
    mother_m_min=2200 * MeV,
    mother_m_max=8000 * MeV,
    vchi2pdof_max=9.0,
    bpvdira_min=0.999,
    b_d_dzs_min=-2.0 * mm,
    pt_min=0 * MeV,
    p_min=0 * MeV,
    comb_doca_max=None,
    bpvfdchi2_min=None,
    bpvvdz_min=None,
):
    """
    Base SL b-hadron decay builder.

    Parameters
    ----------
    particles :
        Maker algorithm instances for input particles.
    descriptor :
        Decay descriptor to be reconstructed.
    name :
        Name of the combiner.
    comb_m_min :
        Lower invariant mass limit for the particle combination.
    comb_m_max :
        Upper invariant mass limit for the particle combination.
    comb_docachi2_max :
        Maximum DOCA chi2 for the particle combination.
    mother_m_min :
        Lower invariant mass limit for the mother particle.
    mother_m_max :
        Upper invariant mass limit for the mother particle.
    vchi2pdof_max :
        Maximum vertex chi2 per DOF for the mother particle.
    bpvdira_min :
        Minimum DIRA to BPV for the mother particle.
    b_d_dzs_min :
        Minimum distance between the B and D end vertices.
    pt_min :
        Minimum PT for the final combined particle.
    p_min :
        Minimum P for the final combined particle.
    comb_doca_max :
        Maximum DOCA for the particle combination.
    bpvfdchi2_min :
        Minimum flight distance chi2 for the mother particle.
    bpvvdz_min :
        Minimum vertex z distance to the PV for the mother particle.
    """
    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max), F.PT > pt_min, F.P > p_min
    )

    if comb_docachi2_max is not None:
        combination_code = F.require_all(
            combination_code, F.MAXSDOCACHI2CUT(comb_docachi2_max)
        )
    if comb_doca_max is not None:
        combination_code = F.require_all(combination_code, F.MAXSDOCACUT(comb_doca_max))

    vertex_code = F.require_all(
        in_range(mother_m_min, F.MASS, mother_m_max),
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVDIRA > bpvdira_min,
    )

    if bpvvdz_min is not None:
        vertex_code = F.require_all(vertex_code, F.OWNPVVDZ > bpvvdz_min)
    if b_d_dzs_min is not None:
        # charm_endvtx_z = F.CHILD(1, F.END_VZ)  #Alternative to MINTREE
        charm_endvtx_z = F.MINTREE(
            F.IS_ABS_ID("D_s-")
            | F.IS_ABS_ID("D+")
            | F.IS_ABS_ID("D0")
            | F.IS_ABS_ID("Lambda_c+")
            | F.IS_ABS_ID("Omega_c0")
            | F.IS_ABS_ID("Xi_c+")
            | F.IS_ABS_ID("Xi_c0")
            | F.IS_ABS_ID("J/psi(1S)"),
            F.END_VZ,
        )

        vertex_code = F.require_all(
            vertex_code, charm_endvtx_z - F.END_VZ > b_d_dzs_min
        )

    if bpvfdchi2_min is not None:
        vertex_code = F.require_all(vertex_code, F.OWNPVFDCHI2 > bpvfdchi2_min)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_b2xulnu(
    particles,
    descriptor,
    name="BToXuLNuCombiner_{hash}",
    comb_m_min=1000 * MeV,
    comb_m_max=10000 * MeV,
    vchi2pdof_max=4.0,
    bpvdira_min=0.994,
    pt_min=1500 * MeV,
    bpvfdchi2_min=120,
    mcorr_min=2500.0 * MeV,
    mcorr_max=7000.0 * MeV,
    comb_doca_max=None,
):
    """
    SL b-hadron decay builder specialized in b->u transitions.

    Parameters
    ----------
    particles :
        Maker algorithm instances for input particles.
    descriptor :
        Decay descriptor to be reconstructed.
    name :
        Name of the combiner.
    make_pvs :
        Function to make the primary vertices.
    comb_m_min :
        Lower invariant mass limit for the particle combination.
    comb_m_max :
        Upper invariant mass limit for the particle combination.
    vchi2pdof_max :
        Maximum vertex chi2 per DOF for the mother particle.
    bpvdira_min :
        Minimum DIRA to BPV for the mother particle.
    pt_min :
        Minimum PT for the final combined particle.
    bpvfdchi2_min :
        Minimum flight distance chi2 for the mother particle.
    mcorr_min :
        Lower invariant mass limit for the mother particle.
    mcorr_max :
        Upper invariant mass limit for the mother particle.
    comb_doca_max :
        Maximum DOCA for the particle combination.
    """
    combination_code = F.require_all(in_range(comb_m_min, F.MASS, comb_m_max))
    if comb_doca_max is not None:
        combination_code = F.require_all(combination_code, F.MAXSDOCACUT(comb_doca_max))

    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVDIRA > bpvdira_min,
        F.PT > pt_min,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
        in_range(mcorr_min, F.OWNPVCORRM, mcorr_max),
    )

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_b2xtaunu(
    particles,
    descriptor,
    name="BToXTauNuCombiner_{hash}",
    m_min=0.0 * MeV,
    m_max=10000.0 * MeV,
    comb_m_min=0.0 * MeV,
    comb_m_max=11000.0 * MeV,
    comb_doca_max=0.5 * mm,
    vchi2pdof_max=6,
    bpvdira_min=0.999,
    b_d_dzs_min=None,
    bpvfdchi2_min=50,
):
    """
    SL b-hadron decay builder specialized in semi-tauonic decays.

    Parameters
    ----------
    particles :
        Maker algorithm instances for input particles.
    descriptor :
        Decay descriptor to be reconstructed.
    name :
        Name of the combiner.
    m_min :
        Lower invariant mass limit for the mother particle.
    m_max :
        Upper invariant mass limit for the mother particle.
    comb_m_min :
        Lower invariant mass limit for the particle combination.
    comb_m_max :
        Upper invariant mass limit for the particle combination.
    comb_doca_max :
        Maximum DOCA for the particle combination.
    vchi2pdof_max :
        Maximum vertex chi2 per DOF for the mother particle.
    bpvdira_min :
        Minimum DIRA to BPV for the mother particle.
    b_d_dzs_min :
        Minimum distance between the B and D end vertices.
    bpvfdchi2_min :
        Minimum flight distance chi2 for the mother particle
    """
    combination_code = F.require_all(in_range(comb_m_min, F.MASS, comb_m_max))

    if comb_doca_max is not None:
        combination_code = F.require_all(combination_code, F.MAXSDOCACUT(comb_doca_max))

    vertex_requirements = [
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVDIRA > bpvdira_min,
    ]

    if bpvfdchi2_min is not None:
        vertex_requirements.append(F.OWNPVFDCHI2 > bpvfdchi2_min)

    if b_d_dzs_min is not None:
        # charm_endvtx_z = F.CHILD(1, F.END_VZ)  #Alternative to MINTREE
        charm_endvtx_z = F.MINTREE(
            F.IS_ABS_ID("D_s-")
            | F.IS_ABS_ID("D+")
            | F.IS_ABS_ID("D0")
            | F.IS_ABS_ID("Lambda_c+")
            | F.IS_ABS_ID("Omega_c0")
            | F.IS_ABS_ID("Xi_c+")
            | F.IS_ABS_ID("Xi_c0")
            | F.IS_ABS_ID("J/psi(1S)"),
            F.END_VZ,
        )

        vertex_requirements.append(charm_endvtx_z - F.END_VZ > b_d_dzs_min)

    vertex_code = F.require_all(*vertex_requirements)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_b2hhmunu(
    particles,
    descriptors,
    name="B2HHMuNuCombiner_{hash}",
    m_min=1800 * MeV,
    m_max=5300 * MeV,
    fdchi2_min=50,
    sum_pt_min=5 * GeV,
    vtx_chi2pdof_max=15.0,
    bpvdira_min=0.999,
    mcorr_min=2500.0 * MeV,
    mcorr_max=7000.0 * MeV,
):
    """
    SL b-hadron builder to a final state of two NR hadrons and muon

    Parameters
    ----------
    particles :
        Maker algorithm instances for input particles.
    descriptors :
        Decay descriptors to be reconstructed.
    name :
        Name of the combiner.
    m_min :
        Lower invariant mass limit for the particle combination.
    m_max :
        Upper invariant mass limit for the particle combination.
    fdchi2_min :
        Minimum flight distance chi2 for the combined particle.
    sum_pt_min :
        Minimum PT sum for the combined particle.
    vtx_chi2pdof_max :
        Maximum vertex chi2 per DOF for the combined particle.
    bpvdira_min :
        Minimum DIRA to BPV for the combined particle.
    mcorr_min :
        Lower invariant mass limit for the mother particle.
    mcorr_max :
        Upper invariant mass limit for the mother particle.
    """
    combination_code = F.require_all(
        in_range(m_min, F.MASS, m_max), F.SUM(F.PT) > sum_pt_min
    )
    vertex_code = F.require_all(
        F.OWNPVFDCHI2 > fdchi2_min,
        F.CHI2DOF < vtx_chi2pdof_max,
        F.OWNPVDIRA > bpvdira_min,
        in_range(mcorr_min, F.OWNPVCORRM, mcorr_max),
    )

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptors,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_b2lllnu(
    particles,
    descriptor,
    name="BToLLLNuCombiner_{hash}",
    comb_m_min=1000.0 * MeV,
    comb_m_max=10000.0 * MeV,
    vchi2pdof_max=4.0,
    bpvdira_min=0.995,
    pt=1500.0 * MeV,
    bpvfdchi2_min=30.0,
    mcorr_min=2500.0 * MeV,
    mcorr_max=10000.0 * MeV,
    comb_doca_max=20.0,
    trghostprob=0.4,
    additional_comb_cut=None,
):
    """
    SL b-hadron decay builder specialized in B(c)+ -> l+ l+ l- nu transitions.

    Parameters
    ----------
    particles :
        Maker algorithm instances for input particles.
    descriptor :
        Decay descriptor to be reconstructed.
    name :
        Name of the combiner.
    comb_m_min :
        Lower invariant mass limit for the particle combination.
    comb_m_max :
        Upper invariant mass limit for the particle combination.
    vchi2pdof_max :
        Maximum vertex chi2 per DOF for the mother particle.
    bpvdira_min :
        Minimum DIRA to BPV for the mother particle.
    pt :
        Minimum PT for the final combined particle.
    bpvfdchi2_min :
        Minimum flight distance chi2 for the mother particle.
    mcorr_min :
        Lower invariant mass limit for the mother particle.
    mcorr_max :
        Upper invariant mass limit for the mother particle.
    comb_doca_max :
        Maximum DOCA for the particle combination.
    trghostprob :
        Maximum ghost probability for the final state particles.
    additional_comb_cut :
        Additional combination cut to be applied to the combiner.
    """

    combination_code = F.require_all(in_range(comb_m_min, F.MASS, comb_m_max))
    if comb_doca_max is not None:
        combination_code = F.require_all(combination_code, F.MAXSDOCACUT(comb_doca_max))

    vertex_code = F.require_all(
        F.PT > pt,
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVDIRA > bpvdira_min,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
        in_range(mcorr_min, F.OWNPVCORRM, mcorr_max),
        F.MAXTREE(
            F.IS_ABS_ID("e+") | F.IS_ABS_ID("mu+") | F.IS_ABS_ID("pi+"), F.GHOSTPROB
        )
        < trghostprob,
    )

    if additional_comb_cut is not None:
        combination_code = F.require_all(combination_code, additional_comb_cut)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_bs2hh(
    particles,
    descriptor,
    name="Bs2HHCombiner",
    am_min=5000 * MeV,
    am_max=6000 * MeV,
    vchi2pdof_max=25,
    bpvdira_min=0.9995,
    bpvvdchi2_min=100,
    bpvdz_min=2,
):
    """
    SL Bs builder to a final state of two mesons

    Parameters
    ----------
    particles :
        Maker algorithm instances for input particles.
    descriptor :
        Decay descriptor to be reconstructed.
    name :
        Name of the combiner.
    am_min :
        Lower invariant mass limit for the particle combination.
    am_max :
        Upper invariant mass limit for the particle combination.
    vchi2pdof_max :
        Maximum vertex chi2 per DOF for the mother particle.
    bpvdira_min :
        Minimum DIRA to BPV for the mother particle.
    bpvvdchi2_min :
        Minimum flight distance chi2 for the mother particle.
    bpvdz_min :
        Minimum vertex z distance to the PV for the mother particle.
    """

    combination_code = F.require_all(in_range(am_min, F.MASS, am_max))
    mother_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        bpvdira_min < F.OWNPVDIRA,
        bpvvdchi2_min < F.OWNPVFDCHI2,
        bpvdz_min < F.OWNPVVDZ,
    )

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=mother_code,
    )
