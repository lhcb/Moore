###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of dilepton builders, used by the B->3lnu lines
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.standard_particles import (
    _make_dielectron_with_brem,
    filter_leptons_loose,
    make_detached_dielectron,
    make_detached_dielectron_with_brem,
    make_down_electrons_no_brem,
    make_long_electrons_no_brem,
    make_long_upstream_and_downstream_electrons_no_brem,
)

from Hlt2Conf.lines.config_pid import no_pid_sl

from .base_builder import make_muons_from_b


@configurable
def make_detached_dimuon_for_b2lllnu(
    name="detached_dimuon_for_b2lllnu_{hash}",
    parent_id="J/psi(1S)",
    pt_dimuon_min=0.0 * MeV,
    pt_muon_min=300.0 * MeV,
    p_muon_min=3000.0 * MeV,
    ipchi2_muon_min=9.0,
    pidmu_muon_min=-4.0,
    adocachi2cut_max=36.0,
    bpvvdchi2_min=16.0,
    vchi2pdof_max=30.0,
    am_min=0.0 * MeV,
    am_max=7000.0 * MeV,
    same_sign=False,
):
    """
    Make the detached dimuon, opposite-sign or same-sign.
    """
    muons = make_muons_from_b(
        pt_min=pt_muon_min,
        p_min=p_muon_min,
        mipchi2_min=ipchi2_muon_min,
        pid=(F.PID_MU > pidmu_muon_min),
    )

    DecayDescriptor = f"{parent_id} -> mu+ mu-"
    if same_sign:
        DecayDescriptor = f"[{parent_id} -> mu+ mu+]cc"

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.PT > pt_dimuon_min,
        F.MAXSDOCACHI2CUT(adocachi2cut_max),
    )
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max, F.OWNPVFDCHI2 > bpvvdchi2_min
    )
    return ParticleCombiner(
        [muons, muons],
        name=name,
        DecayDescriptor=DecayDescriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_detached_dielectron_for_b2lllnu(
    name="detached_dielectron_for_b2lllnu_{hash}",
    pid_e_min=0.0,
    pt_e_min=350 * MeV,
    ipchi2_e_min=25.0,
    parent_id="J/psi(1S)",
    opposite_sign=True,
    pt_diE_min=0.0 * MeV,
    adocachi2cut_max=36.0,
    bpvvdchi2_min=36.0,
    vfaspfchi2ndof_max=10.0,
    am_min=0.0 * MeV,
    am_max=6000.0 * MeV,
):
    """
    Make the detached e+e- pair with the proper bremsstrahlung correction handling.
    """

    if no_pid_sl():
        pid_e_min = None

    dielectrons = make_detached_dielectron_with_brem(
        opposite_sign=opposite_sign,
        PIDe_min=pid_e_min,
        pt_e=pt_e_min,
        minipchi2=ipchi2_e_min,
        dielectron_ID=parent_id,
        pt_diE=pt_diE_min,
        m_diE_min=am_min,
        m_diE_max=am_max,
        adocachi2cut=adocachi2cut_max,
        bpvvdchi2=bpvvdchi2_min,
        vfaspfchi2ndof=vfaspfchi2ndof_max,
    )
    code = F.require_all(F.MASS > am_min, F.MASS < am_max)
    return ParticleFilter(dielectrons, F.FILTER(code), name=name)


@configurable
def make_gammaLL_for_b2lllnu(
    name="gammaLL_for_b2lllnu_{hash}",
    pid_e_min=-2.0,
    pt_e_min=100 * MeV,
    p_e_min=1.0 * GeV,
    pt_min=1.5 * GeV,
    p_min=0.0 * GeV,
    ipchi2_e_min=9.0,
    vfaspfchi2ndof_max=10.0,
    m_max=50.0 * MeV,
):
    """
    Make the detached e+e- pair with the proper bremsstrahlung correction handling.
    """

    if no_pid_sl():
        pid_e_min = None

    electrons = filter_leptons_loose(
        particles=make_long_electrons_no_brem(),
        lepton="electron",
        pt_min=pt_e_min,
        p_min=p_e_min,
        pid_e=pid_e_min,
        minipchi2=ipchi2_e_min,
    )

    dielectron_with_brem = _make_dielectron_with_brem(
        electrons,
        pt_diE=pt_min,
        m_diE_min=0.0 * MeV,
        m_diE_max=m_max,
        m_diE_ID="gamma",
    )

    code_dielectron = F.require_all(
        F.P > p_min,
        F.CHI2DOF < vfaspfchi2ndof_max,
    )

    return ParticleFilter(dielectron_with_brem, F.FILTER(code_dielectron), name=name)


@configurable
def make_gammaDD_for_b2lllnu(
    name="gammaDD_for_b2lllnu_{hash}",
    pid_e_min=-2.0,
    pt_e_min=100 * MeV,
    p_e_min=1.0 * GeV,
    pt_min=1.5 * GeV,
    p_min=0.0 * GeV,
    ipchi2_e_min=9.0,
    vfaspfchi2ndof_max=10.0,
    m_max=100.0 * MeV,
):
    """
    Make the detached e+e- pair with the proper bremsstrahlung correction handling.
    """

    if no_pid_sl():
        pid_e_min = None

    electrons = filter_leptons_loose(
        particles=make_down_electrons_no_brem(),
        lepton="electron",
        pt_min=pt_e_min,
        p_min=p_e_min,
        pid_e=pid_e_min,
        minipchi2=ipchi2_e_min,
    )

    dielectron_with_brem = _make_dielectron_with_brem(
        electrons,
        pt_diE=pt_min,
        m_diE_min=0.0 * MeV,
        m_diE_max=m_max,
        m_diE_ID="gamma",
    )

    code_dielectron = F.require_all(
        F.P > p_min,
        F.CHI2DOF < vfaspfchi2ndof_max,
    )

    return ParticleFilter(dielectron_with_brem, F.FILTER(code_dielectron), name=name)


@configurable
def make_detached_dielectron_for_b2dlllnu(
    name="detached_dielectron_for_b2dlllnu_{hash}",
    pid_e_min=2.0,
    pt_e_min=50 * MeV,
    p_e_min=0.0 * MeV,
    ipchi2_e_min=4.0,
    parent_id="J/psi(1S)",
    opposite_sign=True,
    pt_diE_min=0.0 * MeV,
    adocachi2cut_min=30.0,
    bpvvdchi2_min=30.0,
    vfaspfchi2ndof_max=10.0,
    am_min=0.0 * MeV,
    am_max=6000.0 * MeV,
    with_brem=True,
    with_upstream_and_downstream=False,
):
    """
    Make the detached e+e- pair with/without the proper bremsstrahlung correction handling.
    """
    electron_maker = (
        make_long_upstream_and_downstream_electrons_no_brem
        if with_upstream_and_downstream
        else make_long_electrons_no_brem
    )

    if no_pid_sl():
        pid_e_min = None

    if with_brem:
        return make_detached_dielectron_with_brem(
            electron_maker=electron_maker,
            opposite_sign=opposite_sign,
            PIDe_min=pid_e_min,
            pt_e=pt_e_min,
            p_e=p_e_min,
            minipchi2=ipchi2_e_min,
            dielectron_ID=parent_id,
            pt_diE=pt_diE_min,
            m_diE_min=am_min,
            m_diE_max=am_max,
            adocachi2cut=adocachi2cut_min,
            bpvvdchi2=bpvvdchi2_min,
            vfaspfchi2ndof=vfaspfchi2ndof_max,
        )
    else:
        dielectrons = make_detached_dielectron(
            electron_maker=electron_maker,
            opposite_sign=opposite_sign,
            dilepton_ID=parent_id,
            pid_e=pid_e_min,
            pt_e=pt_e_min,
            minipchi2=ipchi2_e_min,
            adocachi2cut=adocachi2cut_min,
            bpvvdchi2=bpvvdchi2_min,
            vfaspfchi2ndof=vfaspfchi2ndof_max,
        )

        code = F.require_all(F.PT > pt_diE_min, F.MASS > am_min, F.MASS < am_max)
        return ParticleFilter(dielectrons, F.FILTER(code), name=name)
