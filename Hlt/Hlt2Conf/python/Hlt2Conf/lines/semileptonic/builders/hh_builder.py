###############################################################################
# (c) Copyright 2020-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Semileptonic HH builder"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner


@configurable
def make_hh(
    particles,
    descriptors,
    name="HHCombiner_{hash}",
    m_min=1800 * MeV,
    m_max=5300 * MeV,
    pt_min=None,
    fdchi2_min=25.0,
    vtx_chi2pdof_max=9.0,
    bpvdira_min=0.999,
    bpv_ipchi2_min=None,
    comb_pt_any_min=None,
    comb_pt_sum_min=None,
):
    """
    Builder for a hadron--hadron pair.

    Parameters
    ----------
    particles :
        Maker algorithm instances for input particles.
    descriptor :
        a single decay descriptor (+ cc), not a list!
        Decay descriptor to be reconstructed.
        Example: descriptor = "rho(770)0 -> pi+ pi-"
    m_min :
        Lower invariant mass limit for the particle combination.
    m_max :
        Upper invariant mass limit for the particle combination.
    pt_min:
        Minimum PT for the final combined particle
    fdchi2_min:
        Minimum flight distance chi2 for the combined particle
    vtx_chi2pdof_max:
        Maximum vertex chi2 per DOF for combined particle
    bpvdira_min:
        Minimum DIRA to BPV for combined particle
    bpv_ipchi2_min:
        Minimum IPchi2 to best PV for combined particle
    comb_pt_any_min :
        Minimum pt that at least one of the particles in the combination needs to have.
    comb_pt_sum_min :
        Minimum value of the direct sum of the pt of the particles in the combination (pt_1 + pt_2 + ...).
    """

    combination_code = in_range(m_min, F.MASS, m_max)
    if comb_pt_any_min is not None:
        combination_code = F.require_all(
            combination_code, F.SUM(F.PT > comb_pt_any_min) > 0
        )
    if comb_pt_sum_min is not None:
        combination_code = F.require_all(
            combination_code, F.SUM(F.PT) > comb_pt_sum_min
        )

    vertex_code = F.require_all(F.OWNPVDIRA > bpvdira_min, F.CHI2DOF < vtx_chi2pdof_max)
    if pt_min is not None:
        vertex_code = F.require_all(vertex_code, F.PT > pt_min)
    if fdchi2_min is not None:
        vertex_code = F.require_all(vertex_code, F.OWNPVFDCHI2 > fdchi2_min)
    if bpv_ipchi2_min is not None:
        vertex_code = F.require_all(vertex_code, F.OWNPVIPCHI2 > bpv_ipchi2_min)

    return ParticleCombiner(
        particles,
        DecayDescriptor=descriptors,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
