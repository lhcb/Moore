###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from GaudiKernel.SystemOfUnits import MeV, mm
from RecoConf.algorithms_thor import ParticleContainersMerger

from .builders.b_builder import make_b2xulnu
from .builders.base_builder import (
    make_candidate,
    make_fake_tauons_muonic_decay,
    make_kaons_from_b,
    make_tauons_muonic_decay,
)


def make_bstoktaunu_muonic(process):
    """
    Selection for the decay Bs0 -> K tau(mu nu nu ) nu.
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )
    with make_candidate.bind(track_chisq_ndof_max=2.5, track_ghostprob_max=0.03):
        kaons = make_kaons_from_b(
            pid=F.require_all(
                F.PID_K > 10, F.PID_K - F.PID_P > 5, F.PID_K - F.PID_MU > 10
            ),
            p_min=12000 * MeV,
            pt_min=1200 * MeV,
            mipchi2_min=60.0,
        )

    taus = make_tauons_muonic_decay()

    comb_m_min = 1000 * MeV
    comb_m_max = 5000 * MeV
    mcorr_min = 2500 * MeV
    mcorr_max = 6500 * MeV
    vchi2pdof_max = 2.5
    bpvdira_min = 0.999
    bpvfdchi2_min = 180.0
    comb_doca_max = 0.07 * mm

    b_rs = make_b2xulnu(
        particles=[kaons, taus],
        descriptor="[B_s~0 -> K+ mu-]cc",
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        bpvdira_min=bpvdira_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvfdchi2_min=bpvfdchi2_min,
        mcorr_min=mcorr_min,
        mcorr_max=mcorr_max,
    )

    b_ws = make_b2xulnu(
        particles=[kaons, taus],
        descriptor="[B_s~0 -> K+ mu+]cc",
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        bpvdira_min=bpvdira_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvfdchi2_min=bpvfdchi2_min,
        mcorr_min=mcorr_min,
        mcorr_max=mcorr_max,
    )

    line_alg = ParticleContainersMerger([b_rs, b_ws])
    return line_alg


def make_bstoktaunu_muonic_fakemu(process):
    """
    Selection for the decay Bs0 -> K tau(mu nu nu) nu, with a fake muon.
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )
    with make_candidate.bind(track_chisq_ndof_max=2.5, track_ghostprob_max=0.03):
        kaons = make_kaons_from_b(
            pid=F.require_all(
                F.PID_K > 10, F.PID_K - F.PID_P > 5, F.PID_K - F.PID_MU > 10
            ),
            p_min=12000 * MeV,
            pt_min=1200 * MeV,
            mipchi2_min=60.0,
        )

    taus_nopid = make_fake_tauons_muonic_decay()

    comb_m_min = 1000 * MeV
    comb_m_max = 5000 * MeV
    mcorr_min = 2500 * MeV
    mcorr_max = 6500 * MeV
    vchi2pdof_max = 2.5
    bpvdira_min = 0.999
    bpvfdchi2_min = 180.0
    comb_doca_max = 0.07 * mm

    b_rs = make_b2xulnu(
        particles=[kaons, taus_nopid],
        descriptor="[B_s~0 -> K+ mu-]cc",
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        bpvdira_min=bpvdira_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvfdchi2_min=bpvfdchi2_min,
        mcorr_min=mcorr_min,
        mcorr_max=mcorr_max,
    )

    b_ws = make_b2xulnu(
        particles=[kaons, taus_nopid],
        descriptor="[B_s~0 -> K+ mu+]cc",
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        bpvdira_min=bpvdira_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvfdchi2_min=bpvfdchi2_min,
        mcorr_min=mcorr_min,
        mcorr_max=mcorr_max,
    )

    line_alg = ParticleContainersMerger([b_rs, b_ws])
    return line_alg


def make_bstoktaunu_muonic_fakek(process):
    """
    Selection for the decay Bs0 -> K tau(mu nu nu) , with a fake kaon.
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )
    with make_candidate.bind(track_chisq_ndof_max=2.5, track_ghostprob_max=0.03):
        kaons_nopid = make_kaons_from_b(
            pid=None, p_min=12000 * MeV, pt_min=1200 * MeV, mipchi2_min=60.0
        )

    taus = make_tauons_muonic_decay()

    comb_m_min = 1000 * MeV
    comb_m_max = 5000.0 * MeV
    mcorr_min = 2500 * MeV
    mcorr_max = 6500 * MeV
    vchi2pdof_max = 2.5
    bpvdira_min = 0.999
    bpvfdchi2_min = 180.0
    comb_doca_max = 0.07 * mm

    b_rs = make_b2xulnu(
        particles=[kaons_nopid, taus],
        descriptor="[B_s~0 -> K+ mu-]cc",
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        bpvdira_min=bpvdira_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvfdchi2_min=bpvfdchi2_min,
        mcorr_min=mcorr_min,
        mcorr_max=mcorr_max,
    )

    b_ws = make_b2xulnu(
        particles=[kaons_nopid, taus],
        descriptor="[B_s~0 -> K+ mu+]cc",
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        bpvdira_min=bpvdira_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvfdchi2_min=bpvfdchi2_min,
        mcorr_min=mcorr_min,
        mcorr_max=mcorr_max,
    )

    line_alg = ParticleContainersMerger([b_rs, b_ws])
    return line_alg
