# Developing on the semileptonics `SLB` branch

This document details the instructions for builder and line development on a SLB-specific branch and a description of the milestone-based workflow. Both line and builder development should occur on the `SLB` branch of Moore, following the instructions below.

## Motivation

Using a dedicated `SLB` branch makes for a unified environment for line and builder development. Keeping this target branch separate form `master` ensures that the MiCo can review the codebase before officially integrating it into the LHCb software stack. Furthermore, the gitlab web tools can be leveraged to organise the line migration within the semileptonics (SLB) WG, based on the physics investigated in each line. Following the workflow detailed below, a streamlined approach to documenting the progress and performance of each SLB line is accomplished.

You can find the most recent presentation to the WG outlining this workflow [here](https://indico.cern.ch/event/1031048/).

## Issues

Issues are a git-specific tasks that can be initialised and assigned through the gitlab web interface. Developers are required to initialise an issue when commencing the development of a line (or a group of closely-related lines, _e.g._ for a line and its `FakeMuon` counterpart). This communicates to the migration coordinators (MiCos) the intention to port and optimise a given set of selections in Moore.


## Streams

The selections committed to Moore within the semileptonics WG can be grouped into the following _streams_, based on the physics being investigated:

- $`b \rightarrow c`$ hadronic tau lines (includes baryons)
- $`b \rightarrow c`$ semimuonic tau lines
- $`b \rightarrow c`$ light leptonic lines
- $`B_c^+`$ lines
- $`b\rightarrow u`$ light leptonic lines or $`|V_{ub}|`$ lines
- Fully leptonic lines

Much like single lines are associated to issues, streams constitute collections of issues, or _milestones_, within gitlab. Each issue, when initialised, must be associated to the milestone named after the appropriate stream. You can find the list of milestones in the drop-down menu `Issues->Milestones` on the left handside of the gitlab web interface. Each milestone contains in its description a list of the stripping lines expected to make up its scope.

A naming convention has been established to suitably mark all milestones (and labels) relevant for the semileptonics (SLB) migration. Thus, all milestones (_i.e._ the streams above) are named starting with `SLB -`. Please make sure to assign only milestones starting with `SLB -`.


## Workflow

The migration workflow makes use of the web-based gitlab tools to organise the lines being developed and to collect the relevant information. This is amounts to:
- associating each **line** (and its `FakeMuon` counterpart) to an **issue**, i.e. a task either under development or complete.
- associating each **stream** to a **[milestone](https://docs.gitlab.com/ee/user/project/milestones/)**, i.e. a collection of [_issues_](https://docs.gitlab.com/ee/user/project/issues/);
The gitlab web interface allows users to open issues and assign milestones from the left panel of the `LHCb/Moore` web page.

The migration workflow follows three main steps:

### 1. Commencing development of a new line

A new line must be developed on a new branch. This will eventually have to merged to the `SLB` branch of Moore. This keeps `SLB` as a monolithic clean branch where only reviewed and approved code is committed.

In addition, a new issue must be opened via the gitlab web interface. This is done by clicking on the leftmost panel of the `LHCb/Moore` page on 'Issues' and then on the 'New Issue' button. The user will then be presented with an online form. **Please select the 'SLB New Line' option in the 'Description' drop-down menu.** Users are required to fill the form shown on the right handside of Figure 1.

Please complete the steps detailed in the relevant check-list:
1. Select a milestone with a name starting with `SLB -`. This is the stream to which the new line is associated (see above). You can find the complete list of milestones and the lines expected in each stream by clicking on the 'Milestones' menu on the leftmost panel on the git web interface. Each of the `SLB -` milestones contains in its description a list of lines that should be included in its scope.

2. Assign the label `SLB Line`.

The issue should be assigned to the line author(s).

<img src="assets/newline.png" alt="new line template" width="800"/>

Figure 1: instructions for a new line (left) and 'NewLine' issue template (right).

### 2. Development & feature requests (if needed)

Once the issue has been created, the development cycle should take place on the line-specific branch. Please consult the [Moore documentation](https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/index.html) to familiarise yourself with the tools available. To date, these include a wizard to produce efficiency plots and tupling functionality with truth info. A prototype of the topo trigger is available as a pre-filter (see the [twiki](https://twiki.cern.ch/twiki/bin/view/LHCbPhysics/SLUpgradeMigration) for more information). Make sure all the upgrade MC you need is available on the bookeeping. If not, you will need to request this by emailing the upgrade MC liaison.

Line development is expected to happen through the use of centrally-developed `builders`. The cut values of each `configurable` can be modified via the [Tonic](https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/pyconf/tonic.html?highlight=tonic#module-PyConf.tonic) syntax.

It is possible that the builders are not flexible enough to accommodate the set of selections required for a given line. As shown in Figure 2, in this case users should open a new issue and select the `SLB - Builder` template. Please fill the form and complete the check-list as instructed. The developers will take notice of the feature request and implement changes accordingly. You should expect to receive notice of any updates and liaise with the builder developers to make sure that the builders have been suitably modified to accommodate your needs.

<img src="assets/new_feature.png" alt="builder modification request" width="800"/>

Figure 2: instructions for a new feature request (left) and 'SLB New Feature' issue template (right).


### 3. Development complete: merge request (MR) and review

Once the development cycle has concluded, the new selections must be merged into the branch `SLB`. This is done through a merge request (MR), aimed at integrating the code developed on a line-specific branch into `SLB`.Please make sure to submit a merge request with `SLB` as a target and _not_ `master`. The migration coordinators will take care to integrate `SLB` into `LHCb/Moore/master` after having reviewed the code.

Merge requests should be initialised through the gitlab web interface. Users are required to fill the report form shown on the right handside of Figure 3 below. Please select the `SLB Merge Line` template to load the SLB-specific MR form. As illustrated, the line author(s) will be required to detail the efficiency values, rates and similar information resulting from the line development. This makes for a streamlined way to record the performance of each selection.

Users should assign the MiCos as reviewers. This ensures an additional layer of code-checking before integrating the new code into `SLB`. **Please ensure that the line(s) pass the CI pipeline before opening a MR.**

<img src="assets/MR.png" alt="merge request" width="800"/>

Figure 3: MR instructions (left) and MR report form (right).

## Using the TOPO as a prefilter

Work is ongoing to fine-tune the TOPO performance. A functional prototype (not too far-off the optimised implementation) can be used as a prefilter.

The following instructions have been copied from the slb [twiki](https://twiki.cern.ch/twiki/bin/view/LHCbPhysics/SLUpgradeMigration).

There is a prototype set of topological triggers from Greg. You can try these out directly or you can put them in front of your own selection if you are considering sprucing. The two muon topo triggers are the same as the regular ones, with the exception that a muon is required in the final state. This is probably best for the SL group. For all of these the BDT selection is yet to be optimised.

To run the topological triggers:
   * You need to clone the `apearce-topo-weights` branch of the `ParamFiles` package to a local location (the BDT weights are not in a release yet) - https://gitlab.cern.ch/lhcb-datapkg/ParamFiles/-/tree/apearce-topo-weights
   * Once you are inside the Moore environment (./Moore/run bash) you need to set to configure the Root property of the ParamFileSvc to point to wherever you checked out the ParamFiles package
With this set up you can run any of the topo triggers as you would another trigger. To insert a topo trigger into the control flow of your own selection:
   1. Import the trigger algorithm:
```python
from Hlt2Conf.lines.topological_b import twobody_mu_line
```
   2. declare it in your line builder:
```python
topo_line = twobody_mu_line()
```
   3. Add it to the algorithm control flow in front of your selection algorithm :
```python
return Hlt2Line(
    name=name,
    algs=upfront_reconstruction() + [topo_line.output_producer, lbs],
    prescale=prescale,
    extra_outputs=[])
```
See the `LbToPMuNu.py` file for an example.

## Git practices and tips

### Rebasing

Rebasing is a git practice whereby a given branch is brought up-to-date with its `master` as if it had been spawned from the latest version of `master`, as opposed to a previous commit. This allows a given user to integrate on its local branch any unrelated developmements committed to `master` since creating their branch. **This accomplishes a linear history in git**.

For a quick explanation of why rebasing _might_ be useful for you, look no further than [this stack overflow question](https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase). A more comprehensive explanation can be found [here](https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase).

Broadly, rebasing should _reduce the number of merge conflicts_, at the expense of needing to rebase frequently. Please consider whether rebasing might be preferable over merging for your line development.

### Running checks locally before pushing

It is good proctice to try to ensure that any code is is bug-free before committing. Users may check this by following two steps:

1. run `lb-format [pythonfile]` to enforce the correct code formatting;
2. run `flake8 --exclude '*.opts.py' --select=F,E71,E9,W1,W6 $(find Hlt -name '*.py')` in the `Moore` directory. This is the python linting check run by the pipeline.

Analysts should make sure to address any issues found before pushing. Failing to do so will result in an email detailing the failed checks. Users can inspect the mailed report to find out what exactly caused the pipeline to fail.

### CI

Users may way to commit code that is not ready for deployment. In this case, the CI pipeline does not need to be triggered. This may be accomplished by adding `[skip ci]` or `[ci skip]` anywhere in the commit message. This will result in a commit which will _not_ trigger the CI pipeline when pushing.

## FAQ

#### When following the [lb-stack setup](https://gitlab.cern.ch/rmatev/lb-stack-setup) instructions, why doesn't `make` work?

The instructions are pertinent to Moore only, therefore you should `make Moore` after `curl https://gitlab.cern.ch/rmatev/lb-stack-setup/raw/master/setup.py | python3 - stack`. There might be some further issue with your OS-specific build. In that case, the easiest way to find help is to ask on [mattermost](https://mattermost.web.cern.ch/lhcb/channels/upgrade-hlt2).

Last update: Blaise Delaney, 10/05/2021
