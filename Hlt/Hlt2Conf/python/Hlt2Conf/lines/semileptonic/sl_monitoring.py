###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re
from dataclasses import dataclass

import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV
from PyConf.dataflow import DataHandle
from RecoConf.algorithms_thor import ParticleFilter
from SelAlgorithms.monitoring import histogram_1d, monitor


@dataclass
class monitoring_variable:
    """SLB monitoring variable struct"""

    line_alg: DataHandle
    name: str
    title: str
    label: str
    functor: F.grammar.FunctorBase
    bins: int
    range: tuple


charm_mass_ranges = {
    "Dp": [100, (1820 * MeV, 1920 * MeV)],
    "D0": [170, (1780 * MeV, 1950 * MeV)],
    "Ds": [90, (1920 * MeV, 2010 * MeV)],
    "Dsst": [250, (2000 * MeV, 2250 * MeV)],
    "Lc": [70, (2250 * MeV, 2320 * MeV)],
    "Xic": [190, (2370 * MeV, 2560 * MeV)],
    "Omegac": [180, (2605 * MeV, 2785 * MeV)],
    "Jpsi": [100, (3050 * MeV, 3150 * MeV)],
    "DPi": [100, (1820 * MeV, 1920 * MeV)],
    "BsDs": [190, (1820 * MeV, 2210 * MeV)],  # wider mass window for when the Ds
    "BsJpsi": [250, (2850 * MeV, 3350 * MeV)],
}  # or Jpsi come from a BcToBs decay

B_mass_ranges = {
    "B": [200, (2 * GeV, 10 * GeV)],  # default
    "B0": [200, (2 * GeV, 10 * GeV)],
    "Bu": [300, (2 * GeV, 15 * GeV)],
    "Bs": [200, (2 * GeV, 10 * GeV)],
    "BcToBs": [400, (3 * GeV, 20 * GeV)],
    "BcToD0": [300, (3 * GeV, 15 * GeV)],
    "BcToJpsi": [300, (3 * GeV, 15 * GeV)],
    "Lb": [200, (2 * GeV, 10 * GeV)],
    "Xib": [300, (2 * GeV, 15 * GeV)],
    "Omegab": [300, (2 * GeV, 15 * GeV)],
    "B2Tau": [300, (2 * GeV, 15 * GeV)],
    "B2Mu": [300, (2 * GeV, 15 * GeV)],
    "B2E": [300, (2 * GeV, 15 * GeV)],
}


def makeMonVar(linename, MonVar):
    monitoring_hists = histogram_1d(
        name=f"/{linename}/{MonVar.name}",
        title=MonVar.title,
        label=MonVar.label,
        functor=MonVar.functor,
        bins=MonVar.bins,
        range=MonVar.range,
    )

    line_mon = monitor(
        name=f"Monitor_{linename}" + "_{hash}",
        data=MonVar.line_alg,
        histograms=[monitoring_hists],
    )
    return line_mon


def makeMonVars(linename, mon_var_array):
    mon_hists = []
    for MonVar in mon_var_array:
        mon_hists.append(makeMonVar(linename, MonVar))
    return mon_hists


def generate_default_monitoring_vars(
    linename, line_alg, *, charged: bool, charmed: bool
):
    B_cand_name = re.search(
        r"_(B0|Bu|Bs|BcToBs|BcToD0|BcToJpsi|Lb|Omegab|Xib|B2Mu|B2Tau|B2E)", linename
    )
    if B_cand_name:
        B_cand_name = B_cand_name.group()[1:]
        if re.search(r"(BcToBs|BcToD0|BcToJpsi)", B_cand_name):
            trim_B_cand_name = B_cand_name[:2]
        else:
            trim_B_cand_name = B_cand_name
    else:
        B_cand_name = "B"  # default binning and range if no match is found - works for all non-specified lines
        trim_B_cand_name = "B"
    default_monitoring = {
        f"{trim_B_cand_name}_Mass": [
            line_alg,
            f"{trim_B_cand_name}_CorrM",
            f"Corrected Mass of {trim_B_cand_name}",
            f"{trim_B_cand_name} Corrected Mass [MeV/#it{{c}}^{2}]",
            F.OWNPVCORRM(0.0),
            B_mass_ranges.get(B_cand_name)[0],
            B_mass_ranges.get(B_cand_name)[1],
        ],
    }

    if charged:
        if B_cand_name == "BcToBs":
            default_monitoring = {
                **default_monitoring,
                f"{trim_B_cand_name}_charge": [
                    line_alg,
                    f"{trim_B_cand_name}_charge",
                    f"Charge of {trim_B_cand_name}",
                    f"Charge of {trim_B_cand_name}",
                    F.CHARGE @ F.CHILD(2, F.FORWARDARGS),
                    5,
                    (-2, 2),
                ],
                f"{trim_B_cand_name}_plus_Mass": [
                    ParticleFilter(
                        line_alg,
                        F.FILTER((F.CHARGE @ F.CHILD(2, F.FORWARDARGS)) > 0),
                        name=f"{trim_B_cand_name}_plus_chargefilt_{linename}",
                    ),
                    f"{trim_B_cand_name}_plus_CorrM",
                    f"Corrected Mass of the positively charged {trim_B_cand_name}",
                    f"{trim_B_cand_name} Corrected Mass [MeV/#it{{c}}^{2}]",
                    F.OWNPVCORRM(0.0),
                    B_mass_ranges.get(B_cand_name)[0],
                    B_mass_ranges.get(B_cand_name)[1],
                ],
                f"{trim_B_cand_name}_minus_Mass": [
                    ParticleFilter(
                        line_alg,
                        F.FILTER((F.CHARGE @ F.CHILD(2, F.FORWARDARGS)) < 0),
                        name=f"{trim_B_cand_name}_minus_chargefilt_{linename}",
                    ),
                    f"{trim_B_cand_name}_minus_CorrM",
                    f"Corrected Mass of the negatively charged {trim_B_cand_name}",
                    f"{trim_B_cand_name} Corrected Mass [MeV/#it{{c}}^{2}]",
                    F.OWNPVCORRM(0.0),
                    B_mass_ranges.get(B_cand_name)[0],
                    B_mass_ranges.get(B_cand_name)[1],
                ],
            }
        else:
            default_monitoring = {
                **default_monitoring,
                f"{trim_B_cand_name}_charge": [
                    line_alg,
                    f"{trim_B_cand_name}_charge",
                    f"Charge of {trim_B_cand_name}",
                    f"Charge of {trim_B_cand_name}",
                    F.CHARGE,
                    5,
                    (-2, 2),
                ],
                f"{trim_B_cand_name}_plus_Mass": [
                    ParticleFilter(
                        line_alg,
                        F.FILTER(F.CHARGE > 0),
                        name=f"{trim_B_cand_name}_plus_chargefilt_{linename}",
                    ),
                    f"{trim_B_cand_name}_plus_CorrM",
                    f"Corrected Mass of the positively charged {trim_B_cand_name}",
                    f"{trim_B_cand_name} Corrected Mass [MeV/#it{{c}}^{2}]",
                    F.OWNPVCORRM(0.0),
                    B_mass_ranges.get(B_cand_name)[0],
                    B_mass_ranges.get(B_cand_name)[1],
                ],
                f"{trim_B_cand_name}_minus_Mass": [
                    ParticleFilter(
                        line_alg,
                        F.FILTER(F.CHARGE < 0),
                        name=f"{trim_B_cand_name}_minus_chargefilt_{linename}",
                    ),
                    f"{trim_B_cand_name}_minus_CorrM",
                    f"Corrected Mass of the negatively charged {trim_B_cand_name}",
                    f"{trim_B_cand_name} Corrected Mass [MeV/#it{{c}}^{2}]",
                    F.OWNPVCORRM(0.0),
                    B_mass_ranges.get(B_cand_name)[0],
                    B_mass_ranges.get(B_cand_name)[1],
                ],
            }
    if charmed:
        charm_cand_name = re.search(
            r"To(Dsst|Dp|D0|Ds(?!st)|Jpsi|Lc|Xic|Omegac|DPi)", linename
        )

        if charm_cand_name:
            charm_cand_name = charm_cand_name.group()[2:]
        else:
            charm_cand_name = "D0"  # default binning and range

        default_monitoring = {
            **default_monitoring,
            f"{charm_cand_name}_Mass": [
                line_alg,
                f"{charm_cand_name}_M",
                f"Mass of {charm_cand_name}",
                "Mass [MeV/#it{c}^{2}]",
                F.CHILD(1, F.MASS),
                charm_mass_ranges.get(charm_cand_name)[0],
                charm_mass_ranges.get(charm_cand_name)[1],
            ],
        }

    if (
        B_cand_name == "BcToBs"
    ):  # catch the specific lines which have Bc -> Bs decays, and make monitoring plots for the Bs as well as the charm daughter
        B_cand_name = "Bs"
        default_monitoring = {
            **default_monitoring,
            f"{B_cand_name}_Mass": [
                line_alg,
                f"{B_cand_name}_CorrM",
                f"Corrected Mass of {B_cand_name}",
                f"{B_cand_name} Corrected Mass [MeV/#it{{c}}^{2}]",
                F.OWNPVCORRM(0.0) @ F.CHILD(1, F.FORWARDARGS),
                B_mass_ranges.get(B_cand_name)[0],
                B_mass_ranges.get(B_cand_name)[1],
            ],
        }
        charm_cand_name = re.search(r"To(Ds|Jpsi)", linename)

        if charm_cand_name:
            charm_cand_name = f"{charm_cand_name.group()[2:]}"

            default_monitoring = {
                **default_monitoring,
                f"{charm_cand_name}_Mass": [
                    line_alg,
                    f"{charm_cand_name}_M",
                    f"Mass of {charm_cand_name}",
                    "Mass [MeV/#it{c}^{2}]",
                    F.MASS @ F.CHILD(1, F.FORWARDARGS) @ F.CHILD(1, F.FORWARDARGS),
                    charm_mass_ranges.get(charm_cand_name)[0],
                    charm_mass_ranges.get(charm_cand_name)[1],
                ],
            }

    monvar = [monitoring_variable(*value) for value in default_monitoring.values()]
    line_mon = makeMonVars(linename, monvar)
    return line_mon
