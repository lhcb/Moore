###############################################################################
# (C) Copyright 2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
This module provides the mapping of the line names between the old and new
naming conventions.
"""

line_name_mapping = {
    "SpruceSLB_BuToD0TauNu_D0ToKPiPiPi_TauToPiPiPiNu": "SpruceSLB_BuToD0TauNu_D0ToK3Pi_TauToPiPiPiNu",
    "SpruceSLB_BcToJpsiTauNu_JpsiToMuMu_TauToMuNuNu_FakeMuon": "SpruceSLB_BcToJpsiTauNu_JpsiToMuMu_FakeMuon",
    "SpruceSLB_BcToJpsiTauNu_JpsiToMuMu_TauToENuNu_FakeElectron": "SpruceSLB_BcToJpsiTauNu_JpsiToMuMu_FakeElectron",
    "SpruceSLB_B0ToDpTauNu_DpToKPiPi_TauToMuNuNu_FakeMuon": "SpruceSLB_B0ToDpTauNu_DpToKPiPi_FakeMuon",
    "SpruceSLB_B0ToDpTauNu_DpToKPiPi_TauToENuNu_FakeElectron": "SpruceSLB_B0ToDpTauNu_DpToKPiPi_FakeElectron",
    "SpruceSLB_BuToD0TauNu_D0ToKPi_TauToMuNuNu_FakeMuon": "SpruceSLB_BuToD0TauNu_D0ToKPi_FakeMuon",
    "SpruceSLB_BuToD0TauNu_D0ToKPi_TauToENuNu_FakeElectron": "SpruceSLB_BuToD0TauNu_D0ToKPi_FakeElectron",
    "SpruceSLB_BuToD0TauNu_D0ToKPiPiPi_TauToMuNuNu": "SpruceSLB_BuToD0TauNu_D0ToK3Pi_TauToMuNuNu",
    "SpruceSLB_BuToD0TauNu_D0ToKPiPiPi_TauToMuNuNu_FakeMuon": "SpruceSLB_BuToD0TauNu_D0ToK3Pi_FakeMuon",
    "SpruceSLB_BuToD0TauNu_D0ToKPiPiPi_TauToENuNu": "SpruceSLB_BuToD0TauNu_D0ToK3Pi_TauToENuNu",
    "SpruceSLB_BuToD0TauNu_D0ToKPiPiPi_TauToENuNu_FakeElectron": "SpruceSLB_BuToD0TauNu_D0ToK3Pi_FakeElectron",
    "SpruceSLB_BsToDsTauNu_DsToKKPi_TauToMuNuNu_FakeMuon": "SpruceSLB_BsToDsTauNu_DsToKKPi_FakeMuon",
    "SpruceSLB_BsToDsTauNu_DsToKKPi_TauToENuNu_FakeElectron": "SpruceSLB_BsToDsTauNu_DsToKKPi_FakeElectron",
    "SpruceSLB_LbToLcTauNu_LcToPKPi_TauToMuNuNu_FakeMuon": "SpruceSLB_LbToLcTauNu_LcToPKPi_FakeMuon",
    "SpruceSLB_LbToLcTauNu_LcToPKPi_TauToENuNu_FakeElectron": "SpruceSLB_LbToLcTauNu_LcToPKPi_FakeElectron",
    "SpruceSLB_BuToD0MuNu_D0ToKPiPiPi": "SpruceSLB_BuToD0MuNu_D0ToK3Pi",
    "SpruceSLB_BuToD0MuNu_D0ToKPiPiPi_FakeMuon": "SpruceSLB_BuToD0MuNu_D0ToK3Pi_FakeMuon",
    "SpruceSLB_BuToD0ENu_D0ToKPiPiPi": "SpruceSLB_BuToD0ENu_D0ToK3Pi",
    "SpruceSLB_BuToD0ENu_D0ToKPiPiPi_FakeElectron": "SpruceSLB_BuToD0ENu_D0ToK3Pi_FakeElectron",
    "SpruceSLB_Xib0ToXicplusMuNu_XicplusToPKPi": "SpruceSLB_Xib0ToXicplusMuNu_XicplusTopKPi",
    "SpruceSLB_Xib0ToXicplusTauNu_XicplusToPKPi_TauToMuNuNu": "SpruceSLB_Xib0ToXicplusTauNu_XicplusTopKPi_TauToMuNuNu",
    "SpruceSLB_Xib0ToXicplusMuNu_XicplusToPKPi_FakeMuon": "SpruceSLB_Xib0ToXicplusMuNu_XicplusTopKPi_FakeMuon",
    "SpruceSLB_Xib0ToXicplusTauNu_XicplusToPKPi_FakeMuon": "SpruceSLB_Xib0ToXicplusTauNu_XicplusTopKPi_FakeMuon",
    "SpruceSLB_XibminusToXic0MuNu_Xic0ToPKKPi": "SpruceSLB_XibminusToXic0MuNu_Xic0TopKKPi",
    "SpruceSLB_XibminusToXic0TauNu_Xic0ToPKKPi_TauToMuNuNu": "SpruceSLB_XibminusToXic0TauNu_Xic0TopKKPi_TauToMuNuNu",
    "SpruceSLB_XibminusToXic0MuNu_Xic0ToPKKPi_FakeMuon": "SpruceSLB_XibminusToXic0MuNu_Xic0TopKKPi_FakeMuon",
    "SpruceSLB_XibminusToXic0TauNu_Xic0ToPKKPi_FakeMuon": "SpruceSLB_XibminusToXic0TauNu_Xic0TopKKPi_FakeMuon",
    "SpruceSLB_LbToLcTauNu_LcToPKPi_TauToPiPiPiNu": "SpruceSLB_LbToLcTauNu_LcTopKPi_TauToPiPiPiNu",
    "SpruceSLB_B0ToPiMuNu": "SpruceSLB_B2XuMuNuB02Pi",
    "SpruceSLB_B0ToPiMuNu_NoPIDMu": "SpruceSLB_B2XuMuNuB02pi_NoPIDMu",
    "SpruceSLB_B0ToPiMuNu_NoPIDPi": "SpruceSLB_B2XuMuNuB02pi_NoPIDPi",
    "SpruceSLB_B0ToPiTauNu_TauToPiPiPiNu": "SpruceSLB_B2XuTauNu_HadronicB02Pi",
    "SpruceSLB_BsToKMuNu": "SpruceSLB_B2XuMuNuBs2K",
    "SpruceSLB_BsToKMuNu_NoPIDMu": "SpruceSLB_B2XuMuNuBs2K_NoPIDMu",
    "SpruceSLB_BsToKMuNu_NoPIDK": "SpruceSLB_B2XuMuNuBs2K_NoPIDK",
    "SpruceSLB_BsToKTauNu_TauToPiPiPiNu": "SpruceSLB_B2XuTauNu_HadronicBs2K",
    "SpruceSLB_BsToKTauNu_TauToMuNuNu": "SpruceSLB_B2XuTauNu_MuonicBs2K",
    "SpruceSLB_BsToKTauNu_TauToMuNuNu_NoPIDMu": "SpruceSLB_B2XuTauNu_MuonicBs2K_NoPIDMu",
    "SpruceSLB_BsToKTauNu_TauToMuNuNu_NoPIDK": "SpruceSLB_B2XuTauNu_MuonicBs2K_NoPIDK",
    "SpruceSLB_BuToPPbarMuNu": "SpruceSLB_B2PPbarMuNu",
    "SpruceSLB_BuToPPbarMuNu_SS": "SpruceSLB_B2PPbarMuNu_SS",
    "SpruceSLB_BuToPPbarMuNu_FakeP": "SpruceSLB_B2PPbarMuNu_FakeP",
    "SpruceSLB_BuToPPbarMuNu_FakeMu": "SpruceSLB_B2PPbarMuNu_FakeMu",
    "SpruceSLB_BuToPiPiMuNu": "SpruceSLB_B2PiPiMuNu",
    "SpruceSLB_BuToPiPiMuNu_SS": "SpruceSLB_B2PiPiMuNu_SS",
    "SpruceSLB_BuToPiPiMuNu_FakeMu": "SpruceSLB_B2PiPiMuNu_FakeMu",
    "SpruceSLB_BuToKKMuNu": "SpruceSLB_B2KKMuNu",
    "SpruceSLB_BuToKKMuNu_SS": "SpruceSLB_B2KKMuNu_SS",
    "SpruceSLB_BuToKKMuNu_FakeMu": "SpruceSLB_B2KKMuNu_FakeMu",
    "SpruceSLB_BuToMuMuMuNu": "SpruceSLB_B2MuMuMuNu",
    "SpruceSLB_BuToEMuMuNu": "SpruceSLB_B2EMuMuNu",
    "SpruceSLB_BuToMuEENu": "SpruceSLB_B2MuEENu",
    "SpruceSLB_BsToDsstMuNu_DsstToDsGamma_DsToKKPi_GammaToEE": "SpruceSLB_BsToDsstMuNu_DsstToDsGamma_DsToKKPi_Gamma2EE",
    "SpruceSLB_BuToMuGammaNu_GammaToConvertedEELL": "SpruceSLB_B2MuGammaNu_CNVLL",
    "SpruceSLB_BuToMuGammaNu_GammaToConvertedEEDD": "SpruceSLB_B2MuGammaNu_CNVDD",
    "SpruceSLB_BuToEGammaNu_GammaToConvertedEELL": "SpruceSLB_B2EGammaNu_CNVLL",
    "SpruceSLB_BuToEGammaNu_GammaToConvertedEEDD": "SpruceSLB_B2EGammaNu_CNVDD",
    "SpruceSLB_BuToEEENu": "SpruceSLB_B2EEENu",
    "SpruceSLB_BuToTauMuMuNu_TauToPiPiPiNu": "SpruceSLB_B2TauMuMuNu_3Pi",
    "SpruceSLB_BuToTauEENu_TauToPiPiPiNu": "SpruceSLB_B2TauEENu_3Pi",
    "SpruceSLB_BuToMuMuMuNu_SS": "SpruceSLB_B2MuMuMuNu_SS",
    "SpruceSLB_BuToEMuMuNu_SS": "SpruceSLB_B2EMuMuNu_SS",
    "SpruceSLB_BuToMuEENu_SS": "SpruceSLB_B2MuEENu_SS",
    "SpruceSLB_BuToEEENu_SS": "SpruceSLB_B2EEENu_SS",
    "SpruceSLB_BuToTauMuMuNu_TauToPiPiPiNu_SS": "SpruceSLB_B2TauMuMuNu_3Pi_SS",
    "SpruceSLB_BuToTauEENu_TauToPiPiPiNu_SS": "SpruceSLB_B2TauEENu_3Pi_SS",
    "SpruceSLB_LbToLcTauNu_LcToPKSLL_TauToMuNuNu": "SpruceSLB_LbToLcTauNu_LcToPKSLL_TautoMuNuNu",
    "SpruceSLB_LbToLcTauNu_LcToPKSDD_TauToMuNuNu": "SpruceSLB_LbToLcTauNu_LcToPKSDD_TautoMuNuNu",
    "SpruceSLB_LbToLcTauNu_LcToPKSLL_TauToENuNu": "SpruceSLB_LbToLcTauNu_LcToPKSLL_TautoENuNu",
    "SpruceSLB_LbToLcTauNu_LcToPKSDD_TauToENuNu": "SpruceSLB_LbToLcTauNu_LcToPKSDD_TautoENuNu",
    "SpruceSLB_LbToLcTauNu_LcToLambdaPiLL_TauToMuNuNu": "SpruceSLB_LbToLcTauNu_LcToLambdaPiLL_TautoMuNuNu",
    "SpruceSLB_LbToLcTauNu_LcToLambdaPiDD_TauToMuNuNu": "SpruceSLB_LbToLcTauNu_LcToLambdaPiDD_TautoMuNuNu",
    "SpruceSLB_LbToLcTauNu_LcToLambdaPiLL_TauToENuNu": "SpruceSLB_LbToLcTauNu_LcToLambdaPiLL_TautoENuNu",
    "SpruceSLB_LbToLcTauNu_LcToLambdaPiDD_TauToENuNu": "SpruceSLB_LbToLcTauNu_LcToLambdaPiDD_TautoENuNu",
    "SpruceSLB_BuToTauNu_TauToPiPiPiNu_BTracking": "SpruceSLB_BToTauNu_TauToPiPiPiNu_BTracking",
    "SpruceSLB_BuToDPiPi_DToKPiPi_BTracking": "SpruceSLB_BToDPiPi_DToKPiPi_BTracking",
    "SpruceSLB_LbToLcTauNu_LcToPPhi_TauToPiPiPiNu": "SpruceSLB_LbToLcTauNu_LcTopPhi_TauToPiPiPiNu",
    "SpruceSLB_XibminusToXic0TauNu_Xic0ToPKKPi_TauToPiPiPiNu": "SpruceSLB_XibminusToXic0TauNu_Xic0TopKKPi_TauToPiPiPiNu",
}


def map_line_name(line_name, legacy_name=False):
    """
    Maps the line name to the new line name or legacy name.

    Parameters:
    line_name (str): The line name.
    legacy_name (bool): If True, map to the legacy name. Default is False.

    Returns:
    str: The mapped line name with "Spruce" replaced by "Hlt2".
    """

    if legacy_name:
        line_name = line_name_mapping.get(line_name, "UnknownLineName")

    return line_name.replace("Spruce", "Hlt2")
