###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of builders specific for the B->3 leptons neutrino lines.
This includes the following:

Physics lines:

Hlt2SLB_B2MuMuMuNu
Hlt2SLB_B2EMuMuNu
Hlt2SLB_B2MuEENu
Hlt2SLB_B2MuNuGamma_CNVLL
Hlt2SLB_B2MuNuGamma_CNVDD
Hlt2SLB_B2EEENu
Hlt2SLB_B2TauMuMuNu_3Pi
Hlt2SLB_B2TauEENu_3Pi

Same-sign leptons lines:

Hlt2SLB_B2MuMuMuNu_SS
Hlt2SLB_B2EMuMuNu_SS
Hlt2SLB_B2MuEENu_SS
Hlt2SLB_B2EEENu_SS
Hlt2SLB_B2TauMuMuNu_3Pi_SS
Hlt2SLB_B2TauEENu_3Pi_SS

The cuts in the leptonic lines are loose enough to comprise also leptonic tau decays
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV

from .builders.b_builder import make_b2lllnu
from .builders.base_builder import (
    make_electrons_from_b,
    make_muons_from_b,
    make_tauons_hadronic_decay,
)
from .builders.dilepton_builder import (
    make_detached_dielectron_for_b2lllnu,
    make_detached_dimuon_for_b2lllnu,
    make_gammaDD_for_b2lllnu,
    make_gammaLL_for_b2lllnu,
)

all_lines = {}

## Physics lines first ##


def make_b2mumumunu(name, process, prescale=1):
    """
    SL line for the B(c)+ -> mu+mu-mu+nu decays
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    muons = make_muons_from_b(
        pt_min=300.0 * MeV, p_min=3.0 * GeV, mipchi2_min=9.0, pid=(F.PID_MU > -4.0)
    )

    bps = make_b2lllnu(
        particles=[muons, muons, muons],
        descriptor="[B+ -> mu+ mu+ mu-]cc",
        name=name + "Combiner",
    )

    return bps


def make_b2emumunu(name, process, prescale=1):
    """
    SL line for the B(c)+ -> e+mu-mu+nu decays
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    electrons = make_electrons_from_b(
        pt_min=500.0 * MeV,
        p_min=0.0 * GeV,
        mipchi2_min=25.0,
        pid=(F.PID_E > 0.0),
        require_in_calo=1,
    )
    dimuons = make_detached_dimuon_for_b2lllnu()

    bps = make_b2lllnu(
        particles=[dimuons, electrons],
        descriptor="[B+ -> J/psi(1S) e+]cc",
        name=name + "Combiner",
    )

    return bps


def make_b2mueenu(name, process, prescale=1):
    """
    SL line for the B(c)+ -> mu+e-e+nu decays
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    muons = make_muons_from_b(
        pt_min=300.0 * MeV, p_min=3.0 * GeV, mipchi2_min=9.0, pid=(F.PID_MU > -4.0)
    )
    dielectrons = make_detached_dielectron_for_b2lllnu()

    bps = make_b2lllnu(
        particles=[dielectrons, muons],
        descriptor="[B+ -> J/psi(1S) mu+]cc",
        name=name + "Combiner",
    )

    return bps


def make_b2mugammanu_cnvll(name, process, prescale=1):
    """
    SL line for the B(c)+ -> mu+gamma(->e+e-)nu decays with long electrons
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    muons = make_muons_from_b(
        pt_min=300.0 * MeV, p_min=3.0 * GeV, mipchi2_min=9.0, pid=(F.PID_MU > -4.0)
    )
    dielectrons = make_gammaLL_for_b2lllnu()

    bps = make_b2lllnu(
        particles=[dielectrons, muons],
        descriptor="[B+ -> gamma mu+]cc",
        name=name + "Combiner",
    )

    return bps


def make_b2mugammanu_cnvdd(name, process, prescale=1):
    """
    SL line for the B(c)+ -> mu+gamma(->e+e-)nu decays with downstream electrons
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    muons = make_muons_from_b(
        pt_min=300.0 * MeV, p_min=3.0 * GeV, mipchi2_min=9.0, pid=(F.PID_MU > -4.0)
    )
    dielectrons = make_gammaDD_for_b2lllnu()

    bps = make_b2lllnu(
        particles=[dielectrons, muons],
        descriptor="[B+ -> gamma mu+]cc",
        name=name + "Combiner",
    )

    return bps


def make_b2eeenu(name, process, prescale=1):
    """
    SL line for the B(c)+ -> e+e-e+nu decays
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    electrons = make_electrons_from_b(
        pt_min=350.0 * MeV,
        p_min=0.0 * GeV,
        mipchi2_min=25.0,
        pid=(F.PID_E > 0.0),
        require_in_calo=1,
    )

    comb_cut = F.require_all(
        F.require_any(
            F.SUBCOMB(Functor=F.MASS < 1500.0 * MeV, Indices=(1, 3)),
            F.SUBCOMB(Functor=F.MASS < 1500.0 * MeV, Indices=(2, 3)),
        ),
        F.MAX(F.PT) > 500.0 * MeV,
        F.MAX(F.PID_E) > 2.0,
    )

    bps = make_b2lllnu(
        particles=[electrons, electrons, electrons],
        descriptor="[B+ -> e+ e+ e-]cc",
        name=name + "Combiner",
        additional_comb_cut=comb_cut,
    )

    return bps


def make_b2egammanu_cnvll(name, process, prescale=1):
    """
    SL line for the B(c)+ -> e+gamma(->e+e-)nu decays with long conversion electrons
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    electrons = make_electrons_from_b(
        pt_min=800.0 * MeV,
        p_min=3.0 * GeV,
        mipchi2_min=25.0,
        pid=(F.PID_E > 0.0),
        require_in_calo=1,
    )
    dielectrons = make_gammaLL_for_b2lllnu()

    bps = make_b2lllnu(
        particles=[dielectrons, electrons],
        descriptor="[B+ -> gamma e+]cc",
        name=name + "Combiner",
    )

    return bps


def make_b2egammanu_cnvdd(name, process, prescale=1):
    """
    SL line for the B(c)+ -> mu+gamma(->e+e-)nu decays with downstream conversion electrons
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    electrons = make_electrons_from_b(
        pt_min=800.0 * MeV,
        p_min=3.0 * GeV,
        mipchi2_min=25.0,
        pid=(F.PID_E > 0.0),
        require_in_calo=1,
    )
    dielectrons = make_gammaDD_for_b2lllnu()

    bps = make_b2lllnu(
        particles=[dielectrons, electrons],
        descriptor="[B+ -> gamma e+]cc",
        name=name + "Combiner",
    )

    return bps


def make_b2taumumunu_3pi(name, process, prescale=1):
    """
    SL line for the B(c)+ -> tau+mu-mu+nu decays with hadronic tau->3pi
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    tauons = make_tauons_hadronic_decay()
    dimuons = make_detached_dimuon_for_b2lllnu()

    bps = make_b2lllnu(
        particles=[dimuons, tauons],
        descriptor="[B+ -> J/psi(1S) tau+]cc",
        name=name + "Combiner",
    )

    return bps


def make_b2taueenu_3pi(name, process, prescale=1):
    """
    SL line for the B(c)+ -> tau+e-e+nu decays with hadronic tau->3pi
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    tauons = make_tauons_hadronic_decay()
    dielectrons = make_detached_dielectron_for_b2lllnu(am_max=1500.0 * MeV)

    bps = make_b2lllnu(
        particles=[dielectrons, tauons],
        descriptor="[B+ -> J/psi(1S) tau+]cc",
        name=name + "Combiner",
    )

    return bps


## Unphysical lines: same-sign ##


def make_b2mumumunu_ss(name, process, prescale=1):
    """
    SL line for the B(c)+ -> mu+mu+mu+nu decays: same-sign combinations
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    muons = make_muons_from_b(
        pt_min=300.0 * MeV, p_min=3.0 * GeV, mipchi2_min=9.0, pid=(F.PID_MU > -4.0)
    )

    bps = make_b2lllnu(
        particles=[muons, muons, muons],
        descriptor="[B+ -> mu+ mu+ mu+]cc",
        name=name + "Combiner",
    )

    return bps


def make_b2emumunu_ss(name, process, prescale=1):
    """
    SL line for the B(c)+ -> e+mu+mu+nu decays: same-sign combinations
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    electrons = make_electrons_from_b(
        pt_min=500.0 * MeV,
        p_min=0.0 * GeV,
        mipchi2_min=25.0,
        pid=(F.PID_E > 0.0),
        require_in_calo=1,
    )
    dimuons = make_detached_dimuon_for_b2lllnu(same_sign=True)

    bps = make_b2lllnu(
        particles=[dimuons, electrons],
        descriptor="[B+ -> J/psi(1S) e+]cc",
        name=name + "Combiner",
    )

    return bps


def make_b2mueenu_ss(name, process, prescale=1):
    """
    SL line for the B(c)+ -> mu+e+e+nu decays: same-sign combinations
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    muons = make_muons_from_b(
        pt_min=300.0 * MeV, p_min=3.0 * GeV, mipchi2_min=9.0, pid=(F.PID_MU > -4.0)
    )
    dielectrons = make_detached_dielectron_for_b2lllnu(opposite_sign=False)

    bps = make_b2lllnu(
        particles=[dielectrons, muons],
        descriptor="[B+ -> J/psi(1S) mu+]cc",
        name=name + "Combiner",
    )

    return bps


def make_b2eeenu_ss(name, process, prescale=1):
    """
    SL line for the B(c)+ -> e+e+e+nu decays: same-sign combinations
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    electrons = make_electrons_from_b(
        pt_min=350.0 * MeV,
        p_min=0.0 * GeV,
        mipchi2_min=25.0,
        pid=(F.PID_E > 0.0),
        require_in_calo=1,
    )

    comb_cut = F.require_all(
        F.MAX(F.PT) > 500.0 * MeV,
        F.MAX(F.PID_E) > 2.0,
    )

    bps = make_b2lllnu(
        particles=[electrons, electrons, electrons],
        descriptor="[B+ -> e+ e+ e+]cc",
        name=name + "Combiner",
        additional_comb_cut=comb_cut,
    )

    return bps


def make_b2taumumunu_3pi_ss(name, process, prescale=1):
    """
    SL line for the B(c)+ -> tau+mu+mu+nu decays with hadronic tau->3pi: same-sign combinations
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    tauons = make_tauons_hadronic_decay()
    dimuons = make_detached_dimuon_for_b2lllnu(same_sign=True)

    bps = make_b2lllnu(
        particles=[dimuons, tauons],
        descriptor="[B+ -> J/psi(1S) tau+]cc",
        name=name + "Combiner",
    )

    return bps


def make_b2taueenu_3pi_ss(name, process, prescale=1):
    """
    SL line for the B(c)+ -> tau+e+e+nu decays with hadronic tau->3pi: same-sign combinations
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    tauons = make_tauons_hadronic_decay()
    dielectrons = make_detached_dielectron_for_b2lllnu(
        am_max=1500.0 * MeV, opposite_sign=False
    )

    bps = make_b2lllnu(
        particles=[dielectrons, tauons],
        descriptor="[B+ -> J/psi(1S) tau+]cc",
        name=name + "Combiner",
    )

    return bps
