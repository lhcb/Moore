###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import Functors as F
from GaudiKernel.SystemOfUnits import MeV
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import make_long_pions

from Hlt2Conf.isolation import extra_outputs_for_isolation


def get_pv_tracks_for_bstar(
    line_name: str,
    line_output: ParticleCombiner,
    pt_min: float = 400,
    bpvipchi2_max: float = 9,
    pperp_max: float = 350,
    pid=None,
):
    """
    Select additional long tracks consistent with the BPV of a B candidate, that could be combined to make B* (Bs2*, Sigma_b, etc) candidates of various flavors.
    Args:
        line_name : string with name of the B candidate line
        line_output : ParticleCombiner object creating the B candidates:
        pt_min : minimum pT in MeV for extra tracks
        bpvipchi2_max : max OWNPVIPCHI2 for extra tracks
        pperp_max : max momentum perpendicular to B direction for extra tracks
        pid : a PID functor cut to apply to extra tracks

    Returns:
        list that can be passed to extra_outputs of a line for persistence
    """

    all_tracks = make_long_pions()
    pvs = make_pvs()

    if pid is not None:
        pv_tracks = ParticleFilter(
            all_tracks,
            F.FILTER(
                F.require_all(F.PT > pt_min * MeV, F.OWNPVIPCHI2 < bpvipchi2_max, pid)
            ),
        )
    else:
        pv_tracks = ParticleFilter(
            all_tracks,
            F.FILTER(F.require_all(F.PT > pt_min * MeV, F.OWNPVIPCHI2 < bpvipchi2_max)),
        )

    B_child = F.FORWARDARG0
    PV_trk = F.FORWARDARG1
    PV_trk_mom = F.THREEMOMENTUM @ PV_trk
    B_dir = F.BPVFDIR(pvs) @ B_child
    ppar = F.DOT.bind(PV_trk_mom, B_dir)
    pperp_vec = PV_trk_mom - ppar * B_dir

    cut = F.require_all(F.MAGNITUDE @ pperp_vec < pperp_max * MeV, F.SHARE_BPV(pvs))
    return extra_outputs_for_isolation(
        line_name + "_extra_PV_tracks", line_output, pv_tracks, cut
    )
