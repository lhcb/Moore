###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from GaudiKernel.SystemOfUnits import MeV, mm
from PyConf import ConfigurationError
from RecoConf.algorithms_thor import ParticleContainersMerger

from .builders.b_builder import make_b2xclnu, make_bs2hh
from .builders.base_builder import (
    make_electrons_from_b,
    make_kaons_from_b,
    make_kaons_from_phi,
    make_muons_from_b,
    make_pions_from_b,
)
from .builders.charm_hadron_builder import (
    make_ds_tokkpi,
    make_jpsi_tomumu,
)
from .builders.hh_builder import make_hh

_JPSI_M = 3096.90 * MeV
_PHI_M = 1019.455 * MeV


def make_bctobsx(process, BcDecay, BsDecay):
    """
    Lines for the SL decays and control channels Bc->BsX decays

    The Bc decay must be specified as BcToBsMuNu, BcToBsENu, BcToBsPi
    or BcToBsK, to match the following decays:

    Bc+ -> Bs mu+ nu_mu
    Bc+ -> Bs e+ nu_e
    Bc+ -> Bs pi+
    Bc+ -> Bs K+

    The Bs decay must be specified as BsToDsPi, BsToDsK, BsToJpsiPhi,
    to match the following decays:

    Bs -> Ds- pi+
    Bs -> Ds- K+
    Bs -> J/psi phi

    Bs -> K+K-
    Bs -> pi+ pi-
    Bs -> K+ pi- #note, auto merged cp conjugate from Bs WS and RS configs
    """
    if BcDecay == "BcToBsMuNu":
        bach = make_muons_from_b(
            p_min=1000 * MeV, pt_min=150 * MeV, mipchi2_min=0, mip_min=0.02 * mm
        )
        descriptor_rs = "[B_c+ -> B_s0 mu+]cc"
        descriptor_ws = "[B_c+ -> B_s0 mu-]cc"
    elif BcDecay == "BcToBsENu":
        bach = make_electrons_from_b(
            p_min=3000 * MeV,
            pt_min=1000 * MeV,
            mipchi2_min=0,
            mip_min=0.05,
            pid=(F.PID_E > -0.2),
        )
        descriptor_rs = "[B_c+ -> B_s0 e+]cc"
        descriptor_ws = "[B_c+ -> B_s0 e-]cc"
    elif BcDecay == "BcToBsPi":
        bach = make_pions_from_b(
            p_min=1000 * MeV,
            pt_min=350 * MeV,
            mipchi2_min=0,
            mip_min=0.02,
            pid=(F.PID_K < 2.0),
        )
        descriptor_rs = "[B_c+ -> B_s0 pi+]cc"
        descriptor_ws = "[B_c+ -> B_s0 pi-]cc"
    elif BcDecay == "BcToBsK":
        bach = make_kaons_from_b(
            p_min=1000 * MeV,
            pt_min=350 * MeV,
            mipchi2_min=0,
            mip_min=0.05,
            pid=(F.PID_K > 0.0),
        )
        descriptor_rs = "[B_c+ -> B_s0 K+]cc"
        descriptor_ws = "[B_c+ -> B_s0 K-]cc"
    else:
        raise ConfigurationError(
            "BcDecay must be BcToBsMuNu, BcToBsENu, BcToBsPi or BcToBsK"
        )

    if BsDecay == "BsToDsPi":
        # Make Ds and pi candidates
        dss = make_ds_tokkpi(
            comb_m_min=1568.34,
            comb_m_max=2368.34,
            comb_pt_any_min=300 * MeV,
            comb_pt_sum_min=1500 * MeV,
            daughter_p_min=3000 * MeV,
            daughter_pt_min=400 * MeV,
            daughter_mipchi2_min=16,
            vchi2pdof_max=8,
            bpvdira_min=0.99,
            bpvfdchi2_min=25,
            bpvvdz_min=0 * mm,
            kaon_pid=(F.PID_K > 5.0),
            pion_pid=(F.PID_K < -2.0),
        )
        pis_from_bs = make_pions_from_b(
            pid=(F.PID_K < 2),
            p_min=1000 * MeV,
            pt_min=500 * MeV,
            mipchi2_min=16,
            mip_min=0.055 * mm,
        )
        # Make Bs candidates
        bss = make_bs2hh(
            [dss, pis_from_bs], "[B_s0 -> D_s- pi+ ]cc", name="BsToDsPiMaker"
        )
    elif BsDecay == "BsToDsK":
        # Make Ds and pi candidates
        dss = make_ds_tokkpi(
            comb_m_min=1568.34,
            comb_m_max=2368.34,
            comb_pt_any_min=300 * MeV,
            comb_pt_sum_min=1500 * MeV,
            daughter_p_min=3000 * MeV,
            daughter_pt_min=400 * MeV,
            daughter_mipchi2_min=16,
            vchi2pdof_max=8,
            bpvdira_min=0.99,
            bpvfdchi2_min=25,
            bpvvdz_min=0 * mm,
            kaon_pid=(F.PID_K > 5.0),
            pion_pid=(F.PID_K < -2.0),
        )
        ks_from_bs = make_kaons_from_b(
            pid=(F.PID_K > 0.0),
            p_min=1000 * MeV,
            pt_min=500 * MeV,
            mipchi2_min=16,
            mip_min=0.055 * mm,
        )
        # Make Bs candidates
        bss = make_bs2hh([dss, ks_from_bs], "[B_s0 -> D_s- K+ ]cc", name="BsToDsKMaker")
    elif BsDecay == "BsToJpsiPhi":
        jpsi = make_jpsi_tomumu(
            comb_m_min=_JPSI_M - 200 * MeV,
            comb_m_max=_JPSI_M + 200 * MeV,
            comb_pt_any_min=250 * MeV,
            comb_pt_sum_min=500 * MeV,
            comb_docachi2_max=None,
            mother_pt_min=1000 * MeV,
            vchi2pdof_max=10,
            bpvfdchi2_min=25,
            bpvdira_min=0.9,
            bpvvdz_min=1.5,
        )
        kaons = make_kaons_from_phi()
        particles = [kaons, kaons]
        descriptor = "phi(1020) -> K+ K-"
        phi = make_hh(
            particles,
            descriptor,
            name="PhiToKKMaker",
            m_min=_PHI_M - 200 * MeV,
            m_max=_PHI_M + 200 * MeV,
            fdchi2_min=None,
            vtx_chi2pdof_max=20,
            bpvdira_min=0.9,
        )
        bss = make_bs2hh(
            [jpsi, phi], "B_s0 -> J/psi(1S) phi(1020)", name="BsToJpsiPhiMaker"
        )
    elif BsDecay == "BsToKK":
        kaons = make_kaons_from_b()
        particles = [kaons, kaons]
        descriptor = "B_s0 -> K- K+"
        bss = make_bs2hh(particles=particles, descriptor=descriptor, name="BsToKKMaker")
    elif BsDecay == "BsToPiPi":
        pions = make_pions_from_b()
        particles = [pions, pions]
        descriptor = "B_s0 -> pi- pi+"
        bss = make_bs2hh(
            particles=particles, descriptor=descriptor, name="BsToPiPiMaker"
        )
    elif BsDecay == "BsToKPi":
        kaons = make_kaons_from_b()
        pions = make_pions_from_b()
        descriptor = "[B_s0 -> K+ pi-]cc"
        bss = make_bs2hh(
            particles=[kaons, pions], descriptor=descriptor, name="BsToKPiMaker"
        )
    else:
        raise ConfigurationError(
            "BsDecay must be BsToDsPi, BsToDsK, BsToJpsiPhi, BsToKK, BsToPiPi, BsToKPi"
        )

    # Make Bc candidates
    bcs_rs = make_b2xclnu(
        [bss, bach],
        descriptor_rs,
        name=f"{BcDecay}_{BsDecay}_rs_combiner",
        pt_min=3000 * MeV,
        p_min=30_000 * MeV,
    )
    bcs_ws = make_b2xclnu(
        [bss, bach],
        descriptor_ws,
        name=f"{BcDecay}_{BsDecay}_ws_combiner",
        pt_min=3000 * MeV,
        p_min=30_000 * MeV,
    )
    linealg = ParticleContainersMerger([bcs_rs, bcs_ws])

    return linealg
