###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from PyConf import ConfigurationError
from RecoConf.algorithms_thor import ParticleContainersMerger

from .builders.b_builder import make_b2xclnu
from .builders.base_builder import (
    make_electrons_from_b,
    make_fake_electrons_from_b_reversedPID,
    make_fake_muons_from_b_notIsMuon,
    make_fake_muons_from_b_reversedPID,
    make_muons_from_b,
)
from .builders.charm_hadron_builder import (
    make_d0_tok3pi,
    make_d0_tokpi,
    make_dplus_tokkpi,
    make_dplus_tokpipi,
    make_ds_tokkpi,
    make_dst_to_dsgamma,
    make_Hc_to_nbody,
    make_jpsi_tomumu,
    make_lambdac_tolambda0pi,
    make_lambdac_topkpi,
    make_lambdac_topks,
    make_omegac_topkkpi,
    make_xic0_topkkpi,
    make_xicplus_topkpi,
)
from .builders.dilepton_builder import make_detached_dielectron_for_b2dlllnu


def make_bctojpsilnu_jpsitomumu(process, lepton):
    """
    Selection for the decay Bc+ -> Jpsi(-> mu mu) l nu.
    """
    if lepton == "mu":
        leptons = make_muons_from_b()
        descriptor = "[B_c- -> J/psi(1S) mu-]cc"
    elif lepton == "e":
        leptons = make_electrons_from_b()
        descriptor = "[B_c- -> J/psi(1S) e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    # 120 MeV around Jpsi mass
    jpsi = make_jpsi_tomumu(
        comb_m_min=2977 * MeV, comb_m_max=3217 * MeV, vchi2pdof_max=4
    )
    bc_jpsimum = make_b2xclnu(
        [jpsi, leptons],
        descriptor=descriptor,
        name=f"Bc2Jpsi{lepton}Nu_Jpsi2MuMu_combiner",
    )
    return bc_jpsimum


def make_bctojpsilnu_jpsitomumu_fakelepton(process, lepton):
    """
    Selection for the decay Bc+ -> Jpsi(-> mu mu) l nu, with a fake lepton.
    """
    if lepton == "mu":
        fake_leptons = make_fake_muons_from_b_notIsMuon()
        descriptor = "[B_c- -> J/psi(1S) mu-]cc"
    elif lepton == "e":
        fake_leptons = make_fake_electrons_from_b_reversedPID()
        descriptor = "[B_c- -> J/psi(1S) e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    jpsi = make_jpsi_tomumu(
        comb_m_min=2977 * MeV,
        comb_m_max=3217 * MeV,
        comb_pt_any_min=None,
        comb_pt_sum_min=None,
        comb_docachi2_max=None,
        vchi2pdof_max=25,
        bpvfdchi2_min=9,
        bpvdira_min=0,
    )
    bc_jpsimum = make_b2xclnu(
        [jpsi, fake_leptons],
        descriptor=descriptor,
        name=f"Bc2Jpsi{lepton}Nu_Jpsi2MuMu_fakeL_combiner",
    )
    return bc_jpsimum


def make_butod0lnu_d0tokpi(process, lepton):
    """
    Selection for the decay B(c)+ -> D0(-> K pi) l nu.
    """
    if lepton == "mu":
        leptons = make_muons_from_b()
        descriptor_rs = "[B- -> D0 mu-]cc"
        descriptor_ws = "[B+ -> D0 mu+]cc"
    elif lepton == "e":
        leptons = make_electrons_from_b(
            p_min=3.0 * GeV,
            pt_min=300.0 * MeV,
            pid=(F.PID_E > 2.0),
            mipchi2_min=15.0,
            eta_cut=F.require_all(F.ETA > 2.2, F.ETA < 4.2),
        )
        descriptor_rs = "[B- -> D0 e-]cc"
        descriptor_ws = "[B+ -> D0 e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    cuts = {}
    if lepton == "e":
        cuts = {
            "mother_p_min": 15 * GeV,
            "comb_docachi2_max": 5.0,
            "vchi2pdof_max": 5.0,
            "bpvdira_min": 0.9998,
            "daughter_pt_min": 300 * MeV,
            "daughter_p_min": 5 * GeV,
            "daughter_mipchi2_min": 45,
            "kaon_pid": F.require_all(F.PID_K > 4.0, F.GHOSTPROB < 0.35),
            "pion_pid": F.require_all(F.PID_K < 0.0, F.GHOSTPROB < 0.35),
        }
    else:
        cuts = {"mother_p_min": 5 * GeV}

    dzs = make_d0_tokpi(**cuts)

    B_extra_cuts = {}
    if lepton == "e":
        B_extra_cuts = {
            "comb_m_min": 1500 * MeV,
            "comb_m_max": 10000 * MeV,
            "comb_docachi2_max": None,
            "mother_m_min": 1500 * MeV,
            "mother_m_max": 10000 * MeV,
            "comb_doca_max": 0.3 * mm,
            "bpvfdchi2_min": 20,
            "bpvdira_min": 0.9995,
            "b_d_dzs_min": -5 * mm,
        }

    bus_dzmum = make_b2xclnu(
        [dzs, leptons],
        descriptor=descriptor_rs,
        **B_extra_cuts,
        name=f"Bu2D0{lepton}Nu_D02KPi_combiner",
    )
    bus_dzmup = make_b2xclnu(
        [dzs, leptons],
        descriptor=descriptor_ws,
        **B_extra_cuts,
        name=f"Bu2D0{lepton}Nu_D02KPi_WS_combiner",
    )
    line_alg = ParticleContainersMerger([bus_dzmum, bus_dzmup])

    return line_alg


def make_butod0lnu_d0tokpi_fakelepton(process, lepton):
    """
    Selection for the decay B(c)+ -> D0(-> K pi) l nu, with a fake lepton.
    """
    if lepton == "mu":
        fake_leptons = make_fake_muons_from_b_reversedPID()
        descriptor_rs = "[B- -> D0 mu-]cc"
        descriptor_ws = "[B+ -> D0 mu+]cc"
    elif lepton == "e":
        fake_leptons = make_fake_electrons_from_b_reversedPID(
            p_min=3.0 * GeV,
            pt_min=300.0 * MeV,
            pid=(F.PID_E < 2.0),
            mipchi2_min=15.0,
            eta_cut=F.require_all(F.ETA > 2.2, F.ETA < 4.2),
        )
        descriptor_rs = "[B- -> D0 e-]cc"
        descriptor_ws = "[B+ -> D0 e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    cuts = {}
    if lepton == "e":
        cuts = {
            "mother_p_min": 15 * GeV,
            "comb_docachi2_max": 5.0,
            "vchi2pdof_max": 5.0,
            "bpvdira_min": 0.9998,
            "daughter_pt_min": 300 * MeV,
            "daughter_p_min": 5 * GeV,
            "daughter_mipchi2_min": 45,
            "kaon_pid": F.require_all(F.PID_K > 4.0, F.GHOSTPROB < 0.35),
            "pion_pid": F.require_all(F.PID_K < 0.0, F.GHOSTPROB < 0.35),
        }
    else:
        cuts = {"mother_p_min": 5 * GeV}

    dzs = make_d0_tokpi(**cuts)

    B_extra_cuts = {}
    if lepton == "e":
        B_extra_cuts = {
            "comb_m_min": 1500 * MeV,
            "comb_m_max": 10000 * MeV,
            "comb_docachi2_max": None,
            "mother_m_min": 1500 * MeV,
            "mother_m_max": 10000 * MeV,
            "comb_doca_max": 0.3 * mm,
            "bpvfdchi2_min": 20,
            "bpvdira_min": 0.9995,
            "b_d_dzs_min": -5 * mm,
        }

    bus_dzmum = make_b2xclnu(
        [dzs, fake_leptons],
        descriptor=descriptor_rs,
        **B_extra_cuts,
        name=f"Bu2D0{lepton}Nu_D02KPi_fakeL_combiner",
    )
    bus_dzmup = make_b2xclnu(
        [dzs, fake_leptons],
        descriptor=descriptor_ws,
        **B_extra_cuts,
        name=f"Bu2D0{lepton}Nu_D02KPi_fakeL_WS_combiner",
    )
    line_alg = ParticleContainersMerger([bus_dzmum, bus_dzmup])

    return line_alg


def make_b0todplnu_dptokpipi(process, lepton):
    """
    Selction for the decay B0 -> D+(-> K pi pi) l nu.
    """
    if lepton == "mu":
        leptons = make_muons_from_b()
        descriptor_rs = "[B0 -> D- mu+]cc"
        descriptor_ws = "[B0 -> D- mu-]cc"
    elif lepton == "e":
        leptons = make_electrons_from_b()
        descriptor_rs = "[B0 -> D- e+]cc"
        descriptor_ws = "[B0 -> D- e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    dps = make_dplus_tokpipi()
    b0s_rightsign = make_b2xclnu(
        [dps, leptons],
        descriptor=descriptor_rs,
        name=f"B02Dp{lepton}Nu_Dp2KPiPi_combiner",
    )
    b0s_wrongsign = make_b2xclnu(
        [dps, leptons],
        descriptor=descriptor_ws,
        name=f"B02Dp{lepton}Nu_Dp2KPiPi_WS_combiner",
    )
    line_alg = ParticleContainersMerger([b0s_rightsign, b0s_wrongsign])

    return line_alg


def make_b0todplnu_dptokpipi_fakelepton(process, lepton):
    """
    Selection for the decay B0 -> D+(-> K pi pi) l nu, with a fake lepton.
    """
    if lepton == "mu":
        fake_leptons = make_fake_muons_from_b_reversedPID()
        descriptor_rs = "[B0 -> D- mu+]cc"
        descriptor_ws = "[B0 -> D- mu-]cc"
    elif lepton == "e":
        fake_leptons = make_fake_electrons_from_b_reversedPID()
        descriptor_rs = "[B0 -> D- e+]cc"
        descriptor_ws = "[B0 -> D- e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    dps = make_dplus_tokpipi()
    b0s_rightsign = make_b2xclnu(
        [dps, fake_leptons],
        descriptor=descriptor_rs,
        name=f"B02Dp{lepton}Nu_Dp2KPiPi_fakeL_combiner",
    )
    b0s_wrongsign = make_b2xclnu(
        [dps, fake_leptons],
        descriptor=descriptor_ws,
        name=f"B02Dp{lepton}Nu_Dp2KPiPi_fakeL_WS_combiner",
    )
    line_alg = ParticleContainersMerger([b0s_rightsign, b0s_wrongsign])

    return line_alg


def make_b0todplnu_dptokkpi(process, lepton):
    """
    Selction for the Cabibbo-suppressed decay B0 -> D+(-> K K pi) l nu.
    """
    if lepton == "mu":
        leptons = make_muons_from_b()
        descriptor_rs = "[B0 -> D- mu+]cc"
        descriptor_ws = "[B0 -> D- mu-]cc"
    elif lepton == "e":
        leptons = make_electrons_from_b()
        descriptor_rs = "[B0 -> D- e+]cc"
        descriptor_ws = "[B0 -> D- e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    dps = make_dplus_tokkpi()
    b0s_rightsign = make_b2xclnu(
        [dps, leptons],
        descriptor=descriptor_rs,
        name=f"B02Dp{lepton}Nu_Dp2KKPi_combiner",
    )
    b0s_wrongsign = make_b2xclnu(
        [dps, leptons],
        descriptor=descriptor_ws,
        name=f"B02Dp{lepton}Nu_Dp2KKPi_WS_combiner",
    )
    line_alg = ParticleContainersMerger([b0s_rightsign, b0s_wrongsign])

    return line_alg


def make_b0todplnu_dptokkpi_fakelepton(process, lepton):
    """
    Selection for the Cabibbo-suppressed decay B0 -> D+(-> K K pi) l nu, with a fake lepton.
    """
    if lepton == "mu":
        fake_leptons = make_fake_muons_from_b_reversedPID()
        descriptor_rs = "[B0 -> D- mu+]cc"
        descriptor_ws = "[B0 -> D- mu-]cc"
    elif lepton == "e":
        fake_leptons = make_fake_electrons_from_b_reversedPID()
        descriptor_rs = "[B0 -> D- e+]cc"
        descriptor_ws = "[B0 -> D- e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    dps = make_dplus_tokkpi()
    b0s_rightsign = make_b2xclnu(
        [dps, fake_leptons],
        descriptor=descriptor_rs,
        name=f"B02Dp{lepton}Nu_Dp2KKPi_fakeL_combiner",
    )
    b0s_wrongsign = make_b2xclnu(
        [dps, fake_leptons],
        descriptor=descriptor_ws,
        name=f"B02Dp{lepton}Nu_Dp2KKPi_fakeL_WS_combiner",
    )
    line_alg = ParticleContainersMerger([b0s_rightsign, b0s_wrongsign])

    return line_alg


def make_butod0lnu_d0tok3pi(process, lepton):
    """
    Selction for the decay B+ -> D0(-> K pi pi pi) l nu.
    """
    if lepton == "mu":
        leptons = make_muons_from_b()
        descriptor_rs = "[B+ -> D0 mu+]cc"
        descriptor_ws = "[B+ -> D0 mu-]cc"
    elif lepton == "e":
        leptons = make_electrons_from_b(
            p_min=3.0 * GeV,
            pt_min=300.0 * MeV,
            pid=(F.PID_E > 2.0),
            mipchi2_min=15.0,
            eta_cut=F.require_all(F.ETA > 2.2, F.ETA < 4.2),
        )
        descriptor_rs = "[B+ -> D0 e+]cc"
        descriptor_ws = "[B+ -> D0 e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    cuts = {}
    if lepton == "e":
        cuts = {
            "mother_p_min": 15 * GeV,
            "comb_docachi2_max": 5.0,
            "daughter_pt_min": 750 * MeV,
            "vchi2pdof_max": 5.0,
            "bpvdira_min": 0.9998,
            "kaon_pid": (F.PID_K > 4.0),
            "pion_pid": (F.PID_K < 2.0),
        }  # Tighter D0 cut
    else:
        cuts = {"mother_p_min": 5 * GeV}

    dzs = make_d0_tok3pi(**cuts)
    bps_rightsign = make_b2xclnu(
        [dzs, leptons],
        descriptor=descriptor_rs,
        name=f"B2D0{lepton}Nu_D02K3pi_combiner",
    )
    bps_wrongsign = make_b2xclnu(
        [dzs, leptons],
        descriptor=descriptor_ws,
        name=f"B2D0{lepton}Nu_D02K3pi_WS_combiner",
    )
    line_alg = ParticleContainersMerger([bps_rightsign, bps_wrongsign])

    return line_alg


def make_butod0lnu_d0tok3pi_fakelepton(process, lepton):
    """
    Selection for the decay B+ -> D0(-> K pi pi pi) l nu, with a fake lepton.
    """
    if lepton == "mu":
        fake_leptons = make_fake_muons_from_b_reversedPID()
        descriptor_rs = "[B+ -> D0 mu+]cc"
        descriptor_ws = "[B+ -> D0 mu-]cc"
    elif lepton == "e":
        fake_leptons = make_fake_electrons_from_b_reversedPID(
            p_min=3.0 * GeV,
            pt_min=300.0 * MeV,
            pid=(F.PID_E < 2.0),
            mipchi2_min=15.0,
            eta_cut=F.require_all(F.ETA > 2.2, F.ETA < 4.2),
        )
        descriptor_rs = "[B+ -> D0 e+]cc"
        descriptor_ws = "[B+ -> D0 e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    cuts = {}
    if lepton == "e":
        cuts = {
            "mother_p_min": 15 * GeV,
            "comb_docachi2_max": 5.0,
            "vchi2pdof_max": 5.0,
            "bpvdira_min": 0.9998,
            "daughter_pt_min": 750 * MeV,
            "kaon_pid": (F.PID_K > 4.0),
            "pion_pid": (F.PID_K < 2.0),
        }  # Tighter D0 cut
    else:
        cuts = {"mother_p_min": 5 * GeV}

    dzs = make_d0_tok3pi(**cuts)
    bps_rightsign = make_b2xclnu(
        [dzs, fake_leptons],
        descriptor=descriptor_rs,
        name=f"B2D0{lepton}Nu_D02K3pi_fakeL_combiner",
    )
    bps_wrongsign = make_b2xclnu(
        [dzs, fake_leptons],
        descriptor=descriptor_ws,
        name=f"B2D0{lepton}Nu_D02K3pi_fakeL_WS_combiner",
    )
    line_alg = ParticleContainersMerger([bps_rightsign, bps_wrongsign])

    return line_alg


def make_bstodslnu_dstokkpi(process, lepton):
    """
    Selection for the decay B_s0 -> Ds(-> K K pi) l nu.
    """
    if lepton == "mu":
        leptons = make_muons_from_b()
        descriptor_rs = "[B_s0 -> D_s- mu+]cc"
        descriptor_ws = "[B_s0 -> D_s- mu-]cc"
    elif lepton == "e":
        leptons = make_electrons_from_b()
        descriptor_rs = "[B_s0 -> D_s- e+]cc"
        descriptor_ws = "[B_s0 -> D_s- e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    dss = make_ds_tokkpi()
    bss_dsmup = make_b2xclnu(
        [dss, leptons],
        descriptor=descriptor_rs,
        name=f"Bs2Ds{lepton}Nu_Ds2KKPi_combiner",
    )
    bss_dsmum = make_b2xclnu(
        [dss, leptons],
        descriptor=descriptor_ws,
        name=f"Bs2Ds{lepton}Nu_Ds2KKPi_WS_combiner",
    )
    line_alg = ParticleContainersMerger([bss_dsmum, bss_dsmup])

    return line_alg


def make_bstodstlnu_dsttodsgamma_dstokkpi_gammatoee(process, lepton):
    """
    Selection for the decay B_s0 -> D*s(-> Ds(-> K K pi) gamma (-> ee) ) l nu.
    """
    if lepton == "mu":
        leptons = make_muons_from_b()
        descriptor_rs = "[B_s0 -> D*_s- mu+]cc"
        descriptor_ws = "[B_s0 -> D*_s- mu-]cc"
    else:
        raise ConfigurationError("Lepton must be mu")
    dss = make_ds_tokkpi()
    gamma = make_detached_dielectron_for_b2dlllnu(
        parent_id="gamma",
        am_min=0,
        am_max=100,
        pid_e_min=0,
        ipchi2_e_min=0,
        with_brem=False,
        with_upstream_and_downstream=True,
    )

    dsst = make_dst_to_dsgamma(
        dss,
        gamma,
        descriptor="[D*_s+ -> D_s+ gamma]cc",
        name="DsstToDsGamma_DsToKKPi_Gamma2EE_combiner",
    )
    bsst_dsmup = make_b2xclnu(
        [dsst, leptons],
        descriptor=descriptor_rs,
        name=f"BsToDsst{lepton}Nu_DsstToDsGamma_DsToKKPi_Gamma2EE_combiner",
    )
    bsst_dsmum = make_b2xclnu(
        [dsst, leptons],
        descriptor=descriptor_ws,
        name=f"BsToDsst{lepton}Nu_DsstToDsGamma_DsToKKPi_Gamma2EE_WS_combiner",
    )
    line_alg = ParticleContainersMerger([bsst_dsmum, bsst_dsmup])

    return line_alg


def make_bstodslnu_dstokkpi_fakelepton(process, lepton):
    """
    Selection for the decay B_s0 -> Ds(-> K K pi) l nu, with a fake lepton.
    """
    if lepton == "mu":
        fake_leptons = make_fake_muons_from_b_reversedPID()
        descriptor_rs = "[B_s0 -> D_s- mu-]cc"
        descriptor_ws = "[B_s0 -> D_s- mu+]cc"
    elif lepton == "e":
        fake_leptons = make_fake_electrons_from_b_reversedPID()
        descriptor_rs = "[B_s0 -> D_s- e-]cc"
        descriptor_ws = "[B_s0 -> D_s- e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    dss = make_ds_tokkpi()
    bss_dsmum = make_b2xclnu(
        [dss, fake_leptons],
        descriptor=descriptor_rs,
        name=f"Bs2Ds{lepton}Nu_Ds2KKPi_fakeL_combiner",
    )
    bss_dsmup = make_b2xclnu(
        [dss, fake_leptons],
        descriptor=descriptor_ws,
        name=f"Bs2Ds{lepton}Nu_Ds2KKPi_fakeL_WS_combiner",
    )
    line_alg = ParticleContainersMerger([bss_dsmum, bss_dsmup])

    return line_alg


def make_lbtolclnu_lctopkpi(process, lepton):
    """
    Selection for the decay [Lb0 -> Lc+(-> p K pi) l nu]cc.
    """

    if lepton == "mu":
        leptons = make_muons_from_b()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ mu-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ mu+]cc"
    elif lepton == "e":
        leptons = make_electrons_from_b()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ e-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    with make_Hc_to_nbody.bind(vchi2pdof_max=6.0):
        lcs = make_lambdac_topkpi()

    lb_rightsign = make_b2xclnu(
        particles=[lcs, leptons],
        descriptor=descriptor_rs,
        name=f"Lb2Lc{lepton}Nu_Lc2pKPi_combiner",
    )

    lb_wrongsign = make_b2xclnu(
        particles=[lcs, leptons],
        descriptor=descriptor_ws,
        name=f"Lb2Lc{lepton}Nu_Lc2pKPi_WS_combiner",
    )

    line_alg = ParticleContainersMerger([lb_rightsign, lb_wrongsign])

    return line_alg


def make_lbtolclnu_lctopkpi_fakelepton(process, lepton):
    """
    Selection for the decay [Lb0 -> Lc+(-> p K pi) l nu]cc, with a fake lepton.
    """

    if lepton == "mu":
        leptons = make_fake_muons_from_b_notIsMuon()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ mu-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ mu+]cc"
    elif lepton == "e":
        leptons = make_fake_electrons_from_b_reversedPID()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ e-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    with make_Hc_to_nbody.bind(vchi2pdof_max=6.0):
        lcs = make_lambdac_topkpi()

    lb_rightsign = make_b2xclnu(
        particles=[lcs, leptons],
        descriptor=descriptor_rs,
        name=f"Lb2Lc{lepton}Nu_Lc2pKPi_fakeL_combiner",
    )

    lb_wrongsign = make_b2xclnu(
        particles=[lcs, leptons],
        descriptor=descriptor_ws,
        name=f"Lb2Lc{lepton}Nu_Lc2pKPi_fakeL_WS_combiner",
    )

    line_alg = ParticleContainersMerger([lb_rightsign, lb_wrongsign])

    return line_alg


def make_lbtolclnu_lctoV0h(process, V0_name, V0_type, lepton_name):
    """
    Selection for the decay [Lb0 -> Lc+ (-> V0 hadron) tau- (-> l nu nu) nu]cc and combinatorial (same sign) with a fake lepton.
    Here the 'V0' is either a 'KS0' or a 'Lambda0'.
    When 'V0'='KS0', the 'hadron' is a 'pi+. When 'V0'='Lambda0', the 'hadron' is a 'proton'.

    Args:
        process: The process to which the selection belongs to. Must be 'Hlt2' or 'Spruce' or 'Turbo'.
        V0_name: The name of the V0 particle. Must be either 'Lambda0' or 'KS'.
        V0_type: The type of the V0 particle. Must be either 'LL' or 'DD'.
        lepton_name: The name of the lepton. Must be either 'mu' or 'e'.
    """

    Lc_mass = 2286.46  # units MeV
    delta_Lc_comb = 100.0  # units MeV, Delta combination
    delta_Lc_mother = 80.0  # units MeV, Delta composite

    if V0_name == "Lambda0":
        lc_maker = make_lambdac_tolambda0pi
    elif V0_name == "KS0":
        lc_maker = make_lambdac_topks
    else:
        raise ConfigurationError(
            f"V0_name must be either 'Lambda0' or 'KS0'. Got '{V0_name}' instead. Please check."
        )

    if lepton_name == "mu":
        leptons = make_muons_from_b()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ mu-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ mu+]cc"
    elif lepton_name == "e":
        # apply slightly tighter cuts to default
        leptons = make_electrons_from_b(
            p_min=7.5 * GeV, pt_min=2.0 * GeV, mipchi2_min=12.0, pid=(F.PID_E > 2.0)
        )
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ e-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ e+]cc"
    else:
        raise ConfigurationError(
            f"Lepton must be either 'mu' or 'e'. Got '{lepton_name}' instead. Please check."
        )

    with make_Hc_to_nbody.bind(vchi2pdof_max=6.0, bpvfdchi2_min=None, bpvdira_min=None):
        lcs = lc_maker(
            V0_type,
            mother_m_min=(Lc_mass - delta_Lc_mother) * MeV,
            mother_m_max=(Lc_mass + delta_Lc_mother) * MeV,
            comb_m_min=(Lc_mass - delta_Lc_comb) * MeV,
            comb_m_max=(Lc_mass + delta_Lc_comb) * MeV,
        )

    lb_rightsign = make_b2xclnu(particles=[lcs, leptons], descriptor=descriptor_rs)

    lb_wrongsign = make_b2xclnu(particles=[lcs, leptons], descriptor=descriptor_ws)

    line_alg = ParticleContainersMerger([lb_rightsign, lb_wrongsign])

    return line_alg


def make_lbtolclnu_lctoV0h_fakelepton(process, V0_name, V0_type, lepton_name):
    """
    Selection for the decay [Lb0 -> Lc+ (-> V0 hadron) tau- (-> l nu nu) nu]cc and combinatorial (same sign) with a fake lepton.
    Here the 'V0' is either a 'KS0' or a 'Lambda0'.
    When 'V0'='KS0', the 'hadron' is a 'pi+. When 'V0'='Lambda0', the 'hadron' is a 'proton'.

    Args:
        process: The process to which the selection belongs to. Must be 'Hlt2' or 'Spruce' or 'Turbo'.
        V0_name: The name of the V0 particle. Must be either 'Lambda0' or 'KS'.
        V0_type: The type of the V0 particle. Must be either 'LL' or 'DD'.
        lepton_name: The name of the lepton. Must be either 'mu' or 'e'.
    """

    Lc_mass = 2286.46  # units MeV
    delta_Lc_comb = 100.0  # units MeV, Delta combination
    delta_Lc_mother = 80.0  # units MeV, Delta composite

    if V0_name == "Lambda0":
        lc_maker = make_lambdac_tolambda0pi
    elif V0_name == "KS0":
        lc_maker = make_lambdac_topks
    else:
        raise ConfigurationError(
            f"V0_name must be either 'Lambda0' or 'KS0'. Got '{V0_name}' instead. Please check."
        )

    if lepton_name == "mu":
        leptons = make_fake_muons_from_b_notIsMuon()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ mu-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ mu+]cc"
    elif lepton_name == "e":
        leptons = make_fake_electrons_from_b_reversedPID(
            p_min=7.5 * GeV, pt_min=2.0 * GeV, mipchi2_min=12.0, pid=(F.PID_E < 2.0)
        )
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ e-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ e+]cc"
    else:
        raise ConfigurationError(
            f"Lepton must be either 'mu' or 'e'. Got '{lepton_name}' instead. Please check."
        )

    with make_Hc_to_nbody.bind(vchi2pdof_max=6.0, bpvfdchi2_min=None, bpvdira_min=None):
        lcs = lc_maker(
            V0_type,
            mother_m_min=(Lc_mass - delta_Lc_mother) * MeV,
            mother_m_max=(Lc_mass + delta_Lc_mother) * MeV,
            comb_m_min=(Lc_mass - delta_Lc_comb) * MeV,
            comb_m_max=(Lc_mass + delta_Lc_comb) * MeV,
        )

    lb_rightsign = make_b2xclnu(particles=[lcs, leptons], descriptor=descriptor_rs)

    lb_wrongsign = make_b2xclnu(particles=[lcs, leptons], descriptor=descriptor_ws)

    line_alg = ParticleContainersMerger([lb_rightsign, lb_wrongsign])

    return line_alg


def make_xib0toxicplusmunu_xicplustopkpi(process):
    """
    SL line for the decays:
        Xi_b0 -> Xi_c+ (-> p K- pi+) mu- nu (Right sign)
        Xi_b0 -> Xi_c+ (-> p K- pi+) mu+ nu (Wrong sign)
    """
    # make light lepton candidates
    leptons = make_muons_from_b()
    descriptor_rs = "[Xi_b0 -> Xi_c+ mu-]cc"
    descriptor_ws = "[Xi_b0 -> Xi_c+ mu+]cc"

    # make Xi_c+ candidates
    xicplus = make_xicplus_topkpi()

    # make Xib0 candidates (RS and WS)
    xibs_rs = make_b2xclnu(
        name="Xib0ToXicplusMuBuilder_RS",
        descriptor=descriptor_rs,
        particles=[xicplus, leptons],
    )
    xibs_ws = make_b2xclnu(
        name="Xib0ToXicplusMuBuilder_WS",
        descriptor=descriptor_ws,
        particles=[xicplus, leptons],
    )
    return ParticleContainersMerger([xibs_rs, xibs_ws])


def make_xib0toxicplusmunu_xicplustopkpi_fakelepton(process):
    """
    SL line for the decays with fake muon:
        Xi_b0 -> Xi_c+ (-> p K- pi+) mu- nu (Right sign)
        Xi_b0 -> Xi_c+ (-> p K- pi+) mu+ nu (Wrong sign)
    """
    # make light lepton candidates
    fake_leptons = make_fake_muons_from_b_notIsMuon()  # IsMuon==0 and no pid
    descriptor_rs = "[Xi_b0 -> Xi_c+ mu-]cc"
    descriptor_ws = "[Xi_b0 -> Xi_c+ mu+]cc"

    # make Xi_c+ candidates
    xicplus = make_xicplus_topkpi()

    # make Xib0 candidates (RS and WS)
    xibs_rs = make_b2xclnu(
        name="Xib0ToXicplusMuBuilder_FakeLepton_RS",
        descriptor=descriptor_rs,
        particles=[xicplus, fake_leptons],
    )
    xibs_ws = make_b2xclnu(
        name="Xib0ToXicplusMuBuilder_FakeLepton_WS",
        descriptor=descriptor_ws,
        particles=[xicplus, fake_leptons],
    )
    return ParticleContainersMerger([xibs_rs, xibs_ws])


def make_xibminustoxic0munu_xic0topkkpi(process):
    """
    SL line for the decays:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) mu- nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) mu+ nu (Wrong sign)
    """
    # make light lepton candidates
    leptons = make_muons_from_b()
    descriptor_rs = "[Xi_b- -> Xi_c0 mu-]cc"
    descriptor_ws = "[Xi_b- -> Xi_c0 mu+]cc"

    # make Xi_c0 candidates
    xic0 = make_xic0_topkkpi()

    # make Xib- candidates
    xibs_rs = make_b2xclnu(
        name="XibminusToXic0MuBuilder_RS",
        descriptor=descriptor_rs,
        particles=[xic0, leptons],
    )
    xibs_ws = make_b2xclnu(
        name="XibminusToXic0MuBuilder_WS",
        descriptor=descriptor_ws,
        particles=[xic0, leptons],
    )
    return ParticleContainersMerger([xibs_rs, xibs_ws])


def make_xibminustoxic0munu_xic0topkkpi_fakelepton(process):
    """
    SL line for the decays with a fake muon:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) mu- nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) mu+ nu (Wrong sign)
    """
    # make light lepton candidates
    fake_leptons = make_fake_muons_from_b_notIsMuon()  # IsMuon==0 and no pid
    descriptor_rs = "[Xi_b- -> Xi_c0 mu-]cc"
    descriptor_ws = "[Xi_b- -> Xi_c0 mu+]cc"

    # make Xi_c0 candidates
    xic0 = make_xic0_topkkpi()

    # make Xib- candidates
    xibs_rs = make_b2xclnu(
        name="XibminusToXic0MuBuilder_fakeL_RS_combiner",
        descriptor=descriptor_rs,
        particles=[xic0, fake_leptons],
    )
    xibs_ws = make_b2xclnu(
        name="XibminusToXic0MuBuilder_fakeL_WS_combiner",
        descriptor=descriptor_ws,
        particles=[xic0, fake_leptons],
    )
    return ParticleContainersMerger([xibs_rs, xibs_ws])


def make_omegabtoomegacmunu_omegactopkkpi(process):
    """
    Selection for the decay [Omegab- -> Omegac0(-> p K- K- pi+) mu nu]cc.
    """

    leptons = make_muons_from_b()
    descriptor_rs = "[Omega_b- -> Omega_c0 mu-]cc"
    descriptor_ws = "[Omega_b- -> Omega_c0 mu+]cc"

    omegacs = make_omegac_topkkpi()

    omegab_rightsign = make_b2xclnu(
        particles=[omegacs, leptons],
        descriptor=descriptor_rs,
        name="Omegab2OmegacMuNu_Omegac2pKKPi_combiner",
    )
    omegab_wrongsign = make_b2xclnu(
        particles=[omegacs, leptons],
        descriptor=descriptor_ws,
        name="Omegab2OmegacMuNu_Omegac2pKKPi_WS_combiner",
    )
    linealg = ParticleContainersMerger([omegab_rightsign, omegab_wrongsign])

    return linealg


def make_omegabtoomegacmunu_omegactopkkpi_fakelepton(process):
    """
    Selection for the decay [Omegab- -> Omegac0(-> p K- K- pi+) mu nu]cc, with a fake lepton.
    """

    # Fake muons
    leptons = make_fake_muons_from_b_notIsMuon()
    descriptor_rs = "[Omega_b- -> Omega_c0 mu-]cc"
    descriptor_ws = "[Omega_b- -> Omega_c0 mu+]cc"

    omegacs = make_omegac_topkkpi()

    omegab_rightsign = make_b2xclnu(
        particles=[omegacs, leptons],
        descriptor=descriptor_rs,
        name="Omegab2OmegacMuNu_Omegac2pKKPi_fakeL_combiner",
    )
    omegab_wrongsign = make_b2xclnu(
        particles=[omegacs, leptons],
        descriptor=descriptor_ws,
        name="Omegab2OmegacMuNu_Omegac2pKKPi_fakeL_WS_combiner",
    )
    linealg = ParticleContainersMerger([omegab_rightsign, omegab_wrongsign])

    return linealg
