###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of semileptonic selections for the Hb -> Hc tau nu topology,
with tau -> l nu nu

Non trivial imports:
basic builders, prefilters

Returns:
every function returns a line_alg
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from PyConf import ConfigurationError
from RecoConf.algorithms_thor import ParticleContainersMerger

from .builders.b_builder import make_b2xtaunu
from .builders.base_builder import (
    make_fake_tauons_electronic_decay,
    make_fake_tauons_muonic_decay,
    make_tauons_electronic_decay,
    make_tauons_muonic_decay,
)
from .builders.charm_hadron_builder import (
    make_d0_tok3pi,
    make_d0_tokpi,
    make_dplus_tokpipi,
    make_ds_tokkpi,
    make_Hc_to_nbody,
    make_jpsi_tomumu,
    make_lambdac_tolambda0pi,
    make_lambdac_topkpi,
    make_lambdac_topks,
    make_omegac_topkkpi,
    make_xic0_topkkpi,
    make_xicplus_topkpi,
)


def make_bctojpsitaunu_jpsitomumu_tautolnunu(process, lepton):
    """
    Selection for the decay Bc+ -> Jpsi(-> mu mu ) tau(-> l nu nu) nu.
    """
    if lepton == "mu":
        tauons = make_tauons_muonic_decay()
        descriptor_rs = "[B_c- -> J/psi(1S) mu-]cc"
    elif lepton == "e":
        tauons = make_tauons_electronic_decay()
        descriptor_rs = "[B_c- -> J/psi(1S) e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    jpsi = make_jpsi_tomumu(
        comb_m_min=2977 * MeV, comb_m_max=3217 * MeV, vchi2pdof_max=4
    )
    bc_jpsimum = make_b2xtaunu(
        [jpsi, tauons],
        descriptor=descriptor_rs,
        name=f"Bc2JpsiTauNu_Tau2{lepton}NuNu_Jpsi2MuMu_combiner",
    )
    return bc_jpsimum


def make_bctojpsitaunu_jpsitomumu_tautolnunu_fakelepton(process, lepton):
    """
    Selection for the decay Bc+ -> Jpsi(-> mu mu ) tau(-> l nu nu) nu, with a fake lepton.
    """
    if lepton == "mu":
        tauons = make_fake_tauons_muonic_decay()
        descriptor_rs = "[B_c- -> J/psi(1S) mu-]cc"
    elif lepton == "e":
        tauons = make_fake_tauons_electronic_decay()
        descriptor_rs = "[B_c- -> J/psi(1S) e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    jpsi = make_jpsi_tomumu(
        comb_m_min=2977 * MeV, comb_m_max=3217 * MeV, vchi2pdof_max=4
    )
    bc_jpsimum = make_b2xtaunu(
        [jpsi, tauons],
        descriptor=descriptor_rs,
        name=f"Bc2JpsiTauNu_Tau2{lepton}NuNu_Jpsi2MuMu_fakeL_combiner",
    )
    return bc_jpsimum


def make_butod0taunu_d0tokpi_tautolnunu(process, lepton):
    """
    Selection for the decay B+ -> D0(-> K pi) tau(-> l nu nu) nu.
    """
    if lepton == "mu":
        taus = make_tauons_muonic_decay()
        decay_descriptor = "[B- -> D0 mu-]cc"
        decay_descriptor_ws = "[B+ -> D0 mu+]cc"
    elif lepton == "e":
        taus = make_tauons_electronic_decay(
            p_min=3.0 * GeV,
            pt_min=300.0 * MeV,
            pid=(F.PID_E > 2.0),
            mipchi2_min=15.0,
            eta_cut=F.require_all(F.ETA > 2.2, F.ETA < 4.2),
        )
        decay_descriptor = "[B- -> D0 e-]cc"
        decay_descriptor_ws = "[B+ -> D0 e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    if lepton == "e":
        cuts = {
            "mother_p_min": 15 * GeV,
            "comb_docachi2_max": 5.0,
            "vchi2pdof_max": 5.0,
            "bpvdira_min": 0.9998,
            "daughter_pt_min": 300 * MeV,
            "daughter_p_min": 5 * GeV,
            "daughter_mipchi2_min": 45,
            "kaon_pid": F.require_all(F.PID_K > 4.0, F.GHOSTPROB < 0.35),
            "pion_pid": F.require_all(F.PID_K < 0.0, F.GHOSTPROB < 0.35),
        }

    if lepton == "mu":
        d0s = make_d0_tokpi(
            vchi2pdof_max=4,
            bpvdira_min=0.999,
            comb_pt_min=2000 * MeV,
            comb_pt_any_min=800 * MeV,
            comb_pt_sum_min=2.5 * GeV,
            comb_doca_max=0.1 * mm,
            daughter_mipchi2_min=9.0,
            kaon_pid=(F.PID_K > 4.0),
            pion_pid=(F.PID_K < 2.0),
            comb_docachi2_max=None,
            daughter_pt_min=300.0 * MeV,
            daughter_p_min=5 * GeV,
        )

    elif lepton == "e":
        d0s = make_d0_tokpi(**cuts)

    B_extra_cuts = {}
    if lepton == "e":
        B_extra_cuts = {
            "m_min": 1500 * MeV,
            "m_max": 10000 * MeV,
            "comb_m_min": 1500 * MeV,
            "comb_doca_max": 0.3 * mm,
            "bpvfdchi2_min": 20,
            "bpvdira_min": 0.9995,
            "vchi2pdof_max": 9,
            "b_d_dzs_min": -5 * mm,
        }

    b0s_rightsign = make_b2xtaunu(
        particles=[d0s, taus],
        descriptor=decay_descriptor,
        comb_m_max=10000.0 * MeV,
        **B_extra_cuts,
        name=f"B02D0TauNuX_Tau2{lepton}NuNu_D02KPi_combiner",
    )

    b0s_wrongsign = make_b2xtaunu(
        particles=[d0s, taus],
        descriptor=decay_descriptor_ws,
        comb_m_max=10000.0 * MeV,
        **B_extra_cuts,
        name=f"B02D0TauNuX_Tau2{lepton}NuNu_D02KPi_WS_combiner",
    )

    line_alg = ParticleContainersMerger([b0s_rightsign, b0s_wrongsign])

    return line_alg


def make_butod0taunu_d0tokpi_tautolnunu_fakelepton(process, lepton):
    """
    Selection for the decay B+ -> D0(-> K pi) tau(-> l nu nu) nu, with a fake lepton.
    """
    if lepton == "mu":
        taus = make_fake_tauons_muonic_decay()
        decay_descriptor = "[B- -> D0 mu-]cc"
        decay_descriptor_ws = "[B+ -> D0 mu+]cc"
    elif lepton == "e":
        taus = make_fake_tauons_electronic_decay(
            p_min=3.0 * GeV,
            pt_min=300.0 * MeV,
            pid=(F.PID_E < 2.0),
            mipchi2_min=15.0,
            eta_cut=F.require_all(F.ETA > 2.2, F.ETA < 4.2),
        )
        decay_descriptor = "[B- -> D0 e-]cc"
        decay_descriptor_ws = "[B+ -> D0 e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    if lepton == "e":
        cuts = {
            "mother_p_min": 15 * GeV,
            "comb_docachi2_max": 5.0,
            "vchi2pdof_max": 5.0,
            "bpvdira_min": 0.9998,
            "daughter_pt_min": 300 * MeV,
            "daughter_p_min": 5 * GeV,
            "daughter_mipchi2_min": 45,
            "kaon_pid": F.require_all(F.PID_K > 4.0, F.GHOSTPROB < 0.35),
            "pion_pid": F.require_all(F.PID_K < 0.0, F.GHOSTPROB < 0.35),
        }

    if lepton == "mu":
        d0s = make_d0_tokpi(
            vchi2pdof_max=4,
            bpvdira_min=0.999,
            comb_pt_min=2000 * MeV,
            comb_pt_any_min=800 * MeV,
            comb_pt_sum_min=2.5 * GeV,
            comb_doca_max=0.1 * mm,
            daughter_mipchi2_min=9.0,
            kaon_pid=(F.PID_K > 4.0),
            pion_pid=(F.PID_K < 2.0),
            comb_docachi2_max=None,
            daughter_pt_min=300.0 * MeV,
            daughter_p_min=5 * GeV,
        )

    elif lepton == "e":
        d0s = make_d0_tokpi(**cuts)
    B_extra_cuts = {}
    if lepton == "e":
        B_extra_cuts = {
            "m_min": 1500 * MeV,
            "m_max": 10000 * MeV,
            "comb_m_min": 1500 * MeV,
            "comb_doca_max": 0.3 * mm,
            "bpvfdchi2_min": 20,
            "bpvdira_min": 0.9995,
            "vchi2pdof_max": 9,
            "b_d_dzs_min": -5 * mm,
        }

    b0s_rightsign = make_b2xtaunu(
        particles=[d0s, taus],
        descriptor=decay_descriptor,
        comb_m_max=10000.0 * MeV,
        **B_extra_cuts,
        name=f"B02D0TauNuX_Tau2{lepton}NuNu_D02KPi_fakeL_combiner",
    )

    b0s_wrongsign = make_b2xtaunu(
        particles=[d0s, taus],
        descriptor=decay_descriptor_ws,
        comb_m_max=10000.0 * MeV,
        **B_extra_cuts,
        name=f"B02D0TauNuX_Tau2{lepton}NuNu_D02KPi_fakeL_WS_combiner",
    )

    line_alg = ParticleContainersMerger([b0s_rightsign, b0s_wrongsign])

    return line_alg


def make_butod0taunu_d0tok3pi_tautolnunu(process, lepton):
    """
    Selection for the decay B+ -> D0(-> K pi pi) tau(-> l nu nu) nu.
    """
    if lepton == "mu":
        tauons = make_tauons_muonic_decay()
        descriptor_rs = "[B- -> D0 mu-]cc"
        descriptor_ws = "[B+ -> D0 mu+]cc"
    elif lepton == "e":
        tauons = make_tauons_electronic_decay(
            p_min=3.0 * GeV,
            pt_min=300.0 * MeV,
            pid=(F.PID_E > 2.0),
            mipchi2_min=15.0,
            eta_cut=F.require_all(F.ETA > 2.2, F.ETA < 4.2),
        )
        descriptor_rs = "[B- -> D0 e-]cc"
        descriptor_ws = "[B+ -> D0 e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    if lepton == "e":
        cuts = {
            "mother_p_min": 15 * GeV,
            "comb_docachi2_max": 5.0,
            "vchi2pdof_max": 5.0,
            "bpvdira_min": 0.9998,
            "daughter_pt_min": 750.0 * MeV,
        }
    if lepton == "mu":
        dzs = make_d0_tok3pi(
            mother_p_min=5 * GeV, comb_docachi2_max=None, daughter_pt_min=300.0 * MeV
        )

    elif lepton == "e":
        dzs = make_d0_tok3pi(**cuts)
    bus_dzmum = make_b2xtaunu(
        [dzs, tauons],
        descriptor=descriptor_rs,
        name=f"B2D0TauNu_Tau2{lepton}NuNu_D02K3pi_combiner",
    )
    bus_dzmup_wrongsign_D = make_b2xtaunu(
        [dzs, tauons],
        descriptor=descriptor_ws,
        name=f"B2D0TauNu_Tau2{lepton}NuNu_D02K3pi_WS_combiner",
    )
    line_alg = ParticleContainersMerger([bus_dzmum, bus_dzmup_wrongsign_D])

    return line_alg


def make_butod0taunu_d0tok3pi_tautolnunu_fakelepton(process, lepton):
    """
    Selection for the decay B+ -> D0(-> K pi pi pi) tau(-> l nu nu) nu, with a fake lepton.
    """
    if lepton == "mu":
        fake_tauons = make_fake_tauons_muonic_decay()
        descriptor_rs = "[B- -> D0 mu-]cc"
        descriptor_ws = "[B+ -> D0 mu+]cc"
    elif lepton == "e":
        fake_tauons = make_fake_tauons_electronic_decay(
            p_min=3.0 * GeV,
            pt_min=300.0 * MeV,
            pid=(F.PID_E < 2.0),
            mipchi2_min=15.0,
            eta_cut=F.require_all(F.ETA > 2.2, F.ETA < 4.2),
        )
        descriptor_rs = "[B- -> D0 e-]cc"
        descriptor_ws = "[B+ -> D0 e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    if lepton == "e":
        cuts = {
            "mother_p_min": 15 * GeV,
            "comb_docachi2_max": 5.0,
            "vchi2pdof_max": 5.0,
            "bpvdira_min": 0.9998,
            "daughter_pt_min": 750.0 * MeV,
        }
    if lepton == "mu":
        dzs = make_d0_tok3pi(
            mother_p_min=5 * GeV, comb_docachi2_max=None, daughter_pt_min=300.0 * MeV
        )

    elif lepton == "e":
        dzs = make_d0_tok3pi(**cuts)
    bus_dzmum = make_b2xtaunu(
        [dzs, fake_tauons],
        descriptor=descriptor_rs,
        name=f"B2D0TauNu_Tau2{lepton}NuNu_D02K3pi_fakeL_combiner",
    )
    bus_dzmup_wrongsign_D = make_b2xtaunu(
        [dzs, fake_tauons],
        descriptor=descriptor_ws,
        name=f"B2D0TauNu_Tau2{lepton}NuNu_D02K3pi_fakeL_WS_combiner",
    )
    line_alg = ParticleContainersMerger([bus_dzmum, bus_dzmup_wrongsign_D])

    return line_alg


def make_b0todptaunu_dptokpipi_tautolnunu(process, lepton):
    """
    Selection for the decay B0 -> D+(-> K pi pi) tau(-> l nu nu) nu.
    """
    if lepton == "mu":
        tauons = make_tauons_muonic_decay()
        descriptor_rs = "[B0 -> D- mu+]cc"
        descriptor_ws = "[B0 -> D- mu-]cc"
    elif lepton == "e":
        tauons = make_tauons_electronic_decay()
        descriptor_rs = "[B0 -> D- e+]cc"
        descriptor_ws = "[B0 -> D- e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    dps = make_dplus_tokpipi()
    b0s_rightsign = make_b2xtaunu(
        particles=[dps, tauons],
        descriptor=descriptor_rs,
        name=f"B02DpTauNu_Tau2{lepton}NuNu_Dp2KPiPi_combiner",
    )
    b0s_wrongsign = make_b2xtaunu(
        particles=[dps, tauons],
        descriptor=descriptor_ws,
        name=f"B02DpTauNu_Tau2{lepton}NuNu_Dp2KPiPi_WS_combiner",
    )
    line_alg = ParticleContainersMerger([b0s_rightsign, b0s_wrongsign])

    return line_alg


def make_b0todptaunu_dptokpipi_tautolnunu_fakelepton(process, lepton):
    """
    Selection for the decay B0 -> D+(-> K pi pi) tau(-> l nu nu) nu, with a fake lepton.
    """
    if lepton == "mu":
        fake_tauons = make_fake_tauons_muonic_decay()
        descriptor_rs = "[B0 -> D- mu+]cc"
        descriptor_ws = "[B0 -> D- mu-]cc"
    elif lepton == "e":
        fake_tauons = make_fake_tauons_electronic_decay()
        descriptor_rs = "[B0 -> D- e+]cc"
        descriptor_ws = "[B0 -> D- e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    dps = make_dplus_tokpipi()
    b0s_rightsign = make_b2xtaunu(
        particles=[dps, fake_tauons],
        descriptor=descriptor_rs,
        name=f"B02DpTauNu_Tau2{lepton}NuNu_Dp2KPiPi_fakeL_combiner",
    )
    b0s_wrongsign = make_b2xtaunu(
        particles=[dps, fake_tauons],
        descriptor=descriptor_ws,
        name=f"B02DpTauNu_Tau2{lepton}NuNu_Dp2KPiPi_fakeL_WS_combiner",
    )
    line_alg = ParticleContainersMerger([b0s_rightsign, b0s_wrongsign])

    return line_alg


def make_bstodstaunu_dstokkpi_tautolnunu(process, lepton):
    """
    Selection for the decay Bs0 -> Ds-(-> K K pi) tau+(-> l nu nu) nu, and combinatorial (same sign).
    """
    if lepton == "mu":
        tauons = make_tauons_muonic_decay()
        descriptor_rs = "[B_s0 -> D_s- mu+]cc"
        descriptor_ws = "[B_s0 -> D_s- mu-]cc"
    elif lepton == "e":
        tauons = make_tauons_electronic_decay()
        descriptor_rs = "[B_s0 -> D_s- e+]cc"
        descriptor_ws = "[B_s0 -> D_s- e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    # kaons = make_kaons() #maybe put in line instantiation for extra output?
    # pions = make_pions()
    dss = make_ds_tokkpi()
    bss_rightsign = make_b2xtaunu(
        particles=[dss, tauons],
        descriptor=descriptor_rs,
        name=f"Bs2DsTauNu_Tau2{lepton}NuNu_Ds2KKPi_combiner",
    )
    bss_wrongsign = make_b2xtaunu(
        particles=[dss, tauons],
        descriptor=descriptor_ws,
        name=f"Bs2DsTauNu_Tau2{lepton}NuNu_Ds2KKPi_WS_combiner",
    )
    line_alg = ParticleContainersMerger([bss_rightsign, bss_wrongsign])

    return line_alg


def make_bstodstaunu_dstokkpi_tautolnunu_fakelepton(process, lepton):
    """
    Selection for the decay Bs0 -> Ds-(-> K K pi) tau+(-> l nu nu) nu and combinatorial (same sign), with a fake lepton.
    """
    if lepton == "mu":
        fake_tauons = make_fake_tauons_muonic_decay()
        descriptor_rs = "[B_s0 -> D_s- mu+]cc"
        descriptor_ws = "[B_s0 -> D_s- mu-]cc"
    elif lepton == "e":
        fake_tauons = make_fake_tauons_electronic_decay()
        descriptor_rs = "[B_s0 -> D_s- e+]cc"
        descriptor_ws = "[B_s0 -> D_s- e-]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")
    # kaons = make_kaons() maybe put in line instantiation for extra output?
    # pions = make_pions()
    dss = make_ds_tokkpi()
    bss_rightsign = make_b2xtaunu(
        particles=[dss, fake_tauons],
        descriptor=descriptor_rs,
        name=f"Bs2DsTauNu_Tau2{lepton}NuNu_Ds2KKPi_fakeL_combiner",
    )
    bss_wrongsign = make_b2xtaunu(
        particles=[dss, fake_tauons],
        descriptor=descriptor_ws,
        name=f"Bs2DsTauNu_Tau2{lepton}NuNu_Ds2KKPi_fakeL_WS_combiner",
    )

    line_alg = ParticleContainersMerger([bss_rightsign, bss_wrongsign])

    return line_alg


def make_lbtolctaunu_lctopkpi_tautolnu(process, lepton):
    """
    Selection for the decay [Lb0 -> Lc+(-> p K pi) tau-(-> l nu nu) nu]cc.
    """

    if lepton == "mu":
        tauons = make_tauons_muonic_decay()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ mu-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ mu+]cc"
    elif lepton == "e":
        tauons = make_tauons_electronic_decay()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ e-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ e+]cc"
    else:
        raise ConfigurationError("Lepton must be either mu or e")

    lcs = make_lambdac_topkpi(
        daughter_pt_min=300 * MeV,
        daughter_mipchi2_min=9,
        kaon_pid=(F.PID_K > 4.0),
        pion_pid=(F.PID_K < 2.0),
    )

    lb_rightsign = make_b2xtaunu(
        particles=[lcs, tauons],
        descriptor=descriptor_rs,
        comb_m_max=10200.0 * MeV,
        comb_doca_max=None,
        bpvfdchi2_min=None,
        name=f"Lb2LcTauNu_Tau2{lepton}NuNu_Lc2pKPi_combiner",
    )

    lb_wrongsign = make_b2xtaunu(
        particles=[lcs, tauons],
        descriptor=descriptor_ws,
        comb_m_max=10200.0 * MeV,
        comb_doca_max=None,
        bpvfdchi2_min=None,
        name=f"Lb2LcTauNu_Tau2{lepton}NuNu_Lc2pKPi_WS_combiner",
    )

    line_alg = ParticleContainersMerger([lb_rightsign, lb_wrongsign])

    return line_alg


def make_lbtolctaunu_lctopkpi_tautolnu_fakelepton(process, lepton):
    """
    Selection for the decay [Lb0 -> Lc+(-> p K pi) tau-(-> l nu nu) nu]cc and combinatorial (same sign) with a fake muon.
    """

    if lepton == "mu":
        tauons = make_fake_tauons_muonic_decay()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ mu-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ mu+]cc"
    elif lepton == "e":
        tauons = make_fake_tauons_electronic_decay()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ e-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ e+]cc"
    else:
        raise ConfigurationError("Lepton must either mu or e")

    lcs = make_lambdac_topkpi(
        daughter_pt_min=300.0 * MeV,
        daughter_mipchi2_min=9.0,
        kaon_pid=(F.PID_K > 4.0),
        pion_pid=(F.PID_K < 2.0),
    )

    lb_rightsign = make_b2xtaunu(
        particles=[lcs, tauons],
        descriptor=descriptor_rs,
        comb_m_max=10200.0 * MeV,
        comb_doca_max=None,
        bpvfdchi2_min=None,
        name=f"Lb2LcTauNu_Tau2{lepton}NuNu_Lc2pKPi_fakeL_combiner",
    )

    lb_wrongsign = make_b2xtaunu(
        particles=[lcs, tauons],
        descriptor=descriptor_ws,
        comb_m_max=10200.0 * MeV,
        comb_doca_max=None,
        bpvfdchi2_min=None,
        name=f"Lb2LcTauNu_Tau2{lepton}NuNu_Lc2pKPi_fakeL_WS_combiner",
    )

    line_alg = ParticleContainersMerger([lb_rightsign, lb_wrongsign])

    return line_alg


def make_lbtolctaunu_lctoV0h_tautolnu(process, V0_name, V0_type, lepton_name):
    """
    Selection for the decay [Lb0 -> Lc+ (-> V0 hadron) tau- (-> l nu nu) nu]cc and combinatorial (same sign) with a fake lepton.
    Here the 'V0' is either a 'K0S' or a 'Lambda0'.
    When 'V0'='K0S', the 'hadron' is a 'pi+. When 'V0'='Lambda0', the 'hadron' is a 'proton'.

    Args:
        process: The process to which the selection belongs to. Must be 'Hlt2' or 'Spruce' or 'Turbo'.
        V0_name: The name of the V0 particle. Must be either 'Lambda0' or 'KS'.
        V0_type: The type of the V0 particle. Must be either 'LL' or 'DD'.
        lepton_name: The name of the lepton. Must be either 'mu' or 'e'.
    """

    Lc_mass = 2286.46  # units MeV
    delta_Lc_comb = 100.0  # units MeV, Delta combination
    delta_Lc_mother = 80.0  # units MeV, Delta composite

    if V0_name == "Lambda0":
        lc_maker = make_lambdac_tolambda0pi
    elif V0_name == "KS0":
        lc_maker = make_lambdac_topks
    else:
        raise ConfigurationError(
            f"V0_name must be either 'Lambda0' or 'KS'. Got '{V0_name}' instead. Please check."
        )

    if lepton_name == "mu":
        tauons = make_tauons_muonic_decay()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ mu-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ mu+]cc"
    elif lepton_name == "e":
        tauons = make_tauons_electronic_decay()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ e-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ e+]cc"
    else:
        raise ConfigurationError(
            f"Lepton must be either 'mu' or 'e'. Got '{lepton_name}' instead. Please check."
        )

    with make_Hc_to_nbody.bind(vchi2pdof_max=6.0, bpvfdchi2_min=None, bpvdira_min=None):
        lcs = lc_maker(
            V0_type,
            mother_m_min=(Lc_mass - delta_Lc_mother) * MeV,
            mother_m_max=(Lc_mass + delta_Lc_mother) * MeV,
            comb_m_min=(Lc_mass - delta_Lc_comb) * MeV,
            comb_m_max=(Lc_mass + delta_Lc_comb) * MeV,
        )

    lb_rightsign = make_b2xtaunu(particles=[lcs, tauons], descriptor=descriptor_rs)

    lb_wrongsign = make_b2xtaunu(particles=[lcs, tauons], descriptor=descriptor_ws)

    line_alg = ParticleContainersMerger([lb_rightsign, lb_wrongsign])

    return line_alg


def make_lbtolctaunu_lctoV0h_tautolnu_fakelepton(
    process, V0_name, V0_type, lepton_name
):
    """
    Selection for the decay [Lb0 -> Lc+ (-> V0 hadron) tau- (-> l nu nu) nu]cc and combinatorial (same sign) with a fake lepton.
    Here the 'V0' is either a 'K0S' or a 'Lambda0'.
    When 'V0'='K0S', the 'hadron' is a 'pi+. When 'V0'='Lambda0', the 'hadron' is a 'proton'.

    Args:
        process: The process to which the selection belongs to. Must be 'Hlt2' or 'Spruce' or 'Turbo'.
        V0_name: The name of the V0 particle. Must be either 'Lambda0' or 'KS'.
        V0_type: The type of the V0 particle. Must be either 'LL' or 'DD'.
        lepton_name: The name of the lepton. Must be either 'mu' or 'e'.
    """

    Lc_mass = 2286.46  # units MeV
    delta_Lc_comb = 100.0  # units MeV, Delta combination
    delta_Lc_mother = 80.0  # units MeV, Delta composite

    if V0_name == "Lambda0":
        lc_maker = make_lambdac_tolambda0pi
    elif V0_name == "KS0":
        lc_maker = make_lambdac_topks
    else:
        raise ConfigurationError(
            f"V0_name must be either 'Lambda0' or 'KS'. Got '{V0_name}' instead. Please check."
        )

    if lepton_name == "mu":
        tauons = make_fake_tauons_muonic_decay()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ mu-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ mu+]cc"
    elif lepton_name == "e":
        tauons = make_fake_tauons_electronic_decay()
        descriptor_rs = "[Lambda_b0 -> Lambda_c+ e-]cc"
        descriptor_ws = "[Lambda_b0 -> Lambda_c+ e+]cc"
    else:
        raise ConfigurationError(
            f"Lepton must be either 'mu' or 'e'. Got '{lepton_name}' instead. Please check."
        )

    with make_Hc_to_nbody.bind(vchi2pdof_max=6.0, bpvfdchi2_min=None, bpvdira_min=None):
        lcs = lc_maker(
            V0_type,
            mother_m_min=(Lc_mass - delta_Lc_mother) * MeV,
            mother_m_max=(Lc_mass + delta_Lc_mother) * MeV,
            comb_m_min=(Lc_mass - delta_Lc_comb) * MeV,
            comb_m_max=(Lc_mass + delta_Lc_comb) * MeV,
        )

    lb_rightsign = make_b2xtaunu(particles=[lcs, tauons], descriptor=descriptor_rs)

    lb_wrongsign = make_b2xtaunu(particles=[lcs, tauons], descriptor=descriptor_ws)

    line_alg = ParticleContainersMerger([lb_rightsign, lb_wrongsign])

    return line_alg


def make_xib0toxicplustaunu_xicplustopkpi_tautomununu(process):
    """
    SL line for the decays:
        Xi_b0 -> Xi_c+ (-> p K- pi+) tau- (-> mu- nu nu) nu (Right sign)
        Xi_b0 -> Xi_c+ (-> p K- pi+) tau+ (-> mu+ nu nu) nu (Wrong sign)
    """
    # make tau candidates
    descriptor_rs = "[Xi_b0 -> Xi_c+ mu-]cc"
    descriptor_ws = "[Xi_b0 -> Xi_c+ mu+]cc"
    taus = make_tauons_muonic_decay()

    # make Xi_c+ candidates
    xicplus = make_xicplus_topkpi()

    # make Xib0 candidates (RS and WS)
    xibs_rs = make_b2xtaunu(
        name="Xib0ToXicplustauBuilder_Tau2MuNuNu_RS",
        descriptor=descriptor_rs,
        particles=[xicplus, taus],
    )
    xibs_ws = make_b2xtaunu(
        name="Xib0ToXicplustauBuilder_Tau2MuNuNu_WS",
        descriptor=descriptor_ws,
        particles=[xicplus, taus],
    )
    return ParticleContainersMerger([xibs_rs, xibs_ws])


def make_xib0toxicplustaunu_xicplustopkpi_tautomununu_fakelepton(process):
    """
    SL line for the decays:
        Xi_b0 -> Xi_c+ (-> p K- pi+) tau- (-> mu- nu nu) nu (Right sign)
        Xi_b0 -> Xi_c+ (-> p K- pi+) tau+ (-> mu+ nu nu) nu (Wrong sign)
    """
    # make tau candidates
    descriptor_rs = "[Xi_b0 -> Xi_c+ mu-]cc"
    descriptor_ws = "[Xi_b0 -> Xi_c+ mu+]cc"
    fake_taus = make_fake_tauons_muonic_decay()

    # make Xi_c+ candidates
    xicplus = make_xicplus_topkpi()

    # make Xib0 candidates (RS and WS)
    xibs_rs = make_b2xtaunu(
        name="Xib0ToXicplustauBuilder_Tau2MuNuNu_FakeLepton_RS",
        descriptor=descriptor_rs,
        particles=[xicplus, fake_taus],
    )
    xibs_ws = make_b2xtaunu(
        name="Xib0ToXicplustauBuilder_Tau2MuNuNu_FakeLepton_WS",
        descriptor=descriptor_ws,
        particles=[xicplus, fake_taus],
    )
    return ParticleContainersMerger([xibs_rs, xibs_ws])


def make_xibminustoxic0taunu_xic0topkkpi_tautomununu(process):
    """
    SL line for the decays:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau- (-> mu- nu nu) nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau- (-> mu+ nu nu) nu (Wrong sign)
    """
    # make light lepton candidates
    taus = make_tauons_muonic_decay()
    descriptor_rs = "[Xi_b- -> Xi_c0 mu-]cc"
    descriptor_ws = "[Xi_b- -> Xi_c0 mu+]cc"

    # make Xi_c0 candidates
    xic0 = make_xic0_topkkpi()

    # make Xib- candidates
    xibs_rs = make_b2xtaunu(
        name="XibminusToXic0tauBuilder_Tau2MuNuNu_RS",
        descriptor=descriptor_rs,
        particles=[xic0, taus],
    )
    xibs_ws = make_b2xtaunu(
        name="XibminusToXic0tauBuilder_Tau2MuNuNu_WS",
        descriptor=descriptor_ws,
        particles=[xic0, taus],
    )
    return ParticleContainersMerger([xibs_rs, xibs_ws])


def make_xibminustoxic0taunu_xic0topkkpi_tautomununu_fakelepton(process):
    """
    SL line for the decays with a fake lepton:
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau- (-> mu- nu nu) nu (Right sign)
        Xi_b- -> Xi_c0 (-> p K- K- pi+) tau- (-> mu+ nu nu) nu (Wrong sign)
    """
    # make light lepton candidates
    fake_taus = make_fake_tauons_muonic_decay()
    descriptor_rs = "[Xi_b- -> Xi_c0 mu-]cc"
    descriptor_ws = "[Xi_b- -> Xi_c0 mu+]cc"

    # make Xi_c0 candidates
    xic0 = make_xic0_topkkpi()

    # make Xib- candidates
    xibs_rs = make_b2xtaunu(
        name="XibminusToXic0tauBuilder_Tau2MuNuNu_FakeLepton_RS",
        descriptor=descriptor_rs,
        particles=[xic0, fake_taus],
    )
    xibs_ws = make_b2xtaunu(
        name="XibminusToXic0tauBuilder_Tau2MuNuNu_FakeLepton_WS",
        descriptor=descriptor_ws,
        particles=[xic0, fake_taus],
    )
    return ParticleContainersMerger([xibs_rs, xibs_ws])


def make_omegabtoomegactaunu_omegactopkkpi_tautomununu(process):
    """
    Selection for the decay [Omegab- -> Omegac0(-> p K- K- pi+) tau-(-> mu nu nu) nu]cc.
    """

    tauons = make_tauons_muonic_decay()
    descriptor_rs = "[Omega_b- -> Omega_c0 mu-]cc"
    descriptor_ws = "[Omega_b- -> Omega_c0 mu+]cc"

    omegacs = make_omegac_topkkpi()

    omegab_rightsign = make_b2xtaunu(
        particles=[omegacs, tauons],
        descriptor=descriptor_rs,
        name="Omegab2OmegacTauNu_Tau2MuNuNu_Omegac2pKKPi_combiner",
    )
    omegab_wrongsign = make_b2xtaunu(
        particles=[omegacs, tauons],
        descriptor=descriptor_ws,
        name="Omegab2OmegacTauNu_Tau2MuNuNu_Omegac2pKKPi_WS_combiner",
    )
    linealg = ParticleContainersMerger([omegab_rightsign, omegab_wrongsign])

    return linealg


def make_omegabtoomegactaunu_omegactopkkpi_tautomununu_fakelepton(process):
    """
    Selection for the decay [Omegab- -> Omegac0(-> p K- K- pi+) tau-(-> mu nu nu) nu]cc with a fake lepton.
    """

    # Fake muons
    tauons = make_fake_tauons_muonic_decay()
    descriptor_rs = "[Omega_b- -> Omega_c0 mu-]cc"
    descriptor_ws = "[Omega_b- -> Omega_c0 mu+]cc"

    omegacs = make_omegac_topkkpi()

    omegab_rightsign = make_b2xtaunu(
        particles=[omegacs, tauons],
        descriptor=descriptor_rs,
        name="Omegab2OmegacTauNu_Tau2MuNuNu_Omegac2pKKPi_fakeL_combiner",
    )
    omegab_wrongsign = make_b2xtaunu(
        particles=[omegacs, tauons],
        descriptor=descriptor_ws,
        name="Omegab2OmegacTauNu_Tau2MuNuNu_Omegac2pKKPi_fakeL_WS_combiner",
    )
    linealg = ParticleContainersMerger([omegab_rightsign, omegab_wrongsign])

    return linealg
