###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from GaudiKernel.SystemOfUnits import MeV, mm
from RecoConf.algorithms_thor import ParticleContainersMerger

from .builders.b_builder import make_b2xulnu
from .builders.base_builder import (
    make_candidate,
    make_kaons_from_b,
    make_pions_from_b,
    make_protons_from_b,
    make_tauons_hadronic_decay,
)


def make_bstoktaunu_hadronic(process):
    """
    Selection for the decay Bs0 -> K tau (->pi+pi+pi-)
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )
    with make_candidate.bind(track_chisq_ndof_max=2.5, track_ghostprob_max=0.03):
        kaons = make_kaons_from_b(
            pid=F.require_all(
                F.PID_K > 10, F.PID_K - F.PID_P > 5, F.PID_K - F.PID_MU > 10
            ),
            p_min=12000 * MeV,
            pt_min=1200 * MeV,
            mipchi2_min=60.0,
        )

    taus = make_tauons_hadronic_decay()

    comb_m_min = 1000.0 * MeV
    comb_m_max = 5000.0 * MeV
    mcorr_min = 2500.0 * MeV
    mcorr_max = 6500.0 * MeV
    vchi2pdof_max = 2.5
    bpvdira_min = 0.999
    bpvfdchi2_min = 180.0
    comb_doca_max = 0.035 * mm

    bs_rs = make_b2xulnu(
        particles=[kaons, taus],
        descriptor="[B_s~0 -> K+ tau-]cc",
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        bpvdira_min=bpvdira_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvfdchi2_min=bpvfdchi2_min,
        mcorr_min=mcorr_min,
        mcorr_max=mcorr_max,
    )
    bs_ws = make_b2xulnu(
        particles=[kaons, taus],
        descriptor="[B_s~0 -> K+ tau+]cc",
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        bpvdira_min=bpvdira_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvfdchi2_min=bpvfdchi2_min,
        mcorr_min=mcorr_min,
        mcorr_max=mcorr_max,
    )
    line_alg = ParticleContainersMerger([bs_rs, bs_ws])

    return line_alg


def make_b0topitaunu_hadronic(process):
    """
    Selection for the decay B0 -> pi tau (->pi+pi+pi-)
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )
    with make_candidate.bind(track_chisq_ndof_max=2.5, track_ghostprob_max=0.03):
        pions = make_pions_from_b(
            pid=F.require_all(
                F.PID_K < -10, F.PID_K - F.PID_P < -5, F.PID_K - F.PID_MU < -10
            ),
            p_min=12000 * MeV,
            pt_min=1200 * MeV,
            mipchi2_min=60.0,
        )

    taus = make_tauons_hadronic_decay()

    comb_m_min = 1000.0 * MeV
    comb_m_max = 5000.0 * MeV
    mcorr_min = 2500.0 * MeV
    mcorr_max = 6500.0 * MeV
    vchi2pdof_max = 2.5
    bpvdira_min = 0.999
    bpvfdchi2_min = 180.0
    comb_doca_max = 0.035 * mm

    b0_rs = make_b2xulnu(
        particles=[pions, taus],
        descriptor="[B~0 -> pi+ tau-]cc",
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        bpvdira_min=bpvdira_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvfdchi2_min=bpvfdchi2_min,
        mcorr_min=mcorr_min,
        mcorr_max=mcorr_max,
    )
    b0_ws = make_b2xulnu(
        particles=[pions, taus],
        descriptor="[B~0 -> pi+ tau+]cc",
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        comb_doca_max=comb_doca_max,
        bpvdira_min=bpvdira_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvfdchi2_min=bpvfdchi2_min,
        mcorr_min=mcorr_min,
        mcorr_max=mcorr_max,
    )
    line_alg = ParticleContainersMerger([b0_rs, b0_ws])

    return line_alg


def make_lbtoptaunu_tautopipipinu(process):
    """
    Selection for the decay Lambda_b0 -> p+ tau-, hadronic tau decay.
    """
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    with make_candidate.bind(track_chisq_ndof_max=2.5, track_ghostprob_max=0.03):
        protons = make_protons_from_b(
            pid=F.require_all(
                F.PID_P > 12.0, (F.PID_P - F.PID_K) > 12.0, (F.PID_P - F.PID_MU) > 12.0
            ),
            p_min=12000 * MeV,
            pt_min=1200 * MeV,
            mipchi2_min=60,
        )

    tauons = make_tauons_hadronic_decay()

    comb_m_min = 1000.0 * MeV
    comb_m_max = 6000.0 * MeV
    mcorr_min = 2500.0 * MeV
    mcorr_max = 6500.0 * MeV
    vchi2pdof_max = 2.5
    bpvdira_min = 0.999
    bpvfdchi2_min = 150.0
    comb_doca_max = 0.055 * mm

    Lbs_rightsign = make_b2xulnu(
        particles=[protons, tauons],
        descriptor="[Lambda_b0 -> p+ tau-]cc",
        name="SLB_LbTopTauNuCombiner_RS_{hash}",
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        bpvdira_min=bpvdira_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvfdchi2_min=bpvfdchi2_min,
        comb_doca_max=comb_doca_max,
        mcorr_min=mcorr_min,
        mcorr_max=mcorr_max,
    )

    Lbs_wrongsign = make_b2xulnu(
        particles=[protons, tauons],
        descriptor="[Lambda_b0 -> p+ tau+]cc",
        name="SLB_LbTopTauNuCombiner_WS_{hash}",
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        bpvdira_min=bpvdira_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvfdchi2_min=bpvfdchi2_min,
        comb_doca_max=comb_doca_max,
        mcorr_min=mcorr_min,
        mcorr_max=mcorr_max,
    )

    line_alg = ParticleContainersMerger([Lbs_rightsign, Lbs_wrongsign])

    return line_alg
