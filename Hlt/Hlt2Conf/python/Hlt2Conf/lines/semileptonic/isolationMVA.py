###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import math
import re
from dataclasses import dataclass

import Functors as F
import Functors.math as fmath
from DaVinciTools import SubstitutePID
from Moore.monitoring import run_default_monitoring
from PyConf.Algorithms import ThOrParticleSelection
from RecoConf.algorithms_thor import (
    ParticleCombiner,
    ParticleContainersMerger,
    ParticleFilter,
)
from RecoConf.standard_particles import (
    make_has_rich_down_pions,
    make_has_rich_long_pions,
    make_has_rich_up_pions,
)
from SelAlgorithms.monitoring import histogram_1d, monitor

# Multiplying by this number will convert log_e to log_10
# we multiply by log10(e) rather than dividing by ln(10)
# since multiplication is faster
LOG10_E = math.log10(math.e)
# toggle for printing debug messages
# set to False for proper productions
DEBUG = False


@dataclass
class MVAinput:
    """Struct for storing MVA input variables"""

    name: str  # The name of the var (as per the model)
    number: int  # The number of the var
    functor: F.grammar.FunctorBase  # The functor to calculate that variable
    x_range: (float, float)  # axis range for input var monitoring histos


def mva_functor_inclusive(
    make_histos: (bool, str) = (False, ""), useNumbers: bool = False
):
    """
    Compute the output of the MVA classifier (xGBoost) for charged track isolation.
    Higher values of MVA classifier output indicate that the charged track is less isolated
    and is more likely to be associated to be coming from the same decay vertex as the B0.
    For more details: Checkout the presentation https://indico.cern.ch/event/1234758/#sc-1-4-ml-based-charged-isolat

    Args:
        make_histos (tuple): tuple of bool (do you want the input vars to be monitored) and a ParticleContainersMerger (the B*-pion combinations)
        useNumbers (bool): decision of whether to use the numbers or names of vars in the MVA functor
    """
    # get the children of the two-body combination (B0-extraparticle)
    Bstar_p_child = F.CHILD(1, F.FORWARDARGS)
    ExtraParticle_child = F.CHILD(2, F.FORWARDARGS)

    # define the input variables for mva
    mva_input_vars = []  # FunctorCollection()
    # define Impact parameter chi2 of the extra particle wrt to the BPV associated to the two-body combination (B0-extraparticle)
    mva_input_vars.append(
        MVAinput("Epi_BPV_IPCHI2", 0, F.OWNPVIPCHI2.bind(ExtraParticle_child), (0, 20))
    )  # PV
    # define PT of two body combination (B0-extraparticle). Here is transformed to be less peaky
    mva_input_vars.append(
        MVAinput(
            "Epi_PT", 1, fmath.log(F.PT.bind(ExtraParticle_child)) * LOG10_E, (1, 5)
        )
    )
    # define opening angle between B0 and extra particle. Here is transformed to be less peaky
    cos_angle = F.COSANGLE.bind(
        F.THREEMOMENTUM @ Bstar_p_child, F.THREEMOMENTUM @ ExtraParticle_child
    )
    mva_input_vars.append(
        MVAinput("Sb_Epi_COSANGLE_Lb", 2, 1.0 - fmath.pow(1.0 - cos_angle, 0.2), (0, 1))
    )
    # define DIRA of the two body combination (B0-extraparticle)
    mva_input_vars.append(
        MVAinput("Sb_BPV_DIRA_TF", 8, fmath.pow(1.0 - F.OWNPVDIRA, 0.2), (0, 1.2))
    )
    # define magnitude of PV distance i.e. distance between PV and vertex of two-body combination (B0-extraparticle)
    # NB: the sign of the magnitude deterimined by the by the difference in z-coordinate of PV and vertex of two-body combination
    mva_input_vars.append(
        MVAinput(
            "PVdis",
            3,
            (F.MAGNITUDE @ (F.ENDVERTEX_POS - F.OWNPV_POS()))
            * fmath.sign(F.END_VZ - F.OWNPVZ),
            (-50, 100),
        )
    )
    # * F.MAGNITUDE @ (F.ENDVERTEX_POS - F.OWNPV_POS())
    # define magnitude of SV distance i.e. distance between two-body combination (B0-extraparticle) vertex and the vertex
    # without the extra particle included. The sign of the magnitude is determined by the difference in z-coordinate of both vertices
    mva_input_vars.append(
        MVAinput(
            "SVdis",
            4,
            (F.MAGNITUDE @ (F.ENDVERTEX_POS.bind(Bstar_p_child) - F.ENDVERTEX_POS))
            * fmath.sign(F.END_VZ - F.END_VZ.bind(Bstar_p_child)),
            (-50, 50),
        )
    )
    # define DeltaR i.e. the difference in radius of B0 and extra particle in the rapidity-azimuth plane
    delta_eta = F.ETA.bind(Bstar_p_child) - F.ETA.bind(ExtraParticle_child)
    delta_phi = F.PHI.bind(Bstar_p_child) - F.PHI.bind(ExtraParticle_child)
    delta_phi = fmath.where(delta_phi > math.pi, delta_phi - 2 * math.pi, delta_phi)
    delta_phi = fmath.where(delta_phi < -math.pi, delta_phi + 2 * math.pi, delta_phi)
    mva_input_vars.append(
        MVAinput(
            "DeltaREpi",
            5,
            fmath.pow(
                fmath.sqrt(fmath.pow(delta_eta, 2.0) + fmath.pow(delta_phi, 2.0)), 0.2
            ),
            (0, 1.5),
        )
    )

    mva_input_vars.append(
        MVAinput("Sb_MAX_DOCACHI2", 6, fmath.log(F.MAXSDOCACHI2) * LOG10_E, (-2.6, 7.5))
    )
    mva_input_vars.append(
        MVAinput(
            "Sb_Epi_IPCHI2_WRT_LbENDVERTEX",
            7,
            fmath.log(F.IPCHI2.bind(F.ENDVERTEX @ Bstar_p_child, ExtraParticle_child))
            * LOG10_E,
            (-2, 6),
        )
    )  # SV

    # define the mva classifier
    mva = F.MVA(
        MVAType="TMVA",
        Config={
            "XMLFile": "paramfile://data/xgboost_model_cocktail_inclusive_220724.xml",
            "Name": "BDT",
        },
        Inputs={
            (f"f[{var.number}]" if useNumbers else var.name): var.functor
            for var in mva_input_vars
        },
    )
    input_monitoring = (
        make_input_monitoring(mva_input_vars, make_histos) if make_histos[0] else []
    )
    return (mva, input_monitoring)


def make_input_monitoring(mva_input_vars: [MVAinput], make_histos) -> [histogram_1d]:
    input_monitoring = []

    for var in mva_input_vars:
        input_monitoring.append(
            histogram_1d(
                functor=var.functor,
                label=var.name,
                name=f"/{make_histos[1]}/iso_{var.name}",
                title=f"isolation MVA input: {var.name}",
                bins=100,
                range=var.x_range,
            )
        )

    return input_monitoring


def mva_transform_output(xgboost_style_input: float) -> float:
    """
    We've converted our input from xgboost-style to TMVA-style but this gives a transformation on the cut variables
    This function converts from an xgboost-style cut to the corresponding TMVA-style
    taken from: https://github.com/jpata/mlglue/blob/master/mlglue/tree.py#L400-L409
    This takes the domain from [0,1] and transforms it from [-1,1] in a strange (nonlinear) way
    """
    TMVA_style_output = -math.log(1.0 / xgboost_style_input - 1.0)
    TMVA_style_output = 2.0 / (1.0 + math.exp(-2.0 * TMVA_style_output)) - 1.0

    return TMVA_style_output


def combine_iso_pions(
    BstarPlus, extra_particles, line_name, apply_fiducial_cut: bool = False
):
    """
    Make two body combination of B0 and extra particles.

    Args:
        B0 (list): TES location of B0 particles
        extra_particles (list): TES location of extra particles in the event.
          Pion id is assigned to the extra particle.

    Returns:
        list: TES location of two body combination of B0 and extra_particles
    """
    fiducial_cut = F.ALL
    if apply_fiducial_cut:
        # fiducial cut on pv distance
        pv_dist_min = -3.0  # mm
        fiducial_cut_pv_dist = (
            F.MAGNITUDE @ (F.ENDVERTEX_POS - F.OWNPV_POS())
        ) * fmath.sign(F.END_VZ - F.OWNPVZ) > pv_dist_min

        # fiducial cut on the log IPchi2 w.r.t. SV
        # See p11 : https://indico.cern.ch/event/1405184/contributions/5907467/attachments/2835541/4955107/preCuts_11_04_2024.pdf
        log_ipchi2_wrtSV_max = 5
        Bstar_p_child = F.CHILD(1, F.FORWARDARGS)
        ExtraParticle_child = F.CHILD(2, F.FORWARDARGS)
        fiducial_cut_sv_ipchi2 = (
            fmath.log(F.IPCHI2.bind(F.ENDVERTEX @ Bstar_p_child, ExtraParticle_child))
            * LOG10_E
            < log_ipchi2_wrtSV_max
        )

        fiducial_cut = F.require_all(fiducial_cut_pv_dist, fiducial_cut_sv_ipchi2)

    # first combiner: B0 pi+
    descriptor_1 = "[B*0 -> B*+ pi-]cc"
    comb_1 = ParticleCombiner(
        Inputs=[BstarPlus, extra_particles],
        name=f"SbCombiner_onetrack_1_{line_name}",
        DecayDescriptor=descriptor_1,
        CombinationCut=F.ALL,
        CompositeCut=fiducial_cut,
        ParticleCombiner="ParticleVertexFitter",  # NB: for neutrals need to use different combiner e.g. ParticleAdder
    )
    # second combiner: B0 pi-
    descriptor_2 = "[B*0 -> B*+ pi+]cc"
    comb_2 = ParticleCombiner(
        Inputs=[BstarPlus, extra_particles],
        name=f"SbCombiner_onetrack_2_{line_name}",
        DecayDescriptor=descriptor_2,
        CombinationCut=F.ALL,
        CompositeCut=fiducial_cut,
        ParticleCombiner="ParticleVertexFitter",
    )
    # merge two combiners
    comb = ParticleContainersMerger([comb_1, comb_2], name=f"bst_combiner_{line_name}")
    return comb


def get_extra_pions():
    long_pions = make_has_rich_long_pions()
    up_pions = make_has_rich_up_pions()
    down_pions = make_has_rich_down_pions()
    return ParticleContainersMerger(
        [long_pions, up_pions, down_pions], name="Pions_combiner"
    )


def extract_parent_name(line_name: str) -> (str, str):
    # First do some pruning
    # e.g. SpruceSLB_BcToJpsiTauNu_JpsiToMuMu_FakeElectron -> JpsiTauNu
    parent_string = line_name.split("_")[1].split("To")[0]
    # remove any 'Fake' stuff
    parent_string = re.sub(r"Fake.*$", "", parent_string)

    # define dicts to convert from our naming convention to
    Bcand_dict = {
        "Bu": "B+",
        "B0": "B0",
        "Bs": "B_s0",
        "Bc": "B_c+",
        "Lb": "Lambda_b0",
        "Xib0": "Xi_b0",
        "Xibminus": "Xi_b-",
        "Omegab": "Omega_b-",
    }
    anti_Bcand_dict = {
        "Bu": "B-",
        "B0": "B~0",
        "Bs": "B_s~0",
        "Bc": "B_c-",
        "Lb": "Lambda_b~0",
        "Xib0": "Xi_b~0",
        "Xibminus": "Xi_b~+",
        "Omegab": "Omega_b~+",
    }
    # throw an error if the parent is not in the dict
    if parent_string not in Bcand_dict:
        raise ValueError(
            f"ERROR: Couldn't automatically determine parent for line: {line_name}"
        )

    return Bcand_dict[parent_string], anti_Bcand_dict[parent_string]


def prepare_MVA(
    line_name: str,
    line_output: ParticleCombiner,
    *,  # All further options must be given as keyword arguments
    mva_min: float = None,
    parent: str = "",
    anti_parent: str = "",
    mva_used: str = "inclusive",
    apply_fiducial_cut: bool = True,
) -> (ThOrParticleSelection, [monitor]):
    new_parent_name = "B*+"
    anti_new_parent_name = "B*-"

    if not parent and not anti_parent:
        parent_name, anti_parent_name = extract_parent_name(line_name)
    else:
        parent_name = parent
        anti_parent_name = anti_parent

    # Yes, we really do need all those brackets
    substitutions = [
        f"{parent_name}{{{{{new_parent_name}}}}}",
        f"{anti_parent_name}{{{{{anti_new_parent_name}}}}}",
    ]
    if DEBUG:
        print(f"{substitutions = }")
    from Gaudi.Configuration import ALL, INFO

    output_level = ALL if DEBUG else INFO
    Bst_p = SubstitutePID(
        name=f"PIDSubstitute_{line_name}",
        input_particles=line_output,
        substitutions=substitutions,
        output_level=output_level,
    ).Particles

    pions = get_extra_pions()
    Bst_z = combine_iso_pions(
        Bst_p, pions, line_name, apply_fiducial_cut
    )  # vertex = B + each extra track - vertex 2

    # Only the inclusive training is being maintained now, no other models are available
    if mva_used == "inclusive":
        mva_getter = mva_functor_inclusive
        if mva_min is None:
            mva_min = 0.05  # default mu MVA cut
    else:
        raise ValueError(
            f"ERROR: in {__file__}, mva requested must be the inclusive training, no other models are currently maintainted, this has gone wrong for line: {line_name}"
        )
    MVA_functor, input_monitoring = mva_getter(make_histos=(True, line_name))
    MVA_cut = mva_transform_output(mva_min)
    MVA_cut_functor = MVA_functor > MVA_cut
    if DEBUG:
        print(f"for line {line_name: <56}\t MVA cut: {MVA_cut}")
    # We'll monitor all the combinations from the MVA, not just those that pass
    monitoring_histo = histogram_1d(
        functor=MVA_functor,
        label="MVA output",
        name=f"/{line_name}/iso_MVA",
        title=f"isolation MVA (cut at {MVA_cut} in sprucing)",
        bins=200,
        range=(-1, 1),
    )

    # However we'll only keep the subset that pass
    Bst_z_subset = ParticleFilter(
        Bst_z, F.FILTER(MVA_cut_functor), name=f"filter_isoPions_{line_name}"
    )  # B + extra track combos passing the MVA cuts

    monitoring_histos = (
        [monitor(data=Bst_z, histograms=input_monitoring + [monitoring_histo])]
        if run_default_monitoring()
        else []
    )

    extra_track_cut = F.IS_ABS_ID("pi+")
    code_extra_track = F.FILTER(extra_track_cut) @ F.GET_CHILDREN()
    return (
        ThOrParticleSelection(
            name=f"extra_track_selection_{line_name}",
            InputParticles=Bst_z_subset,
            Functor=code_extra_track,
        ).OutputSelection,
        monitoring_histos,
    )
