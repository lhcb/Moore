###############################################################################
# (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of the B0 -> hh inclusive HLT2 line"""

from GaudiKernel.SystemOfUnits import MeV

from Hlt2Conf.lines.bnoc.builders import b_builder
from Hlt2Conf.lines.bnoc.builders.basic_builder import make_tight_pions
from Hlt2Conf.lines.bnoc.utils import check_process


@check_process
def make_BdToPipPim_NoPID_Full(process):
    pions = make_tight_pions(
        pi_pidk_max=None,
        mipchi2_min=16,
        pt_min=1000 * MeV,
        p_min=1000 * MeV,
    )
    line_alg = b_builder.make_b2hh(particles=[pions, pions], descriptor="B0 -> pi+ pi-")
    return [line_alg]


@check_process
def make_BdToPipPim_NoPID(process):
    pions = make_tight_pions(
        pi_pidk_max=None,
        mipchi2_min=16,
        pt_min=1000 * MeV,
        p_min=1000 * MeV,
    )
    line_alg = b_builder.make_b2hh(particles=[pions, pions], descriptor="B0 -> pi+ pi-")
    return [line_alg]
