###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of following BNOC lines:
b-baryon -> 4p h (h)
Implemented:
B+ -> p p pbar pbar pi+
B+ -> p p pbar pbar K+
Lb0/Xib0 -> p p p pbar pbar pi-
Lb0/Xib0 -> p p p pbar pbar K-
"""

from Hlt2Conf.lines.bnoc.builders.b_builder import (
    make_bbaryon_5body,
    make_bbaryon_6body,
)
from Hlt2Conf.lines.bnoc.builders.basic_builder import (
    make_bbaryon_detached_kaons,
    make_bbaryon_detached_pions,
    make_bbaryon_detached_protons,
)
from Hlt2Conf.lines.bnoc.utils import check_process

all_lines = {}

###################
### Global cuts ###
###################

##################
### HLT2 Lines ###
##################


@check_process
def make_BuToPpPpPmPmPip(process):
    if process == "hlt2":
        pions = make_bbaryon_detached_pions()
        protons = make_bbaryon_detached_protons()
    elif process == "spruce":
        pions = make_bbaryon_detached_pions(pid=None)
        protons = make_bbaryon_detached_protons(pid=None)
    line_alg = make_bbaryon_5body(
        particles=[protons, protons, protons, protons, pions],
        descriptor="[B+ -> p+ p+ p~- p~- pi+]cc",
        bcvtx_sep_min=None,
    )
    return line_alg


@check_process
def make_BuToPpPpPmPmKp(process):
    if process == "hlt2":
        kaons = make_bbaryon_detached_kaons()
        protons = make_bbaryon_detached_protons()
    elif process == "spruce":
        kaons = make_bbaryon_detached_kaons(pid=None)
        protons = make_bbaryon_detached_protons(pid=None)
    line_alg = make_bbaryon_5body(
        particles=[protons, protons, protons, protons, kaons],
        descriptor="[B+ -> p+ p+ p~- p~- K+]cc",
        bcvtx_sep_min=None,
    )
    return line_alg


@check_process
def make_Lb0ToPpPpPpPmPmPim(process):
    if process == "hlt2":
        pions = make_bbaryon_detached_pions()
        protons = make_bbaryon_detached_protons()
    elif process == "spruce":
        pions = make_bbaryon_detached_pions(pid=None)
        protons = make_bbaryon_detached_protons(pid=None)
    line_alg = make_bbaryon_6body(
        particles=[protons, protons, protons, protons, protons, pions],
        descriptor="[Lambda_b0 -> p+ p+ p+ p~- p~- pi-]cc",
        bcvtx_sep_min=None,
    )
    return line_alg


@check_process
def make_Lb0ToPpPpPpPmPmKm(process):
    if process == "hlt2":
        kaons = make_bbaryon_detached_kaons()
        protons = make_bbaryon_detached_protons()
    elif process == "spruce":
        kaons = make_bbaryon_detached_kaons(pid=None)
        protons = make_bbaryon_detached_protons(pid=None)
    line_alg = make_bbaryon_6body(
        particles=[protons, protons, protons, protons, protons, kaons],
        descriptor="[Lambda_b0 -> p+ p+ p+ p~- p~- K-]cc",
        bcvtx_sep_min=None,
    )
    return line_alg
