# BnoC Run 3 Migration Branch

Please push all BnoC HLT2 lines to this subdirectory and open a MR to the bnoc_run3 branch.

For more information on line authoring, including line testing examples in BnoC, please refer to https://codimd.web.cern.ch/JE62QMvlRuOJEvbnILCoCQ#.
