###############################################################################
# (c) Copyright 2021-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B0(s) -> VV, V -> hh HLT2 lines.
* V: phi, kstar, rho
"""

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, picosecond
from PyConf import configurable

from Hlt2Conf.lines.bnoc.builders.b_builder import (
    make_b2Kpi,
    make_BdsToVKpKm,
    make_btovv,
)
from Hlt2Conf.lines.bnoc.builders.basic_builder import (
    make_KS_LL,
    make_merged_pi0s,
    make_omega0,
    make_omega0_fake,
    make_phi,
    make_phi_fake,
    make_rho0,
    make_rho0_fake,
    make_soft_kaons,
    make_soft_pions,
    make_tight_kaons,
    make_tight_pions,
    make_wide_kstar0,
)
from Hlt2Conf.lines.bnoc.utils import check_process


@check_process
@configurable
def make_BdsToPhiPhi(process):
    phi = make_phi()
    line_alg = make_btovv(
        particles=[phi, phi],
        descriptor="B_s0 -> phi(1020) phi(1020)",
        asumpt_min=None,
        ptproduct_min=1.5 * GeV * GeV,
        adoca12_max=None,
        ltime_min=0.2 * picosecond,
        bpvfdchi2_min=None,
        bpvdira_min=None,
    )
    return [phi, line_alg]


@check_process
@configurable
def make_BdsToKstzKstzb(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        vchi2pdof_max=9,
        adoca12_max=0.15 * mm,
        motherpt_min=900 * MeV,
        mipchi2_min=20.0,
    )
    line_alg = make_btovv(
        particles=[kstar, kstar],
        descriptor="B_s0 -> K*(892)0 K*(892)~0",
        asumpt_min=4000.0 * MeV,
        adoca12_max=0.08 * mm,
        vchi2pdof_max=9,
        mipchi2_max=16,
        bpvfdchi2_min=81,
    )
    return [kstar, line_alg]


kstzphi_kstar_selection = {
    "am_max": 1800 * MeV,
    "adoca12_max": 0.1 * mm,
    "asumpt_min": 500 * MeV,
}
kstzphi_phi_selection = {
    "k_pidk_min": -5.0,
    "adoca12_max": 0.15 * mm,
    "k_p_min": 1.5 * GeV,
    "k_pt_min": 500 * MeV,
    "asumpt_min": 900 * MeV,
    "vchi2pdof_max": 15,
}
kstzphi_b_selection = {
    "asumpt_min": 2000 * MeV,
    "adoca12_max": 0.15 * mm,
    "vchi2pdof_max": 12,
    "mipchi2_max": 25,
}


@check_process
@configurable
def make_BdsToKstzPhi(process):
    kstar = make_wide_kstar0(
        make_pions=make_tight_pions,
        make_kaons=make_tight_kaons,
        **kstzphi_kstar_selection,
    )
    phi = make_phi(make_kaons=make_tight_kaons, **kstzphi_phi_selection)
    line_alg = make_btovv(
        particles=[phi, kstar],
        descriptor="[B0 -> phi(1020) K*(892)0]cc",
        **kstzphi_b_selection,
    )
    return [kstar, phi, line_alg]


@check_process
@configurable
def make_BdsToKstzPhi_FakeKstPi(process):
    kstar = make_wide_kstar0(
        make_pions=make_tight_pions,
        make_kaons=make_tight_kaons,
        **kstzphi_kstar_selection,
        invert_pi_pid=True,
    )
    phi = make_phi(make_kaons=make_tight_kaons, **kstzphi_phi_selection)
    line_alg = make_btovv(
        particles=[phi, kstar],
        descriptor="[B0 -> phi(1020) K*(892)0]cc",
        **kstzphi_b_selection,
    )
    return [kstar, phi, line_alg]


@check_process
@configurable
def make_BdsToKstzPhi_FakeKstK(process):
    kstar = make_wide_kstar0(
        make_pions=make_tight_pions,
        make_kaons=make_tight_kaons,
        **kstzphi_kstar_selection,
        invert_k_pid=True,
    )
    phi = make_phi(make_kaons=make_tight_kaons, **kstzphi_phi_selection)
    line_alg = make_btovv(
        particles=[phi, kstar],
        descriptor="[B0 -> phi(1020) K*(892)0]cc",
        **kstzphi_b_selection,
    )
    return [kstar, phi, line_alg]


@check_process
@configurable
def make_BdsToKstzPhi_FakeKstDouble(process):
    kstar = make_wide_kstar0(
        make_pions=make_tight_pions,
        make_kaons=make_tight_kaons,
        **kstzphi_kstar_selection,
        invert_pi_pid=True,
        invert_k_pid=True,
    )
    phi = make_phi(make_kaons=make_tight_kaons, **kstzphi_phi_selection)
    line_alg = make_btovv(
        particles=[phi, kstar],
        descriptor="[B0 -> phi(1020) K*(892)0]cc",
        **kstzphi_b_selection,
    )
    return [kstar, phi, line_alg]


@check_process
@configurable
def make_BdsToKstzPhi_FakePhiK(process):
    kstar = make_wide_kstar0(
        make_pions=make_tight_pions,
        make_kaons=make_tight_kaons,
        **kstzphi_kstar_selection,
    )
    phi = make_phi_fake(make_kaons=make_tight_kaons, **kstzphi_phi_selection)
    line_alg = make_btovv(
        particles=[phi, kstar],
        descriptor="[B0 -> phi(1020) K*(892)0]cc",
        **kstzphi_b_selection,
    )
    return [kstar, phi, line_alg]


@check_process
@configurable
def make_BdsToKstzPhi_FakePhiDouble(process):
    kstar = make_wide_kstar0(
        make_pions=make_tight_pions,
        make_kaons=make_tight_kaons,
        **kstzphi_kstar_selection,
    )
    phi = make_phi(
        make_kaons=make_tight_kaons, **kstzphi_phi_selection, invert_pid=True
    )
    line_alg = make_btovv(
        particles=[phi, kstar],
        descriptor="[B0 -> phi(1020) K*(892)0]cc",
        **kstzphi_b_selection,
    )
    return [kstar, phi, line_alg]


kstzrho_kstar_selection = {
    "pi_pidk": 2.0,
    "pi_pt_min": 500 * MeV,
    "k_pidk_min": -3.0,
    "k_pt_min": 500 * MeV,
    "adoca12_max": 0.3 * mm,
    "bpvfdchi2_min": 16,
    "vchi2pdof_max": 9,
    "motherpt_min": 1200 * MeV,
    "mipchi2_min": 36.0,
    "am_min": 590 * MeV,
    "am_max": 1200 * MeV,
}
kstzrho_rho_selection = {
    "pi_pidk_max": 2.0,
    "pi_p_min": 1.5 * GeV,
    "pi_pt_min": 250.0 * MeV,
    "am_min": 280 * MeV,
    "am_max": 1300 * MeV,
    "adoca12_max": 0.2 * mm,
    "bpvfdchi2_min": 16,
    "vchi2pdof_max": 12,
    "asumpt_min": 1250 * MeV,
    "motherpt_min": 1250 * MeV,
    "mipchi2_min": 32.0,
}
kstzrho_b_selection = {
    "asumpt_min": 5000.0 * MeV,
    "adoca12_max": 0.15 * mm,
    "vchi2pdof_max": 12,
    "mipchi2_max": 25,
    "bpvfdchi2_min": 36,
}


@check_process
@configurable
def make_BdsToKstzRho(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        **kstzrho_kstar_selection,
    )
    rho = make_rho0(make_pions=make_soft_pions, **kstzrho_rho_selection)
    line_alg = make_btovv(
        particles=[kstar, rho],
        descriptor="[B0 -> K*(892)0 rho(770)0]cc",
        **kstzrho_b_selection,
    )
    return [kstar, rho, line_alg]


@check_process
@configurable
def make_BdsToKstzRho_FakeKstPi(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        **kstzrho_kstar_selection,
        invert_pi_pid=True,
    )
    rho = make_rho0(make_pions=make_soft_pions, **kstzrho_rho_selection)
    line_alg = make_btovv(
        particles=[kstar, rho],
        descriptor="[B0 -> K*(892)0 rho(770)0]cc",
        **kstzrho_b_selection,
    )
    return [kstar, rho, line_alg]


@check_process
@configurable
def make_BdsToKstzRho_FakeKstK(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        **kstzrho_kstar_selection,
        invert_k_pid=True,
    )
    rho = make_rho0(make_pions=make_soft_pions, **kstzrho_rho_selection)
    line_alg = make_btovv(
        particles=[kstar, rho],
        descriptor="[B0 -> K*(892)0 rho(770)0]cc",
        **kstzrho_b_selection,
    )
    return [kstar, rho, line_alg]


@check_process
@configurable
def make_BdsToKstzRho_FakeKstDouble(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        **kstzrho_kstar_selection,
        invert_pi_pid=True,
        invert_k_pid=True,
    )
    rho = make_rho0(make_pions=make_soft_pions, **kstzrho_rho_selection)
    line_alg = make_btovv(
        particles=[kstar, rho],
        descriptor="[B0 -> K*(892)0 rho(770)0]cc",
        **kstzrho_b_selection,
    )
    return [kstar, rho, line_alg]


@check_process
@configurable
def make_BdsToKstzRho_FakeRhoPi(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        **kstzrho_kstar_selection,
    )
    rho = make_rho0_fake(make_pions=make_soft_pions, **kstzrho_rho_selection)
    line_alg = make_btovv(
        particles=[kstar, rho],
        descriptor="[B0 -> K*(892)0 rho(770)0]cc",
        **kstzrho_b_selection,
    )
    return [kstar, rho, line_alg]


@check_process
@configurable
def make_BdsToKstzRho_FakeRhoDouble(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        **kstzrho_kstar_selection,
    )
    rho = make_rho0(
        make_pions=make_soft_pions, **kstzrho_rho_selection, invert_pid=True
    )
    line_alg = make_btovv(
        particles=[kstar, rho],
        descriptor="[B0 -> K*(892)0 rho(770)0]cc",
        **kstzrho_b_selection,
    )
    return [kstar, rho, line_alg]


kstzomegares_kstar_selection = {
    "pi_pidk": 2.0,
    "pi_pt_min": 500 * MeV,
    "k_pidk_min": -3.0,
    "k_pt_min": 500 * MeV,
    "adoca12_max": 0.3 * mm,
    "bpvfdchi2_min": 16,
    "vchi2pdof_max": 9,
    "motherpt_min": 1200 * MeV,
    "mipchi2_min": 36.0,
    "am_min": 590 * MeV,
    "am_max": 1200 * MeV,
}
kstzomegares_omega_selection = {
    "pi_pidk_max": 2.0,
    "pi_p_min": 1.0 * GeV,
    "pi_pt_min": 250.0 * MeV,
    "am_min": 580 * MeV,
    "am_max": 980 * MeV,
    "adoca12_max": 0.3 * mm,
    "bpvfdchi2_min": 16,
    "vchi2pdof_max": 7.5,
    "asumpt_min": 500 * MeV,
    "motherpt_min": 500 * MeV,
    "mipchi2_min": 16.0,
}
kstzomegares_b_selection = {
    "asumpt_min": 2500.0 * MeV,
    "adoca12_max": 0.15 * mm,
    "vchi2pdof_max": 12,
    "mipchi2_max": 25,
    "bpvfdchi2_min": 36,
}


@check_process
@configurable
def make_BdsToKstzOmegaResolved(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        **kstzomegares_kstar_selection,
    )
    omega = make_omega0(make_pions=make_soft_pions, **kstzomegares_omega_selection)
    line_alg = make_btovv(
        descriptor="[B0 -> K*(892)0 omega(782)]cc",
        particles=[kstar, omega],
        **kstzomegares_b_selection,
    )
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaResolved_FakeKstPi(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        **kstzomegares_kstar_selection,
        invert_pi_pid=True,
    )
    omega = make_omega0(make_pions=make_soft_pions, **kstzomegares_omega_selection)
    line_alg = make_btovv(
        descriptor="[B0 -> K*(892)0 omega(782)]cc",
        particles=[kstar, omega],
        **kstzomegares_b_selection,
    )
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaResolved_FakeKstK(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        **kstzomegares_kstar_selection,
        invert_k_pid=True,
    )
    omega = make_omega0(make_pions=make_soft_pions, **kstzomegares_omega_selection)
    line_alg = make_btovv(
        descriptor="[B0 -> K*(892)0 omega(782)]cc",
        particles=[kstar, omega],
        **kstzomegares_b_selection,
    )
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaResolved_FakeKstDouble(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        **kstzomegares_kstar_selection,
        invert_pi_pid=True,
        invert_k_pid=True,
    )
    omega = make_omega0(make_pions=make_soft_pions, **kstzomegares_omega_selection)
    line_alg = make_btovv(
        descriptor="[B0 -> K*(892)0 omega(782)]cc",
        particles=[kstar, omega],
        **kstzomegares_b_selection,
    )
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaResolved_FakeOmegaPi(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        **kstzomegares_kstar_selection,
    )
    omega = make_omega0_fake(make_pions=make_soft_pions, **kstzomegares_omega_selection)
    line_alg = make_btovv(
        descriptor="[B0 -> K*(892)0 omega(782)]cc",
        particles=[kstar, omega],
        **kstzomegares_b_selection,
    )
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaResolved_FakeOmegaDouble(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        **kstzomegares_kstar_selection,
    )
    omega = make_omega0(
        make_pions=make_soft_pions, **kstzomegares_omega_selection, invert_pid=True
    )
    line_alg = make_btovv(
        descriptor="[B0 -> K*(892)0 omega(782)]cc",
        particles=[kstar, omega],
        **kstzomegares_b_selection,
    )
    return [kstar, omega, line_alg]


kstomegamerged_kstar_selection = {
    "pi_pidk": 3.0,
    "pi_pt_min": 500 * MeV,
    "k_pidk_min": -3.0,
    "k_pt_min": 500 * MeV,
    "adoca12_max": 0.3 * mm,
    "bpvfdchi2_min": 16,
    "vchi2pdof_max": 9,
    "motherpt_min": 1200 * MeV,
    "mipchi2_min": 36.0,
}
kstomegamerged_omega_selection = {
    "pi_pidk_max": 0.0,
    "pi_p_min": 1.0 * GeV,
    "pi_pt_min": 250.0 * MeV,
    "am_min": 580 * MeV,
    "am_max": 980 * MeV,
    "adoca12_max": 0.3 * mm,
    "bpvfdchi2_min": 16,
    "vchi2pdof_max": 7.5,
    "asumpt_min": 500 * MeV,
    "motherpt_min": 500 * MeV,
    "mipchi2_min": 36.0,
}
kstomegamerged_b_selection = {
    "asumpt_min": 2500.0 * MeV,
    "adoca12_max": 0.15 * mm,
    "vchi2pdof_max": 12,
    "mipchi2_max": 25,
    "bpvfdchi2_min": 36,
}


@check_process
@configurable
def make_BdsToKstzOmegaMerged(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        **kstomegamerged_kstar_selection,
    )
    omega = make_omega0(
        make_pions=make_soft_pions,
        make_pi0s=make_merged_pi0s,
        **kstomegamerged_omega_selection,
    )
    line_alg = make_btovv(
        particles=[kstar, omega],
        descriptor="[B0 -> K*(892)0 omega(782)]cc",
        **kstomegamerged_b_selection,
    )
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaMerged_FakeKstPi(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        **kstomegamerged_kstar_selection,
        invert_pi_pid=True,
    )
    omega = make_omega0(
        make_pions=make_soft_pions,
        make_pi0s=make_merged_pi0s,
        **kstomegamerged_omega_selection,
    )
    line_alg = make_btovv(
        particles=[kstar, omega],
        descriptor="[B0 -> K*(892)0 omega(782)]cc",
        **kstomegamerged_b_selection,
    )
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaMerged_FakeKstK(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        **kstomegamerged_kstar_selection,
        invert_k_pid=True,
    )
    omega = make_omega0(
        make_pions=make_soft_pions,
        make_pi0s=make_merged_pi0s,
        **kstomegamerged_omega_selection,
    )
    line_alg = make_btovv(
        particles=[kstar, omega],
        descriptor="[B0 -> K*(892)0 omega(782)]cc",
        **kstomegamerged_b_selection,
    )
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaMerged_FakeKstDouble(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        **kstomegamerged_kstar_selection,
        invert_pi_pid=True,
        invert_k_pid=True,
    )
    omega = make_omega0(
        make_pions=make_soft_pions,
        make_pi0s=make_merged_pi0s,
        **kstomegamerged_omega_selection,
    )
    line_alg = make_btovv(
        particles=[kstar, omega],
        descriptor="[B0 -> K*(892)0 omega(782)]cc",
        **kstomegamerged_b_selection,
    )
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaMerged_FakeOmegaPi(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        **kstomegamerged_kstar_selection,
    )
    omega = make_omega0_fake(
        make_pions=make_soft_pions,
        make_pi0s=make_merged_pi0s,
        **kstomegamerged_omega_selection,
    )
    line_alg = make_btovv(
        particles=[kstar, omega],
        descriptor="[B0 -> K*(892)0 omega(782)]cc",
        **kstomegamerged_b_selection,
    )
    return [kstar, omega, line_alg]


@check_process
@configurable
def make_BdsToKstzOmegaMerged_FakeOmegaDouble(process):
    kstar = make_wide_kstar0(
        make_kaons=make_soft_kaons,
        make_pions=make_soft_pions,
        **kstomegamerged_kstar_selection,
    )
    omega = make_omega0(
        make_pions=make_soft_pions,
        make_pi0s=make_merged_pi0s,
        **kstomegamerged_omega_selection,
        invert_pid=True,
    )
    line_alg = make_btovv(
        particles=[kstar, omega],
        descriptor="[B0 -> K*(892)0 omega(782)]cc",
        **kstomegamerged_b_selection,
    )
    return [kstar, omega, line_alg]


phirho_phi_selection = {
    "k_pidk_min": -5.0,
    "adoca12_max": 0.12 * mm,
    "asumpt_min": 1100 * MeV,
    "bpvfdchi2_min": 11,
    "vchi2pdof_max": 9,
    "mipchi2_min": 32.0,
}
phirho_rho_selection = {
    "pi_pidk_max": 5.0,
    "adoca12_max": 0.12 * mm,
    "bpvfdchi2_min": 20,
    "vchi2pdof_max": 15,
    "mipchi2_min": 32.0,
}
phirho_b_selection = {
    "asumpt_min": 2000.0 * MeV,
    "ptproduct_min": 2.5 * GeV * GeV,
    "adoca12_max": 0.09 * mm,
    "vchi2pdof_max": 12,
    "mipchi2_max": 25,
    "bpvfdchi2_min": 36,
}


@check_process
@configurable
def make_BdsToPhiRho(process):
    phi = make_phi(make_kaons=make_soft_kaons, **phirho_phi_selection)
    rho = make_rho0(make_pions=make_soft_pions, **phirho_rho_selection)
    line_alg = make_btovv(
        descriptor="B_s0 -> phi(1020) rho(770)0",
        particles=[phi, rho],
        **phirho_b_selection,
    )
    return [phi, rho, line_alg]


@check_process
@configurable
def make_BdsToPhiRho_FakePhiK(process):
    phi = make_phi_fake(make_kaons=make_soft_kaons, **phirho_phi_selection)
    rho = make_rho0(make_pions=make_soft_pions, **phirho_rho_selection)
    line_alg = make_btovv(
        descriptor="B_s0 -> phi(1020) rho(770)0",
        particles=[phi, rho],
        **phirho_b_selection,
    )
    return [phi, rho, line_alg]


@check_process
@configurable
def make_BdsToPhiRho_FakePhiDouble(process):
    phi = make_phi(make_kaons=make_soft_kaons, **phirho_phi_selection, invert_pid=True)
    rho = make_rho0(make_pions=make_soft_pions, **phirho_rho_selection)
    line_alg = make_btovv(
        descriptor="B_s0 -> phi(1020) rho(770)0",
        particles=[phi, rho],
        **phirho_b_selection,
    )
    return [phi, rho, line_alg]


@check_process
@configurable
def make_BdsToPhiRho_FakeRhoPi(process):
    phi = make_phi(make_kaons=make_soft_kaons, **phirho_phi_selection)
    rho = make_rho0_fake(make_pions=make_soft_pions, **phirho_rho_selection)
    line_alg = make_btovv(
        descriptor="B_s0 -> phi(1020) rho(770)0",
        particles=[phi, rho],
        **phirho_b_selection,
    )
    return [phi, rho, line_alg]


@check_process
@configurable
def make_BdsToPhiRho_FakeRhoDouble(process):
    phi = make_phi(make_kaons=make_soft_kaons, **phirho_phi_selection)
    rho = make_rho0(make_pions=make_soft_pions, **phirho_rho_selection, invert_pid=True)
    line_alg = make_btovv(
        descriptor="B_s0 -> phi(1020) rho(770)0",
        particles=[phi, rho],
        **phirho_b_selection,
    )
    return [phi, rho, line_alg]


phiomegares_phi_selection = {
    "k_pidk_min": -5.0,
    "adoca12_max": 0.12 * mm,
    "asumpt_min": 1100 * MeV,
    "bpvfdchi2_min": 11,
    "vchi2pdof_max": 9,
    "mipchi2_min": 32.0,
}
phiomegares_omega_selection = {
    "pi_pidk_max": 0.0,
    "pi_p_min": 1.0 * GeV,
    "pi_pt_min": 250.0 * MeV,
    "am_min": 700 * MeV,
    "am_max": 860 * MeV,
    "adoca12_max": 0.3 * mm,
    "bpvfdchi2_min": 16,
    "vchi2pdof_max": 7.5,
    "asumpt_min": 500 * MeV,
    "motherpt_min": 500 * MeV,
    "mipchi2_min": 36.0,
}
phiomegares_b_selection = {
    "asumpt_min": 2000.0 * MeV,
    "ptproduct_min": 2.5 * GeV * GeV,
    "adoca12_max": 0.09 * mm,
    "vchi2pdof_max": 12,
    "mipchi2_max": 25,
    "bpvfdchi2_min": 36,
}


@check_process
@configurable
def make_BdsToPhiOmegaResolved(process):
    phi = make_phi(make_kaons=make_soft_kaons, **phiomegares_phi_selection)
    omega = make_omega0(make_pions=make_soft_pions, **phiomegares_omega_selection)
    line_alg = make_btovv(
        descriptor="B_s0 -> phi(1020) omega(782)",
        particles=[phi, omega],
        **phiomegares_b_selection,
    )
    return [phi, omega, line_alg]


@check_process
@configurable
def make_BdsToPhiOmegaResolved_FakePhiK(process):
    phi = make_phi_fake(make_kaons=make_soft_kaons, **phiomegares_phi_selection)
    omega = make_omega0(make_pions=make_soft_pions, **phiomegares_omega_selection)
    line_alg = make_btovv(
        descriptor="B_s0 -> phi(1020) omega(782)",
        particles=[phi, omega],
        **phiomegares_b_selection,
    )
    return [phi, omega, line_alg]


@check_process
@configurable
def make_BdsToPhiOmegaResolved_FakePhiDouble(process):
    phi = make_phi(
        make_kaons=make_soft_kaons, **phiomegares_phi_selection, invert_pid=True
    )
    omega = make_omega0(make_pions=make_soft_pions, **phiomegares_omega_selection)
    line_alg = make_btovv(
        descriptor="B_s0 -> phi(1020) omega(782)",
        particles=[phi, omega],
        **phiomegares_b_selection,
    )
    return [phi, omega, line_alg]


@check_process
@configurable
def make_BdsToPhiOmegaResolved_FakeOmegaPi(process):
    phi = make_phi(make_kaons=make_soft_kaons, **phiomegares_phi_selection)
    omega = make_omega0_fake(make_pions=make_soft_pions, **phiomegares_omega_selection)
    line_alg = make_btovv(
        descriptor="B_s0 -> phi(1020) omega(782)",
        particles=[phi, omega],
        **phiomegares_b_selection,
    )
    return [phi, omega, line_alg]


@check_process
@configurable
def make_BdsToPhiOmegaResolved_FakeOmegaDouble(process):
    phi = make_phi(make_kaons=make_soft_kaons, **phiomegares_phi_selection)
    omega = make_omega0(
        make_pions=make_soft_pions, **phiomegares_omega_selection, invert_pid=True
    )
    line_alg = make_btovv(
        descriptor="B_s0 -> phi(1020) omega(782)",
        particles=[phi, omega],
        **phiomegares_b_selection,
    )
    return [phi, omega, line_alg]


phiomegamerged_phi_selection = {
    "k_pidk_min": -5.0,
    "adoca12_max": 0.12 * mm,
    "asumpt_min": 1100 * MeV,
    "bpvfdchi2_min": 11,
    "vchi2pdof_max": 9,
    "mipchi2_min": 32.0,
}
phiomegamerged_omega_selection = {
    "pi_pidk_max": 0.0,
    "pi_p_min": 1.0 * GeV,
    "pi_pt_min": 250.0 * MeV,
    "am_min": 700 * MeV,
    "am_max": 860 * MeV,
    "adoca12_max": 0.3 * mm,
    "bpvfdchi2_min": 16,
    "vchi2pdof_max": 7.5,
    "asumpt_min": 500 * MeV,
    "motherpt_min": 500 * MeV,
    "mipchi2_min": 36.0,
}
phiomegamerged_b_selection = {
    "asumpt_min": 2000.0 * MeV,
    "ptproduct_min": 2.5 * GeV * GeV,
    "adoca12_max": 0.09 * mm,
    "vchi2pdof_max": 12,
    "mipchi2_max": 25,
    "bpvfdchi2_min": 36,
}


@check_process
@configurable
def make_BdsToPhiOmegaMerged(process):
    phi = make_phi(make_kaons=make_soft_kaons, **phiomegamerged_phi_selection)
    omega = make_omega0(
        make_pions=make_soft_pions,
        make_pi0s=make_merged_pi0s,
        **phiomegamerged_omega_selection,
    )
    line_alg = make_btovv(
        descriptor="B_s0 -> phi(1020) omega(782)",
        particles=[phi, omega],
        **phiomegamerged_b_selection,
    )
    return [phi, omega, line_alg]


@check_process
@configurable
def make_BdsToPhiOmegaMerged_FakePhiK(process):
    phi = make_phi_fake(make_kaons=make_soft_kaons, **phiomegamerged_phi_selection)
    omega = make_omega0(
        make_pions=make_soft_pions,
        make_pi0s=make_merged_pi0s,
        **phiomegamerged_omega_selection,
    )
    line_alg = make_btovv(
        descriptor="B_s0 -> phi(1020) omega(782)",
        particles=[phi, omega],
        **phiomegamerged_b_selection,
    )
    return [phi, omega, line_alg]


@check_process
@configurable
def make_BdsToPhiOmegaMerged_FakePhiDouble(process):
    phi = make_phi(
        make_kaons=make_soft_kaons, **phiomegamerged_phi_selection, invert_pid=True
    )
    omega = make_omega0(
        make_pions=make_soft_pions,
        make_pi0s=make_merged_pi0s,
        **phiomegamerged_omega_selection,
    )
    line_alg = make_btovv(
        descriptor="B_s0 -> phi(1020) omega(782)",
        particles=[phi, omega],
        **phiomegamerged_b_selection,
    )
    return [phi, omega, line_alg]


@check_process
@configurable
def make_BdsToPhiOmegaMerged_FakeOmegaPi(process):
    phi = make_phi(make_kaons=make_soft_kaons, **phiomegamerged_phi_selection)
    omega = make_omega0_fake(
        make_pions=make_soft_pions,
        make_pi0s=make_merged_pi0s,
        **phiomegamerged_omega_selection,
    )
    line_alg = make_btovv(
        descriptor="B_s0 -> phi(1020) omega(782)",
        particles=[phi, omega],
        **phiomegamerged_b_selection,
    )
    return [phi, omega, line_alg]


@check_process
@configurable
def make_BdsToPhiOmegaMerged_FakeOmegaDouble(process):
    phi = make_phi(make_kaons=make_soft_kaons, **phiomegamerged_phi_selection)
    omega = make_omega0(
        make_pions=make_soft_pions,
        make_pi0s=make_merged_pi0s,
        **phiomegamerged_omega_selection,
        invert_pid=True,
    )
    line_alg = make_btovv(
        descriptor="B_s0 -> phi(1020) omega(782)",
        particles=[phi, omega],
        **phiomegamerged_b_selection,
    )
    return [phi, omega, line_alg]


@check_process
@configurable
def make_BdsToRhoRho(process):
    rho = make_rho0(
        make_pions=make_soft_pions,
        pi_pidk_max=0.0,
        pi_p_min=1500 * MeV,
        pi_pt_min=250.0 * MeV,
        am_min=280 * MeV,
        am_max=1600 * MeV,
        adoca12_max=0.2 * mm,
        bpvfdchi2_min=16,
        vchi2pdof_max=7.5,
        asump_min=800 * MeV,
        asumpt_min=1200 * MeV,
        motherpt_min=1200 * MeV,
        mipchi2_min=36.0,
    )
    line_alg = make_btovv(
        particles=[rho, rho],
        descriptor="B0 -> rho(770)0 rho(770)0",
        asumpt_min=4000.0 * MeV,
        ptproduct_min=4.0 * GeV * GeV,
        adoca12_max=0.09 * mm,
        vchi2pdof_max=7,
        mipchi2_max=13,
        bpvfdchi2_min=81,
    )
    return [rho, line_alg]


@check_process
@configurable
def make_BdsToKSPi0_LL(process):
    ks = make_KS_LL(
        pi_p_min=0 * GeV,
        p_min=8000.0 * MeV,
        pt_min=500.0 * MeV,
        vchi2_max=10.0,
        bpvfdchi2_min=100.0,
        mipchi2_min=100.0,
    )
    pions = make_merged_pi0s(p_min=5000 * MeV, pt_min=3500 * MeV)

    line_alg = make_b2Kpi(
        particles=[ks, pions],
        descriptor="B0 -> KS0 pi0",
        comb_m_min=4000 * MeV,
        comb_m_max=6200 * MeV,
        comb_pt_min=5000 * MeV,
        mtdocachi2_max=8.0,
        pt_min=4000 * MeV,
    )
    return [ks, line_alg]


@check_process
@configurable
def make_BdsToPhiKpKm(process):
    phi = make_phi(am_min=995 * MeV, am_max=1045 * MeV, k_pt_min=500 * MeV)
    kaons = make_soft_kaons(k_pidk_min=0.0, pt_min=500 * MeV)
    line_alg = make_BdsToVKpKm(
        particles=[phi, kaons, kaons], descriptor="B_s0 -> phi(1020) K+ K-"
    )
    return [phi, line_alg]


@check_process
@configurable
def make_BdsToKstzKpKm(process):
    kstar = make_wide_kstar0(
        make_pions=make_tight_pions,
        make_kaons=make_tight_kaons,
        k_pidk_min=0.0,
        am_max=1800 * MeV,
        asumpt_min=500 * MeV,
    )
    kaons = make_soft_kaons(k_pidk_min=0.0, pt_min=500 * MeV)
    line_alg = make_BdsToVKpKm(
        particles=[kstar, kaons, kaons], descriptor="[B0 -> K*(892)0 K+ K-]cc"
    )
    return [kstar, line_alg]
