###############################################################################
# (c) Copyright 2021-2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of B0(s) -> pi0pi0 lines."""

from GaudiKernel.SystemOfUnits import MeV
from PyConf import configurable

from Hlt2Conf.lines.bnoc.builders.b_builder import make_b2hh
from Hlt2Conf.lines.bnoc.builders.basic_builder import (
    make_dalitz_pi0s,
    make_resolved_pi0s,
    make_tight_merged_pi0s,
    make_tight_photons,
)

pi0_merged_cut = {"p_min": 1000 * MeV}

pi0_resolved_cut = {"pion_mass_min": 115 * MeV, "pion_mass_max": 175 * MeV}

pi0_dalitz_cut = {
    "p_min": 1000 * MeV,
    "pt_min": 300 * MeV,
    "gamma_is_not_H": 0.05,
    "gamma_is_photon": 0.2,
    "min_ipchi2": 16,
}


@configurable
def make_BdsToPi0Pi0_merged(process):
    dalitz_pi0s = make_dalitz_pi0s(**pi0_dalitz_cut)
    pi0s = make_tight_merged_pi0s(**pi0_merged_cut)
    line_alg = make_b2hh(
        particles=[dalitz_pi0s, pi0s],
        descriptor="B0 -> pi0 pi0",
        sum_pt=3000 * MeV,
        fdchi2_min=None,
        AllowDiffInputsForSameIDChildren=True,
    )
    return line_alg


@configurable
def make_BdsToPi0Pi0_resolved(process):
    dalitz_pi0s = make_dalitz_pi0s(**pi0_dalitz_cut)
    pi0s = make_resolved_pi0s(gamma=make_tight_photons, **pi0_resolved_cut)
    line_alg = make_b2hh(
        particles=[dalitz_pi0s, pi0s],
        descriptor="B0 -> pi0 pi0",
        sum_pt=3000 * MeV,
        fdchi2_min=None,
        AllowDiffInputsForSameIDChildren=True,
    )
    return line_alg
