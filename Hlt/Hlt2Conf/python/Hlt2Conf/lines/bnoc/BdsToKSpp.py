###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
# This software is distributed under the terms of the GNU General Public      #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of B0(s) -> KSpp inclusive HLT2 lines."""

from Hlt2Conf.lines.bnoc.builders.b_builder import make_b2kspp
from Hlt2Conf.lines.bnoc.builders.basic_builder import (
    make_bbaryon_detached_protons,
    make_bbaryon_ks0_dd,
    make_bbaryon_ks0_ll,
)
from Hlt2Conf.lines.bnoc.utils import check_process


@check_process
def make_BdsToPpPmKS_LL(process):
    if process == "spruce":
        protons = make_bbaryon_detached_protons(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_detached_protons()
    ks = make_bbaryon_ks0_ll()
    line_alg = make_b2kspp(
        particles=[protons, protons, ks], descriptor="B0 -> p+ p~- KS0"
    )  ## ks more than protons
    return [ks, line_alg]


@check_process
def make_BdsToPpPmKS_DD(process):
    if process == "spruce":
        protons = make_bbaryon_detached_protons(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_detached_protons()
    ks = make_bbaryon_ks0_dd()
    line_alg = make_b2kspp(
        particles=[protons, protons, ks], descriptor="B0 -> p+ p~- KS0"
    )  ## ks more than protons
    return [ks, line_alg]
