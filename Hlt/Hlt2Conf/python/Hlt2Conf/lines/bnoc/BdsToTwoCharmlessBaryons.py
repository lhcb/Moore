###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of B0(s) -> Lambda0 Lambda~0,
              B0(s) -> Xi~+ Xi-,
              B0(s) -> Xi~0 Xi0,
              B0(s) -> Omega~+ Omega- HLT2 lines.
"""

from GaudiKernel.SystemOfUnits import mm
from PyConf import configurable
from RecoConf.standard_particles import make_down_kaons, make_resolved_pi0s

from Hlt2Conf.lines.bnoc.builders.b_builder import make_BdsToTwoCharmlessBaryons
from Hlt2Conf.lines.bnoc.builders.basic_builder import (
    make_down_pions,
    make_kaons,
    make_loose_lambda_DD,
    make_omegam_to_lambda_k_ddd,
    make_omegam_to_lambda_k_ddl,
    make_omegam_to_lambda_k_lll,
    make_pions,
    make_veryloose_lambda_LL,
    make_xi0,
    make_xim_to_lambda_pi_ddd,
    make_xim_to_lambda_pi_ddl,
    make_xim_to_lambda_pi_lll,
)
from Hlt2Conf.lines.bnoc.utils import check_process


@check_process
@configurable
def make_BdsToL0L0bar_LLLL(process):
    lambdall = make_veryloose_lambda_LL()
    line_alg = make_BdsToTwoCharmlessBaryons(
        particle1=lambdall,
        particle2=lambdall,
        descriptor="B_s0 -> Lambda0 Lambda~0",
        name="BdsToL0L0bar_LLLL",
        bcvtx_sep_min=None,
        VCHI2PDOF_Max=25,
        BPVIPCHI2_Max=35,
    )
    return [lambdall, line_alg]


@check_process
@configurable
def make_BdsToL0L0bar_DDDD(process):
    lambdadd = make_loose_lambda_DD()
    line_alg = make_BdsToTwoCharmlessBaryons(
        particle1=lambdadd,
        particle2=lambdadd,
        descriptor="B_s0 -> Lambda0 Lambda~0",
        name="BdsToL0L0bar_DDDD",
        bcvtx_sep_min=None,
        VCHI2PDOF_Max=25,
        BPVIPCHI2_Max=35,
    )
    return [lambdadd, line_alg]


@check_process
@configurable
def make_BdsToL0L0bar_LLDD(process):
    lambdall = make_veryloose_lambda_LL()
    lambdadd = make_loose_lambda_DD()
    line_alg = make_BdsToTwoCharmlessBaryons(
        particle1=lambdall,
        particle2=lambdadd,
        descriptor="B_s0 -> Lambda0 Lambda~0",
        name="BdsToL0L0bar_LLDD",
        bcvtx_sep_min=None,
        VCHI2PDOF_Max=25,
        BPVIPCHI2_Max=35,
    )
    return [lambdall, lambdadd, line_alg]


@check_process
@configurable
def make_BdsToXipXim_LLLLLL(process):
    Xi_LLL = make_xim_to_lambda_pi_lll(
        lambdas=make_veryloose_lambda_LL(),
        pions=make_pions(),
        vchi2pdof_max=50.0,
        bpvvdz_min=0.0 * mm,
    )
    line_alg = make_BdsToTwoCharmlessBaryons(
        particle1=Xi_LLL,
        particle2=Xi_LLL,
        descriptor="B_s0 -> Xi~+ Xi-",
        name="BdsToXipXim_LLLLLL",
        bcvtx_sep_min=None,
        VCHI2PDOF_Max=50.0,
        BPVIPCHI2_Max=100.0,
    )
    return [Xi_LLL, line_alg]


@check_process
@configurable
def make_BdsToXipXim_LLLDDL(process):
    Xi_LLL = make_xim_to_lambda_pi_lll(
        lambdas=make_veryloose_lambda_LL(),
        pions=make_pions(),
        vchi2pdof_max=50.0,
        bpvvdz_min=0.0 * mm,
    )
    Xi_DDL = make_xim_to_lambda_pi_ddl(
        lambdas=make_loose_lambda_DD(),
        pions=make_pions(),
        vchi2pdof_max=50.0,
        bpvvdz_min=0.0 * mm,
    )
    line_alg = make_BdsToTwoCharmlessBaryons(
        particle1=Xi_LLL,
        particle2=Xi_DDL,
        descriptor="B_s0 -> Xi~+ Xi-",
        name="BdsToXipXim_LLLDDL",
        bcvtx_sep_min=None,
        VCHI2PDOF_Max=50.0,
        BPVIPCHI2_Max=100.0,
    )
    return [Xi_LLL, Xi_DDL, line_alg]


@check_process
@configurable
def make_BdsToXipXim_LLLDDD(process):
    Xi_LLL = make_xim_to_lambda_pi_lll(
        lambdas=make_veryloose_lambda_LL(),
        pions=make_pions(),
        vchi2pdof_max=50.0,
        bpvvdz_min=0.0 * mm,
    )
    Xi_DDD = make_xim_to_lambda_pi_ddd(
        lambdas=make_loose_lambda_DD(),
        pions=make_down_pions(),
        vchi2pdof_max=50.0,
        bpvvdz_min=0.0 * mm,
    )
    line_alg = make_BdsToTwoCharmlessBaryons(
        particle1=Xi_LLL,
        particle2=Xi_DDD,
        descriptor="B_s0 -> Xi~+ Xi-",
        name="BdsToXipXim_LLLDDD",
        bcvtx_sep_min=None,
        VCHI2PDOF_Max=50.0,
        BPVIPCHI2_Max=100.0,
    )
    return [Xi_LLL, Xi_DDD, line_alg]


@check_process
@configurable
def make_BdsToXipXim_DDDDDD(process):
    Xi_DDD = make_xim_to_lambda_pi_ddd(
        lambdas=make_loose_lambda_DD(),
        pions=make_down_pions(),
        vchi2pdof_max=50.0,
        bpvvdz_min=0.0 * mm,
    )
    line_alg = make_BdsToTwoCharmlessBaryons(
        particle1=Xi_DDD,
        particle2=Xi_DDD,
        descriptor="B_s0 -> Xi~+ Xi-",
        name="BdsToXipXim_DDDDDD",
        bcvtx_sep_min=None,
        VCHI2PDOF_Max=50.0,
        BPVIPCHI2_Max=100.0,
    )
    return [Xi_DDD, line_alg]


@check_process
@configurable
def make_BdsToXipXim_DDDDDL(process):
    Xi_DDD = make_xim_to_lambda_pi_ddd(
        lambdas=make_loose_lambda_DD(),
        pions=make_down_pions(),
        vchi2pdof_max=50.0,
        bpvvdz_min=0.0 * mm,
    )
    Xi_DDL = make_xim_to_lambda_pi_ddl(
        lambdas=make_loose_lambda_DD(),
        pions=make_pions(),
        vchi2pdof_max=50.0,
        bpvvdz_min=0.0 * mm,
    )
    line_alg = make_BdsToTwoCharmlessBaryons(
        particle1=Xi_DDD,
        particle2=Xi_DDL,
        descriptor="B_s0 -> Xi~+ Xi-",
        name="BdsToXipXim_DDDDDL",
        bcvtx_sep_min=None,
        VCHI2PDOF_Max=50.0,
        BPVIPCHI2_Max=100.0,
    )
    return [Xi_DDD, Xi_DDL, line_alg]


@check_process
@configurable
def make_BdsToXipXim_DDLDDL(process):
    Xi_DDL = make_xim_to_lambda_pi_ddl(
        lambdas=make_loose_lambda_DD(),
        pions=make_pions(),
        vchi2pdof_max=50.0,
        bpvvdz_min=0.0 * mm,
    )
    line_alg = make_BdsToTwoCharmlessBaryons(
        particle1=Xi_DDL,
        particle2=Xi_DDL,
        descriptor="B_s0 -> Xi~+ Xi-",
        name="BdsToXipXim_DDLDDL",
        bcvtx_sep_min=None,
        VCHI2PDOF_Max=50.0,
        BPVIPCHI2_Max=100.0,
    )
    return [Xi_DDL, line_alg]


@check_process
@configurable
def make_BdsToXi0Xi0bar_LLLL(process):
    Xi0_LL = make_xi0(lambdas=make_veryloose_lambda_LL(), pi0=make_resolved_pi0s())
    line_alg = make_BdsToTwoCharmlessBaryons(
        particle1=Xi0_LL,
        particle2=Xi0_LL,
        descriptor="B_s0 -> Xi~0 Xi0",
        name="BdsToXi0Xi0bar_LLLL",
        bcvtx_sep_min=None,
        VCHI2PDOF_Max=None,
        BPVIPCHI2_Max=None,
    )
    return [Xi0_LL, line_alg]


@check_process
@configurable
def make_BdsToXi0Xi0bar_LLDD(process):
    Xi0_LL = make_xi0(lambdas=make_veryloose_lambda_LL(), pi0=make_resolved_pi0s())
    Xi0_DD = make_xi0(lambdas=make_loose_lambda_DD(), pi0=make_resolved_pi0s())
    line_alg = make_BdsToTwoCharmlessBaryons(
        particle1=Xi0_LL,
        particle2=Xi0_DD,
        descriptor="B_s0 -> Xi~0 Xi0",
        name="BdsToXi0Xi0bar_LLDD",
        bcvtx_sep_min=None,
        VCHI2PDOF_Max=None,
        BPVIPCHI2_Max=None,
    )
    return [Xi0_LL, Xi0_DD, line_alg]


@check_process
@configurable
def make_BdsToXi0Xi0bar_DDDD(process):
    Xi0_DD = make_xi0(lambdas=make_loose_lambda_DD(), pi0=make_resolved_pi0s())
    line_alg = make_BdsToTwoCharmlessBaryons(
        particle1=Xi0_DD,
        particle2=Xi0_DD,
        descriptor="B_s0 -> Xi~0 Xi0",
        name="BdsToXi0Xi0bar_DDDD",
        bcvtx_sep_min=None,
        VCHI2PDOF_Max=None,
        BPVIPCHI2_Max=None,
    )
    return [Xi0_DD, line_alg]


@check_process
@configurable
def make_BdsToOmegapOmegam_LLLLLL(process):
    Omega_LLL = make_omegam_to_lambda_k_lll(
        lambdas=make_veryloose_lambda_LL(),
        kaons=make_kaons(),
        vchi2pdof_max=50.0,
        bpvvdz_min=0.0 * mm,
    )
    line_alg = make_BdsToTwoCharmlessBaryons(
        particle1=Omega_LLL,
        particle2=Omega_LLL,
        descriptor="B_s0 -> Omega~+ Omega-",
        name="BdsToOmegapOmegam_LLLLLL",
        bcvtx_sep_min=None,
        VCHI2PDOF_Max=50.0,
        BPVIPCHI2_Max=100.0,
    )
    return [Omega_LLL, line_alg]


@check_process
@configurable
def make_BdsToOmegapOmegam_LLLDDL(process):
    Omega_LLL = make_omegam_to_lambda_k_lll(
        lambdas=make_veryloose_lambda_LL(),
        kaons=make_kaons(),
        vchi2pdof_max=50.0,
        bpvvdz_min=0.0 * mm,
    )
    Omega_DDL = make_omegam_to_lambda_k_ddl(
        lambdas=make_loose_lambda_DD(),
        kaons=make_kaons(),
        vchi2pdof_max=50.0,
        bpvvdz_min=0.0 * mm,
    )
    line_alg = make_BdsToTwoCharmlessBaryons(
        particle1=Omega_LLL,
        particle2=Omega_DDL,
        descriptor="B_s0 -> Omega~+ Omega-",
        name="BdsToOmegapOmegam_LLLDDL",
        bcvtx_sep_min=None,
        VCHI2PDOF_Max=50.0,
        BPVIPCHI2_Max=100.0,
    )
    return [Omega_LLL, Omega_DDL, line_alg]


@check_process
@configurable
def make_BdsToOmegapOmegam_LLLDDD(process):
    Omega_LLL = make_omegam_to_lambda_k_lll(
        lambdas=make_veryloose_lambda_LL(),
        kaons=make_kaons(),
        vchi2pdof_max=50.0,
        bpvvdz_min=0.0 * mm,
    )
    Omega_DDD = make_omegam_to_lambda_k_ddd(
        lambdas=make_loose_lambda_DD(),
        kaons=make_down_kaons(),
        vchi2pdof_max=50.0,
        bpvvdz_min=0.0 * mm,
    )
    line_alg = make_BdsToTwoCharmlessBaryons(
        particle1=Omega_LLL,
        particle2=Omega_DDD,
        descriptor="B_s0 -> Omega~+ Omega-",
        name="BdsToOmegapOmegam_LLLDDD",
        bcvtx_sep_min=None,
        VCHI2PDOF_Max=50.0,
        BPVIPCHI2_Max=100.0,
    )
    return [Omega_LLL, Omega_DDD, line_alg]


@check_process
@configurable
def make_BdsToOmegapOmegam_DDDDDD(process):
    Omega_DDD = make_omegam_to_lambda_k_ddd(
        lambdas=make_loose_lambda_DD(),
        kaons=make_down_kaons(),
        vchi2pdof_max=50.0,
        bpvvdz_min=0.0 * mm,
    )
    line_alg = make_BdsToTwoCharmlessBaryons(
        particle1=Omega_DDD,
        particle2=Omega_DDD,
        descriptor="B_s0 -> Omega~+ Omega-",
        name="BdsToOmegapOmegam_DDDDDD",
        bcvtx_sep_min=None,
        VCHI2PDOF_Max=50.0,
        BPVIPCHI2_Max=100.0,
    )
    return [Omega_DDD, line_alg]


@check_process
@configurable
def make_BdsToOmegapOmegam_DDDDDL(process):
    Omega_DDD = make_omegam_to_lambda_k_ddd(
        lambdas=make_loose_lambda_DD(),
        kaons=make_down_kaons(),
        vchi2pdof_max=50.0,
        bpvvdz_min=0.0 * mm,
    )
    Omega_DDL = make_omegam_to_lambda_k_ddl(
        lambdas=make_loose_lambda_DD(),
        kaons=make_kaons(),
        vchi2pdof_max=50.0,
        bpvvdz_min=0.0 * mm,
    )
    line_alg = make_BdsToTwoCharmlessBaryons(
        particle1=Omega_DDD,
        particle2=Omega_DDL,
        descriptor="B_s0 -> Omega~+ Omega-",
        name="BdsToOmegapOmegam_DDDDDL",
        bcvtx_sep_min=None,
        VCHI2PDOF_Max=50.0,
        BPVIPCHI2_Max=100.0,
    )
    return [Omega_DDD, Omega_DDL, line_alg]


@check_process
@configurable
def make_BdsToOmegapOmegam_DDLDDL(process):
    Omega_DDL = make_omegam_to_lambda_k_ddl(
        lambdas=make_loose_lambda_DD(),
        kaons=make_kaons(),
        vchi2pdof_max=50.0,
        bpvvdz_min=0.0 * mm,
    )
    line_alg = make_BdsToTwoCharmlessBaryons(
        particle1=Omega_DDL,
        particle2=Omega_DDL,
        descriptor="B_s0 -> Omega~+ Omega-",
        name="BdsToOmegapOmegam_DDLDDL",
        bcvtx_sep_min=None,
        VCHI2PDOF_Max=50.0,
        BPVIPCHI2_Max=100.0,
    )
    return [Omega_DDL, line_alg]
