###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" ""
* Definition of B0 -> eta(eta') K*0 HLT lines

"""

from GaudiKernel.SystemOfUnits import MeV
from PyConf import configurable

from Hlt2Conf.lines.bnoc.builders.b_builder import make_BToEtaKaon
from Hlt2Conf.lines.bnoc.builders.basic_builder import (
    make_eta_pipigamma,
    make_eta_threepions_merged,
    make_eta_threepions_resolved,
    make_etaprime_gamma,
    make_etaprime_merged,
    make_etaprime_resolved,
    make_kstar0,
    make_kstarplus_DD,
    make_kstarplus_LL,
    make_tight_kaons,
    make_tight_pions,
)
from Hlt2Conf.lines.bnoc.utils import check_process


###############################################################################
## B(s)0 -> eta Kst0, etap Kst0
###############################################################################
## B0 -> eta Kst0
@check_process
@configurable
def make_BdsToEtaKstz_Merged(process):
    eta = make_eta_threepions_merged()
    kst0 = make_kstar0()

    line_alg = make_BToEtaKaon(
        particles=[eta, kst0],
        name="BNOC_BdsToEtaKstz_Merged_Combiner_{hash}",
        descriptor="[B0 -> eta K*(892)0]cc",
    )

    return [eta, kst0, line_alg]


@check_process
@configurable
def make_BdsToEtaKstz_Resolved(process):
    eta = make_eta_threepions_resolved()
    kst0 = make_kstar0()

    line_alg = make_BToEtaKaon(
        particles=[eta, kst0],
        name="BNOC_BdsToEtaKstz_Resolved_Combiner_{hash}",
        descriptor="[B0 -> eta K*(892)0]cc",
    )

    return [eta, kst0, line_alg]


@check_process
@configurable
def make_BdsToEtaKstz_Gamma(process):
    eta = make_eta_pipigamma()
    kst0 = make_kstar0()

    line_alg = make_BToEtaKaon(
        particles=[eta, kst0],
        name="BNOC_BdsToEtaKstz_Gamma_Combiner_{hash}",
        descriptor="[B0 -> eta K*(892)0]cc",
    )

    return [eta, kst0, line_alg]


## B0 -> eta' Kst0
@check_process
@configurable
def make_BdsToEtapKstz_Merged(process):
    etap = make_etaprime_merged()
    kst0 = make_kstar0()

    line_alg = make_BToEtaKaon(
        particles=[etap, kst0],
        name="BNOC_BdsToEtapKstz_Merged_Combiner_{hash}",
        descriptor="[B0 -> eta_prime K*(892)0]cc",
    )

    return [etap, kst0, line_alg]


@check_process
@configurable
def make_BdsToEtapKstz_Resolved(process):
    etap = make_etaprime_resolved()
    kst0 = make_kstar0()

    line_alg = make_BToEtaKaon(
        particles=[etap, kst0],
        name="BNOC_BdsToEtapKstz_Resolved_Combiner_{hash}",
        descriptor="[B0 -> eta_prime K*(892)0]cc",
    )

    return [etap, kst0, line_alg]


@check_process
@configurable
def make_BdsToEtapKstz_Gamma(process):
    etap = make_etaprime_gamma(
        am_min=810 * MeV,
        am_max=1110 * MeV,
        pt_min=600 * MeV,
        gamma_et_min=450 * MeV,
        dipions_mass_min=725 * MeV,
        dipions_mass_max=925 * MeV,
    )
    kst0 = make_kstar0(
        am_min=787 * MeV,
        am_max=997 * MeV,
        pi_pt_min=425 * MeV,
        k_pt_min=425 * MeV,
        pi_p_min=2500 * MeV,
        k_p_min=2500 * MeV,
        asumpt_min=1500 * MeV,
    )

    line_alg = make_BToEtaKaon(
        particles=[etap, kst0],
        name="BNOC_BdsToEtapKstz_Gamma_Combiner_{hash}",
        descriptor="[B0 -> eta_prime K*(892)0]cc",
    )

    return [etap, kst0, line_alg]


###############################################################################
## B(s)0 -> eta eta, etap etap, eta etap
###############################################################################
## B(s)0 -> eta eta: only consider pi0 modes
@check_process
@configurable
def make_BdsToEtaEta_MergedMerged(process):
    eta = make_eta_threepions_merged()

    line_alg = make_BToEtaKaon(
        particles=[eta, eta],
        name="BNOC_BdsToEtaEta_MergedMerged_Combiner_{hash}",
        descriptor="B0 -> eta eta",
    )

    return [eta, line_alg]


@check_process
@configurable
def make_BdsToEtaEta_ResolvedResolved(process):
    eta = make_eta_threepions_resolved()

    line_alg = make_BToEtaKaon(
        particles=[eta, eta],
        name="BNOC_BdsToEtaEta_ResolvedResolved_Combiner_{hash}",
        descriptor="B0 -> eta eta",
    )

    return [eta, line_alg]


@check_process
@configurable
def make_BdsToEtaEta_MergedResolved(process):
    eta_m = make_eta_threepions_merged()
    eta_r = make_eta_threepions_resolved()

    line_alg = make_BToEtaKaon(
        particles=[eta_m, eta_r],
        name="BNOC_BdsToEtaEta_MergedResolved_Combiner_{hash}",
        AllowDiffInputsForSameIDChildren=True,
        descriptor="B0 -> eta eta",
    )

    return [eta_m, eta_r, line_alg]


## B(s)0 -> eta etap: only consider pi0 modes
@check_process
@configurable
def make_BdsToEtaEtap_Merged(process):
    eta = make_eta_threepions_merged()
    etap = make_etaprime_gamma()

    line_alg = make_BToEtaKaon(
        particles=[eta, etap],
        name="BNOC_BdsToEtaEtap_Merged_Combiner_{hash}",
        descriptor="B0 -> eta eta_prime",
    )

    return [eta, etap, line_alg]


@check_process
@configurable
def make_BdsToEtaEtap_Resolved(process):
    eta = make_eta_threepions_resolved()
    etap = make_etaprime_gamma()

    line_alg = make_BToEtaKaon(
        particles=[eta, etap],
        name="BNOC_BdsToEtaEtap_Resolved_Combiner_{hash}",
        descriptor="B0 -> eta eta_prime",
    )

    return [eta, etap, line_alg]


## B(s)0 -> etap etap
@check_process
@configurable
def make_BdsToEtapEtap(process):
    etap = make_etaprime_gamma()

    line_alg = make_BToEtaKaon(
        particles=[etap, etap],
        name="BNOC_BdsToEtapEtap_Combiner_{hash}",
        descriptor="B0 -> eta_prime eta_prime",
    )

    return [etap, line_alg]


###############################################################################
## B(s)0 -> K*+ K*-
###############################################################################
@check_process
@configurable
def make_BdsToKstpKstp_LL(process):
    kstplus = make_kstarplus_LL()

    line_alg = make_BToEtaKaon(
        particles=[kstplus, kstplus],
        name="BNOC_BdsToKstpKstp_LL_Combiner_{hash}",
        descriptor="B0 -> K*(892)+ K*(892)-",
    )

    return [kstplus, line_alg]


@check_process
@configurable
def make_BdsToKstpKstp_DD(process):
    kstplus = make_kstarplus_DD()

    line_alg = make_BToEtaKaon(
        particles=[kstplus, kstplus],
        name="BNOC_BdsToKstpKstp_DD_Combiner_{hash}",
        descriptor="B0 -> K*(892)+ K*(892)-",
    )

    return [kstplus, line_alg]


@check_process
@configurable
def make_BdsToKstpKstp_LD(process):
    kstplus_ll = make_kstarplus_LL()
    kstplus_dd = make_kstarplus_DD()

    line_alg = make_BToEtaKaon(
        particles=[kstplus_ll, kstplus_dd],
        name="BNOC_BdsToKstpKstp_LD_Combiner_{hash}",
        descriptor="[B0 -> K*(892)+ K*(892)-]cc",
    )

    return [kstplus_ll, kstplus_dd, line_alg]


###############################################################################
## B+ -> eta Kst+, etap Kst+
###############################################################################
## B+ -> eta Kst+: LL
@check_process
@configurable
def make_BuToEtaKstp_LL_Merged(process):
    eta = make_eta_threepions_merged()
    kstplus = make_kstarplus_LL()

    line_alg = make_BToEtaKaon(
        particles=[eta, kstplus],
        am_max=6750 * MeV,
        vtx_am_max=6700 * MeV,
        name="BNOC_BuToEtaKstp_LL_Merged_Combiner_{hash}",
        descriptor="[B+ -> eta K*(892)+]cc",
    )

    return [eta, kstplus, line_alg]


@check_process
@configurable
def make_BuToEtaKstp_LL_Resolved(process):
    eta = make_eta_threepions_resolved()
    kstplus = make_kstarplus_LL()

    line_alg = make_BToEtaKaon(
        particles=[eta, kstplus],
        am_max=6750 * MeV,
        vtx_am_max=6700 * MeV,
        name="BNOC_BuToEtaKstp_LL_Resolved_Combiner_{hash}",
        descriptor="[B+ -> eta K*(892)+]cc",
    )

    return [eta, kstplus, line_alg]


@check_process
@configurable
def make_BuToEtaKstp_LL_Gamma(process):
    eta = make_eta_pipigamma(
        am_min=475 * MeV,
        am_max=625 * MeV,
        pt_min=550 * MeV,
        pt_min_gamma=450 * MeV,
        dipions_mass_min=400 * MeV,
        dipions_mass_max=600 * MeV,
    )
    kstplus = make_kstarplus_LL(
        am_min=752 * MeV,
        am_max=1032 * MeV,
        pi_p_min=3000 * MeV,
        pi_pt_min=700 * MeV,  # limit
        asumpt_min=1500 * MeV,
        bpvfdchi2_min=150,
        vchi2pdof_max=12,
    )

    line_alg = make_BToEtaKaon(
        particles=[eta, kstplus],
        am_max=6750 * MeV,
        vtx_am_max=6700 * MeV,
        name="BNOC_BuToEtaKstp_LL_Gamma_Combiner_{hash}",
        descriptor="[B+ -> eta K*(892)+]cc",
    )

    return [eta, kstplus, line_alg]


## B+ -> eta Kst+: DD
@check_process
@configurable
def make_BuToEtaKstp_DD_Merged(process):
    eta = make_eta_threepions_merged()
    kstplus = make_kstarplus_DD()

    line_alg = make_BToEtaKaon(
        particles=[eta, kstplus],
        am_max=6750 * MeV,
        vtx_am_max=6700 * MeV,
        name="BNOC_BuToEtaKstp_DD_Merged_Combiner_{hash}",
        descriptor="[B+ -> eta K*(892)+]cc",
    )

    return [eta, kstplus, line_alg]


@check_process
@configurable
def make_BuToEtaKstp_DD_Resolved(process):
    eta = make_eta_threepions_resolved()
    kstplus = make_kstarplus_DD()

    line_alg = make_BToEtaKaon(
        particles=[eta, kstplus],
        am_max=6750 * MeV,
        vtx_am_max=6700 * MeV,
        name="BNOC_BuToEtaKstp_DD_Resolved_Combiner_{hash}",
        descriptor="[B+ -> eta K*(892)+]cc",
    )

    return [eta, kstplus, line_alg]


@check_process
@configurable
def make_BuToEtaKstp_DD_Gamma(process):
    eta = make_eta_pipigamma()
    kstplus = make_kstarplus_DD()

    line_alg = make_BToEtaKaon(
        particles=[eta, kstplus],
        am_max=6750 * MeV,
        vtx_am_max=6700 * MeV,
        name="BNOC_BuToEtaKstp_DD_Gamma_Combiner_{hash}",
        descriptor="[B+ -> eta K*(892)+]cc",
    )

    return [eta, kstplus, line_alg]


## B+ -> eta' Kst+: LL
@check_process
@configurable
def make_BuToEtapKstp_LL_Merged(process):
    etap = make_etaprime_merged()
    kstplus = make_kstarplus_LL()

    line_alg = make_BToEtaKaon(
        particles=[etap, kstplus],
        am_max=6750 * MeV,
        vtx_am_max=6700 * MeV,
        name="BNOC_BuToEtapKstp_LL_Merged_Combiner_{hash}",
        descriptor="[B+ -> eta_prime K*(892)+]cc",
    )

    return [etap, kstplus, line_alg]


@check_process
@configurable
def make_BuToEtapKstp_LL_Resolved(process):
    etap = make_etaprime_resolved()
    kstplus = make_kstarplus_LL()

    line_alg = make_BToEtaKaon(
        particles=[etap, kstplus],
        am_max=6750 * MeV,
        vtx_am_max=6700 * MeV,
        name="BNOC_BuToEtapKstp_LL_Resolved_Combiner_{hash}",
        descriptor="[B+ -> eta_prime K*(892)+]cc",
    )

    return [etap, kstplus, line_alg]


@check_process
@configurable
def make_BuToEtapKstp_LL_Gamma(process):
    etap = make_etaprime_gamma(
        am_min=810 * MeV,
        am_max=1110 * MeV,
        pt_min=600 * MeV,
        gamma_et_min=450 * MeV,
        dipions_mass_min=725 * MeV,
        dipions_mass_max=925 * MeV,
    )
    kstplus = make_kstarplus_LL(
        am_min=752 * MeV,
        am_max=1032 * MeV,
        pi_p_min=3000 * MeV,
        pi_pt_min=650 * MeV,  # limit
        asumpt_min=1500 * MeV,
    )
    line_alg = make_BToEtaKaon(
        particles=[etap, kstplus],
        am_max=6750 * MeV,
        vtx_am_max=6700 * MeV,
        name="BNOC_BuToEtapKstp_LL_Gamma_Combiner_{hash}",
        descriptor="[B+ -> eta_prime K*(892)+]cc",
    )

    return [etap, kstplus, line_alg]


## B+ -> eta' Kst+: DD
@check_process
@configurable
def make_BuToEtapKstp_DD_Merged(process):
    etap = make_etaprime_merged()
    kstplus = make_kstarplus_DD()

    line_alg = make_BToEtaKaon(
        particles=[etap, kstplus],
        am_max=6750 * MeV,
        vtx_am_max=6700 * MeV,
        name="BNOC_BuToEtapKstp_DD_Merged_Combiner_{hash}",
        descriptor="[B+ -> eta_prime K*(892)+]cc",
    )

    return [etap, kstplus, line_alg]


@check_process
@configurable
def make_BuToEtapKstp_DD_Resolved(process):
    etap = make_etaprime_resolved()
    kstplus = make_kstarplus_DD()

    line_alg = make_BToEtaKaon(
        particles=[etap, kstplus],
        am_max=6750 * MeV,
        vtx_am_max=6700 * MeV,
        name="BNOC_BuToEtapKstp_DD_Resolved_Combiner_{hash}",
        descriptor="[B+ -> eta_prime K*(892)+]cc",
    )

    return [etap, kstplus, line_alg]


@check_process
@configurable
def make_BuToEtapKstp_DD_Gamma(process):
    etap = make_etaprime_gamma()
    kstplus = make_kstarplus_DD()

    line_alg = make_BToEtaKaon(
        particles=[etap, kstplus],
        am_max=6750 * MeV,
        vtx_am_max=6700 * MeV,
        name="BNOC_BuToEtapKstp_DD_Gamma_Combiner_{hash}",
        descriptor="[B+ -> eta_prime K*(892)+]cc",
    )

    return [etap, kstplus, line_alg]


###############################################################################
## B+ -> eta K+, etap K+
###############################################################################
## B+ -> eta K+
@check_process
@configurable
def make_BuToEtaKp_Merged(process):
    eta = make_eta_threepions_merged()
    kplus = make_tight_kaons(pt_min=500.0 * MeV)

    line_alg = make_BToEtaKaon(
        particles=[eta, kplus],
        am_max=6750 * MeV,
        vtx_am_max=6700 * MeV,
        name="BNOC_BuToEtaKp_Merged_Combiner_{hash}",
        descriptor="[B+ -> eta K+]cc",
    )

    return [eta, line_alg]


@check_process
@configurable
def make_BuToEtaKp_Resolved(process):
    eta = make_eta_threepions_resolved()
    kplus = make_tight_kaons(pt_min=500.0 * MeV)

    line_alg = make_BToEtaKaon(
        particles=[eta, kplus],
        am_max=6750 * MeV,
        vtx_am_max=6700 * MeV,
        name="BNOC_BuToEtaKp_Resolved_Combiner_{hash}",
        descriptor="[B+ -> eta K+]cc",
    )

    return [eta, line_alg]


@check_process
@configurable
def make_BuToEtaKp_Gamma(process):
    eta = make_eta_pipigamma()
    kplus = make_tight_kaons(pt_min=500.0 * MeV)

    line_alg = make_BToEtaKaon(
        particles=[eta, kplus],
        am_max=6750 * MeV,
        vtx_am_max=6700 * MeV,
        name="BNOC_BuToEtaKp_Gamma_Combiner_{hash}",
        descriptor="[B+ -> eta K+]cc",
    )

    return [eta, line_alg]


## B+ -> eta' K+
@check_process
@configurable
def make_BuToEtapKp_Merged(process):
    etap = make_etaprime_merged()
    kplus = make_tight_kaons(pt_min=500.0 * MeV)

    line_alg = make_BToEtaKaon(
        particles=[etap, kplus],
        am_max=6750 * MeV,
        vtx_am_max=6700 * MeV,
        name="BNOC_BuToEtapKp_Merged_Combiner_{hash}",
        descriptor="[B+ -> eta_prime K+]cc",
    )

    return [etap, line_alg]


@check_process
@configurable
def make_BuToEtapKp_Resolved(process):
    etap = make_etaprime_resolved()
    kplus = make_tight_kaons(pt_min=500.0 * MeV)

    line_alg = make_BToEtaKaon(
        particles=[etap, kplus],
        am_max=6750 * MeV,
        vtx_am_max=6700 * MeV,
        name="BNOC_BuToEtapKp_Resolved_Combiner_{hash}",
        descriptor="[B+ -> eta_prime K+]cc",
    )

    return [etap, line_alg]


@check_process
@configurable
def make_BuToEtapKp_Gamma(process):
    etap = make_etaprime_gamma()
    kplus = make_tight_kaons(pt_min=500.0 * MeV)

    line_alg = make_BToEtaKaon(
        particles=[etap, kplus],
        am_max=6750 * MeV,
        vtx_am_max=6700 * MeV,
        name="BNOC_BuToEtapKp_Gamma_Combiner_{hash}",
        descriptor="[B+ -> eta_prime K+]cc",
    )

    return [etap, line_alg]


###############################################################################
## B+ -> K*0 K*+
###############################################################################
@check_process
@configurable
def make_BuToKstzKstp_LL(process):
    kst0 = make_kstar0(
        make_pions=make_tight_pions,
        make_kaons=make_tight_kaons,
        pi_pt_min=500 * MeV,
        k_pt_min=500 * MeV,
        asumpt_min=1200 * MeV,
    )
    kstplus = make_kstarplus_LL()

    line_alg = make_BToEtaKaon(
        particles=[kst0, kstplus],
        am_max=6750 * MeV,
        vtx_am_max=6700 * MeV,
        name="BNOC_BuToKstzKstp_LL_Merged_Combiner_{hash}",
        descriptor="[B+ -> K*(892)0 K*(892)+]cc",
    )

    return [kst0, kstplus, line_alg]


@check_process
@configurable
def make_BuToKstzKstp_DD(process):
    kst0 = make_kstar0(
        make_pions=make_tight_pions,
        make_kaons=make_tight_kaons,
        pi_pt_min=500 * MeV,
        k_pt_min=500 * MeV,
        asumpt_min=1200 * MeV,
    )
    kstplus = make_kstarplus_DD()

    line_alg = make_BToEtaKaon(
        particles=[kst0, kstplus],
        am_max=6750 * MeV,
        vtx_am_max=6700 * MeV,
        name="BNOC_BuToKstzKstp_DD_Merged_Combiner_{hash}",
        descriptor="[B+ -> K*(892)0 K*(892)+]cc",
    )

    return [kst0, kstplus, line_alg]
