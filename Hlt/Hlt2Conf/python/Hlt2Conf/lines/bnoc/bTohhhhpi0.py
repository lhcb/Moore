###############################################################################
# (c) Copyright 2021-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of b -> hhhhpi0 HLT2 lines with h=pi/K/p"""

from GaudiKernel.SystemOfUnits import MeV, mm
from PyConf import configurable

from Hlt2Conf.lines.bnoc.builders.b_builder import make_bbaryon_5body
from Hlt2Conf.lines.bnoc.builders.basic_builder import (
    make_bbaryon_ks0_dd,
    make_bbaryon_ks0_ll,
    make_lambda_DD,
    make_lambda_LL,
    make_resolved_pi0s,
    make_tight_merged_pi0s,
    make_tight_photons,
    make_verytight_pions,
)

pi0_resolved_cut = {
    "pion_mass_min": 115 * MeV,
    "pion_mass_max": 165 * MeV,
}
pi0_merged_cut = {"p_min": 1000 * MeV}
h_noPID_cut = {"pi_pidk_max": None, "pi_pidp_max": None, "mipchi2_min": 16}
BdsTo4h_cut = {"mass_min": 4700 * MeV, "mass_max": 7000 * MeV, "bcvtx_sep_min": None}
BpTo4h_LL_cut = {
    "mass_min": 4700 * MeV,
    "mass_max": 7000 * MeV,
    "bcvtx_sep_min": 5 * mm,
}
bmTo4h_cut = {
    "mass_min": 4700 * MeV,
    "mass_max": 7000 * MeV,
    "bcvtx_sep_min": 15 * mm,
}

## BdsTohhhhPi0, h=k/pi/p
## remove the pid cut to include all 4hpi0 modes


@configurable
def make_BdsToPipPipPimPimPi0_NoPID_merged(process):
    pions_charged = make_verytight_pions(**h_noPID_cut)
    pi0_merged = make_tight_merged_pi0s(**pi0_merged_cut)
    line_alg = make_bbaryon_5body(
        particles=[
            pions_charged,
            pions_charged,
            pions_charged,
            pions_charged,
            pi0_merged,
        ],
        descriptor="B0 -> pi+ pi+ pi- pi- pi0",
        **BdsTo4h_cut,
    )
    return line_alg


@configurable
def make_BdsToPipPipPimPimPi0_NoPID_resolved(process):
    pions_charged = make_verytight_pions(**h_noPID_cut)
    pi0_resolved = make_resolved_pi0s(gamma=make_tight_photons, **pi0_resolved_cut)
    line_alg = make_bbaryon_5body(
        particles=[
            pions_charged,
            pions_charged,
            pions_charged,
            pions_charged,
            pi0_resolved,
        ],
        descriptor="B0 -> pi+ pi+ pi- pi- pi0",
        **BdsTo4h_cut,
    )
    return line_alg


## BuToKshhhpi0, h=k/pi/p


@configurable
def make_BuToKsPipPipPimPi0_NoPID_LL_merged(process):
    pions_charged = make_verytight_pions(**h_noPID_cut)
    Ks_LL = make_bbaryon_ks0_ll()
    pi0_merged = make_tight_merged_pi0s(**pi0_merged_cut)
    line_alg = make_bbaryon_5body(
        particles=[Ks_LL, pions_charged, pions_charged, pions_charged, pi0_merged],
        descriptor="[B+ -> KS0 pi+ pi+ pi- pi0]cc",
        **BpTo4h_LL_cut,
    )
    return line_alg


@configurable
def make_BuToKsPipPipPimPi0_NoPID_LL_resolved(process):
    pions_charged = make_verytight_pions(**h_noPID_cut)
    Ks_LL = make_bbaryon_ks0_ll()
    pi0_resolved = make_resolved_pi0s(gamma=make_tight_photons, **pi0_resolved_cut)
    line_alg = make_bbaryon_5body(
        particles=[Ks_LL, pions_charged, pions_charged, pions_charged, pi0_resolved],
        descriptor="[B+ -> KS0 pi+ pi+ pi- pi0]cc",
        **BpTo4h_LL_cut,
    )
    return line_alg


@configurable
def make_BuToKsPipPipPimPi0_NoPID_DD_merged(process):
    pions_charged = make_verytight_pions(**h_noPID_cut)
    Ks_DD = make_bbaryon_ks0_dd()
    pi0_merged = make_tight_merged_pi0s(**pi0_merged_cut)
    line_alg = make_bbaryon_5body(
        particles=[Ks_DD, pions_charged, pions_charged, pions_charged, pi0_merged],
        descriptor="[B+ -> KS0 pi+ pi+ pi- pi0]cc",
        **BdsTo4h_cut,
    )
    return line_alg


@configurable
def make_BuToKsPipPipPimPi0_NoPID_DD_resolved(process):
    pions_charged = make_verytight_pions(**h_noPID_cut)
    Ks_DD = make_bbaryon_ks0_dd()
    pi0_resolved = make_resolved_pi0s(gamma=make_tight_photons, **pi0_resolved_cut)
    line_alg = make_bbaryon_5body(
        particles=[Ks_DD, pions_charged, pions_charged, pions_charged, pi0_resolved],
        descriptor="[B+ -> KS0 pi+ pi+ pi- pi0]cc",
        **BdsTo4h_cut,
    )
    return line_alg


## XibToLmdhhhpi0, h=k/pi/p


@configurable
def make_XibToLmdPipPimPimPi0_NoPID_LL_merged(process):
    pions_charged = make_verytight_pions(**h_noPID_cut)
    Lmd_LL = make_lambda_LL()
    pi0_merged = make_tight_merged_pi0s(**pi0_merged_cut)
    line_alg = make_bbaryon_5body(
        particles=[Lmd_LL, pions_charged, pions_charged, pions_charged, pi0_merged],
        descriptor="[Xi_b- -> Lambda0 pi+ pi- pi- pi0]cc",
        **bmTo4h_cut,
    )
    return line_alg


@configurable
def make_XibToLmdPipPimPimPi0_NoPID_LL_resolved(process):
    pions_charged = make_verytight_pions(**h_noPID_cut)
    Lmd_LL = make_lambda_LL()
    pi0_resolved = make_resolved_pi0s(gamma=make_tight_photons, **pi0_resolved_cut)
    line_alg = make_bbaryon_5body(
        particles=[Lmd_LL, pions_charged, pions_charged, pions_charged, pi0_resolved],
        descriptor="[Xi_b- -> Lambda0 pi+ pi- pi- pi0]cc",
        **bmTo4h_cut,
    )
    return line_alg


@configurable
def make_XibToLmdPipPimPimPi0_NoPID_DD_merged(process):
    pions_charged = make_verytight_pions(**h_noPID_cut)
    Lmd_DD = make_lambda_DD()
    pi0_merged = make_tight_merged_pi0s(**pi0_merged_cut)
    line_alg = make_bbaryon_5body(
        particles=[Lmd_DD, pions_charged, pions_charged, pions_charged, pi0_merged],
        descriptor="[Xi_b- -> Lambda0 pi+ pi- pi- pi0]cc",
        **BdsTo4h_cut,
    )
    return line_alg


@configurable
def make_XibToLmdPipPimPimPi0_NoPID_DD_resolved(process):
    pions_charged = make_verytight_pions(**h_noPID_cut)
    Lmd_DD = make_lambda_DD()
    pi0_resolved = make_resolved_pi0s(gamma=make_tight_photons, **pi0_resolved_cut)
    line_alg = make_bbaryon_5body(
        particles=[Lmd_DD, pions_charged, pions_charged, pions_charged, pi0_resolved],
        descriptor="[Xi_b- -> Lambda0 pi+ pi- pi- pi0]cc",
        **BdsTo4h_cut,
    )
    return line_alg
