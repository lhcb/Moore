###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of following BNOC lines:
b-baryon -> proton 3h lines
Implemented:
Lb0/Xib0 -> pKKK
Lb0/Xib0 -> pKKpi
Lb0/Xib0 -> pKpipi
Lb0/Xib0 -> ppipipi
Lb0/Xib0 -> pppd

b-baryon -> proton proton anti-proton h lines

b-baryon -> Lambda0 2h lines

"""

import Functors as F
from GaudiKernel.SystemOfUnits import MeV, mm

from Hlt2Conf.lines.bnoc.builders.b_builder import (
    make_bbaryon_2body,
    make_bbaryon_3body,
    make_bbaryon_4body,
)
from Hlt2Conf.lines.bnoc.builders.basic_builder import (
    make_bbaryon_detached_deuterons,
    make_bbaryon_detached_kaons,
    make_bbaryon_detached_pions,
    make_bbaryon_detached_protons,
    make_bbaryon_ks0_dd,
    make_bbaryon_ks0_ll,
    make_bbaryon_soft_kaons,
    make_bbaryon_soft_pions,
    make_bbaryon_soft_protons,
    make_loose_lambda_DD,
    make_phi,
    make_sigma0_dd,
    make_sigma0_ll,
    make_veryloose_lambda_LL,
)
from Hlt2Conf.lines.bnoc.utils import check_process

all_lines = {}

###################
### Global cuts ###
###################

lb_cuts_no_sep_cut_p3h_nopid = {
    "mass_min": 5195 * MeV,
    "mass_max": 6105 * MeV,
    "med_pt_min": 500 * MeV,
    "med_bpvipchi2_min": 20.0,
    "bpvipchi2_sum_min": 250.0,
    "bcvtx_sep_min": None,
}

lb_cuts_no_sep_cut = {
    "mass_min": 5195 * MeV,
    "mass_max": 6105 * MeV,
    "med_pt_min": 400 * MeV,
    "med_bpvipchi2_min": 20.0,
    "bpvipchi2_sum_min": 250.0,
    "bcvtx_sep_min": None,
}

lb_with_l0_2body_dd_cut = {
    "mass_min": 5195 * MeV,
    "mass_max": 6105 * MeV,
    "bcvtx_sep_min": None,
}

lb_with_sigma0_2body_cut = {
    "mass_min": 5119 * MeV,
    "mass_max": 6105 * MeV,
    "pt_min": 2000 * MeV,
    "ipchi2_max": 12.0,
    "dira_min": 0.995,
    "bcvtx_sep_min": None,
}

lb_with_ks_ll_cut = {
    "mass_min": 5295 * MeV,
    "mass_max": 6205 * MeV,
    "pt_sum_min": 4400 * MeV,
    "ipchi2_max": 9.0,
    "bpvfdchi2_min": 50.0,
    "dira_min": 0.9997,
    "med_pt_min": 750 * MeV,
    "bpvipchi2_sum_min": 1000.0,
    "bcvtx_sep_min": 15 * mm,
    "daughter_index": 2,
}

lb_with_ks_dd_cut = {
    "mass_min": 5295 * MeV,
    "mass_max": 6205 * MeV,
    "pt_sum_min": 4400 * MeV,
    "ipchi2_max": 9.0,
    "bpvfdchi2_min": 50.0,
    "dira_min": 0.9997,
    "med_pt_min": 750 * MeV,
    "bpvipchi2_sum_min": 300.0,
    "bcvtx_sep_min": None,
}

lb_with_l0_3body_ll_cut = {
    "mass_min": 5195 * MeV,
    "mass_max": 6105 * MeV,
    "pt_sum_min": 4000 * MeV,
    "ipchi2_max": 12.0,
    "bpvfdchi2_min": 50.0,
    "dira_min": 0.9997,
    "med_pt_min": 700 * MeV,
    "bpvipchi2_sum_min": 300.0,
    "bcvtx_sep_min": 15 * mm,
    "daughter_index": 1,
}

lb_with_l0_3body_dd_cut = {
    "mass_min": 5195 * MeV,
    "mass_max": 6105 * MeV,
    "pt_sum_min": 4000 * MeV,
    "ipchi2_max": 12.0,
    "bpvfdchi2_min": 50.0,
    "dira_min": 0.9997,
    "med_pt_min": 700 * MeV,
    "bpvipchi2_sum_min": 200.0,
    "bcvtx_sep_min": None,
}

##################
### HLT2 Lines ###
##################


@check_process
def make_Lb0ToPpKpKmKm(process):
    if process == "spruce":
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_4body(
        particles=[protons, hadron1, hadron1, hadron1],
        descriptor="[Lambda_b0 -> p+ K+ K- K-]cc",
        **lb_cuts_no_sep_cut,
    )
    return line_alg


@check_process
def make_Lb0ToPpKmKpPim(process):
    if process == "spruce":
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[protons, hadron1, hadron1, hadron2],
        descriptor="[Lambda_b0 -> p+ K- K+ pi-]cc",
        **lb_cuts_no_sep_cut,
    )
    return line_alg


@check_process
def make_Lb0ToPpKmKmPip(process):
    if process == "spruce":
        protons = (make_bbaryon_detached_protons(),)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[protons, hadron1, hadron1, hadron2],
        descriptor="[Lambda_b0 -> p+ K- K- pi+]cc",
        **lb_cuts_no_sep_cut,
    )
    return line_alg


@check_process
def make_Lb0ToPpKmPipPim(process):
    if process == "spruce":
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[protons, hadron1, hadron2, hadron2],
        descriptor="[Lambda_b0 -> p+ K- pi+ pi-]cc",
        **lb_cuts_no_sep_cut,
    )
    return line_alg


@check_process
def make_Lb0ToPpKpPimPim(process):
    if process == "spruce":
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[protons, hadron1, hadron2, hadron2],
        descriptor="[Lambda_b0 -> p+ K+ pi- pi-]cc",
        **lb_cuts_no_sep_cut,
    )
    return line_alg


@check_process
def make_Lb0ToPpPipPimPim(process):
    if process == "spruce":
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[protons, hadron1, hadron1, hadron1],
        descriptor="[Lambda_b0 -> p+ pi+ pi- pi-]cc",
        **lb_cuts_no_sep_cut,
    )
    return line_alg


@check_process
def make_Lb0ToPpPipPimPim_NoPID(process):
    if process == "spruce":
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_detached_protons(
            pid=((F.PID_P > 3.0) & (F.PID_P - F.PID_K > -2))
        )
        hadron1 = make_bbaryon_detached_pions(pid=None)
    line_alg = make_bbaryon_4body(
        particles=[protons, hadron1, hadron1, hadron1],
        descriptor="[Lambda_b0 -> p+ pi+ pi- pi-]cc",
        **lb_cuts_no_sep_cut_p3h_nopid,
    )
    return line_alg


# b-baryon -> proton proton anti-proton h


@check_process
def make_Lb0ToPpPpPmPim(process):
    if process == "spruce":
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[protons, protons, protons, hadron1],
        descriptor="[Lambda_b0 -> p+ p+ p~- pi-]cc",
        **lb_cuts_no_sep_cut,
    )
    return line_alg


@check_process
def make_Lb0ToPpPpPmKm(process):
    if process == "spruce":
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_detached_protons()
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_4body(
        particles=[protons, protons, protons, hadron1],
        descriptor="[Lambda_b0 -> p+ p+ p~- K-]cc",
        **lb_cuts_no_sep_cut,
    )
    return line_alg


@check_process
def make_Lb0ToPpPmPmDeutp(process):
    if process == "spruce":
        protons = make_bbaryon_detached_protons()
        deuterons = make_bbaryon_detached_deuterons()
    elif process == "hlt2":
        protons = make_bbaryon_detached_protons()
        deuterons = make_bbaryon_detached_deuterons(pid=None)
    line_alg = make_bbaryon_4body(
        particles=[protons, protons, protons, deuterons],
        descriptor="[Lambda_b0 -> p+ p~- p~- deuteron]cc",
        **lb_cuts_no_sep_cut,
    )
    return line_alg


# b-baryon -> p KS0 h LL / DD


@check_process
def make_Lb0ToPpKSPim_LL(process):
    if process == "spruce":
        protons = make_bbaryon_soft_protons(pid=None)
        hadron1 = make_bbaryon_ks0_ll(pt_min=0 * MeV)
        hadron2 = make_bbaryon_soft_pions(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_soft_protons()
        hadron1 = make_bbaryon_ks0_ll(pt_min=0 * MeV)
        hadron2 = make_bbaryon_soft_pions()
    line_alg = make_bbaryon_3body(
        particles=[protons, hadron1, hadron2],
        descriptor="[Lambda_b0 -> p+ KS0 pi-]cc",
        **lb_with_ks_ll_cut,
    )
    return [hadron1, line_alg]


@check_process
def make_Lb0ToPpKSKm_LL(process):
    if process == "spruce":
        protons = make_bbaryon_soft_protons(pid=None)
        hadron1 = make_bbaryon_ks0_ll(pt_min=0 * MeV)
        hadron2 = make_bbaryon_soft_kaons(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_soft_protons()
        hadron1 = make_bbaryon_ks0_ll(pt_min=0 * MeV)
        hadron2 = make_bbaryon_soft_kaons()
    line_alg = make_bbaryon_3body(
        particles=[protons, hadron1, hadron2],
        descriptor="[Lambda_b0 -> p+ KS0 K-]cc",
        **lb_with_ks_ll_cut,
    )
    return [hadron1, line_alg]


@check_process
def make_Lb0ToPpKSPim_DD(process):
    if process == "spruce":
        protons = make_bbaryon_soft_protons(pid=None)
        hadron1 = make_bbaryon_ks0_dd(pt_min=0 * MeV)
        hadron2 = make_bbaryon_soft_pions(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_soft_protons()
        hadron1 = make_bbaryon_ks0_dd(pt_min=0 * MeV)
        hadron2 = make_bbaryon_soft_pions()
    line_alg = make_bbaryon_3body(
        particles=[protons, hadron1, hadron2],
        descriptor="[Lambda_b0 -> p+ KS0 pi-]cc",
        **lb_with_ks_dd_cut,
    )
    return [hadron1, line_alg]


@check_process
def make_Lb0ToPpKSKm_DD(process):
    if process == "spruce":
        protons = make_bbaryon_soft_protons(pid=None)
        hadron1 = make_bbaryon_ks0_dd(pt_min=0 * MeV)
        hadron2 = make_bbaryon_soft_kaons(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_soft_protons()
        hadron1 = make_bbaryon_ks0_dd(pt_min=0 * MeV)
        hadron2 = make_bbaryon_soft_kaons()
    line_alg = make_bbaryon_3body(
        particles=[protons, hadron1, hadron2],
        descriptor="[Lambda_b0 -> p+ KS0 K-]cc",
        **lb_with_ks_dd_cut,
    )
    return [hadron1, line_alg]


# b-baryon -> Lambda0 2h LL


@check_process
def make_Lb0ToL0PipPim_LL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_soft_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_soft_pions()
    line_alg = make_bbaryon_3body(
        particles=[lambda0s, hadron1, hadron1],
        descriptor="[Lambda_b0 -> Lambda0 pi+ pi-]cc",
        **lb_with_l0_3body_ll_cut,
    )
    return [lambda0s, line_alg]


@check_process
def make_Lb0ToL0KpPim_LL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_soft_kaons(pid=None)
        hadron2 = make_bbaryon_soft_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_soft_kaons()
        hadron2 = make_bbaryon_soft_pions()
    line_alg = make_bbaryon_3body(
        particles=[lambda0s, hadron1, hadron2],
        descriptor="[Lambda_b0 -> Lambda0 K+ pi-]cc",
        **lb_with_l0_3body_ll_cut,
    )
    return [lambda0s, line_alg]


@check_process
def make_Lb0ToL0KmPip_LL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_soft_kaons(pid=None)
        hadron2 = make_bbaryon_soft_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_soft_kaons()
        hadron2 = make_bbaryon_soft_pions()
    line_alg = make_bbaryon_3body(
        particles=[lambda0s, hadron1, hadron2],
        descriptor="[Lambda_b0 -> Lambda0 K- pi+]cc",
        **lb_with_l0_3body_ll_cut,
    )
    return [lambda0s, line_alg]


@check_process
def make_Lb0ToL0KpKm_LL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_soft_kaons(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_soft_kaons()
    line_alg = make_bbaryon_3body(
        particles=[lambda0s, hadron1, hadron1],
        descriptor="[Lambda_b0 -> Lambda0 K+ K-]cc",
        **lb_with_l0_3body_ll_cut,
    )
    return [lambda0s, line_alg]


@check_process
def make_Lb0ToL0PpPm_LL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_soft_protons(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_soft_protons()
    line_alg = make_bbaryon_3body(
        particles=[lambda0s, hadron1, hadron1],
        descriptor="[Lambda_b0 -> Lambda0 p+ p~-]cc",
        **lb_with_l0_3body_ll_cut,
    )
    return [lambda0s, line_alg]


# b-baryon -> Lambda0 2h DD


@check_process
def make_Lb0ToL0PipPim_DD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_soft_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_soft_pions()
    line_alg = make_bbaryon_3body(
        particles=[lambda0s, hadron1, hadron1],
        descriptor="[Lambda_b0 -> Lambda0 pi+ pi-]cc",
        **lb_with_l0_3body_dd_cut,
    )
    return [lambda0s, line_alg]


@check_process
def make_Lb0ToL0KpPim_DD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_soft_kaons(pid=None)
        hadron2 = make_bbaryon_soft_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_soft_kaons()
        hadron2 = make_bbaryon_soft_pions()
    line_alg = make_bbaryon_3body(
        particles=[lambda0s, hadron1, hadron2],
        descriptor="[Lambda_b0 -> Lambda0 K+ pi-]cc",
        **lb_with_l0_3body_dd_cut,
    )
    return [lambda0s, line_alg]


@check_process
def make_Lb0ToL0KmPip_DD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_soft_kaons(pid=None)
        hadron2 = make_bbaryon_soft_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_soft_kaons()
        hadron2 = make_bbaryon_soft_pions()
    line_alg = make_bbaryon_3body(
        particles=[lambda0s, hadron1, hadron2],
        descriptor="[Lambda_b0 -> Lambda0 K- pi+]cc",
        **lb_with_l0_3body_dd_cut,
    )
    return [lambda0s, line_alg]


@check_process
def make_Lb0ToL0KpKm_DD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_soft_kaons(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_soft_kaons()
    line_alg = make_bbaryon_3body(
        particles=[lambda0s, hadron1, hadron1],
        descriptor="[Lambda_b0 -> Lambda0 K+ K-]cc",
        **lb_with_l0_3body_dd_cut,
    )
    return [lambda0s, line_alg]


@check_process
def make_Lb0ToL0PpPm_DD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_soft_protons(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_soft_protons()
    line_alg = make_bbaryon_3body(
        particles=[lambda0s, hadron1, hadron1],
        descriptor="[Lambda_b0 -> Lambda0 p+ p~-]cc",
        **lb_with_l0_3body_dd_cut,
    )
    return [lambda0s, line_alg]


# b-baryon -> Lambda0 phi LL/DD


@check_process
def make_Lb0ToL0Phi_LL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_phi(k_p_min=500 * MeV, k_pt_min=0 * MeV)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_phi(k_p_min=500 * MeV, k_pt_min=0 * MeV)
    line_alg = make_bbaryon_2body(
        particles=[lambda0s, hadron1],
        descriptor="[Lambda_b0 -> Lambda0 phi(1020)]cc",
        **lb_with_l0_2body_dd_cut,
    )
    return [hadron1, lambda0s, line_alg]


@check_process
def make_Lb0ToL0Phi_DD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_phi(k_p_min=500 * MeV, k_pt_min=0 * MeV)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_phi(k_p_min=500 * MeV, k_pt_min=0 * MeV)
    line_alg = make_bbaryon_2body(
        particles=[lambda0s, hadron1],
        descriptor="[Lambda_b0 -> Lambda0 phi(1020)]cc",
        **lb_with_l0_2body_dd_cut,
    )
    return [hadron1, lambda0s, line_alg]


# b-baryon -> Lambda0 KS LLLL / LLDD / DDLL / DDDD


@check_process
def make_Lb0ToL0KS_LLLL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_ks0_ll()
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_ks0_ll()
    line_alg = make_bbaryon_2body(
        particles=[lambda0s, hadron1],
        descriptor="[Lambda_b0 -> Lambda0 KS0]cc",
        **lb_with_l0_2body_dd_cut,
    )
    return [hadron1, lambda0s, line_alg]


@check_process
def make_Lb0ToL0KS_LLDD(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_ks0_dd()
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_ks0_dd()
    line_alg = make_bbaryon_2body(
        particles=[lambda0s, hadron1],
        descriptor="[Lambda_b0 -> Lambda0 KS0]cc",
        **lb_with_l0_2body_dd_cut,
    )
    return [hadron1, lambda0s, line_alg]


@check_process
def make_Lb0ToL0KS_DDLL(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_ks0_ll()
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_ks0_ll()
    line_alg = make_bbaryon_2body(
        particles=[lambda0s, hadron1],
        descriptor="[Lambda_b0 -> Lambda0 KS0]cc",
        **lb_with_l0_2body_dd_cut,
    )
    return [hadron1, lambda0s, line_alg]


@check_process
def make_Lb0ToL0KS_DDDD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_ks0_dd()
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_ks0_dd()
    line_alg = make_bbaryon_2body(
        particles=[lambda0s, hadron1],
        descriptor="[Lambda_b0 -> Lambda0 KS0]cc",
        **lb_with_l0_2body_dd_cut,
    )
    return [hadron1, lambda0s, line_alg]


# b-baryon -> Sigma0 (-> Lambda0 gamma) phi LL/DD


@check_process
def make_Lb0ToSigma0Phi_LL(process):
    if process == "spruce":
        sigma0s = make_sigma0_ll()
        hadron1 = make_phi(k_p_min=500 * MeV, k_pt_min=100 * MeV)
    elif process == "hlt2":
        sigma0s = make_sigma0_ll()
        hadron1 = make_phi(k_p_min=500 * MeV, k_pt_min=100 * MeV)
    line_alg = make_bbaryon_2body(
        particles=[sigma0s, hadron1],
        descriptor="[Lambda_b0 -> Sigma0 phi(1020)]cc",
        **lb_with_sigma0_2body_cut,
    )
    return [sigma0s, line_alg]


@check_process
def make_Lb0ToSigma0Phi_DD(process):
    if process == "spruce":
        sigma0s = make_sigma0_dd()
        hadron1 = make_phi(k_p_min=500 * MeV, k_pt_min=100 * MeV)
    elif process == "hlt2":
        sigma0s = make_sigma0_dd()
        hadron1 = make_phi(k_p_min=500 * MeV, k_pt_min=100 * MeV)
    line_alg = make_bbaryon_2body(
        particles=[sigma0s, hadron1],
        descriptor="[Lambda_b0 -> Sigma0 phi(1020)]cc",
        **lb_with_sigma0_2body_cut,
    )
    return [sigma0s, line_alg]
