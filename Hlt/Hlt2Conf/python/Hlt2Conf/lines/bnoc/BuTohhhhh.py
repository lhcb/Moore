###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of Bu -> Kpipipipi and Bu -> pipipipipi lines."""

from GaudiKernel.SystemOfUnits import GeV, MeV
from PyConf import configurable

from Hlt2Conf.lines.bnoc.builders.b_builder import make_bTo5h
from Hlt2Conf.lines.bnoc.builders.basic_builder import (
    make_tight_kaons,
    make_tight_pions,
)


@configurable
def make_BuTopipipipipi(process):
    pions = make_tight_pions(
        pi_pidk_max=0, pt_min=250 * MeV, p_min=1.5 * GeV, mipchi2_min=6
    )
    line_alg = make_bTo5h(
        particles=[pions, pions, pions, pions, pions],
        descriptor="[B+ -> pi+ pi+ pi+ pi- pi-]cc",
    )
    return line_alg


@configurable
def make_BuToKpipipipi(process):
    pions = make_tight_pions(
        pi_pidk_max=0, pt_min=250 * MeV, p_min=1.5 * GeV, mipchi2_min=6
    )
    kaons = make_tight_kaons(
        k_pidk_min=0, p_min=2 * GeV, pt_min=250 * MeV, mipchi2_min=6
    )
    line_alg = make_bTo5h(
        particles=[kaons, pions, pions, pions, pions],
        descriptor="[B+ -> K+ pi+ pi+ pi- pi-]cc",
    )
    return line_alg
