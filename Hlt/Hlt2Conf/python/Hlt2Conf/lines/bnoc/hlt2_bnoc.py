###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Booking of BnoC hlt2 lines. This is where Hlt2 lines are actually booked"""

from Hlt2Conf.lines.bnoc import (
    BdsTohhhh,
    BdsTohhpi0,
    BdsToKShh,
    BdsToKSKS,
    BdsToKSpp,
    BdsToPpPmhh,
    BdsToTwoCharmlessBaryons,
    BdsToVV,
    BdTohh,
    BToEtaKStar,
    BToLambdahhh,
    BToPpPm,
    BuTohhh,
    BuToKSh,
    BuToKShhh,
    bbaryon_to_hyperon_4h,
    bbaryon_to_l0hhh,
    bbaryon_to_lightbaryon_h,
    bbaryon_to_lightbaryon_hh,
    bbaryon_to_phh,
    bbaryon_to_phhh,
    bbaryon_to_pppph,
    bTohh,
)
from Hlt2Conf.lines.bnoc.utils import (
    make_custom_lines,
    make_default_lines,
    make_flavour_tagging_lines,
    make_full_stream_lines,
    make_prescaled_lines,
    update_makers,
)

PROCESS = "hlt2"
hlt2_lines = {}
full_lines = {}

line_makers = {}
update_makers(line_makers, bbaryon_to_hyperon_4h)
update_makers(line_makers, bbaryon_to_l0hhh)
update_makers(line_makers, bbaryon_to_lightbaryon_h)
update_makers(line_makers, bbaryon_to_lightbaryon_hh)
update_makers(line_makers, bbaryon_to_phh)
update_makers(line_makers, bbaryon_to_phhh)
update_makers(line_makers, bbaryon_to_pppph)
update_makers(line_makers, BdsTohhhh)
update_makers(line_makers, BdsTohhpi0)
update_makers(line_makers, BdsToKShh)
update_makers(line_makers, BdsToKSKS)
update_makers(line_makers, BdsToKSpp)
update_makers(line_makers, BdsToPpPmhh)
update_makers(line_makers, BdsToVV)
update_makers(line_makers, BdTohh)
update_makers(line_makers, BToEtaKStar)
update_makers(line_makers, bTohh)
update_makers(line_makers, BToLambdahhh)
update_makers(line_makers, BToPpPm)
update_makers(line_makers, BuTohhh)
update_makers(line_makers, BuToKSh)
update_makers(line_makers, BuToKShhh)
update_makers(line_makers, BdsToTwoCharmlessBaryons)

###############################################################################
# Add new lines in the appropriate list / dictionary below
# Note that persistreco=False by default
# Ensure that if you add a new module (e.g. BdsToVV.py) it is imported above
# and included in the `update_makers` calls
###############################################################################

default_lines = [
    "BdsToEtaKstz_Gamma",
    "BdsToEtaKstz_Merged",
    "BdsToEtaKstz_Resolved",
    "BdsToEtapKstz_Gamma",
    "BdsToEtapKstz_Merged",
    "BdsToEtapKstz_Resolved",
    "BdsToKstzKpKm",
    "BdsToKstzOmegaMerged",
    "BdsToKstzOmegaMerged_FakeKstPi",
    "BdsToKstzOmegaMerged_FakeKstK",
    "BdsToKstzOmegaMerged_FakeKstDouble",
    "BdsToKstzOmegaMerged_FakeOmegaPi",
    "BdsToKstzOmegaMerged_FakeOmegaDouble",
    "BdsToKstzOmegaResolved",
    "BdsToKstzOmegaResolved_FakeKstPi",
    "BdsToKstzOmegaResolved_FakeKstK",
    "BdsToKstzOmegaResolved_FakeKstDouble",
    "BdsToKstzOmegaResolved_FakeOmegaPi",
    "BdsToKstzOmegaResolved_FakeOmegaDouble",
    "BdsToPhiOmegaMerged_FakePhiK",
    "BdsToPhiOmegaMerged_FakePhiDouble",
    "BdsToPhiOmegaMerged_FakeOmegaPi",
    "BdsToPhiOmegaMerged_FakeOmegaDouble",
    "BdsToPhiOmegaResolved_FakePhiK",
    "BdsToPhiOmegaResolved_FakePhiDouble",
    "BdsToPhiOmegaResolved_FakeOmegaPi",
    "BdsToPhiOmegaResolved_FakeOmegaDouble",
    "BdsToPhiRho_FakePhiK",
    "BdsToPhiRho_FakePhiDouble",
    "BdsToPhiRho_FakeRhoPi",
    "BdsToPhiRho_FakeRhoDouble",
    "BuToEtaKp_Gamma",
    "BuToEtaKp_Merged",
    "BuToEtaKp_Resolved",
    "BuToEtaKstp_DD_Gamma",
    "BuToEtaKstp_DD_Merged",
    "BuToEtaKstp_DD_Resolved",
    "BuToEtaKstp_LL_Gamma",
    "BuToEtaKstp_LL_Merged",
    "BuToEtaKstp_LL_Resolved",
    "BuToEtapKp_Gamma",
    "BuToEtapKp_Merged",
    "BuToEtapKp_Resolved",
    "BuToEtapKstp_DD_Gamma",
    "BuToEtapKstp_DD_Merged",
    "BuToEtapKstp_DD_Resolved",
    "BuToEtapKstp_LL_Gamma",
    "BuToEtapKstp_LL_Merged",
    "BuToEtapKstp_LL_Resolved",
    "BuToKpPi0",
    "BuToKstzKstp_DD",
    "BuToKstzKstp_LL",
    "BuToL0barPpPipPim_NoPID_DD",
    "BuToL0barPpPipPim_NoPID_LL",
    "BuToL0barPpPipPim_NoPID_TT",
    "BuToL0barPpPpPm_DD",
    "BuToL0barPpPpPm_LL",
    "BuToL0barPpPpPm_TT",
    "BuToL0PmPipPip_NoPID_DD",
    "BuToL0PmPipPip_NoPID_LL",
    "BuToL0PmPipPip_NoPID_TT",
    "BuToPipPi0",
    "BuToPpPpDeutm",
    "BuToPpPpPmPmKp",
    "BuToPpPpPmPmPip",
    "Lb0ToL0KmPip_DD",
    "Lb0ToL0KmPip_LL",
    "Lb0ToL0KpKm_DD",
    "Lb0ToL0KpKm_LL",
    "Lb0ToL0KpPim_DD",
    "Lb0ToL0KpPim_LL",
    "Lb0ToL0KS_DDDD",
    "Lb0ToL0KS_DDLL",
    "Lb0ToL0KS_LLDD",
    "Lb0ToL0KS_LLLL",
    "Lb0ToL0Phi_DD",
    "Lb0ToL0Phi_LL",
    "Lb0ToL0PipPim_DD",
    "Lb0ToL0PipPim_LL",
    "Lb0ToL0PpPm_DD",
    "Lb0ToL0PpPm_LL",
    "Lb0ToPpKSKm_DD",
    "Lb0ToPpKSKm_LL",
    "Lb0ToPpKSPim_DD",
    "Lb0ToPpKSPim_LL",
    "Lb0ToPpPipPimPim_NoPID",
    "Lb0ToPpPmPmDeutp",
    "Lb0ToPpPpPmKm",
    "Lb0ToPpPpPmPim",
    "Lb0ToPpPpPpPmPmKm",
    "Lb0ToPpPpPpPmPmPim",
    "Lb0ToXimKp_DDD",
    "Lb0ToXimKp_DDL",
    "Lb0ToXimKp_LLL",
    "OmbmToOmmKmPip_DDD",
    "OmbmToOmmKmPip_DDL",
    "OmbmToOmmKmPip_LLL",
    "OmbmToOmmKpKm_DDD",
    "OmbmToOmmKpKm_DDL",
    "OmbmToOmmKpKm_LLL",
    "OmbmToOmmKpKmPipPim_DDD",
    "OmbmToOmmKpKmPipPim_DDL",
    "OmbmToOmmKpKmPipPim_LLL",
    "OmbmToOmmKpKpPimPim_DDD",
    "OmbmToOmmKpKpPimPim_DDL",
    "OmbmToOmmKpKpPimPim_LLL",
    "OmbmToOmmKpPim_DDD",
    "OmbmToOmmKpPim_DDL",
    "OmbmToOmmKpPim_LLL",
    "OmbmToOmmKpPipPimPim_DDD",
    "OmbmToOmmKpPipPimPim_DDL",
    "OmbmToOmmKpPipPimPim_LLL",
    "OmbmToOmmKS_DDDDD",
    "OmbmToOmmKS_DDDLL",
    "OmbmToOmmKS_DDLDD",
    "OmbmToOmmKS_DDLLL",
    "OmbmToOmmKS_LLLLL",
    "OmbmToOmmPhi_DDD",
    "OmbmToOmmPhi_DDL",
    "OmbmToOmmPhi_LLL",
    "OmbmToOmmPipPim_DDD",
    "OmbmToOmmPipPim_DDL",
    "OmbmToOmmPipPim_LLL",
    "OmbmToOmmPipPipPimPim_DDD",
    "OmbmToOmmPipPipPimPim_DDL",
    "OmbmToOmmPipPipPimPim_LLL",
    "OmbmToOmmPpPm_DDD",
    "OmbmToOmmPpPm_DDL",
    "OmbmToOmmPpPm_LLL",
    "OmbmToOmmPpPmPipPim_DDD",
    "OmbmToOmmPpPmPipPim_DDL",
    "OmbmToOmmPpPmPipPim_LLL",
    "Xib0ToOmmKp_DDD",
    "Xib0ToOmmKp_DDL",
    "Xib0ToOmmKp_LLL",
    "Xib0ToXimPip_DDD",
    "Xib0ToXimPip_DDL",
    "Xib0ToXimPip_LLL",
    "XibmToL0Km_DD",
    "XibmToL0Km_LL",
    "XibmToL0KmKmPip_DD",
    "XibmToL0KmKmPip_LL",
    "XibmToL0KmPipPim_DD",
    "XibmToL0KmPipPim_LL",
    "XibmToL0KpKmKm_DD",
    "XibmToL0KpKmKm_LL",
    "XibmToL0KpKmPim_DD",
    "XibmToL0KpKmPim_LL",
    "XibmToL0KpPimPim_DD",
    "XibmToL0KpPimPim_LL",
    "XibmToL0Pim_DD",
    "XibmToL0Pim_LL",
    "XibmToL0PipPimPim_DD",
    "XibmToL0PipPimPim_LL",
    "XibmToL0PpPmPim_DD",
    "XibmToL0PpPmPim_LL",
    "XibmToPpKmKm",
    "XibmToPpKmPim",
    "XibmToPpKSKmKm_DD",
    "XibmToPpKSKmKm_LL",
    "XibmToPpKSKmPim_DD",
    "XibmToPpKSKmPim_LL",
    "XibmToPpKSPimPim_DD",
    "XibmToPpKSPimPim_LL",
    "XibmToPpPimPim",
    "XibmToXimKmPip_DDD",
    "XibmToXimKmPip_DDL",
    "XibmToXimKmPip_LLL",
    "XibmToXimKpKm_DDD",
    "XibmToXimKpKm_DDL",
    "XibmToXimKpKm_LLL",
    "XibmToXimKpKmPipPim_DDD",
    "XibmToXimKpKmPipPim_DDL",
    "XibmToXimKpKmPipPim_LLL",
    "XibmToXimKpKpPimPim_DDD",
    "XibmToXimKpKpPimPim_DDL",
    "XibmToXimKpKpPimPim_LLL",
    "XibmToXimKpPim_DDD",
    "XibmToXimKpPim_DDL",
    "XibmToXimKpPim_LLL",
    "XibmToXimKpPipPimPim_DDD",
    "XibmToXimKpPipPimPim_DDL",
    "XibmToXimKpPipPimPim_LLL",
    "XibmToXimKS_DDDDD",
    "XibmToXimKS_DDDLL",
    "XibmToXimKS_DDLDD",
    "XibmToXimKS_DDLLL",
    "XibmToXimKS_LLLDD",
    "XibmToXimKS_LLLLL",
    "XibmToXimPhi_DDD",
    "XibmToXimPhi_DDL",
    "XibmToXimPhi_LLL",
    "XibmToXimPipPim_DDD",
    "XibmToXimPipPim_DDL",
    "XibmToXimPipPim_LLL",
    "XibmToXimPipPipPimPim_DDD",
    "XibmToXimPipPipPimPim_DDL",
    "XibmToXimPipPipPimPim_LLL",
    "XibmToXimPpPm_DDD",
    "XibmToXimPpPm_DDL",
    "XibmToXimPpPm_LLL",
    "XibmToXimPpPmPipPim_DDD",
    "XibmToXimPpPmPipPim_DDL",
    "XibmToXimPpPmPipPim_LLL",
]

# Lines with applied prescale

prescale_lines = {
    "BdsToKstzPhi_FakeKstDouble": {"prescale": 0.1},
    "BdsToKstzPhi_FakeKstK": {"prescale": 0.1},
    "BdsToKstzPhi_FakeKstPi": {"prescale": 0.1},
    "BdsToKstzPhi_FakePhiDouble": {"prescale": 0.1},
    "BdsToKstzPhi_FakePhiK": {"prescale": 0.1},
    "BdsToKstzRho_FakeKstDouble": {"prescale": 0.1},
    "BdsToKstzRho_FakeKstK": {"prescale": 0.1},
    "BdsToKstzRho_FakeKstPi": {"prescale": 0.1},
    "BdsToKstzRho_FakeRhoDouble": {"prescale": 0.1},
    "BdsToKstzRho_FakeRhoPi": {"prescale": 0.1},
    "BuToPpPpDeutm_SS": {"prescale": 0.1},
}

# Listing on lines requiring flavour tagging and any additional information
flavour_tagging_lines = {
    "BdsToEtaEta_MergedMerged": {
        "pv_tracks": True,
    },
    "BdsToEtaEta_MergedResolved": {
        "pv_tracks": True,
    },
    "BdsToEtaEta_ResolvedResolved": {
        "pv_tracks": True,
    },
    "BdsToEtaEtap_Merged": {
        "pv_tracks": True,
    },
    "BdsToEtaEtap_Resolved": {
        "pv_tracks": True,
    },
    "BdsToEtapEtap": {
        "pv_tracks": True,
    },
    "BdsToKSKS_DDDD": {
        "pv_tracks": True,
    },
    "BdsToKSKS_LLDD": {
        "pv_tracks": True,
    },
    "BdsToKSKS_LLLL": {
        "pv_tracks": True,
    },
    "BdsToKSPi0_LL": {
        "pv_tracks": True,
    },
    "BdsToKstpKstp_DD": {
        "pv_tracks": True,
    },
    "BdsToKstpKstp_LD": {
        "pv_tracks": True,
    },
    "BdsToKstpKstp_LL": {
        "pv_tracks": True,
    },
    "BdsToL0L0barPipPim_NoPID_DDDD": {
        "pv_tracks": True,
    },
    "BdsToL0L0barPipPim_NoPID_LLDD": {
        "pv_tracks": True,
    },
    "BdsToL0L0barPipPim_NoPID_LLLL": {
        "pv_tracks": True,
    },
    "BdsToL0L0barPipPim_NoPID_LLTT": {
        "pv_tracks": True,
    },
    "BdsToL0L0barPipPim_NoPID_DDTT": {
        "pv_tracks": True,
    },
    "BdsToL0L0barPipPim_NoPID_TTTT": {
        "pv_tracks": True,
    },
    "BdsToL0L0barPpPm_DDDD": {
        "pv_tracks": True,
    },
    "BdsToL0L0barPpPm_LLDD": {
        "pv_tracks": True,
    },
    "BdsToL0L0barPpPm_LLLL": {
        "pv_tracks": True,
    },
    "BdsToPhiOmegaResolved": {
        "pv_tracks": True,
    },
    "BdsToPhiOmegaMerged": {
        "pv_tracks": True,
    },
    "BdsToPpPmKpKm": {
        "pv_tracks": True,
        "require_GEC": True,
        "require_topo": True,
        "min_twobody_mva": 0.95,
        "min_threebody_mva": 0.95,
    },
    # commented due to high bandwidth
    # 'BdsToPpPmKpPim': {
    #     'pv_tracks': True,
    #     'require_GEC': True,
    #     'require_topo': True,
    #     'min_twobody_mva': 0.95,
    #     'min_threebody_mva': 0.95
    # },
    "BdsToPpPmKS_DD": {
        "pv_tracks": True,
        "require_GEC": True,
        "require_topo": True,
        "min_twobody_mva": 0.1,
        "min_threebody_mva": 0.1,
    },
    "BdsToPpPmKS_LL": {
        "pv_tracks": True,
        "require_GEC": True,
        "require_topo": True,
        "min_twobody_mva": 0.1,
        "min_threebody_mva": 0.1,
    },
    # commented due to high bandwidth
    # 'BdsToPpPmPipPim': {
    #     'pv_tracks': True,
    #     'require_GEC': True,
    #     'require_topo': True,
    #     'min_twobody_mva': 0.95,
    #     'min_threebody_mva': 0.95
    # },
    "BdsToL0L0bar_LLLL": {
        "pv_tracks": True,
    },
    "BdsToL0L0bar_LLDD": {
        "pv_tracks": True,
    },
    "BdsToL0L0bar_DDDD": {
        "pv_tracks": True,
    },
    "BdsToXipXim_LLLLLL": {
        "pv_tracks": True,
    },
    "BdsToXipXim_LLLDDL": {
        "pv_tracks": True,
    },
    "BdsToXipXim_LLLDDD": {
        "pv_tracks": True,
    },
    "BdsToXipXim_DDDDDL": {
        "pv_tracks": True,
    },
    "BdsToXipXim_DDLDDL": {
        "pv_tracks": True,
    },
    "BdsToXipXim_DDDDDD": {
        "pv_tracks": True,
    },
    "BdsToXi0Xi0bar_LLLL": {
        "pv_tracks": True,
    },
    "BdsToXi0Xi0bar_LLDD": {
        "pv_tracks": True,
    },
    "BdsToXi0Xi0bar_DDDD": {
        "pv_tracks": True,
    },
    "BdsToOmegapOmegam_LLLLLL": {
        "pv_tracks": True,
    },
    "BdsToOmegapOmegam_LLLDDL": {
        "pv_tracks": True,
    },
    "BdsToOmegapOmegam_LLLDDD": {
        "pv_tracks": True,
    },
    "BdsToOmegapOmegam_DDDDDL": {
        "pv_tracks": True,
    },
    "BdsToOmegapOmegam_DDLDDL": {
        "pv_tracks": True,
    },
    "BdsToOmegapOmegam_DDDDDD": {
        "pv_tracks": True,
    },
}

# Below are the lines which require a more custom configuration
# You may want to add a new line category if you have some specific use-case
# e.g. a line with persistreco=True but no other customisation, to keep clear the
# different groups of lines we define.

# Lines with GEC and topo but no flavour tagging
# The 2 (3) body MVA cuts can be specified rather than using the default
# 0.1 that all the below lines currently use (see
# flavour_tagging_gec_topo_lines for examples)
gec_topo_lines = {
    "BcToKpKpKm_NoPID": {"require_GEC": True, "require_topo": True},
    "BcToL0barPp_DD": {"require_GEC": True, "require_topo": True},
    "BcToL0barPp_LL": {"require_GEC": True, "require_topo": True},
    "BuToKpKpKm_NoPID": {
        "require_GEC": True,
        "require_topo": True,
        "min_twobody_mva": 0.13,
        "min_threebody_mva": 0.13,
    },
    "BuToPpPmKp_NoPID": {
        "require_GEC": True,
        "require_topo": True,
    },
    "BuToKSPpPmPip_DD": {
        "require_GEC": True,
        "require_topo": True,
    },
    "BuToKSPpPmPip_LL": {
        "require_GEC": True,
        "require_topo": True,
    },
    "BuToKSKp_DD": {"require_GEC": True, "require_topo": True},
    "BuToKSKp_LL": {"require_GEC": True, "require_topo": True},
    "BuToKSPip_DD": {"require_GEC": True, "require_topo": True},
    "BuToKSPip_LL": {"require_GEC": True, "require_topo": True},
    "BuToL0barPp_DD": {"require_GEC": True, "require_topo": True},
    "BuToL0barPp_LL": {"require_GEC": True, "require_topo": True},
}

# Using 'iso_kwargs: {}' will use the default arguments in utils/make_iso_particles
# Setting `iso_kwargs` to anything other than its default value of `None` will
# try to save isolation information
isolation_lines = {
    "BdsToKSKpKm_DD": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
        "require_GEC": True,
        "require_topo": True,
        "min_twobody_mva": 0.1,
        "min_threebody_mva": 0.1,
    },
    "BdsToKSKpKm_LL": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
        "require_GEC": True,
        "require_topo": True,
        "min_twobody_mva": 0.1,
        "min_threebody_mva": 0.1,
    },
    "BdsToKSKpPim_DD": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
        "require_GEC": True,
        "require_topo": True,
        "min_twobody_mva": 0.1,
        "min_threebody_mva": 0.1,
    },
    "BdsToKSKpPim_LL": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
        "require_GEC": True,
        "require_topo": True,
        "min_twobody_mva": 0.1,
        "min_threebody_mva": 0.1,
    },
    "BdsToKSPipPim_DD": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
        "require_GEC": True,
        "require_topo": True,
        "min_twobody_mva": 0.1,
        "min_threebody_mva": 0.1,
    },
    "BdsToKSPipPim_LL": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
        "require_GEC": True,
        "require_topo": True,
        "min_twobody_mva": 0.1,
        "min_threebody_mva": 0.1,
    },
    "BdsToPipPimPi0_NoPID_merged": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BdsToPipPimPi0_NoPID_resolved": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "Lb0ToPpPimPi0_NoPID_merged": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "Lambda_b0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "Lb0ToPpPimPi0_NoPID_resolved": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "Lambda_b0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BdsToKstzKstzb": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B_s0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BdsToKstzPhi": {
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BdsToKstzRho": {
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BdsToPhiKpKm": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BdsToPhiPhi": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
        "raw_banks": ["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    },
    "BdsToPhiRho": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B_s0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BdsToRhoRho": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BdsToPpPpPmPm": {
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BdsToPpPm": {
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
}

# Special lines with settings not belonging to any remaining group
special_lines = {}

# Lines belonging to FULL stream

full_stream_lines = {
    "BdToPipPim_NoPID_Full": {
        "pv_tracks": True,
        "raw_banks": ["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    },
}

# Collecting various lists

flavour_tagging_list = {}
flavour_tagging_list.update(flavour_tagging_lines)

# If you add a new category of custom line, update the below dictionary

custom_lines = {}
custom_lines.update(gec_topo_lines)
custom_lines.update(isolation_lines)
# placeholder if any new special line would be registered in future
custom_lines.update(special_lines)

make_default_lines(
    process=PROCESS,
    line_dict=hlt2_lines,
    line_makers=line_makers,
    default_lines=default_lines,
)

make_prescaled_lines(
    process=PROCESS,
    line_dict=hlt2_lines,
    line_makers=line_makers,
    prescaled_lines=prescale_lines,
)

make_flavour_tagging_lines(
    process=PROCESS,
    line_dict=hlt2_lines,
    line_makers=line_makers,
    flavour_tagging_lines=flavour_tagging_list,
)

make_custom_lines(
    process=PROCESS,
    line_dict=hlt2_lines,
    line_makers=line_makers,
    custom_lines=custom_lines,
)

make_full_stream_lines(
    process=PROCESS,
    line_dict=full_lines,
    line_makers=line_makers,
    full_stream_lines=full_stream_lines,
)
