###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Booking of BnoC sprucing lines. This is where sprucing lines are actually booked"""

from Hlt2Conf.lines.bnoc import (
    BdsTohhhh,
    BdsTohhpi0,
    BdsToKShh,
    BdsToKSKS,
    BdsToKSpp,
    BdsTopi0pi0,
    BdsToPpPmhh,
    BdsToVV,
    BdTohh,
    BToEtaKStar,
    BToLambdahhh,
    BuTohhh,
    BuTohhhhh,
    BuToKSh,
    BuToKShhh,
    bbaryon_to_hyperon_4h,
    bbaryon_to_l0hhh,
    bbaryon_to_lightbaryon_h,
    bbaryon_to_lightbaryon_hh,
    bbaryon_to_phh,
    bbaryon_to_phhh,
    bbaryon_to_pppph,
    bTohh,
    bTohhhhpi0,
    bTohhhpi0,
)
from Hlt2Conf.lines.bnoc.utils import (
    make_custom_lines,
    make_default_lines,
    make_flavour_tagging_lines,
    make_prescaled_lines,
    update_makers,
)

PROCESS = "spruce"
sprucing_lines = {}

line_makers = {}
update_makers(line_makers, bbaryon_to_hyperon_4h)
update_makers(line_makers, bbaryon_to_l0hhh)
update_makers(line_makers, bbaryon_to_lightbaryon_h)
update_makers(line_makers, bbaryon_to_lightbaryon_hh)
update_makers(line_makers, bbaryon_to_phh)
update_makers(line_makers, bbaryon_to_phhh)
update_makers(line_makers, bbaryon_to_pppph)
update_makers(line_makers, BdsTohhhh)
update_makers(line_makers, BdsTohhpi0)
update_makers(line_makers, BdsToKShh)
update_makers(line_makers, BdsToKSKS)
update_makers(line_makers, BdsToKSpp)
update_makers(line_makers, BdsTopi0pi0)
update_makers(line_makers, BdsToPpPmhh)
update_makers(line_makers, BdsToVV)
update_makers(line_makers, BdTohh)
update_makers(line_makers, BToEtaKStar)
update_makers(line_makers, BuTohhhhh)
update_makers(line_makers, bTohh)
update_makers(line_makers, bTohhhpi0)
update_makers(line_makers, bTohhhhpi0)
update_makers(line_makers, BToLambdahhh)
update_makers(line_makers, BuTohhh)
update_makers(line_makers, BuToKSh)
update_makers(line_makers, BuToKShhh)

# Automate creation of sprucing lines for all Hlt2 lines that have persistreco=True
from Hlt2Conf.lines.bnoc.hlt2_bnoc import custom_lines as hlt2_custom_lines

# Automatically add HLT2 decision filtering for HLT2 lines with persistreco=True
spruce_hlt2_filters = {}
persistreco_custom_lines = {}

for decay, kwargs in hlt2_custom_lines.items():
    if "persistreco" in kwargs:
        spruce_hlt2_filters[decay] = [f"Hlt2BnoC_{decay}Decision"]
        persistreco_custom_lines[decay] = kwargs

# If you want your sprucing line to filter on Hlt2 lines, e.g.
# Hlt2Topo2BodyDecision, add them to this dictionary (will automatically check
# if the line has already been automatically added to spurce_hlt_filters, and
# will merge accordingly)
spruce_hlt2_extra_filters = {
    "BdsToPi0Pi0_merged": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
        "Hlt2QEE_DiElectronPrompt_PersistPhotons_FullDecision",
        "Hlt2QEE_DiElectronDisplaced_PersistPhotons_FullDecision",
    ],
    "BdsToPi0Pi0_resolved": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
        "Hlt2QEE_DiElectronPrompt_PersistPhotons_FullDecision",
        "Hlt2QEE_DiElectronDisplaced_PersistPhotons_FullDecision",
    ],
    "BdsToKSKS_LLLL": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "BdsToL0L0barPpPm_LLLL": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "BdsToL0L0barPipPim_NoPID_LLLL": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "BdToPipPim_NoPID": [
        "Hlt2BnoC_BdToPipPim_NoPID_FullDecision",
    ],
    "BdsToKpKpKmKm": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "BdsToKpKpKmPim": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "BdsToKpKmPipPim": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "BdsToKpPipPimPim": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "BdsToKsPipPimPi0_NoPID_DD_merged": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "BdsToKsPipPimPi0_NoPID_DD_resolved": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "BdsToKsPipPimPi0_NoPID_LL_merged": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "BdsToKsPipPimPi0_NoPID_LL_resolved": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "BdsToPipPipPimPim": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "BdsToPipPipPimPimPi0_NoPID_merged": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "BdsToPipPipPimPimPi0_NoPID_resolved": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "BuToKSPipPipPim_DD": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "BuToKSPipPipPim_LL": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "BuToKSKpPipPim_DD": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "BuToKSKpPipPim_LL": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "BuToKSKmPipPip_DD": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "BuToKSKmPipPip_LL": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "BuToKSKpKmPip_DD": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "BuToKSKpKmPip_LL": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "BuToKSKpKpPim_DD": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "BuToKSKpKpPim_LL": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "BuToKSKpKpKm_DD": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "BuToKSKpKpKm_LL": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "BuToKpipipipi": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "BuTopipipipipi": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "BuToKsPipPi0_NoPID_DD_merged": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "BuToKsPipPi0_NoPID_DD_resolved": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "BuToKsPipPi0_NoPID_LL_merged": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "BuToKsPipPi0_NoPID_LL_resolved": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "BuToKsPipPipPimPi0_NoPID_LL_merged": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "BuToKsPipPipPimPi0_NoPID_LL_resolved": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "BuToKsPipPipPimPi0_NoPID_DD_merged": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "BuToKsPipPipPimPi0_NoPID_DD_resolved": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "BuToPipPipPimPi0_NoPID_merged": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "BuToPipPipPimPi0_NoPID_resolved": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "Lb0ToLmdPipPimPi0_NoPID_DD_merged": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "Lb0ToLmdPipPimPi0_NoPID_DD_resolved": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "Lb0ToLmdPipPimPi0_NoPID_LL_merged": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "Lb0ToLmdPipPimPi0_NoPID_LL_resolved": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "XibToLmdPimPi0_NoPID_DD_merged": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "XibToLmdPimPi0_NoPID_DD_resolved": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "XibToLmdPimPi0_NoPID_LL_merged": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "XibToLmdPimPi0_NoPID_LL_resolved": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "XibToLmdPipPimPimPi0_NoPID_LL_merged": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "XibToLmdPipPimPimPi0_NoPID_LL_resolved": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "XibToLmdPipPimPimPi0_NoPID_DD_merged": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "XibToLmdPipPimPimPi0_NoPID_DD_resolved": [
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ],
    "Lb0ToPpKpKmKm": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "Lb0ToPpKmKpPim": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "Lb0ToPpKmPipPim": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "Lb0ToPpKpPimPim": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "Lb0ToPpPipPimPim": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "Lb0ToSigma0Phi_LL": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "Lb0ToSigma0Phi_DD": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "XibmToSigma0Km_LL": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "XibmToSigma0Km_DD": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "XibmToSigma0Pim_LL": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    "XibmToSigma0Pim_DD": ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
}

for decay, hlt2_lines in spruce_hlt2_extra_filters.items():
    if decay in spruce_hlt2_filters:
        spruce_hlt2_filters[decay] += hlt2_lines
    else:
        spruce_hlt2_filters[decay] = hlt2_lines

make_custom_lines(
    process=PROCESS,
    line_dict=sprucing_lines,
    line_makers=line_makers,
    custom_lines=persistreco_custom_lines,
    spruce_hlt2_filters=spruce_hlt2_filters,
)

###############################################################################
# Add new lines in the appropriate list / dictionary below
# Note that persistreco=False by default
# Ensure that if you add a new module (e.g. BdsToVV.py) it is imported above
# and included in the `update_makers` calls
###############################################################################

default_lines = [
    "Lb0ToPpKpKmKm",
    "Lb0ToPpKmKpPim",
    "Lb0ToPpKmPipPim",
    "Lb0ToPpKpPimPim",
    "Lb0ToPpPipPimPim",
    "BuToKSPipPipPim_DD",
    "BuToKSPipPipPim_LL",
    "BuToKSKpPipPim_DD",
    "BuToKSKpPipPim_LL",
    "BuToKSKmPipPip_DD",
    "BuToKSKmPipPip_LL",
    "BuToKSKpKmPip_DD",
    "BuToKSKpKmPip_LL",
    "BuToKSKpKpPim_DD",
    "BuToKSKpKpPim_LL",
    "BuToKSKpKpKm_DD",
    "BuToKSKpKpKm_LL",
    "BuToKpipipipi",
    "BuTopipipipipi",
]

flavour_tagging_lines = {
    "BdsToKpKpKmKm": {
        "pv_tracks": True,
    },
    "BdsToKpKpKmPim": {
        "pv_tracks": True,
    },
    "BdsToKpKmPipPim": {
        "pv_tracks": True,
    },
    "BdsToPipPipPimPim": {
        "pv_tracks": True,
    },
}

isolation_lines = {
    "BdsToKpPipPimPim": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BdsToKsPipPimPi0_NoPID_DD_merged": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BdsToKsPipPimPi0_NoPID_DD_resolved": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BdsToKsPipPimPi0_NoPID_LL_merged": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BdsToKsPipPimPi0_NoPID_LL_resolved": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BdsToPi0Pi0_merged": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.5,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BdsToPi0Pi0_resolved": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.5,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BdsToPipPipPimPimPi0_NoPID_merged": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BdsToPipPipPimPimPi0_NoPID_resolved": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BuToKsPipPi0_NoPID_DD_merged": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B+",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BuToKsPipPi0_NoPID_DD_resolved": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B+",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BuToKsPipPi0_NoPID_LL_merged": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B+",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BuToKsPipPi0_NoPID_LL_resolved": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B+",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BuToKsPipPipPimPi0_NoPID_DD_merged": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B+",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BuToKsPipPipPimPi0_NoPID_DD_resolved": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B+",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BuToKsPipPipPimPi0_NoPID_LL_merged": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B+",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BuToKsPipPipPimPi0_NoPID_LL_resolved": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B+",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BuToPipPipPimPi0_NoPID_merged": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B+",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "BuToPipPipPimPi0_NoPID_resolved": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "B+",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "Lb0ToLmdPipPimPi0_NoPID_DD_merged": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "Lambda_b0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "Lb0ToLmdPipPimPi0_NoPID_DD_resolved": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "Lambda_b0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "Lb0ToLmdPipPimPi0_NoPID_LL_merged": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "Lambda_b0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "Lb0ToLmdPipPimPi0_NoPID_LL_resolved": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "Lambda_b0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "Lb0ToSigma0Phi_LL": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "Lambda_b0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "Lb0ToSigma0Phi_DD": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "Lambda_b0",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "XibToLmdPimPi0_NoPID_DD_merged": {
        "iso_kwargs": {
            "name": "Xi_b-",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "XibToLmdPimPi0_NoPID_DD_resolved": {
        "iso_kwargs": {
            "name": "Xi_b-",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "XibToLmdPimPi0_NoPID_LL_merged": {
        "iso_kwargs": {
            "name": "Xi_b-",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "XibToLmdPimPi0_NoPID_LL_resolved": {
        "iso_kwargs": {
            "name": "Xi_b-",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "XibToLmdPipPimPimPi0_NoPID_DD_merged": {
        "iso_kwargs": {
            "name": "Xi_b-",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "XibToLmdPipPimPimPi0_NoPID_DD_resolved": {
        "iso_kwargs": {
            "name": "Xi_b-",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "XibToLmdPipPimPimPi0_NoPID_LL_merged": {
        "iso_kwargs": {
            "name": "Xi_b-",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "XibToLmdPipPimPimPi0_NoPID_LL_resolved": {
        "iso_kwargs": {
            "name": "Xi_b-",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "XibmToSigma0Km_LL": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "Xi_b-",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "XibmToSigma0Km_DD": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "Xi_b-",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "XibmToSigma0Pim_LL": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "Xi_b-",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
    "XibmToSigma0Pim_DD": {
        "pv_tracks": True,
        "iso_kwargs": {
            "name": "Xi_b-",
            "coneangle": 1.0,
            "NeutralIso": True,
            "PiZerosIso": True,
        },
    },
}

prescale_lines = {}

# Below are the lines which require a more custom configuration
# You may want to add a new line category if you have some specific use-case
# e.g. a line with persistreco=True but no other customisation, to keep clear the
# different groups of lines we define.

# Lines requiring flavour tagging, GEC and topo
flavour_tagging_gec_topo_lines = {}

# Lines with GEC and topo but no flavour tagging
# The 2 (3) body MVA cuts can be specified rather than using the default
# 0.1 that all the below lines currently use (see
# flavour_tagging_gec_topo_lines for examples)
gec_topo_lines = {}

spruce_with_raw = {
    "BdToPipPim_NoPID": {
        "flavour_tagging": True,
        "pv_tracks": True,
        "persistreco": True,
        "raw_banks": ["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    },
}

# If you add a new category of custom line, update the below dictionary
custom_lines = {}
custom_lines.update(flavour_tagging_gec_topo_lines)
custom_lines.update(gec_topo_lines)
custom_lines.update(spruce_with_raw)
custom_lines.update(isolation_lines)

make_default_lines(
    process=PROCESS,
    line_dict=sprucing_lines,
    line_makers=line_makers,
    default_lines=default_lines,
    spruce_hlt2_filters=spruce_hlt2_filters,
)

make_prescaled_lines(
    process=PROCESS,
    line_dict=sprucing_lines,
    line_makers=line_makers,
    prescaled_lines=prescale_lines,
    spruce_hlt2_filters=spruce_hlt2_filters,
)

make_flavour_tagging_lines(
    process=PROCESS,
    line_dict=sprucing_lines,
    line_makers=line_makers,
    flavour_tagging_lines=flavour_tagging_lines,
    spruce_hlt2_filters=spruce_hlt2_filters,
)

make_custom_lines(
    process=PROCESS,
    line_dict=sprucing_lines,
    line_makers=line_makers,
    custom_lines=custom_lines,
    spruce_hlt2_filters=spruce_hlt2_filters,
)
