###############################################################################
# (c) Copyright 2021-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of B0(s) -> hhpi0 HLT2 lines with h=pi/K/p"""

from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond
from PyConf import configurable

from Hlt2Conf.lines.bnoc.builders.b_builder import make_b2hhh
from Hlt2Conf.lines.bnoc.builders.basic_builder import (
    make_bbaryon_ks0_dd,
    make_bbaryon_ks0_ll,
    make_lambda_DD,
    make_lambda_LL,
    make_resolved_pi0s,
    make_tight_merged_pi0s,
    make_tight_photons,
    make_tight_protons,
    make_verytight_pions,
)

Lb0_cut = {
    "am_min_vtx": 4900 * MeV,
    "am_min": 4850 * MeV,
    "am_max": 6050 * MeV,
    "am_max_vtx": 6000 * MeV,
    "PT_sum_min": 5 * GeV,
}

Bds_cut = {
    "am_min_vtx": 4500 * MeV,
    "am_max_vtx": 5700 * MeV,
    "am_min": 4450 * MeV,
    "am_max": 5750 * MeV,
    "PT_sum_min": 5 * GeV,
    "bpvltime_max": 30 * picosecond,
}

Bu_cut = {
    "am_min_vtx": 4500 * MeV,
    "am_max_vtx": 6300 * MeV,
    "am_min": 4450 * MeV,
    "am_max": 6350 * MeV,
    "PT_sum_min": 5 * GeV,
    "bpvltime_max": 30 * picosecond,
}

Xi_cut = {
    "am_min_vtx": 4900 * MeV,
    "am_min": 4850 * MeV,
    "am_max": 6550 * MeV,
    "am_max_vtx": 6500 * MeV,
    "PT_sum_min": 5 * GeV,
}


def make_Lb0ToPpPimPi0_NoPID_merged(process):
    pions_charged = make_verytight_pions(p_min=3 * GeV, pi_pidk_max=None)
    protons_charged = make_tight_protons(mipchi2_min=30)
    pi0_merged = make_tight_merged_pi0s()
    line_alg = make_b2hhh(
        particles=[protons_charged, pions_charged, pi0_merged],
        descriptor="[Lambda_b0 -> p+ pi- pi0]cc",
        **Lb0_cut,
    )
    return line_alg


@configurable
def make_Lb0ToPpPimPi0_NoPID_resolved(process):
    pions_charged = make_verytight_pions(p_min=3 * GeV, pi_pidk_max=None)
    protons_charged = make_tight_protons(mipchi2_min=30)
    pi0_resolved = make_resolved_pi0s(gamma=make_tight_photons, pion_mass_min=115 * MeV)
    line_alg = make_b2hhh(
        particles=[protons_charged, pions_charged, pi0_resolved],
        descriptor="[Lambda_b0 -> p+ pi- pi0]cc",
        **Lb0_cut,
    )
    return line_alg


def make_BdsToPipPimPi0_NoPID_merged(process):
    pions_charged = make_verytight_pions(
        p_min=3 * GeV, pi_pidk_max=None, mipchi2_min=30
    )
    pi0_merged = make_tight_merged_pi0s()
    line_alg = make_b2hhh(
        particles=[pions_charged, pions_charged, pi0_merged],
        descriptor="B0 -> pi+ pi- pi0",
        **Bds_cut,
    )
    return line_alg


@configurable
def make_BdsToPipPimPi0_NoPID_resolved(process):
    pions_charged = make_verytight_pions(
        p_min=3 * GeV, pi_pidk_max=None, mipchi2_min=30
    )
    pi0_resolved = make_resolved_pi0s(gamma=make_tight_photons, pion_mass_min=115 * MeV)
    line_alg = make_b2hhh(
        particles=[pions_charged, pions_charged, pi0_resolved],
        descriptor="B0 -> pi+ pi- pi0",
        **Bds_cut,
    )
    return line_alg


@configurable
def make_BuToKsPipPi0_NoPID_LL_merged(process):
    pions_charged = make_verytight_pions(
        p_min=3 * GeV, pi_pidk_max=None, mipchi2_min=30
    )
    pi0_merged = make_tight_merged_pi0s()
    Ks_LL = make_bbaryon_ks0_ll()
    line_alg = make_b2hhh(
        particles=[Ks_LL, pions_charged, pi0_merged],
        descriptor="[B+ -> KS0 pi+ pi0]cc",
        **Bu_cut,
    )
    return line_alg


@configurable
def make_BuToKsPipPi0_NoPID_LL_resolved(process):
    pions_charged = make_verytight_pions(
        p_min=3 * GeV, pi_pidk_max=None, mipchi2_min=30
    )
    pi0_resolved = make_resolved_pi0s(gamma=make_tight_photons, pion_mass_min=115 * MeV)
    Ks_LL = make_bbaryon_ks0_ll()
    line_alg = make_b2hhh(
        particles=[Ks_LL, pions_charged, pi0_resolved],
        descriptor="[B+ -> KS0 pi+ pi0]cc",
        **Bu_cut,
    )
    return line_alg


@configurable
def make_BuToKsPipPi0_NoPID_DD_merged(process):
    pions_charged = make_verytight_pions(
        p_min=3 * GeV, pi_pidk_max=None, mipchi2_min=30
    )
    pi0_merged = make_tight_merged_pi0s()
    Ks_DD = make_bbaryon_ks0_dd()
    line_alg = make_b2hhh(
        particles=[Ks_DD, pions_charged, pi0_merged],
        descriptor="[B+ -> KS0 pi+ pi0]cc",
        **Bu_cut,
    )
    return line_alg


@configurable
def make_BuToKsPipPi0_NoPID_DD_resolved(process):
    pions_charged = make_verytight_pions(
        p_min=3 * GeV, pi_pidk_max=None, mipchi2_min=30
    )
    pi0_resolved = make_resolved_pi0s(gamma=make_tight_photons, pion_mass_min=115 * MeV)
    Ks_DD = make_bbaryon_ks0_dd()
    line_alg = make_b2hhh(
        particles=[Ks_DD, pions_charged, pi0_resolved],
        descriptor="[B+ -> KS0 pi+ pi0]cc",
        **Bu_cut,
    )
    return line_alg


@configurable
def make_XibToLmdPimPi0_NoPID_DD_merged(process):
    pions_charged = make_verytight_pions(
        p_min=3 * GeV, pi_pidk_max=None, mipchi2_min=30
    )
    pi0_merged = make_tight_merged_pi0s()
    Lmd_DD = make_lambda_DD()
    line_alg = make_b2hhh(
        particles=[Lmd_DD, pions_charged, pi0_merged],
        descriptor="[Xi_b- -> Lambda0 pi- pi0]cc",
        **Xi_cut,
    )
    return line_alg


def make_XibToLmdPimPi0_NoPID_DD_resolved(process):
    pions_charged = make_verytight_pions(
        p_min=3 * GeV, pi_pidk_max=None, mipchi2_min=30
    )
    pi0_resolved = make_resolved_pi0s(gamma=make_tight_photons, pion_mass_min=115 * MeV)
    Lmd_DD = make_lambda_DD()
    line_alg = make_b2hhh(
        particles=[Lmd_DD, pions_charged, pi0_resolved],
        descriptor="[Xi_b- -> Lambda0 pi- pi0]cc",
        **Xi_cut,
    )
    return line_alg


@configurable
def make_XibToLmdPimPi0_NoPID_LL_merged(process):
    pions_charged = make_verytight_pions(
        p_min=3 * GeV, pi_pidk_max=None, mipchi2_min=30
    )
    pi0_merged = make_tight_merged_pi0s()
    Lmd_LL = make_lambda_LL()
    line_alg = make_b2hhh(
        particles=[Lmd_LL, pions_charged, pi0_merged],
        descriptor="[Xi_b- -> Lambda0 pi- pi0]cc",
        **Xi_cut,
    )
    return line_alg


def make_XibToLmdPimPi0_NoPID_LL_resolved(process):
    pions_charged = make_verytight_pions(
        p_min=3 * GeV, pi_pidk_max=None, mipchi2_min=30
    )
    pi0_resolved = make_resolved_pi0s(gamma=make_tight_photons, pion_mass_min=115 * MeV)
    Lmd_LL = make_lambda_LL()
    line_alg = make_b2hhh(
        particles=[Lmd_LL, pions_charged, pi0_resolved],
        descriptor="[Xi_b- -> Lambda0 pi- pi0]cc",
        **Xi_cut,
    )
    return line_alg
