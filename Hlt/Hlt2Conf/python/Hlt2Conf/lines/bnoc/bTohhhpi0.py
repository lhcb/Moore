###############################################################################
# (c) Copyright 2021-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of b -> hhhpi0 HLT2 lines with h=pi/K/p"""

from GaudiKernel.SystemOfUnits import MeV, mm
from PyConf import configurable

from Hlt2Conf.lines.bnoc.builders.b_builder import make_bbaryon_4body
from Hlt2Conf.lines.bnoc.builders.basic_builder import (
    make_bbaryon_ks0_dd,
    make_bbaryon_ks0_ll,
    make_lambda_DD,
    make_lambda_LL,
    make_resolved_pi0s,
    make_tight_merged_pi0s,
    make_tight_photons,
    make_verytight_pions,
)

## b+ lines (hhhpi0)
## BTohhhpi0_DD_and_NoV0_cuts is for 3h+pi0 and ks(DD)2h+pi0 channels
BTohhhpi0_DD_and_NoV0_cuts = {
    "mass_min": 4800 * MeV,
    "mass_max": 7000 * MeV,
    "bcvtx_sep_min": None,
}
BToKshhpi0_LL_cuts = {
    "mass_min": 4800 * MeV,
    "mass_max": 7000 * MeV,
    "bcvtx_sep_min": 5 * mm,
}
Lb0_cuts_LL = {
    "mass_min": 5000 * MeV,
    "mass_max": 6500 * MeV,
    "bcvtx_sep_min": 15 * mm,
}
Lb0_cuts_DD = {
    "mass_min": 5000 * MeV,
    "mass_max": 6500 * MeV,
    "bcvtx_sep_min": None,
}
pi0_resolved_cut = {
    "pion_mass_min": 115 * MeV,
    "pion_mass_max": 165 * MeV,
}
pi0_merged_cut = {"p_min": 1000 * MeV}
h_noPID_cut = {"pi_pidk_max": None, "pi_pidp_max": None, "mipchi2_min": 16}


@configurable
def make_BuToPipPipPimPi0_NoPID_merged(process):
    pions_charged = make_verytight_pions(**h_noPID_cut)
    pi0_merged = make_tight_merged_pi0s(**pi0_merged_cut)
    line_alg = make_bbaryon_4body(
        particles=[pions_charged, pions_charged, pions_charged, pi0_merged],
        descriptor="[B+ -> pi+ pi+ pi- pi0]cc",
        **BTohhhpi0_DD_and_NoV0_cuts,
    )
    return line_alg


@configurable
def make_BuToPipPipPimPi0_NoPID_resolved(process):
    pions_charged = make_verytight_pions(**h_noPID_cut)
    pi0_resolved = make_resolved_pi0s(gamma=make_tight_photons, **pi0_resolved_cut)
    line_alg = make_bbaryon_4body(
        particles=[pions_charged, pions_charged, pions_charged, pi0_resolved],
        descriptor="[B+ -> pi+ pi+ pi- pi0]cc",
        **BTohhhpi0_DD_and_NoV0_cuts,
    )
    return line_alg


# b0 lines (Ks/Lmd hhpi0)


## KS LL/DD + merged/resolved
@configurable
def make_BdsToKsPipPimPi0_NoPID_LL_merged(process):
    pions_charged = make_verytight_pions(**h_noPID_cut)
    pi0_merged = make_tight_merged_pi0s(**pi0_merged_cut)
    Ks_LL = make_bbaryon_ks0_ll()
    line_alg = make_bbaryon_4body(
        particles=[Ks_LL, pions_charged, pions_charged, pi0_merged],
        descriptor="B0 -> KS0 pi+ pi- pi0",
        **BToKshhpi0_LL_cuts,
    )
    return line_alg


@configurable
def make_BdsToKsPipPimPi0_NoPID_LL_resolved(process):
    pions_charged = make_verytight_pions(**h_noPID_cut)
    pi0_resolved = make_resolved_pi0s(gamma=make_tight_photons, **pi0_resolved_cut)
    Ks_LL = make_bbaryon_ks0_ll()
    line_alg = make_bbaryon_4body(
        particles=[Ks_LL, pions_charged, pions_charged, pi0_resolved],
        descriptor="B0 -> KS0 pi+ pi- pi0",
        **BToKshhpi0_LL_cuts,
    )
    return line_alg


@configurable
def make_BdsToKsPipPimPi0_NoPID_DD_merged(process):
    pions_charged = make_verytight_pions(**h_noPID_cut)
    pi0_merged = make_tight_merged_pi0s(**pi0_merged_cut)
    Ks_DD = make_bbaryon_ks0_dd()
    line_alg = make_bbaryon_4body(
        particles=[Ks_DD, pions_charged, pions_charged, pi0_merged],
        descriptor="B0 -> KS0 pi+ pi- pi0",
        **BTohhhpi0_DD_and_NoV0_cuts,
    )
    return line_alg


@configurable
def make_BdsToKsPipPimPi0_NoPID_DD_resolved(process):
    pions_charged = make_verytight_pions(**h_noPID_cut)
    pi0_resolved = make_resolved_pi0s(gamma=make_tight_photons, **pi0_resolved_cut)
    Ks_DD = make_bbaryon_ks0_dd()
    line_alg = make_bbaryon_4body(
        particles=[Ks_DD, pions_charged, pions_charged, pi0_resolved],
        descriptor="B0 -> KS0 pi+ pi- pi0",
        **BTohhhpi0_DD_and_NoV0_cuts,
    )
    return line_alg


## Lmd LL/DD + merged/resolved


@configurable
def make_Lb0ToLmdPipPimPi0_NoPID_LL_merged(process):
    pions_charged = make_verytight_pions(**h_noPID_cut)
    pi0_merged = make_tight_merged_pi0s(**pi0_merged_cut)
    Lmd_LL = make_lambda_LL()
    line_alg = make_bbaryon_4body(
        particles=[Lmd_LL, pions_charged, pions_charged, pi0_merged],
        descriptor="[Lambda_b0 -> Lambda0 pi+ pi- pi0]cc",
        **Lb0_cuts_LL,
    )
    return line_alg


@configurable
def make_Lb0ToLmdPipPimPi0_NoPID_LL_resolved(process):
    pions_charged = make_verytight_pions(**h_noPID_cut)
    pi0_resolved = make_resolved_pi0s(gamma=make_tight_photons, **pi0_resolved_cut)
    Lmd_LL = make_lambda_LL()
    line_alg = make_bbaryon_4body(
        particles=[Lmd_LL, pions_charged, pions_charged, pi0_resolved],
        descriptor="[Lambda_b0 -> Lambda0 pi+ pi- pi0]cc",
        **Lb0_cuts_LL,
    )
    return line_alg


@configurable
def make_Lb0ToLmdPipPimPi0_NoPID_DD_merged(process):
    pions_charged = make_verytight_pions(**h_noPID_cut)
    pi0_merged = make_tight_merged_pi0s(**pi0_merged_cut)
    Lmd_DD = make_lambda_DD()
    line_alg = make_bbaryon_4body(
        particles=[Lmd_DD, pions_charged, pions_charged, pi0_merged],
        descriptor="[Lambda_b0 -> Lambda0 pi+ pi- pi0]cc",
        **Lb0_cuts_DD,
    )
    return line_alg


@configurable
def make_Lb0ToLmdPipPimPi0_NoPID_DD_resolved(process):
    pions_charged = make_verytight_pions(**h_noPID_cut)
    pi0_resolved = make_resolved_pi0s(gamma=make_tight_photons, **pi0_resolved_cut)
    Lmd_DD = make_lambda_DD()
    line_alg = make_bbaryon_4body(
        particles=[Lmd_DD, pions_charged, pions_charged, pi0_resolved],
        descriptor="[Lambda_b0 -> Lambda0 pi+ pi- pi0]cc",
        **Lb0_cuts_DD,
    )
    return line_alg
