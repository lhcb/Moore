###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Builds BNOC B decays, and defines the common default cuts applied to the B2X combinations
"""

import Functors as F
from Functors import require_all
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, picosecond
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.bnoc.utils import get_2b_combinations, get_3b_combinations

##############################################
# For BdsToKSKS lines                        #
##############################################


@configurable
def make_b2ksks(
    particles,
    descriptor,
    name="BNOC_b2ksksCombiner_{hash}",
    am_min=4200 * MeV,
    am_max=6300 * MeV,
    adoca_max=4 * mm,
    asumpt_min=1500 * MeV,
    vtx_am_min=4400 * MeV,
    vtx_am_max=6100 * MeV,
    vchi2pdof_max=40,
    dira_min=0.999,
    ltime_min=0.1 * picosecond,
    bcvtx_sep_min=0 * mm,
    AllowDiffInputsForSameIDChildren=False,
):
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.MAXSDOCACUT(adoca_max),
        F.SUM(F.PT) > asumpt_min,
    )

    vertex_code = F.require_all(
        in_range(vtx_am_min, F.MASS, vtx_am_max),
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVDIRA > dira_min,
        F.OWNPVLTIME > ltime_min,
        (F.CHILD(1, F.END_VZ) - F.END_VZ) > bcvtx_sep_min,
        (F.CHILD(2, F.END_VZ) - F.END_VZ) > bcvtx_sep_min,
    )
    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        AllowDiffInputsForSameIDChildren=AllowDiffInputsForSameIDChildren,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


##############################################
# For BdsToPpPmhh lines                       #
##############################################


@configurable
def make_B2PpPmhh(
    particles,
    descriptor,
    name="BNOC_B2PpPmhhCombiner_{hash}",
    am_min_ppbar=1000.0 * MeV,
    am_min_ppbarh=1000.0 * MeV,
    am_max_ppbar=4500.0 * MeV,
    am_max_ppbarh=5000.0 * MeV,
    adoca_chi2_ppbar=10.0,
    asum_PT_ppbar=750.0 * MeV,
    asum_P_ppbar=7000.0 * MeV,
    am_max_ppbarK=5600.0 * MeV,
    adoca_chi2_ppbarK=10.0,
    am_min_ppbarKpi=5050 * MeV,
    am_max_ppbarKpi=5550 * MeV,
    adoca_chi2_ppbarKpi=10.0,
    amaxdoca4h=0.2 * mm,
    ltime_min=0.2 * picosecond,
    comb_PTSUM_min=3000.0 * MeV,
    B_dira_min=0.9999,
    B_vtx_CHI2_max=9.0,
    bpvipchi2_max=10.0,
    daughter_mipchi2_min=6,
    B_PT_min=1000.0 * MeV,
    B_minip_max=0.1 * mm,
):
    combination12_code = F.require_all(
        in_range(am_min_ppbar, F.MASS, am_max_ppbar),
        F.SDOCACHI2(1, 2) < adoca_chi2_ppbar,
        F.SUM(F.PT) > asum_PT_ppbar,
        F.SUM(F.P) > asum_P_ppbar,
        F.MAXSDOCACHI2CUT(adoca_chi2_ppbar),
    )  # cuts on the ppbar combination

    combination123_code = F.require_all(
        in_range(am_min_ppbarh, F.MASS, am_max_ppbarh),
        F.SDOCACHI2(2, 3) < adoca_chi2_ppbar,
        F.SDOCACHI2(1, 3) < adoca_chi2_ppbar,
        F.MAXSDOCACHI2CUT(adoca_chi2_ppbarK),
    )  # cuts on the ppbarK combination

    combination_code = F.require_all(
        in_range(am_min_ppbarKpi - 50 * MeV, F.MASS, am_max_ppbarKpi + 50 * MeV),
        F.SDOCACHI2(1, 4) < adoca_chi2_ppbar,
        F.SDOCACHI2(2, 4) < adoca_chi2_ppbar,
        F.SDOCACHI2(3, 4) < adoca_chi2_ppbar,
        F.MAXSDOCACUT(amaxdoca4h),
        F.MAXSDOCACHI2CUT(adoca_chi2_ppbarKpi),
    )  # cuts on the ppbarKpi combination

    pvs = make_pvs()

    vertex_code = F.require_all(
        in_range(am_min_ppbarKpi, F.MASS, am_max_ppbarKpi),
        F.OWNPVDIRA > B_dira_min,
        F.CHI2DOF < B_vtx_CHI2_max,
        F.OWNPVLTIME > ltime_min,
        F.OWNPVIPCHI2 < bpvipchi2_max,
        F.SUM(F.PT) > comb_PTSUM_min,
        F.PT > B_PT_min,
        F.MIN(F.MINIPCHI2(pvs)) > daughter_mipchi2_min,
        F.MINIP(pvs) < B_minip_max,
    )  # cuts on the B0

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        Combination123Cut=combination123_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


##############################################
# For BdsTohhhh and BuToKShhh lines          #
##############################################

# aprox masses of particles: lower than true value because of detector resolution
m_K = 480.0 * MeV
m_pi = 120.0 * MeV


@configurable
def make_b2x(
    particles,
    descriptor,
    name="BNOC_B2XCombiner_{hash}",
    am_min=4500 * MeV,  # Beware TightCut!
    am_max=6300 * MeV,
    MassWindow=False,  # VV-AS inclusive mass window
    am123_max=1900.0 * MeV,
    am12_max=1900.0 * MeV,  # include D0
    am123_min=3 * m_pi,  # should be at production threshold
    am12_min=2 * m_pi,
    mothermass_min=4800 * MeV,
    mothermass_max=6100 * MeV,
    asumpt_min=1.5 * GeV,  # 5 GeV???
    motherpt_min=250.0 * MeV,  # 1500 * MeV Run12
    achi2doca_max=30.0,
    bpvipchi2_max=25.0,
    dira_min=0.999,
    ltime_min=0.2 * picosecond,
    mipchi2_max=None,
    daughter_mipchi2_min=None,
    adoca_max=None,
    vtxchi2pdof_max=20.0,
    charged=False,
):
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > asumpt_min,
        F.MAXSDOCACHI2CUT(achi2doca_max),
    )
    combination123_code = F.require_all(F.MAXSDOCACHI2CUT(achi2doca_max))
    combination12_code = F.require_all(F.MAXSDOCACHI2CUT(achi2doca_max))
    if MassWindow:
        # For BuToKshhh lines
        if charged:
            combination_code &= (
                (
                    F.SUBCOMB(
                        Functor=in_range(am12_min, F.MASS, am12_max), Indices=[1, 2]
                    )
                    & F.SUBCOMB(
                        Functor=in_range(am12_min, F.MASS, am12_max), Indices=[3, 4]
                    )
                )
                | (
                    F.SUBCOMB(
                        Functor=in_range(am12_min, F.MASS, am12_max), Indices=[1, 3]
                    )
                    & F.SUBCOMB(
                        Functor=in_range(am12_min, F.MASS, am12_max), Indices=[2, 4]
                    )
                )
                | (
                    F.SUBCOMB(
                        Functor=in_range(am123_min, F.MASS, am123_max),
                        Indices=[1, 2, 4],
                    )
                )
                | (
                    F.SUBCOMB(
                        Functor=in_range(am123_min, F.MASS, am123_max),
                        Indices=[1, 3, 4],
                    )
                )
                | (
                    F.SUBCOMB(
                        Functor=in_range(am123_min, F.MASS, am123_max),
                        Indices=[2, 3, 4],
                    )
                )
            )
        else:
            # Automatically figure out the physical 2- and 3- body mass combinations
            twobody_indices = get_2b_combinations(descriptor)
            threebody_indices = get_3b_combinations(descriptor)
            mass_code = F.SUBCOMB(
                Functor=in_range(am123_min, F.MASS, am123_max),
                Indices=threebody_indices[0],
            )
            for threeb_i in threebody_indices[1:]:
                mass_code |= F.SUBCOMB(
                    Functor=in_range(am123_min, F.MASS, am123_max), Indices=threeb_i
                )
            for twob_pair in twobody_indices:
                mass_code |= F.SUBCOMB(
                    Functor=in_range(am12_min, F.MASS, am12_max), Indices=twob_pair[0]
                ) & F.SUBCOMB(
                    Functor=in_range(am12_min, F.MASS, am12_max), Indices=twob_pair[1]
                )
            combination_code &= mass_code
    else:
        combination123_code &= in_range(am123_min, F.MASS, am123_max)
        combination12_code &= in_range(am12_min, F.MASS, am12_max)

    pvs = make_pvs()
    vertex_code = F.require_all(
        in_range(mothermass_min, F.MASS, mothermass_max),
        F.PT > motherpt_min,
        F.CHI2DOF < vtxchi2pdof_max,
        F.OWNPVIPCHI2 < bpvipchi2_max,
        F.OWNPVDIRA > dira_min,
    )
    if mipchi2_max is not None:
        vertex_code &= F.MINIPCHI2(pvs) < mipchi2_max
    if daughter_mipchi2_min is not None:
        vertex_code &= F.MIN(F.MINIPCHI2(pvs)) > daughter_mipchi2_min
    if ltime_min is not None:
        F.OWNPVLTIME > ltime_min
    if adoca_max is not None:
        combination12_code &= F.SDOCA(1, 2) < adoca_max
        combination123_code &= F.require_all(
            F.SDOCA(1, 3) < adoca_max, F.SDOCA(2, 3) < adoca_max
        )
        combination_code &= F.require_all(
            F.SDOCA(1, 4) < adoca_max,
            F.SDOCA(2, 4) < adoca_max,
            F.SDOCA(3, 4) < adoca_max,
        )

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        Combination123Cut=combination123_code,
        Combination12Cut=combination12_code,
    )


@configurable
def make_lifetime_unbiased_b2x(
    particles,
    descriptor,
    name="BNOC_B2XLTUCombiner_{hash}",
    am_min=5050 * MeV,
    am_max=5650 * MeV,
    am_min_vtx=5050 * MeV,
    am_max_vtx=5650 * MeV,
    sum_pt_min=5 * GeV,
    vtx_chi2pdof_max=20.0,
):  # was 10 in Run1+2
    """
    LifeTime Unbiased B decay maker: defines default cuts and B mass range.
    """
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.SUM(F.PT) > sum_pt_min
    )

    vertex_code = F.require_all(
        in_range(am_min_vtx, F.MASS, am_max_vtx), F.CHI2DOF < vtx_chi2pdof_max
    )

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


##############################################
# For BcTohhh and BuTohhh lines              #
##############################################


@configurable
def make_b2hhh(
    particles,
    descriptor,
    name="BNOC_B2HHHCombiner_{hash}",
    am_min=4950 * MeV,
    am_max=5750 * MeV,
    am_min_vtx=5000 * MeV,
    am_max_vtx=5700 * MeV,
    PT_sum_min=2.5 * GeV,
    FDCHI2=500,
    PVDOCAmin=3.0,
    IPCHI2_min=10,
    PVIPCHI2sum=500,
    vtx_chi2pdof_max=10,
    bpvipchi2_max=25,
    bpvltime_min=0.2 * picosecond,
    bpvltime_max=None,
    bpvdira_min=0.9999,
    adoca_max=0.2 * mm,
):
    """
    Specialised 3-body Hb --> (h h h)  decay maker
    """

    combination12_code = F.require_all(F.SDOCA(1, 2) < adoca_max)

    pvs = make_pvs()
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > PT_sum_min,
        F.SUM(F.MINIPCHI2(pvs)) > PVIPCHI2sum,
    )

    vertex_code = F.require_all(
        in_range(am_min_vtx, F.MASS, am_max_vtx),
        F.CHI2DOF < vtx_chi2pdof_max,
        F.OWNPVIPCHI2 < bpvipchi2_max,
        F.OWNPVFDCHI2 > FDCHI2,
        F.MIN_ELEMENT @ F.ALLPV_FD(pvs) > PVDOCAmin,
        F.MINIPCHI2(pvs) < IPCHI2_min,
        F.OWNPVLTIME > bpvltime_min,
        F.OWNPVDIRA > bpvdira_min,
    )

    if bpvltime_max is not None:
        vertex_code &= F.OWNPVLTIME < bpvltime_max

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


##############################################
# For bTohh lines                            #
##############################################


@configurable
def make_b2Lambdah(
    particles,
    descriptor,
    name="BNOC_b2LambdahCombiner_{hash}",
    apt_min=1000.0 * MeV,
    apt1_min=500.0 * MeV,
    sumpt_min=800.0 * MeV,
    sumpt_num_min=2,
    am_min=4800.0 * MeV,
    am_max=7000.0 * MeV,
    acutdocachi2=5.0,
    vt_pt=800.0 * MeV,
    vt_vchi2pdof_max=12.0,
    vt_bpvdira_min=0.995,
    mipchidv_max=15.0,
    bpvfdchi2_min=30.0,
):
    """
    For B+->Lambda~0 p+,
        Bc+->Lambda~0 p+
        Xi_b- -> Lambda0 pi-,
        Xi_b- -> Lambda0 K-
    """

    combination_code = require_all(
        F.PT > apt_min,
        F.CHILD(1, F.PT) > apt1_min,
        F.SUM(F.PT > sumpt_min) >= sumpt_num_min,
        in_range(am_min, F.MASS, am_max),
        F.MAXSDOCACHI2CUT(acutdocachi2),
    )

    pvs = make_pvs()

    vertex_code = require_all(
        F.PT > vt_pt,
        F.OWNPVDIRA > vt_bpvdira_min,
        F.CHI2DOF < vt_vchi2pdof_max,
        F.MINIPCHI2(pvs) < mipchidv_max,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
    )
    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_b2Kpi(
    particles,
    descriptor,
    name="BNOC_b2KpiCombiner_{hash}",
    comb_m_min=4000 * MeV,
    comb_m_max=6200 * MeV,
    comb_pt_min=5000 * MeV,
    mtdocachi2_max=8.0,
    pt_min=5000 * MeV,
):
    combination_code = require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.SUM(F.PT) > comb_pt_min,
    )

    pvs = make_pvs()
    composite_code = require_all(
        F.MTDOCACHI2(1, pvs) < mtdocachi2_max,
        F.PT > pt_min,
    )
    return ParticleCombiner(
        particles,
        ParticleCombiner="ParticleAdder",
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


##############################################
# For BuToKSh lines                          #
##############################################


@configurable
def make_b2ksh(
    particles,
    descriptor,
    name="BNOC_BC2KSHCombiner_{hash}",
    am_min=4950.0 * MeV,
    am_max=6500.0 * MeV,
    amed_pt_min=4000.0 * MeV,
    p_min=25000.0 * MeV,
    vchi2pdof_max=10.0,
    bpvdira_min=0.9995,
    mipchi2_max=10.0,
    bpvfdchi2_min=50.0,
    pt_min=1500 * MeV,
):
    med_pt_cut = (F.CHILD(1, F.PT) + F.CHILD(2, F.PT)) > amed_pt_min

    combination_code = F.require_all(
        in_range(am_min - 50 * MeV, F.MASS, am_max + 50 * MeV), med_pt_cut
    )
    pvs = make_pvs()

    vertex_code = F.require_all(
        F.P > p_min,
        in_range(am_min, F.MASS, am_max),
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVDIRA > bpvdira_min,
        F.MINIPCHI2(pvs) < mipchi2_max,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
        F.PT > pt_min,
    )
    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


##############################################
# For BdTohh lines                           #
##############################################


@configurable
def make_b2hh(
    particles,
    descriptor,
    name="BNOC_B2HHCombiner_{hash}",
    am_min=4700 * MeV,
    am_max=6200 * MeV,
    sum_pt=4500 * MeV,
    docachi2=9.0,
    pt_min=1200 * MeV,
    dira_min=0.99,
    ipchi2_max=9,
    fdchi2_min=100,
    AllowDiffInputsForSameIDChildren=False,
):
    combination_cut = require_all(
        F.SUM(F.PT) > sum_pt,
        in_range(am_min, F.MASS, am_max),
        F.MAXSDOCACHI2CUT(docachi2),
    )
    composite_cut = require_all(
        F.PT > pt_min,
        F.OWNPVDIRA > dira_min,
        F.OWNPVIPCHI2 < ipchi2_max,
    )
    if fdchi2_min is not None:
        composite_cut = require_all(composite_cut, F.OWNPVFDCHI2 > fdchi2_min)

    return ParticleCombiner(
        particles,
        DecayDescriptor=descriptor,
        name=name,
        CombinationCut=combination_cut,
        CompositeCut=composite_cut,
        AllowDiffInputsForSameIDChildren=AllowDiffInputsForSameIDChildren,
    )


##################################################
#### Builders for 2-/3-/4-body bbaryon decays ####
##################################################

### Currently most of the baryonic lines are Xibm/Ombm, so defualt mass window is tuned for those decays, so a different sent of values has to be passed manually when using for Lambda0/Xib0


@configurable
def make_bbaryon_2body(
    particles,
    descriptor,
    mass_min=5395 * MeV,
    mass_max=6305 * MeV,
    pt_min=1500 * MeV,
    pt_sum_min=2000 * MeV,
    docachi2_max=25.0,
    vchi2pdof_max=10.0,
    ipchi2_max=25.0,
    bpvfdchi2_min=40.0,
    dira_min=0.9995,
    bpvltime_min=0.4 * picosecond,
    bcvtx_sep_min=10 * mm,
    daughter_index=1,
):
    """Builder for two-body b-baryon decay."""
    name = "BNOC_bbaryon_2body_{hash}"

    combination_cut = require_all(
        in_range(mass_min - 100 * MeV, F.MASS, mass_max + 100 * MeV),
        F.PT > pt_min,
        F.CHILD(1, F.PT) + F.CHILD(2, F.PT) > pt_sum_min,
        F.MAXSDOCACHI2CUT(docachi2_max),
    )

    composite_cut = require_all(
        in_range(mass_min, F.MASS, mass_max),
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVDIRA > dira_min,
        F.OWNPVIPCHI2 < ipchi2_max,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
        F.OWNPVLTIME > bpvltime_min,
    )

    # To be used only for lines with composite daughter (KS0, L0, Xi-, Omega-)
    if bcvtx_sep_min is not None:
        composite_cut &= F.CHILD(daughter_index, F.END_VZ) - F.END_VZ > bcvtx_sep_min

    return ParticleCombiner(
        Inputs=particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_cut,
        CompositeCut=composite_cut,
    )


@configurable
def make_bbaryon_3body(
    particles,
    descriptor,
    mass_min=5395 * MeV,
    mass_max=6305 * MeV,
    pt_min=1500 * MeV,
    pt_sum_min=2000 * MeV,
    docachi2_max=15.0,
    vchi2pdof_max=10.0,
    ipchi2_max=25.0,
    bpvfdchi2_min=40.0,
    dira_min=0.9995,
    bpvltime_min=0.4 * picosecond,
    med_pt_min=400 * MeV,
    med_bpvipchi2_min=15,
    bpvipchi2_sum_min=100,
    bcvtx_sep_min=10 * mm,
    daughter_index=1,
):
    """Builder for three-body b-baryon decay."""
    name = "BNOC_bbaryon_3body_{hash}"

    combination12_code = require_all(F.MASS < mass_max, F.MAXSDOCACHI2CUT(docachi2_max))

    combination_cut = require_all(
        in_range(mass_min - 100 * MeV, F.MASS, mass_max + 100 * MeV),
        F.CHILD(1, F.PT) + F.CHILD(2, F.PT) + F.CHILD(3, F.PT) > pt_sum_min,
        F.PT > pt_min,
        F.MAXSDOCACHI2CUT(docachi2_max),
    )

    # optional cut requiring that 2 out of 3 of the children have PT > med_pt_min
    if med_pt_min is not None:
        combination_cut &= F.SUM(F.PT > med_pt_min) >= 2

    composite_cut = require_all(
        in_range(mass_min, F.MASS, mass_max),
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVDIRA > dira_min,
        F.OWNPVIPCHI2 < ipchi2_max,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
        F.OWNPVLTIME > bpvltime_min,
    )

    # optional cut requiring that 2 out of 3 of the children have IP chisq > med_bpvipchi2_min
    if med_bpvipchi2_min is not None:
        composite_cut &= F.SUM(F.OWNPVIPCHI2 > med_bpvipchi2_min) >= 2

    # optional cut requiring that the sum of the IP chisq of all tracks in the decay tree > bpvipchi2_sum_min
    if bpvipchi2_sum_min is not None:
        composite_cut &= (
            F.SUM_RANGE @ F.MAP(F.OWNPVIPCHI2) @ F.FILTER(F.HASOWNPV) @ F.GET_ALL_BASICS
        ) > bpvipchi2_sum_min

    # To be used only for lines with composite daughter (KS0, L0, Xi-, Omega-)
    if bcvtx_sep_min is not None:
        composite_cut &= F.CHILD(daughter_index, F.END_VZ) - F.END_VZ > bcvtx_sep_min

    return ParticleCombiner(
        Inputs=particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        CombinationCut=combination_cut,
        CompositeCut=composite_cut,
    )


@configurable
def make_bbaryon_4body(
    particles,
    descriptor,
    mass_min=5395 * MeV,
    mass_max=6305 * MeV,
    pt_min=1500 * MeV,
    pt_sum_min=2000 * MeV,
    docachi2_max=15.0,
    vchi2pdof_max=10.0,
    ipchi2_max=25.0,
    bpvfdchi2_min=40.0,
    dira_min=0.9995,
    bpvltime_min=0.4 * picosecond,
    med_pt_min=350 * MeV,
    med_bpvipchi2_min=15,
    bpvipchi2_sum_min=100,
    bcvtx_sep_min=10 * mm,
    daughter_index=1,
):
    """Builder for four-body b-baryon decay."""
    name = "BNOC_bbaryon_4body_{hash}"

    combination12_code = require_all(F.MASS < mass_max, F.MAXSDOCACHI2CUT(docachi2_max))

    combination123_code = F.require_all(
        F.MASS < mass_max, F.MAXSDOCACHI2CUT(docachi2_max)
    )

    combination_cut = require_all(
        in_range(mass_min - 100 * MeV, F.MASS, mass_max + 100 * MeV),
        F.PT > pt_min,
        F.CHILD(1, F.PT) + F.CHILD(2, F.PT) + F.CHILD(3, F.PT) + F.CHILD(4, F.PT)
        > pt_sum_min,
        F.MAXSDOCACHI2CUT(docachi2_max),
    )

    # optional cut requiring that 2 out of 4 of the children have PT > med_pt_min
    if med_pt_min is not None:
        combination_cut &= F.SUM(F.PT > med_pt_min) >= 2

    composite_cut = require_all(
        in_range(mass_min, F.MASS, mass_max),
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVDIRA > dira_min,
        F.OWNPVIPCHI2 < ipchi2_max,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
        F.OWNPVLTIME > bpvltime_min,
    )

    # optional cut requiring that 2 out of 4 of the children have IP chisq > med_bpvipchi2_min
    if med_bpvipchi2_min is not None:
        composite_cut &= F.SUM(F.OWNPVIPCHI2 > med_bpvipchi2_min) >= 2

    # optional cut requiring that the sum of the IP chisq of all tracks in the decay tree > bpvipchi2_sum_min
    if bpvipchi2_sum_min is not None:
        composite_cut &= (
            F.SUM_RANGE @ F.MAP(F.OWNPVIPCHI2) @ F.FILTER(F.HASOWNPV) @ F.GET_ALL_BASICS
        ) > bpvipchi2_sum_min

    # To be used only for lines with composite daughter (KS0, L0, Xi-, Omega-)
    if bcvtx_sep_min is not None:
        composite_cut &= F.CHILD(daughter_index, F.END_VZ) - F.END_VZ > bcvtx_sep_min

    return ParticleCombiner(
        Inputs=particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        Combination123Cut=combination123_code,
        CombinationCut=combination_cut,
        CompositeCut=composite_cut,
    )


@configurable
def make_bbaryon_5body(
    particles,
    descriptor,
    mass_min=5395 * MeV,
    mass_max=6305 * MeV,
    pt_min=1500 * MeV,
    pt_sum_min=2000 * MeV,
    docachi2_max=20.0,
    vchi2pdof_max=10.0,
    ipchi2_max=25.0,
    bpvfdchi2_min=40.0,
    dira_min=0.9995,
    bpvltime_min=0.4 * picosecond,
    med_pt_min=300 * MeV,
    med_bpvipchi2_min=10,
    bpvipchi2_sum_min=100,
    bcvtx_sep_min=10 * mm,
    daughter_index=1,
):
    """Builder for five-body b-baryon decay."""
    name = "BNOC_bbaryon_5body_{hash}"

    combination12_code = require_all(F.MASS < mass_max, F.MAXSDOCACHI2CUT(docachi2_max))

    combination123_code = F.require_all(
        F.MASS < mass_max, F.MAXSDOCACHI2CUT(docachi2_max)
    )

    combination_cut = require_all(
        in_range(mass_min - 100 * MeV, F.MASS, mass_max + 100 * MeV),
        F.PT > pt_min,
        F.CHILD(1, F.PT)
        + F.CHILD(2, F.PT)
        + F.CHILD(3, F.PT)
        + F.CHILD(4, F.PT)
        + F.CHILD(5, F.PT)
        > pt_sum_min,
        F.MAXSDOCACHI2CUT(docachi2_max),
    )

    # optional cut requiring that 2 out of 5 of the children have PT > med_pt_min
    if med_pt_min is not None:
        combination_cut &= F.SUM(F.PT > med_pt_min) >= 2

    composite_cut = require_all(
        in_range(mass_min, F.MASS, mass_max),
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVDIRA > dira_min,
        F.OWNPVIPCHI2 < ipchi2_max,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
        F.OWNPVLTIME > bpvltime_min,
    )

    # optional cut requiring that 2 out of 5 of the children have IP chisq > med_bpvipchi2_min
    if med_bpvipchi2_min is not None:
        composite_cut &= F.SUM(F.OWNPVIPCHI2 > med_bpvipchi2_min) >= 2

    # optional cut requiring that the sum of the IP chisq of all tracks in the decay tree > bpvipchi2_sum_min
    if bpvipchi2_sum_min is not None:
        composite_cut &= (
            F.SUM_RANGE @ F.MAP(F.OWNPVIPCHI2) @ F.FILTER(F.HASOWNPV) @ F.GET_ALL_BASICS
        ) > bpvipchi2_sum_min

    # To be used only for lines with composite daughter (KS0, L0, Xi-, Omega-)
    if bcvtx_sep_min is not None:
        composite_cut &= (F.CHILD(daughter_index, F.END_VZ) - F.END_VZ) > bcvtx_sep_min

    return ParticleCombiner(
        Inputs=particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        Combination123Cut=combination123_code,
        CombinationCut=combination_cut,
        CompositeCut=composite_cut,
    )


@configurable
def make_bbaryon_6body(
    particles,
    descriptor,
    mass_min=5395 * MeV,
    mass_max=6305 * MeV,
    pt_min=1500 * MeV,
    pt_sum_min=2500 * MeV,
    docachi2_max=20.0,
    vchi2pdof_max=10.0,
    ipchi2_max=25.0,
    bpvfdchi2_min=40.0,
    dira_min=0.9995,
    med_pt_min=300 * MeV,
    med_bpvipchi2_min=10,
    bpvipchi2_sum_min=100,
    bpvltime_min=0.4 * picosecond,
    bcvtx_sep_min=10 * mm,
    daughter_index=1,
):
    """Builder for six-body b-baryon decay."""
    name = "BNOC_bbaryon_6body_{hash}"

    combination12_code = require_all(F.MASS < mass_max, F.MAXSDOCACHI2CUT(docachi2_max))

    combination123_code = F.require_all(
        F.MASS < mass_max, F.MAXSDOCACHI2CUT(docachi2_max)
    )

    combination_cut = require_all(
        in_range(mass_min - 100 * MeV, F.MASS, mass_max + 100 * MeV),
        F.PT > pt_min,
        F.CHILD(1, F.PT)
        + F.CHILD(2, F.PT)
        + F.CHILD(3, F.PT)
        + F.CHILD(4, F.PT)
        + F.CHILD(5, F.PT)
        + F.CHILD(6, F.PT)
        > pt_sum_min,
        F.MAXSDOCACHI2CUT(docachi2_max),
    )

    # optional cut requiring that 3 out of 6 of the children have PT > med_pt_min
    if med_pt_min is not None:
        combination_cut &= F.SUM(F.PT > med_pt_min) >= 3

    composite_cut = require_all(
        in_range(mass_min, F.MASS, mass_max),
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVDIRA > dira_min,
        F.OWNPVIPCHI2 < ipchi2_max,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
        F.OWNPVLTIME > bpvltime_min,
    )

    # optional cut requiring that 3 out of 6 of the children have IP chisq > med_bpvipchi2_min
    if med_bpvipchi2_min is not None:
        composite_cut &= F.SUM(F.OWNPVIPCHI2 > med_bpvipchi2_min) >= 3

    # optional cut requiring that the sum of the IP chisq of all tracks in the decay tree > bpvipchi2_sum_min
    if bpvipchi2_sum_min is not None:
        composite_cut &= (
            F.SUM_RANGE @ F.MAP(F.OWNPVIPCHI2) @ F.FILTER(F.HASOWNPV) @ F.GET_ALL_BASICS
        ) > bpvipchi2_sum_min

    # To be used only for lines with composite daughter (KS0, L0, Xi-, Omega-)
    if bcvtx_sep_min is not None:
        composite_cut &= (F.CHILD(daughter_index, F.END_VZ) - F.END_VZ) > bcvtx_sep_min

    return ParticleCombiner(
        Inputs=particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        Combination123Cut=combination123_code,
        CombinationCut=combination_cut,
        CompositeCut=composite_cut,
    )


############################################
# For BdsToKShh lines                      #
############################################


@configurable
def make_b2kshh(
    particles,
    descriptor,
    name="BNOC_B2KSHHCombiner_{hash}",
    asumpt_min=4400.0 * MeV,
    amed_pt_min=750.0 * MeV,
    hh_amaxpt_min=750.0 * MeV,
    hh_amaxp_min=10.0 * GeV,
    adoca23_max=15.0,
    adoca12_max=25.0,
    adoca13_max=25.0,
    m_min=5000.0 * MeV,
    m_max=6000.0 * MeV,
    pt_min=1500 * MeV,
    vchi2pdof_max=12.0,
    ksvtx_sep_min=15.0 * mm,
    bpvipchi2_max=9.0,
    bpvfdchi2_min=50.0,
    bpvdira_min=0.999,
    hh_bpvipchi2_min=50.0,
    bpvipchi2_sum_min=300.0,
):
    """
    A generic 3body decay maker. Makes use of ThreeBodyCombiner
    to be more efficient, first making a DOCAcut on the *2 first particles in the
    decay descriptor*.

    Parameters
    ----------
    particles
        Maker algorithm instances for input particles.
    descriptor : string
        Decay descriptor to be reconstructed.
    make_pvs : callable
        Primary vertex maker function.
    Remaining parameters define thresholds for the selection.
    """
    combination12_cut = require_all(F.SDOCACHI2(1, 2) < adoca12_max)

    combination_cut = require_all(
        in_range(m_min - 100 * MeV, F.MASS, m_max + 100 * MeV),
        F.SUM(F.PT) > asumpt_min,
        F.SUM(F.PT > amed_pt_min) >= 2,
        F.SUBCOMB(Functor=F.MAX(F.P), Indices=[2, 3]) > hh_amaxp_min,
        F.SUBCOMB(Functor=F.MAX(F.PT), Indices=[2, 3]) > hh_amaxpt_min,
        F.SDOCACHI2(1, 3) < adoca13_max,
        F.SDOCACHI2(2, 3) < adoca23_max,
    )

    hh_ipchi2_cut = (
        F.CHILD(2, F.OWNPVIPCHI2) + F.CHILD(3, F.OWNPVIPCHI2)
    ) > hh_bpvipchi2_min
    sum_ipchi2_cut = (
        F.SUM_RANGE @ F.MAP(F.OWNPVIPCHI2) @ F.FILTER(F.HASOWNPV) @ F.GET_ALL_BASICS
    ) > bpvipchi2_sum_min
    ksvtx_sep_cut = (F.CHILD(1, F.END_VZ) - F.END_VZ) > ksvtx_sep_min

    vertex_cut = require_all(
        F.PT > pt_min,
        in_range(m_min, F.MASS, m_max),
        ksvtx_sep_cut,
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVDIRA > bpvdira_min,
        F.OWNPVIPCHI2 < bpvipchi2_max,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
        hh_ipchi2_cut,
        sum_ipchi2_cut,
    )

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_cut,
        CombinationCut=combination_cut,
        CompositeCut=vertex_cut,
    )


##############################################
# For BdsToPpPmKS lines                      #
##############################################


@configurable
def make_b2kspp(
    particles,
    descriptor,
    name="BNOC_b2kshhCombiner_{hash}",
    am_min=5000.0 * MeV,
    am_max=6000.0 * MeV,
    sum_pt_min=3.5 * GeV,
    pt_min=1500 * MeV,
    hh_amaxp_min=10.0 * GeV,
    amed_pt_min=1300.0 * MeV,
    vchi2pdof_max=12.0,
    bpvipchi2_max=9.0,
    bpvdira_min=0.999,
    bpvfdchi2_min=50.0,
):
    combination_code = F.require_all(
        in_range(0.99 * am_min, F.MASS, 1.01 * am_max),
        ((F.CHILD(1, F.PT) + F.CHILD(2, F.PT)) > amed_pt_min),
        F.SUBCOMB(Functor=F.MAX(F.P), Indices=[1, 2]) > hh_amaxp_min,
        F.SUM(F.PT) > sum_pt_min,
    )

    vertex_code = F.require_all(
        F.PT > pt_min,
        in_range(am_min, F.MASS, am_max),
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVIPCHI2 < bpvipchi2_max,
        F.OWNPVDIRA > bpvdira_min,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
    )

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


##############################################
# For BdsToppbar(ppbar) lines                #
##############################################


# https://lhcbdoc.web.cern.ch/lhcbdoc/stripping/config/stripping34/bhadron/strippingb24pb24pline.html
@configurable
def make_bds2ppbarppbar(
    particles,
    descriptor,
    name="BNOC_bds2ppbarppbarCombiner_{hash}",
    am_min_mass=5000.0 * MeV,
    am_max_mass=5700.0 * MeV,
    vtx_min_mass=5020.0 * MeV,
    vtx_max_mass=5680.0 * MeV,
    vtx_max_chi2=9.0,
    vtx_min_dira=0.9997,
    vtx_max_ipchi2=16.0,
    vtx_min_pt=1500.0 * MeV,
    sdocachi2_max=20.0,
    b_min_lft=0.2 * picosecond,
    b_min_fdchi2=100,
):
    combination12_code = F.require_all(
        F.MAXSDOCACHI2CUT(sdocachi2_max), F.MASS < am_max_mass
    )

    combination123_code = F.MAXSDOCACHI2CUT(sdocachi2_max)

    combination_code = F.require_all(  ## form the ppbarppbar
        in_range(am_min_mass, F.MASS, am_max_mass), F.MAXSDOCACHI2CUT(sdocachi2_max)
    )

    vertex_code = F.require_all(  ## fit to the B0(s)->ppbarppbar
        in_range(vtx_min_mass, F.MASS, vtx_max_mass),
        F.CHI2DOF < vtx_max_chi2,
        F.OWNPVIPCHI2 < vtx_max_ipchi2,
        F.OWNPVDIRA > vtx_min_dira,
        F.OWNPVFDCHI2 > b_min_fdchi2,
        F.OWNPVLTIME > b_min_lft,
        F.PT > vtx_min_pt,
    )

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        Combination123Cut=combination123_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


# https://lhcbdoc.web.cern.ch/lhcbdoc/stripping/config/stripping34/bhadroncompleteevent/strippingb2twobaryonsb2ppbarline.html
@configurable
def make_bds2ppbar(
    particles,
    descriptor,
    name="BNOC_bds2ppbarCombiner_{hash}",
    am_min_mass=5000.0 * MeV,
    am_max_mass=5700.0 * MeV,
    vtx_min_mass=5020.0 * MeV,
    vtx_max_mass=5680.0 * MeV,
    comb_max_pt=2100.0 * MeV,
    vtx_max_chi2=9.0,
    vtx_min_pt=2000.0 * MeV,
    am_max_ipchi2=25,
    vtx_max_ipchi2=16,
    b0_dira=0.9997,
    b_min_lft=0.2 * picosecond,
):
    combination_code = F.require_all(
        in_range(am_min_mass, F.MASS, am_max_mass),
        F.MAX(F.PT) > comb_max_pt,
        F.MAX(F.OWNPVIPCHI2) > am_max_ipchi2,
    )

    vertex_code = F.require_all(
        in_range(vtx_min_mass, F.MASS, vtx_max_mass),
        F.CHI2DOF < vtx_max_chi2,
        F.OWNPVIPCHI2 < vtx_max_ipchi2,
        F.OWNPVDIRA > b0_dira,
        F.OWNPVLTIME > b_min_lft,
        F.PT > vtx_min_pt,
    )

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


##############################################
# For BdsToVV lines                          #
##############################################


@configurable
def make_btovv(
    particles,
    descriptor,
    am_min=4900 * MeV,
    am_max=6000 * MeV,
    vtx_am_min=4950 * MeV,
    vtx_am_max=5950 * MeV,
    name="BNOC_b2vvCombiner_{hash}",
    asumpt_min=1800 * MeV,
    ptproduct_min=0.0 * GeV * GeV,
    adoca12_max=0.5 * mm,
    vchi2pdof_max=10,
    mipchi2_max=None,
    ltime_min=None,
    bpvfdchi2_min=36,
    bpvdira_min=0.999,
):
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        (F.CHILD(1, F.PT) * F.CHILD(2, F.PT)) > ptproduct_min,
    )
    if asumpt_min is not None:
        combination_code &= F.SUM(F.PT) > asumpt_min
    if adoca12_max is not None:
        combination_code &= F.SDOCA(1, 2) < adoca12_max

    pvs = make_pvs()
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max, in_range(vtx_am_min, F.MASS, vtx_am_max)
    )
    if bpvfdchi2_min is not None:
        vertex_code &= F.OWNPVFDCHI2 > bpvfdchi2_min
    if bpvdira_min is not None:
        vertex_code &= F.OWNPVDIRA > bpvdira_min
    if mipchi2_max is not None:
        vertex_code &= F.MINIPCHI2(pvs) < mipchi2_max
    if ltime_min is not None:
        vertex_code &= F.OWNPVLTIME > ltime_min

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


##############################################
# For BdsToVKpKm lines                      #
##############################################


@configurable
def make_BdsToVKpKm(
    particles,
    descriptor,
    name="BNOC_BdsVKpKmCombiner_{hash}",
    asumpt_min=2000.0 * MeV,
    adoca_max=0.3 * mm,
    m_min=5000.0 * MeV,
    m_max=6000.0 * MeV,
    pt_min=1500 * MeV,
    kk_min=1050 * MeV,
    kk_max=5000 * MeV,
    vchi2pdof_max=9.0,
    bpvipchi2_max=20.0,
    bpvdira_min=0.999,
):
    combination12_cut = require_all(F.SDOCACHI2(1, 2) < adoca_max)

    combination_cut = require_all(
        in_range(m_min - 50 * MeV, F.MASS, m_max + 50 * MeV),
        F.SUM(F.PT) > asumpt_min,
        F.SDOCACHI2(1, 3) < adoca_max,
        F.SDOCACHI2(2, 3) < adoca_max,
    )
    combination_cut &= F.SUBCOMB(
        Functor=in_range(kk_min, F.MASS, kk_max), Indices=[2, 3]
    )

    vertex_cut = require_all(
        F.PT > pt_min,
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVDIRA > bpvdira_min,
        F.OWNPVIPCHI2 < bpvipchi2_max,
    )

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_cut,
        CombinationCut=combination_cut,
        CompositeCut=vertex_cut,
    )


##############################################
# For BudsToEtaKst lines                      #
##############################################
@configurable
def make_BToEtaKaon(
    particles,
    descriptor,
    name="BNOC_BToEtaKaon_Combiner_{hash}",
    am_min=4750 * MeV,  # Adjust these values as needed
    am_max=6050 * MeV,  # Adjust these values as needed
    vtx_am_min=4800 * MeV,  # Adjust these values as needed
    vtx_am_max=6000 * MeV,  # Adjust these values as needed
    make_pvs=make_pvs,
    asumpt_min=2000 * MeV,
    adoca12_max=0.3 * mm,
    vchi2pdof_max=16,
    mipchi2_max=9,
    ltime_min=0.1 * picosecond,
    AllowDiffInputsForSameIDChildren=False,
    bpvfdchi2_min=30.0,
    bpvdira_min=0.99,
):
    combination_code = F.require_all(in_range(am_min, F.MASS, am_max))
    if asumpt_min is not None:
        combination_code &= F.SUM(F.PT) > asumpt_min
    if adoca12_max is not None:
        combination_code &= F.SDOCA(1, 2) < adoca12_max

    pvs = make_pvs()
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max, in_range(vtx_am_min, F.MASS, vtx_am_max)
    )
    if bpvfdchi2_min is not None:
        vertex_code &= F.OWNPVFDCHI2 > bpvfdchi2_min
    if bpvdira_min is not None:
        vertex_code &= F.OWNPVDIRA > bpvdira_min
    if mipchi2_max is not None:
        vertex_code &= F.MINIPCHI2(pvs) < mipchi2_max
    if ltime_min is not None:
        vertex_code &= F.OWNPVLTIME > ltime_min

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        AllowDiffInputsForSameIDChildren=AllowDiffInputsForSameIDChildren,
        CompositeCut=vertex_code,
    )


##############################################
# For BdsToTwoCharmlessBaryons lines                           #
##############################################


@configurable
def make_BdsToTwoCharmlessBaryons(
    particle1,
    particle2,
    descriptor,
    name="BdsToTwoCharmlessBaryonsCombiner_{hash}",
    am_Min=4800.0 * MeV,
    am_Max=6000.0 * MeV,
    m_Min=5000.0 * MeV,
    m_Max=5900.0 * MeV,
    bcvtx_sep_min=None,
    VCHI2PDOF_Max=None,
    BPVIPCHI2_Max=None,
):
    combination_code = F.require_all(in_range(am_Min, F.MASS, am_Max))
    vertex_code = F.require_all(in_range(m_Min, F.MASS, m_Max))
    if bcvtx_sep_min is not None:
        vertex_code &= (F.CHILD(1, F.END_VZ) - F.END_VZ) > bcvtx_sep_min
        vertex_code &= (F.CHILD(2, F.END_VZ) - F.END_VZ) > bcvtx_sep_min
    if VCHI2PDOF_Max is not None:
        vertex_code &= F.CHI2DOF < VCHI2PDOF_Max
    if BPVIPCHI2_Max is not None:
        vertex_code &= F.OWNPVIPCHI2 < BPVIPCHI2_Max
    return ParticleCombiner(
        Inputs=[particle1, particle2],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


##############################################
# For BuToKpipipipi and BuTopipipipipi lines                           #
##############################################


@configurable
def make_bTo5h(
    particles,
    descriptor,
    name="BNOC_Bto5hCombiner_{hash}",
    am_min=4900 * MeV,
    am_max=5900 * MeV,
    asumpt_min=1.0 * GeV,
    achi2doca_max=30.0,
    PVIPCHI2sum=400,
    mothermass_min=5079 * MeV,
    mothermass_max=5679 * MeV,
    motherpt_min=1.0 * GeV,
    vtxchi2pdof_max=4.0,
    bpvipchi2_max=25.0,
    dira_min=0.99995,
    mipchi2_max=10,
    daughter_mipchi2_min=6,
    ltime_min=0.2 * picosecond,
    bpvfdchi2_min=200,
):
    pvs = make_pvs()
    ### basic combiantion requirement
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > asumpt_min,
        F.MAXSDOCACHI2CUT(achi2doca_max),
    )
    if PVIPCHI2sum is not None:
        combination_code &= F.SUM(F.MINIPCHI2(pvs)) > PVIPCHI2sum

    ### vertex requirement
    vertex_code = F.require_all(
        in_range(mothermass_min, F.MASS, mothermass_max),
        F.PT > motherpt_min,
        F.CHI2DOF < vtxchi2pdof_max,
        F.OWNPVIPCHI2 < bpvipchi2_max,
        F.OWNPVDIRA > dira_min,
    )
    if mipchi2_max is not None:
        vertex_code &= F.MINIPCHI2(pvs) < mipchi2_max
    if daughter_mipchi2_min is not None:
        vertex_code &= F.MIN(F.MINIPCHI2(pvs)) > daughter_mipchi2_min
    if ltime_min is not None:
        vertex_code &= F.OWNPVLTIME > ltime_min
    if bpvfdchi2_min is not None:
        vertex_code &= F.OWNPVFDCHI2 > bpvfdchi2_min

    ### make the Combiner
    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
