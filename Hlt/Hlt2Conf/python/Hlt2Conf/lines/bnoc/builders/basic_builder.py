###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of BnoC basic objects: pions, kaons, ..."""

import Functors as F
from Functors import require_all
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, ps
from GaudiKernel.SystemOfUnits import micrometer as um
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    make_detached_dielectron_with_brem,
    make_down_kaons,
    make_down_pions_for_V0,
    make_down_protons_for_V0,
    make_has_rich_down_pions,
    make_has_rich_long_deuterons,
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
    make_has_rich_long_protons,
    make_LambdaDD,
    make_LambdaLL,
    make_long_and_upstream_electrons_no_brem,
    make_long_pions_for_V0,
    make_long_protons_for_V0,
    make_photons,
)
from RecoConf.standard_particles import make_merged_pi0s as standard_merged_pi0

from Hlt2Conf.lines.topological_b import (
    make_filtered_topo_threebody,
    make_filtered_topo_twobody,
)

####################################
# Track selections                 #
####################################


@configurable
def filter_particles(
    make_particles, make_pvs=make_pvs, mipchi2_min=4, pt_min=250 * MeV, p_min=1 * GeV
):
    pvs = make_pvs()
    code = F.require_all(F.PT > pt_min, F.P > p_min, F.MINIPCHI2(pvs) > mipchi2_min)
    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_pions(pi_pidk_max=5.0, p_min=1 * GeV, pt_min=250 * MeV, invert_pid=False):
    """Return pions filtered by thresholds common to BnoC decay product selections."""
    pions = filter_particles(
        make_particles=make_has_rich_long_pions, p_min=p_min, pt_min=pt_min
    )
    if pi_pidk_max is not None:
        code = F.PID_K < pi_pidk_max
        if invert_pid:
            code = F.PID_K > pi_pidk_max
        pions = ParticleFilter(pions, F.FILTER(code))
    return pions


"""
for make_KS_DD
"""


@configurable
def make_down_pions(pi_pidk_max=5.0, p_min=1 * GeV, pt_min=250 * MeV):
    """Return pions filtered by thresholds common to BnoC decay product selections."""
    pions = filter_particles(
        make_particles=make_has_rich_down_pions, p_min=p_min, pt_min=pt_min
    )
    if pi_pidk_max is not None:
        code = F.PID_K < pi_pidk_max
        pions = ParticleFilter(pions, F.FILTER(code))
    return pions


@configurable
def make_tight_pions(
    pi_pidk_max=0.0, p_min=1.5 * GeV, pt_min=500 * MeV, mipchi2_min=9, invert_pid=False
):
    """Return pions filtered by thresholds common to BnoC decay product selections."""
    pions = filter_particles(
        make_particles=make_has_rich_long_pions,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min,
    )
    if pi_pidk_max is not None:
        code = F.PID_K < pi_pidk_max
        if invert_pid:
            code = F.PID_K > pi_pidk_max
        pions = ParticleFilter(pions, F.FILTER(code))
    return pions


@configurable
def make_verytight_pions(
    pi_pidk_max=0.0,
    pi_pidp_max=0.0,
    p_min=1.5 * GeV,
    pt_min=500 * MeV,
    mipchi2_min=16,
    is_muon=0,
):
    """Return pions with very tight cuts"""
    pions = filter_particles(
        make_particles=make_has_rich_long_pions,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min,
    )
    code = F.require_all(F.ISMUON == is_muon)
    if pi_pidk_max is not None:
        code = F.require_all(code, F.PID_K < pi_pidk_max)
    if pi_pidp_max is not None:
        code = F.require_all(code, F.PID_P < pi_pidp_max)
    pions = ParticleFilter(pions, F.FILTER(code))
    return pions


@configurable
def make_soft_pions(
    pi_pidk_max=0, p_min=1.5 * GeV, pt_min=200 * MeV, mipchi2_min=4, invert_pid=False
):
    """Return accompanying pions filtered by thresholds common to BnoC very soft selections."""
    pions = filter_particles(
        make_particles=make_has_rich_long_pions,
        mipchi2_min=mipchi2_min,
        p_min=p_min,
        pt_min=pt_min,
    )
    if pi_pidk_max is not None:
        code = F.PID_K < pi_pidk_max
        if invert_pid:
            code = F.PID_K > pi_pidk_max
        pions = ParticleFilter(pions, F.FILTER(code))
    return pions


@configurable
def make_PionsforB2ppbarhh(
    pi_pidk_max=0.0, p_min=1.5 * GeV, pt_min=300 * MeV, mipchi2_min=8
):
    """Return pions used for B_ppbarhh line."""
    pions = filter_particles(
        make_particles=make_has_rich_long_pions,
        mipchi2_min=mipchi2_min,
        p_min=p_min,
        pt_min=pt_min,
    )
    if pi_pidk_max is not None:
        code = F.PID_K < pi_pidk_max
        pions = ParticleFilter(pions, F.FILTER(code))
    return pions


@configurable
def make_detached_pions(
    p_min=2.0 * GeV, pt_min=250 * MeV, mipchi2_min=9.0, pi_pidk_max=2.0
):  # (F.PID_K <= 2.)):
    """
    Return detached pions.
    """
    pions = filter_particles(
        make_particles=make_long_pions_for_V0, p_min=p_min, mipchi2_min=mipchi2_min
    )
    if pi_pidk_max is not None:
        code = F.PID_K < pi_pidk_max
        pions = ParticleFilter(pions, F.FILTER(code))
    return pions


@configurable
def make_kaons(k_pidk_min=-5.0, p_min=1 * GeV, pt_min=250 * MeV, invert_pid=False):
    """Return kaons filtered by thresholds common to BnoC decay product selections."""
    kaons = filter_particles(
        make_particles=make_has_rich_long_kaons, p_min=p_min, pt_min=pt_min
    )
    if k_pidk_min is not None:
        code = F.PID_K > k_pidk_min
        if invert_pid:
            code = F.PID_K < k_pidk_min
        kaons = ParticleFilter(kaons, F.FILTER(code))
    return kaons


@configurable
def make_tight_kaons(
    k_pidk_min=2.0, p_min=1.5 * GeV, pt_min=500 * MeV, mipchi2_min=9, invert_pid=False
):
    """Return pions filtered by thresholds common to BnoC decay product selections."""
    kaons = filter_particles(
        make_particles=make_has_rich_long_kaons,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min,
    )
    if k_pidk_min is not None:
        code = F.PID_K > k_pidk_min
        if invert_pid:
            code = F.PID_K < k_pidk_min
        kaons = ParticleFilter(kaons, F.FILTER(code))
    return kaons


@configurable
def make_soft_kaons(
    k_pidk_min=0, p_min=1.5 * GeV, pt_min=200 * MeV, mipchi2_min=4, invert_pid=False
):
    """Return accompanying kaons filtered by thresholds common to BnoC very soft selections."""
    kaons = filter_particles(
        make_particles=make_has_rich_long_kaons,
        mipchi2_min=mipchi2_min,
        p_min=p_min,
        pt_min=pt_min,
    )
    if k_pidk_min is not None:
        code = F.PID_K > k_pidk_min
        if invert_pid:
            code = F.PID_K < k_pidk_min
        kaons = ParticleFilter(kaons, F.FILTER(code))
    return kaons


@configurable
def make_KaonsforB2ppbarhh(
    k_pidk_min=5.0, p_min=1.5 * GeV, pt_min=300 * MeV, mipchi2_min=6.0
):
    """Return kaons used for B_ppbarhh line."""
    kaons = filter_particles(
        make_particles=make_has_rich_long_kaons,
        mipchi2_min=mipchi2_min,
        p_min=p_min,
        pt_min=pt_min,
    )
    if k_pidk_min is not None:
        code = F.PID_K > k_pidk_min
        kaons = ParticleFilter(kaons, F.FILTER(code))
    return kaons


@configurable
def make_protons(p_pidp_min=-5, p_min=1 * GeV, pt_min=250 * MeV):
    """Return protons filtered by thresholds common to BnoC decay product selections."""
    protons = filter_particles(
        make_particles=make_has_rich_long_protons, p_min=p_min, pt_min=pt_min
    )
    if p_pidp_min is not None:
        code = F.PID_P > p_pidp_min
        protons = ParticleFilter(protons, F.FILTER(code))
    return protons


@configurable
def make_tight_protons(p_pidp_min=0, p_min=1.5 * GeV, pt_min=500 * MeV, mipchi2_min=9):
    """Return protons filtered by thresholds common to BnoC decay product selections."""
    protons = filter_particles(
        make_particles=make_has_rich_long_protons,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min,
    )
    if p_pidp_min is not None:
        code = F.PID_P > p_pidp_min
        protons = ParticleFilter(protons, F.FILTER(code))
    return protons


@configurable
def make_soft_protons(p_pidp_min=-10, p_min=1 * GeV, pt_min=100 * MeV, mipchi2_min=4):
    """Return accompanying protons filtered by thresholds common to BnoC very soft selections."""
    protons = filter_particles(
        make_particles=make_has_rich_long_protons,
        mipchi2_min=mipchi2_min,
        p_min=p_min,
        pt_min=pt_min,
    )
    if p_pidp_min is not None:
        code = F.PID_P > p_pidp_min
        protons = ParticleFilter(protons, F.FILTER(code))
    return protons


@configurable
def make_ProtonsforB2ppbarhh(
    p_pidp_min=3.0, p_pid_pkdiff=-5.0, p_min=1.5 * GeV, pt_min=300 * MeV, mipchi2_min=3
):
    """Return protons used for B_ppbarhh line."""
    protons = filter_particles(
        make_particles=make_has_rich_long_protons,
        mipchi2_min=mipchi2_min,
        p_min=p_min,
        pt_min=pt_min,
    )
    if p_pidp_min is not None:
        code = F.PID_P > p_pidp_min
        protons = ParticleFilter(protons, F.FILTER(code))
    if p_pid_pkdiff is not None:
        code &= F.PID_P - F.PID_K > p_pid_pkdiff
        protons = ParticleFilter(protons, F.FILTER(code))

    return protons


@configurable
def make_detached_protons(
    p_min=2.0 * GeV, pt_min=250 * MeV, mipchi2_min=9.0, p_pidp_min=-2.0
):  # (F.PID_P > -2.)):
    """
    Return detached pions.
    """

    protons = filter_particles(
        make_particles=make_long_protons_for_V0,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min,
    )
    if p_pidp_min is not None:
        code = F.PID_P > p_pidp_min
        protons = ParticleFilter(protons, F.FILTER(code))
    return protons


############################################
######## Filters for b-baryon lines ########
############################################


@configurable
def make_bbaryon_filter_tracks(
    make_particles=make_has_rich_long_pions,
    name="BNOC_has_rich_long_pions_{hash}",
    pt_min=250.0 * MeV,
    p_min=2.0 * GeV,
    mipchi2dvprimary_min=None,
    pid=None,
    notmuon=True,
):
    """
    Build generic long tracks.
    """
    code = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
    )

    if pid is not None:
        code &= pid

    if mipchi2dvprimary_min is not None:
        pvs = make_pvs()
        code &= F.MINIPCHI2(pvs) > mipchi2dvprimary_min

    if notmuon is True:
        code &= ~F.ISMUON

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


### Detached protons / kaons / pions ###


@configurable
def make_bbaryon_detached_pions(
    name="BNOC_bbaryon_detached_pions_{hash}",
    p_min=1.5 * GeV,
    pt_min=300.0 * MeV,
    mipchi2dvprimary_min=11.0,  # TBC
    pid=(F.PID_K < 1.0),
):
    """
    Return bnoc b-baryon detached pions.
    """
    return make_bbaryon_filter_tracks(
        name=name,
        make_particles=make_has_rich_long_pions,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid,
    )


@configurable
def make_bbaryon_detached_kaons(
    name="BNOC_bbaryon_detached_kaons_{hash}",
    p_min=1.5 * GeV,
    pt_min=300.0 * MeV,
    mipchi2dvprimary_min=11.0,  # TBC
    pid=(F.PID_K > 3.0),
):
    """
    Return bnoc b-baryon detached kaons.
    """
    return make_bbaryon_filter_tracks(
        name=name,
        make_particles=make_has_rich_long_kaons,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid,
    )


@configurable
def make_bbaryon_detached_protons(
    name="BNOC_bbaryon_detached_protons_{hash}",
    p_min=1.5 * GeV,
    pt_min=500.0 * MeV,
    mipchi2dvprimary_min=11.0,  # TBC
    pid=((F.PID_P > 3.0) & (F.PID_P - F.PID_K > -5)),
):
    """
    Return bnoc b-baryon detached protons.
    """
    return make_bbaryon_filter_tracks(
        name=name,
        make_particles=make_has_rich_long_protons,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid,
    )


def make_bbaryon_detached_deuterons(
    name="BNOC_bbaryon_detached_deuterons_{hash}",
    p_min=1.5 * GeV,
    pt_min=500.0 * MeV,
    mipchi2dvprimary_min=11.0,  # TBC
    pid=(F.PID_P > 3.0),
):
    """
    Return bnoc b-baryon detached deuterons.
    """
    return make_bbaryon_filter_tracks(
        name=name,
        make_particles=make_has_rich_long_deuterons,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid,
    )


@configurable
def make_bbaryon_detached_down_kaons(
    name="BNOC_bbaryon_detached_kaons_{hash}",
    mipchi2dvprimary_min=4.0,  # TBC
    pt_min=0.0 * MeV,
    p_min=0.0 * GeV,
    pid=None,
):
    """
    Return bnoc b-baryon downstream hadrons with kaon mass hypothesis.
    """
    return make_bbaryon_filter_tracks(
        make_particles=make_down_kaons,
        name=name,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pt_min=pt_min,
        p_min=pt_min,
        pid=pid,
    )


@configurable
def make_bbaryon_soft_pions(
    name="BNOC_bbaryon_soft_pions_{hash}",
    p_min=2.0 * GeV,
    pt_min=200.0 * MeV,
    mipchi2dvprimary_min=6.0,
    pid=(F.PID_K < 0.0),
):
    """
    Return bnoc b-baryon soft pions.
    """
    return make_bbaryon_filter_tracks(
        name=name,
        make_particles=make_has_rich_long_pions,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid,
    )


@configurable
def make_bbaryon_soft_kaons(
    name="BNOC_bbaryon_soft_kaons_{hash}",
    p_min=2.0 * GeV,
    pt_min=200.0 * MeV,
    mipchi2dvprimary_min=6.0,
    pid=(F.PID_K > 0.0),
):
    """
    Return bnoc b-baryon soft kaons.
    """
    return make_bbaryon_filter_tracks(
        name=name,
        make_particles=make_has_rich_long_kaons,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid,
    )


@configurable
def make_bbaryon_soft_protons(
    name="BNOC_bbaryon_soft_protons_{hash}",
    p_min=8.5 * GeV,
    pt_min=200.0 * MeV,
    mipchi2dvprimary_min=6.0,
    pid=((F.PID_P > 3.0) & (F.PID_P - F.PID_K > -5)),
):
    """
    Return bnoc b-baryon soft protons.
    """
    return make_bbaryon_filter_tracks(
        name=name,
        make_particles=make_has_rich_long_protons,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid,
    )


###########################
# Downstream tracks       #
###########################


@configurable
def make_detached_down_protons(
    p_min=2.0 * GeV, pt_min=250 * MeV, mipchi2_min=4.0, p_pidp_min=None
):
    """
    Return downstream hadrons with proton mass hypothesis.
    """
    protons = filter_particles(
        make_particles=make_down_protons_for_V0,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min,
    )

    if p_pidp_min is not None:
        code = F.PID_P > p_pidp_min
        protons = ParticleFilter(protons, F.FILTER(code))
    return protons


@configurable
def make_detached_down_pions(
    p_min=2.0 * GeV, pt_min=250 * MeV, mipchi2_min=4.0, pi_pidk_max=None
):
    """
    Return downstream hadrons with pion mass hypothesis.
    """
    pions = filter_particles(
        make_particles=make_down_pions_for_V0,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min,
    )
    if pi_pidk_max is not None:
        code = F.PID_K < pi_pidk_max
        pions = ParticleFilter(pions, F.FILTER(code))
    return pions


####################################
# Neutral objects selections       #
####################################


@configurable
def make_photons(make_particles=make_photons, is_photon=0.5, et_min=150 * MeV):
    """For the time being just a dummy selection"""

    code = F.require_all(F.IS_PHOTON > is_photon, F.PT > et_min)
    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_tight_photons(
    make_particles=make_photons, is_photon=0.5, et_min=150 * MeV, is_not_h=0.06
):
    code = F.require_all(F.IS_PHOTON > is_photon, F.PT > et_min, F.IS_NOT_H > is_not_h)
    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_resolved_pi0s(
    gamma=make_photons,
    gamma_is_photon=0.5,
    gamma_et_min=150 * MeV,
    pion_mass_min=109.9768 * MeV,
    pion_mass_max=149.9768 * MeV,
    pion_p_min=2000 * MeV,
    pion_pt_min=1500 * MeV,
):
    gamma = gamma(is_photon=gamma_is_photon, et_min=gamma_et_min)
    combination_code = in_range(0.9 * pion_mass_min, F.MASS, 1.1 * pion_mass_max)
    composite_code = F.require_all(
        F.P > pion_p_min,
        F.PT > pion_pt_min,
        in_range(pion_mass_min, F.MASS, pion_mass_max),
    )
    return ParticleCombiner(
        ParticleCombiner="ParticleAdder",
        Inputs=[gamma, gamma],
        name="BNOC_resolved_pi0_{hash}",
        DecayDescriptor="pi0 -> gamma gamma",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


@configurable
def make_merged_pi0s(p_min=2000 * MeV, pt_min=250 * MeV):
    """For the time being just a dummy selection"""
    make_particles = standard_merged_pi0
    code = F.require_all(F.PT > pt_min, F.P > p_min)
    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_tight_merged_pi0s(
    p_min=2000 * MeV, pt_min=250 * MeV, am_min=100 * MeV, am_max=170 * MeV, is_not_H=0.1
):
    """For the time being just a dummy selection"""
    make_particles = standard_merged_pi0
    code = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.MASS > am_min,
        F.MASS < am_max,
        F.IS_NOT_H > is_not_H,
    )
    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_dalitz_pi0s(
    name="BNOC_Dalitz_Pi0_filter_{hash}",
    dielectron=make_detached_dielectron_with_brem,
    photon=make_tight_photons,
    p_min=1000 * MeV,
    pt_min=250 * MeV,
    am_min=100 * MeV,
    am_max=170 * MeV,
    gamma_is_not_H=0.1,
    gamma_is_photon=0.2,
    min_ipchi2=16,
):
    """For the time being just a dummy selection"""
    gamma = photon(is_photon=gamma_is_photon, is_not_h=gamma_is_not_H)
    diele = dielectron(
        electron_maker=make_long_and_upstream_electrons_no_brem,
        opposite_sign=True,
        fake_electrons=0,
        m_diE_min=0 * MeV,
        m_diE_max=300 * MeV,
        minipchi2=min_ipchi2,
    )
    combination_code = in_range(0.9 * am_min, F.MASS, 1.1 * am_max)
    composite_code = F.require_all(
        F.P > p_min,
        F.PT > pt_min,
        in_range(am_min, F.MASS, am_max),
        # F.CHILD(1,
        #        F.CHILD(1, F.BREMHYPOID) != F.CHILD(2, F.CALO_NEUTRAL_ID)),
        # F.CHILD(1,
        #        F.CHILD(2, F.BREMHYPOID) != F.CHILD(2, F.CALO_NEUTRAL_ID)),
    )
    return ParticleCombiner(
        [diele, gamma],
        name=name,
        DecayDescriptor="pi0 -> J/psi(1S) gamma",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


####################################
# ks, kstar0, ... 2-body decays    #
####################################
""" KS LL and DD already defined the same in B2OC module
so just import them from there unless we want to change something.
Moore will not compile if you have a repeat of the same algorithm
"""


# for ButoKSh
@configurable
def make_KS_LL(
    name="BNOC_KS_LL_filter_{hash}",
    make_pions=make_pions,
    make_pvs=make_pvs,
    pi_p_min=2 * GeV,
    p_min=8000.0 * MeV,
    pt_min=1000.0 * MeV,
    am_min=482.0 * MeV,
    am_max=512.0 * MeV,
    vchi2_max=10.0,
    bpvfdchi2_min=100.0,
    mipchi2_min=0,
):
    """
    Builds LL Kshorts, currently corresponding to the Run2
    StdVeryLooseKSLL.
    """
    pions = make_pions(p_min=pi_p_min)
    descriptor = "KS0 -> pi+ pi-"

    combination_code = in_range(am_min - 35, F.MASS, am_max + 35)

    pvs = make_pvs()
    vertex_code = F.require_all(
        F.P > p_min,
        F.PT > pt_min,
        F.CHI2 < vchi2_max,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
        F.MINIPCHI2(pvs) > mipchi2_min,
    )

    return ParticleCombiner(
        [pions, pions],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_KS_DD(
    name="BNOC_KS_DD_filter_{hash}",
    make_pions=make_down_pions,
    make_pvs=make_pvs,
    pi_p_min=2 * GeV,
    p_min=8000.0 * MeV,
    am_max=527.0 * MeV,
    am_min=467.0 * MeV,
    vchi2_max=10.0,
    bpvfdchi2_min=50.0,
    pt_min=1000.0 * MeV,
):
    """
    Builds DD Kshorts, currently corresponding to the Run2
    StdLooseKSDD.
    """
    pions = make_down_pions(p_min=pi_p_min)
    descriptor = "KS0 -> pi+ pi-"

    combination_code = in_range(am_min - 50, F.MASS, am_max + 50)

    vertex_code = F.require_all(
        F.P > p_min, F.PT > pt_min, F.CHI2 < vchi2_max, F.OWNPVFDCHI2 > bpvfdchi2_min
    )
    return ParticleCombiner(
        [pions, pions],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_bbaryon_ks0_ll(
    name="BNOC_bbaryon_ks0_ll_{hash}",
    make_pions=make_soft_pions,
    make_pvs=make_pvs,
    mass_min=467.611 * MeV,
    mass_max=527.611 * MeV,
    pi_p_min=1.0 * GeV,
    pi_pt_min=0.0 * GeV,  # has to be 0 !
    pi_ipchi2_min=12.0,
    pt_min=300 * MeV,
    doca_max=150.0 * um,
    bpvfdchi2_min=50.0,
    bpvvdz_min=4.0 * mm,
    bpvltime_min=1.0 * ps,
    vchi2pdof_max=15.0,
):
    """
    Modified make_KS_LL
    """

    pions = make_pions(
        p_min=pi_p_min, pt_min=pi_pt_min, mipchi2_min=pi_ipchi2_min, pi_pidk_max=None
    )  # remove any PID cuts
    descriptor = "KS0 -> pi+ pi-"
    combination_code = F.require_all(
        in_range(mass_min * 0.95, F.MASS, mass_max * 1.05), F.MAXDOCACUT(doca_max)
    )
    vertex_code = F.require_all(
        in_range(mass_min, F.MASS, mass_max),
        F.PT > pt_min,
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVLTIME > bpvltime_min,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
        F.OWNPVVDZ > bpvvdz_min,
    )
    return ParticleCombiner(
        [pions, pions],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_bbaryon_ks0_dd(
    name="BNOC_bbaryon_ks0_dd_{hash}",
    make_pions=make_down_pions,
    make_pvs=make_pvs,
    mass_min=457.611 * MeV,
    mass_max=537.611 * MeV,
    pi_p_min=2.0 * GeV,
    pi_pt_min=0.0,  # has to be 0 !
    pt_min=300 * MeV,
    doca_max=2.0 * mm,
    adocachi2cut=15.0,
    bpvfdchi2_min=4.0,
    bpvltime_min=1.0 * ps,
    vchi2pdof_max=15.0,
):
    """
    Modified make_KS_DD
    """

    pions = make_pions(
        p_min=pi_p_min, pt_min=pi_pt_min, pi_pidk_max=None
    )  # remove any PID cuts
    descriptor = "KS0 -> pi+ pi-"
    combination_code = F.require_all(
        in_range(mass_min * 0.95, F.MASS, mass_max * 1.05),
        F.MAXDOCACHI2CUT(adocachi2cut),
    )
    vertex_code = F.require_all(
        in_range(mass_min, F.MASS, mass_max),
        F.CHI2DOF < vchi2pdof_max,
        F.PT > pt_min,
        F.OWNPVLTIME > bpvltime_min,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
    )
    return ParticleCombiner(
        [pions, pions],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_kstar0(
    name="BNOC_Kstar_Combiner_{hash}",
    make_pions=make_pions,
    make_kaons=make_kaons,
    make_pvs=make_pvs,
    am_min=742 * MeV,
    am_max=1042 * MeV,
    pi_p_min=2 * GeV,
    pi_pt_min=100 * MeV,
    k_p_min=2 * GeV,
    k_pt_min=100 * MeV,
    adoca12_max=0.5 * mm,
    asumpt_min=1000 * MeV,
    bpvfdchi2_min=16,
    vchi2pdof_max=16,
):
    """
    Build Kstar0 candidates.
    """

    pions = make_pions(p_min=pi_p_min, pt_min=pi_pt_min)
    kaons = make_kaons(p_min=k_p_min, pt_min=k_pt_min)
    descriptor = "[K*(892)0 -> K+ pi-]cc"
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > asumpt_min,
        F.MAXSDOCACUT(adoca12_max),
    )
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max, F.OWNPVFDCHI2 > bpvfdchi2_min
    )
    return ParticleCombiner(
        [kaons, pions],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_wide_kstar0(
    name="BNOC_WideKstar_Combiner_{hash}",
    make_pions=make_pions,
    make_kaons=make_kaons,
    make_pvs=make_pvs,
    am_min=630 * MeV,
    am_max=1600 * MeV,
    pi_mipchi2_min=None,
    pi_pidk=5,
    pi_p_min=2 * GeV,
    pi_pt_min=500 * MeV,
    k_mipchi2_min=None,
    k_pidk_min=-5.0,
    k_p_min=2 * GeV,
    k_pt_min=500 * MeV,
    adoca12_max=0.5 * mm,
    asumpt_min=1000 * MeV,
    bpvfdchi2_min=16,
    vchi2pdof_max=16,
    motherpt_min=None,
    mipchi2_min=None,
    invert_pi_pid=False,
    invert_k_pid=False,
):
    """
    Build wide Kstar0 candidates.
    """
    pi_kwargs = {"pi_pidk_max": pi_pidk, "p_min": pi_p_min, "pt_min": pi_pt_min}
    # Not all pion/kaon builders have these args so only add when needed
    if invert_pi_pid:
        pi_kwargs["invert_pid"] = invert_pi_pid
    if pi_mipchi2_min is not None:
        pi_kwargs["mipchi2_min"] = pi_mipchi2_min
    pions = make_pions(**pi_kwargs)
    k_kwargs = {"k_pidk_min": k_pidk_min, "p_min": k_p_min, "pt_min": k_pt_min}
    if invert_k_pid:
        k_kwargs["invert_pid"] = invert_k_pid
    if k_mipchi2_min is not None:
        k_kwargs["mipchi2_min"] = k_mipchi2_min
    kaons = make_kaons(**k_kwargs)

    descriptor = "[K*(892)0 -> K+ pi-]cc"
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > asumpt_min,
        F.MAXSDOCACUT(adoca12_max),
    )
    pvs = make_pvs()
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max, F.OWNPVFDCHI2 > bpvfdchi2_min
    )
    if motherpt_min is not None:
        vertex_code &= F.PT > motherpt_min
    if mipchi2_min is not None:
        vertex_code &= F.MINIPCHI2(pvs) > mipchi2_min
    return ParticleCombiner(
        [kaons, pions],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_kstarplus_LL(
    name="BNOC_KstarPlus_LL_Combiner_{hash}",
    make_pions=make_pions,
    make_ks=make_KS_LL,
    make_pvs=make_pvs,
    am_min=742 * MeV,
    am_max=1042 * MeV,
    pi_p_min=2 * GeV,
    pi_pt_min=500 * MeV,
    pi_mipchi2_min=16,
    adoca12_max=0.1 * mm,
    asumpt_min=1000 * MeV,
    bpvfdchi2_min=50,
    vchi2pdof_max=16,
):
    """
    Build Kstarplus candidates.
    """
    ks = make_ks(
        pt_min=pi_pt_min, bpvfdchi2_min=bpvfdchi2_min, mipchi2_min=pi_mipchi2_min
    )
    pions = make_pions(p_min=pi_p_min, pt_min=pi_pt_min)
    descriptor = "[K*(892)+ -> KS0 pi+]cc"
    combination_code = F.require_all(
        in_range(am_min - 10 * MeV, F.MASS, am_max + 10 * MeV),
        F.SUM(F.PT) > asumpt_min,
        F.MAXSDOCACUT(adoca12_max),
    )
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
        in_range(am_min, F.MASS, am_max),
    )
    return ParticleCombiner(
        [ks, pions],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_kstarplus_DD(
    name="BNOC_KstarPlus_DD_Combiner_{hash}",
    make_pions=make_pions,
    make_ks=make_KS_DD,
    make_pvs=make_pvs,
    am_min=742 * MeV,
    am_max=1042 * MeV,
    pi_p_min=2 * GeV,
    pi_pt_min=500 * MeV,
    pi_mipchi2_min=16,
    adoca12_max=0.1 * mm,
    asumpt_min=1000 * MeV,
    bpvfdchi2_min=50,
    vchi2pdof_max=16,
):
    """
    Build Kstarplus candidates.
    """
    ks = make_ks(pt_min=pi_pt_min, bpvfdchi2_min=bpvfdchi2_min)
    pions = make_pions(p_min=pi_p_min, pt_min=pi_pt_min)
    descriptor = "[K*(892)+ -> KS0 pi+]cc"
    combination_code = F.require_all(
        in_range(am_min - 10 * MeV, F.MASS, am_max + 10 * MeV),
        F.SUM(F.PT) > asumpt_min,
        F.MAXSDOCACUT(adoca12_max),
    )
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
        in_range(am_min, F.MASS, am_max),
    )
    return ParticleCombiner(
        [ks, pions],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_phi(
    name="BNOC_Phi_Combiner_{hash}",
    make_kaons=make_kaons,
    make_pvs=make_pvs,
    am_min=990 * MeV,
    am_max=1050 * MeV,
    k_pidk_min=-5.0,
    k_p_min=1 * GeV,
    k_pt_min=400 * MeV,
    adoca12_max=None,
    asumpt_min=0 * MeV,
    bpvfdchi2_min=0,
    vchi2pdof_max=15,
    motherpt_min=None,
    mipchi2_min=None,
    invert_pid=False,
):
    """
    Build phi candidates.
    """

    kaons = make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min, invert_pid=invert_pid
    )
    descriptor = "phi(1020) -> K+ K-"
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.SUM(F.PT) > asumpt_min
    )
    if adoca12_max is not None:
        combination_code &= F.MAXSDOCACUT(adoca12_max)
    pvs = make_pvs()
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max, F.OWNPVFDCHI2 > bpvfdchi2_min
    )
    if motherpt_min is not None:
        vertex_code &= F.PT > motherpt_min
    if mipchi2_min is not None:
        vertex_code &= F.MINIPCHI2(pvs) > mipchi2_min
    return ParticleCombiner(
        [kaons, kaons],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_phi_fake(
    name="BNOC_Phi_Combiner_fake_{hash}",
    make_kaons=make_kaons,
    make_pvs=make_pvs,
    am_min=990 * MeV,
    am_max=1050 * MeV,
    k_pidk_min=-5.0,
    k_p_min=1 * GeV,
    k_pt_min=400 * MeV,
    adoca12_max=None,
    asumpt_min=0 * MeV,
    bpvfdchi2_min=0,
    vchi2pdof_max=15,
    motherpt_min=None,
    mipchi2_min=None,
    invert_two_pid=False,
):
    """
    Build phi candidates with at least one fake kaon.
    """

    kaons1 = make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min, invert_pid=True
    )
    kaons2 = make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min, invert_pid=invert_two_pid
    )

    descriptor = "[phi(1020) -> K+ K-]cc"
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.SUM(F.PT) > asumpt_min
    )
    if adoca12_max is not None:
        combination_code &= F.MAXSDOCACUT(adoca12_max)
    pvs = make_pvs()
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max, F.OWNPVFDCHI2 > bpvfdchi2_min
    )
    if motherpt_min is not None:
        vertex_code &= F.PT > motherpt_min
    if mipchi2_min is not None:
        vertex_code &= F.MINIPCHI2(pvs) > mipchi2_min
    return ParticleCombiner(
        [kaons1, kaons2],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_rho0(
    name="BNOC_Rho_Combiner_{hash}",
    make_pions=make_pions,
    make_pvs=make_pvs,
    am_min=400 * MeV,
    am_max=1500 * MeV,
    pi_mipchi2_min=None,
    pi_pidk_max=None,
    pi_p_min=2 * GeV,
    pi_pt_min=500 * MeV,
    adoca12_max=0.5 * mm,
    asump_min=1 * GeV,
    asumpt_min=900 * MeV,
    bpvfdchi2_min=25,
    vchi2pdof_max=9,
    motherpt_min=None,
    mipchi2_min=None,
    invert_pid=False,
):
    """
    Build Rho0 candidates.
    """
    pi_kwargs = {"pi_pidk_max": pi_pidk_max, "p_min": pi_p_min, "pt_min": pi_pt_min}
    # Not all pion builders have these args so only add when needed
    if invert_pid:
        pi_kwargs["invert_pid"] = invert_pid
    if pi_mipchi2_min is not None:
        pi_kwargs["mipchi2_min"] = pi_mipchi2_min
    pions = make_pions(**pi_kwargs)

    descriptor = "rho(770)0 -> pi+ pi-"
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > asumpt_min,
        F.SUM(F.P) > asump_min,
        F.MAXSDOCACUT(adoca12_max),
    )
    pvs = make_pvs()
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max, F.OWNPVFDCHI2 > bpvfdchi2_min
    )
    if motherpt_min is not None:
        vertex_code &= F.PT > motherpt_min
    if mipchi2_min is not None:
        vertex_code &= F.MINIPCHI2(pvs) > mipchi2_min
    return ParticleCombiner(
        [pions, pions],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_rho0_fake(
    name="BNOC_Rho_Combiner_fake_{hash}",
    make_pions=make_pions,
    make_pvs=make_pvs,
    am_min=400 * MeV,
    am_max=1500 * MeV,
    pi_pidk_max=None,
    pi_p_min=2 * GeV,
    pi_pt_min=500 * MeV,
    adoca12_max=0.5 * mm,
    asump_min=1 * GeV,
    asumpt_min=900 * MeV,
    bpvfdchi2_min=25,
    vchi2pdof_max=9,
    motherpt_min=None,
    mipchi2_min=None,
    invert_two_pid=False,
):
    """
    Build Rho0 candidates with at least one fake hadron
    """

    pions1 = make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min, invert_pid=True
    )
    pions2 = make_pions(
        pi_pidk_max=pi_pidk_max,
        p_min=pi_p_min,
        pt_min=pi_pt_min,
        invert_pid=invert_two_pid,
    )
    descriptor = "[rho(770)0 -> pi+ pi-]cc"
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > asumpt_min,
        F.SUM(F.P) > asump_min,
        F.MAXSDOCACUT(adoca12_max),
    )
    pvs = make_pvs()
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max, F.OWNPVFDCHI2 > bpvfdchi2_min
    )
    if motherpt_min is not None:
        vertex_code &= F.PT > motherpt_min
    if mipchi2_min is not None:
        vertex_code &= F.MINIPCHI2(pvs) > mipchi2_min
    return ParticleCombiner(
        [pions1, pions2],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_omega0(
    name="BNOC_Omega0_Combiner_{hash}",
    make_pions=make_pions,
    make_pi0s=make_resolved_pi0s,
    make_pvs=make_pvs,
    am_min=700 * MeV,
    am_max=860 * MeV,
    pi_pidk_max=None,
    pi_p_min=2 * GeV,
    pi_pt_min=500 * MeV,
    adoca12_max=0.5 * mm,
    asump_min=1 * GeV,
    asumpt_min=900 * MeV,
    bpvfdchi2_min=25,
    vchi2pdof_max=9,
    motherpt_min=None,
    mipchi2_min=None,
    invert_pid=False,
):
    """
    Build Omega0 candidates.
    """

    pions = make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min, invert_pid=invert_pid
    )
    pi0s = make_pi0s()
    descriptor = "omega(782) -> pi+ pi- pi0"
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > asumpt_min,
        F.SUM(F.P) > asump_min,
    )
    combination12_code = F.MAXSDOCACUT(adoca12_max)
    pvs = make_pvs()
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max, F.OWNPVFDCHI2 > bpvfdchi2_min
    )
    if motherpt_min is not None:
        vertex_code &= F.PT > motherpt_min
    if mipchi2_min is not None:
        vertex_code &= F.MINIPCHI2(pvs) > mipchi2_min
    return ParticleCombiner(
        [pions, pions, pi0s],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        Combination12Cut=combination12_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_omega0_fake(
    name="BNOC_Omega0_Combiner_fake_{hash}",
    make_pions=make_pions,
    make_pi0s=make_resolved_pi0s,
    make_pvs=make_pvs,
    am_min=700 * MeV,
    am_max=860 * MeV,
    pi_pidk_max=None,
    pi_p_min=2 * GeV,
    pi_pt_min=500 * MeV,
    adoca12_max=0.5 * mm,
    asump_min=1 * GeV,
    asumpt_min=900 * MeV,
    bpvfdchi2_min=25,
    vchi2pdof_max=9,
    motherpt_min=None,
    mipchi2_min=None,
    invert_two_pid=False,
):
    """
    Build Omega0 candidates with at least one fake hadron
    """

    pions1 = make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min, invert_pid=True
    )
    pions2 = make_pions(
        pi_pidk_max=pi_pidk_max,
        p_min=pi_p_min,
        pt_min=pi_pt_min,
        invert_pid=invert_two_pid,
    )
    pi0s = make_pi0s()
    descriptor = "[omega(782) -> pi+ pi- pi0]cc"
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > asumpt_min,
        F.SUM(F.P) > asump_min,
    )
    combination12_code = F.MAXSDOCACUT(adoca12_max)
    pvs = make_pvs()
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max, F.OWNPVFDCHI2 > bpvfdchi2_min
    )
    if motherpt_min is not None:
        vertex_code &= F.PT > motherpt_min
    if mipchi2_min is not None:
        vertex_code &= F.MINIPCHI2(pvs) > mipchi2_min
    return ParticleCombiner(
        [pions1, pions2, pi0s],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        Combination12Cut=combination12_code,
        CompositeCut=vertex_code,
    )


# eta and eta' builders
# Build eta from pions
def make_dipions_for_eta(
    name="BNOC_DiPions_filter_{hash}",
    make_pvs=make_pvs,
    am_min=280 * MeV,
    am_max=960 * MeV,
    decaytime=0.2 * ps,
    mipchi2_min=16.0,
    pt_min=350 * MeV,
    pi_pidk_max=5,
    adoca12_max=0.1 * mm,
    vchi2_max=12.0,
    bpvfdchi2_min=20.0,
):
    pions = make_detached_pions(
        pt_min=pt_min, mipchi2_min=mipchi2_min, pi_pidk_max=pi_pidk_max
    )
    descriptor = "rho(770)0 -> pi+ pi-"

    combination_code = F.require_all(
        in_range(am_min - 10 * MeV, F.MASS, am_max + 10 * MeV),
        F.MAXSDOCACUT(adoca12_max),
    )

    vertex_code = F.require_all(
        F.CHI2 < vchi2_max,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
        in_range(am_min, F.MASS, am_max),
        F.OWNPVLTIME > decaytime,
    )

    return ParticleCombiner(
        [pions, pions],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_eta_threepions_resolved(
    name="BNOC_Eta_PiPiPi_Resolved_Combiner_{hash}",
    make_dipions=make_dipions_for_eta,
    make_pi0s=make_resolved_pi0s,
    make_pvs=make_pvs,
    am_min=450 * MeV,
    am_max=650 * MeV,
    pt_min=500 * MeV,
    pt_min_pi0=350 * MeV,
    dipions_mass_min=280 * MeV,
    dipions_mass_max=600 * MeV,
    bpvfdchi2_min=30,
):
    pi0s = make_pi0s(pion_pt_min=pt_min_pi0)
    pion_pairs = make_dipions(
        make_pvs=make_pvs, am_min=dipions_mass_min, am_max=dipions_mass_max
    )

    combination_code = F.require_all(
        in_range(am_min - 10 * MeV, F.MASS, am_max + 10 * MeV), F.PT > pt_min
    )

    vertex_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.OWNPVFDCHI2 > bpvfdchi2_min
    )

    return ParticleCombiner(
        [pion_pairs, pi0s],
        name=name,
        DecayDescriptor="eta -> rho(770)0 pi0",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_eta_threepions_merged(
    name="BNOC_Eta_PiPiPi_Merged_Combiner_{hash}",
    make_dipions=make_dipions_for_eta,
    make_pi0s=make_merged_pi0s,
    make_pvs=make_pvs,
    am_min=450 * MeV,
    am_max=650 * MeV,
    pt_min=500 * MeV,
    pt_min_pi0=350 * MeV,
    dipions_mass_min=280 * MeV,
    dipions_mass_max=600 * MeV,
    bpvfdchi2_min=30,
):
    pi0s = make_pi0s(pt_min=pt_min_pi0)
    pion_pairs = make_dipions(
        make_pvs=make_pvs, am_min=dipions_mass_min, am_max=dipions_mass_max
    )

    combination_code = F.require_all(
        in_range(am_min - 10 * MeV, F.MASS, am_max + 10 * MeV), F.PT > pt_min
    )

    vertex_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.OWNPVFDCHI2 > bpvfdchi2_min
    )

    return ParticleCombiner(
        [pion_pairs, pi0s],
        name=name,
        DecayDescriptor="eta -> rho(770)0 pi0",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_eta_pipigamma(
    name="BNOC_Eta_PiPiGamma_Combiner_{hash}",
    make_dipions=make_dipions_for_eta,
    make_photons=make_photons,
    make_pvs=make_pvs,
    am_min=450 * MeV,
    am_max=650 * MeV,
    pt_min=500 * MeV,
    pt_min_gamma=350 * MeV,
    dipions_mass_min=280 * MeV,
    dipions_mass_max=600 * MeV,
    bpvfdchi2_min=30,
):
    gamma = make_photons(et_min=pt_min_gamma)
    pion_pairs = make_dipions(
        make_pvs=make_pvs, am_min=dipions_mass_min, am_max=dipions_mass_max
    )

    combination_code = F.require_all(
        in_range(am_min - 10 * MeV, F.MASS, am_max + 10 * MeV), F.PT > pt_min_gamma
    )

    vertex_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.OWNPVFDCHI2 > bpvfdchi2_min
    )

    return ParticleCombiner(
        [pion_pairs, gamma],
        name=name,
        DecayDescriptor="eta -> rho(770)0 gamma",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


# Build eta' from eta and pi+ pi-
@configurable
def make_etaprime_resolved(
    name="BNOC_Etaprime_Resolved_{hash}",
    make_dipions=make_dipions_for_eta,
    make_eta=make_eta_threepions_resolved,
    make_pvs=make_pvs,
    am_min=760 * MeV,
    am_max=1160 * MeV,
    pt_min=500 * MeV,
    pt_min_pi0=350 * MeV,
    dipions_mass_min=280 * MeV,
    dipions_mass_max=1050 * MeV,
    decaytime_dipions=0.2 * ps,
    bpvfdchi2_min=30,
):
    etas = make_eta()
    pion_pairs = make_dipions(
        make_pvs=make_pvs,
        am_min=dipions_mass_min,
        am_max=dipions_mass_max,
        decaytime=decaytime_dipions,
    )

    combination_code = F.require_all(
        in_range(am_min - 10 * MeV, F.MASS, am_max + 10 * MeV), F.PT > pt_min
    )

    vertex_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.OWNPVFDCHI2 > bpvfdchi2_min
    )
    return ParticleCombiner(
        [pion_pairs, etas],
        DecayDescriptor="eta_prime -> rho(770)0 eta",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_etaprime_merged(
    name="BNOC_Etaprime_Merged_{hash}",
    make_dipions=make_dipions_for_eta,
    make_eta=make_eta_threepions_merged,
    make_pvs=make_pvs,
    am_min=760 * MeV,
    am_max=1160 * MeV,
    pt_min=500 * MeV,
    pt_min_pi0=350 * MeV,
    dipions_mass_min=280 * MeV,
    dipions_mass_max=1050 * MeV,
    decaytime_dipions=0.2 * ps,
    bpvfdchi2_min=30,
):
    etas = make_eta()
    pion_pairs = make_dipions(
        make_pvs=make_pvs,
        am_min=dipions_mass_min,
        am_max=dipions_mass_max,
        decaytime=decaytime_dipions,
    )

    combination_code = F.require_all(
        in_range(am_min - 10 * MeV, F.MASS, am_max + 10 * MeV), F.PT > pt_min
    )

    vertex_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.OWNPVFDCHI2 > bpvfdchi2_min
    )
    return ParticleCombiner(
        [pion_pairs, etas],
        DecayDescriptor="eta_prime -> rho(770)0 eta",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_etaprime_gamma(
    name="BNOC_Etaprime_Gamma_{hash}",
    make_photons=make_photons,
    make_dipions=make_dipions_for_eta,
    make_pvs=make_pvs,
    am_min=760 * MeV,
    am_max=1160 * MeV,
    pt_min=500 * MeV,
    gamma_et_min=350 * MeV,
    dipions_mass_min=550 * MeV,
    dipions_mass_max=1050 * MeV,
    decaytime_dipions=0.2 * ps,
    bpvfdchi2_min=30,
):
    gamma = make_photons(et_min=gamma_et_min)
    pion_pairs = make_dipions(
        make_pvs=make_pvs,
        am_min=dipions_mass_min,
        am_max=dipions_mass_max,
        decaytime=decaytime_dipions,
    )

    combination_code = F.require_all(
        in_range(am_min - 10 * MeV, F.MASS, am_max + 10 * MeV), F.PT > pt_min
    )

    vertex_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.OWNPVFDCHI2 > bpvfdchi2_min
    )
    return ParticleCombiner(
        [pion_pairs, gamma],
        DecayDescriptor="eta_prime -> rho(770)0 gamma",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


####################################
# lambda0  2-body decays           #
####################################
def make_lambda_LL(
    make_lambda=make_LambdaLL,
    m_max=1135 * MeV,
    m_min=1095 * MeV,
    pt_min=550 * MeV,
    p_min=1.0 * GeV,
    vchi2dof_max=9.0,
    endvz_min=-100.0 * mm,
    endvz_max=500.0 * mm,
):
    code = require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.P > p_min,
        F.CHI2DOF < vchi2dof_max,
        in_range(endvz_min, F.END_VZ, endvz_max),
    )
    return ParticleFilter(make_lambda(), F.FILTER(code), name="BNOC_Lambda_LL_Combiner")


def make_lambda_DD(
    make_lambda=make_LambdaDD,
    m_max=1140 * MeV,
    m_min=1095 * MeV,
    pt_min=500.0 * MeV,
    p_min=1.0 * GeV,
    vchi2dof_max=9.0,
    endvz_min=300.0 * mm,
    endvz_max=2275.0 * mm,
):
    code = require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.P > p_min,
        F.CHI2DOF < vchi2dof_max,
        in_range(endvz_min, F.END_VZ, endvz_max),
    )
    return ParticleFilter(make_lambda(), F.FILTER(code), name="BNOC_Lambda_DD_Combiner")


@configurable
def make_veryloose_lambda_LL(
    name="BNOC_veryloose_lambda_LL",
    make_protons=make_detached_protons,
    make_pions=make_detached_pions,
    make_pvs=make_pvs,
    mass_window_comb_min=1065.0 * MeV,
    mass_window_comb_max=1165.0 * MeV,
    mass_window_min=1095.0 * MeV,
    mass_window_max=1135.0 * MeV,
    pi_p_min=2.0 * GeV,
    p_p_min=2.0 * GeV,
    p_pt_min=0.0,  # recommended to be small
    pi_pt_min=0.0,  # has to be 0 !!!
    pi_ipchi2_min=9.0,
    p_ipchi2_min=9.0,
    adocachi2cut=30.0,
    bpvfdchi2_min=50.0,
    bpvvdz_min=8 * mm,
    bpvltime_min=1.0 * ps,
    vchi2pdof_max=15.0,
    endvz_min=0 * mm,
):
    protons = make_protons(
        p_min=p_p_min, pt_min=p_pt_min, mipchi2_min=p_ipchi2_min, p_pidp_min=None
    )  # important to get rid of any PID cuts!
    pions = make_pions(
        p_min=pi_p_min, pt_min=pi_pt_min, mipchi2_min=pi_ipchi2_min, pi_pidk_max=None
    )
    descriptor = "[Lambda0 -> p+ pi-]cc"
    combination_code = require_all(
        in_range(mass_window_comb_min, F.MASS, mass_window_comb_max),
        F.MAXDOCACHI2CUT(adocachi2cut),
    )
    vertex_code = require_all(
        in_range(mass_window_min, F.MASS, mass_window_max),
        F.CHI2DOF < vchi2pdof_max,
        F.END_VZ > endvz_min,
        F.OWNPVLTIME > bpvltime_min,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
        F.OWNPVVDZ > bpvvdz_min,
    )
    return ParticleCombiner(
        [protons, pions],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_loose_lambda_DD(
    name="BNOC_loose_lambda_DD",
    make_protons=make_detached_down_protons,
    make_pions=make_detached_down_pions,
    make_pvs=make_pvs,
    mass_window_comb_min=1035.0 * MeV,
    mass_window_comb_max=1195.0 * MeV,
    mass_window_min=1090.0 * MeV,
    mass_window_max=1140.0 * MeV,
    pi_p_min=2.0 * GeV,
    p_p_min=2.0 * GeV,
    p_pt_min=0.0,  # recommended to be small
    pi_pt_min=0.0,  # has to be 0 !!!
    pi_ipchi2_min=4.0,
    p_ipchi2_min=4.0,
    adocachi2cut=25.0,
    bpvltime_min=1.0 * ps,
    vchi2pdof_max=15.0,
    vt_p_min=5000.0 * MeV,
    vt_bpvfdchi2_min=50.0,
    endvz_min=250 * mm,
    endvz_max=2485 * mm,
):
    protons = make_protons(
        p_min=p_p_min, pt_min=p_pt_min, mipchi2_min=p_ipchi2_min, p_pidp_min=None
    )  # important to get rid of any PID cuts!
    pions = make_pions(
        p_min=pi_p_min, mipchi2_min=pi_ipchi2_min, pt_min=pi_pt_min, pi_pidk_max=None
    )
    descriptor = "[Lambda0 -> p+ pi-]cc"
    combination_code = require_all(
        in_range(mass_window_comb_min, F.MASS, mass_window_comb_max),
        F.MAXDOCACHI2CUT(adocachi2cut),
    )
    vertex_code = require_all(
        in_range(mass_window_min, F.MASS, mass_window_max),
        F.math.in_range(endvz_min, F.END_VZ, endvz_max),
        F.P > vt_p_min,
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVLTIME > bpvltime_min,
        F.OWNPVFDCHI2 > vt_bpvfdchi2_min,
    )

    return ParticleCombiner(
        [protons, pions],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


####################################
# Sigma0 -> Lambda0 gamma decays   #
####################################


@configurable
def make_sigma0_ll(
    mass_window_comb_min=1069.0 * MeV,
    mass_window_comb_max=1230.0 * MeV,
    mass_window_min=1089.0 * MeV,
    mass_window_max=1220.0 * MeV,
    pt_min=750 * MeV,
    adocachi2cut=25.0,
    gamma_et_min=100 * MeV,
    lambda_endvz=20 * mm,
):
    lambdas_ll = make_veryloose_lambda_LL(endvz_min=lambda_endvz)
    gamma = make_photons(et_min=gamma_et_min)
    descriptor = "[Sigma0 -> Lambda0 gamma]cc"
    combination_code = require_all(
        in_range(mass_window_comb_min, F.MASS, mass_window_comb_max),
        F.MAXDOCACHI2CUT(adocachi2cut),
    )
    vertex_code = require_all(
        in_range(mass_window_min, F.MASS, mass_window_max), F.PT > pt_min
    )
    return ParticleCombiner(
        [lambdas_ll, gamma],
        name="BNOC_sigma0_LL_{hash}",
        ParticleCombiner="ParticleAdder",
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_sigma0_dd(
    mass_window_comb_min=1059.0 * MeV,
    mass_window_comb_max=1230.0 * MeV,
    mass_window_min=1079.0 * MeV,
    mass_window_max=1220.0 * MeV,
    pt_min=750 * MeV,
    adocachi2cut=25.0,
    gamma_et_min=80 * MeV,
):
    lambdas_dd = make_loose_lambda_DD()
    gamma = make_photons(et_min=gamma_et_min)
    descriptor = "[Sigma0 -> Lambda0 gamma]cc"
    combination_code = require_all(
        in_range(mass_window_comb_min, F.MASS, mass_window_comb_max),
        F.MAXDOCACHI2CUT(adocachi2cut),
    )
    vertex_code = require_all(
        in_range(mass_window_min, F.MASS, mass_window_max),
        F.PT > pt_min,
    )
    return ParticleCombiner(
        [lambdas_dd, gamma],
        name="BNOC_sigma0_DD_{hash}",
        ParticleCombiner="ParticleAdder",
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


###############################################################################
# Specific hyperon decay builders, overrides default cuts where needed        #
###############################################################################
@configurable
def make_xim_to_lambda0pim(
    lambdas,
    pions,
    comb_m_min=1260 * MeV,
    comb_m_max=1390 * MeV,
    m_min=1272 * MeV,
    m_max=1372 * MeV,
    comb_p_min=9.0 * GeV,
    p_min=9.5 * GeV,
    pt_min=500 * MeV,
    doca_max=750.0 * um,
    vchi2pdof_max=12.0,
    bpvfdchi2_min=15.0,
    bpvvdz_min=3.5 * mm,
    bcvtx_sep_min=None,
    bpvdira_min=None,
):
    """
    Make Xi- -> Lambda pi-.
    """
    comb_cut = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.P > comb_p_min,
        F.MAXDOCACUT(doca_max),
    )
    vertex_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.P > p_min,
        F.PT > pt_min,
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
        F.OWNPVVDZ > bpvvdz_min,
    )
    if bpvdira_min is not None:
        vertex_cut &= F.require_all(F.OWNPVDIRA > bpvdira_min)
    if bcvtx_sep_min is not None:
        # daughter index is hardcoded as no other position is allowed
        vertex_cut &= (F.CHILD(1, F.END_VZ) - F.END_VZ) > bcvtx_sep_min

    return ParticleCombiner(
        [lambdas, pions],
        name="BNOC_XimToL0Pim_Combiner_{hash}",
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        CombinationCut=comb_cut,
        CompositeCut=vertex_cut,
    )


@configurable
def make_omegam_to_lambda0km(
    lambdas,
    kaons,
    comb_m_min=1612 * MeV,
    comb_m_max=1732 * MeV,
    m_min=1622 * MeV,
    m_max=1722 * MeV,
    comb_p_min=9.0 * GeV,
    p_min=9.5 * GeV,
    pt_min=500 * MeV,
    doca_max=150.0 * um,
    vchi2pdof_max=20.0,
    bpvfdchi2_min=10.0,
    bpvvdz_min=0.5 * mm,
    bcvtx_sep_min=None,
    bpvdira_min=None,
):
    """
    Make Omega- -> Lambda K-.
    """
    comb_cut = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.P > comb_p_min,
        F.MAXDOCACUT(doca_max),
    )
    vertex_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.P > p_min,
        F.PT > pt_min,
        F.CHI2DOF < vchi2pdof_max,
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVVDZ > bpvvdz_min,
    )
    if bpvdira_min is not None:
        vertex_cut &= F.require_all(F.OWNPVDIRA > bpvdira_min)
    if bcvtx_sep_min is not None:
        # daughter index is hardcoded as no other position is allowed
        vertex_cut &= (F.CHILD(1, F.END_VZ) - F.END_VZ) > bcvtx_sep_min

    return ParticleCombiner(
        [lambdas, kaons],
        name="BNOC_OmmToL0Km_Combiner_{hash}",
        DecayDescriptor="[Omega- -> Lambda0 K-]cc",
        CombinationCut=comb_cut,
        CompositeCut=vertex_cut,
    )


def make_xim_to_lambda_pi_lll(
    lambdas,
    pions,
    comb_m_min=1260 * MeV,
    comb_m_max=1390 * MeV,
    m_min=1272 * MeV,
    m_max=1372 * MeV,
    comb_p_min=9.0 * GeV,
    p_min=9.5 * GeV,
    pt_min=500 * MeV,
    doca_max=150.0 * um,
    vchi2pdof_max=10.0,
    bpvfdchi2_min=15.0,
    bpvvdz_min=3.5 * mm,
    bcvtx_sep_min=5 * mm,
    bpvdira_min=None,
):
    return make_xim_to_lambda0pim(
        lambdas=lambdas,
        pions=pions,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        m_min=m_min,
        m_max=m_max,
        comb_p_min=comb_p_min,
        p_min=p_min,
        pt_min=pt_min,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bcvtx_sep_min=bcvtx_sep_min,
        bpvdira_min=bpvdira_min,
    )


def make_xim_to_lambda_pi_ddl(
    lambdas,
    pions,
    comb_m_min=1240 * MeV,
    comb_m_max=1410 * MeV,
    m_min=1260 * MeV,
    m_max=1390 * MeV,
    comb_p_min=9.0 * GeV,
    p_min=9.5 * GeV,
    doca_max=750.0 * um,
    vchi2pdof_max=12.0,
    bpvvdz_min=4 * mm,
    bcvtx_sep_min=5 * mm,
    bpvdira_min=None,
):
    return make_xim_to_lambda0pim(
        lambdas=lambdas,
        pions=pions,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        m_min=m_min,
        m_max=m_max,
        comb_p_min=comb_p_min,
        p_min=p_min,
        doca_max=doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bcvtx_sep_min=bcvtx_sep_min,
        bpvdira_min=bpvdira_min,
    )


def make_xim_to_lambda_pi_ddd(
    lambdas,
    pions,
    comb_m_min=1240 * MeV,
    comb_m_max=1410 * MeV,
    m_min=1260 * MeV,
    m_max=1390 * MeV,
    comb_p_min=9.0 * GeV,
    p_min=9.5 * GeV,
    doca_max=3.0 * mm,
    vchi2pdof_max=25.0,
    bpvvdz_min=20 * mm,
    bcvtx_sep_min=5 * mm,
    bpvdira_min=None,
):
    return make_xim_to_lambda0pim(
        lambdas=lambdas,
        pions=pions,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        m_min=m_min,
        m_max=m_max,
        comb_p_min=comb_p_min,
        p_min=p_min,
        doca_max=doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bcvtx_sep_min=bcvtx_sep_min,
        bpvdira_min=bpvdira_min,
    )


def make_omegam_to_lambda_k_lll(
    lambdas,
    kaons,
    comb_m_min=1612 * MeV,
    comb_m_max=1732 * MeV,
    m_min=1622 * MeV,
    m_max=1722 * MeV,
    comb_p_min=9.0 * GeV,
    p_min=9.5 * GeV,
    doca_max=150.0 * um,
    vchi2pdof_max=10.0,
    bpvvdz_min=2.0 * mm,
    bcvtx_sep_min=5 * mm,
    bpvdira_min=None,
):
    return make_omegam_to_lambda0km(
        lambdas=lambdas,
        kaons=kaons,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        m_min=m_min,
        m_max=m_max,
        comb_p_min=comb_p_min,
        p_min=p_min,
        doca_max=doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bcvtx_sep_min=bcvtx_sep_min,
        bpvdira_min=bpvdira_min,
    )


def make_omegam_to_lambda_k_ddl(
    lambdas,
    kaons,
    comb_m_min=1600 * MeV,
    comb_m_max=1750 * MeV,
    m_min=1610 * MeV,
    m_max=1730 * MeV,
    comb_p_min=9.0 * GeV,
    p_min=9.5 * GeV,
    doca_max=750.0 * um,
    vchi2pdof_max=12.0,
    bpvvdz_min=2.0 * mm,
    bcvtx_sep_min=5 * mm,
    bpvdira_min=None,
):
    return make_omegam_to_lambda0km(
        lambdas=lambdas,
        kaons=kaons,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        m_min=m_min,
        m_max=m_max,
        comb_p_min=comb_p_min,
        p_min=p_min,
        doca_max=doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bcvtx_sep_min=bcvtx_sep_min,
        bpvdira_min=bpvdira_min,
    )


def make_omegam_to_lambda_k_ddd(
    lambdas,
    kaons,
    comb_m_min=1590 * MeV,
    comb_m_max=1750 * MeV,
    m_min=1600 * MeV,
    m_max=1740 * MeV,
    comb_p_min=9.0 * GeV,
    p_min=9.5 * GeV,
    doca_max=3.0 * mm,
    vchi2pdof_max=24.0,
    bpvvdz_min=20 * mm,
    bcvtx_sep_min=5 * mm,
    bpvdira_min=None,
):
    return make_omegam_to_lambda0km(
        lambdas=lambdas,
        kaons=kaons,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        m_min=m_min,
        m_max=m_max,
        comb_p_min=comb_p_min,
        p_min=p_min,
        doca_max=doca_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bcvtx_sep_min=bcvtx_sep_min,
        bpvdira_min=bpvdira_min,
    )


################################
# select topo-prefiltered tracks
################################


@configurable
def Topo_prefilter_extra_outputs(min_twobody_mva=0.1, min_threebody_mva=0.1):
    return [
        ("Topo_2Body", make_filtered_topo_twobody(MVACut=min_twobody_mva)),
        ("Topo_3Body", make_filtered_topo_threebody(MVACut=min_threebody_mva)),
    ]


####################################
# Xi0  2-body decays               #
####################################


@configurable
def make_xi0(
    lambdas,
    pi0,
    comb_m_min=1150 * MeV,
    comb_m_max=1450 * MeV,
    comb_pt_min=200 * MeV,
    m_min=1200 * MeV,
    m_max=1400 * MeV,
    pt_min=200 * MeV,
    can_reco_vertex=False,
    vchi2pdof_max=50.0,
    bpvltime_min=2.0 * ps,
):
    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max), F.SUM(F.PT) > comb_pt_min
    )
    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVLTIME > bpvltime_min,
    )
    return ParticleCombiner(
        [lambdas, pi0],
        name="Xi0ToL0Pi0Combiner_{hash}",
        ParticleCombiner=(
            "ParticleVertexFitter" if can_reco_vertex else "ParticleAdder"
        ),
        DecayDescriptor="[Xi0 -> Lambda0 pi0]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
