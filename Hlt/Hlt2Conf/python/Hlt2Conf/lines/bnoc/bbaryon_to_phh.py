###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of BNOC b-baryon -> proton 2h lines
Implemented:
Xib- -> pKK
Xib- -> pKpi
Xib- -> ppd
Xib- -> ppipi
Xib- -> L0pi (LL / DD)
Xib- -> L0K (LL / DD)
"""

from GaudiKernel.SystemOfUnits import MeV

from Hlt2Conf.lines.bnoc.builders.b_builder import (
    make_bbaryon_2body,
    make_bbaryon_3body,
)
from Hlt2Conf.lines.bnoc.builders.basic_builder import (
    make_bbaryon_detached_deuterons,
    make_bbaryon_detached_kaons,
    make_bbaryon_detached_pions,
    make_bbaryon_detached_protons,
    make_bbaryon_soft_kaons,
    make_bbaryon_soft_pions,
    make_bbaryon_soft_protons,
    make_loose_lambda_DD,
    make_sigma0_dd,
    make_sigma0_ll,
    make_veryloose_lambda_LL,
)
from Hlt2Conf.lines.bnoc.utils import check_process

all_lines = {}

###################
### Global cuts ###
###################

xibm_3body_no_sep_cut = {
    "mass_min": 5500 * MeV,
    "mass_max": 6600 * MeV,
    "pt_sum_min": 4100 * MeV,
    "ipchi2_max": 12.0,
    "bpvfdchi2_min": 50.0,
    "dira_min": 0.9997,
    "med_pt_min": 700 * MeV,
    "med_bpvipchi2_min": 30.0,
    "bpvipchi2_sum_min": 150.0,
    "bcvtx_sep_min": None,
}

xibm_sigma0_no_sep_cut = {
    "mass_min": 5200 * MeV,
    "mass_max": 6305 * MeV,
    "pt_min": 2000 * MeV,
    "ipchi2_max": 12.0,
    "dira_min": 0.995,
    "bcvtx_sep_min": None,
}

##################
### HLT2 Lines ###
##################


# b-baryon -> proton 2h
@check_process
def make_XibmToPpKmKm(process):
    if process == "spruce":
        protons = make_bbaryon_soft_protons(pid=None)
        hadron1 = make_bbaryon_soft_kaons(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_soft_protons()
        hadron1 = make_bbaryon_soft_kaons()
    line_alg = make_bbaryon_3body(
        particles=[protons, hadron1, hadron1],
        descriptor="[Xi_b- -> p+ K- K-]cc",
        **xibm_3body_no_sep_cut,
    )
    return line_alg


@check_process
def make_XibmToPpKmPim(process):
    if process == "spruce":
        protons = make_bbaryon_soft_protons(pid=None)
        hadron1 = make_bbaryon_soft_kaons(pid=None)
        hadron2 = make_bbaryon_soft_pions(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_soft_protons()
        hadron1 = make_bbaryon_soft_kaons()
        hadron2 = make_bbaryon_soft_pions()
    line_alg = make_bbaryon_3body(
        particles=[protons, hadron1, hadron2],
        descriptor="[Xi_b- -> p+ K- pi-]cc",
        **xibm_3body_no_sep_cut,
    )
    return line_alg


@check_process
def make_XibmToPpPimPim(process):
    if process == "spruce":
        protons = make_bbaryon_soft_protons(pid=None)
        hadron1 = make_bbaryon_soft_pions(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_soft_protons()
        hadron1 = make_bbaryon_soft_pions()
    line_alg = make_bbaryon_3body(
        particles=[protons, hadron1, hadron1],
        descriptor="[Xi_b- -> p+ pi- pi-]cc",
        **xibm_3body_no_sep_cut,
    )
    return line_alg


@check_process
def make_BuToPpPpDeutm(process):
    if process == "spruce":
        protons = make_bbaryon_detached_protons(pid=None)
        deuterons = make_bbaryon_detached_deuterons(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_detached_protons()
        deuterons = make_bbaryon_detached_deuterons(pid=None)
    line_alg = make_bbaryon_3body(
        particles=[protons, protons, deuterons],
        descriptor="[B+ -> p+ p+ deuteron~]cc",
        **xibm_3body_no_sep_cut,
    )
    return line_alg


@check_process
def make_BuToPpPpDeutm_SS(process):  # same sign version of BuToPpPpDeutm
    if process == "spruce":
        protons = make_bbaryon_detached_protons(pid=None)
        deuterons = make_bbaryon_detached_deuterons(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_detached_protons()
        deuterons = make_bbaryon_detached_deuterons(pid=None)
    line_alg = make_bbaryon_3body(
        particles=[protons, protons, deuterons],
        descriptor="[B+ -> p+ p+ deuteron]cc",
        **xibm_3body_no_sep_cut,
    )
    return line_alg


# b-baryon -> Lambda0 h LL / DD


@check_process
def make_XibmToL0Pim_LL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_2body(
        particles=[lambda0s, hadron1], descriptor="[Xi_b- -> Lambda0 pi-]cc"
    )
    return [lambda0s, line_alg]


@check_process
def make_XibmToL0Pim_DD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_2body(
        particles=[lambda0s, hadron1], descriptor="[Xi_b- -> Lambda0 pi-]cc"
    )
    return [lambda0s, line_alg]


@check_process
def make_XibmToL0Km_LL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_2body(
        particles=[lambda0s, hadron1], descriptor="[Xi_b- -> Lambda0 K-]cc"
    )
    return [lambda0s, line_alg]


@check_process
def make_XibmToL0Km_DD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_2body(
        particles=[lambda0s, hadron1], descriptor="[Xi_b- -> Lambda0 K-]cc"
    )
    return [lambda0s, line_alg]


# b-baryon -> Sigma0 (-> Lambda0 gamma) h LL / DD


@check_process
def make_XibmToSigma0Km_LL(process):
    if process == "spruce":
        sigma0s = make_sigma0_ll()
        hadron1 = make_bbaryon_detached_kaons()
    elif process == "hlt2":
        sigma0s = make_sigma0_ll()
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_2body(
        particles=[sigma0s, hadron1],
        descriptor="[Xi_b- -> Sigma0 K-]cc",
        **xibm_sigma0_no_sep_cut,
    )
    return [sigma0s, line_alg]


@check_process
def make_XibmToSigma0Km_DD(process):
    if process == "spruce":
        sigma0s = make_sigma0_dd()
        hadron1 = make_bbaryon_detached_kaons()
    elif process == "hlt2":
        sigma0s = make_sigma0_dd()
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_2body(
        particles=[sigma0s, hadron1],
        descriptor="[Xi_b- -> Sigma0 K-]cc",
        **xibm_sigma0_no_sep_cut,
    )
    return [sigma0s, line_alg]


@check_process
def make_XibmToSigma0Pim_LL(process):
    if process == "spruce":
        sigma0s = make_sigma0_ll()
        hadron1 = make_bbaryon_detached_pions()
    elif process == "hlt2":
        sigma0s = make_sigma0_ll()
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_2body(
        particles=[sigma0s, hadron1],
        descriptor="[Xi_b- -> Sigma0 pi-]cc",
        **xibm_sigma0_no_sep_cut,
    )
    return [sigma0s, line_alg]


@check_process
def make_XibmToSigma0Pim_DD(process):
    if process == "spruce":
        sigma0s = make_sigma0_dd()
        hadron1 = make_bbaryon_detached_pions()
    elif process == "hlt2":
        sigma0s = make_sigma0_dd()
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_2body(
        particles=[sigma0s, hadron1],
        descriptor="[Xi_b- -> Sigma0 pi-]cc",
        **xibm_sigma0_no_sep_cut,
    )
    return [sigma0s, line_alg]
