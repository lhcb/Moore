###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Lines to select the decay B(s)0 -> KS0 KS0 and KS0 -> pi+ pi-.
The KS0 can be reconstructed either from long or downstream tracks, resulting
in the combinations LLLL, LLDD, DDDD.

"""

from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from PyConf import configurable

from Hlt2Conf.lines.bnoc.builders.b_builder import make_b2ksks
from Hlt2Conf.lines.bnoc.builders.basic_builder import (
    make_bbaryon_ks0_dd,
    make_bbaryon_ks0_ll,
)


@configurable
def make_BdsToKSKS_LLLL(process):
    ll_kshorts = make_bbaryon_ks0_ll()
    line_alg = make_b2ksks(
        particles=[ll_kshorts, ll_kshorts],
        descriptor="B_s0 -> KS0 KS0",
        am_min=4900 * MeV,
        am_max=5700 * MeV,
        adoca_max=1 * mm,
        vtx_am_min=4600 * MeV,
        vtx_am_max=5900 * MeV,
        vchi2pdof_max=20,
        bcvtx_sep_min=15 * mm,
    )
    return [ll_kshorts, line_alg]


@configurable
def make_BdsToKSKS_LLDD(process):
    ll_kshorts = make_bbaryon_ks0_ll()
    dd_kshorts = make_bbaryon_ks0_dd(
        pi_p_min=2.0 * GeV, vchi2pdof_max=9, pt_min=2500.0 * MeV
    )
    line_alg = make_b2ksks(
        particles=[dd_kshorts, ll_kshorts],
        descriptor="B_s0 -> KS0 KS0",
        am_min=4900 * MeV,
        am_max=5700 * MeV,
        asumpt_min=2000 * MeV,
        vchi2pdof_max=30,
        bcvtx_sep_min=15 * mm,
        AllowDiffInputsForSameIDChildren=True,
    )
    return [ll_kshorts, dd_kshorts, line_alg]


@configurable
def make_BdsToKSKS_DDDD(process):
    dd_kshorts = make_bbaryon_ks0_dd(
        pi_p_min=2.0 * GeV, vchi2pdof_max=9, pt_min=2500.0 * MeV
    )
    line_alg = make_b2ksks(
        particles=[dd_kshorts, dd_kshorts],
        descriptor="B_s0 -> KS0 KS0",
        am_min=4900 * MeV,
        am_max=5700 * MeV,
        asumpt_min=2000 * MeV,
        vchi2pdof_max=30,
    )
    return [dd_kshorts, line_alg]
