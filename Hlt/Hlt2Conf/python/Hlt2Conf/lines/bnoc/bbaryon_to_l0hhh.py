###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of BnoC lines with a final signature as Hb- -> L0 3h
b-baryon -> L0 3h lines
Implemented:
Xibm/Ombm -> L0KKK
Xibm/Ombm -> L0KKpi
Xibm/Ombm -> L0Kpipi
Xibm/Ombm -> L0pipipi

b-baryon -> p KS hh

Xibm/Ombm -> pKSpipi
Xibm/Ombm -> pKSKpi
Xibm/Ombm -> pKSKK


All lines are implemented as LL/DD

"""

from Hlt2Conf.lines.bnoc.builders.b_builder import make_bbaryon_4body
from Hlt2Conf.lines.bnoc.builders.basic_builder import (
    make_bbaryon_detached_kaons,
    make_bbaryon_detached_pions,
    make_bbaryon_detached_protons,
    make_bbaryon_ks0_dd,
    make_bbaryon_ks0_ll,
    make_loose_lambda_DD,
    make_veryloose_lambda_LL,
)
from Hlt2Conf.lines.bnoc.utils import check_process

all_lines = {}

###################
### Global cuts ###
###################

xibm_3body_l0_ll_cut = {
    "bpvipchi2_sum_min": 200.0,
}

xibm_3body_l0_dd_cut = {
    "bpvipchi2_sum_min": 100.0,
    "bcvtx_sep_min": None,  # "bcvtx_sep_min": 50 * mm
}

xibm_3body_ks_ll_cut = {"bpvipchi2_sum_min": 200.0, "daughter_index": 2}

xibm_3body_ks_dd_cut = {
    "bpvipchi2_sum_min": 100.0,
    "bcvtx_sep_min": None,  # 40 * mm
    "daughter_index": 2,
}

##################
### HLT2 Lines ###
##################

# b-baryon -> L0 3h LL


@check_process
def make_XibmToL0KpKmKm_LL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_4body(
        particles=[lambda0s, hadron1, hadron1, hadron1],
        descriptor="[Xi_b- -> Lambda0 K+ K- K-]cc",
        **xibm_3body_l0_ll_cut,
    )
    return [lambda0s, line_alg]


@check_process
def make_XibmToL0KpKmPim_LL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[lambda0s, hadron1, hadron1, hadron2],
        descriptor="[Xi_b- -> Lambda0 K+ K- pi-]cc",
        **xibm_3body_l0_ll_cut,
    )
    return [lambda0s, line_alg]


@check_process
def make_XibmToL0KmKmPip_LL(process):
    if process == "spruce":
        lambda0s = (make_veryloose_lambda_LL(),)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[lambda0s, hadron1, hadron1, hadron2],
        descriptor="[Xi_b- -> Lambda0 K- K- pi+]cc",
        **xibm_3body_l0_ll_cut,
    )
    return [lambda0s, line_alg]


@check_process
def make_XibmToL0KmPipPim_LL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[lambda0s, hadron1, hadron2, hadron2],
        descriptor="[Xi_b- -> Lambda0 K- pi+ pi-]cc",
        **xibm_3body_l0_ll_cut,
    )
    return [lambda0s, line_alg]


@check_process
def make_XibmToL0KpPimPim_LL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[lambda0s, hadron1, hadron2, hadron2],
        descriptor="[Xi_b- -> Lambda0 K+ pi- pi-]cc",
        **xibm_3body_l0_ll_cut,
    )
    return [lambda0s, line_alg]


@check_process
def make_XibmToL0PipPimPim_LL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[lambda0s, hadron1, hadron1, hadron1],
        descriptor="[Xi_b- -> Lambda0 pi+ pi- pi-]cc",
        **xibm_3body_l0_ll_cut,
    )
    return [lambda0s, line_alg]


@check_process
def make_XibmToL0PpPmPim_LL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_protons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        hadron1 = make_bbaryon_detached_protons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[lambda0s, hadron1, hadron1, hadron2],
        descriptor="[Xi_b- -> Lambda0 p+ p~- pi-]cc",
        **xibm_3body_l0_ll_cut,
    )
    return [lambda0s, line_alg]


# b-baryon -> L0 3h DD


@check_process
def make_XibmToL0KpKmKm_DD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_4body(
        particles=[lambda0s, hadron1, hadron1, hadron1],
        descriptor="[Xi_b- -> Lambda0 K+ K- K-]cc",
        **xibm_3body_l0_dd_cut,
    )
    return [lambda0s, line_alg]


@check_process
def make_XibmToL0KpKmPim_DD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[lambda0s, hadron1, hadron1, hadron2],
        descriptor="[Xi_b- -> Lambda0 K+ K- pi-]cc",
        **xibm_3body_l0_dd_cut,
    )
    return [lambda0s, line_alg]


@check_process
def make_XibmToL0KmKmPip_DD(process):
    if process == "spruce":
        lambda0s = (make_loose_lambda_DD(),)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[lambda0s, hadron1, hadron1, hadron2],
        descriptor="[Xi_b- -> Lambda0 K- K- pi+]cc",
        **xibm_3body_l0_dd_cut,
    )
    return [lambda0s, line_alg]


@check_process
def make_XibmToL0KmPipPim_DD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[lambda0s, hadron1, hadron2, hadron2],
        descriptor="[Xi_b- -> Lambda0 K- pi+ pi-]cc",
        **xibm_3body_l0_dd_cut,
    )
    return [lambda0s, line_alg]


@check_process
def make_XibmToL0KpPimPim_DD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[lambda0s, hadron1, hadron2, hadron2],
        descriptor="[Xi_b- -> Lambda0 K+ pi- pi-]cc",
        **xibm_3body_l0_dd_cut,
    )
    return [lambda0s, line_alg]


@check_process
def make_XibmToL0PipPimPim_DD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[lambda0s, hadron1, hadron1, hadron1],
        descriptor="[Xi_b- -> Lambda0 pi+ pi- pi-]cc",
        **xibm_3body_l0_dd_cut,
    )
    return [lambda0s, line_alg]


@check_process
def make_XibmToL0PpPmPim_DD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_protons()
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        hadron1 = make_bbaryon_detached_protons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[lambda0s, hadron1, hadron1, hadron2],
        descriptor="[Xi_b- -> Lambda0 p+ p~- pi-]cc",
        **xibm_3body_l0_dd_cut,
    )
    return [lambda0s, line_alg]


# b-baryon -> p KS hh LL / DD


@check_process
def make_XibmToPpKSPimPim_LL(process):
    if process == "spruce":
        protons = make_bbaryon_detached_protons()
        ks0s = make_bbaryon_ks0_ll()
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_detached_protons()
        ks0s = make_bbaryon_ks0_ll()
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[protons, ks0s, hadron1, hadron1],
        descriptor="[Xi_b- -> p+ KS0 pi- pi-]cc",
        **xibm_3body_ks_ll_cut,
    )
    return [ks0s, line_alg]


@check_process
def make_XibmToPpKSKmPim_LL(process):
    if process == "spruce":
        protons = make_bbaryon_detached_protons()
        ks0s = make_bbaryon_ks0_ll()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_detached_protons()
        ks0s = make_bbaryon_ks0_ll()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[protons, ks0s, hadron1, hadron2],
        descriptor="[Xi_b- -> p+ KS0 K- pi-]cc",
        **xibm_3body_ks_ll_cut,
    )
    return [ks0s, line_alg]


@check_process
def make_XibmToPpKSKmKm_LL(process):
    if process == "spruce":
        protons = make_bbaryon_detached_protons()
        ks0s = make_bbaryon_ks0_ll()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_detached_protons()
        ks0s = make_bbaryon_ks0_ll()
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_4body(
        particles=[protons, ks0s, hadron1, hadron1],
        descriptor="[Xi_b- -> p+ KS0 K- K-]cc",
        **xibm_3body_ks_ll_cut,
    )
    return [ks0s, line_alg]


@check_process
def make_XibmToPpKSPimPim_DD(process):
    if process == "spruce":
        protons = make_bbaryon_detached_protons()
        ks0s = make_bbaryon_ks0_dd()
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_detached_protons()
        ks0s = make_bbaryon_ks0_dd()
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[protons, ks0s, hadron1, hadron1],
        descriptor="[Xi_b- -> p+ KS0 pi- pi-]cc",
        **xibm_3body_ks_dd_cut,
    )
    return [ks0s, line_alg]


@check_process
def make_XibmToPpKSKmPim_DD(process):
    if process == "spruce":
        protons = make_bbaryon_detached_protons()
        ks0s = make_bbaryon_ks0_dd()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_detached_protons()
        ks0s = make_bbaryon_ks0_dd()
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_4body(
        particles=[protons, ks0s, hadron1, hadron2],
        descriptor="[Xi_b- -> p+ KS0 K- pi-]cc",
        **xibm_3body_ks_dd_cut,
    )
    return [ks0s, line_alg]


@check_process
def make_XibmToPpKSKmKm_DD(process):
    if process == "spruce":
        protons = make_bbaryon_detached_protons()
        ks0s = make_bbaryon_ks0_dd()
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == "hlt2":
        protons = make_bbaryon_detached_protons()
        ks0s = make_bbaryon_ks0_dd()
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_4body(
        particles=[protons, ks0s, hadron1, hadron1],
        descriptor="[Xi_b- -> p+ KS0 K- K-]cc",
        **xibm_3body_ks_dd_cut,
    )
    return [ks0s, line_alg]
