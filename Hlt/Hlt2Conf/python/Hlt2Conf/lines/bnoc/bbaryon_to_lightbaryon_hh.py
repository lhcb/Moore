###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of BnoC lines with a final signature as Hb- -> L0 3h
b-baryon -> Xim / Omm hh LLL / DDL / DDD

"""

from GaudiKernel.SystemOfUnits import mm

from Hlt2Conf.lines.bnoc.builders.b_builder import (
    make_bbaryon_2body,
    make_bbaryon_3body,
)
from Hlt2Conf.lines.bnoc.builders.basic_builder import (
    make_bbaryon_detached_down_kaons,
    make_bbaryon_detached_kaons,
    make_bbaryon_detached_pions,
    make_bbaryon_detached_protons,
    make_bbaryon_ks0_dd,
    make_bbaryon_ks0_ll,
    make_detached_down_pions,
    make_loose_lambda_DD,
    make_omegam_to_lambda_k_ddd,
    make_omegam_to_lambda_k_ddl,
    make_omegam_to_lambda_k_lll,
    make_phi,
    make_veryloose_lambda_LL,
    make_xim_to_lambda_pi_ddd,
    make_xim_to_lambda_pi_ddl,
    make_xim_to_lambda_pi_lll,
)
from Hlt2Conf.lines.bnoc.utils import check_process

all_lines = {}

xibm_2body_ddl_cut = {"bcvtx_sep_min": None}

xibm_2body_ddd_cut = {"bcvtx_sep_min": 100 * mm}

xibm_3body_lll_cut = {
    "bpvipchi2_sum_min": 200.0,
}

xibm_3body_ddl_cut = {"bpvipchi2_sum_min": 100.0, "bcvtx_sep_min": None}

xibm_3body_ddd_cut = {"bpvipchi2_sum_min": 100.0, "bcvtx_sep_min": 100 * mm}

# b-baryon -> Xim / Omm 2h LLL


@check_process
def make_XibmToXimPipPim_LLL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_lll(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_lll(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[xims, hadron1, hadron1],
        descriptor="[Xi_b- -> Xi- pi+ pi-]cc",
        **xibm_3body_lll_cut,
    )
    return [lambda0s, xims, line_alg]


@check_process
def make_XibmToXimKpPim_LLL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_lll(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_lll(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[xims, hadron1, hadron2],
        descriptor="[Xi_b- -> Xi- K+ pi-]cc",
        **xibm_3body_lll_cut,
    )
    return [lambda0s, xims, line_alg]


@check_process
def make_XibmToXimKmPip_LLL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_lll(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_lll(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[xims, hadron1, hadron2],
        descriptor="[Xi_b- -> Xi- K- pi+]cc",
        **xibm_3body_lll_cut,
    )
    return [lambda0s, xims, line_alg]


@check_process
def make_XibmToXimKpKm_LLL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_lll(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_lll(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_3body(
        particles=[xims, hadron1, hadron1],
        descriptor="[Xi_b- -> Xi- K+ K-]cc",
        **xibm_3body_lll_cut,
    )
    return [lambda0s, xims, line_alg]


@check_process
def make_XibmToXimPpPm_LLL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_lll(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_protons()
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_lll(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_protons()
    line_alg = make_bbaryon_3body(
        particles=[xims, hadron1, hadron1],
        descriptor="[Xi_b- -> Xi- p+ p~-]cc",
        **xibm_3body_lll_cut,
    )
    return [lambda0s, xims, line_alg]


@check_process
def make_OmbmToOmmPipPim_LLL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_lll(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_lll(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[omms, hadron1, hadron1],
        descriptor="[Omega_b- -> Omega- pi+ pi-]cc",
        **xibm_3body_lll_cut,
    )
    return [lambda0s, omms, line_alg]


@check_process
def make_OmbmToOmmKpPim_LLL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_lll(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_lll(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[omms, hadron1, hadron2],
        descriptor="[Omega_b- -> Omega- K+ pi-]cc",
        **xibm_3body_lll_cut,
    )
    return [lambda0s, omms, line_alg]


@check_process
def make_OmbmToOmmKmPip_LLL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_lll(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_lll(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[omms, hadron1, hadron2],
        descriptor="[Omega_b- -> Omega- K- pi+]cc",
        **xibm_3body_lll_cut,
    )
    return [lambda0s, omms, line_alg]


@check_process
def make_OmbmToOmmKpKm_LLL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_lll(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_lll(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_3body(
        particles=[omms, hadron1, hadron1],
        descriptor="[Omega_b- -> Omega- K+ K-]cc",
        **xibm_3body_lll_cut,
    )
    return [lambda0s, omms, line_alg]


@check_process
def make_OmbmToOmmPpPm_LLL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_lll(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_protons()
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_lll(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_protons()
    line_alg = make_bbaryon_3body(
        particles=[omms, hadron1, hadron1],
        descriptor="[Omega_b- -> Omega- p+ p~-]cc",
        **xibm_3body_lll_cut,
    )
    return [lambda0s, omms, line_alg]


# b-baryon -> Xim / Omm 2h DDL


@check_process
def make_XibmToXimPipPim_DDL(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddl(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddl(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[xims, hadron1, hadron1],
        descriptor="[Xi_b- -> Xi- pi+ pi-]cc",
        **xibm_3body_ddl_cut,
    )
    return [lambda0s, xims, line_alg]


@check_process
def make_XibmToXimKpPim_DDL(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddl(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddl(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[xims, hadron1, hadron2],
        descriptor="[Xi_b- -> Xi- K+ pi-]cc",
        **xibm_3body_ddl_cut,
    )
    return [lambda0s, xims, line_alg]


@check_process
def make_XibmToXimKmPip_DDL(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddl(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddl(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[xims, hadron1, hadron2],
        descriptor="[Xi_b- -> Xi- K- pi+]cc",
        **xibm_3body_ddl_cut,
    )
    return [lambda0s, xims, line_alg]


@check_process
def make_XibmToXimKpKm_DDL(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddl(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddl(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_3body(
        particles=[xims, hadron1, hadron1],
        descriptor="[Xi_b- -> Xi- K+ K-]cc",
        **xibm_3body_ddl_cut,
    )
    return [lambda0s, xims, line_alg]


@check_process
def make_XibmToXimPpPm_DDL(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddl(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_protons()
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddl(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_protons()
    line_alg = make_bbaryon_3body(
        particles=[xims, hadron1, hadron1],
        descriptor="[Xi_b- -> Xi- p+ p~-]cc",
        **xibm_3body_ddl_cut,
    )
    return [lambda0s, xims, line_alg]


@check_process
def make_OmbmToOmmPipPim_DDL(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddl(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddl(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[omms, hadron1, hadron1],
        descriptor="[Omega_b- -> Omega- pi+ pi-]cc",
        **xibm_3body_ddl_cut,
    )
    return [lambda0s, omms, line_alg]


@check_process
def make_OmbmToOmmKpPim_DDL(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddl(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddl(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[omms, hadron1, hadron2],
        descriptor="[Omega_b- -> Omega- K+ pi-]cc",
        **xibm_3body_ddl_cut,
    )
    return [lambda0s, omms, line_alg]


@check_process
def make_OmbmToOmmKmPip_DDL(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddl(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddl(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[omms, hadron1, hadron2],
        descriptor="[Omega_b- -> Omega- K- pi+]cc",
        **xibm_3body_ddl_cut,
    )
    return [lambda0s, omms, line_alg]


@check_process
def make_OmbmToOmmKpKm_DDL(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddl(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddl(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_3body(
        particles=[omms, hadron1, hadron1],
        descriptor="[Omega_b- -> Omega- K+ K-]cc",
        **xibm_3body_ddl_cut,
    )
    return [lambda0s, omms, line_alg]


@check_process
def make_OmbmToOmmPpPm_DDL(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddl(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_protons()
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddl(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_protons()
    line_alg = make_bbaryon_3body(
        particles=[omms, hadron1, hadron1],
        descriptor="[Omega_b- -> Omega- p+ p~-]cc",
        **xibm_3body_ddl_cut,
    )
    return [lambda0s, omms, line_alg]


# b-baryon -> Xim / Omm 2h DDD


@check_process
def make_XibmToXimPipPim_DDD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_detached_down_pions()
        xims = make_xim_to_lambda_pi_ddd(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_detached_down_pions()
        xims = make_xim_to_lambda_pi_ddd(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[xims, hadron1, hadron1],
        descriptor="[Xi_b- -> Xi- pi+ pi-]cc",
        **xibm_3body_ddd_cut,
    )
    return [lambda0s, xims, line_alg]


@check_process
def make_XibmToXimKpPim_DDD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_detached_down_pions()
        xims = make_xim_to_lambda_pi_ddd(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_detached_down_pions()
        xims = make_xim_to_lambda_pi_ddd(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[xims, hadron1, hadron2],
        descriptor="[Xi_b- -> Xi- K+ pi-]cc",
        **xibm_3body_ddd_cut,
    )
    return [lambda0s, xims, line_alg]


@check_process
def make_XibmToXimKmPip_DDD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_detached_down_pions()
        xims = make_xim_to_lambda_pi_ddd(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_detached_down_pions()
        xims = make_xim_to_lambda_pi_ddd(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[xims, hadron1, hadron2],
        descriptor="[Xi_b- -> Xi- K- pi+]cc",
        **xibm_3body_ddd_cut,
    )
    return [lambda0s, xims, line_alg]


@check_process
def make_XibmToXimKpKm_DDD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_detached_down_pions()
        xims = make_xim_to_lambda_pi_ddd(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_detached_down_pions()
        xims = make_xim_to_lambda_pi_ddd(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_3body(
        particles=[xims, hadron1, hadron1],
        descriptor="[Xi_b- -> Xi- K+ K-]cc",
        **xibm_3body_ddd_cut,
    )
    return [lambda0s, xims, line_alg]


@check_process
def make_XibmToXimPpPm_DDD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_detached_down_pions()
        xims = make_xim_to_lambda_pi_ddd(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_protons()
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_detached_down_pions()
        xims = make_xim_to_lambda_pi_ddd(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_detached_protons()
    line_alg = make_bbaryon_3body(
        particles=[xims, hadron1, hadron1],
        descriptor="[Xi_b- -> Xi- p+ p~-]cc",
        **xibm_3body_ddd_cut,
    )
    return [lambda0s, xims, line_alg]


@check_process
def make_OmbmToOmmPipPim_DDD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_down_kaons()
        omms = make_omegam_to_lambda_k_ddd(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_down_kaons()
        omms = make_omegam_to_lambda_k_ddd(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[omms, hadron1, hadron1],
        descriptor="[Omega_b- -> Omega- pi+ pi-]cc",
        **xibm_3body_ddd_cut,
    )
    return [lambda0s, omms, line_alg]


@check_process
def make_OmbmToOmmKpPim_DDD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_down_kaons()
        omms = make_omegam_to_lambda_k_ddd(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_down_kaons()
        omms = make_omegam_to_lambda_k_ddd(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[omms, hadron1, hadron2],
        descriptor="[Omega_b- -> Omega- K+ pi-]cc",
        **xibm_3body_ddd_cut,
    )
    return [lambda0s, omms, line_alg]


@check_process
def make_OmbmToOmmKmPip_DDD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_down_kaons()
        omms = make_omegam_to_lambda_k_ddd(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
        hadron2 = make_bbaryon_detached_pions(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_down_kaons()
        omms = make_omegam_to_lambda_k_ddd(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons()
        hadron2 = make_bbaryon_detached_pions()
    line_alg = make_bbaryon_3body(
        particles=[omms, hadron1, hadron2],
        descriptor="[Omega_b- -> Omega- K- pi+]cc",
        **xibm_3body_ddd_cut,
    )
    return [lambda0s, omms, line_alg]


@check_process
def make_OmbmToOmmKpKm_DDD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_down_kaons()
        omms = make_omegam_to_lambda_k_ddd(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons(pid=None)
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_down_kaons()
        omms = make_omegam_to_lambda_k_ddd(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_kaons()
    line_alg = make_bbaryon_3body(
        particles=[omms, hadron1, hadron1],
        descriptor="[Omega_b- -> Omega- K+ K-]cc",
        **xibm_3body_ddd_cut,
    )
    return [lambda0s, omms, line_alg]


@check_process
def make_OmbmToOmmPpPm_DDD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_down_kaons()
        omms = make_omegam_to_lambda_k_ddd(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_protons()
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_down_kaons()
        omms = make_omegam_to_lambda_k_ddd(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_detached_protons()
    line_alg = make_bbaryon_3body(
        particles=[omms, hadron1, hadron1],
        descriptor="[Omega_b- -> Omega- p+ p~-]cc",
        **xibm_3body_ddd_cut,
    )
    return [lambda0s, omms, line_alg]


# Xi_b- -> Xim (LLL/DDL/DDD) KS (LL/DD)


@check_process
def make_XibmToXimKS_LLLLL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_lll(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_ks0_ll()
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_lll(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_ks0_ll()
    line_alg = make_bbaryon_2body(
        particles=[xims, hadron1], descriptor="[Xi_b- -> Xi- KS0]cc"
    )
    return [hadron1, lambda0s, xims, line_alg]


@check_process
def make_XibmToXimKS_LLLDD(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_lll(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_ks0_dd()
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_lll(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_ks0_dd()
    line_alg = make_bbaryon_2body(
        particles=[xims, hadron1], descriptor="[Xi_b- -> Xi- KS0]cc"
    )
    return [hadron1, lambda0s, xims, line_alg]


@check_process
def make_XibmToXimKS_DDLLL(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddl(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_ks0_ll()
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddl(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_ks0_ll()
    line_alg = make_bbaryon_2body(
        particles=[xims, hadron1],
        descriptor="[Xi_b- -> Xi- KS0]cc",
        **xibm_2body_ddl_cut,
    )
    return [hadron1, lambda0s, xims, line_alg]


@check_process
def make_XibmToXimKS_DDLDD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddl(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_ks0_dd()
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddl(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_ks0_dd()
    line_alg = make_bbaryon_2body(
        particles=[xims, hadron1],
        descriptor="[Xi_b- -> Xi- KS0]cc",
        **xibm_2body_ddl_cut,
    )
    return [hadron1, lambda0s, xims, line_alg]


@check_process
def make_XibmToXimKS_DDDLL(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_detached_down_pions()
        xims = make_xim_to_lambda_pi_ddd(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_ks0_ll()
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_detached_down_pions()
        xims = make_xim_to_lambda_pi_ddd(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_ks0_ll()
    line_alg = make_bbaryon_2body(
        particles=[xims, hadron1],
        descriptor="[Xi_b- -> Xi- KS0]cc",
        **xibm_2body_ddd_cut,
    )
    return [hadron1, lambda0s, xims, line_alg]


@check_process
def make_XibmToXimKS_DDDDD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_detached_down_pions()
        xims = make_xim_to_lambda_pi_ddd(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_ks0_dd()
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_detached_down_pions()
        xims = make_xim_to_lambda_pi_ddd(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_bbaryon_ks0_dd()
    line_alg = make_bbaryon_2body(
        particles=[xims, hadron1],
        descriptor="[Xi_b- -> Xi- KS0]cc",
        **xibm_2body_ddd_cut,
    )
    return [hadron1, lambda0s, xims, line_alg]


# Omega_b- -> Omm (LLL/DDL/DDD) KS (LL/DD)


def make_OmbmToOmmKS_LLLLL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_lll(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_ks0_ll()
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_lll(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_ks0_ll()
    line_alg = make_bbaryon_2body(
        particles=[omms, hadron1], descriptor="[Omega_b- -> Omega- KS0]cc"
    )
    return [hadron1, lambda0s, omms, line_alg]


@check_process
def make_OmbmToOmmKS_LLLDD(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_lll(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_ks0_dd()
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_lll(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_ks0_dd()
    line_alg = make_bbaryon_2body(
        particles=[omms, hadron1], descriptor="[Omega_b- -> Omega- KS0]cc"
    )
    return [hadron1, lambda0s, omms, line_alg]


@check_process
def make_OmbmToOmmKS_DDLLL(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddl(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_ks0_ll()
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddl(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_ks0_ll()
    line_alg = make_bbaryon_2body(
        particles=[omms, hadron1],
        descriptor="[Omega_b- -> Omega- KS0]cc",
        **xibm_2body_ddl_cut,
    )
    return [hadron1, lambda0s, omms, line_alg]


@check_process
def make_OmbmToOmmKS_DDLDD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddl(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_ks0_dd()
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddl(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_ks0_dd()
    line_alg = make_bbaryon_2body(
        particles=[omms, hadron1],
        descriptor="[Omega_b- -> Omega- KS0]cc",
        **xibm_2body_ddl_cut,
    )
    return [hadron1, lambda0s, omms, line_alg]


@check_process
def make_OmbmToOmmKS_DDDLL(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_down_kaons()
        omms = make_omegam_to_lambda_k_ddd(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_ks0_ll()
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_down_kaons()
        omms = make_omegam_to_lambda_k_ddd(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_ks0_ll()
    line_alg = make_bbaryon_2body(
        particles=[omms, hadron1],
        descriptor="[Omega_b- -> Omega- KS0]cc",
        **xibm_2body_ddd_cut,
    )
    return [hadron1, lambda0s, omms, line_alg]


@check_process
def make_OmbmToOmmKS_DDDDD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_down_kaons()
        omms = make_omegam_to_lambda_k_ddd(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_ks0_dd()
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_down_kaons()
        omms = make_omegam_to_lambda_k_ddd(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_bbaryon_ks0_dd()
    line_alg = make_bbaryon_2body(
        particles=[omms, hadron1],
        descriptor="[Omega_b- -> Omega- KS0]cc",
        **xibm_2body_ddd_cut,
    )
    return [hadron1, lambda0s, omms, line_alg]


# Xi_b- -> Xim (LLL/DDL/DDD) phi


@check_process
def make_XibmToXimPhi_LLL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_lll(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_phi()
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_lll(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_phi()
    line_alg = make_bbaryon_2body(
        particles=[xims, hadron1], descriptor="[Xi_b- -> Xi- phi(1020)]cc"
    )
    return [hadron1, lambda0s, xims, line_alg]


@check_process
def make_XibmToXimPhi_DDL(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddl(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_phi()
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddl(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_phi()
    line_alg = make_bbaryon_2body(
        particles=[xims, hadron1],
        descriptor="[Xi_b- -> Xi- phi(1020)]cc",
        **xibm_2body_ddl_cut,
    )
    return [hadron1, lambda0s, xims, line_alg]


@check_process
def make_XibmToXimPhi_DDD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddd(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_phi()
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        xim_pions = make_bbaryon_detached_pions()
        xims = make_xim_to_lambda_pi_ddd(lambdas=lambda0s, pions=xim_pions)
        hadron1 = make_phi()
    line_alg = make_bbaryon_2body(
        particles=[xims, hadron1],
        descriptor="[Xi_b- -> Xi- phi(1020)]cc",
        **xibm_2body_ddd_cut,
    )
    return [hadron1, lambda0s, xims, line_alg]


# Omega_b- -> Omm (LLL/DDL/DDD) phi


def make_OmbmToOmmPhi_LLL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_lll(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_phi()
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_lll(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_phi()
    line_alg = make_bbaryon_2body(
        particles=[omms, hadron1], descriptor="[Omega_b- -> Omega- phi(1020)]cc"
    )
    return [hadron1, lambda0s, omms, line_alg]


@check_process
def make_OmbmToOmmPhi_DDL(process):
    if process == "spruce":
        lambda0s = make_veryloose_lambda_LL()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddl(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_phi()
    elif process == "hlt2":
        lambda0s = make_veryloose_lambda_LL()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddl(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_phi()
    line_alg = make_bbaryon_2body(
        particles=[omms, hadron1],
        descriptor="[Omega_b- -> Omega- phi(1020)]cc",
        **xibm_2body_ddl_cut,
    )
    return [hadron1, lambda0s, omms, line_alg]


@check_process
def make_OmbmToOmmPhi_DDD(process):
    if process == "spruce":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddd(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_phi()
    elif process == "hlt2":
        lambda0s = make_loose_lambda_DD()
        omm_kaons = make_bbaryon_detached_kaons()
        omms = make_omegam_to_lambda_k_ddd(lambdas=lambda0s, kaons=omm_kaons)
        hadron1 = make_phi()
    line_alg = make_bbaryon_2body(
        particles=[omms, hadron1],
        descriptor="[Omega_b- -> Omega- phi(1020)]cc",
        **xibm_2body_ddd_cut,
    )
    return [hadron1, lambda0s, omms, line_alg]
