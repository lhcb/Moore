###############################################################################
# (c) Copyright 2021-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of BNOC BToLambdahhh lines, with LL, DD and TT Lambda
* Use standard particle maker
"""

from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from PyConf import configurable
from RecoConf.standard_particles import make_LambdaTT

from Hlt2Conf.lines.bnoc.builders.b_builder import make_b2x
from Hlt2Conf.lines.bnoc.builders.basic_builder import (
    make_loose_lambda_DD,
    make_pions,
    make_tight_protons,
    make_veryloose_lambda_LL,
)
from Hlt2Conf.lines.bnoc.utils import check_process

# aprox masses of particles: lower than true value because of detector resolution
mL0 = 1110 * MeV
mp = 930 * MeV
mpi = 120 * MeV
mK = 480 * MeV
mMu = 100 * MeV

Bu_kwargs = {
    "am_min": 4800 * MeV,  # invariant mass
    "am_max": 5700 * MeV,
    "mothermass_min": 4900 * MeV,  # vertex mass, tighter than invariant mass
    "mothermass_max": 5600 * MeV,
    "asumpt_min": 4000 * MeV,  # sum PT
    "motherpt_min": 3000 * MeV,  # vertex fit PT
    "MassWindow": False,
    "am12_max": 4000.0 * MeV,
    "am123_max": 5000.0 * MeV,
    "achi2doca_max": 20.0,
    "dira_min": 0.9995,
    "adoca_max": 3 * mm,
    "vtxchi2pdof_max": 8.0,
    "mipchi2_max": 20,
    "bpvipchi2_max": 9.0,
    "daughter_mipchi2_min": 6,
}

Bds_kwargs = {
    "am_min": 4800 * MeV,
    "am_max": 5700 * MeV,
    "MassWindow": False,
    "am12_max": 4900.0 * MeV,
    "am123_max": 5600.0 * MeV,
    "adoca_max": 0.5 * mm,
    "dira_min": 0.9995,
    "asumpt_min": 4000 * MeV,
    "motherpt_min": 3000 * MeV,
    "achi2doca_max": 25.0,
    "bpvipchi2_max": 10,
    "vtxchi2pdof_max": 15,
    "daughter_mipchi2_min": 6,
}

Bu_kwargs_TT = {k: v for k, v in Bu_kwargs.items()}
Bu_kwargs_TT["vtxchi2pdof_max"] = 30
Bu_kwargs_TT["adoca_max"] = 5 * mm
Bu_kwargs_TT["bpvipchi2_max"] = 60

Bds_kwargs_TT = {k: v for k, v in Bds_kwargs.items()}
Bds_kwargs_TT["vtxchi2pdof_max"] = 30
Bds_kwargs_TT["adoca_max"] = 5 * mm
Bds_kwargs_TT["bpvipchi2_max"] = 60

LambdaLL_kwargs = {
    "name": "BNOC_veryloose_lambda_LL_{hash}",
    "mass_window_comb_min": 1090.0 * MeV,
    "mass_window_comb_max": 1150.0 * MeV,
    "mass_window_min": 1100.0 * MeV,
    "mass_window_max": 1130.0 * MeV,
    "p_pt_min": 0.05 * GeV,  # recommended to be small
    "bpvvdz_min": 15 * mm,
}

###############
## L0barphh  ##
###############


@configurable
def make_BuToL0barPpPpPm_LL(process):
    lambdas = make_veryloose_lambda_LL(**LambdaLL_kwargs)
    protons = make_tight_protons(mipchi2_min=4.0)
    line_alg = make_b2x(
        particles=[lambdas, protons, protons, protons],
        descriptor="[B+ -> Lambda~0 p+ p+  p~-]cc",
        **Bu_kwargs,
        am12_min=mL0 + mp,
        am123_min=mL0 + mp + mp,
    )
    return [lambdas, line_alg]


@configurable
def make_BuToL0barPpPpPm_DD(process):
    lambdas = make_loose_lambda_DD()
    protons = make_tight_protons(mipchi2_min=4.0)

    line_alg = make_b2x(
        particles=[lambdas, protons, protons, protons],
        descriptor="[B+ -> Lambda~0 p+ p+ p~-]cc",
        **Bu_kwargs,
        am12_min=mL0 + mp,
        am123_min=mL0 + mp + mp,
    )
    return [lambdas, line_alg]


@configurable
def make_BuToL0barPpPpPm_TT(process):
    lambdas = make_LambdaTT()
    protons = make_tight_protons(mipchi2_min=4.0)

    line_alg = make_b2x(
        particles=[lambdas, protons, protons, protons],
        descriptor="[B+ -> Lambda~0 p+ p+ p~-]cc",
        **Bu_kwargs_TT,
        am12_min=mL0 + mp,
        am123_min=mL0 + mp + mp,
    )
    return [lambdas, line_alg]


@configurable
def make_BuToL0barPpPipPim_NoPID_LL(process):
    lambdas = make_veryloose_lambda_LL(**LambdaLL_kwargs)
    protons = make_tight_protons(mipchi2_min=4.0)
    pions = make_pions(pi_pidk_max=None, p_min=1500 * MeV, pt_min=500 * MeV)
    line_alg = make_b2x(
        particles=[lambdas, protons, pions, pions],
        descriptor="[B+ -> Lambda~0 p+ pi- pi+ ]cc",
        **Bu_kwargs,
        am12_min=mL0 + mp,
        am123_min=mL0 + mp + mpi,
    )
    return [lambdas, line_alg]


@configurable
def make_BuToL0barPpPipPim_NoPID_DD(process):
    lambdas = make_loose_lambda_DD()
    protons = make_tight_protons(mipchi2_min=4.0)
    pions = make_pions(pi_pidk_max=None, p_min=1500 * MeV, pt_min=500 * MeV)
    line_alg = make_b2x(
        particles=[lambdas, protons, pions, pions],
        descriptor="[B+ -> Lambda~0 p+ pi- pi+]cc",
        **Bu_kwargs,
        am12_min=mL0 + mp,
        am123_min=mL0 + mp + mpi,
    )
    return [lambdas, line_alg]


@configurable
def make_BuToL0barPpPipPim_NoPID_TT(process):
    lambdas = make_LambdaTT()

    protons = make_tight_protons(mipchi2_min=4.0)
    pions = make_pions(pi_pidk_max=None, p_min=1500 * MeV, pt_min=500 * MeV)

    line_alg = make_b2x(
        particles=[lambdas, protons, pions, pions],
        descriptor="[B+ -> Lambda~0 p+ pi- pi+]cc",
        **Bu_kwargs_TT,
        am12_min=mL0 + mp,
        am123_min=mL0 + mp + mpi,
    )
    return [lambdas, line_alg]


@configurable
def make_BuToL0PmPipPip_NoPID_LL(process):
    lambdas = make_veryloose_lambda_LL(**LambdaLL_kwargs)
    protons = make_tight_protons(mipchi2_min=4.0)

    pions = make_pions(pi_pidk_max=None, p_min=1500 * MeV, pt_min=500 * MeV)
    line_alg = make_b2x(
        particles=[lambdas, protons, pions, pions],
        descriptor="[B+ -> Lambda0 p~- pi+ pi+]cc",
        **Bu_kwargs,
        am12_min=mL0 + mp,
        am123_min=mL0 + mp + mpi,
    )
    return [lambdas, line_alg]


@configurable
def make_BuToL0PmPipPip_NoPID_DD(process):
    lambdas = make_loose_lambda_DD()
    protons = make_tight_protons(mipchi2_min=4.0)

    pions = make_pions(pi_pidk_max=None, p_min=1500 * MeV, pt_min=500 * MeV)
    line_alg = make_b2x(
        particles=[lambdas, protons, pions, pions],
        descriptor="[B+ -> Lambda0 p~- pi+ pi+]cc",
        **Bu_kwargs,
        am12_min=mL0 + mp,
        am123_min=mL0 + mp + mpi,
    )
    return [lambdas, line_alg]


@configurable
def make_BuToL0PmPipPip_NoPID_TT(process):
    lambdas = make_LambdaTT()
    protons = make_tight_protons(mipchi2_min=4.0)

    pions = make_pions(pi_pidk_max=None, p_min=1500 * MeV, pt_min=500 * MeV)
    line_alg = make_b2x(
        particles=[lambdas, protons, pions, pions],
        descriptor="[B+ -> Lambda0 p~- pi+ pi+]cc",
        **Bu_kwargs_TT,
        am12_min=mL0 + mp,
        am123_min=mL0 + mp + mpi,
    )
    return [lambdas, line_alg]


# ###############
# ## L0L0barhh ##
# ###############


@check_process
@configurable
def make_BdsToL0L0barPpPm_LLLL(process):
    lambdas = make_veryloose_lambda_LL(**LambdaLL_kwargs)
    protons = make_tight_protons(mipchi2_min=4.0)

    line_alg = make_b2x(
        particles=[lambdas, lambdas, protons, protons],
        descriptor="B0 -> Lambda0 Lambda~0 p~- p+",
        **Bds_kwargs,
        am12_min=2 * mL0,
        am123_min=2 * mL0 + mp,
    )
    return [lambdas, line_alg]


@check_process
@configurable
def make_BdsToL0L0barPpPm_DDDD(process):
    lambdas = make_loose_lambda_DD()
    protons = make_tight_protons(mipchi2_min=4.0)

    line_alg = make_b2x(
        particles=[lambdas, lambdas, protons, protons],
        descriptor="B0 -> Lambda0 Lambda~0 p~- p+",
        **Bds_kwargs,
        am12_min=2 * mL0,
        am123_min=2 * mL0 + mp,
    )
    return [lambdas, line_alg]


@check_process
@configurable
def make_BdsToL0L0barPpPm_TTTT(process):
    lambdas = make_LambdaTT()
    protons = make_tight_protons(mipchi2_min=4.0)

    line_alg = make_b2x(
        particles=[lambdas, lambdas, protons, protons],
        descriptor="B0 -> Lambda0 Lambda~0 p~- p+",
        **Bds_kwargs_TT,
        am12_min=2 * mL0,
        am123_min=2 * mL0 + mp,
    )
    return [lambdas, line_alg]


@check_process
@configurable
def make_BdsToL0L0barPpPm_LLDD(process):
    lambdasll = make_veryloose_lambda_LL(**LambdaLL_kwargs)
    lambdasdd = make_loose_lambda_DD()
    protons = make_tight_protons(mipchi2_min=4.0)

    line_alg = make_b2x(
        particles=[lambdasll, lambdasdd, protons, protons],
        descriptor="[B0 -> Lambda0 Lambda~0 p~- p+]cc",
        **Bds_kwargs,
        am12_min=2 * mL0,
        am123_min=2 * mL0 + mp,
    )
    return [lambdasll, lambdasdd, line_alg]


@check_process
@configurable
def make_BdsToL0L0barPpPm_LLTT(process):
    lambdasll = make_veryloose_lambda_LL(**LambdaLL_kwargs)
    lambdastt = make_LambdaTT()

    protons = make_tight_protons(mipchi2_min=4.0)
    line_alg = make_b2x(
        particles=[lambdasll, lambdastt, protons, protons],
        descriptor="[B0 -> Lambda0 Lambda~0 p~- p+]cc",
        **Bds_kwargs_TT,
        am12_min=2 * mL0,
        am123_min=2 * mL0 + mp,
    )
    return [lambdasll, lambdastt, line_alg]


@check_process
@configurable
def make_BdsToL0L0barPpPm_DDTT(process):
    lambdasdd = make_loose_lambda_DD()
    lambdastt = make_LambdaTT()
    protons = make_tight_protons(mipchi2_min=4.0)

    line_alg = make_b2x(
        particles=[lambdasdd, lambdastt, protons, protons],
        descriptor="[B0 -> Lambda0 Lambda~0 p~- p+]cc",
        **Bds_kwargs_TT,
        am12_min=2 * mL0,
        am123_min=2 * mL0 + mp,
    )
    return [lambdasdd, lambdastt, line_alg]


@check_process
@configurable
def make_BdsToL0L0barPipPim_NoPID_LLLL(process):
    lambdas = make_veryloose_lambda_LL(**LambdaLL_kwargs)
    pions = make_pions(pi_pidk_max=None, p_min=1500 * MeV, pt_min=300 * MeV)

    line_alg = make_b2x(
        particles=[lambdas, lambdas, pions, pions],
        descriptor="B0 -> Lambda0 Lambda~0 pi+ pi-",
        **Bds_kwargs,
        am12_min=2 * mL0,
        am123_min=2 * mL0 + mpi,
    )
    return [lambdas, line_alg]


@check_process
@configurable
def make_BdsToL0L0barPipPim_NoPID_DDDD(process):
    lambdas = make_loose_lambda_DD()
    pions = make_pions(pi_pidk_max=None, p_min=1500 * MeV, pt_min=300 * MeV)

    line_alg = make_b2x(
        particles=[lambdas, lambdas, pions, pions],
        descriptor="B0 -> Lambda0 Lambda~0 pi+ pi-",
        **Bds_kwargs,
        am12_min=2 * mL0,
        am123_min=2 * mL0 + mpi,
    )
    return [lambdas, line_alg]


@check_process
@configurable
def make_BdsToL0L0barPipPim_NoPID_LLDD(process):
    lambdasll = make_veryloose_lambda_LL(**LambdaLL_kwargs)
    lambdasdd = make_loose_lambda_DD()
    pions = make_pions(pi_pidk_max=None, p_min=1500 * MeV, pt_min=300 * MeV)

    line_alg = make_b2x(
        particles=[lambdasll, lambdasdd, pions, pions],
        descriptor="[B0 -> Lambda0 Lambda~0 pi+ pi-]cc",
        **Bds_kwargs,
        am12_min=2 * mL0,
        am123_min=2 * mL0 + mpi,
    )
    return [lambdasll, lambdasdd, line_alg]


@check_process
@configurable
def make_BdsToL0L0barPipPim_NoPID_TTTT(process):
    lambdas = make_LambdaTT()
    pions = make_pions(pi_pidk_max=None, p_min=1500 * MeV, pt_min=300 * MeV)

    line_alg = make_b2x(
        particles=[lambdas, lambdas, pions, pions],
        descriptor="B0 -> Lambda0 Lambda~0 pi+ pi-",
        **Bds_kwargs_TT,
        am12_min=2 * mL0,
        am123_min=2 * mL0 + mpi,
    )
    return [lambdas, line_alg]


@check_process
@configurable
def make_BdsToL0L0barPipPim_NoPID_LLTT(process):
    lambdasll = make_veryloose_lambda_LL(**LambdaLL_kwargs)
    lambdastt = make_LambdaTT()

    pions = make_pions(pi_pidk_max=None, p_min=1500 * MeV, pt_min=300 * MeV)
    line_alg = make_b2x(
        particles=[lambdasll, lambdastt, pions, pions],
        descriptor="[B0 -> Lambda0 Lambda~0 pi+ pi-]cc",
        **Bds_kwargs_TT,
        am12_min=2 * mL0,
        am123_min=2 * mL0 + mpi,
    )
    return [lambdasll, lambdastt, line_alg]


@check_process
@configurable
def make_BdsToL0L0barPipPim_NoPID_DDTT(process):
    lambdasdd = make_loose_lambda_DD()
    lambdastt = make_LambdaTT()

    pions = make_pions(pi_pidk_max=None, p_min=1500 * MeV, pt_min=300 * MeV)

    line_alg = make_b2x(
        particles=[lambdasdd, lambdastt, pions, pions],
        descriptor="[B0 -> Lambda0 Lambda~0 pi+ pi-]cc",
        **Bds_kwargs_TT,
        am12_min=2 * mL0,
        am123_min=2 * mL0 + mpi,
    )
    return [lambdasdd, lambdastt, line_alg]
