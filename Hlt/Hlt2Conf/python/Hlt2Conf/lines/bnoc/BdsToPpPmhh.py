###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of BNOC BdsToPpPmhh lines
"""

from GaudiKernel.SystemOfUnits import MeV

from Hlt2Conf.lines.bnoc.builders.b_builder import make_B2PpPmhh
from Hlt2Conf.lines.bnoc.builders.basic_builder import (
    make_KaonsforB2ppbarhh,
    make_PionsforB2ppbarhh,
    make_ProtonsforB2ppbarhh,
)
from Hlt2Conf.lines.bnoc.utils import check_process

m_K = 480.0 * MeV
m_pi = 120.0 * MeV
m_p = 910.0 * MeV


@check_process
def make_BdsToPpPmPipPim(process):
    pions = make_PionsforB2ppbarhh()
    protons = make_ProtonsforB2ppbarhh()
    Bd0 = make_B2PpPmhh(
        particles=[protons, protons, pions, pions],
        am_min_ppbar=m_p + m_p,
        am_min_ppbarh=m_p + m_p + m_pi,
        am_max_ppbar=5300.0 * MeV,
        am_max_ppbarh=5400.0 * MeV,
        descriptor="B0 -> p+ p~- pi+ pi-",
    )
    return Bd0


@check_process
def make_BdsToPpPmKpKm(process):
    kaons = make_KaonsforB2ppbarhh()
    protons = make_ProtonsforB2ppbarhh()
    Bd0 = make_B2PpPmhh(
        particles=[protons, protons, kaons, kaons],
        am_min_ppbar=m_p + m_p,
        am_min_ppbarh=m_p + m_p + m_K,
        am_max_ppbar=4700.0 * MeV,
        am_max_ppbarh=5000.0 * MeV,
        descriptor="B0 -> p+ p~- K+ K-",
    )
    return Bd0


@check_process
def make_BdsToPpPmKpPim(process):
    kaons = make_KaonsforB2ppbarhh()
    pions = make_PionsforB2ppbarhh()
    protons = make_ProtonsforB2ppbarhh()
    Bd0 = make_B2PpPmhh(
        particles=[protons, protons, kaons, pions],
        am_min_ppbar=m_p + m_p,
        am_min_ppbarh=m_p + m_p + m_pi,
        am_max_ppbar=5000.0 * MeV,
        am_max_ppbarh=5200 * MeV,
        descriptor="[B0 -> p+ p~- K+ pi-]cc",
    )
    return Bd0
