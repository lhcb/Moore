###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Following lines are defined:

  1. prompt lines:
    [Xi_c0 -> p+ K-]CC
    [Omega_c0 -> p+ K-]CC

  2. b-decays:
    [Xi_b- -> (Xi_c0 -> p+ K-) pi-]CC
    [Omega_b- -> (Omega_c0 -> p+ K-) pi-]CC

Proponents: Miroslav Saur, Xiao-Rui Lyu, Ziyi Wang
TODO:
    - check all lines based on real data
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, picosecond
from GaudiKernel.SystemOfUnits import micrometer as um
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
    make_has_rich_long_protons,
)

from .prefilters import charm_prefilters

all_lines = {}

###############################################################################
# Track filters
###############################################################################


def filter_long_pions(particles, pvs, pt_min=500 * MeV, p_min=3 * GeV, mipchi2_min=5.0):
    cut = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
    )
    return ParticleFilter(particles, F.FILTER(cut))


def filter_long_kaons(
    particles, pvs, pt_min=300 * MeV, p_min=3 * GeV, mipchi2_min=15.0, pidk_min=2.0
):
    cut = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
        F.PID_K > pidk_min,
    )
    return ParticleFilter(particles, F.FILTER(cut))


def filter_long_protons(
    particles,
    pvs,
    pt_min=400 * MeV,
    p_min=9 * GeV,
    mipchi2_min=12.0,
    pidp_min=2,
    dllp_min=5,
):
    cut = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
        F.PID_P > pidp_min,
        (F.PID_P - F.PID_K > dllp_min),
    )
    return ParticleFilter(particles, F.FILTER(cut))


###############################################################################
# Particle combiners
###############################################################################


def combine_xic_pk(
    protons,
    particle,
    pvs,
    decay_descriptor,
    name="Charm xic0_to_pk_combine_xic_{hash}",
    comb_m_min=2400 * MeV,
    comb_m_max=2540 * MeV,
    m_min=2410 * MeV,
    m_max=2530 * MeV,
    pt_min=2 * GeV,
    sum_pt_min=1.5 * GeV,
    comb_p_min=19 * GeV,
    p_min=20 * GeV,
    doca_max=100 * um,
    vchi2pdof_max=6.0,
    bpvvdz_min=0.5 * mm,
    bpvfdchi2_min=10.0,
    bpvdira_min=0.995,
    bpvipchi2_max=7,
):
    """Combine protons and hh to form Xic0.
    Cuts generally based on Run2 Turbo lines.
    """
    comb_cut = F.require_all(
        F.MAXSDOCACUT(doca_max),
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.SUM(F.PT) > sum_pt_min,
        F.P > comb_p_min,
    )
    vertex_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.P > p_min,
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVVDZ > bpvvdz_min,
        F.OWNPVFDCHI2 > bpvfdchi2_min,  # TODO to be checked
    )

    if bpvdira_min is not None:
        vertex_cut = F.require_all(vertex_cut, F.OWNPVDIRA > bpvdira_min)
    if bpvipchi2_max is not None:
        vertex_cut = F.require_all(vertex_cut, F.OWNPVIPCHI2 < bpvipchi2_max)
    return ParticleCombiner(
        [protons, particle],
        DecayDescriptor=decay_descriptor,
        name=name,
        CombinationCut=comb_cut,
        CompositeCut=vertex_cut,
    )


def combine_xib(
    xicp,
    particle,
    pvs,
    decay_descriptor,
    name="Charm xic0_to_pk_combine_xib_{hash}",
    comb_m_min=5580 * MeV,
    comb_m_max=6920 * MeV,
    m_min=5600 * MeV,
    m_max=6100 * MeV,
    pt_min=3 * GeV,
    sum_pt_min=2 * GeV,
    comb_p_min=15 * GeV,
    p_min=16 * GeV,
    doca_max=100 * um,
    vchi2pdof_max=5.0,
    bpvltime_min=0.2 * picosecond,
    bpvvdz_min=0.5 * mm,
    bpvfdchi2_min=30.0,
    bpvdira_min=0.99,
    bpvipchi2_max=9,
):
    """Combiner for Xic0/Oc0 and pion/kaon to create Xib/Omegab."""
    comb_cut = F.require_all(
        F.MAXSDOCACUT(doca_max),
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.SUM(F.PT) > sum_pt_min,
        F.P > comb_p_min,
    )
    vertex_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.P > p_min,
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVLTIME > bpvltime_min,
        F.OWNPVVDZ > bpvvdz_min,
        F.OWNPVFDCHI2 > bpvfdchi2_min,  # TODO to be checked
        F.OWNPVDIRA > bpvdira_min,
        F.OWNPVIPCHI2 < bpvipchi2_max,
    )
    return ParticleCombiner(
        [xicp, particle],
        DecayDescriptor=decay_descriptor,
        name=name,
        CombinationCut=comb_cut,
        CompositeCut=vertex_cut,
    )


###############################################################################
# Hlt2 lines definition
###############################################################################


@register_line_builder(all_lines)
def xic0_to_pk_line(name="Hlt2Charm_Xic0ToPpKm", prescale=1):
    pvs = make_pvs()
    long_protons = filter_long_protons(make_has_rich_long_protons(), pvs)
    long_kaons = filter_long_kaons(make_has_rich_long_kaons(), pvs)
    xic0_pk = combine_xic_pk(long_protons, long_kaons, pvs, "[Xi_c0 -> p+ K-]cc")
    return Hlt2Line(name=name, algs=charm_prefilters() + [xic0_pk], prescale=prescale)


@register_line_builder(all_lines)
def omc0_to_pk_line(name="Hlt2Charm_Oc0ToPpKm", prescale=1):
    pvs = make_pvs()
    long_protons = filter_long_protons(make_has_rich_long_protons(), pvs)
    long_kaons = filter_long_kaons(make_has_rich_long_kaons(), pvs)
    omegac0_pk = combine_xic_pk(
        long_protons,
        long_kaons,
        pvs,
        "[Omega_c0 -> p+ K-]cc",
        name="combine_omegac_pk",
        comb_m_min=2625 * MeV,
        comb_m_max=2765 * MeV,
        m_min=2630 * MeV,
        m_max=2760 * MeV,
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [omegac0_pk], prescale=prescale
    )


@register_line_builder(all_lines)
def xibm_to_xic0pi_line(name="Hlt2Charm_XibmToXic0Pim_Xic0ToPpKm", prescale=1):
    pvs = make_pvs()
    long_pions_from_b = filter_long_pions(make_has_rich_long_pions(), pvs)
    long_protons = filter_long_protons(make_has_rich_long_protons(), pvs)
    long_kaons = filter_long_kaons(make_has_rich_long_kaons(), pvs)
    xic0_pk = combine_xic_pk(
        long_protons,
        long_kaons,
        pvs,
        "[Xi_c0 -> p+ K-]cc",
        bpvipchi2_max=None,
        bpvdira_min=None,
    )
    xibm = combine_xib(xic0_pk, long_pions_from_b, pvs, "[Xi_b0 -> Xi_c+ pi-]cc")
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [xic0_pk, xibm], prescale=prescale
    )


@register_line_builder(all_lines)
def omegabm_to_omegac0pim_line(name="Hlt2Charm_ObmToOc0Pim_Oc0ToPpKm", prescale=1):
    pvs = make_pvs()
    long_pions_from_b = filter_long_pions(make_has_rich_long_pions(), pvs)
    long_protons = filter_long_protons(make_has_rich_long_protons(), pvs)
    long_kaons = filter_long_kaons(make_has_rich_long_kaons(), pvs)
    omegac0_pk = combine_xic_pk(
        long_protons,
        long_kaons,
        pvs,
        "[Omega_c0 -> p+ K-]cc",
        comb_m_min=2625 * MeV,
        comb_m_max=2765 * MeV,
        m_min=2630 * MeV,
        m_max=2760 * MeV,
        bpvipchi2_max=None,
        bpvdira_min=None,
    )
    omegab = combine_xib(
        omegac0_pk,
        long_pions_from_b,
        pvs,
        "[Omega_b- -> Omega_c0 pi-]cc",
        comb_m_min=5830 * MeV,
        comb_m_max=6370 * MeV,
        m_min=5850 * MeV,
        m_max=6350 * MeV,
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [omegac0_pk, omegab], prescale=prescale
    )
