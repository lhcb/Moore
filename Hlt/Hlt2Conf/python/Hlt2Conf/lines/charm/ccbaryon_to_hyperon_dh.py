###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""HLT2 lines for charmed baryon searches:

 1. Omegacc+ -> Xi- D+  pi+
 2. Xicc+    -> Xi- Ds+ pi+
 3. Xicc+    -> Xi- D+  K+

where the daughter baryon is reconstructed via:

  Xi- -> Lambda0(LL/DD) pi-
  D+  -> K- pi+ pi+
  Ds+ -> K- K+  pi+

TODO: add requirement on tracks chi2/ndof, HLT1 filtering
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm  # TODO , picosecond as ps
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    make_down_pions,
    make_down_protons,
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
    make_long_pions,
    make_long_protons,
)

from .ccbaryon_hadronic import create_hc_mon
from .particle_properties import _DP_M, _DS_M, _XIM_M
from .prefilters import charm_prefilters

all_lines = {}

###############################################################################
# Track filters
###############################################################################


def filter_long_pions(mipchi2_min=None):
    cut = F.require_all(
        F.PT > 100 * MeV,
        F.P > 1 * GeV,
        F.PID_K < 5.0,
    )
    if isinstance(mipchi2_min, (float, int)):
        cut &= F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=make_pvs())
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(cut),
    )


def filter_long_kaons(mipchi2_min=None):
    cut = F.require_all(
        F.PT > 100 * MeV,
        F.P > 1 * GeV,
        F.PID_K > 5.0,
    )
    if isinstance(mipchi2_min, (float, int)):
        cut &= F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=make_pvs())
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(cut),
    )


def filter_long_pions_from_lambda():
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 100 * MeV,
                F.P > 1 * GeV,
                F.MINIPCHI2CUT(IPChi2Cut=36.0, Vertices=make_pvs()),
                F.PID_K < 5.0,
            ),
        ),
    )


def filter_long_protons_from_lambda():
    return ParticleFilter(
        make_long_protons(),
        F.FILTER(
            F.require_all(
                F.PT > 100 * MeV,
                F.P > 1 * GeV,
                F.MINIPCHI2CUT(IPChi2Cut=6.0, Vertices=make_pvs()),
                F.PID_P > 5.0,
                F.PID_P - F.PID_K > 0.0,
            ),
        ),
    )


def filter_down_pions_from_lambda():
    return ParticleFilter(
        make_down_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 100 * MeV,
                F.P > 1 * GeV,
            ),
        ),
    )


def filter_down_protons_from_lambda():
    return ParticleFilter(
        make_down_protons(),
        F.FILTER(
            F.require_all(
                F.PT > 300 * MeV,
                F.P > 5 * GeV,
            ),
        ),
    )


###############################################################################
# Basic combiners
###############################################################################


def make_lambdall():
    """Make Lambda -> p+ pi- from long tracks."""
    protons = filter_long_protons_from_lambda()
    pions = filter_long_pions_from_lambda()
    combinationcode = F.require_all(
        in_range(1085 * MeV, F.MASS, 1145 * MeV),
        F.MAXDOCACHI2CUT(30.0),
        F.MAXDOCACUT(0.2 * mm),
    )
    vertexcode = F.require_all(
        in_range(1095 * MeV, F.MASS, 1135 * MeV),
        F.CHI2DOF < 9.0,
        F.OWNPVVDZ > 4 * mm,
        in_range(-100 * mm, F.END_VZ, 500 * mm),
    )
    return ParticleCombiner(
        Inputs=[protons, pions],
        name="Charm_CCBaryonToHyperonDH_make_lambdall_{hash}",
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        CombinationCut=combinationcode,
        CompositeCut=vertexcode,
    )


def make_lambdadd():
    """Make Lambda -> p+ pi- from downstream tracks."""
    pions = filter_down_pions_from_lambda()
    protons = filter_down_protons_from_lambda()
    combinationcode = F.require_all(
        in_range(1075 * MeV, F.MASS, 1160 * MeV),
        F.MAXDOCACHI2CUT(30.0),
        F.MAXDOCACUT(2 * mm),
    )
    vertexcode = F.require_all(
        in_range(1095 * MeV, F.MASS, 1140 * MeV),
        F.CHI2DOF < 9.0,
        in_range(300 * mm, F.END_VZ, 2275 * mm),
    )
    return ParticleCombiner(
        Inputs=[protons, pions],
        name="Charm_CCBaryonToHyperonDH_make_lambdadd_{hash}",
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        CombinationCut=combinationcode,
        CompositeCut=vertexcode,
    )


def make_xim_to_lambdapi(lambdas, pions):
    """Return a Xi- -> Lambda0 pi- decay maker."""
    combinationcode = F.require_all(
        in_range(1248 * MeV, F.MASS, 1396 * MeV),
        F.MAXDOCACUT(2 * mm),
        F.SUM(F.PT) > 500 * MeV,
    )
    vertexcode = F.require_all(
        in_range(1258 * MeV, F.MASS, 1386 * MeV),
        F.PT > 300 * MeV,
        F.CHI2DOF < 10.0,
        F.OWNPVFDCHI2 > 10.0,
    )
    return ParticleCombiner(
        Inputs=[lambdas, pions],
        name="Charm_CCBaryonToHyperonDH_make_xim_to_lambdapi_{hash}",
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        CombinationCut=combinationcode,
        CompositeCut=vertexcode,
    )


def make_d_to_3h(
    meson1,
    meson2,
    meson3,
    name,
    decay_descriptor,
    comb_pt_min,
    # bpvltime_min,  # TODO
    vchi2dof_max,
    m_min,
    m_max,
):
    """Return a decay maker for either of the decays:
    1. D-  -> K- pi+ pi+
    2. Ds- -> K- K+  pi+
    """
    combination12_code = F.MAXSDOCACUT(0.2 * mm)
    combination_code = F.require_all(
        F.PT > comb_pt_min,
        F.MAXSDOCACUT(0.2 * mm),
        in_range(m_min - 20 * MeV, F.MASS, m_max + 20 * MeV),
    )
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2dof_max,
        F.OWNPVFDCHI2 > 15.0,
        # F.OWNPVLTIME > bpvltime_min,  # TODO
        F.OWNPVVDZ > 0.5 * mm,
        in_range(m_min, F.MASS, m_max),
    )
    return ParticleCombiner(
        Inputs=[meson1, meson2, meson3],
        name=name,
        DecayDescriptor=decay_descriptor,
        Combination12Cut=combination12_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def make_cc_baryon(
    particle1,
    particle2,
    particle3,
    name,
    decay_descriptor,
    m_min,
    m_max,
    vchi2dof_max,
    bpvipchi2_max,
    bpvdira_min,
):
    combination12_code = F.MAXSDOCACUT(2 * mm)
    combination_code = F.require_all(
        F.SDOCA(1, 3) < 2 * mm,
        F.SDOCA(2, 3) < 0.2 * mm,
        in_range(m_min - 30 * MeV, F.MASS, m_max + 30 * MeV),
    )
    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < vchi2dof_max,
        F.OWNPVIPCHI2 < bpvipchi2_max,
        F.OWNPVDIRA > bpvdira_min,
    )
    return ParticleCombiner(
        Inputs=[particle1, particle2, particle3],
        name=name,
        DecayDescriptor=decay_descriptor,
        Combination12Cut=combination12_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


###############################################################################
# Lines definitions
###############################################################################


@register_line_builder(all_lines)
def omegaccp_to_ximdppip_lll_line(name="Hlt2Charm_OccpToDpXimPip_LLL", prescale=1):
    dp_kaons = filter_long_kaons(mipchi2_min=4.0)
    dp_pions = filter_long_pions(mipchi2_min=4.0)
    dp = make_d_to_3h(
        meson1=dp_kaons,
        meson2=dp_pions,
        meson3=dp_pions,
        name="Charm_CCBaryonToHyperonDH_make_d_to_3h_dp2kpipi_{hash}",
        decay_descriptor="[D+ -> K- pi+ pi+]cc",
        comb_pt_min=150 * MeV,
        # bpvltime_min=0.3 * ps,  # TODO
        vchi2dof_max=10.0,
        m_min=1790 * MeV,
        m_max=1940 * MeV,
    )
    dp_mon = create_hc_mon(dp, name, _DP_M)
    xim_lambdas = make_lambdall()
    xim_pions = filter_long_pions(mipchi2_min=16.0)
    xim = make_xim_to_lambdapi(lambdas=xim_lambdas, pions=xim_pions)
    xim_mon = create_hc_mon(xim, name, _XIM_M, name="hyp")
    pions = filter_long_pions(mipchi2_min=1.0)
    line_alg = make_cc_baryon(
        particle1=xim,
        particle2=dp,
        particle3=pions,
        name="Charm_CCBaryonToHyperonDH_make_cc_baryon_OmccToDpXimPip_LLL_{hash}",
        decay_descriptor="[Omega_cc+ -> Xi- D+ pi+]cc",
        m_min=3300 * MeV,
        m_max=4100 * MeV,
        vchi2dof_max=25.0,
        bpvipchi2_max=25.0,
        bpvdira_min=0.0,
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xim, line_alg, dp_mon, xim_mon],
        prescale=prescale,
        monitoring_variables=(),
    )


@register_line_builder(all_lines)
def omegaccp_to_ximdppip_ddl_line(name="Hlt2Charm_OccpToDpXimPip_DDL", prescale=1):
    dp_kaons = filter_long_kaons(mipchi2_min=4.0)
    dp_pions = filter_long_pions(mipchi2_min=4.0)
    dp = make_d_to_3h(
        meson1=dp_kaons,
        meson2=dp_pions,
        meson3=dp_pions,
        name="Charm_CCBaryonToHyperonDH_make_d_to_3h_dp2kpipi_{hash}",
        decay_descriptor="[D+ -> K- pi+ pi+]cc",
        comb_pt_min=150 * MeV,
        # bpvltime_min=0.3 * ps,  # TODO
        vchi2dof_max=10.0,
        m_min=1790 * MeV,
        m_max=1940 * MeV,
    )
    dp_mon = create_hc_mon(dp, name, _DP_M)
    xim_lambdas = make_lambdadd()
    xim_pions = filter_long_pions(mipchi2_min=16.0)
    xim = make_xim_to_lambdapi(lambdas=xim_lambdas, pions=xim_pions)
    xim_mon = create_hc_mon(xim, name, _XIM_M, name="hyp")
    pions = filter_long_pions(mipchi2_min=1.0)
    line_alg = make_cc_baryon(
        particle1=xim,
        particle2=dp,
        particle3=pions,
        name="Charm_CCBaryonToHyperonDH_make_cc_baryon_OmccToDpXimPip_DDL_{hash}",
        decay_descriptor="[Omega_cc+ -> Xi- D+ pi+]cc",
        m_min=3300 * MeV,
        m_max=4100 * MeV,
        vchi2dof_max=25.0,
        bpvipchi2_max=25.0,
        bpvdira_min=0.0,
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xim, line_alg, dp_mon, xim_mon],
        prescale=prescale,
        monitoring_variables=(),
    )


@register_line_builder(all_lines)
def xiccp_to_ximdsppip_lll_line(name="Hlt2Charm_XiccpToDspXimPip_LLL", prescale=1):
    ds_kaons = filter_long_kaons(mipchi2_min=4.0)
    ds_pions = filter_long_pions(mipchi2_min=4.0)
    dsp = make_d_to_3h(
        meson1=ds_kaons,
        meson2=ds_kaons,
        meson3=ds_pions,
        name="Charm_CCBaryonToHyperonDH_make_d_to_3h_dsp2kkpi_{hash}",
        decay_descriptor="[D_s+ -> K- K+ pi+]cc",
        comb_pt_min=150 * MeV,
        # bpvltime_min=0.15 * ps,  # TODO
        vchi2dof_max=10.0,
        m_min=1890 * MeV,
        m_max=2040 * MeV,
    )
    dsp_mon = create_hc_mon(dsp, name, _DS_M)
    xim_lambdas = make_lambdall()
    xim_pions = filter_long_pions(mipchi2_min=16.0)
    xim = make_xim_to_lambdapi(lambdas=xim_lambdas, pions=xim_pions)
    xim_mon = create_hc_mon(xim, name, _XIM_M, name="hyp")
    pions = filter_long_pions(mipchi2_min=1.0)
    line_alg = make_cc_baryon(
        particle1=xim,
        particle2=dsp,
        particle3=pions,
        name="Charm_CCBaryonToHyperonDH_make_cc_baryon_XiccToDspXimPip_LLL_{hash}",
        decay_descriptor="[Xi_cc+ -> Xi- D_s+ pi+]cc",
        m_min=3300 * MeV,
        m_max=4100 * MeV,
        vchi2dof_max=25.0,
        bpvipchi2_max=25.0,
        bpvdira_min=0.0,
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xim, line_alg, dsp_mon, xim_mon],
        prescale=prescale,
        monitoring_variables=(),
    )


@register_line_builder(all_lines)
def xiccp_to_ximdsppip_ddl_line(name="Hlt2Charm_XiccpToDspXimPip_DDL", prescale=1):
    ds_kaons = filter_long_kaons(mipchi2_min=4.0)
    ds_pions = filter_long_pions(mipchi2_min=4.0)
    dsp = make_d_to_3h(
        meson1=ds_kaons,
        meson2=ds_kaons,
        meson3=ds_pions,
        name="Charm_CCBaryonToHyperonDH_make_d_to_3h_dsp2kkpi_{hash}",
        decay_descriptor="[D_s+ -> K- K+ pi+]cc",
        comb_pt_min=150 * MeV,
        # bpvltime_min=0.15 * ps,  # TODO
        vchi2dof_max=10.0,
        m_min=1890 * MeV,
        m_max=2040 * MeV,
    )
    dsp_mon = create_hc_mon(dsp, name, _DS_M)
    xim_lambdas = make_lambdadd()
    xim_pions = filter_long_pions(mipchi2_min=16.0)
    xim = make_xim_to_lambdapi(lambdas=xim_lambdas, pions=xim_pions)
    xim_mon = create_hc_mon(xim, name, _XIM_M, name="hyp")
    pions = filter_long_pions(mipchi2_min=1.0)
    line_alg = make_cc_baryon(
        particle1=xim,
        particle2=dsp,
        particle3=pions,
        name="Charm_CCBaryonToHyperonDH_make_cc_baryon_XiccToDspXimPip_DDL_{hash}",
        decay_descriptor="[Xi_cc+ -> Xi- D_s+ pi+]cc",
        m_min=3300 * MeV,
        m_max=4100 * MeV,
        vchi2dof_max=25.0,
        bpvipchi2_max=25.0,
        bpvdira_min=0.0,
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xim, line_alg, dsp_mon, xim_mon],
        prescale=prescale,
        monitoring_variables=(),
    )


@register_line_builder(all_lines)
def xiccp_to_ximdpkp_lll_line(name="Hlt2Charm_XiccpToDpXimKp_LLL", prescale=1):
    dp_kaons = filter_long_kaons(mipchi2_min=4.0)
    dp_pions = filter_long_pions(mipchi2_min=4.0)
    dp = make_d_to_3h(
        meson1=dp_kaons,
        meson2=dp_pions,
        meson3=dp_pions,
        name="Charm_CCBaryonToHyperonDH_make_d_to_3h_dp2kpipi_{hash}",
        decay_descriptor="[D+ -> K- pi+ pi+]cc",
        comb_pt_min=150 * MeV,
        # bpvltime_min=0.3 * ps,  # TODO
        vchi2dof_max=10.0,
        m_min=1790 * MeV,
        m_max=1940 * MeV,
    )
    dp_mon = create_hc_mon(dp, name, _DP_M)
    xim_lambdas = make_lambdall()
    xim_pions = filter_long_pions(mipchi2_min=16.0)
    xim = make_xim_to_lambdapi(lambdas=xim_lambdas, pions=xim_pions)
    xim_mon = create_hc_mon(xim, name, _XIM_M, name="hyp")
    kaons = filter_long_kaons(mipchi2_min=1.0)
    line_alg = make_cc_baryon(
        particle1=xim,
        particle2=dp,
        particle3=kaons,
        name="Charm_CCBaryonToHyperonDH_make_cc_baryon_XiccToDpXimKp_LLL_{hash}",
        decay_descriptor="[Xi_cc+ -> Xi- D+ K+]cc",
        m_min=3300 * MeV,
        m_max=4100 * MeV,
        vchi2dof_max=25.0,
        bpvipchi2_max=25.0,
        bpvdira_min=0.0,
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xim, line_alg, dp_mon, xim_mon],
        prescale=prescale,
        monitoring_variables=(),
    )


@register_line_builder(all_lines)
def xiccp_to_ximdpkp_ddl_line(name="Hlt2Charm_XiccpToDpXimKp_DDL", prescale=1):
    dp_kaons = filter_long_kaons(mipchi2_min=4.0)
    dp_pions = filter_long_pions(mipchi2_min=4.0)
    dp = make_d_to_3h(
        meson1=dp_kaons,
        meson2=dp_pions,
        meson3=dp_pions,
        name="Charm_CCBaryonToHyperonDH_make_d_to_3h_dp2kpipi_{hash}",
        decay_descriptor="[D+ -> K- pi+ pi+]cc",
        comb_pt_min=150 * MeV,
        # bpvltime_min=0.3 * ps,  # TODO
        vchi2dof_max=10.0,
        m_min=1790 * MeV,
        m_max=1940 * MeV,
    )
    dp_mon = create_hc_mon(dp, name, _DP_M)
    xim_lambdas = make_lambdadd()
    xim_pions = filter_long_pions(mipchi2_min=16.0)
    xim = make_xim_to_lambdapi(lambdas=xim_lambdas, pions=xim_pions)
    xim_mon = create_hc_mon(xim, name, _XIM_M, name="hyp")
    kaons = filter_long_kaons(mipchi2_min=1.0)
    line_alg = make_cc_baryon(
        particle1=xim,
        particle2=dp,
        particle3=kaons,
        name="Charm_CCBaryonToHyperonDH_make_cc_baryon_XiccToDpXimKp_DDL_{hash}",
        decay_descriptor="[Xi_cc+ -> Xi- D+ K+]cc",
        m_min=3300 * MeV,
        m_max=4100 * MeV,
        vchi2dof_max=25.0,
        bpvipchi2_max=25.0,
        bpvdira_min=0.0,
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xim, line_alg, dp_mon, xim_mon],
        prescale=prescale,
        monitoring_variables=(),
    )
