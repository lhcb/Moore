###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Following lines are defined:

  1. D+ -> KS0 (-> pi+ pi-) pi+
  2. D+ -> KS0 (-> pi+ pi-) K+
  3. D_s+ -> KS0 (-> pi+ pi-) pi+
  4. D_s+ -> KS0 (-> pi+ pi-) K+

All LL, LD and DD modes of KS are included as a separate lines.
The D+ and Ds+ decaying to the same final state are included in same line as
in Run 2.

Proponents: Miroslav Saur, Xiao-Rui Lyu, Ziyi Wang

TODO:
- check BPVIPCHI2 based on the data
- apply BPVLTIME requirements to KS and D(s)+ when functor will be fully working / not flooding logs with warnings
"""

import Functors as F
import Functors.math as fmath
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from GaudiKernel.SystemOfUnits import micrometer as um
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    make_has_rich_down_pions,
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
)

from .prefilters import charm_prefilters

# from . import charm_isolation as isolation

all_lines = {}

###################
## track filters ##
###################

## Large part of the code taken and modified from: https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Hlt2Conf/python/Hlt2Conf/lines/charm/hyperons.py


def filter_long_pions(
    pvs, pt_min=250 * MeV, p_min=2 * GeV, mipchi2_min=35.0, pion_pidk_max=5.0
):
    """Filter long pions with P PT, MINIPCHI2CUT and PIDk cuts."""
    cut = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
        F.PID_K < pion_pidk_max,
    )
    return ParticleFilter(make_has_rich_long_pions(), F.FILTER(cut))


def filter_long_kaons(
    pvs, pt_min=250 * MeV, p_min=3 * GeV, mipchi2_min=35.0, pidk_min=-5.0
):
    """Filter long kaons with P PT, MINIPCHI2CUT and PIDk cuts."""
    cut = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
        F.PID_K > pidk_min,
    )
    return ParticleFilter(make_has_rich_long_kaons(), F.FILTER(cut))


def filter_long_pions_from_ks(
    pvs, pt_min=250 * MeV, p_min=2 * GeV, mipchi2_min=35.0, pion_pidk_max=15.0
):
    """Filter long pions with P PT, MINIPCHI2CUT and PIDk cuts."""
    cut = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
        F.PID_K < pion_pidk_max,
    )
    return ParticleFilter(make_has_rich_long_pions(), F.FILTER(cut))


def filter_down_pions_from_ks(pvs, pt_min=250 * MeV, p_min=2 * GeV, pion_pidk_max=15.0):
    """Filter downstream pions with P PT, and PIDk cuts."""
    cut = F.require_all(F.PT > pt_min, F.P > p_min, F.PID_K < pion_pidk_max)
    return ParticleFilter(make_has_rich_down_pions(), F.FILTER(cut))


#######################
## strange combiners ##
#######################
def make_ks_ll(
    pions1,
    pions2,
    pvs,
    name="Charm_DToKsH_KsLL_{hash}",
    comb_m_min=445 * MeV,
    comb_m_max=550 * MeV,
    m_min=460 * MeV,
    m_max=535 * MeV,
    comb_pt_min=300 * MeV,
    pt_min=350 * MeV,
    comb_p_min=3.5 * GeV,
    p_min=4 * GeV,
    doca_max=1 * mm,
    vchi2pdof_max=7.0,
    bpvvdz_min=10 * mm,
    bpvfdchi2_min=3.0,
    mipchi2_min=2,
    # bpvltime_min=1. * picosecond
):
    """Make KS -> pi+ pi- from long tracks."""
    comb_cut = F.require_all(
        F.MAXDOCACUT(doca_max),
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.PT > comb_pt_min,
        F.P > comb_p_min,
    )
    vertex_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.P > p_min,
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVVDZ > bpvvdz_min,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
        F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
        # F.OWNPVLTIME > bpvltime_min  #<-- not converging at all
    )
    return ParticleCombiner(
        [pions1, pions2],
        name=name,
        DecayDescriptor="KS0 -> pi+ pi-",
        CombinationCut=comb_cut,
        CompositeCut=vertex_cut,
    )


def make_ks_dd(
    pions1,
    pions2,
    pvs,
    name="Charm_DToKsH_KsDD_{hash}",
    comb_m_min=417 * MeV,
    comb_m_max=577 * MeV,
    m_min=437 * MeV,
    m_max=557 * MeV,
    comb_pt_min=400 * MeV,
    pt_min=450 * MeV,
    sum_pt_min=500 * MeV,
    comb_p_min=4.5 * GeV,
    p_min=5 * GeV,
    doca_max=2 * mm,
    docachi2_max=12.0,
    vchi2pdof_max=10.0,
    # bpvltime_min=1. * picosecond
):
    """
    Make KS -> pi+ pi- from downstream tracks.
    """
    comb_cut = F.require_all(
        F.MAXDOCACUT(doca_max),
        F.MAXDOCACHI2CUT(docachi2_max),
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.PT > comb_pt_min,
        F.SUM(F.PT) > sum_pt_min,
        F.P > comb_p_min,
    )
    vertex_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.P > p_min,
        F.CHI2DOF < vchi2pdof_max,
        # F.OWNPVLTIME > bpvltime_min #--- not converging
    )
    return ParticleCombiner(
        [pions1, pions2],
        name=name,
        DecayDescriptor="KS0 -> pi+ pi-",
        CombinationCut=comb_cut,
        CompositeCut=vertex_cut,
    )


########################
## D mesons combiners ##
########################
def combine_d_ks_h(
    ks0,
    particle,
    pvs,
    decay_descriptor,
    name="Charm_DToKsH_DToKsHCombiner_{hash}",
    comb_m_min=1779 * MeV,
    comb_m_max=2059 * MeV,
    m_min=1789 * MeV,  # lower range for D+
    m_max=2049 * MeV,  # upper range for Ds+
    comb_pt_min=1.5 * GeV,
    pt_min=2.0 * GeV,
    sum_pt_min=1.8 * GeV,
    comb_p_min=15 * GeV,
    p_min=16 * GeV,
    doca_max=100 * um,
    vchi2pdof_max=5.0,
    # bpvltime_min=0.25 * picosecond,
    bpvvdz_min=0.5 * mm,
    bpvfdchi2_min=25.0,
    bpfdrho_max=5 * mm,
    # bpvipchi2_max=999.,
    bpvdira_min=0.995,
    dz1_min=5.0 * mm,
    ks_fdrho_min=1 * mm,
    dzero_vz_min=-300 * mm,
):
    """Combine D meson with KS and additonal hadron (K/pi).
    Make MASS, P, PT, SUM(PT), MAXDOCACUT cuts in the combination;
    MASS, P, PT, CHI2DOF, BPVVDZ, BPVFDCHI2, BPVIPCHI2, BPVDIRA cuts after the vertex fit.
    Cuts generally based on Run2 Turbo lines.
    """
    comb_cut = F.require_all(
        F.MAXSDOCACUT(doca_max),
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.PT > comb_pt_min,
        F.SUM(F.PT) > sum_pt_min,
        F.P > comb_p_min,
    )
    vertex_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.END_VZ > dzero_vz_min,
        F.OWNPVVDRHO < bpfdrho_max,
        F.PT > pt_min,
        F.P > p_min,
        F.CHI2DOF < vchi2pdof_max,
        # F.OWNPVLTIME > bpvltime_min,
        F.OWNPVVDZ > bpvvdz_min,
        F.OWNPVFDCHI2 > bpvfdchi2_min,  # <-- to be checked
        # F.OWNPVIPCHI2 < bpvipchi2_max, #<-- to be checked
        F.OWNPVDIRA > bpvdira_min,
        F.CHILD(1, F.END_VZ) - F.END_VZ > dz1_min,
        fmath.sqrt(
            (F.CHILD(1, F.END_VX) - F.END_VX) ** 2
            + (F.CHILD(1, F.END_VY) - F.END_VY) ** 2
        )
        > ks_fdrho_min,
    )
    return ParticleCombiner(
        [ks0, particle],
        name=name,
        DecayDescriptor=decay_descriptor,
        CombinationCut=comb_cut,
        CompositeCut=vertex_cut,
    )


###########################
## Hlt2 lines definition ##
###########################
@register_line_builder(all_lines)
def dp_to_kspi_ll_line(name="Hlt2Charm_DpDspToKsPip_LL", prescale=1):
    pvs = make_pvs()
    long_pions = filter_long_pions(pvs)
    ks_ll = make_ks_ll(
        filter_long_pions_from_ks(pvs), filter_long_pions_from_ks(pvs), pvs
    )
    dp_kspi_ll = combine_d_ks_h(
        ks_ll, long_pions, pvs, "[D+ -> KS0 pi+]cc", name=f"{name}_Combiner"
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ks_ll, dp_kspi_ll],
        prescale=prescale,
        # extra_outputs=isolation.make_iso_particles(dp_kspi_ll, coneangle=0.5)
    )


@register_line_builder(all_lines)
def dp_to_kspi_ld_line(name="Hlt2Charm_DpDspToKsPip_LD", prescale=1):
    pvs = make_pvs()
    long_pions = filter_long_pions(pvs)
    ks_ld = make_ks_dd(
        filter_long_pions_from_ks(pvs), filter_down_pions_from_ks(pvs), pvs
    )
    dp_kspi_ld = combine_d_ks_h(
        ks_ld,
        long_pions,
        pvs,
        "[D+ -> KS0 pi+]cc",
        name=f"{name}_Combiner",
        doca_max=2.0 * mm,
        dz1_min=50.0 * mm,
        ks_fdrho_min=10 * mm,
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ks_ld, dp_kspi_ld],
        prescale=prescale,
        # extra_outputs=isolation.make_iso_particles(dp_kspi_ld, coneangle=0.5, DownstreamTrackIso=True)
    )


@register_line_builder(all_lines)
def dp_to_kspi_dd_line(name="Hlt2Charm_DpDspToKsPip_DD", prescale=1):
    pvs = make_pvs()
    long_pions = filter_long_pions(pvs)
    ks_dd = make_ks_dd(
        filter_down_pions_from_ks(pvs), filter_down_pions_from_ks(pvs), pvs
    )
    dp_kspi_dd = combine_d_ks_h(
        ks_dd,
        long_pions,
        pvs,
        "[D+ -> KS0 pi+]cc",
        name=f"{name}_Combiner",
        doca_max=2.0 * mm,
        dz1_min=50.0 * mm,
        ks_fdrho_min=10 * mm,
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ks_dd, dp_kspi_dd],
        prescale=prescale,
        # extra_outputs=isolation.make_iso_particles(dp_kspi_dd, coneangle=0.5, DownstreamTrackIso=True)
    )


@register_line_builder(all_lines)
def dp_to_kskp_ll_line(name="Hlt2Charm_DpDspToKsKp_LL", prescale=1):
    pvs = make_pvs()
    long_kaons = filter_long_kaons(pvs)
    ks_ll = make_ks_ll(
        filter_long_pions_from_ks(pvs), filter_long_pions_from_ks(pvs), pvs
    )
    dp_kskp_ll = combine_d_ks_h(
        ks_ll, long_kaons, pvs, "[D+ -> KS0 K+]cc", name=f"{name}_Combiner"
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ks_ll, dp_kskp_ll],
        prescale=prescale,
        # extra_outputs=isolation.make_iso_particles(dp_kskp_ll, coneangle=0.5)
    )


@register_line_builder(all_lines)
def dp_to_kskp_ld_line(name="Hlt2Charm_DpDspToKsKp_LD", prescale=1):
    pvs = make_pvs()
    long_kaons = filter_long_kaons(pvs)
    ks_ld = make_ks_dd(
        filter_long_pions_from_ks(pvs), filter_down_pions_from_ks(pvs), pvs
    )
    dp_kskp_ld = combine_d_ks_h(
        ks_ld,
        long_kaons,
        pvs,
        "[D+ -> KS0 K+]cc",
        name=f"{name}_Combiner",
        doca_max=2.0 * mm,
        dz1_min=50.0 * mm,
        ks_fdrho_min=10 * mm,
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ks_ld, dp_kskp_ld],
        prescale=prescale,
        # extra_outputs=isolation.make_iso_particles(dp_kskp_ld, coneangle=0.5, DownstreamTrackIso=True)
    )


@register_line_builder(all_lines)
def dp_to_kskp_dd_line(name="Hlt2Charm_DpDspToKsKp_DD", prescale=1):
    pvs = make_pvs()
    long_kaons = filter_long_kaons(pvs)
    ks_dd = make_ks_dd(
        filter_down_pions_from_ks(pvs), filter_down_pions_from_ks(pvs), pvs
    )
    dp_kskp_dd = combine_d_ks_h(
        ks_dd,
        long_kaons,
        pvs,
        "[D+ -> KS0 K+]cc",
        name=f"{name}_Combiner",
        doca_max=2.0 * mm,
        dz1_min=50.0 * mm,
        ks_fdrho_min=10 * mm,
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ks_dd, dp_kskp_dd],
        prescale=prescale,
        # extra_outputs=isolation.make_iso_particles(dp_kskp_dd, coneangle=0.5, DownstreamTrackIso=True)
    )
