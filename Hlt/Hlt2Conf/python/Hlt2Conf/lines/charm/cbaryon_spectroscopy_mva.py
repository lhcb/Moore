###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of Lambda_c+/Xi_c+ -> p+ K- pi+, Xic0 -> p+ K- K- pi+ HLT2 lines.

Primary physics use-case in spectroscopy studies.
Based on Charm Production Cross-section Early Measurements lines as a start point.

----

Hlt2Charm_LcpToPpKmPip_MVA_PR
[Lambda_c+ -> p+ K- pi+]cc

Hlt2Charm_XicpToPpKmPip_MVA_PR
[Xi_c+ -> p+ K- pi+]cc

Hlt2Charm_Xic0ToPpKmKmPip_MVA_PR
[Xi_c0 -> p+ K- K- pi+]cc


TODO/Notes:
- !!! All lines are PersistReco, we should go to Selective Persistence eventually.
    - Cone algorithm being developed to reduce number of extra_outputs particles
    - to control PR rate we may need to prescale, hopefully this never becomes necessary
      (as we've should have gone to SP at that point + tuned cuts to keep rate acceptable)

- Cuts will need tightening to control rate:
    - Currently, cuts tuned according to latest available Upgrade MC
        - Sim10Ua1
    - Aim to minimise background bandwidth, go for high signal purity, even if
      signal efficiency lost.
    - Ideas from Run 2 studies may help inform cuts.
    - Keep enough mass-sideband for data-driven selection studies

    - From @mstahl:
        - Have you considered tuning cuts differently for Xic and Lc?
          You should be able/might need to cut harder on the Xic due
          to longer lifetime and Cabibbo suppression.
        - Has been done - thresholds not identical but quite similar

- (F.PID_P - F.PID_K) > dllp_m_dllk_min cut on the proton was added (from Run 2).

- Xi_c0 line had tighter PID cuts than the other two lines. Consider
    re-visiting those if needed (probably after PID is tuned?)

- FDCHI2 cut
    - Investigated. Added threshold at 50

- Tune PT and IP cuts for hadrons
    - make_*_from_baryons.

"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from GaudiKernel.SystemOfUnits import picosecond as ps
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
    make_has_rich_long_protons,
)

from Hlt2Conf.lines.charm.prefilters import charm_prefilters

from . import cbaryon_spectroscopy_mva_builder

all_lines = {}


@configurable
def _make_protons_from_baryons_mva(
    pt_min=500 * MeV, p_min=10 * GeV, mipchi2_min=4.0, dllp_min=5.0, dllp_m_dllk_min=3.0
):
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_long_protons(),
        F.FILTER(
            F.require_all(
                F.PT > pt_min,
                F.P > p_min,
                F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
                F.PID_P > dllp_min,
                (F.PID_P - F.PID_K) > dllp_m_dllk_min,
            )
        ),
    )


@configurable
def _make_kaons_from_baryons_mva(
    pt_min=300 * MeV,
    p_min=3.6 * GeV,
    mipchi2_min=5.0,
    dllk_min=5.0,
):
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > pt_min,
                F.P > p_min,
                F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
                F.PID_K > dllk_min,
            ),
        ),
    )


@configurable
def _make_pions_from_baryons_mva(
    pt_min=200 * MeV, p_min=1 * GeV, mipchi2_min=6.0, dllk_max=-0.5
):
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > pt_min,
                F.P > p_min,
                F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
                F.PID_K < dllk_max,
            ),
        ),
    )


@configurable
def _make_lc_or_xicp_pKpi_mva(
    sum_pt_min=2.5 * GeV,
    maxtree_pt_min=0.8 * GeV,
    mipchi2_min_at_least_one=16.0,
    mipchi2_min_at_least_two=9.0,
    mintree_pt_min=200.0 * MeV,
    mintree_mipchi2_min=4.0,
    maxtree_mipchi2_min=16.0,
    doca_max=0.1 * mm,
    bpvvdrho_min=0.01 * mm,
    bpvvdrho_max=10 * mm,
    bpvdira_min=0.9998,
    vxchi2ndof_max=9,
    bpvltime_min=0.2 * ps,
    bpvfdchi2_min=24,
    bpvipchi2_max=9,
    pt_min=2.0 * GeV,
    #
    comb_m_min=2_201 * MeV,
    comb_m_max=2_372 * MeV,
    m_min=2_211 * MeV,
    m_max=2_362 * MeV,
    decay_descriptor="[Lambda_c+ -> p+ K- pi+]cc",
):
    pvs = make_pvs()
    return ParticleCombiner(
        Inputs=[
            _make_protons_from_baryons_mva(),
            _make_kaons_from_baryons_mva(),
            _make_pions_from_baryons_mva(),
        ],
        DecayDescriptor=decay_descriptor,
        name=f"Charm_Spec_{'Lcp' if 'Lambda_c+' in decay_descriptor else 'Xicp'}_PpKmPip",
        CombinationCut=F.require_all(
            F.math.in_range(
                comb_m_min,
                F.MASS,
                comb_m_max,
            ),
            F.SUM(F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min_at_least_one, Vertices=pvs))
            >= 1,
            F.SUM(F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min_at_least_two, Vertices=pvs))
            >= 2,
            #
            F.SUM(F.PT) > sum_pt_min,
            F.MAX(F.PT) > maxtree_pt_min,
            F.MIN(F.PT) > mintree_pt_min,
            #
            F.MIN(F.MINIPCHI2(pvs)) > mintree_mipchi2_min,
            F.MAX(F.MINIPCHI2(pvs)) > maxtree_mipchi2_min,
            F.MAXSDOCACUT(doca_max),
        ),
        CompositeCut=F.require_all(
            F.PT > pt_min,
            F.math.in_range(m_min, F.MASS, m_max),
            F.CHI2DOF < vxchi2ndof_max,
            F.math.in_range(bpvvdrho_min, F.OWNPVVDRHO, bpvvdrho_max),
            F.OWNPVDIRA > bpvdira_min,
            F.OWNPVLTIME > bpvltime_min,
            F.OWNPVDCHI2 > bpvfdchi2_min,
            F.OWNPVIPCHI2 < bpvipchi2_max,
        ),
    )


@configurable
def _make_lcp_pKpi_mva(
    comb_m_min=2_201 * MeV,
    comb_m_max=2_372 * MeV,
    m_min=2_211 * MeV,
    m_max=2_362 * MeV,
):
    return _make_lc_or_xicp_pKpi_mva(
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        m_min=m_min,
        m_max=m_max,
        decay_descriptor="[Lambda_c+ -> p+ K- pi+]cc",
    )


@configurable
def _make_xicp_pKpi_mva(
    comb_m_min=2_382 * MeV,
    comb_m_max=2_553 * MeV,
    m_min=2_392 * MeV,
    m_max=2_543 * MeV,
    mintree_mipchi2_min=6.0,
    bpvvdrho_min=0.015,
    bpvdira_min=0.9999,
    vxchi2ndof_max=6,
    bpvltime_min=0.3 * ps,
    bpvfdchi2_min=42,
):
    return _make_lc_or_xicp_pKpi_mva(
        bpvvdrho_min=bpvvdrho_min,
        bpvdira_min=bpvdira_min,
        vxchi2ndof_max=vxchi2ndof_max,
        bpvltime_min=bpvltime_min,
        bpvfdchi2_min=bpvfdchi2_min,
        mintree_mipchi2_min=mintree_mipchi2_min,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        m_min=m_min,
        m_max=m_max,
        pt_min=2.4 * GeV,
        decay_descriptor="[Xi_c+ -> p+ K- pi+]cc",
    )


### Xic0 line


@configurable
def _make_xic0_pkkpi_mva(
    comb_m_min=2_386.0 * MeV,
    comb_m_max=2_780.0 * MeV,
    comb_sum_pt_min=2 * GeV,
    comb_mipchi2_min_at_least_one=8.0,
    comb_mipchi2_min_at_least_two=6.0,
    doca_max=0.1 * mm,
    #
    bpvvdrho_min=0.01,
    bpvdira_min=0.999,
    vxchi2ndof_max=6,
    bpvltime_min=0.1 * ps,
    m_min=2_396.0 * MeV,
    m_max=2_770.0 * MeV,
):
    pvs = make_pvs()
    decay_descriptor = "[Xi_c0 -> p+ K- K- pi+]cc"

    return ParticleCombiner(
        Inputs=[
            _make_protons_from_baryons_mva(mipchi2_min=2.0),
            _make_kaons_from_baryons_mva(mipchi2_min=4.0),
            _make_kaons_from_baryons_mva(mipchi2_min=4.0),
            _make_pions_from_baryons_mva(mipchi2_min=6.0),
        ],
        DecayDescriptor=decay_descriptor,
        name="Charm_Spec_Xic0_PpKmKmPip_{hash}",
        CombinationCut=F.require_all(
            F.math.in_range(
                comb_m_min,
                F.MASS,
                comb_m_max,
            ),
            F.MAXSDOCACUT(doca_max),
            F.SUM(F.PT) > comb_sum_pt_min,
            F.SUM(F.MINIPCHI2(pvs) > comb_mipchi2_min_at_least_one) >= 1,
            F.SUM(F.MINIPCHI2(pvs) > comb_mipchi2_min_at_least_two) >= 2,
        ),
        CompositeCut=F.require_all(
            F.CHI2DOF < vxchi2ndof_max,
            F.OWNPVDIRA > bpvdira_min,
            F.OWNPVLTIME > bpvltime_min,
            F.OWNPVVDRHO > bpvvdrho_min,
            F.math.in_range(m_min, F.MASS, m_max),
            F.OWNPVDCHI2 > 16.0,
            F.PT > 2.2 * GeV,
            F.OWNPVIPCHI2 < 9.0,
        ),
    )


##  Baryon MVA lines


@register_line_builder(all_lines)
@configurable
def lcp2pkpi_mva_line(name="Hlt2Charm_LcpToPpKmPip_MVA_PR", prescale=1.0, mva_wp=-0.16):
    mva_name = "lc_xic"
    lc = _make_lcp_pKpi_mva()
    pvs = make_pvs()
    lc_mva = cbaryon_spectroscopy_mva_builder.make_cbaryon_lc_xicp_mva(
        lc, pvs, mva_name, mva_wp
    )

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lc_mva],
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def xicp2pkpi_mva_line(
    name="Hlt2Charm_XicpToPpKmPip_MVA_PR", prescale=1.0, mva_wp=-0.15
):
    mva_name = "lc_xic"
    pvs = make_pvs()
    xcp = _make_xicp_pKpi_mva()
    xcp_mva = cbaryon_spectroscopy_mva_builder.make_cbaryon_lc_xicp_mva(
        xcp, pvs, mva_name, mva_wp
    )

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xcp_mva],
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(all_lines)
@configurable
def xic02pkkpi_mva_line(
    name="Hlt2Charm_Xic0ToPpKmKmPip_MVA_PR", prescale=1.0, mva_wp=-0.2
):
    mva_name = "xic0"
    pvs = make_pvs()
    xc0 = _make_xic0_pkkpi_mva()
    xc0_mva = cbaryon_spectroscopy_mva_builder.make_cbaryon_xic0_mva(
        xc0, pvs, mva_name, mva_wp
    )

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xc0_mva],
        persistreco=True,
        prescale=prescale,
    )
