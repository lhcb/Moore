###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Lines for SL decays for single chamed baryons

Prompt decays and the corresponding WS lines:
    1. Lambda_c+ / Xi_c+ -> Lambda0(LL/DD)  mu nu :
        Hlt2Charm_LcpXicpToL0MupNu_LL(_WS) and Hlt2Charm_LcpXicpToL0MupNu_DD(_WS)

    2. Lambda_c+ / Xi_c+ -> p+  h-(pi-/K-)  mu nu :
        Hlt2Charm_LcpXicpToPpKmMupNu(_WS) and Hlt2Charm_LcpXicpToPpPimMupNu(_WS)

    3. Omega_c0  -> Omega-(LL/DD)L     mu nu :
        Hlt2Charm_Oc0ToOmMupNu_LLL(_WS) and Hlt2Charm_Oc0ToOmMupNu_DDL(_WS)

    4. Xi_c0 / Omega_c0  -> Xi-(LL/DD)L mu nu :
        Hlt2Charm_Xic0Oc0ToXimMupNu_LLL(_WS) and Hlt2Charm_Xic0Oc0ToXimMupNu_DDL(_WS)

    5. Xi_c+  -> Xi-(LL/DD)L pi+ mu nu :
        Hlt2Charm_XicpToXimPipMupNu_LLL(_WS) and Hlt2Charm_XicpToXimPipMupNu_DDL(_WS)

    6. Xi_c0 / Omega_c0  -> Lambda0(LL/DD) K-  mu nu :
        Hlt2Charm_Xic0Oc0ToL0KmMupNu_LL(_WS) and Hlt2Charm_Xic0Oc0ToL0KmMupNu_DD(_WS)
    ## The order in ParticleCombiner is by intermediate status and then the size of stable particles from smallest to largest.

The following lines cover SL decays above but b-baryons are present in the decay
topologies to further suppress backgrounds and implement DecayTreeFitter with a missing nu.
These lines include (most) possible SL decays of Lambda_c+, Xi_c+, Xi_c0, Omega_c0.
Charmed baryons from beauty decays (Btag) along with WS lines and normalization lines are listed in the following:
    1. Lambda_b0 ->  Lambda_c+ pi- pi- pi+,  Lambda_c+ ->  p K-  mu+  nu :
        Hlt2Charm_Lb0ToLcpPipPimPim_LcpToPpKmMupNu
        Hlt2Charm_Lb0ToLcpPipPimPim_LcpToPpKmMumNu_WS
        Hlt2Charm_Lb0ToLcpPipPimPim_LcpToPpKmPip

    2. Lambda_b0 ->  Lambda_c+ pi- pi- pi+,  Lambda_c+ ->  p  pi-  mu+  nu :
        Hlt2Charm_Lb0ToLcpPipPimPim_LcpToPpPimMupNu
        Hlt2Charm_Lb0ToLcpPipPimPim_LcpToPpPimMumNu_WS
        Hlt2Charm_Lb0ToLcpPipPimPim_LcpToPpPimPip

    3. Lambda_b0 ->  Lambda_c+ pi- pi- pi+,  Lambda_c+ ->   Lambda  mu+  nu :
        Hlt2Charm_Lb0ToLcpPimPimPip_LcpToL0MupNu_LL and Hlt2Charm_Lb0ToLcpPimPimPip_LcpToL0MupNu_DD
        Hlt2Charm_Lb0ToLcpPimPimPip_LcpToL0MumNu_LL_WS and Hlt2Charm_Lb0ToLcpPimPimPip_LcpToL0MumNu_DD_WS
        Hlt2Charm_Lb0ToLcpPimPimPip_LcpToL0Pip_LL and Hlt2Charm_Lb0ToLcpPimPimPip_LcpToL0Pip_DD

    4. Xi_b0 ->  Xi_c+ pi+ pi- pi-,  Xi_c+ ->   Lambda  mu+  nu :
        Hlt2Charm_Xib0ToXicpPimPimPip_XicpToL0MupNu_LL and Hlt2Charm_Xib0ToXicpPimPimPip_XicpToL0MupNu_DD
        Hlt2Charm_Xib0ToXicpPimPimPip_XicpToL0MumNu_LL_WS and Hlt2Charm_Xib0ToXicpPimPimPip_XicpToL0MumNu_DD_WS
        Hlt2Charm_Xib0ToXicpPimPimPip_XicpToL0Pip_LL and Hlt2Charm_Xib0ToXicpPimPimPip_XicpToL0Pip_DD

    5. Xi_b- ->  Xi_c0 pi+ pi- pi-,  Xi_c0 ->   Xi- mu+  nu :
        Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0PimMupNu_LL and Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0PimMupNu_DD
        Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0PimMumNu_LL_WS and Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0PimMumNu_DD_WS
        Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0PimMupNu_LL and Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0PimMupNu_DD

    6. Xi_b- ->  Xi_c0 pi+ pi- pi-,  Xi_c0 ->   Lambda0 K- mu+  nu :
        Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0KmMupNu_LL and Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0KmMupNu_DD
        Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0KmMumNu_LL_WS and Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0KmMumNu_DD_WS
        Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0KmPip_LL and Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0KmPip_DD

    7. Xi_b- ->  Xi_c0 pi+ pi- pi-,  Xi_c0 ->   Lambda0 pi- mu+  nu :
        Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0PimMupNu_LL and Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0PimMupNu_DD
        Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0PimMumNu_LL_WS and Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0PimMumNu_DD_WS
        Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0PimPip_LL and Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0PimPip_DD

    8. Omega_b- ->  Omega_c0 pi- pi- pi+,  Omega_c0 ->   Xi-  mu+  nu :
        Hlt2Charm_ObmToOc0PipPimPim_Oc0ToXimMupNu_LLL and DDL DDD
        Hlt2Charm_ObmToOc0PipPimPim_Oc0ToXimMumNu_LLL_WS and DDL DDD
        Hlt2Charm_ObmToOc0PipPimPim_Oc0ToXimPip_LLL and DDL DDD

    9. Omega_b- ->  Omega_c0 pi- pi- pi+,  Omega_c0 ->   Omega-  mu+  nu :
        Hlt2Charm_ObmToOc0PipPimPim_Oc0ToOmMupNuNu_LLL and DDL DDD
        Hlt2Charm_ObmToOc0PipPimPim_Oc0ToOmMumNu_LLL_WS and DDL DDD
        Hlt2Charm_ObmToOc0PipPimPim_Oc0ToOmPipNu_LLL and DDL DDD

where the daughter baryon is reconstructed via:
  Lambda0 -> p pi-  (LL/DD)
  Xi-     -> Lambda0(LL/DD) pi- (L)
  Omega-  -> Lambda0(LL/DD) K-  (L)

The following channels can be used as the normalization channels:
  Lambda_c+ -> Lambda0(LL/DD) pi+
  Lambda_c+ -> p+  h-(pi-/K-) pi+
  Omega_c0  -> Omega-(LL/DD)L pi+
  Omega_c0  -> Xi-(LL/DD)L    pi+
  Xi_c+     -> Lambda0(LL/DD) pi+
  Xi_c0     -> Xi-(LL/DD)L    pi+


Proponents: Miroslav Saur, Xiao-Rui Lyu, Ziyi Wang, Yangjie Su, Han Gao, Ying Liu
TODO:
    add requirements on tracks chi2/ndf
    add HLT1 filtering
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    make_down_kaons,
    make_down_pions,
    make_down_protons,
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
    make_has_rich_long_protons,
    make_ismuon_long_muon,
    make_long_kaons,
    make_long_pions,
    make_long_protons,
)

from . import charm_isolation as isolation
from .prefilters import charm_prefilters

all_lines = {}


# re-definitions of functors
def _DZ_CHILD(i):
    return F.CHILD(i, F.END_VZ) - F.END_VZ


def _MIPCHI2_MIN(cut, pvs=make_pvs):
    return F.MINIPCHI2CUT(IPChi2Cut=cut, Vertices=pvs())


###############################################################################
# Track filters
###############################################################################


def _filter_long_pions():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 500 * MeV, F.P > 3 * GeV, _MIPCHI2_MIN(6.0), F.PID_K < 5.0
            ),
        ),
    )


def _filter_long_kaons():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > 300 * MeV, F.P > 3 * GeV, _MIPCHI2_MIN(6.0), F.PID_K > 5.0
            ),
        ),
    )


def _filter_long_protons():
    return ParticleFilter(
        make_has_rich_long_protons(),
        F.FILTER(
            F.require_all(
                F.PT > 500 * MeV,
                F.P > 3 * GeV,
                _MIPCHI2_MIN(4.0),
                F.PID_P > 5.0,
                F.PID_P - F.PID_K > 0.0,
            ),
        ),
    )


def _filter_long_muons_loose():
    return ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.PT > 250 * MeV,
                F.P > 3 * GeV,
                _MIPCHI2_MIN(4.0),
                F.PID_MU > 0.0,
            ),
        ),
    )


def _filter_long_muons():
    return ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.PT > 600 * MeV,
                F.P > 10 * GeV,
                _MIPCHI2_MIN(4.0),
                F.PID_MU > 0.0,
            ),
        ),
    )


def _filter_tight_long_muons():
    return ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.PT > 1 * GeV,
                F.P > 10 * GeV,
                _MIPCHI2_MIN(9.0),
                F.PID_MU > 3.0,
                # trghostprob_max=None, # TODO
            ),
        ),
    )


def _filter_long_pions_from_lambda():
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 100 * MeV,
                _MIPCHI2_MIN(36.0),
                F.OWNPVIPCHI2 > 1,
            ),
        ),
    )


def _filter_long_protons_from_lambda():
    return ParticleFilter(
        make_long_protons(),
        F.FILTER(
            F.require_all(
                F.PT > 100 * MeV,
                F.P > 1 * GeV,
                _MIPCHI2_MIN(6.0),
                F.PID_P > 5.0,
                F.PID_P - F.PID_K > 0.0,
                F.OWNPVIPCHI2 > 1,
            ),
        ),
    )


def _filter_down_pions_from_lambda():
    return ParticleFilter(
        make_down_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 100 * MeV,
                F.P > 1 * GeV,
                F.OWNPVIPCHI2 > 5,
            ),
        ),
    )


def _filter_down_protons_from_lambda():
    #
    return ParticleFilter(
        make_down_protons(),
        F.FILTER(
            F.require_all(
                F.PT > 300 * MeV,
                F.P > 5 * GeV,
                F.OWNPVIPCHI2 > 5,
            ),
        ),
    )


def _filter_long_pions_from_xi():
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 100 * MeV,
                _MIPCHI2_MIN(8.0),
            ),
        ),
    )


def _filter_down_pions_from_xi():
    return ParticleFilter(
        make_down_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 100 * MeV,
            ),
        ),
    )


def _filter_long_kaons_from_omega():
    return ParticleFilter(
        make_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > 180 * MeV,
                F.P > 3 * GeV,
                _MIPCHI2_MIN(9.0),
                F.PID_K > -2.0,
            ),
        ),
    )


def _filter_down_kaons_from_omega():
    return ParticleFilter(
        make_down_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > 180 * MeV,
                F.P > 3 * GeV,
            ),
        ),
    )


###############################################################################
# Basic combiners
###############################################################################


def make_lambdall():
    """Make Lambda -> p+ pi- from long tracks."""
    protons = _filter_long_protons_from_lambda()
    pions = _filter_long_pions_from_lambda()
    return ParticleCombiner(
        [protons, pions],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name="Charm_CBaryonToSl_make_lambdall_{hash}",
        CombinationCut=F.require_all(
            F.MASS < 1160 * MeV,
            F.MAXSDOCACUT(0.2 * mm),
            F.MAXSDOCACHI2CUT(16.0),
            F.SUM(F.PT) > 800 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(1095 * MeV, F.MASS, 1140 * MeV),
            F.CHI2DOF < 9.0,
            F.OWNPVVDZ > 4 * mm,
            F.END_VZ > -100 * mm,
            F.END_VZ < 500 * mm,
        ),
    )


def make_lambdadd():
    """Make Lambda -> p+ pi- from downstream tracks."""
    pions = _filter_down_pions_from_lambda()
    protons = _filter_down_protons_from_lambda()
    return ParticleCombiner(
        [protons, pions],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name="Charm_CBaryonToSl_make_lambdadd_{hash}",
        CombinationCut=F.require_all(
            F.MASS < 1180 * MeV,
            F.MAXSDOCACUT(2 * mm),
            F.MAXSDOCACHI2CUT(16.0),
            F.SUM(F.PT) > 800 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(1095 * MeV, F.MASS, 1140 * MeV),
            F.CHI2DOF < 9.0,
            F.OWNPVVDZ > 20 * mm,
            F.END_VZ > 300 * mm,
            F.END_VZ < 2275 * mm,
        ),
    )


def make_xim_to_lambdapi(lambdas, pions, ll=True):
    """Return a Xi- -> Lambda0 pi- decay maker."""
    composite_cut = F.require_all(
        in_range(1258 * MeV, F.MASS, 1386 * MeV),
        F.CHI2 < 10.0,
    )
    if ll:
        composite_cut &= F.OWNPVFDCHI2 > 10.0
    return ParticleCombiner(
        [lambdas, pions],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        name="Charm_CBaryonToSl_make_xim_to_lambdapi_{hash}",
        CombinationCut=F.require_all(
            in_range(1242 * MeV, F.MASS, 1402 * MeV),
            F.MAXSDOCACUT((0.2 if ll else 2) * mm),
            F.SUM(F.PT) > 500 * MeV,
        ),
        CompositeCut=composite_cut,
    )


def make_omegam_to_lambdak(lambdas, kaons, ll=True):
    """Return a Omega- -> Lambda0 K- decay maker."""
    composite_cut = F.require_all(
        in_range(1608 * MeV, F.MASS, 1736 * MeV),
        F.CHI2 < 10.0,
    )
    if ll:
        composite_cut &= F.OWNPVFDCHI2 > 10.0
    return ParticleCombiner(
        [lambdas, kaons],
        DecayDescriptor="[Omega- -> Lambda0 K-]cc",
        name="Charm_CBaryonToSl_make_omegam_to_lambdak_{hash}",
        CombinationCut=F.require_all(
            in_range(1592 * MeV, F.MASS, 1752 * MeV),
            F.MAXSDOCACUT((0.2 if ll else 2) * mm),
            F.SUM(F.PT) > 500 * MeV,
        ),
        CompositeCut=composite_cut,
    )


def make_lambdacp_to_pKmu_from_beauty(decay_descriptor="[Lambda_c+ -> p+ K- mu+]cc"):
    """Return a Lambda_c+ -> p K mu decay maker. Lambda_c+ is from beauty decays."""
    protons = _filter_long_protons()
    kaons = _filter_long_kaons()
    muons = _filter_long_muons_loose()
    return ParticleCombiner(
        [protons, kaons, muons],
        DecayDescriptor=decay_descriptor,
        name="Charm_CBaryonToSl_LcToPKMuFromBeauty_{hash}",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1700 * MeV, F.MASS, 2350 * MeV),
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.OWNPVVDZ > 1.5 * mm,
            F.CHI2DOF < 6.0,
            F.OWNPVFDCHI2 > 15.0,
            F.OWNPVDIRA > 0.95,
        ),
    )


def make_lambdacp_to_pKpi_from_beauty():
    """Return a Lambda_c+ -> p K pi decay maker. Lambda_c+ is from beauty decays."""
    protons = _filter_long_protons()
    kaons = _filter_long_kaons()
    pions = _filter_long_pions()
    return ParticleCombiner(
        [protons, kaons, pions],
        DecayDescriptor="[Lambda_c+ -> p+ K- pi+]cc",
        name="Charm_CBaryonToSl_LcToPKPiFromBeauty_{hash}",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.P > 15 * GeV,
            in_range(2200 * MeV, F.MASS, 2350 * MeV),
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 1.5 * mm,
            F.OWNPVFDCHI2 > 20.0,
            F.OWNPVDIRA > 0.995,
        ),
    )


def make_lambdacp_to_ppimu_from_beauty(decay_descriptor="[Lambda_c+ -> p+ pi- mu+]cc"):
    """Return a Lambda_c+ -> p pi mu decay maker. Lambda_c+ is from beauty decays."""
    protons = _filter_long_protons()
    pions = _filter_long_pions()
    muons = _filter_long_muons_loose()
    return ParticleCombiner(
        [protons, pions, muons],
        DecayDescriptor=decay_descriptor,
        name="Charm_CBaryonToSl_LcToPPiMuFromBeauty_{hash}",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.7 * GeV,
            in_range(1200 * MeV, F.MASS, 2350 * MeV),
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.OWNPVFDCHI2 > 15.0,
            F.OWNPVVDZ > 1.5 * mm,
            F.CHI2DOF < 6.0,
            F.OWNPVDIRA > 0.95,
        ),
    )


def make_lambdacp_to_ppipi_from_beauty():
    """Return a Lambda_c+ -> p pi pi decay maker. Lambda_c+ is from beauty decays."""
    protons = _filter_long_protons()
    pions = _filter_long_pions()
    return ParticleCombiner(
        [protons, pions, pions],
        DecayDescriptor="[Lambda_c+ -> p+ pi- pi+]cc",
        name="Charm_CBaryonToSl_LcToPPiPiFromBeauty_{hash}",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.7 * GeV,
            in_range(2200 * MeV, F.MASS, 2350 * MeV),
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 1.5 * mm,
            F.OWNPVFDCHI2 > 20.0,
            F.OWNPVDIRA > 0.995,
        ),
    )


def make_lambdacpxicp_to_lambdamu_ll_from_beauty(decay_descriptor, Mmax):
    """Return a Lambda_c+ -> Lambda0 mu+ decay maker. Lambda_c+ is from beauty decays."""
    muons = _filter_long_muons_loose()
    lambdas = make_lambdall()

    return ParticleCombiner(
        [lambdas, muons],
        DecayDescriptor=decay_descriptor,
        name="Charm_CBaryonToSl_LcpXicpToLmdMu_LL_{hash}",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            in_range(1200 * MeV, F.MASS, Mmax * MeV),
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 16 * GeV,
            F.CHI2DOF < 10.0,
            _DZ_CHILD(1) > 8 * mm,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVFDCHI2 > 15.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_lambdacpxicp_to_lambdapi_ll_from_beauty(decay_descriptor, Mmin, Mmax):
    """Return a Lambda_c+ -> Lambda0 pi+ decay maker. Lambda_c+ is from beauty decays."""
    pions = _filter_long_pions()
    lambdas = make_lambdall()

    return ParticleCombiner(
        [lambdas, pions],
        DecayDescriptor=decay_descriptor,
        name="Charm_CBaryonToSl_LcpXicpToLmdPip_LL_{hash}",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            in_range(Mmin * MeV, F.MASS, Mmax * MeV),
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 16 * GeV,
            F.CHI2DOF < 10.0,
            _DZ_CHILD(1) > 8 * mm,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVFDCHI2 > 15.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_lambdacpxicp_to_lambdamu_dd_from_beauty(decay_descriptor, Mmax):
    """Return a Lambda_c+ -> Lambda0 mu+ decay maker. Lambda_c+ is from beauty decays."""
    muons = _filter_long_muons_loose()
    lambdas = make_lambdadd()

    return ParticleCombiner(
        [lambdas, muons],
        DecayDescriptor=decay_descriptor,
        name="Charm_CBaryonToSl_LcpXicpToLmdMu_LL_{hash}",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            in_range(1200 * MeV, F.MASS, Mmax * MeV),
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 16 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 15.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_lambdacpxicp_to_lambdapi_dd_from_beauty(decay_descriptor, Mmin, Mmax):
    """Return a Lambda_c+ -> Lambda0 pi+ decay maker. Lambda_c+ is from beauty decays."""
    pions = _filter_long_pions()
    lambdas = make_lambdadd()

    return ParticleCombiner(
        [lambdas, pions],
        DecayDescriptor=decay_descriptor,
        name="Charm_CBaryonToSl_LcpXicpToLmdPip_LL_{hash}",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            in_range(Mmin * MeV, F.MASS, Mmax * MeV),
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 16 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 15.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


###############################################################################
# charm baryon SL combiners
###############################################################################


def make_xic0_to_lambda0pillmu_from_beauty():
    """Return a Xic0 -> lambda0 pi mu decay maker. Xic0 is from beauty decays. lambda0 is ll."""
    muons = _filter_long_muons()
    pions = _filter_long_pions()
    lambdas = make_lambdall()
    return ParticleCombiner(
        [lambdas, pions, muons],
        DecayDescriptor="[Xi_c0 -> Lambda0 pi- mu+]cc",
        name="Charm_CBaryonToSl_Xic0ToL0piLLMuFromBeauty",
        Combination12Cut=F.require_all(
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.2 * mm),
            in_range(1500 * MeV, F.MASS, 2600 * MeV),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 10.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_xic0_to_lambda0piddmu_from_beauty():
    """Return a Xic0 -> lambda0 pi mu decay maker. Xic0 is from beauty decays. lambda0 is dd."""
    muons = _filter_long_muons()
    pions = _filter_long_pions()
    lambdas = make_lambdadd()
    return ParticleCombiner(
        [lambdas, pions, muons],
        DecayDescriptor="[Xi_c0 -> Lambda0 pi- mu+]cc",
        name="Charm_CBaryonToSl_Xic0ToL0piDDMuFromBeauty",
        Combination12Cut=F.require_all(
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 2600 * MeV),
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 10.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_xic0_to_lambda0kllmu_from_beauty():
    """Return a Xic0 -> lambda0 k mu decay maker. Xic0 is from beauty decays. lambda0 is ll."""
    muons = _filter_long_muons()
    kaons = _filter_long_kaons()
    lambdas = make_lambdall()
    return ParticleCombiner(
        [lambdas, kaons, muons],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- mu+]cc",
        name="Charm_CBaryonToSl_Xic0ToL0KLLMuFromBeauty",
        Combination12Cut=F.require_all(
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1700 * MeV, F.MASS, 2600 * MeV),
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_xic0_to_lambda0kddmu_from_beauty():
    """Return a Xic0 -> lambda0 k mu decay maker. Xic0 is from beauty decays. lambda0 is dd."""
    muons = _filter_long_muons()
    kaons = _filter_long_kaons()
    lambdas = make_lambdadd()
    return ParticleCombiner(
        [lambdas, kaons, muons],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- mu+]cc",
        name="Charm_CBaryonToSl_Xic0ToL0KDDMuFromBeauty",
        Combination12Cut=F.require_all(
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 2600 * MeV),
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_xic0omegac0_to_ximlllmu_from_beauty():
    """Return a Xic0 -> Xim mu decay maker. Xic0 is from beauty decays. Xim is lll."""
    muons = _filter_long_muons()
    pions = _filter_long_pions_from_xi()
    lambdas = make_lambdall()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions, ll=True)
    return ParticleCombiner(
        [xim, muons],
        DecayDescriptor="[Xi_c0 -> Xi- mu+]cc",
        name="Charm_CBaryonToSl_Xic0Omc0ToXimLLLMuFromBeauty",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 3000 * MeV),
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_xic0omegac0_to_ximddlmu_from_beauty():
    """Return a Xic0 -> Xim mu decay maker. Xic0 is from beauty decays. Xim is ddl."""
    muons = _filter_long_muons()
    pions = _filter_long_pions_from_xi()
    lambdas = make_lambdadd()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions, ll=False)
    return ParticleCombiner(
        [xim, muons],
        DecayDescriptor="[Xi_c0 -> Xi- mu+]cc",
        name="Charm_CBaryonToSl_Xic0Omc0ToXimDDLMuFromBeauty",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 3000 * MeV),
            F.MAXSDOCACUT(0.3 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_xic0omegac0_to_ximdddmu_from_beauty():
    """Return a Xic0 -> Xim mu decay maker. Xic0 is from beauty decays. Xim is ddd."""
    muons = _filter_long_muons()
    pions = _filter_down_pions_from_xi()
    lambdas = make_lambdadd()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions, ll=False)
    return ParticleCombiner(
        [xim, muons],
        DecayDescriptor="[Xi_c0 -> Xi- mu+]cc",
        name="Charm_CBaryonToSl_Xic0Omc0ToXimDDDMuFromBeauty",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 3000 * MeV),
            F.MAXSDOCACUT(0.3 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_omegac0_to_omegamlllmu_from_beauty():
    """Return a Omegac0 -> Omegam mu decay maker. Omegac0 is from beauty decays. Omega is lll."""
    muons = _filter_long_muons()
    kaons = _filter_long_kaons_from_omega()
    lambdas = make_lambdall()
    omegam = make_omegam_to_lambdak(lambdas=lambdas, kaons=kaons, ll=True)
    return ParticleCombiner(
        [omegam, muons],
        DecayDescriptor="[Omega_c0 -> Omega- mu+]cc",
        name="Charm_CBaryonToSl_Omc0ToOmmLLLMuFromBeauty",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1800 * MeV, F.MASS, 3000 * MeV),
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.3 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_omegac0_to_omegamddlmu_from_beauty():
    """Return a Omegac0 -> Omegam mu decay maker. Omegac0 is from beauty decays. Omega is ddl."""
    muons = _filter_long_muons()
    kaons = _filter_long_kaons_from_omega()
    lambdas = make_lambdadd()
    omegam = make_omegam_to_lambdak(lambdas=lambdas, kaons=kaons, ll=False)
    return ParticleCombiner(
        [omegam, muons],
        DecayDescriptor="[Omega_c0 -> Omega- mu+]cc",
        name="Charm_CBaryonToSl_Omc0ToOmmDDLMuFromBeauty",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 3000 * MeV),
            F.MAXSDOCACUT(0.3 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.3 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_omegac0_to_omegamdddmu_from_beauty():
    """Return a Omegac0 -> Omegam mu decay maker. Omegac0 is from beauty decays."""
    muons = _filter_long_muons()
    kaons = _filter_down_kaons_from_omega()
    lambdas = make_lambdadd()
    omegam = make_omegam_to_lambdak(lambdas=lambdas, kaons=kaons, ll=False)
    return ParticleCombiner(
        [omegam, muons],
        DecayDescriptor="[Omega_c0 -> Omega- mu+]cc",
        name="Charm_CBaryonToSl_Omc0ToOmmDDDMuFromBeauty",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 3000 * MeV),
            F.MAXSDOCACUT(0.3 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.3 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


###############################################################################
# charm baryon NC combiners
###############################################################################


def make_xic0_to_lambda0pillpi_from_beauty():
    """Return a Xic0 -> lambda0 pi pi decay maker. Xic0 is from beauty decays. lambda0 is ll."""
    pions = _filter_long_pions()
    lambdas = make_lambdall()
    return ParticleCombiner(
        [lambdas, pions, pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 pi- pi+]cc",
        name="Charm_CBaryonToSl_Xic0ToL0piLLPiFromBeauty",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 2600 * MeV),
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 10.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_xic0_to_lambda0piddpi_from_beauty():
    """Return a Xic0 -> lambda0 pi pi decay maker. Xic0 is from beauty decays. lambda0 is dd."""
    pions = _filter_long_pions()
    lambdas = make_lambdadd()
    return ParticleCombiner(
        [lambdas, pions, pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 pi- pi+]cc",
        name="Charm_CBaryonToSl_Xic0ToL0piDDPiFromBeauty",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 2600 * MeV),
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 10.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_xic0_to_lambda0kllpi_from_beauty():
    """Return a Xic0 -> lambda0 k pi decay maker. Xic0 is from beauty decays. lambda0 is ll."""
    kaons = _filter_long_kaons()
    pions = _filter_long_pions()
    lambdas = make_lambdall()
    return ParticleCombiner(
        [lambdas, kaons, pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- pi+]cc",
        name="Charm_CBaryonToSl_Xic0ToL0KLLPiFromBeauty",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 2600 * MeV),
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_xic0_to_lambda0kddpi_from_beauty():
    """Return a Xic0 -> lambda0 k pi decay maker. Xic0 is from beauty decays. lambda0 is dd."""
    kaons = _filter_long_kaons()
    pions = _filter_long_pions()
    lambdas = make_lambdadd()
    return ParticleCombiner(
        [lambdas, kaons, pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- pi+]cc",
        name="Charm_CBaryonToSl_Xic0ToL0KDDPiFromBeauty",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 2600 * MeV),
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_xic0omegac0_to_ximlllpi_from_beauty(decay_descriptor):
    """Return a Xic0 -> Xim pi decay maker. Xic0 is from beauty decays. Xim is lll."""
    pions = _filter_long_pions()
    pions_xi = _filter_long_pions_from_xi()
    lambdas = make_lambdall()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions_xi, ll=True)
    return ParticleCombiner(
        [xim, pions],
        DecayDescriptor=decay_descriptor,
        name="Charm_CBaryonToSl_Xic0Omc0ToXimLLLPiFromBeauty_{hash}",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 3000 * MeV),
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_xic0omegac0_to_ximddlpi_from_beauty(decay_descriptor):
    """Return a Xic0 -> Xim pi decay maker. Xic0 is from beauty decays. Xim is ddl."""
    pions = _filter_long_pions()
    pions_xi = _filter_long_pions_from_xi()
    lambdas = make_lambdadd()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions_xi, ll=False)
    return ParticleCombiner(
        [xim, pions],
        DecayDescriptor=decay_descriptor,
        name="Charm_CBaryonToSl_Xic0Omc0ToXimDDLPiFromBeauty_{hash}",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 3000 * MeV),
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_xic0omegac0_to_ximdddpi_from_beauty(decay_descriptor):
    """Return a Xic0 -> Xim pi decay maker. Xic0 is from beauty decays. Xim is ddd."""
    pions = _filter_long_pions()
    pions_xi = _filter_down_pions_from_xi()
    lambdas = make_lambdadd()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions_xi, ll=False)
    return ParticleCombiner(
        [xim, pions],
        DecayDescriptor=decay_descriptor,
        name="Charm_CBaryonToSl_Xic0Omc0ToXimDDDPiFromBeauty_{hash}",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 3000 * MeV),
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_omegac0_to_omegamlllpi_from_beauty():
    """Return a Omegac0 -> Omegam pi decay maker. Omegac0 is from beauty decays. Omega is lll."""
    pions = _filter_long_pions()
    kaons = _filter_long_kaons_from_omega()
    lambdas = make_lambdall()
    omegam = make_omegam_to_lambdak(lambdas=lambdas, kaons=kaons, ll=True)
    return ParticleCombiner(
        [omegam, pions],
        DecayDescriptor="[Omega_c0 -> Omega- pi+]cc",
        name="Charm_CBaryonToSl_Omc0ToOmmLLLPiFromBeauty",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 3000 * MeV),
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.3 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_omegac0_to_omegamddlpi_from_beauty():
    """Return a Omegac0 -> Omegam pi decay maker. Omegac0 is from beauty decays. Omega is ddl."""
    pions = _filter_long_pions()
    kaons = _filter_long_kaons_from_omega()
    lambdas = make_lambdadd()
    omegam = make_omegam_to_lambdak(lambdas=lambdas, kaons=kaons, ll=False)
    return ParticleCombiner(
        [omegam, pions],
        DecayDescriptor="[Omega_c0 -> Omega- pi+]cc",
        name="Charm_CBaryonToSl_Omc0ToOmmDDLPiFromBeauty",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 3000 * MeV),
            F.MAXSDOCACUT(0.3 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.3 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_omegac0_to_omegamdddpi_from_beauty():
    """Return a Omegac0 -> Omegam pi decay maker. Omegac0 is from beauty decays."""
    pions = _filter_long_pions()
    kaons = _filter_down_kaons_from_omega()
    lambdas = make_lambdadd()
    omegam = make_omegam_to_lambdak(lambdas=lambdas, kaons=kaons, ll=False)
    return ParticleCombiner(
        [omegam, pions],
        DecayDescriptor="[Omega_c0 -> Omega- pi+]cc",
        name="Charm_CBaryonToSl_Omc0ToOmmDDDPiFromBeauty",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 3000 * MeV),
            F.MAXSDOCACUT(0.3 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.3 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


###############################################################################
# charm baryon WS combiners
###############################################################################


def make_xic0_to_lambda0pillmu_ws_from_beauty():
    """Return a Xic0 -> lambda0 pi mu decay maker. Xic0 is from beauty decays. lambda0 is ll."""
    muons = _filter_long_muons()
    pions = _filter_long_pions()
    lambdas = make_lambdall()
    return ParticleCombiner(
        [lambdas, pions, muons],
        DecayDescriptor="[Xi_c0 -> Lambda0 pi- mu-]cc",
        name="Charm_CBaryonToSl_Xic0ToL0piLLMuWSFromBeauty",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 2600 * MeV),
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_xic0_to_lambda0piddmu_ws_from_beauty():
    """Return a Xic0 -> lambda0 pi mu decay maker. Xic0 is from beauty decays. lambda0 is dd."""
    muons = _filter_long_muons()
    pions = _filter_long_pions()
    lambdas = make_lambdadd()
    return ParticleCombiner(
        [lambdas, pions, muons],
        DecayDescriptor="[Xi_c0 -> Lambda0 pi- mu-]cc",
        name="Charm_CBaryonToSl_Xic0ToL0piDDMuWSFromBeauty",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 2600 * MeV),
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_xic0_to_lambda0kllmu_ws_from_beauty():
    """Return a Xic0 -> lambda0 k mu decay maker. Xic0 is from beauty decays. lambda0 is ll."""
    muons = _filter_long_muons()
    kaons = _filter_long_kaons()
    lambdas = make_lambdall()
    return ParticleCombiner(
        [lambdas, kaons, muons],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- mu-]cc",
        name="Charm_CBaryonToSl_Xic0ToL0KLLMuWSFromBeauty",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 2600 * MeV),
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 16 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_xic0_to_lambda0kddmu_ws_from_beauty():
    """Return a Xic0 -> lambda0 k mu decay maker. Xic0 is from beauty decays. lambda0 is dd."""
    muons = _filter_long_muons()
    kaons = _filter_long_kaons()
    lambdas = make_lambdadd()
    return ParticleCombiner(
        [lambdas, kaons, muons],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- mu-]cc",
        name="Charm_CBaryonToSl_Xic0ToL0KDDMuWSFromBeauty",
        Combination12Cut=F.MAXSDOCACUT(0.15 * mm),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 2600 * MeV),
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 16 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_xic0omegac0_to_ximlllmu_ws_from_beauty(decay_descriptor):
    """Return a Xic0 -> Xim mu decay maker. Xic0 is from beauty decays. Xim is lll."""
    muons = _filter_long_muons()
    pions = _filter_long_pions_from_xi()
    lambdas = make_lambdall()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions, ll=True)
    return ParticleCombiner(
        [xim, muons],
        DecayDescriptor=decay_descriptor,
        name="Charm_CBaryonToSl_Xic0Omc0ToXimLLLMuWSFromBeauty_{hash}",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 3000 * MeV),
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_xic0omegac0_to_ximddlmu_ws_from_beauty(decay_descriptor):
    """Return a Xic0 -> Xim mu decay maker. Xic0 is from beauty decays. Xim is ddl."""
    muons = _filter_long_muons()
    pions = _filter_long_pions_from_xi()
    lambdas = make_lambdadd()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions, ll=False)
    return ParticleCombiner(
        [xim, muons],
        DecayDescriptor=decay_descriptor,
        name="Charm_CBaryonToSl_Xic0Omc0ToXimDDLMuWSFromBeauty_{hash}",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 3000 * MeV),
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_xic0omegac0_to_ximdddmu_ws_from_beauty(decay_descriptor):
    """Return a Xic0 -> Xim mu decay maker. Xic0 is from beauty decays. Xim is ddd."""
    muons = _filter_long_muons()
    pions = _filter_down_pions_from_xi()
    lambdas = make_lambdadd()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions, ll=False)
    return ParticleCombiner(
        [xim, muons],
        DecayDescriptor=decay_descriptor,
        name="Charm_CBaryonToSl_Xic0Omc0ToXimDDDMuWSFromBeauty_{hash}",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 3000 * MeV),
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_omegac0_to_omegamlllmu_ws_from_beauty():
    """Return a Omegac0 -> Omegam mu decay maker. Omegac0 is from beauty decays. Omega is lll."""
    muons = _filter_long_muons()
    kaons = _filter_long_kaons_from_omega()
    lambdas = make_lambdall()
    omegam = make_omegam_to_lambdak(lambdas=lambdas, kaons=kaons, ll=True)
    return ParticleCombiner(
        [omegam, muons],
        DecayDescriptor="[Omega_c0 -> Omega- mu-]cc",
        name="Charm_CBaryonToSl_Omc0ToOmmLLLMuWSFromBeauty",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 3000 * MeV),
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.3 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_omegac0_to_omegamddlmu_ws_from_beauty():
    """Return a Omegac0 -> Omegam mu decay maker. Omegac0 is from beauty decays. Omega is ddl."""
    muons = _filter_long_muons()
    kaons = _filter_long_kaons_from_omega()
    lambdas = make_lambdadd()
    omegam = make_omegam_to_lambdak(lambdas=lambdas, kaons=kaons, ll=False)
    return ParticleCombiner(
        [omegam, muons],
        DecayDescriptor="[Omega_c0 -> Omega- mu-]cc",
        name="Charm_CBaryonToSl_Omc0ToOmmDDLMuWSFromBeauty",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 3000 * MeV),
            F.MAXSDOCACUT(0.3 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.3 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


def make_omegac0_to_omegamdddmu_ws_from_beauty():
    """Return a Omegac0 -> Omegam mu decay maker. Omegac0 is from beauty decays."""
    muons = _filter_long_muons()
    kaons = _filter_down_kaons_from_omega()
    lambdas = make_lambdadd()
    omegam = make_omegam_to_lambdak(lambdas=lambdas, kaons=kaons, ll=False)
    return ParticleCombiner(
        [omegam, muons],
        DecayDescriptor="[Omega_c0 -> Omega- mu-]cc",
        name="Charm_CBaryonToSl_Omc0ToOmmDDDMuWSFromBeauty",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            in_range(1500 * MeV, F.MASS, 3000 * MeV),
            F.MAXSDOCACUT(0.3 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 10.0,
            F.OWNPVVDZ > -0.3 * mm,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )


###############################################################################
# Lines definitions
###############################################################################

###############################################################################
# SL Lines
###############################################################################


@register_line_builder(all_lines)
def lambdacpxicp_to_lambda0llmu_line(name="Hlt2Charm_LcpXicpToL0MupNu_LL", prescale=1):
    muons = _filter_long_muons()
    lambdas = make_lambdall()
    line_alg = ParticleCombiner(
        [lambdas, muons],
        DecayDescriptor="[Lambda_c+ -> Lambda0 mu+]cc",
        name="Charm_CBaryonToSl_LcpXicpToLmdLLMu",
        CombinationCut=F.require_all(
            F.PT > 1 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 16 * GeV,
            F.CHI2DOF < 10.0,
            F.MASS < 2.8 * GeV,
            _DZ_CHILD(1) > 8 * mm,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVFDCHI2 > 15.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lambdas, line_alg], prescale=prescale
    )


@register_line_builder(all_lines)
def lambdacpxicp_to_lambda0ddmu_line(name="Hlt2Charm_LcpXicpToL0MupNu_DD", prescale=1):
    muons = _filter_tight_long_muons()
    lambdas = make_lambdadd()
    line_alg = ParticleCombiner(
        [lambdas, muons],
        DecayDescriptor="[Lambda_c+ -> Lambda0 mu+]cc",
        name="Charm_CBaryonToSl_LcpXicpToLmdDDMu",
        CombinationCut=F.require_all(
            F.PT > 1 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 16 * GeV,
            F.CHI2DOF < 10.0,
            F.MASS < 2.8 * GeV,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 15.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lambdas, line_alg], prescale=prescale
    )


@register_line_builder(all_lines)
def lambdacp_to_pkmu_line(name="Hlt2Charm_LcpXicpToPpKmMupNu", prescale=1):
    muons = _filter_tight_long_muons()
    kaons = _filter_long_kaons()
    protons = _filter_long_protons()
    line_alg = ParticleCombiner(
        [muons, protons, kaons],
        DecayDescriptor="[Lambda_c+ -> mu+ p+ K-]cc",
        name="Charm_CBaryonToSl_LcToPKMu",
        CombinationCut=F.require_all(
            F.PT > 1 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 16 * GeV,
            F.CHI2DOF < 6.0,
            F.MASS < 2.8 * GeV,
            F.OWNPVVDZ > 0.0 * mm,
            F.OWNPVFDCHI2 > 15.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
def lambdacp_to_ppimu_line(name="Hlt2Charm_LcpXicpToPpPimMupNu", prescale=1):
    muons = _filter_tight_long_muons()
    pions = _filter_long_pions()
    protons = _filter_long_protons()
    line_alg = ParticleCombiner(
        [muons, protons, pions],
        DecayDescriptor="[Lambda_c+ -> mu+ p+ pi-]cc",
        name="Charm_CBaryonToSl_LcToPPiMu",
        Combination12Cut=F.MAXSDOCACUT(
            0.15 * mm,
        ),
        CombinationCut=F.require_all(
            F.PT > 1.4 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 4.0,
            F.MASS < 2.8 * GeV,
            F.OWNPVVDZ > 0.2 * mm,
            F.OWNPVFDCHI2 > 18.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
def omegac0_to_omegamlllmu_line(name="Hlt2Charm_Oc0ToOmMupNu_LLL", prescale=1):
    muons = _filter_long_muons()
    kaons = _filter_long_kaons_from_omega()
    lambdas = make_lambdall()
    omegam = make_omegam_to_lambdak(lambdas=lambdas, kaons=kaons, ll=True)
    line_alg = ParticleCombiner(
        [omegam, muons],
        DecayDescriptor="[Omega_c0 -> Omega- mu+]cc",
        name="Charm_CBaryonToSl_Omc0ToOmmLLLMu",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 3 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > -0.3 * mm,
            F.CHI2DOF < 10.0,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdas, omegam, line_alg],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def omegac0_to_omegamddlmu_line(name="Hlt2Charm_Oc0ToOmMupNu_DDL", prescale=1):
    muons = _filter_long_muons()
    kaons = _filter_long_kaons_from_omega()
    lambdas = make_lambdadd()
    omegam = make_omegam_to_lambdak(lambdas=lambdas, kaons=kaons, ll=False)
    line_alg = ParticleCombiner(
        [omegam, muons],
        DecayDescriptor="[Omega_c0 -> Omega- mu+]cc",
        name="Charm_CBaryonToSl_Omc0ToOmmDDLMu",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.3 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 3 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > -0.3 * mm,
            F.CHI2DOF < 10.0,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdas, omegam, line_alg],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def omegac0_to_omegamdddmu_line(name="Hlt2Charm_Oc0ToOmMupNu_DDD", prescale=1):
    muons = _filter_long_muons()
    kaons = _filter_down_kaons_from_omega()
    lambdas = make_lambdall()
    omegam = make_omegam_to_lambdak(lambdas=lambdas, kaons=kaons, ll=True)
    line_alg = ParticleCombiner(
        [omegam, muons],
        DecayDescriptor="[Omega_c0 -> Omega- mu+]cc",
        name="Charm_CBaryonToSl_Omc0ToOmmDDDMu",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 3 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > -0.3 * mm,
            F.CHI2DOF < 10.0,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdas, omegam, line_alg],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def xic0omegac0_to_ximlllmu_line(name="Hlt2Charm_Xic0Oc0ToXimMupNu_LLL", prescale=1):
    muons = _filter_long_muons()
    pions = _filter_long_pions_from_xi()
    lambdas = make_lambdall()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions, ll=True)
    line_alg = ParticleCombiner(
        [xim, muons],
        DecayDescriptor="[Xi_c0 -> Xi- mu+]cc",
        name="Charm_CBaryonToSl_Xic0Omc0ToXimLLLMu",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 3 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > -0.5 * mm,
            F.CHI2DOF < 10.0,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lambdas, xim, line_alg], prescale=prescale
    )


@register_line_builder(all_lines)
def xic0omegac0_to_ximddlmu_line(name="Hlt2Charm_Xic0Oc0ToXimMupNu_DDL", prescale=1):
    muons = _filter_long_muons()
    pions = _filter_long_pions_from_xi()
    lambdas = make_lambdadd()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions, ll=False)
    line_alg = ParticleCombiner(
        [xim, muons],
        DecayDescriptor="[Xi_c0 -> Xi- mu+]cc",
        name="Charm_CBaryonToSl_Xic0Omc0ToXimDDLMu",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.3 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 3 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > -0.5 * mm,
            F.CHI2DOF < 10.0,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lambdas, xim, line_alg], prescale=prescale
    )


@register_line_builder(all_lines)
def xicp_to_ximlllpipmu_line(name="Hlt2Charm_XicpToXimPipMupNu_LLL", prescale=1):
    muons = _filter_long_muons()
    pions = _filter_long_pions()
    lambdas = make_lambdall()
    pions_from_xi = _filter_long_pions_from_xi()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions_from_xi, ll=True)
    line_alg = ParticleCombiner(
        [xim, pions, muons],
        DecayDescriptor="[Xi_c+ -> Xi- pi+ mu+]cc",
        name="Charm_CBaryonToSl_XicpToXimLLLPipMu",
        Combination12Cut=F.MAXSDOCACUT(
            0.2 * mm,
        ),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 2.8 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 0.0 * mm,
            F.CHI2DOF < 6.0,
            F.OWNPVFDCHI2 > 9.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lambdas, xim, line_alg], prescale=prescale
    )


@register_line_builder(all_lines)
def xicp_to_ximddlpipmu_line(name="Hlt2Charm_XicpToXimPipMupNu_DDL", prescale=1):
    muons = _filter_long_muons()
    pions = _filter_long_pions()
    lambdas = make_lambdadd()
    pions_from_xi = _filter_long_pions_from_xi()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions_from_xi, ll=False)
    line_alg = ParticleCombiner(
        [xim, pions, muons],
        DecayDescriptor="[Xi_c+ -> Xi- pi+ mu+ ]cc",
        name="Charm_CBaryonToSl_XicpToXimDDLPipMu",
        Combination12Cut=F.MAXSDOCACUT(
            0.25 * mm,
        ),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.25 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 2.8 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > -0.2 * mm,
            F.CHI2DOF < 6.0,
            F.OWNPVFDCHI2 > 9.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lambdas, xim, line_alg], prescale=prescale
    )


@register_line_builder(all_lines)
def xic0_to_lambda0llkmmu_line(name="Hlt2Charm_Xic0Oc0ToL0KmMupNu_LL", prescale=1):
    muons = _filter_long_muons()
    kaons = _filter_long_kaons()
    lambdas = make_lambdall()
    line_alg = ParticleCombiner(
        [lambdas, kaons, muons],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- mu+]cc",
        name="Charm_CBaryonToSl_Xic0ToLmdLLKMu",
        Combination12Cut=F.MAXSDOCACUT(
            0.2 * mm,
        ),
        CombinationCut=F.require_all(
            F.PT > 1.6 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 3 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 8 * mm,
            F.OWNPVVDZ > -0.3 * mm,
            F.CHI2DOF < 6.0,
            F.OWNPVFDCHI2 > 9.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lambdas, line_alg], prescale=prescale
    )


@register_line_builder(all_lines)
def xic0_to_lambda0ddkmmu_line(name="Hlt2Charm_Xic0Oc0ToL0KmMupNu_DD", prescale=1):
    muons = _filter_long_muons()
    kaons = _filter_long_kaons()
    lambdas = make_lambdadd()
    line_alg = ParticleCombiner(
        [lambdas, muons, kaons],
        DecayDescriptor="[Xi_c0 -> Lambda0 mu+ K-]cc",
        name="Charm_CBaryonToSl_Xic0ToLmdDDKMu",
        Combination12Cut=F.MAXSDOCACUT(
            2 * mm,
        ),
        CombinationCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.SDOCA(1, 3) < 2 * mm,
            F.SDOCA(2, 3)
            < 0.3
            * mm,  # for faster calculation, use SDOCA when L0, KS are not involved
        ),
        CompositeCut=F.require_all(
            F.MASS < 3 * GeV,
            F.P > 18 * GeV,
            F.OWNPVVDZ > -0.4 * mm,
            F.CHI2DOF < 6.0,
            F.OWNPVFDCHI2 > 9.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lambdas, line_alg], prescale=prescale
    )


###############################################################################
# SL WS Lines
###############################################################################


@register_line_builder(all_lines)
def lambdacpxicp_to_lambda0llmu_ws_line(
    name="Hlt2Charm_LcpXicpToL0MupNu_LL_WS", prescale=1
):
    muons = _filter_long_muons()
    lambdas = make_lambdall()
    line_alg = ParticleCombiner(
        [lambdas, muons],
        DecayDescriptor="[Lambda_c+ -> Lambda0 mu-]cc",
        name="Charm_CBaryonToSl_LcpXicpToLmdLLMu_WS",
        CombinationCut=F.require_all(
            F.PT > 1 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 16 * GeV,
            F.CHI2DOF < 10.0,
            F.MASS < 2.8 * GeV,
            _DZ_CHILD(1) > 8 * mm,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVFDCHI2 > 15.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lambdas, line_alg], prescale=prescale
    )


@register_line_builder(all_lines)
def lambdacpxicp_to_lambda0ddmu_ws_line(
    name="Hlt2Charm_LcpXicpToL0MupNu_DD_WS", prescale=1
):
    muons = _filter_tight_long_muons()
    lambdas = make_lambdadd()
    line_alg = ParticleCombiner(
        [lambdas, muons],
        DecayDescriptor="[Lambda_c+ -> Lambda0 mu-]cc",
        name="Charm_CBaryonToSl_LcpXicpToLmdDDMu_WS",
        CombinationCut=F.require_all(
            F.PT > 1 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 16 * GeV,
            F.CHI2DOF < 10.0,
            F.MASS < 2.8 * GeV,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVFDCHI2 > 15.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lambdas, line_alg], prescale=prescale
    )


@register_line_builder(all_lines)
def lambdacp_to_pkmu_ws_line(name="Hlt2Charm_LcpXicpToPpKmMupNu_WS", prescale=1):
    muons = _filter_tight_long_muons()
    kaons = _filter_long_kaons()
    protons = _filter_long_protons()
    line_alg = ParticleCombiner(
        [muons, protons, kaons],
        DecayDescriptor="[Lambda_c+ -> mu- p+ K-]cc",
        name="Charm_CBaryonToSl_LcToPKMu_WS",
        CombinationCut=F.require_all(
            F.PT > 1 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 16 * GeV,
            F.CHI2DOF < 6.0,
            F.MASS < 2.8 * GeV,
            F.OWNPVVDZ > 0.0 * mm,
            F.OWNPVFDCHI2 > 15.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
def lambdacp_to_ppimu_ws_line(name="Hlt2Charm_LcpXicpToPpPimMupNu_WS", prescale=1):
    muons = _filter_tight_long_muons()
    pions = _filter_long_pions()
    protons = _filter_long_protons()
    line_alg = ParticleCombiner(
        [muons, protons, pions],
        DecayDescriptor="[Lambda_c+ -> mu- p+ pi-]cc",
        name="Charm_CBaryonToSl_LcToPPiMu_WS",
        Combination12Cut=F.MAXSDOCACUT(
            0.15 * mm,
        ),
        CombinationCut=F.require_all(
            F.PT > 1.4 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.15 * mm),
        ),
        CompositeCut=F.require_all(
            F.P > 20 * GeV,
            F.CHI2DOF < 4.0,
            F.MASS < 2.8 * GeV,
            F.OWNPVVDZ > 0.2 * mm,
            F.OWNPVFDCHI2 > 18.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(all_lines)
def omegac0_to_omegamlllmu_ws_line(name="Hlt2Charm_Oc0ToOmMupNu_LLL_WS", prescale=1):
    muons = _filter_long_muons()
    kaons = _filter_long_kaons_from_omega()
    lambdas = make_lambdall()
    omegam = make_omegam_to_lambdak(lambdas=lambdas, kaons=kaons, ll=True)
    line_alg = ParticleCombiner(
        [omegam, muons],
        DecayDescriptor="[Omega_c0 -> Omega- mu-]cc",
        name="Charm_CBaryonToSl_Omc0ToOmmLLLMu_WS",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 3 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > -0.3 * mm,
            F.CHI2DOF < 10.0,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdas, omegam, line_alg],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def omegac0_to_omegamddlmu_ws_line(name="Hlt2Charm_Oc0ToOmMupNu_DDL_WS", prescale=1):
    muons = _filter_long_muons()
    kaons = _filter_long_kaons_from_omega()
    lambdas = make_lambdadd()
    omegam = make_omegam_to_lambdak(lambdas=lambdas, kaons=kaons, ll=False)
    line_alg = ParticleCombiner(
        [omegam, muons],
        DecayDescriptor="[Omega_c0 -> Omega- mu-]cc",
        name="Charm_CBaryonToSl_Omc0ToOmmDDLMu_WS",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.3 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 3 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > -0.3 * mm,
            F.CHI2DOF < 10.0,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdas, omegam, line_alg],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def omegac0_to_omegamdddmu_ws_line(name="Hlt2Charm_Oc0ToOmMupNu_DDD_WS", prescale=1):
    muons = _filter_long_muons()
    kaons = _filter_down_kaons_from_omega()
    lambdas = make_lambdall()
    omegam = make_omegam_to_lambdak(lambdas=lambdas, kaons=kaons, ll=True)
    line_alg = ParticleCombiner(
        [omegam, muons],
        DecayDescriptor="[Omega_c0 -> Omega- mu-]cc",
        name="Charm_CBaryonToSl_Omc0ToOmmDDDMu_WS",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 3 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > -0.3 * mm,
            F.CHI2DOF < 10.0,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdas, omegam, line_alg],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def xic0omegac0_to_ximlllmu_ws_line(
    name="Hlt2Charm_Xic0Oc0ToXimMupNu_LLL_WS", prescale=1
):
    muons = _filter_long_muons()
    pions = _filter_long_pions_from_xi()
    lambdas = make_lambdall()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions, ll=True)
    line_alg = ParticleCombiner(
        [xim, muons],
        DecayDescriptor="[Xi_c0 -> Xi- mu-]cc",
        name="Charm_CBaryonToSl_Xic0Omc0ToXimLLLMu_WS",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 3 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > -0.5 * mm,
            F.CHI2DOF < 10.0,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lambdas, xim, line_alg], prescale=prescale
    )


@register_line_builder(all_lines)
def xic0omegac0_to_ximddlmu_ws_line(
    name="Hlt2Charm_Xic0Oc0ToXimMupNu_DDL_WS", prescale=1
):
    muons = _filter_long_muons()
    pions = _filter_long_pions_from_xi()
    lambdas = make_lambdadd()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions, ll=False)
    line_alg = ParticleCombiner(
        [xim, muons],
        DecayDescriptor="[Xi_c0 -> Xi- mu-]cc",
        name="Charm_CBaryonToSl_Xic0Omc0ToXimDDLMu_WS",
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.3 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 3 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > -0.5 * mm,
            F.CHI2DOF < 10.0,
            F.OWNPVFDCHI2 > 6.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lambdas, xim, line_alg], prescale=prescale
    )


@register_line_builder(all_lines)
def xicp_to_ximlllpipmu_ws_line(name="Hlt2Charm_XicpToXimPipMupNu_LLL_WS", prescale=1):
    muons = _filter_long_muons()
    pions = _filter_long_pions()
    lambdas = make_lambdall()
    pions_from_xi = _filter_long_pions_from_xi()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions_from_xi, ll=True)
    line_alg = ParticleCombiner(
        [xim, pions, muons],
        DecayDescriptor="[Xi_c+ -> Xi- pi+ mu-]cc",
        name="Charm_CBaryonToSl_XicpToXimLLLPipMu_WS",
        Combination12Cut=F.MAXSDOCACUT(
            0.2 * mm,
        ),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 2.8 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 0.0 * mm,
            F.CHI2DOF < 6.0,
            F.OWNPVFDCHI2 > 9.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lambdas, xim, line_alg], prescale=prescale
    )


@register_line_builder(all_lines)
def xicp_to_ximddlpipmu_ws_line(name="Hlt2Charm_XicpToXimPipMupNu_DDL_WS", prescale=1):
    muons = _filter_long_muons()
    pions = _filter_long_pions()
    lambdas = make_lambdadd()
    pions_from_xi = _filter_long_pions_from_xi()
    xim = make_xim_to_lambdapi(lambdas=lambdas, pions=pions_from_xi, ll=False)
    line_alg = ParticleCombiner(
        [xim, pions, muons],
        DecayDescriptor="[Xi_c+ -> Xi- pi+ mu-]cc",
        name="Charm_CBaryonToSl_XicpToXimDDLPipMu_WS",
        Combination12Cut=F.MAXSDOCACUT(
            0.25 * mm,
        ),
        CombinationCut=F.require_all(
            F.PT > 1.2 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.25 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 2.8 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > -0.2 * mm,
            F.CHI2DOF < 6.0,
            F.OWNPVFDCHI2 > 9.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lambdas, xim, line_alg], prescale=prescale
    )


@register_line_builder(all_lines)
def xic0_to_lambda0llkmmu_ws_line(
    name="Hlt2Charm_Xic0Oc0ToL0KmMupNu_LL_WS", prescale=1
):
    muons = _filter_long_muons()
    kaons = _filter_long_kaons()
    lambdas = make_lambdall()
    line_alg = ParticleCombiner(
        [lambdas, kaons, muons],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- mu-]cc",
        name="Charm_CBaryonToSl_Xic0ToLmdLLKMu_WS",
        Combination12Cut=F.MAXSDOCACUT(
            0.2 * mm,
        ),
        CombinationCut=F.require_all(
            F.PT > 1.6 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 3 * GeV,
            F.P > 16 * GeV,
            _DZ_CHILD(1) > 8 * mm,
            F.OWNPVVDZ > -0.3 * mm,
            F.CHI2DOF < 6.0,
            F.OWNPVFDCHI2 > 9.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lambdas, line_alg], prescale=prescale
    )


@register_line_builder(all_lines)
def xic0_to_lambda0ddkmmu_ws_line(
    name="Hlt2Charm_Xic0Oc0ToL0KmMupNu_DD_WS", prescale=1
):
    muons = _filter_long_muons()
    kaons = _filter_long_kaons()
    lambdas = make_lambdadd()
    line_alg = ParticleCombiner(
        [lambdas, muons, kaons],
        DecayDescriptor="[Xi_c0 -> Lambda0 mu- K-]cc",
        name="Charm_CBaryonToSl_Xic0ToLmdDDKMu_WS",
        Combination12Cut=F.MAXSDOCACUT(
            2 * mm,
        ),
        CombinationCut=F.require_all(
            F.PT > 1.8 * GeV,
            F.P > 15 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.SDOCA(1, 3) < 2 * mm,
            F.SDOCA(2, 3)
            < 0.3
            * mm,  # for faster calculation, use SDOCA when L0, KS are not involved
        ),
        CompositeCut=F.require_all(
            F.MASS < 3 * GeV,
            F.P > 18 * GeV,
            F.OWNPVVDZ > -0.4 * mm,
            F.CHI2DOF < 6.0,
            F.OWNPVFDCHI2 > 9.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lambdas, line_alg], prescale=prescale
    )


@register_line_builder(all_lines)
def lb0_to_lambdacppippimpim_lambdacpkmmu_line(
    name="Hlt2Charm_Lb0ToLcpPipPimPim_LcpToPpKmMupNu", prescale=1
):
    pions = _filter_long_pions()
    lambdacs = make_lambdacp_to_pKmu_from_beauty()
    line_alg = ParticleCombiner(
        [lambdacs, pions, pions, pions],
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_Lb0ToLcpPipPimPim_LcpToPpKmMupNu",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6200 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.CHI2DOF < 6.0,
            F.SUM(F.PT) > 2.5 * GeV,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdacs, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def lb0_to_lambdacppimpimpip_lambdacpkmpip_line(
    name="Hlt2Charm_Lb0ToLcpPimPimPip_LcpToPpKmPip", prescale=1
):
    pions = _filter_long_pions()
    lambdacs = make_lambdacp_to_pKpi_from_beauty()
    line_alg = ParticleCombiner(
        [lambdacs, pions, pions, pions],
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_Lb0ToLcpPipPimPim_LcpToPpKmPip",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(5400 * MeV, F.MASS, 6000 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.CHI2DOF < 6.0,
            F.SUM(F.PT) > 2.5 * GeV,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdacs, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def lb0_to_lambdacppimpimpip_lambdacpkmmu_ws_line(
    name="Hlt2Charm_Lb0ToLcpPimPimPip_LcpToPpKmMumNu_WS", prescale=1
):
    pions = _filter_long_pions()
    lambdacs = make_lambdacp_to_pKmu_from_beauty("[Lambda_c+ -> p+ K- mu-]cc")
    line_alg = ParticleCombiner(
        [lambdacs, pions, pions, pions],
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_Lb0ToLcpPipPimPim_LcpToPpKmMumNu",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6200 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.CHI2DOF < 6.0,
            F.SUM(F.PT) > 2.5 * GeV,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdacs, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


def lb0_to_lambdacppimpimpim_ws_lambdacpkmmu_line(
    name="Hlt2Charm_Lb0ToLcpPimPimPim_LcpToPpKmMupNu_WS", prescale=1
):
    pions = _filter_long_pions()
    lambdacs = make_lambdacp_to_pKmu_from_beauty()
    line_alg = ParticleCombiner(
        [lambdacs, pions, pions, pions],
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi- pi- pi-]cc",
        name="Charm_CBaryonToSl_Lb0ToLcpPimPimPim_LcpToPpKmMupNu",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6200 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdacs, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def lb0_to_lambdacppimpimpip_lambdacppimmu_line(
    name="Hlt2Charm_Lb0ToLcpPimPimPip_LcpToPpPimMupNu", prescale=1
):
    pions = _filter_long_pions()
    lambdacs = make_lambdacp_to_ppimu_from_beauty()
    line_alg = ParticleCombiner(
        [lambdacs, pions, pions, pions],
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_Lb0ToLcpPipPimPim_LcpToPpPimMupNu",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(3600 * MeV, F.MASS, 6100 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 30 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdacs, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def lb0_to_lambdacppimpimpip_lambdacppimpip_line(
    name="Hlt2Charm_Lb0ToLcpPimPimPip_LcpToPpPimPip", prescale=1
):
    pions = _filter_long_pions()
    lambdacs = make_lambdacp_to_ppipi_from_beauty()
    line_alg = ParticleCombiner(
        [lambdacs, pions, pions, pions],
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_Lb0ToLcpPipPimPim_LcpToPpPimPip",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(5400 * MeV, F.MASS, 6000 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 30 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdacs, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def lb0_to_lambdacppimpimpip_lambdacppimmu_ws_line(
    name="Hlt2Charm_Lb0ToLcpPimPimPip_LcpToPpPimMumNu_WS", prescale=1
):
    pions = _filter_long_pions()
    lambdacs = make_lambdacp_to_ppimu_from_beauty("[Lambda_c+ -> p+ pi- mu-]cc")
    line_alg = ParticleCombiner(
        [lambdacs, pions, pions, pions],
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_Lb0ToLcpPipPimPim_LcpToPpPimMumNu",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(3600 * MeV, F.MASS, 6100 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdacs, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def lb0_to_lambdacppimpimpim_ws_lambdacppimmu_line(
    name="Hlt2Charm_Lb0ToLcpPimPimPim_LcpToPpPimMupNu_WS", prescale=1
):
    pions = _filter_long_pions()
    lambdacs = make_lambdacp_to_ppimu_from_beauty()
    line_alg = ParticleCombiner(
        [lambdacs, pions, pions, pions],
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi- pi- pi-]cc",
        name="Charm_CBaryonToSl_Lb0ToLcpPimPimPim_LcpToPpPimMupNu",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(3600 * MeV, F.MASS, 6100 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdacs, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0L0Pimmu_ll_line(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0PimMupNu_LL", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0_to_lambda0pillmu_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToL0PimMupNu_LL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def lb0_to_lambdacppimpimpip_lambdaclambdamu_ll_line(
    name="Hlt2Charm_Lb0ToLcpPimPimPip_LcpToL0MupNu_LL", prescale=1
):
    pions = _filter_long_pions()
    lambdacs = make_lambdacpxicp_to_lambdamu_ll_from_beauty(
        "[Lambda_c+ -> Lambda0 mu+]cc", 2350
    )
    line_alg = ParticleCombiner(
        [lambdacs, pions, pions, pions],
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_Lb0ToLcpPipPimPim_LcpToL0MupNu_LL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(3600 * MeV, F.MASS, 6200 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdacs, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0L0Pimmu_dd_line(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0PimMupNu_DD", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0_to_lambda0piddmu_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToL0PimMupNu_DD",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def lb0_to_lambdacppimpimpip_lambdaclambdapip_ll_line(
    name="Hlt2Charm_Lb0ToLcpPimPimPip_LcpToL0Pip_LL", prescale=1
):
    pions = _filter_long_pions()
    lambdacs = make_lambdacpxicp_to_lambdapi_ll_from_beauty(
        "[Lambda_c+ -> Lambda0 pi+]cc", 2200, 2350
    )
    line_alg = ParticleCombiner(
        [lambdacs, pions, pions, pions],
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_Lb0ToLcpPipPimPim_LcpToL0Pip_LL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(5400 * MeV, F.MASS, 6000 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdacs, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0L0Kmmu_ll_line(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0KmMupNu_LL", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0_to_lambda0kllmu_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToL0KmMupNu_LL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def lb0_to_lambdacppimpimpip_lambdaclambdamu_ll_ws_line(
    name="Hlt2Charm_Lb0ToLcpPimPimPip_LcpToL0MumNu_LL_WS", prescale=1
):
    pions = _filter_long_pions()
    lambdacs = make_lambdacpxicp_to_lambdamu_ll_from_beauty(
        "[Lambda_c+ -> Lambda0 mu-]cc", 2350
    )
    line_alg = ParticleCombiner(
        [lambdacs, pions, pions, pions],
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_Lb0ToLcpPipPimPim_LcpToL0MumNu_LL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(3600 * MeV, F.MASS, 6200 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdacs, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def lb0_to_lambdacppimpimpim_ws_lambdaclambdamu_ll_line(
    name="Hlt2Charm_Lb0ToLcpPimPimPim_LcpToL0MupNu_LL_WS", prescale=1
):
    pions = _filter_long_pions()
    lambdacs = make_lambdacpxicp_to_lambdamu_ll_from_beauty(
        "[Lambda_c+ -> Lambda0 mu+]cc", 2350
    )
    line_alg = ParticleCombiner(
        [lambdacs, pions, pions, pions],
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi- pi- pi-]cc",
        name="Charm_CBaryonToSl_Lb0ToLcpPimPimPim_LcpToL0MupNu_LL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(3600 * MeV, F.MASS, 6200 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdacs, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0L0Kmmu_dd_line(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0KmMupNu_DD", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0_to_lambda0kddmu_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToL0KmMupNu_DD",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def lb0_to_lambdacppimpimpip_lambdaclambdamu_dd_line(
    name="Hlt2Charm_Lb0ToLcpPimPimPip_LcpToL0MupNu_DD", prescale=1
):
    pions = _filter_long_pions()
    lambdacs = make_lambdacpxicp_to_lambdamu_dd_from_beauty(
        "[Lambda_c+ -> Lambda0 mu+]cc", 2350
    )
    line_alg = ParticleCombiner(
        [lambdacs, pions, pions, pions],
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_Lb0ToLcpPipPimPim_LcpToL0MupNu_DD",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(3600 * MeV, F.MASS, 6200 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdacs, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def lb0_to_lambdacppimpimpip_lambdaclambdapip_dd_line(
    name="Hlt2Charm_Lb0ToLcpPimPimPip_LcpToL0Pip_DD", prescale=1
):
    pions = _filter_long_pions()
    lambdacs = make_lambdacpxicp_to_lambdapi_dd_from_beauty(
        "[Lambda_c+ -> Lambda0 pi+]cc", 2200, 2350
    )
    line_alg = ParticleCombiner(
        [lambdacs, pions, pions, pions],
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_Lb0ToLcpPipPimPim_LcpToL0Pip_DD",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(5400 * MeV, F.MASS, 6000 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdacs, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0Ximmu_lll_line(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToXimMupNu_LLL", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0omegac0_to_ximlllmu_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToXimMupNu_LLL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def lb0_to_lambdacppimpimpip_lambdaclambdamu_dd_ws_line(
    name="Hlt2Charm_Lb0ToLcpPimPimPip_LcpToL0MumNu_DD", prescale=1
):
    pions = _filter_long_pions()
    lambdacs = make_lambdacpxicp_to_lambdamu_dd_from_beauty(
        "[Lambda_c+ -> Lambda0 mu-]cc", 2350
    )
    line_alg = ParticleCombiner(
        [lambdacs, pions, pions, pions],
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_Lb0ToLcpPipPimPim_LcpToL0MumNu_DD",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(3600 * MeV, F.MASS, 6200 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdacs, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def lb0_to_lambdacppimpimpim_ws_lambdaclambdamu_dd_line(
    name="Hlt2Charm_Lb0ToLcpPimPimPim_LcpToL0MupNu_DD_WS", prescale=1
):
    pions = _filter_long_pions()
    lambdacs = make_lambdacpxicp_to_lambdamu_dd_from_beauty(
        "[Lambda_c+ -> Lambda0 mu+]cc", 2350
    )
    line_alg = ParticleCombiner(
        [lambdacs, pions, pions, pions],
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi- pi- pi-]cc",
        name="Charm_CBaryonToSl_Lb0ToLcpPimPimPim_LcpToL0MupNu_DD",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(3600 * MeV, F.MASS, 6200 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lambdacs, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def xib0_to_xicppimpimpip_xicplambdamu_ll_line(
    name="Hlt2Charm_Xib0ToXicpPimPimPip_XicpToL0MupNu_LL", prescale=1
):
    pions = _filter_long_pions()
    xics = make_lambdacpxicp_to_lambdamu_ll_from_beauty(
        "[Xi_c+ -> Lambda0 mu+]cc", 2550
    )
    line_alg = ParticleCombiner(
        [xics, pions, pions, pions],
        DecayDescriptor="[Xi_b0 -> Xi_c+ pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_Xib0ToXicpPipPimPim_XipToL0MupNu_LL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(3600 * MeV, F.MASS, 6350 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xics, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0Ximmu_ddl_line(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToXimMupNu_DDL", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0omegac0_to_ximddlmu_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToXimMupNu_DDL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def xib0_to_xicppimpimpip_xicplambdapip_ll_line(
    name="Hlt2Charm_Xib0ToXicpPimPimPip_XicpToL0Pip_LL", prescale=1
):
    pions = _filter_long_pions()
    xics = make_lambdacpxicp_to_lambdapi_ll_from_beauty(
        "[Xi_c+ -> Lambda0 pi+]cc", 2350, 2550
    )
    line_alg = ParticleCombiner(
        [xics, pions, pions, pions],
        DecayDescriptor="[Xi_b0 -> Xi_c+ pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_Xib0ToXicpPipPimPim_XipToL0Pip_LL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(5650 * MeV, F.MASS, 6200 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xics, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0Ximmu_ddd_line(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToXimMupNu_DDD", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0omegac0_to_ximdddmu_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToXimMupNu_DDD",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def xib0_to_xicppimpimpip_xicplambdamu_ll_ws_line(
    name="Hlt2Charm_Xib0ToXicpPimPimPip_XicpToL0MumNu_LL_WS", prescale=1
):
    pions = _filter_long_pions()
    xics = make_lambdacpxicp_to_lambdamu_ll_from_beauty(
        "[Xi_c+ -> Lambda0 mu-]cc", 2550
    )
    line_alg = ParticleCombiner(
        [xics, pions, pions, pions],
        DecayDescriptor="[Xi_b0 -> Xi_c+ pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_Xib0ToXicpPipPimPim_XipToL0MumNu_LL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(3600 * MeV, F.MASS, 6350 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xics, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def xib0_to_xicppimpimpim_ws_xicplambdamu_ll_line(
    name="Hlt2Charm_Xib0ToXicpPimPimPim_XicpToL0MupNu_LL_WS", prescale=1
):
    pions = _filter_long_pions()
    xics = make_lambdacpxicp_to_lambdamu_ll_from_beauty(
        "[Xi_c+ -> Lambda0 mu+]cc", 2550
    )
    line_alg = ParticleCombiner(
        [xics, pions, pions, pions],
        DecayDescriptor="[Xi_b0 -> Xi_c+ pi- pi- pi-]cc",
        name="Charm_CBaryonToSl_Xib0ToXicpPimPimPim_XipToL0MupNu_LL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(3600 * MeV, F.MASS, 6350 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xics, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pippimpim_Oc0Ximmu_lll_line(
    name="Hlt2Charm_ObmToOc0PipPimPim_Oc0ToXimMupNu_LLL", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_xic0omegac0_to_ximlllmu_from_beauty()
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PipPimPim_Oc0ToXimMupNu_LLL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def xib0_to_xicppimpimpip_xicplambdamu_dd_line(
    name="Hlt2Charm_Xib0ToXicpPimPimPip_XicpToL0MupNu_DD", prescale=1
):
    pions = _filter_long_pions()
    xics = make_lambdacpxicp_to_lambdamu_dd_from_beauty(
        "[Xi_c+ -> Lambda0 mu+]cc", 2550
    )
    line_alg = ParticleCombiner(
        [xics, pions, pions, pions],
        DecayDescriptor="[Xi_b0 -> Xi_c+ pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_Xib0ToXicpPipPimPim_XipToL0MupNu_DD",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(3600 * MeV, F.MASS, 6350 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xics, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def xib0_to_xicppimpimpip_xicplambdapip_dd_line(
    name="Hlt2Charm_Xib0ToXicpPimPimPip_XicpToL0Pip_DD", prescale=1
):
    pions = _filter_long_pions()
    xics = make_lambdacpxicp_to_lambdapi_dd_from_beauty(
        "[Xi_c+ -> Lambda0 pi+]cc", 2350, 2550
    )
    line_alg = ParticleCombiner(
        [xics, pions, pions, pions],
        DecayDescriptor="[Xi_b0 -> Xi_c+ pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_Xib0ToXicpPipPimPim_XipToL0Pip_DD",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(5650 * MeV, F.MASS, 6200 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xics, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pippimpim_Oc0Ximmu_ddl_line(
    name="Hlt2Charm_ObmToOc0PipPimPim_Oc0ToXimMupNu_DDL", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_xic0omegac0_to_ximddlmu_from_beauty()
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PipPimPim_Oc0ToXimMupNu_DDL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def xib0_to_xicppimpimpip_xicplambdamu_dd_ws_line(
    name="Hlt2Charm_Xib0ToXicpPimPimPip_XicpToL0MumNu_DD_WS", prescale=1
):
    pions = _filter_long_pions()
    xics = make_lambdacpxicp_to_lambdamu_dd_from_beauty(
        "[Xi_c+ -> Lambda0 mu-]cc", 2550
    )
    line_alg = ParticleCombiner(
        [xics, pions, pions, pions],
        DecayDescriptor="[Xi_b0 -> Xi_c+ pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_Xib0ToXicpPipPimPim_XipToL0MumNu_DD",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(3600 * MeV, F.MASS, 6350 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xics, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pippimpim_Oc0Ximmu_ddd_line(
    name="Hlt2Charm_ObmToOc0PipPimPim_Oc0ToXimMupNu_DDD", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_xic0omegac0_to_ximdddmu_from_beauty()
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PipPimPim_Oc0ToXimMupNu_DDD",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def xib0_to_xicppimpimpim_ws_xicplambdamu_dd_line(
    name="Hlt2Charm_Xib0ToXicpPimPimPim_XicpToL0MupNu_DD", prescale=1
):
    pions = _filter_long_pions()
    xics = make_lambdacpxicp_to_lambdamu_dd_from_beauty(
        "[Xi_c+ -> Lambda0 mu+]cc", 2550
    )
    line_alg = ParticleCombiner(
        [xics, pions, pions, pions],
        DecayDescriptor="[Xi_b0 -> Xi_c+ pi- pi- pi-]cc",
        name="Charm_CBaryonToSl_Xib0ToXicpPimPimPim_XipToL0MupNu_DD",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(3600 * MeV, F.MASS, 6350 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xics, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pippimpim_Oc0Ommu_lll_line(
    name="Hlt2Charm_ObmToOc0PipPimPim_Oc0ToOmMupNu_LLL", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_omegac0_to_omegamlllmu_from_beauty()
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PipPimPim_Oc0ToOmMupNu_LLL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pippimpim_Oc0Ommu_ddl_line(
    name="Hlt2Charm_ObmToOc0PipPimPim_Oc0ToOmMupNu_DDL", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_omegac0_to_omegamddlmu_from_beauty()
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PipPimPim_Oc0ToOmMupNu_DDL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pippimpim_Oc0Ommu_ddd_line(
    name="Hlt2Charm_ObmToOc0PipPimPim_Oc0ToOmMupNu_DDD", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_omegac0_to_omegamdddmu_from_beauty()
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PipPimPim_Oc0ToOmMupNu_DDD",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


###############################################################################
# NC Lines
###############################################################################
@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0L0PimPip_ll_line(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0PimPip_LL", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0_to_lambda0pillpi_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToL0PimPip_LL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(5500 * MeV, F.MASS, 6100 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0L0PimPip_dd_line(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0PimPip_DD", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0_to_lambda0piddpi_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToL0PimPip_DD",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(5500 * MeV, F.MASS, 6100 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0L0Kmpi_ll_line(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0KmPip_LL", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0_to_lambda0kllpi_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToL0KmPip_LL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(5500 * MeV, F.MASS, 6100 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0L0Kmpi_dd_line(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0KmPip_DD", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0_to_lambda0kddpi_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToL0KmPip_DD",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(5500 * MeV, F.MASS, 6100 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0Ximpi_lll_line(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToXimPip_LLL", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0omegac0_to_ximlllpi_from_beauty("[Xi_c0 -> Xi- pi+]cc")
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToXimPip_LLL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(5500 * MeV, F.MASS, 6100 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0Ximpi_ddl_line(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToXimPip_DDL", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0omegac0_to_ximddlpi_from_beauty("[Xi_c0 -> Xi- pi+]cc")
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToXimPip_DDL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(5500 * MeV, F.MASS, 6100 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0Ximpi_ddd_line(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToXimPip_DDD", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0omegac0_to_ximdddpi_from_beauty("[Xi_c0 -> Xi- pi+]cc")
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToXimPip_DDD",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(5500 * MeV, F.MASS, 6100 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pippimpim_Oc0Ximpi_lll_line(
    name="Hlt2Charm_ObmToOc0PipPimPim_Oc0ToXimPip_LLL", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_xic0omegac0_to_ximlllpi_from_beauty("[Omega_c0 -> Xi- pi+]cc")
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PipPimPim_Oc0ToXimPip_LLL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(5600 * MeV, F.MASS, 6250 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pippimpim_Oc0Ximpi_ddl_line(
    name="Hlt2Charm_ObmToOc0PipPimPim_Oc0ToXimPip_DDL", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_xic0omegac0_to_ximddlpi_from_beauty("[Omega_c0 -> Xi- pi+]cc")
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PipPimPim_Oc0ToXimPip_DDL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(5600 * MeV, F.MASS, 6250 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pippimpim_Oc0Ximpi_ddd_line(
    name="Hlt2Charm_ObmToOc0PipPimPim_Oc0ToXimPip_DDD", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_xic0omegac0_to_ximdddpi_from_beauty("[Omega_c0 -> Xi- pi+]cc")
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PipPimPim_Oc0ToXimPip_DDD",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(5600 * MeV, F.MASS, 6250 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pippimpim_Oc0Ompi_lll_line(
    name="Hlt2Charm_ObmToOc0PipPimPim_Oc0ToOmPip_LLL", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_omegac0_to_omegamlllpi_from_beauty()
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PipPimPim_Oc0ToOmPip_LLL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(5600 * MeV, F.MASS, 6250 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pippimpim_Oc0Ompi_ddl_line(
    name="Hlt2Charm_ObmToOc0PipPimPim_Oc0ToOmPip_DDL", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_omegac0_to_omegamddlpi_from_beauty()
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PipPimPim_Oc0ToOmPip_DDL",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(5600 * MeV, F.MASS, 6250 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pippimpim_Oc0Ompi_ddd_line(
    name="Hlt2Charm_ObmToOc0PipPimPim_Oc0ToOmPip_DDD", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_omegac0_to_omegamdddpi_from_beauty()
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PipPimPim_Oc0ToOmPip_DDD",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(5600 * MeV, F.MASS, 6250 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


###############################################################################
# WS Lines
###############################################################################


@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0L0Pimmum_ll_wsline(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0PimMumNu_LL_WS", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0_to_lambda0pillmu_ws_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToL0PimMumNu_LL_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0L0Pimmum_dd_wsline(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0PimMumNu_DD_WS", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0_to_lambda0piddmu_ws_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToL0PimMumNu_DD_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0L0Kmmum_ll_wsline(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0KmMumNu_LL_WS", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0_to_lambda0kllmu_ws_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToL0KmMumNu_LL_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0L0Kmmum_dd_wsline(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToL0KmMumNu_DD_WS", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0_to_lambda0kddmu_ws_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToL0KmMumNu_DD_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pimpimpim_Xic0L0Pimmu_ll_wsline(
    name="Hlt2Charm_XibmToXic0PimPimPim_Xic0ToL0PimMupNu_LL_WS", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0_to_lambda0pillmu_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi- pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PimPimPim_Xic0ToL0PimMupNu_LL_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pimpimpim_Xic0L0Pimmu_dd_wsline(
    name="Hlt2Charm_XibmToXic0PimPimPim_Xic0ToL0PimMupNu_DD_WS", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0_to_lambda0piddmu_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi- pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PimPimPim_Xic0ToL0PimMupNu_DD_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pimpimpim_Xic0L0Kmmu_ll_wsline(
    name="Hlt2Charm_XibmToXic0PimPimPim_Xic0ToL0KmMupNu_LL_WS", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0_to_lambda0kllmu_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi- pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PimPimPim_Xic0ToL0KmMupNu_LL_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pimpimpim_Xic0L0Kmmu_dd_wsline(
    name="Hlt2Charm_XibmToXic0PimPimPim_Xic0ToL0KmMupNu_DD_WS", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0_to_lambda0kddmu_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi- pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PimPimPim_Xic0ToL0KmMupNu_DD_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0Ximmum_lll_wsline(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToXimMumNu_LLL_WS", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0omegac0_to_ximlllmu_ws_from_beauty("[Xi_c0 -> Xi- mu-]cc")
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToXimMumNu_LLL_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0Ximmum_ddl_wsline(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToXimMumNu_DDL_WS", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0omegac0_to_ximddlmu_ws_from_beauty("[Xi_c0 -> Xi- mu-]cc")
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToXimMumNu_DDL_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pippimpim_Xic0Ximmum_ddd_wsline(
    name="Hlt2Charm_XibmToXic0PipPimPim_Xic0ToXimMumNu_DDD_WS", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0omegac0_to_ximdddmu_ws_from_beauty("[Xi_c0 -> Xi- mu-]cc")
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PipPimPim_Xic0ToXimMumNu_DDD_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pimpimpim_Xic0Ximmu_lll_wsline(
    name="Hlt2Charm_XibmToXic0PimPimPim_Xic0ToXimMupNu_LLL_WS", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0omegac0_to_ximlllmu_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi- pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PimPimPim_Xic0ToXimMupNu_LLL_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pimpimpim_Xic0Ximmu_ddl_wsline(
    name="Hlt2Charm_XibmToXic0PimPimPim_Xic0ToXimMupNu_DDL_WS", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0omegac0_to_ximddlmu_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi- pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PimPimPim_Xic0ToXimMupNu_DDL_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Xibm_to_Xic0pimpimpim_Xic0Ximmu_ddd_wsline(
    name="Hlt2Charm_XibmToXic0PimPimPim_Xic0ToXimMupNu_DDD_WS", prescale=1
):
    pions = _filter_long_pions()
    xic0s = make_xic0omegac0_to_ximdddmu_from_beauty()
    line_alg = ParticleCombiner(
        [xic0s, pions, pions, pions],
        DecayDescriptor="[Xi_b- -> Xi_c0 pi- pi- pi-]cc",
        name="Charm_CBaryonToSl_XibmToXic0PimPimPim_Xic0ToXimMupNu_DDD_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xic0s, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pippimpim_Oc0Ximmum_lll_wsline(
    name="Hlt2Charm_ObmToOc0PipPimPim_Oc0ToXimMumNu_LLL_WS", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_xic0omegac0_to_ximlllmu_ws_from_beauty("[Omega_c0 -> Xi- mu-]cc")
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PipPimPim_Oc0ToXimMumNu_LLL_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pippimpim_Oc0Ximmum_ddl_wsline(
    name="Hlt2Charm_ObmToOc0PipPimPim_Oc0ToXimMumNu_DDL_WS", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_xic0omegac0_to_ximddlmu_ws_from_beauty("[Omega_c0 -> Xi- mu-]cc")
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PipPimPim_Oc0ToXimMumNu_DDL_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pippimpim_Oc0Ximmum_ddd_wsline(
    name="Hlt2Charm_ObmToOc0PipPimPim_Oc0ToXimMumNu_DDD_WS", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_xic0omegac0_to_ximdddmu_ws_from_beauty("[Omega_c0 -> Xi- mu-]cc")
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PipPimPim_Oc0ToXimMumNu_DDD_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pippimpim_Oc0Ommum_lll_wsline(
    name="Hlt2Charm_ObmToOc0PipPimPim_Oc0ToOmMumNu_LLL_WS", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_omegac0_to_omegamlllmu_ws_from_beauty()
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PipPimPim_Oc0ToOmMumNu_LLL_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pippimpim_Oc0Ommum_ddl_wsline(
    name="Hlt2Charm_ObmToOc0PipPimPim_Oc0ToOmMumNu_DDL_WS", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_omegac0_to_omegamddlmu_ws_from_beauty()
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PipPimPim_Oc0ToOmMumNu_DDL_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pippimpim_Oc0Ommum_ddd_wsline(
    name="Hlt2Charm_ObmToOc0PipPimPim_Oc0ToOmMumNu_DDD_WS", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_omegac0_to_omegamdddmu_ws_from_beauty()
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi+ pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PipPimPim_Oc0ToOmMumNu_DDD_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pimpimpim_Oc0Ximmu_lll_wsline(
    name="Hlt2Charm_ObmToOc0PimPimPim_Oc0ToXimMupNu_LLL_WS", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_xic0omegac0_to_ximlllmu_from_beauty()
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi- pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PimPimPim_Oc0ToXimMupNu_LLL_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pimpimpim_Oc0Ximmu_ddl_wsline(
    name="Hlt2Charm_ObmToOc0PimPimPim_Oc0ToXimMupNu_DDL_WS", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_xic0omegac0_to_ximddlmu_from_beauty()
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi- pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PimPimPim_Oc0ToXimMupNu_DDL_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pimpimpim_Oc0Ximmu_ddd_wsline(
    name="Hlt2Charm_ObmToOc0PimPimPim_Oc0ToXimMupNu_DDD_WS", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_xic0omegac0_to_ximdddmu_from_beauty()
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi- pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PimPimPim_Oc0ToXimMupNu_DDD_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pimpimpim_Oc0Ommu_lll_wsline(
    name="Hlt2Charm_ObmToOc0PimPimPim_Oc0ToOmMupNu_LLL_WS", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_omegac0_to_omegamlllmu_from_beauty()
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi- pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PimPimPim_Oc0ToOmMupNu_LLL_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pimpimpim_Oc0Ommu_ddl_wsline(
    name="Hlt2Charm_ObmToOc0PimPimPim_Oc0ToOmMupNu_DDL_WS", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_omegac0_to_omegamddlmu_from_beauty()
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi- pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PimPimPim_Oc0ToOmMupNu_DDL_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def Obm_to_Oc0pimpimpim_Oc0Ommu_ddd_wsline(
    name="Hlt2Charm_ObmToOc0PimPimPim_Oc0ToOmMupNu_DDD_WS", prescale=1
):
    pions = _filter_long_pions()
    oc0 = make_omegac0_to_omegamdddmu_from_beauty()
    line_alg = ParticleCombiner(
        [oc0, pions, pions, pions],
        DecayDescriptor="[Omega_b- -> Omega_c0 pi- pi- pi-]cc",
        name="Charm_CBaryonToSl_ObmToOc0PimPimPim_Oc0ToOmMupNu_DDD_WS",
        CombinationCut=F.require_all(
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.0 * GeV,
            F.P > 20 * GeV,
            in_range(4000 * MeV, F.MASS, 6500 * MeV),
            F.SDOCA(2, 4) < 0.3 * mm,
            F.SDOCA(3, 4) < 0.3 * mm,
            F.MAXSDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.PT > 2.0 * GeV,
            F.P > 22 * GeV,
            F.SUM(F.PT) > 2.5 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.7 * mm,
            F.OWNPVFDCHI2 > 50.0,
            F.OWNPVDIRA > 0.95,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [oc0, line_alg],
        extra_outputs=isolation.make_iso_particles(line_alg, coneangle=0.5),
        prescale=prescale,
    )
