###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of MVA tools for Hlt2 lines"""

import Functors as F
from Functors.math import log
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleFilter


@configurable
def _lc_xicp_BDT_functor(pvs, mva_name):
    lc_xic_vars = {
        "log(Pp_PT)": log(F.CHILD(1, F.PT)),
        "log(Km_PT)": log(F.CHILD(2, F.PT)),
        "log(Pip_PT)": log(F.CHILD(3, F.PT)),
        "log(Pp_PT+Km_PT+Pip_PT)": log(
            F.CHILD(1, F.PT) + F.CHILD(2, F.PT) + F.CHILD(3, F.PT)
        ),
        "log(Lc_PT)": log(F.PT),
        "log(Pp_BPVIPCHI2)": log(F.CHILD(1, F.OWNPVIPCHI2)),
        "log(Km_BPVIPCHI2)": log(
            F.CHILD(
                2,
            )
        ),
        "log(Pip_BPVIPCHI2)": log(F.CHILD(3, F.OWNPVIPCHI2)),
        "log(Pp_BPVIPCHI2+Km_BPVIPCHI2+Pip_BPVIPCHI2)": log(
            F.CHILD(1, F.OWNPVIPCHI2)
            + F.CHILD(2, F.OWNPVIPCHI2)
            + F.CHILD(3, F.OWNPVIPCHI2)
        ),
        "log(Lc_BPVFDCHI2)": log(F.OWNPVFDCHI2),
        "Lc_CHI2DOF": F.CHI2DOF,
    }

    bdt_vars = {"lc_xic": lc_xic_vars}

    xml_files = {"lc_xic": "paramfile://data/Hlt2LcpXicp_InclBDT_v2.xml"}

    return F.MVA(
        MVAType="TMVA",
        Config={
            "XMLFile": xml_files[mva_name],
            "Name": "BDT",
        },
        Inputs=bdt_vars[mva_name],
    )


@configurable
def make_cbaryon_lc_xicp_mva(presel_b, pvs, mva_name, bdt_cut, filter_name=None):
    code = _lc_xicp_BDT_functor(pvs, mva_name) > bdt_cut
    return ParticleFilter(presel_b, F.FILTER(code), name=filter_name)


@configurable
def _xic0_BDT_functor(pvs, mva_name):
    xic0_vars = {
        "log(p_PT)": log(F.CHILD(1, F.PT)),
        "log(k1_PT)": log(F.CHILD(2, F.PT)),
        "log(k2_PT)": log(F.CHILD(3, F.PT)),
        "log(pi_PT)": log(F.CHILD(4, F.PT)),
        "log(p_PT+k1_PT+k2_PT+pi_PT)": log(
            F.CHILD(1, F.PT) + F.CHILD(2, F.PT) + F.CHILD(3, F.PT) + F.CHILD(4, F.PT)
        ),
        "log(xc_PT)": log(F.PT),
        "log(p_IPCHI2)": log(F.CHILD(1, F.OWNPVIPCHI2)),
        "log(k1_IPCHI2)": log(F.CHILD(2, F.OWNPVIPCHI2)),
        "log(k2_IPCHI2)": log(F.CHILD(3, F.OWNPVIPCHI2)),
        "log(pi_IPCHI2)": log(F.CHILD(4, F.OWNPVIPCHI2)),
        "log(p_IPCHI2+k1_IPCHI2+k2_IPCHI2+pi_IPCHI2)": log(
            F.CHILD(1, F.OWNPVIPCHI2)
            + F.CHILD(2, F.OWNPVIPCHI2)
            + F.CHILD(3, F.OWNPVIPCHI2)
        ),
        "xc_log_fdchi2": log(F.OWNPVFDCHI2),
        "xc_log_vtx_chi2": log(F.CHI2DOF),
    }

    bdt_vars = {"xic0": xic0_vars}

    xml_files = {"xic0": "paramfile://data/Hlt2Xic0_InclBDT_v2.xml"}

    return F.MVA(
        MVAType="TMVA",
        Config={
            "XMLFile": xml_files[mva_name],
            "Name": "BDT",
        },
        Inputs=bdt_vars[mva_name],
    )


@configurable
def make_cbaryon_xic0_mva(presel_b, pvs, mva_name, bdt_cut, filter_name=None):
    code = _xic0_BDT_functor(pvs, mva_name) > bdt_cut
    return ParticleFilter(presel_b, F.FILTER(code), name=filter_name)
