###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of Xi_c+/Xi_c0 -> Lambda/Xi- X HLT2 lines
The Lambda -> proton pi are reconstrcted based on T-tracks

2-body lines:
---------------------
- Xic0 -> Xi pi
  Hlt2Charm_Xic0ToXimPip_TTL_L
  Hlt2Charm_Xic0ToXimPip_TTD_L
  Hlt2Charm_Xic0ToXimPip_TTD_D

3-body lines:
---------------------
- Xic+ -> Xi pi pi
  Hlt2Charm_XicpToXimPipPip_TTL_LL
  Hlt2Charm_XicpToXimPipPip_TTD_LL
  Hlt2Charm_XicpToXimPipPip_TTD_DD
- Xic0 -> L K- pi+
  Hlt2Charm_Xic0ToL0KmPip_TT_LL
  Hlt2Charm_Xic0ToL0KmPip_TT_DD

4-body lines:
---------------------
- Lc+ -> L pi+ pi+ pi-
  Hlt2Charm_LcpToL0PimPipPip_TT_LLL
  Hlt2Charm_LcpToL0PimPipPip_TT_DDD
- Xic+ -> L K- pi+ pi+
  Hlt2Charm_XicpToL0KmPipPip_TT_LLL
  Hlt2Charm_XicpToL0KmPipPip_TT_DDD
- Xic0 -> Xi- pi+ pi+ pi-
  Hlt2Charm_Xic0ToXimPimPipPip_TTL_LLL
  Hlt2Charm_Xic0ToXimPimPipPip_TTD_LLL
  Hlt2Charm_Xic0ToXimPimPipPip_TTD_DDD
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    _make_V0TT,
    make_down_pions,
    make_has_rich_down_kaons,
    make_has_rich_down_pions,
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
    make_long_pions,
    make_ttrack_pions,
    make_ttrack_pions_for_V0,
    make_ttrack_protons,
    make_ttrack_protons_for_V0,
)
from RecoConf.ttrack_selections_reco import make_ttrack_MVAfiltered_protoparticles

from .prefilters import charm_prefilters


# re-definitions of functors
def _DZ_CHILD(i):
    return F.CHILD(i, F.END_VZ) - F.END_VZ


def _make_long_pions_for_xi():
    pvs = make_pvs()
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 150 * MeV,
                F.MINIPCHI2(pvs) > 8.0,
                F.GHOSTPROB < 0.6,
            )
        ),
    )


def _make_down_pions_for_xi():
    pvs = make_pvs()
    return ParticleFilter(
        make_down_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 180 * MeV,
                F.MINIPCHI2(pvs) > 25.0,
                F.GHOSTPROB < 0.6,
            )
        ),
    )


#########################################################
## Local bachelor particle filters used at least twice ##
#########################################################
def _make_loose_pions_for_charm():
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 250 * MeV,
                F.P > 3.5 * GeV,
                F.MINIPCHI2(pvs) > 3.0,
                F.PID_K < 5.0,
                F.GHOSTPROB < 0.6,
            ),
        ),
    )


def _make_loose_kaons_for_charm():
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > 500 * MeV,
                F.P > 10 * GeV,
                F.MINIPCHI2(pvs) > 3.0,
                F.PID_K > -2.0,
                F.GHOSTPROB < 0.6,
            )
        ),
    )


def _make_loose_down_pions_for_charm():
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_down_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 150 * MeV,
                F.P > 2 * GeV,
                F.MINIPCHI2(pvs) > 3.0,
                F.PID_K < 5.0,
                F.GHOSTPROB < 0.6,
            )
        ),
    )


def _make_loose_down_kaons_for_charm():
    pvs = make_pvs()
    return ParticleFilter(
        make_has_rich_down_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > 150 * MeV,
                F.P > 2 * GeV,
                F.MINIPCHI2(pvs) > 2.0,
                F.PID_K > -2.0,
                F.GHOSTPROB < 0.6,
            )
        ),
    )


def _make_mva_pions():
    return make_ttrack_pions(make_protoparticles=make_ttrack_MVAfiltered_protoparticles)


def _make_mva_protons():
    return make_ttrack_protons(
        make_protoparticles=make_ttrack_MVAfiltered_protoparticles
    )


def _make_ttrack_pions():
    return make_ttrack_pions_for_V0(
        pt_min=400.0 * MeV,
        p_min=5.0 * GeV,
        minipchi2_min=2000.0,
        make_pions=_make_mva_pions,
    )


def _make_ttrack_protons():
    return make_ttrack_protons_for_V0(
        pt_min=1.0 * GeV,
        p_min=22.0 * GeV,
        minip_max=250.0,
        minipchi2_max=6000.0,
        minipchi2_min=100.0,
        make_protons=_make_mva_protons,
    )


def _make_tt_lambdas():
    """Make Lambda -> p+ pi- from t-tracks."""
    ttrack_pions = _make_ttrack_pions()
    ttrack_protons = _make_ttrack_protons()
    descriptors = "[Lambda0 -> p+ pi-]cc"
    return _make_V0TT(
        particles=[ttrack_protons, ttrack_pions],
        descriptors=descriptors,
        maxdoca=35.0,
        maxdocachi2=80.0,
        p_min=25 * GeV,
        max_chi2=5.0,
        bpv_ip_chi2_max=8.0,
        name="Charm_Hyperons_Lambda0RKTTCombiner_{hash}",
    )


# This will always ever be called with ll_lambdas=_make_ll_lambdas_for_hyperon.
def _make_ttl_xis():
    tt_lambdas = _make_tt_lambdas()
    pions = _make_long_pions_for_xi()
    return ParticleCombiner(
        [tt_lambdas, pions],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        name="Charm_Hyperons_Xim_TTL_{hash}",
        CombinationCut=F.require_all(
            F.MASS < 1800 * MeV,
            F.PT > 1.0 * GeV,
            F.P > 30.0 * GeV,
            F.MAXDOCACUT(25.0 * mm),
            F.MAXDOCACHI2CUT(4.0),
        ),
        CompositeCut=F.require_all(
            F.MASS < 1700 * MeV,
            F.PT > 1.5 * GeV,
            F.P > 35.0 * GeV,
            F.CHI2DOF < 3.0,
            _DZ_CHILD(1) > 2500 * mm,
            F.OWNPVVDZ > 100 * mm,
            F.OWNPVIPCHI2 < 30,
            F.OWNPVFDCHI2 > 100.0,
        ),
    )


def _make_ttd_xis():
    tt_lambdas = _make_tt_lambdas()
    pions = _make_down_pions_for_xi()
    return ParticleCombiner(
        [tt_lambdas, pions],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        name="Charm_Hyperons_Xim_TTD_{hash}",
        CombinationCut=F.require_all(
            F.MASS < 1800 * MeV,
            F.PT > 1.0 * GeV,
            F.P > 25.0 * GeV,
            F.MAXDOCACUT(25.0 * mm),
            F.MAXDOCACHI2CUT(4.0),
        ),
        CompositeCut=F.require_all(
            F.MASS < 1700 * MeV,
            F.PT > 1.5 * GeV,
            F.P > 30.0 * GeV,
            F.CHI2DOF < 3.0,
            _DZ_CHILD(1) > 1500 * mm,
            F.OWNPVVDZ > 500.0 * mm,
            F.OWNPVIPCHI2 < 15.0,
            F.OWNPVFDCHI2 > 30.0,
        ),
    )


all_lines = {}


# Xic0 -> Xi- pi+.  Xi- for TTL, pi+ for L
@register_line_builder(all_lines)
def xicz_to_ximpi_ttl_l_line(name="Hlt2Charm_Xic0ToXimPip_TTL_L"):
    ttl_xis = _make_ttl_xis()
    long_xics_pions = _make_loose_pions_for_charm()
    xic0s = ParticleCombiner(
        [ttl_xis, long_xics_pions],
        DecayDescriptor="[Xi_c0 -> Xi- pi+]cc",
        name="Charm_Hyperons_Xic0ToXimPip_TTL_L_{hash}",
        CombinationCut=F.require_all(
            F.math.in_range(2000 * MeV, F.MASS, 2900 * MeV),
            F.PT > 1.0 * GeV,
            F.P > 40.0 * GeV,
            F.MAXDOCACUT(15 * mm),
            F.MAXDOCACHI2CUT(4.0),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2070 * MeV, F.MASS, 2870 * MeV),
            F.PT > 1.5 * GeV,
            F.P > 45.0 * GeV,
            F.CHI2DOF < 4.0,
            _DZ_CHILD(1) > 100 * mm,
            F.OWNPVVDZ > 2.0 * mm,
            F.OWNPVFDCHI2 > 5,
            F.OWNPVIPCHI2 < 30.0,
            F.OWNPVDIRA > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        # hlt1_filter_code=r"Hlt1.*TrackMVADecision",
        algs=charm_prefilters()
        + [
            long_xics_pions,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            _make_tt_lambdas(),
            ttl_xis,
            xic0s,
        ],
    )


# Xic0 -> Xi- pi+.  Xi- for TTD, pi+ for L
@register_line_builder(all_lines)
def xicz_to_ximpi_ttd_l_line(name="Hlt2Charm_Xic0ToXimPip_TTD_L"):
    ttd_xis = _make_ttd_xis()
    long_xics_pions = _make_loose_pions_for_charm()
    xic0s = ParticleCombiner(
        [ttd_xis, long_xics_pions],
        DecayDescriptor="[Xi_c0 -> Xi- pi+]cc",
        name="Charm_Hyperons_Xic0ToXimPip_TTD_L_{hash}",
        CombinationCut=F.require_all(
            F.math.in_range(2000 * MeV, F.MASS, 2900 * MeV),
            F.PT > 1.0 * GeV,
            F.P > 40.0 * GeV,
            F.MAXDOCACUT(15 * mm),
            F.MAXDOCACHI2CUT(4.0),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2070 * MeV, F.MASS, 2870 * MeV),
            F.PT > 1.5 * GeV,
            F.P > 45.0 * GeV,
            F.CHI2DOF < 4.0,
            _DZ_CHILD(1) > 400.0 * mm,
            F.OWNPVVDZ > 3.0 * mm,
            F.OWNPVFDCHI2 > 5,
            F.OWNPVIPCHI2 < 30.0,
            F.OWNPVDIRA > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        # hlt1_filter_code=r"Hlt1.*TrackMVADecision",
        algs=charm_prefilters()
        + [
            long_xics_pions,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            _make_tt_lambdas(),
            ttd_xis,
            xic0s,
        ],
    )


# Xic0 -> Xi- pi+.  Xi- for TTD, pi+ for D
@register_line_builder(all_lines)
def xicz_to_ximpi_ttd_d_line(name="Hlt2Charm_Xic0ToXimPip_TTD_D"):
    ttd_xis = _make_ttd_xis()
    down_xics_pions = _make_loose_down_pions_for_charm()
    xic0s = ParticleCombiner(
        [ttd_xis, down_xics_pions],
        DecayDescriptor="[Xi_c0 -> Xi- pi+]cc",
        name="Charm_Hyperons_Xic0ToXimPip_TTD_D_{hash}",
        CombinationCut=F.require_all(
            F.math.in_range(2000 * MeV, F.MASS, 2900 * MeV),
            F.PT > 1.0 * GeV,
            F.P > 25.0 * GeV,
            F.MAXDOCACUT(20 * mm),
            F.MAXDOCACHI2CUT(5.0),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2070 * MeV, F.MASS, 2870 * MeV),
            F.PT > 1.5 * GeV,
            F.P > 40.0 * GeV,
            F.CHI2DOF < 5.0,
            _DZ_CHILD(1) > 400.0 * mm,
            F.OWNPVVDZ > 40.0 * mm,
            F.OWNPVFDCHI2 > 10.0,
            F.OWNPVIPCHI2 < 60.0,
            F.OWNPVDIRA > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        # hlt1_filter_code=r"Hlt1.*TrackMVADecision",
        algs=charm_prefilters()
        + [
            down_xics_pions,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            _make_tt_lambdas(),
            ttd_xis,
            xic0s,
        ],
    )


# Xic+ -> Xi- pi+ pi+.  Xi- for TTL, pi+pi+ for LL
@register_line_builder(all_lines)
def xicp_to_ximpipi_ttl_ll_line(name="Hlt2Charm_XicpToXimPipPip_TTL_LL"):
    ttl_xis = _make_ttl_xis()
    long_xics_pions = _make_loose_pions_for_charm()
    xicps = ParticleCombiner(
        [ttl_xis, long_xics_pions, long_xics_pions],
        DecayDescriptor="[Xi_c+ -> Xi- pi+ pi+]cc",
        name="Charm_Hyperons_XicpToXimPipPip_TTL_LL_{hash}",
        Combination12Cut=F.require_all(
            F.math.in_range(1400 * MeV, F.MASS, 2600 * MeV),
            F.MAXDOCACUT(20 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2000 * MeV, F.MASS, 2950 * MeV),
            F.PT > 1.0 * GeV,
            F.P > 40.0 * GeV,
            F.DOCA(1, 3) < 20 * mm,
            F.DOCA(2, 3) < 0.2 * mm,
            F.MAXDOCACHI2CUT(15.0),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2070 * MeV, F.MASS, 2870 * MeV),
            F.PT > 1.5 * GeV,
            F.P > 50.0 * GeV,
            F.CHI2DOF < 8.0,
            _DZ_CHILD(1) > 60 * mm,
            F.OWNPVVDZ > 1.0 * mm,
            F.OWNPVFDCHI2 > 5.0,
            F.OWNPVIPCHI2 < 30.0,
            F.OWNPVDIRA > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        # hlt1_filter_code=r"Hlt1.*TrackMVADecision",
        algs=charm_prefilters()
        + [
            long_xics_pions,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            _make_tt_lambdas(),
            ttl_xis,
            xicps,
        ],
    )


# Xic+ -> Xi- pi+ pi+.  Xi- for TTD, pi+pi+ for LL
@register_line_builder(all_lines)
def xicp_to_ximpipi_ttd_ll_line(name="Hlt2Charm_XicpToXimPipPip_TTD_LL"):
    ttd_xis = _make_ttd_xis()
    long_xics_pions = _make_loose_pions_for_charm()
    xicps = ParticleCombiner(
        [ttd_xis, long_xics_pions, long_xics_pions],
        DecayDescriptor="[Xi_c+ -> Xi- pi+ pi+]cc",
        name="Charm_Hyperons_XicpToXimPipPip_TTD_LL_{hash}",
        Combination12Cut=F.require_all(
            F.math.in_range(1400 * MeV, F.MASS, 2600 * MeV),
            F.MAXDOCACUT(20 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2000 * MeV, F.MASS, 2950 * MeV),
            F.PT > 1.0 * GeV,
            F.P > 40.0 * GeV,
            F.DOCA(1, 3) < 20 * mm,
            F.DOCA(2, 3) < 0.2 * mm,
            F.MAXDOCACHI2CUT(15.0),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2070 * MeV, F.MASS, 2870 * MeV),
            F.PT > 2.0 * GeV,
            F.P > 50.0 * GeV,
            F.CHI2DOF < 8.0,
            _DZ_CHILD(1) > 400.0 * mm,
            F.OWNPVVDZ > 1.0 * mm,
            F.OWNPVFDCHI2 > 5.0,
            F.OWNPVIPCHI2 < 30.0,
            F.OWNPVDIRA > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        # hlt1_filter_code=r"Hlt1.*TrackMVADecision",
        algs=charm_prefilters()
        + [
            long_xics_pions,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            _make_tt_lambdas(),
            ttd_xis,
            xicps,
        ],
    )


# Xic+ -> Xi- pi+ pi+.  Xi- for TTD, pi+pi+ for DD
@register_line_builder(all_lines)
def xicp_to_ximpipi_ttd_dd_line(name="Hlt2Charm_XicpToXimPipPip_TTD_DD"):
    ttd_xis = _make_ttd_xis()
    down_xics_pions = _make_loose_down_pions_for_charm()
    xicps = ParticleCombiner(
        [ttd_xis, down_xics_pions, down_xics_pions],
        DecayDescriptor="[Xi_c+ -> Xi- pi+ pi+]cc",
        name="Charm_Hyperons_XicpToXimPipPip_TTD_DD_{hash}",
        Combination12Cut=F.require_all(
            F.math.in_range(1400 * MeV, F.MASS, 2600 * MeV),
            F.MAXDOCACUT(25 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2000 * MeV, F.MASS, 2950 * MeV),
            F.PT > 0.8 * GeV,
            F.P > 20.0 * GeV,
            F.DOCA(1, 3) < 20 * mm,
            F.DOCA(2, 3) < 20 * mm,
            F.MAXDOCACHI2CUT(30.0),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2070 * MeV, F.MASS, 2870 * MeV),
            F.PT > 1.0 * GeV,
            F.P > 30.0 * GeV,
            F.CHI2DOF < 10.0,
            _DZ_CHILD(1) > 500 * mm,
            F.OWNPVVDZ > 50.0 * mm,
            F.OWNPVFDCHI2 > 5.0,
            F.OWNPVIPCHI2 < 60.0,
            F.OWNPVDIRA > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        # hlt1_filter_code=r"Hlt1.*TrackMVADecision",
        algs=charm_prefilters()
        + [
            down_xics_pions,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            _make_tt_lambdas(),
            ttd_xis,
            xicps,
        ],
    )


# Xic0 -> Lambda K- pi+.  Lambda for TT, K-pi+ for LL
@register_line_builder(all_lines)
def xicz_to_lkpi_tt_ll_line(name="Hlt2Charm_Xic0ToL0KmPip_TT_LL"):
    tt_lambdas = _make_tt_lambdas()
    long_xicz_pions = _make_loose_pions_for_charm()
    long_xicz_kaons = _make_loose_kaons_for_charm()
    xic0s = ParticleCombiner(
        [tt_lambdas, long_xicz_kaons, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- pi+]cc",
        name="Charm_Hyperons_Xic0ToL0KmPip_TT_LL_{hash}",
        Combination12Cut=F.require_all(
            F.math.in_range(1400 * MeV, F.MASS, 2600 * MeV),
            F.MAXDOCACUT(15 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2000 * MeV, F.MASS, 2950 * MeV),
            F.PT > 1.5 * GeV,
            F.P > 40.0 * GeV,
            F.DOCA(1, 3) < 25 * mm,
            F.DOCA(2, 3) < 0.2 * mm,
            F.MAXDOCACHI2CUT(10.0),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2070 * MeV, F.MASS, 2870 * MeV),
            F.PT > 2.0 * GeV,
            F.P > 60.0 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 3500.0 * mm,
            F.OWNPVVDZ > 1.5 * mm,
            F.OWNPVFDCHI2 > 5.0,
            F.OWNPVIPCHI2 < 15.0,
            F.OWNPVDIRA > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        # hlt1_filter_code=r"Hlt1.*TrackMVADecision",
        algs=charm_prefilters()
        + [
            long_xicz_pions,
            long_xicz_kaons,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            tt_lambdas,
            xic0s,
        ],
    )


# Xic0 -> Lambda K- pi+.  Lambda for TT, K-pi+ for DD
@register_line_builder(all_lines)
def xicz_to_lkpi_tt_dd_line(name="Hlt2Charm_Xic0ToL0KmPip_TT_DD"):
    tt_lambdas = _make_tt_lambdas()
    down_xicz_pions = _make_loose_down_pions_for_charm()
    down_xicz_kaons = _make_loose_down_kaons_for_charm()
    xic0s = ParticleCombiner(
        [tt_lambdas, down_xicz_kaons, down_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- pi+]cc",
        name="Charm_Hyperons_Xic0ToL0KmPip_TT_DD_{hash}",
        Combination12Cut=F.require_all(
            F.math.in_range(1400 * MeV, F.MASS, 2600 * MeV),
            F.MAXDOCACUT(15 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2000 * MeV, F.MASS, 2950 * MeV),
            F.PT > 0.8 * GeV,
            F.P > 20.0 * GeV,
            F.DOCA(1, 3) < 20 * mm,
            F.DOCA(2, 3) < 10 * mm,
            F.MAXDOCACHI2CUT(15.0),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2070 * MeV, F.MASS, 2870 * MeV),
            F.PT > 1.5 * GeV,
            F.P > 40.0 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 3500.0 * mm,
            F.OWNPVVDZ > 40.0 * mm,
            F.OWNPVFDCHI2 > 60.0,
            F.OWNPVIPCHI2 < 40.0,
            F.OWNPVDIRA > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        # hlt1_filter_code=r"Hlt1.*TrackMVADecision",
        algs=charm_prefilters()
        + [
            down_xicz_pions,
            down_xicz_kaons,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            tt_lambdas,
            xic0s,
        ],
    )


# Lc+ -> Lambda pi- pi+ pi+.  Lambda for TT, 3pi for L
@register_line_builder(all_lines)
def lc_to_l0pipipi_tt_lll_line(name="Hlt2Charm_LcpToL0PimPipPip_TT_LLL"):
    tt_lambdas = _make_tt_lambdas()
    long_lc_pions = _make_loose_pions_for_charm()
    lcs = ParticleCombiner(
        [tt_lambdas, long_lc_pions, long_lc_pions, long_lc_pions],
        DecayDescriptor="[Lambda_c+ -> Lambda0 pi- pi+ pi+]cc",
        name="Charm_Hyperons_LcpToL0PimPipPip_TT_LLL_{hash}",
        Combination12Cut=F.require_all(
            F.math.in_range(1200 * MeV, F.MASS, 3000 * MeV),
            F.MAXDOCACUT(30 * mm),
        ),
        Combination123Cut=F.require_all(
            F.math.in_range(1350 * MeV, F.MASS, 2900 * MeV),
            F.DOCA(1, 3) < 30 * mm,
            F.DOCA(2, 3) < 0.2 * mm,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(1750 * MeV, F.MASS, 2800 * MeV),
            F.PT > 2 * GeV,
            F.P > 20 * GeV,
            F.SUM(F.PT) > 3.0 * GeV,
            F.DOCA(1, 4) < 30 * mm,
            F.DOCA(2, 4) < 0.2 * mm,
            F.DOCA(3, 4) < 0.2 * mm,
            F.MAXDOCACUT(30.0 * mm),
            F.MAXDOCACHI2CUT(20.0),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1890 * MeV, F.MASS, 2690 * MeV),
            F.PT > 2 * GeV,
            F.P > 60 * GeV,
            F.CHI2DOF < 8.0,
            _DZ_CHILD(1) > 3500 * mm,
            F.OWNPVVDZ > 1.0 * mm,
            F.OWNPVFDCHI2 > 5.0,
            F.OWNPVIPCHI2 < 30,
            F.OWNPVDIRA > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters()
        + [
            long_lc_pions,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            tt_lambdas,
            lcs,
        ],
    )


# Lc+ -> Lambda pi- pi+ pi+. Lambda for TT, 3pi for D
@register_line_builder(all_lines)
def lc_to_l0pipipi_tt_ddd_line(name="Hlt2Charm_LcpToL0PimPipPip_TT_DDD"):
    tt_lambdas = _make_tt_lambdas()
    down_lc_pions = _make_loose_down_pions_for_charm()
    lcs = ParticleCombiner(
        [tt_lambdas, down_lc_pions, down_lc_pions, down_lc_pions],
        DecayDescriptor="[Lambda_c+ -> Lambda0 pi- pi+ pi+]cc",
        name="Charm_Hyperons_LcpToL0PimPipPip_TT_DDD_{hash}",
        Combination12Cut=F.require_all(
            F.math.in_range(1200 * MeV, F.MASS, 3000 * MeV),
            F.MAXDOCACUT(30 * mm),
        ),
        Combination123Cut=F.require_all(
            F.math.in_range(1350 * MeV, F.MASS, 2900 * MeV),
            F.DOCA(1, 3) < 30 * mm,
            F.DOCA(2, 3) < 20 * mm,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(1750 * MeV, F.MASS, 2800 * MeV),
            F.PT > 2 * GeV,
            F.P > 20 * GeV,
            F.SUM(F.PT) > 3.0 * GeV,
            F.DOCA(1, 4) < 30 * mm,
            F.DOCA(2, 4) < 20 * mm,
            F.DOCA(3, 4) < 20 * mm,
            F.MAXDOCACUT(30.0 * mm),
            F.MAXDOCACHI2CUT(30.0),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1890 * MeV, F.MASS, 2690 * MeV),
            F.PT > 1.5 * GeV,
            F.P > 40 * GeV,
            F.CHI2DOF < 8.0,
            _DZ_CHILD(1) > 3500 * mm,
            F.OWNPVVDZ > 100.0 * mm,
            F.OWNPVFDCHI2 > 35.0,
            F.OWNPVIPCHI2 < 60,
            F.OWNPVDIRA > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters()
        + [
            down_lc_pions,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            tt_lambdas,
            lcs,
        ],
    )


# Xic+ -> Lambda K- pi+ pi+. Lambda for TT, K and pi for L
@register_line_builder(all_lines)
def xicp_to_lkpipi_tt_lll_line(name="Hlt2Charm_XicpToL0KmPipPip_TT_LLL"):
    tt_lambdas = _make_tt_lambdas()
    long_xicp_pions = _make_loose_pions_for_charm()
    long_xicp_kaons = _make_loose_kaons_for_charm()
    xicps = ParticleCombiner(
        [tt_lambdas, long_xicp_kaons, long_xicp_pions, long_xicp_pions],
        DecayDescriptor="[Xi_c+ -> Lambda0 K- pi+ pi+]cc",
        name="Charm_Hyperons_XicpToL0KmPipPip_TT_LLL_{hash}",
        Combination12Cut=F.require_all(
            F.math.in_range(1400 * MeV, F.MASS, 2600 * MeV),
            F.MAXDOCACUT(30 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 3000 * MeV,
            F.DOCA(1, 3) < 30 * mm,
            F.DOCA(2, 3) < 0.2 * mm,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2000 * MeV, F.MASS, 2950 * MeV),
            F.PT > 2 * GeV,
            F.P > 20 * GeV,
            F.SUM(F.PT) > 3.0 * GeV,
            F.DOCA(1, 4) < 30 * mm,
            F.DOCA(2, 4) < 0.2 * mm,
            F.DOCA(3, 4) < 0.2 * mm,
            F.MAXDOCACUT(30.0 * mm),
            F.MAXDOCACHI2CUT(20.0),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2070 * MeV, F.MASS, 2870 * MeV),
            F.PT > 3 * GeV,
            F.P > 65 * GeV,
            F.CHI2DOF < 8.0,
            _DZ_CHILD(1) > 3000 * mm,
            F.OWNPVVDZ > 1.5 * mm,
            F.OWNPVFDCHI2 > 10.0,
            F.OWNPVIPCHI2 < 20.0,
            F.OWNPVDIRA > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters()
        + [
            long_xicp_pions,
            long_xicp_kaons,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            tt_lambdas,
            xicps,
        ],
    )


# Xic+ -> Lambda K- pi+ pi+. Lambda for TT, K and pi for D
@register_line_builder(all_lines)
def xicp_to_lkpipi_tt_ddd_line(name="Hlt2Charm_XicpToL0KmPipPip_TT_DDD"):
    tt_lambdas = _make_tt_lambdas()
    down_xicp_pions = _make_loose_down_pions_for_charm()
    down_xicp_kaons = _make_loose_down_kaons_for_charm()
    xicps = ParticleCombiner(
        [tt_lambdas, down_xicp_kaons, down_xicp_pions, down_xicp_pions],
        DecayDescriptor="[Xi_c+ -> Lambda0 K- pi+ pi+]cc",
        name="Charm_Hyperons_XicpToL0KmPipPip_TT_DDD_{hash}",
        Combination12Cut=F.require_all(
            F.math.in_range(1400 * MeV, F.MASS, 3000 * MeV),
            F.MAXDOCACUT(30 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 3000 * MeV,
            F.DOCA(1, 3) < 30 * mm,
            F.DOCA(2, 3) < 20 * mm,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2000 * MeV, F.MASS, 2950 * MeV),
            F.PT > 2 * GeV,
            F.P > 20 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.DOCA(1, 4) < 30 * mm,
            F.DOCA(2, 4) < 20 * mm,
            F.DOCA(3, 4) < 20 * mm,
            F.MAXDOCACUT(30.0 * mm),
            F.MAXDOCACHI2CUT(30.0),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2070 * MeV, F.MASS, 2870 * MeV),
            F.PT > 3 * GeV,
            F.P > 60 * GeV,
            F.CHI2DOF < 8.0,
            _DZ_CHILD(1) > 3000 * mm,
            F.OWNPVVDZ > 30.0 * mm,
            F.OWNPVFDCHI2 > 30.0,
            F.OWNPVIPCHI2 < 60.0,
            F.OWNPVDIRA > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters()
        + [
            down_xicp_pions,
            down_xicp_kaons,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            tt_lambdas,
            xicps,
        ],
    )


# Xic0 -> Xi- pi- pi+ pi+. Xi- for TTL, pi for L
@register_line_builder(all_lines)
def xicz_to_xim3pi_ttl_lll_line(name="Hlt2Charm_Xic0ToXimPimPipPip_TTL_LLL"):
    ttl_xis = _make_ttl_xis()
    long_xics_pions = _make_loose_pions_for_charm()
    xic0s = ParticleCombiner(
        [ttl_xis, long_xics_pions, long_xics_pions, long_xics_pions],
        DecayDescriptor="[Xi_c0 -> Xi- pi- pi+ pi+]cc",
        name="Charm_Hyperons_Xic0ToXimPimPipPip_TTL_LLL_{hash}",
        Combination12Cut=F.require_all(
            F.MASS < 3000 * MeV,
            F.MAXDOCACUT(30 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 3000 * MeV,
            F.DOCA(1, 3) < 20 * mm,
            F.DOCA(2, 3) < 0.3 * mm,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2000 * MeV, F.MASS, 3000 * MeV),
            F.PT > 2 * GeV,
            F.P > 20 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.DOCA(1, 4) < 20 * mm,
            F.DOCA(2, 4) < 0.3 * mm,
            F.DOCA(3, 4) < 0.3 * mm,
            F.MAXDOCACUT(30.0 * mm),
            F.MAXDOCACHI2CUT(30.0),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2070 * MeV, F.MASS, 2870 * MeV),
            F.PT > 2.0 * GeV,
            F.P > 55 * GeV,
            F.CHI2DOF < 15.0,
            _DZ_CHILD(1) > 100 * mm,
            F.OWNPVVDZ > 1.0 * mm,
            F.OWNPVFDCHI2 > 5.0,
            F.OWNPVIPCHI2 < 60.0,
            F.OWNPVDIRA > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters()
        + [
            long_xics_pions,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            _make_tt_lambdas(),
            ttl_xis,
            xic0s,
        ],
    )


# Xic0 -> Xi- pi- pi+ pi+. Xi- for TTD, pi for L
@register_line_builder(all_lines)
def xicz_to_xim3pi_ttd_lll_line(name="Hlt2Charm_Xic0ToXimPimPipPip_TTD_LLL"):
    ttd_xis = _make_ttd_xis()
    long_xics_pions = _make_loose_pions_for_charm()
    xic0s = ParticleCombiner(
        [ttd_xis, long_xics_pions, long_xics_pions, long_xics_pions],
        DecayDescriptor="[Xi_c0 -> Xi- pi- pi+ pi+]cc",
        name="Charm_Hyperons_Xic0ToXimPimPipPip_TTD_LLL_{hash}",
        Combination12Cut=F.require_all(
            F.MASS < 3000 * MeV,
            F.MAXDOCACUT(30 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 3000 * MeV,
            F.DOCA(1, 3) < 20 * mm,
            F.DOCA(2, 3) < 0.3 * mm,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2000 * MeV, F.MASS, 3000 * MeV),
            F.PT > 2 * GeV,
            F.P > 20 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.DOCA(1, 4) < 20 * mm,
            F.DOCA(2, 4) < 0.3 * mm,
            F.DOCA(3, 4) < 0.3 * mm,
            F.MAXDOCACUT(30.0 * mm),
            F.MAXDOCACHI2CUT(30.0),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2070 * MeV, F.MASS, 2870 * MeV),
            F.PT > 2.5 * GeV,
            F.P > 55 * GeV,
            F.CHI2DOF < 15.0,
            _DZ_CHILD(1) > 300 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVFDCHI2 > 5.0,
            F.OWNPVIPCHI2 < 60.0,
            F.OWNPVDIRA > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters()
        + [
            long_xics_pions,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            _make_tt_lambdas(),
            ttd_xis,
            xic0s,
        ],
    )


# Xic0 -> Xi- pi- pi+ pi+. Xi- for TTD, pi for D
@register_line_builder(all_lines)
def xicz_to_xim3pi_ttd_ddd_line(name="Hlt2Charm_Xic0ToXimPimPipPip_TTD_DDD"):
    ttd_xis = _make_ttd_xis()
    down_xics_pions = _make_loose_down_pions_for_charm()
    xic0s = ParticleCombiner(
        [ttd_xis, down_xics_pions, down_xics_pions, down_xics_pions],
        DecayDescriptor="[Xi_c0 -> Xi- pi- pi+ pi+]cc",
        name="Charm_Hyperons_Xic0ToXimPimPipPip_TTD_DDD_{hash}",
        Combination12Cut=F.require_all(
            F.MASS < 3000 * MeV,
            F.MAXDOCACUT(30 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 3000 * MeV,
            F.DOCA(1, 3) < 30 * mm,
            F.DOCA(2, 3) < 30 * mm,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2000 * MeV, F.MASS, 3000 * MeV),
            F.PT > 1 * GeV,
            F.P > 10 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.DOCA(1, 4) < 30 * mm,
            F.DOCA(2, 4) < 30 * mm,
            F.DOCA(3, 4) < 30 * mm,
            F.MAXDOCACUT(30.0 * mm),
            F.MAXDOCACHI2CUT(30.0),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2070 * MeV, F.MASS, 2870 * MeV),
            F.PT > 2 * GeV,
            F.P > 55 * GeV,
            F.CHI2DOF < 15.0,
            _DZ_CHILD(1) > 300 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVFDCHI2 > 5.0,
            F.OWNPVIPCHI2 < 60.0,
            F.OWNPVDIRA > 0.995,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters()
        + [
            down_xics_pions,
            _make_ttrack_protons(),
            _make_ttrack_pions(),
            _make_tt_lambdas(),
            ttd_xis,
            xic0s,
        ],
    )
