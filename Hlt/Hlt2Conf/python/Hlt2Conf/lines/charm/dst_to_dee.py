###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of HLT2 lines for the dark photon search A'(-> e+ e-) produced in a D* decay.
   The lines also preserve photons near the dielectron candidates in order to reconstruct,
   later, {pi0, eta} -> e+ e- gamma, for background studies.

Prompt lines
============

 1. D*0  -> D0    e+ e-, D0    -> K- pi+                 Hlt2Charm_Dst0ToD0EmEp_D0ToKmPip_OS/SS/_MVA
 2. D*0  -> D0    e+ e-, D0    -> K- pi- pi+ pi+         Hlt2Charm_Dst0ToD0EmEp_D0ToKmPimPipPip_OS/SS/_MVA
 3. Ds*+ -> D(s)+ e+ e-, D(s)+ -> K- K+  pi+             Hlt2Charm_DstpToDpDspEmEp_DpDspToKmKpPip_OS/SS/_MVA


TODO: displaced lines, include upstream tracks, tune when possible: F.CHI2DOF, F.CHI2, F.GHOSTPROB, F.P, F.OWNPVLTIME
"""

import Functors as F
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleFilter
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.charm.dst_to_dee_makers import (
    dst_BDT_functor,
    make_ds_to_hhh,
    make_dst_to_dee,
    make_dzeros_for_hh,
    make_dzeros_for_hhhh,
    make_kaons_for_d0_to_hh,
    make_kaons_for_d0_to_hhhh,
    make_kaons_for_d_to_hhh,
    make_photons_to_preserve,
    make_pions_for_d0_to_hh,
    make_pions_for_d0_to_hhhh,
    make_pions_for_d_to_hhh,
    make_prompt_electrons_no_brem,
    make_TwoElectrons_with_brem,
    make_untight_ds_to_hhh,
    make_untight_dzeros_for_hh,
    make_untight_dzeros_for_hhhh,
    make_untight_kaons_for_d0_to_hh,
    make_untight_kaons_for_d0_to_hhhh,
    make_untight_kaons_for_d_to_hhh,
    make_untight_pions_for_d0_to_hh,
    make_untight_pions_for_d0_to_hhhh,
    make_untight_pions_for_d_to_hhh,
    make_untight_prompt_electrons_no_brem,
)
from Hlt2Conf.lines.charm.prefilters import charm_prefilters

###############################################################################
# Lines definition
###############################################################################

all_lines = {}


@register_line_builder(all_lines)
@configurable
def dstar2dzeroee_dzero2kpi_os_line(
    name="Hlt2Charm_Dst0ToD0EmEp_D0ToKmPip_OS", prescale=1.0
):
    kaons = make_kaons_for_d0_to_hh()
    pions = make_pions_for_d0_to_hh()
    dzeros = make_dzeros_for_hh(kaons, pions, "[D0 -> K- pi+]cc")

    electrons = make_prompt_electrons_no_brem(with_upstream=True)
    dielectrons = make_TwoElectrons_with_brem(electrons)

    dst0s = make_dst_to_dee(
        dzeros,
        dielectrons,
        "[D*(2007)0 -> D0 gamma]cc",
        "Dst0ToD0EmEp_D0ToKmPip_OS_combiner",
    )

    persistable_photons = make_photons_to_preserve(dielectrons)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dst0s],
        prescale=prescale,
        persistreco=False,
        extra_outputs=[
            (
                "Dst0ToD0EmEp_D0ToKmPip_OS_prompt_Photons",
                persistable_photons.OutputLocation,
            )
        ],
        # hlt1_filter_code=["Hlt1.*MVADecision"],
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeroee_dzero2kpi_ss_line(
    name="Hlt2Charm_Dst0ToD0EmEp_D0ToKmPip_SS", prescale=1.0
):
    kaons = make_kaons_for_d0_to_hh()
    pions = make_pions_for_d0_to_hh()
    dzeros = make_dzeros_for_hh(kaons, pions, "[D0 -> K- pi+]cc")

    electrons = make_prompt_electrons_no_brem(with_upstream=True)
    dielectrons = make_TwoElectrons_with_brem(electrons, isOS=False)

    dst0s = make_dst_to_dee(
        dzeros,
        dielectrons,
        "[D*(2007)0 -> D0 gamma]cc",
        "Dst0ToD0EmEp_D0ToKmPip_SS_combiner",
    )

    persistable_photons = make_photons_to_preserve(dielectrons)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dst0s],
        prescale=prescale,
        persistreco=False,
        extra_outputs=[
            (
                "Dst0ToD0EmEp_D0ToKmPip_SS_prompt_Photons",
                persistable_photons.OutputLocation,
            )
        ],
        # hlt1_filter_code=["Hlt1.*MVADecision"],
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeroee_dzero2kpi_os_mva_line(
    name="Hlt2Charm_Dst0ToD0EmEp_D0ToKmPip_OS_MVA", prescale=1.0
):
    kaons = make_untight_kaons_for_d0_to_hh()
    pions = make_untight_pions_for_d0_to_hh()
    dzeros = make_untight_dzeros_for_hh(kaons, pions, "[D0 -> K- pi+]cc")

    electrons = make_untight_prompt_electrons_no_brem(with_upstream=True)
    dielectrons = make_TwoElectrons_with_brem(electrons)

    dst0s = make_dst_to_dee(
        dzeros,
        dielectrons,
        "[D*(2007)0 -> D0 gamma]cc",
        "Dst0ToD0EmEp_D0ToKmPip_OS_MVA_combiner",
    )

    code = dst_BDT_functor(make_pvs(), line="Hlt2Charm_Dst0ToD0EmEp_D0ToKmPip") > 0.8
    dst0s_bdt = ParticleFilter(dst0s, F.FILTER(code), name=name + "_BDT_selection")

    persistable_photons = make_photons_to_preserve(dielectrons)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dst0s_bdt],
        prescale=prescale,
        persistreco=False,
        extra_outputs=[
            (
                "Dst0ToD0EmEp_D0ToKmPip_OS_MVA_prompt_Photons",
                persistable_photons.OutputLocation,
            )
        ],
        # hlt1_filter_code=["Hlt1.*MVADecision"],
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeroee_dzero2kpi_ss_mva_line(
    name="Hlt2Charm_Dst0ToD0EmEp_D0ToKmPip_SS_MVA", prescale=1.0
):
    kaons = make_untight_kaons_for_d0_to_hh()
    pions = make_untight_pions_for_d0_to_hh()
    dzeros = make_untight_dzeros_for_hh(kaons, pions, "[D0 -> K- pi+]cc")

    electrons = make_untight_prompt_electrons_no_brem(with_upstream=True)
    dielectrons = make_TwoElectrons_with_brem(electrons, isOS=False)

    dst0s = make_dst_to_dee(
        dzeros,
        dielectrons,
        "[D*(2007)0 -> D0 gamma]cc",
        "Dst0ToD0EmEp_D0ToKmPip_SS_MVA_combiner",
    )

    code = dst_BDT_functor(make_pvs(), line="Hlt2Charm_Dst0ToD0EmEp_D0ToKmPip") > 0.8
    dst0s_bdt = ParticleFilter(dst0s, F.FILTER(code), name=name + "_BDT_selection")

    persistable_photons = make_photons_to_preserve(dielectrons)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dst0s_bdt],
        prescale=prescale,
        persistreco=False,
        extra_outputs=[
            (
                "Dst0ToD0EmEp_D0ToKmPip_SS_MVA_prompt_Photons",
                persistable_photons.OutputLocation,
            )
        ],
        # hlt1_filter_code=["Hlt1.*MVADecision"],
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeroee_dzero2kpipipi_os_line(
    name="Hlt2Charm_Dst0ToD0EmEp_D0ToKmPimPipPip_OS", prescale=1.0
):
    kaons = make_kaons_for_d0_to_hhhh()
    pions = make_pions_for_d0_to_hhhh()
    dzeros = make_dzeros_for_hhhh(
        kaons, pions, pions, pions, "[D0 -> K- pi- pi+ pi+]cc"
    )

    electrons = make_prompt_electrons_no_brem(with_upstream=True)
    dielectrons = make_TwoElectrons_with_brem(electrons)

    dst0s = make_dst_to_dee(
        dzeros,
        dielectrons,
        "[D*(2007)0 -> D0 gamma]cc",
        "Dst0ToD0EmEp_D0ToKmPimPipPip_OS_combiner",
    )

    persistable_photons = make_photons_to_preserve(dielectrons)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dst0s],
        prescale=prescale,
        persistreco=False,
        extra_outputs=[
            (
                "Dst0ToD0EmEp_D0ToKmPimPipPip_OS_prompt_Photons",
                persistable_photons.OutputLocation,
            )
        ],
        # hlt1_filter_code=["Hlt1.*MVADecision"]
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeroee_dzero2kpipipi_ss_line(
    name="Hlt2Charm_Dst0ToD0EmEp_D0ToKmPimPipPip_SS", prescale=1.0
):
    kaons = make_kaons_for_d0_to_hhhh()
    pions = make_pions_for_d0_to_hhhh()
    dzeros = make_dzeros_for_hhhh(
        kaons, pions, pions, pions, "[D0 -> K- pi- pi+ pi+]cc"
    )

    electrons = make_prompt_electrons_no_brem(with_upstream=True)
    dielectrons = make_TwoElectrons_with_brem(electrons, isOS=False)

    dst0s = make_dst_to_dee(
        dzeros,
        dielectrons,
        "[D*(2007)0 -> D0 gamma]cc",
        "Dst0ToD0EmEp_D0ToKmPimPipPip_SS_combiner",
    )

    persistable_photons = make_photons_to_preserve(dielectrons)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dst0s],
        prescale=prescale,
        persistreco=False,
        extra_outputs=[
            (
                "Dst0ToD0EmEp_D0ToKmPimPipPip_SS_prompt_Photons",
                persistable_photons.OutputLocation,
            )
        ],
        # hlt1_filter_code=["Hlt1.*MVADecision"]
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeroee_dzero2kpipipi_os_mva_line(
    name="Hlt2Charm_Dst0ToD0EmEp_D0ToKmPimPipPip_OS_MVA", prescale=1.0
):
    kaons = make_untight_kaons_for_d0_to_hhhh()
    pions = make_untight_pions_for_d0_to_hhhh()
    dzeros = make_untight_dzeros_for_hhhh(
        kaons, pions, pions, pions, "[D0 -> K- pi- pi+ pi+]cc"
    )

    electrons = make_untight_prompt_electrons_no_brem(with_upstream=True)
    dielectrons = make_TwoElectrons_with_brem(electrons)

    dst0s = make_dst_to_dee(
        dzeros,
        dielectrons,
        "[D*(2007)0 -> D0 gamma]cc",
        "Dst0ToD0EmEp_D0ToKmPimPipPip_OS_MVA_combiner",
    )

    code = (
        dst_BDT_functor(make_pvs(), line="Hlt2Charm_Dst0ToD0EmEp_D0ToKmPimPipPip") > 0.8
    )
    dst0s_bdt = ParticleFilter(dst0s, F.FILTER(code), name=name + "_BDT_selection")

    persistable_photons = make_photons_to_preserve(dielectrons)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dst0s_bdt],
        prescale=prescale,
        persistreco=False,
        extra_outputs=[
            (
                "Dst0ToD0EmEp_D0ToKmPimPipPip_OS_MVA_prompt_Photons",
                persistable_photons.OutputLocation,
            )
        ],
        # hlt1_filter_code=["Hlt1.*MVADecision"]
    )


@register_line_builder(all_lines)
@configurable
def dstar2dzeroee_dzero2kpipipi_ss_mva_line(
    name="Hlt2Charm_Dst0ToD0EmEp_D0ToKmPimPipPip_SS_MVA", prescale=1.0
):
    kaons = make_untight_kaons_for_d0_to_hhhh()
    pions = make_untight_pions_for_d0_to_hhhh()
    dzeros = make_untight_dzeros_for_hhhh(
        kaons, pions, pions, pions, "[D0 -> K- pi- pi+ pi+]cc"
    )

    electrons = make_untight_prompt_electrons_no_brem(with_upstream=True)
    dielectrons = make_TwoElectrons_with_brem(electrons, isOS=False)

    dst0s = make_dst_to_dee(
        dzeros,
        dielectrons,
        "[D*(2007)0 -> D0 gamma]cc",
        "Dst0ToD0EmEp_D0ToKmPimPipPip_SS_MVA_combiner",
    )

    code = (
        dst_BDT_functor(make_pvs(), line="Hlt2Charm_Dst0ToD0EmEp_D0ToKmPimPipPip") > 0.8
    )
    dst0s_bdt = ParticleFilter(dst0s, F.FILTER(code), name=name + "_BDT_selection")

    persistable_photons = make_photons_to_preserve(dielectrons)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dst0s_bdt],
        prescale=prescale,
        persistreco=False,
        extra_outputs=[
            (
                "Dst0ToD0EmEp_D0ToKmPimPipPip_SS_MVA_prompt_Photons",
                persistable_photons.OutputLocation,
            )
        ],
        # hlt1_filter_code=["Hlt1.*MVADecision"]
    )


@register_line_builder(all_lines)
@configurable
def dstarplus2dsee_dsp2kkpi_os_line(
    name="Hlt2Charm_DstpToDpDspEmEp_DpDspToKmKpPip_OS", prescale=1.0
):
    kaons = make_kaons_for_d_to_hhh()
    pions = make_pions_for_d_to_hhh()
    dsplus = make_ds_to_hhh(kaons, kaons, pions, "[D_s+ -> K- K+ pi+]cc")

    electrons = make_prompt_electrons_no_brem(with_upstream=True)
    dielectrons = make_TwoElectrons_with_brem(electrons)

    dstp = make_dst_to_dee(
        dsplus,
        dielectrons,
        "[D*_s+ -> D_s+ gamma]cc",
        "DstpToDpDspEmEp_DpDspToKmKpPip_OS_combiner",
    )

    persistable_photons = make_photons_to_preserve(dielectrons)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dstp],
        prescale=prescale,
        persistreco=False,
        extra_outputs=[
            (
                "DstpToDpDspEmEp_DpDspToKmKpPip_OS_prompt_Photons",
                persistable_photons.OutputLocation,
            )
        ],
        # hlt1_filter_code=["Hlt1.*MVADecision"],
    )


@register_line_builder(all_lines)
@configurable
def dstarplus2dsee_dsp2kkpi_ss_line(
    name="Hlt2Charm_DstpToDpDspEmEp_DpDspToKmKpPip_SS", prescale=1.0
):
    kaons = make_kaons_for_d_to_hhh()
    pions = make_pions_for_d_to_hhh()
    dsplus = make_ds_to_hhh(kaons, kaons, pions, "[D_s+ -> K- K+ pi+]cc")

    electrons = make_prompt_electrons_no_brem(with_upstream=True)
    dielectrons = make_TwoElectrons_with_brem(electrons, isOS=False)

    dstp = make_dst_to_dee(
        dsplus,
        dielectrons,
        "[D*_s+ -> D_s+ gamma]cc",
        "DstpToDpDspEmEp_DpDspToKmKpPip_SS_combiner",
    )

    persistable_photons = make_photons_to_preserve(dielectrons)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dstp],
        prescale=prescale,
        persistreco=False,
        extra_outputs=[
            (
                "DstpToDpDspEmEp_DpDspToKmKpPip_SS_prompt_Photons",
                persistable_photons.OutputLocation,
            )
        ],
        # hlt1_filter_code=["Hlt1.*MVADecision"],
    )


@register_line_builder(all_lines)
@configurable
def dstarplus2dsee_dsp2kkpi_os_mva_line(
    name="Hlt2Charm_DstpToDpDspEmEp_DpDspToKmKpPip_OS_MVA", prescale=1.0
):
    kaons = make_untight_kaons_for_d_to_hhh()
    pions = make_untight_pions_for_d_to_hhh()
    dsplus = make_untight_ds_to_hhh(kaons, kaons, pions, "[D_s+ -> K- K+ pi+]cc")

    electrons = make_prompt_electrons_no_brem(with_upstream=True)
    dielectrons = make_TwoElectrons_with_brem(electrons)

    dstp = make_dst_to_dee(
        dsplus,
        dielectrons,
        "[D*_s+ -> D_s+ gamma]cc",
        "DstpToDpDspEmEp_DpDspToKmKpPip_OS_MVA_combiner",
    )

    code = (
        dst_BDT_functor(make_pvs(), line="Hlt2Charm_DstpToDpDspEmEp_DpDspToKmKpPip")
        > 0.8
    )
    dstp_bdt = ParticleFilter(dstp, F.FILTER(code), name=name + "_BDT_selection")

    persistable_photons = make_photons_to_preserve(dielectrons)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dstp_bdt],
        prescale=prescale,
        persistreco=False,
        extra_outputs=[
            (
                "DstpToDpDspEmEp_DpDspToKmKpPip_OS_MVA_prompt_Photons",
                persistable_photons.OutputLocation,
            )
        ],
        # hlt1_filter_code=["Hlt1.*MVADecision"],
    )


@register_line_builder(all_lines)
@configurable
def dstarplus2dsee_dsp2kkpi_ss_mva_line(
    name="Hlt2Charm_DstpToDpDspEmEp_DpDspToKmKpPip_SS_MVA", prescale=1.0
):
    kaons = make_untight_kaons_for_d_to_hhh()
    pions = make_untight_pions_for_d_to_hhh()
    dsplus = make_untight_ds_to_hhh(kaons, kaons, pions, "[D_s+ -> K- K+ pi+]cc")

    electrons = make_prompt_electrons_no_brem(with_upstream=True)
    dielectrons = make_TwoElectrons_with_brem(electrons, isOS=False)

    dstp = make_dst_to_dee(
        dsplus,
        dielectrons,
        "[D*_s+ -> D_s+ gamma]cc",
        "DstpToDpDspEmEp_DpDspToKmKpPip_SS_MVA_combiner",
    )

    code = (
        dst_BDT_functor(make_pvs(), line="Hlt2Charm_DstpToDpDspEmEp_DpDspToKmKpPip")
        > 0.8
    )
    dstp_bdt = ParticleFilter(dstp, F.FILTER(code), name=name + "_BDT_selection")

    persistable_photons = make_photons_to_preserve(dielectrons)

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dstp_bdt],
        prescale=prescale,
        persistreco=False,
        extra_outputs=[
            (
                "DstpToDpDspEmEp_DpDspToKmKpPip_SS_MVA_prompt_Photons",
                persistable_photons.OutputLocation,
            )
        ],
        # hlt1_filter_code=["Hlt1.*MVADecision"],
    )
