###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of D -> hhh gamma HLT2 lines.

1. D+ -> K+  K-  K+  gamma (Hlt2Charm_DpToKmKpKpG)
2. D+ -> pi+ pi- pi+ gamma (Hlt2Charm_DpToPimPipPipG)
3. D+ -> K+  K-  pi+ gamma (Hlt2Charm_DpToKmKpPipG)
4. D+ -> K+  K+  pi- gamma (Hlt2Charm_DpToKpKpPimG)
5. D+ -> K-  pi+ pi+ gamma (Hlt2Charm_DpToKmPipPipG)
6. D+ -> K+  pi+ pi- gamma (Hlt2Charm_DpToKpPimPipG)
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, mm, picosecond
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
    make_photons,
)

from . import charm_isolation as isolation
from .particle_properties import _PION_M
from .prefilters import charm_prefilters

###############################################################################
# Basic particles builders (kaon and pion cuts from Stripping29r2p1)
###############################################################################


def _make_charm_pions():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 300 * MeV,
                F.MINIPCHI2CUT(IPChi2Cut=6.0, Vertices=make_pvs()),
                F.PID_K < 0,
            )
        ),
    )


def _make_charm_kaons():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > 300 * MeV,
                F.MINIPCHI2CUT(IPChi2Cut=6.0, Vertices=make_pvs()),
                F.PID_K > 0,
            )
        ),
    )


def _photon_maker():
    return make_photons(PtCut=2000 * MeV, ConfLevelCut=0.2)


###############################################################################
# Particles combiners
###############################################################################


def _make_d2hhhgamma(h1, h2, h3, gamma, descriptor, name):
    """Reconstruct the radiative decay of a D(s)+ meson from three hadrons and
    one photon.
    Cuts from http://lhcbdoc.web.cern.ch/lhcbdoc/stripping/config/stripping34r0p1/charm/strippingd2hhhgammad2kpipigammaline.html
    """
    m_max = 2100 * MeV
    return ParticleCombiner(
        [h1, h2, h3, gamma],
        DecayDescriptor=descriptor,
        name=name,
        Combination12Cut=F.require_all(
            F.MAXSDOCACUT(0.15 * mm),
            F.MAXSDOCACHI2CUT(10.0),
            F.MASS < m_max + 20 * MeV - _PION_M,
        ),
        Combination123Cut=F.require_all(
            F.SDOCA(1, 3) < 0.15 * mm,
            F.SDOCA(2, 3) < 0.15 * mm,
            F.MAXSDOCACHI2CUT(10.0),
        ),
        CombinationCut=F.require_all(
            in_range(1680 * MeV, F.MASS, m_max + 20 * MeV),
            F.SUM(F.PT) > 500 * MeV,
        ),
        CompositeCut=F.require_all(
            in_range(1700 * MeV, F.MASS, m_max),
            F.CHI2DOF < 12,
            F.PT > 2000 * MeV,
            F.OWNPVDIRA > 0.99998,
            F.OWNPVLTIME > 0.3 * picosecond,
        ),
    )


###############################################################################
# Lines definition
###############################################################################

all_lines = {}


@register_line_builder(all_lines)
def d2KpKpKmgamma_line(name="Hlt2Charm_DpToKmKpKpG", prescale=1):
    kaons = _make_charm_kaons()
    photons = _photon_maker()
    dplus = _make_d2hhhgamma(
        kaons,
        kaons,
        kaons,
        photons,
        "[D+ -> K- K+ K+ gamma]cc",
        "Charm_DToHHHG_DpToKmKpKpG",
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dplus],
        prescale=prescale,
        calo_clusters=True,
        calo_digits=True,
        extra_outputs=isolation.make_iso_particles(dplus, PizIso=True),
    )


@register_line_builder(all_lines)
def d2pippippimgamma_line(name="Hlt2Charm_DpToPimPipPipG", prescale=1):
    pions = _make_charm_pions()
    photons = _photon_maker()
    dplus = _make_d2hhhgamma(
        pions,
        pions,
        pions,
        photons,
        "[D+ -> pi- pi+ pi+ gamma]cc",
        "Charm_DToHHHG_DpToPimPipPipG",
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dplus],
        prescale=prescale,
        calo_clusters=True,
        calo_digits=True,
        extra_outputs=isolation.make_iso_particles(dplus, PizIso=True),
    )


@register_line_builder(all_lines)
def d2KmKppipgamma_line(name="Hlt2Charm_DpToKmKpPipG", prescale=1):
    kaons = _make_charm_kaons()
    pions = _make_charm_pions()
    photons = _photon_maker()
    dplus = _make_d2hhhgamma(
        kaons,
        kaons,
        pions,
        photons,
        "[D+ -> K- K+ pi+ gamma]cc",
        "Charm_DToHHHG_DpToKmKpPipG",
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dplus],
        prescale=prescale,
        calo_clusters=True,
        calo_digits=True,
        extra_outputs=isolation.make_iso_particles(dplus, PizIso=True),
    )


@register_line_builder(all_lines)
def d2KpKppimgamma_line(name="Hlt2Charm_DpToKpKpPimG", prescale=1):
    kaons = _make_charm_kaons()
    pions = _make_charm_pions()
    photons = _photon_maker()
    dplus = _make_d2hhhgamma(
        kaons,
        kaons,
        pions,
        photons,
        "[D+ -> K+ K+ pi- gamma]cc",
        "Charm_DToHHHG_DpToKpKpPimG",
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dplus],
        prescale=prescale,
        calo_clusters=True,
        calo_digits=True,
        extra_outputs=isolation.make_iso_particles(dplus, PizIso=True),
    )


@register_line_builder(all_lines)
def d2Kmpippipgamma_line(name="Hlt2Charm_DpToKmPipPipG", prescale=1):
    kaons = _make_charm_kaons()
    pions = _make_charm_pions()
    photons = _photon_maker()
    dplus = _make_d2hhhgamma(
        kaons,
        pions,
        pions,
        photons,
        "[D+ -> K- pi+ pi+ gamma]cc",
        "Charm_DToHHHG_DpToKmPipPipG",
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dplus],
        prescale=prescale,
        calo_clusters=True,
        calo_digits=True,
        extra_outputs=isolation.make_iso_particles(dplus, PizIso=True),
    )


@register_line_builder(all_lines)
def d2Kppippimgamma_line(name="Hlt2Charm_DpToKpPimPipG", prescale=1):
    kaons = _make_charm_kaons()
    pions = _make_charm_pions()
    photons = _photon_maker()
    dplus = _make_d2hhhgamma(
        kaons,
        pions,
        pions,
        photons,
        "[D+ -> K+ pi- pi+ gamma]cc",
        "Charm_DToHHHG_DpToKpPimPipG",
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dplus],
        prescale=prescale,
        calo_clusters=True,
        calo_digits=True,
        extra_outputs=isolation.make_iso_particles(dplus, PizIso=True),
    )
