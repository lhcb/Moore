###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of D0 -> pi0 pi0 HLT2 lines.

From D0 mesons produced in the D*+ decays from B0 decays. Final states built are:

B0  -> (D*(2010)+ -> (D0 -> pi0 pi0) pi+) mu- and B0  -> (D*(2010)- -> (D0 -> pi0 pi0) pi-) mu+

"""

import Functors as F
from Functors.math import in_range, sqrt
from GaudiKernel.SystemOfUnits import MeV
from GaudiKernel.SystemOfUnits import micrometer as um
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf.Algorithms import LHCb__Phys__ParticleMakers__PhotonMaker as PhotonMaker
from RecoConf.algorithms_thor import ParticleCombiner, ParticleContainersMerger
from RecoConf.reconstruction_objects import make_neutral_protoparticles, make_pvs
from SelAlgorithms.monitoring import histogram_1d, monitor

from . import charm_isolation as isolation
from .particle_properties import _D0_M
from .prefilters import charm_prefilters
from .taggers import make_tagging_muons, make_tagging_pions

all_lines = {}

Dst_M = sqrt(
    pow(
        sqrt(
            pow(F.CHILD(1, F.CHILD(1, F.MASS)), 2) + pow(F.CHILD(1, F.CHILD(1, F.P)), 2)
        )
        + sqrt(pow(F.CHILD(2, F.MASS), 2) + pow(F.CHILD(2, F.P), 2)),
        2,
    )
    - (
        pow(F.CHILD(1, F.CHILD(1, F.PX)) + F.CHILD(2, F.PX), 2)
        + pow(F.CHILD(1, F.CHILD(1, F.PY)) + F.CHILD(2, F.PY), 2)
        + pow(F.CHILD(1, F.CHILD(1, F.PZ)) + F.CHILD(2, F.PZ), 2)
    )
)
D0_M = F.CHILD(2, F.MASS)
Dst_DM = Dst_M - D0_M


def make_dzeros_2N():
    g = PhotonMaker(
        InputProtoParticles=make_neutral_protoparticles(),
        InputPrimaryVertices=make_pvs(),
        ConfLevelCut=-99,
        PtCut=2000 * MeV,
    ).Particles
    combination_code = in_range(_D0_M - 700 * MeV, F.MASS, _D0_M + 500 * MeV)
    composite_code = F.PT > 2500 * MeV
    return ParticleCombiner(
        ParticleCombiner="ParticleAdder",
        Inputs=[g, g],
        name="Charm_D0ToPi0Pi0_D0_2N",
        DecayDescriptor="D0 -> gamma gamma",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


def make_pimu_pair(
    particle1,
    particle2,
    descriptor="[K*(892)~0 -> pi+ mu-]cc",
    name="Charm_D0ToPi0Pi0_Kst0bToPipMum",
    comb_m_max=1400 * MeV,
    docachi2_max=15.0,
    doca_max=100 * um,
    vchi2dof_max=3.0,
    bpvfdchi2_min=100.0,
    bpvdira_min=0.99,
):
    """Make the intermediate 2-body resonances `res -> spip mum`."""

    combination_code = F.require_all(
        F.MASS < comb_m_max, F.MAXSDOCACHI2CUT(docachi2_max), F.SDOCA(1, 2) < doca_max
    )

    vertex_code = F.require_all(
        F.CHI2DOF < vchi2dof_max,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
        F.OWNPVDIRA > bpvdira_min,
    )

    return ParticleCombiner(
        [particle1, particle2],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def make_bzeros_from_descriptor(
    particle1,
    particle2,
    descriptor,
    name,
    comb_m_min=2340 * MeV,
    comb_m_max=5160 * MeV,
    m_min=2350 * MeV,
    m_max=5150 * MeV,
    sum_pt_min=2500 * MeV,
    bpvdira_min=0.99,
):
    """Combine the 2-body vertex and the D0 to make the B0 candidate."""

    assert particle1 is not None, "particles must be specified"
    assert particle2 is not None, "particles must be specified"

    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max), F.SUM(F.PT) > sum_pt_min
    )
    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.OWNPVDIRA > bpvdira_min,
        Dst_DM < 210,
    )

    return ParticleCombiner(
        [particle1, particle2],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def make_bzeros(spipmum, dzeros):
    bzeros_pos = make_bzeros_from_descriptor(
        spipmum, dzeros, "[B0 -> K*(892)~0 D0]cc", "Charm_D0ToPi0Pi0_B0ToKst0bD0_{hash}"
    )
    bzeros_neg = make_bzeros_from_descriptor(
        spipmum,
        dzeros,
        "[B0 -> K*(892)~0 D~0]cc",
        "Charm_D0ToPi0Pi0_B0ToKst0bD0b_{hash}",
    )
    bzeros = ParticleContainersMerger([bzeros_pos, bzeros_neg])
    return bzeros


@register_line_builder(all_lines)
def b2dstarmu_dstarp2dzeropip_dzero2pi0pi02N_line(
    name="Hlt2Charm_BToDstpMumX_DstpToD0Pip_D0ToPi0Pi0_2N", prescale=1
):
    dzeros = make_dzeros_2N()
    spip = make_tagging_pions(True)
    mum = make_tagging_muons()
    spipmum = make_pimu_pair(spip, mum)
    bzeros = make_bzeros(spipmum, dzeros)

    dst_dm_mon = monitor(
        data=bzeros,
        histograms=[
            histogram_1d(
                functor=Dst_DM,
                name=f"/{name}/dst_dm",
                title=f"Monitor_{name}_dst_dm",
                label="Dst_DM",
                bins=100,
                range=(130 * MeV, 210 * MeV),
            )
        ],
    )

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dzeros, spipmum, bzeros, dst_dm_mon],
        prescale=prescale,
        calo_clusters=True,
        calo_digits=True,
        extra_outputs=isolation.make_iso_particles(bzeros, coneangle=1.5, PizIso=True),
    )
