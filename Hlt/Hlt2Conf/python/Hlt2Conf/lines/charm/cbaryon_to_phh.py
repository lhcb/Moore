###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Following prompt lines are defined:

 1. [Lambda_c+ -> p+ K- K+]CC
 2. [Lambda_c+ -> p+ K+ pi-]CC
 3. [Lambda_c+ -> p+ pi- pi+]CC
 4. [Xi_c+ -> p+ K- K+]CC
 5. [Xi_c+ -> p+ pi- pi+]CC

Additionally Lc originating from Lambda0 is selected in following channels:

 4. [Lambda_b0 -> (Lambda_c+ -> p+ K- K+) mu-]CC
 5. [Lambda_b0 -> (Lambda_c+ -> p+ K+ pi-) mu-]CC
 6. [Lambda_b0 -> (Lambda_c+ -> p+ pi- pi+) mu-]CC
 7. [Lambda_b0 -> (Xi_c+ -> p+ K- K+) mu-]CC
 8. [Lambda_b0 -> (Xi_c+ -> p+ pi- pi+) mu-]CC

Proponents: Miroslav Saur, Xiao-Rui Lyu, Ziyi Wang
TODO: study DOCA cuts for 3-body combinations, check IPCHI2 requirements.
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from GaudiKernel.SystemOfUnits import micrometer as um
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
    make_has_rich_long_protons,
    make_ismuon_long_muon,
)

from .prefilters import charm_prefilters

all_lines = {}


###################
## track filters ##
###################
def filter_long_pions(
    pvs,
    pt_min=450 * MeV,
    p_min=2 * GeV,
    mipchi2_min=6.0,
    pion_pidk_max=0.0,  # pion PID cut by using cut on PID_K
):
    """Filter long pions with P PT, MINIPCHI2CUT and PIDk cuts."""
    cut = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
        F.PID_K < pion_pidk_max,
    )
    return ParticleFilter(make_has_rich_long_pions(), F.FILTER(cut))


def filter_long_kaons(
    pvs, pt_min=450 * MeV, p_min=5 * GeV, mipchi2_min=6.0, pidk_min=5.0
):
    """Filter long kaons with P PT, MINIPCHI2CUT and pidp cuts."""
    cut = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
        F.PID_K > pidk_min,
    )
    return ParticleFilter(make_has_rich_long_kaons(), F.FILTER(cut))


def filter_long_kaons_from_strange(
    pvs,
    pt_min=200 * MeV,
    p_min=3 * GeV,
    mipchi2_min=12.0,  # could go up to 20?
    pidk_min=3.0,
):
    """Filter long kaons with P PT, MINIPCHI2CUT and pidp cuts."""
    cut = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
        F.PID_K > pidk_min,
    )
    return ParticleFilter(make_has_rich_long_kaons(), F.FILTER(cut))


def filter_long_protons(
    pvs, pt_min=400 * MeV, p_min=4 * GeV, mipchi2_min=5.0, pidp_min=2, pidp_vs_k_min=5
):
    """Filter long protons with P PT, MINIPCHI2CUT and ProbNNp cuts.
    Defaults tuned for Lambda selection.
    """
    cut = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
        F.PID_P > pidp_min,
        (F.PID_P - F.PID_K > pidp_vs_k_min),
    )
    return ParticleFilter(make_has_rich_long_protons(), F.FILTER(cut))


def filter_muons_from_b(
    pvs, pt_min=1.0 * GeV, p_min=6.0 * GeV, mipchi2_min=9.0, pidmu_min=(0.0)
):
    cut = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.MINIPCHI2CUT(IPChi2Cut=mipchi2_min, Vertices=pvs),
        F.PID_MU > pidmu_min,
    )
    return ParticleFilter(make_ismuon_long_muon(), F.FILTER(cut))


########################
## phi(1020) combiner ##
########################
def combine_phi_kk(
    pvs,
    name="Charm_cbaryon_to_phh_combine_phi_kk_{hash}",
    comb_m_min=960 * MeV,
    comb_m_max=1240 * MeV,
    m_min=970 * MeV,
    m_max=1220 * MeV,  # asymmetric range to better cover uppersideband
    comb_pt_min=300 * MeV,
    pt_min=350 * MeV,
    comb_p_min=3.0 * GeV,
    p_min=3.5 * GeV,
    doca_max=0.25 * mm,
    vchi2pdof_max=7.0,
    bpvfdchi2_min=3.0,
):
    """Make phi(1020) -> K- K+ from long tracks."""
    kaons = filter_long_kaons_from_strange(pvs)
    comb_cut = F.require_all(
        F.MAXSDOCACUT(doca_max),
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.PT > comb_pt_min,
        F.P > comb_p_min,
    )
    vertex_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.P > p_min,
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
    )
    return ParticleCombiner(
        [kaons, kaons],
        name=name,
        DecayDescriptor="phi(1020) -> K- K+",
        CombinationCut=comb_cut,
        CompositeCut=vertex_cut,
    )


###################
## Lc+  combiner ##
###################
def combine_lc_phh(
    protons,
    particle1,
    particle2,
    pvs,
    decay_descriptor,
    name="Charm_cbaryon_to_phh_combine_lc_phh_{hash}",
    comb_m_min=2191 * MeV,
    comb_m_max=2381 * MeV,
    m_min=2201 * MeV,
    m_max=2371 * MeV,
    pt_min=2 * GeV,
    sum_pt_min=4 * GeV,
    comb_p_min=15 * GeV,
    p_min=16 * GeV,
    doca_max=100 * um,
    vchi2pdof_max=6.0,
    bpvvdz_min=0.0 * mm,
    bpvfdchi2_min=8.0,
    bpvdira_min=0.995,
    bpvipchi2_max=30,
):
    """Combine protons and hh to form Lc+.
    Make MASS, P, PT, SUM(PT), MAXDOCACUT cuts in the combination;
    MASS, P, PT, CHI2DOF, BPVVDZ, BPVFDCHI2, BPVIPCHI2, BPVDIRA cuts after the vertex fit.
    Cuts generally based on Run2 Turbo lines.
    """
    comb_cut = F.require_all(
        F.MAXSDOCACUT(doca_max),
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.SUM(F.PT) > sum_pt_min,
        F.P > comb_p_min,
    )
    vertex_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.P > p_min,
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVVDZ > bpvvdz_min,
        F.OWNPVFDCHI2 > bpvfdchi2_min,  # <-- to be checked
    )
    if bpvdira_min is not None:
        vertex_cut = F.require_all(vertex_cut, F.OWNPVDIRA > bpvdira_min)
    if bpvipchi2_max is not None:
        vertex_cut = F.require_all(vertex_cut, F.OWNPVIPCHI2 < bpvipchi2_max)
    return ParticleCombiner(
        [protons, particle1, particle2],
        DecayDescriptor=decay_descriptor,
        name=name,
        CombinationCut=comb_cut,
        CompositeCut=vertex_cut,
    )


def combine_lc_phip(
    phis,
    protons,
    pvs,
    decay_descriptor="[Lambda_c+ -> phi(1020) p+]cc",
    name="Charm_cbaryon_to_phh_combine_lc_pphi_{hash}",
    comb_m_min=2191 * MeV,
    comb_m_max=2381 * MeV,
    m_min=2201 * MeV,
    m_max=2371 * MeV,
    pt_min=2 * GeV,
    sum_pt_min=4 * GeV,
    comb_p_min=15 * GeV,
    p_min=16 * GeV,
    doca_max=500 * um,
    vchi2pdof_max=6.0,
    bpvvdz_min=0.5 * mm,
    bpvfdchi2_min=6.0,
    bpvdira_min=0.99,
    bpvipchi2_max=30,
):
    """Combine protons and hh to form Lc+.
    Make MASS, P, PT, SUM(PT), MAXDOCACUT cuts in the combination;
    MASS, P, PT, CHI2DOF, BPVVDZ, BPVFDCHI2, BPVIPCHI2, BPVDIRA cuts after the vertex fit.
    Cuts generally based on Run2 Turbo lines.
    """
    comb_cut = F.require_all(
        F.MAXSDOCACUT(doca_max),
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.SUM(F.PT) > sum_pt_min,
        F.P > comb_p_min,
    )
    vertex_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.P > p_min,
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVVDZ > bpvvdz_min,
        F.OWNPVFDCHI2 > bpvfdchi2_min,  # <-- to be checked
    )
    if bpvdira_min is not None:
        vertex_cut = F.require_all(vertex_cut, F.OWNPVDIRA > bpvdira_min)
    if bpvipchi2_max is not None:
        vertex_cut = F.require_all(vertex_cut, F.OWNPVIPCHI2 < bpvipchi2_max)
    return ParticleCombiner(
        [phis, protons],
        DecayDescriptor=decay_descriptor,
        name=name,
        CombinationCut=comb_cut,
        CompositeCut=vertex_cut,
    )


##################
## Lb+ combiner ##
##################
# based on SL make_b2xlnu builder
def combine_lb_lclnu(
    lc,
    muons,
    pvs,
    decay_descriptor,
    name="Charm_LcToPHH_LbToLcLNu_{hash}",
    make_pvs=make_pvs,
    comb_m_min=2200 * MeV,
    comb_m_max=8000 * MeV,
    comb_docachi2_max=10.0,
    mother_m_min=2200 * MeV,
    mother_m_max=8000 * MeV,
    vchi2pdof_max=9.0,
    bpvdira_min=0.99,
    pt_min=0 * MeV,
    p_min=0 * MeV,
    bpvvdz_min=None,
):
    """Base SL b-hadron decay builder."""
    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.MAXSDOCACHI2CUT(comb_docachi2_max),
        F.PT > pt_min,
        F.P > p_min,
    )
    vertex_cut = F.require_all(
        in_range(mother_m_min, F.MASS, mother_m_max),
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVDIRA > bpvdira_min,
    )

    if bpvvdz_min is not None:
        vertex_cut = F.require_all(vertex_cut, F.OWNPVVDZ > bpvvdz_min)

    return ParticleCombiner(
        [lc, muons],
        DecayDescriptor=decay_descriptor,
        name=name,
        CombinationCut=combination_code,
        CompositeCut=vertex_cut,
    )


###########################
## Hlt2 lines definition ##
###########################
@register_line_builder(all_lines)
def lc_to_pkmkp_line(name="Hlt2Charm_LcpToPpKmKp", prescale=1):
    pvs = make_pvs()
    long_protons = filter_long_protons(pvs)
    long_kaons = filter_long_kaons(pvs)
    lc_pkmkp = combine_lc_phh(
        long_protons, long_kaons, long_kaons, pvs, "[Lambda_c+ -> p+ K- K+]cc"
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [lc_pkmkp], prescale=prescale)


@register_line_builder(all_lines)
def lc_to_pkppim_line(name="Hlt2Charm_LcpToPpKpPim", prescale=1):
    pvs = make_pvs()
    long_protons = filter_long_protons(pvs)
    long_kaons = filter_long_kaons(pvs)
    long_pions = filter_long_pions(pvs)
    lc_pkppim = combine_lc_phh(
        long_protons, long_kaons, long_pions, pvs, "[Lambda_c+ -> p+ K+ pi-]cc"
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [lc_pkppim], prescale=prescale)


@register_line_builder(all_lines)
def lc_to_ppimpip_line(name="Hlt2Charm_LcpToPpPimPip", prescale=1):
    pvs = make_pvs()
    long_protons = filter_long_protons(pvs)
    long_pions = filter_long_pions(pvs)
    lc_ppimpip = combine_lc_phh(
        long_protons, long_pions, long_pions, pvs, "[Lambda_c+ -> p+ pi- pi+]cc"
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lc_ppimpip], prescale=prescale
    )


@register_line_builder(all_lines)
def xicp_to_pkmkp_line(name="Hlt2Charm_XicpToPpKmKp", prescale=1):
    pvs = make_pvs()
    long_protons = filter_long_protons(pvs)
    long_kaons = filter_long_kaons(pvs)
    xicp_pkmkp = combine_lc_phh(
        long_protons,
        long_kaons,
        long_kaons,
        pvs,
        "[Xi_c+ -> p+ K- K+]cc",
        comb_m_min=2390 * MeV,
        comb_m_max=2517 * MeV,
        m_min=2400 * MeV,
        m_max=2507 * MeV,
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [xicp_pkmkp], prescale=prescale
    )


@register_line_builder(all_lines)
def xicp_to_ppimpip_line(name="Hlt2Charm_XicpToPpPimPip", prescale=1):
    pvs = make_pvs()
    long_protons = filter_long_protons(pvs)
    long_pions = filter_long_pions(pvs)
    xicp_ppimpip = combine_lc_phh(
        long_protons,
        long_pions,
        long_pions,
        pvs,
        "[Xi_c+ -> p+ pi- pi+]cc",
        comb_m_min=2390 * MeV,
        comb_m_max=2517 * MeV,
        m_min=2400 * MeV,
        m_max=2507 * MeV,
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [xicp_ppimpip], prescale=prescale
    )


@register_line_builder(all_lines)
def lc_to_phip_line(name="Hlt2Charm_LcpToPhiPp", prescale=1):
    pvs = make_pvs()
    phi = combine_phi_kk(pvs)
    long_protons = filter_long_protons(pvs)
    lc_phip = combine_lc_phip(phi, long_protons, pvs)
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [phi, lc_phip], prescale=prescale
    )


### b-decays


@register_line_builder(all_lines)
def lb_to_lcmunu_c_to_pkmkp_line(
    name="Hlt2Charm_Lb0ToLcpMumNu_LcpToPpKmKp", prescale=1
):
    pvs = make_pvs()
    long_muons = filter_muons_from_b(pvs)
    long_protons = filter_long_protons(pvs, pt_min=400 * MeV, p_min=2 * GeV)
    long_kaons = filter_long_kaons(pvs, pt_min=400 * MeV, p_min=2 * GeV)
    lc_pkmkp = combine_lc_phh(
        long_protons,
        long_kaons,
        long_kaons,
        pvs,
        "[Lambda_c+ -> p+ K- K+]cc",
        sum_pt_min=2.5 * GeV,
        bpvdira_min=None,
        bpvipchi2_max=None,
    )
    lb_lcmunu = combine_lb_lclnu(
        lc_pkmkp, long_muons, pvs, "[Lambda_b0 -> Lambda_c+ mu-]cc"
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lc_pkmkp, lb_lcmunu], prescale=prescale
    )


@register_line_builder(all_lines)
def lb_to_lcmunu_c_to_pkppim_line(
    name="Hlt2Charm_Lb0ToLcpMumNu_LcpToPpKpPim", prescale=1
):
    pvs = make_pvs()
    long_muons = filter_muons_from_b(pvs)
    long_protons = filter_long_protons(pvs, pt_min=400 * MeV, p_min=2 * GeV)
    long_kaons = filter_long_kaons(pvs, pt_min=400 * MeV, p_min=2 * GeV)
    long_pions = filter_long_pions(pvs, pt_min=400 * MeV, p_min=2 * GeV)
    lc_pkppim = combine_lc_phh(
        long_protons,
        long_kaons,
        long_pions,
        pvs,
        "[Lambda_c+ -> p+ K+ pi-]cc",
        sum_pt_min=2.5 * GeV,
        bpvdira_min=None,
        bpvipchi2_max=None,
    )
    lb_lcmunu = combine_lb_lclnu(
        lc_pkppim, long_muons, pvs, "[Lambda_b0 -> Lambda_c+ mu-]cc"
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lc_pkppim, lb_lcmunu], prescale=prescale
    )


@register_line_builder(all_lines)
def lb_to_lcmunu_c_to_ppimpip_line(
    name="Hlt2Charm_Lb0ToLcpMumNu_LcpToPpPimPip", prescale=1
):
    pvs = make_pvs()
    long_muons = filter_muons_from_b(pvs)
    long_protons = filter_long_protons(pvs, pt_min=400 * MeV, p_min=2 * GeV)
    long_pions = filter_long_pions(pvs, pt_min=400 * MeV, p_min=2 * GeV)
    lc_ppimpip = combine_lc_phh(
        long_protons,
        long_pions,
        long_pions,
        pvs,
        "[Lambda_c+ -> p+ pi- pi+]cc",
        sum_pt_min=2.5 * GeV,
        bpvdira_min=None,
        bpvipchi2_max=None,
    )
    lb_lcmunu = combine_lb_lclnu(
        lc_ppimpip, long_muons, pvs, "[Lambda_b0 -> Lambda_c+ mu-]cc"
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lc_ppimpip, lb_lcmunu], prescale=prescale
    )


@register_line_builder(all_lines)
def lb_to_xicpmunu_c_to_pkmkp_line(
    name="Hlt2Charm_Xib0ToXicpMumNu_XicpToPpKmKp", prescale=1
):
    pvs = make_pvs()
    long_muons = filter_muons_from_b(pvs)
    long_protons = filter_long_protons(pvs, pt_min=400 * MeV, p_min=2 * GeV)
    long_kaons = filter_long_kaons(pvs, pt_min=400 * MeV, p_min=2 * GeV)
    xicp_pkmkp = combine_lc_phh(
        long_protons,
        long_kaons,
        long_kaons,
        pvs,
        "[Xi_c+ -> p+ K- K+]cc",
        comb_m_min=2390 * MeV,
        comb_m_max=2517 * MeV,
        m_min=2400 * MeV,
        m_max=2507 * MeV,
        sum_pt_min=2.5 * GeV,
        bpvdira_min=None,
        bpvipchi2_max=None,
    )
    lb_lcmunu = combine_lb_lclnu(xicp_pkmkp, long_muons, pvs, "[Xi_b0 -> Xi_c+ mu-]cc")
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [xicp_pkmkp, lb_lcmunu], prescale=prescale
    )


@register_line_builder(all_lines)
def lb_to_xicpmunu_c_to_ppimpip_line(
    name="Hlt2Charm_Xib0ToXicpMumNu_XicpToPpPimPip", prescale=1
):
    pvs = make_pvs()
    long_muons = filter_muons_from_b(pvs)
    long_protons = filter_long_protons(pvs, pt_min=400 * MeV, p_min=2 * GeV)
    long_pions = filter_long_pions(pvs, pt_min=400 * MeV, p_min=2 * GeV)
    xicp_ppimpip = combine_lc_phh(
        long_protons,
        long_pions,
        long_pions,
        pvs,
        "[Xi_c+ -> p+ pi- pi+]cc",
        comb_m_min=2390 * MeV,
        comb_m_max=2517 * MeV,
        m_min=2400 * MeV,
        m_max=2507 * MeV,
        sum_pt_min=2.5 * GeV,
        bpvdira_min=None,
        bpvipchi2_max=None,
    )
    lb_lcmunu = combine_lb_lclnu(
        xicp_ppimpip, long_muons, pvs, "[Xi_b0 -> Xi_c+ mu-]cc"
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xicp_ppimpip, lb_lcmunu],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def lb_to_lcmunu_c_to_phip_line(name="Hlt2Charm_Lb0ToLcpMumNu_LcpToPhiPp", prescale=1):
    pvs = make_pvs()
    long_muons = filter_muons_from_b(pvs)
    phis = combine_phi_kk(pvs)
    long_protons = filter_long_protons(pvs, pt_min=400 * MeV, p_min=2 * GeV)
    lc_phip = combine_lc_phip(
        phis,
        long_protons,
        pvs,
        sum_pt_min=2.5 * GeV,
        bpvdira_min=None,
        bpvipchi2_max=None,
    )
    lb_lcmunu = combine_lb_lclnu(
        lc_phip, long_muons, pvs, "[Lambda_b0 -> Lambda_c+ mu-]cc"
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [lc_phip, lb_lcmunu], prescale=prescale
    )
