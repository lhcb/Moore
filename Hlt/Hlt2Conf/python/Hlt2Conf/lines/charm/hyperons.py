###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of Lambda_c+/Xi_c+/Xi_c0/Omega_c0 -> Lambda/Xi-/Omega- X HLT2 lines.
Most decays are prompt, some have a dedicated from-b selection.

inclusive hyperon lines
-----------------------
- Xi- -> L pi-
  Hlt2Charm_XimToL0Pim_VeloMatch_SP
- Omega- -> L K-
  Hlt2Charm_OmToL0Km_VeloMatch_SP
- Sigma+ -> p mu mu
  Hlt2Charm_SpToPpMumMup_VeloMatch_SP
- Xi- -> Lambda mu nu
  Hlt2Charm_XimToL0MumNu_VeloMatch_SP

(quasi) 2-body charm lines
--------------------------
- Lc -> L pi (+ S0 pi)
  Hlt2Charm_LcpToL0Pip_LL
  Hlt2Charm_LcpToL0Pip_DD
  Hlt2Charm_LcpToL0Pip_LL_Inclb_SP
  Hlt2Charm_LcpToL0Pip_DD_Inclb_SP
  Hlt2Charm_Lb0ToLcpDm_LcpToL0Pip_DmToKpPimPim_LL
  Hlt2Charm_Lb0ToLcpDm_LcpToL0Pip_DmToKpPimPim_DD
  Hlt2Charm_Lb0ToLcpDsm_LcpToL0Pip_DmDsmToKmKpPim_LL
  Hlt2Charm_Lb0ToLcpDsm_LcpToL0Pip_DmDsmToKmKpPim_DD
- Lc -> L K (+ S0 K), Xic0 -> L K (+ S0 K)  (author: Miroslav Saur)
  Hlt2Charm_LcpXicpToL0Kp_LL
  Hlt2Charm_LcpXicpToL0Kp_DD
  Hlt2Charm_LcpToL0Kp_LL_Inclb_SP
  Hlt2Charm_LcpToL0Kp_DD_Inclb_SP
- Xic+ -> L pi (+ S0 pi)
  Hlt2Charm_XicpToL0Pip_LL
  Hlt2Charm_XicpToL0Pip_DD
  Hlt2Charm_XicpToL0Pip_LL_Inclb_SP
  Hlt2Charm_XicpToL0Pip_DD_Inclb_SP
  Hlt2Charm_Xib0ToXicpDm_XicpToL0Pip_DmToKpPimPim_LL
  Hlt2Charm_Xib0ToXicpDm_XicpToL0Pip_DmToKpPimPim_DD
  Hlt2Charm_Xib0ToXicpDsm_XicpToL0Pip_DmDsmToKmKpPim_LL
  Hlt2Charm_Xib0ToXicpDsm_XicpToL0Pip_DmDsmToKmKpPim_DD
- Xic0 -> Sigma- pi+
  Hlt2Charm_Xic0ToSmPip_LongSm_Inclb_SP
- Xic0 -> Xi pi
  Hlt2Charm_Xic0ToXimPip_LLL
  Hlt2Charm_Xic0ToXimPip_DDL
  Hlt2Charm_Xic0ToXimPip_LLL_Inclb_SP
  Hlt2Charm_Xic0ToXimPip_DDL_Inclb_SP
  Hlt2Charm_XibmToXic0Dm_Xic0ToXimPip_DmToKpPimPim_LLL
  Hlt2Charm_XibmToXic0Dm_Xic0ToXimPip_DmToKpPimPim_DDL
  Hlt2Charm_XibmToXic0Dsm_Xic0ToXimPip_DmDsmToKmKpPim_LLL
  Hlt2Charm_XibmToXic0Dsm_Xic0ToXimPip_DmDsmToKmKpPim_DDL
- Xic0 -> Xi K
  Hlt2Charm_Xic0ToXimKp_LLL
  Hlt2Charm_Xic0ToXimKp_DDL
  Hlt2Charm_Xic0ToXimKp_LLL_Inclb_SP
  Hlt2Charm_Xic0ToXimKp_DDL_Inclb_SP
  Hlt2Charm_XibmToXic0Dm_Xic0ToXimKp_DmToKpPimPim_LLL
  Hlt2Charm_XibmToXic0Dm_Xic0ToXimKp_DmToKpPimPim_DDL
  Hlt2Charm_XibmToXic0Dsm_Xic0ToXimKp_DmDsmToKmKpPim_LLL
  Hlt2Charm_XibmToXic0Dsm_Xic0ToXimKp_DmDsmToKmKpPim_DDL
- Xic0 -> O K
  Hlt2Charm_Xic0ToOmKp_LLL
  Hlt2Charm_Xic0ToOmKp_DDL
- Xic0 -> L0 KS0  (author: Miroslav Saur)
  Hlt2Charm_Xic0ToL0Ks_LLLL
  Hlt2Charm_Xic0ToL0Ks_DDLL
  Hlt2Charm_Xic0ToL0Ks_LLDD
  Hlt2Charm_Xic0ToL0Ks_DDDD
- Oc -> Xi pi
  Hlt2Charm_Oc0ToXimPip_LLL
  Hlt2Charm_Oc0ToXimPip_DDL
- Oc -> Xi K
  Hlt2Charm_Oc0ToXimKp_LLL
  Hlt2Charm_Oc0ToXimKp_DDL
  Hlt2Charm_Oc0ToXimKp_LLL_Inclb_SP
  Hlt2Charm_Oc0ToXimKp_DDL_Inclb_SP
  Hlt2Charm_ObmToOc0Dm_Oc0ToXimKp_DmToKpPimPim_LLL
  Hlt2Charm_ObmToOc0Dm_Oc0ToXimKp_DmToKpPimPim_DDL
  Hlt2Charm_ObmToOc0Dsm_Oc0ToXimKp_DmDsmToKmKpPim_LLL
  Hlt2Charm_ObmToOc0Dsm_Oc0ToXimKp_DmDsmToKmKpPim_DDL
- Oc -> O pi
  Hlt2Charm_Oc0ToOmPip_LLL
  Hlt2Charm_Oc0ToOmPip_DDL
  Hlt2Charm_Oc0ToOmPip_LLL_Inclb_SP
  Hlt2Charm_Oc0ToOmPip_DDL_Inclb_SP
  Hlt2Charm_ObmToOc0Dm_Oc0ToOmPip_DmToKpPimPim_LLL
  Hlt2Charm_ObmToOc0Dm_Oc0ToOmPip_DmToKpPimPim_DDL
  Hlt2Charm_ObmToOc0Dsm_Oc0ToOmPip_DmDsmToKmKpPim_LLL
  Hlt2Charm_ObmToOc0Dsm_Oc0ToOmPip_DmDsmToKmKpPim_DDL
- Oc -> O K
  Hlt2Charm_Oc0ToOmKp_LLL
  Hlt2Charm_Oc0ToOmKp_DDL
- Oc -> L0 KS0  (author: Miroslav Saur)
  Hlt2Charm_Oc0ToL0Ks_LLLL
  Hlt2Charm_Oc0ToL0Ks_DDLL
  Hlt2Charm_Oc0ToL0Ks_LLDD
  Hlt2Charm_Oc0ToL0Ks_DDDD

(quasi) 3-body charm lines
--------------------------
- Lc+ -> Sigma+ pi+ pi- (long Sigma+) (author : Theraa TORK)
  Hlt2Charm_LcpToSpPimPip_LongSp
- Lc+ -> Sigma- pi+ pi+ (long Sigma-) (author : Theraa TORK)
  Hlt2Charm_LcpToSmPipPip_LongSm
- Omega- -> Xi- pi- pi+ (long Xi-)
  Hlt2Charm_OmToXimPimPip_LongXim_SP
- Lc -> L Ks K (+ S0 Ks K)
  Hlt2Charm_LcpToL0KsKp_LLLL
  Hlt2Charm_LcpToL0KsKp_DDLL
  Hlt2Charm_LcpToL0KsKp_LLDD
- Lc -> Sigma- pi+ pi+ (long Sigma-, from b)
  Hlt2Charm_Lb0ToLcpPim_LcpToSmPipPip_LongSm_SP
- Lc -> Xi K pi
  Hlt2Charm_LcpToXimKpPip_LLL
  Hlt2Charm_LcpToXimKpPip_DDL
- Xic+ -> L Ks pi (+ S0 Ks pi)
  Hlt2Charm_XicpToL0KsPip_LLLL
  Hlt2Charm_XicpToL0KsPip_DDLL
  Hlt2Charm_XicpToL0KsPip_LLDD
- Xic+ -> L Ks K (+ S0 Ks K)
  Hlt2Charm_XicpToL0KsKp_LLLL
  Hlt2Charm_XicpToL0KsKp_DDLL
  Hlt2Charm_XicpToL0KsKp_LLDD
- Xic+ -> Xi pi pi
  Hlt2Charm_XicpToXimPipPip_LLL
  Hlt2Charm_XicpToXimPipPip_DDL
  Hlt2Charm_XicpToXimPipPip_LLL_Inclb_SP
  Hlt2Charm_XicpToXimPipPip_DDL_Inclb_SP
  Hlt2Charm_Xib0ToXicpDm_XicpToXimPipPip_DmToKpPimPim_LLL
  Hlt2Charm_Xib0ToXicpDm_XicpToXimPipPip_DmToKpPimPim_DDL
  Hlt2Charm_Xib0ToXicpDsm_XicpToXimPipPip_DmDsmToKmKpPim_LLL
  Hlt2Charm_Xib0ToXicpDsm_XicpToXimPipPip_DmDsmToKmKpPim_DDL
  Hlt2Charm_Xib0ToXicpPim_XicpToXimPipPip_LongXi_SP
- Xic+ -> Xi K pi
  Hlt2Charm_XicpToXimKpPip_LLL
  Hlt2Charm_XicpToXimKpPip_DDL
- Xic+ -> O K pi
  Hlt2Charm_XicpToOmKpPip_LLL
  Hlt2Charm_XicpToOmKpPip_DDL
  Hlt2Charm_Xib0ToXicpPim_XicpToOmKpPip_LongOm_SP
- Xic0 -> L pi pi (+ S0 pi pi)
  Hlt2Charm_Xic0ToL0PimPip_LL
  Hlt2Charm_Xic0ToL0PimPip_DD
- Xic0 -> L K- pi+ (+ S0 K- pi+), Oc -> L K pi (+ S0 K pi)
  Hlt2Charm_Xic0Oc0ToL0KmPip_LL
  Hlt2Charm_Xic0Oc0ToL0KmPip_DD
  Hlt2Charm_Xic0ToL0KmPip_LL_Inclb_SP
  Hlt2Charm_Xic0ToL0KmPip_DD_Inclb_SP
  Hlt2Charm_XibmToXic0Dm_Xic0ToL0KmPip_DmToKpPimPim_DD
  Hlt2Charm_XibmToXic0Dm_Xic0ToL0KmPip_DmToKpPimPim_LL
  Hlt2Charm_XibmToXic0Dsm_Xic0ToL0KmPip_DmDsmToKmKpPim_DD
  Hlt2Charm_XibmToXic0Dsm_Xic0ToL0KmPip_DmDsmToKmKpPim_LL
- Xic0 -> L K K (+ S0 K K), Oc -> L K K (+ S0 K K)
  Hlt2Charm_Xic0Oc0ToL0KmKp_LL
  Hlt2Charm_Xic0Oc0ToL0KmKp_DD
- Xi_c0 -> Sigma- KS pi+
  Hlt2Charm_Xic0ToSmKsPip_LL_LongSm_Inclb_SP
  Hlt2Charm_Xic0ToSmKsPip_DD_LongSm_Inclb_SP
- Xic0 -> Xi Ks pi, Oc0 -> Xi Ks pi
  Hlt2Charm_Xic0Oc0ToXimKsPip_LLLLL
  Hlt2Charm_Xic0Oc0ToXimKsPip_LLLDD
  Hlt2Charm_Xic0Oc0ToXimKsPip_DDLLL
  Hlt2Charm_Xic0Oc0ToXimKsPip_DDLDD
- Xic0 -> Xi Ks K
  Hlt2Charm_Xic0ToXimKsKp_LLLLL
  Hlt2Charm_Xic0ToXimKsKp_LLLDD
  Hlt2Charm_Xic0ToXimKsKp_DDLLL
  Hlt2Charm_Xic0ToXimKsKp_DDLDD

(quasi) 4-body charm lines
--------------------------
- Lc -> L pi- pi+ pi+
  Hlt2Charm_LcpToL0PimPipPip_LL  (author: Miguel Rebollo De Miguel)
  Hlt2Charm_LcpToL0PimPipPip_DD  (author: Miguel Rebollo De Miguel)
- Lc -> L K- K+ pi+, Xic+ -> L K- K+ pi+ (+ S0 K- K+ pi+)
  Hlt2Charm_LcpXicpToL0KmKpPip_LL
  Hlt2Charm_LcpXicpToL0KmKpPip_DD
- Lc -> Xi Ks pi pi
  Hlt2Charm_LcpToXimKsPipPip_LLLLL
  Hlt2Charm_LcpToXimKsPipPip_LLLDD
  Hlt2Charm_LcpToXimKsPipPip_DDLLL
  Hlt2Charm_LcpToXimKsPipPip_DDLDD
- Xic+ -> L K pi+ pi+ (+ S0 K pi+ pi+)
  Hlt2Charm_XicpToL0KmPipPip_LL
  Hlt2Charm_XicpToL0KmPipPip_DD
- Xic0 -> L Ks pi pi (+ S0 Ks pi pi)
  Hlt2Charm_Xic0ToL0KsPimPip_LLLL
  Hlt2Charm_Xic0ToL0KsPimPip_LLDD
  Hlt2Charm_Xic0ToL0KsPimPip_DDLL
- Xic0 -> Xi 3pi
  Hlt2Charm_Xic0ToXimPimPipPip_LLL
  Hlt2Charm_Xic0ToXimPimPipPip_DDL
- Xic0 -> O K+ pi+ pi-, Oc -> O K+ pi+ pi-
  Hlt2Charm_Xic0Oc0ToOmKpPimPip_LLL
  Hlt2Charm_Xic0Oc0ToOmKpPimPip_DDL
- Oc -> L Ks K- pi+ (+ S0 Ks K- pi+)
  Hlt2Charm_Oc0ToL0KsKmPip_LLLL
  Hlt2Charm_Oc0ToL0KsKmPip_LLDD
  Hlt2Charm_Oc0ToL0KsKmPip_DDLL
- Oc -> Xi K- pi+ pi+
  Hlt2Charm_Oc0ToXimKmPipPip_LLL
  Hlt2Charm_Oc0ToXimKmPipPip_DDL
- Oc -> O 3pi
  Hlt2Charm_Oc0ToOmPimPipPip_LLL
  Hlt2Charm_Oc0ToOmPimPipPip_DDL
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, ps
from GaudiKernel.SystemOfUnits import micrometer as um
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf.Algorithms import MatchVeloTrackToVertex
from PyConf.Tools import ParticleVertexFitter
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.hlt2_tracking import (
    get_track_master_extrapolator,
    get_track_master_fitter,
)
from RecoConf.legacy_rec_hlt1_tracking import all_velo_track_types
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    make_down_muons,
    make_has_rich_down_kaons,
    make_has_rich_down_pions,
    make_has_rich_down_protons,
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
    make_has_rich_long_protons,
    make_long_omegas,
    make_long_pions,
    make_long_sigmams,
    make_long_sigmaps,
    make_long_xis,
)

from .prefilters import charm_prefilters


# re-definitions of functors
def _DZ_CHILD(i):
    return F.CHILD(i, F.END_VZ) - F.END_VZ


def _match_ddd_hyperon_to_velo_track(hyperon, velo_tracks=all_velo_track_types):
    return MatchVeloTrackToVertex(
        SeedParticles=hyperon,
        VeloTracks=velo_tracks()["v1"],
        PrimaryVertices=make_pvs(),
        VeloMatchIP=3 * mm,
        EtaTolerance=0.5,
        VeloMatchChi2=48.0,
        TrackFitter=get_track_master_fitter(),
        TrackExtrapolator=get_track_master_extrapolator(),
    ).OutputParticles


########################################################################
## Local builders for (combined) particles used throughout the module ##
########################################################################
# Some selections are used multiple times throughout the module.
# We define their builders upfront here. These builders are local
# and follow the PEP 8 Style Guide for Python Code
# https://www.python.org/dev/peps/pep-0008/#descriptive-naming-styles
#  >  _single_leading_underscore: weak "internal use" indicator.
#     E.g. from M import * does not import objects whose names
#     start with an underscore.
#
# The cut values are set in the body of the builders,
# such that they are immutable.
# This ensures that:
# - Only one filter or combiner instance is created and called.
#   This speeds up the selection.
# - Cut values are set only once and cannot be overwritten.
#   The code is easier to read and less error-prone.
#
# Some builders are standalone and not part of the "configuration-flow"
# mentioned in the style-guidelines used for Moore/RecoConf:
# https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/recoconf/recoconf.html
# The idea behind this is to re-define basic particles as starting point
# for selections in the module. These builders use proto-particle-makers
# and pvs directly in the body of the function. Other common selections,
# like the Xi- builders consume "rare" input particles.
# These rare input particles are intended to be used to configure the
# control flow and speed up selections.
########################################################################


def _make_long_pions_for_lambda():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 80 * MeV,
                F.OWNPVIPCHI2 > 32.0,
                F.OWNPVIP > 200 * um,
                F.PROBNN_PI > 0.01,
            )
        ),
    )


def _make_long_pions_for_kshort():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 100 * MeV, F.OWNPVIPCHI2 > 24.0, F.PROBNN_PI > 0.03)
        ),
    )


def _make_long_pions_for_xi():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 100 * MeV,
                F.OWNPVIPCHI2 > 24.0,
                F.OWNPVIP > 150 * um,
                F.PROBNN_PI > 0.3,
            )
        ),
    )


def _make_long_pions_for_d():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 180 * MeV, F.OWNPVIPCHI2 > 12.0, F.PROBNN_PI > 0.1)
        ),
    )


def _make_down_pions_for_kshort():
    return ParticleFilter(
        make_has_rich_down_pions(),
        F.FILTER(F.require_all(F.PT > 150 * MeV, F.PROBNN_PI > 0.1)),
    )


def _make_down_pions_for_lambda():
    return ParticleFilter(
        make_has_rich_down_pions(),
        F.FILTER(F.require_all(F.PT > 100 * MeV, F.PROBNN_PI > 0.4)),
    )


def _make_long_kaons_for_omega():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 180 * MeV, F.OWNPVIPCHI2 > 24.0, F.PROBNN_K > 0.1)
        ),
    )


def _make_down_kaons_for_omega():
    return ParticleFilter(
        make_has_rich_down_kaons(),
        F.FILTER(F.require_all(F.PT > 200 * MeV, F.PROBNN_K > 0.1)),
    )


def _make_long_kaons_for_d():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 240 * MeV, F.OWNPVIPCHI2 > 9.0, F.PROBNN_K > 0.3)
        ),
    )


def _make_long_protons_for_lambda():
    return ParticleFilter(
        make_has_rich_long_protons(),
        F.FILTER(
            F.require_all(
                F.PT > 450 * MeV,
                F.OWNPVIPCHI2 > 12.0,
                F.OWNPVIP > 80 * um,
                F.PROBNN_P > 0.01,
            )
        ),
    )


def _make_down_protons_for_lambda():
    return ParticleFilter(
        make_has_rich_down_protons(),
        F.FILTER(F.require_all(F.PT > 600 * MeV, F.PROBNN_P > 0.01)),
    )


def _make_ll_kshorts():
    return ParticleCombiner(
        [_make_long_pions_for_kshort(), _make_long_pions_for_kshort()],
        DecayDescriptor="KS0 -> pi+ pi-",
        name="Charm_Hyperons_KS0_LL_{hash}",
        CombinationCut=F.require_all(
            F.math.in_range(435 * MeV, F.MASS, 560 * MeV),
            F.PT > 400 * MeV,
            F.MAX(F.PT) > 400 * MeV,
            F.MAX(F.OWNPVIPCHI2) > 64.0,
            F.MAXDOCACUT(150 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(455 * MeV, F.MASS, 540 * MeV),
            F.PT > 500 * MeV,
            F.math.in_range(-190 * mm, F.END_VZ, 650 * mm),
            F.CHI2DOF < 12.0,
            F.OWNPVVDZ > 4 * mm,
            F.OWNPVVDRHO > 0.7 * mm,
            F.OWNPVIP > 32 * um,
        ),
    )


def _make_dd_kshorts():
    return ParticleCombiner(
        [_make_down_pions_for_kshort(), _make_down_pions_for_kshort()],
        DecayDescriptor="KS0 -> pi+ pi-",
        name="Charm_Hyperons_KS0_DD_{hash}",
        CombinationCut=F.require_all(
            F.math.in_range(425 * MeV, F.MASS, 570 * MeV),
            F.PT > 500 * MeV,
            F.MAXDOCACUT(5 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(455 * MeV, F.MASS, 540 * MeV),
            F.PT > 600 * MeV,
            F.CHI2DOF < 16.0,
            F.math.in_range(250 * mm, F.END_VZ, 2485 * mm),
            F.OWNPVVDRHO > 12 * mm,
        ),
    )


def _make_ll_lambdas_for_charm():
    return ParticleCombiner(
        [_make_long_protons_for_lambda(), _make_long_pions_for_lambda()],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name="Charm_Hyperons_LambdaFromC_LL_{hash}",
        CombinationCut=F.require_all(
            F.CHILD(1, F.PT) > 1.2 * GeV,
            F.CHILD(2, F.PT) > 130 * MeV,
            F.CHILD(2, F.OWNPVIPCHI2) > 64.0,
            F.math.in_range(1075 * MeV, F.MASS, 1160 * MeV),
            F.PT > 1.3 * GeV,
            F.MAXDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1090 * MeV, F.MASS, 1140 * MeV),
            F.PT > 1.4 * GeV,
            F.math.in_range(-180 * mm, F.END_VZ, 650 * mm),
            F.CHI2DOF < 16.0,
            F.OWNPVVDZ > 12 * mm,
            F.OWNPVVDRHO > 2 * mm,
            F.OWNPVDIRA > 0.99999,
        ),
    )


def _make_ll_lambdas_for_hyperon():
    return ParticleCombiner(
        [_make_long_protons_for_lambda(), _make_long_pions_for_lambda()],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name="Charm_Hyperons_LambdaFromHyperon_LL_{hash}",
        CombinationCut=F.require_all(
            F.math.in_range(1075 * MeV, F.MASS, 1160 * MeV),
            F.PT > 550 * MeV,
            F.MAXDOCACUT(120 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1090 * MeV, F.MASS, 1140 * MeV),
            F.PT > 600 * MeV,
            F.math.in_range(-180 * mm, F.END_VZ, 650 * mm),
            F.CHI2DOF < 16.0,
            F.OWNPVVDZ > 24 * mm,
            F.OWNPVVDRHO > 3 * mm,
            F.OWNPVDIRA > 0.9999,
            F.OWNPVIP > 32 * um,
        ),
    )


def _make_dd_lambdas():
    return ParticleCombiner(
        [_make_down_protons_for_lambda(), _make_down_pions_for_lambda()],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name="Charm_Hyperons_Lambda_DD_{hash}",
        CombinationCut=F.require_all(
            F.math.in_range(1075 * MeV, F.MASS, 1160 * MeV),
            F.PT > 700 * MeV,
            F.MAXDOCACUT(5 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1090 * MeV, F.MASS, 1140 * MeV),
            F.PT > 800 * MeV,
            F.math.in_range(250 * mm, F.END_VZ, 2485 * mm),
            F.OWNPVVDRHO > 12 * mm,
            F.CHI2DOF < 16.0,
        ),
    )


# This will always ever be called with ll_lambdas=_make_ll_lambdas_for_hyperon and _make_long_pions_for_xi.
def _make_lll_xis(ll_lambdas, pions):
    return ParticleCombiner(
        [ll_lambdas, pions],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        name="Charm_Hyperons_Xim_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        CombinationCut=F.require_all(
            F.math.in_range(1272 * MeV, F.MASS, 1372 * MeV),
            F.PT > 650 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1292 * MeV, F.MASS, 1352 * MeV),
            F.PT > 750 * MeV,
            _DZ_CHILD(1) > 4 * mm,
            F.math.in_range(-180 * mm, F.END_VZ, 650 * mm),
            F.CHI2DOF < 16.0,
            F.OWNPVVDZ > 12 * mm,
            F.OWNPVVDRHO > 700 * um,
            F.OWNPVDIRA > 0.9997,
            F.OWNPVIP > 12 * um,
        ),
    )


# This will always ever be called with dd_lambdas=_make_dd_lambdas and _make_long_pions_for_xi.
def _make_ddl_xis(dd_lambdas, pions):
    return ParticleCombiner(
        [dd_lambdas, pions],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        name="Charm_Hyperons_Xim_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        CombinationCut=F.require_all(
            F.math.in_range(1272 * MeV, F.MASS, 1372 * MeV),
            F.PT > 0.9 * GeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1292 * MeV, F.MASS, 1352 * MeV),
            F.PT > 1 * GeV,
            F.CHI2DOF < 16.0,
            F.OWNPVVDZ > 4 * mm,
            F.OWNPVVDRHO > 0.7 * mm,
        ),
    )


# This will always ever be called with dd_lambdas=_make_dd_lambdas.
def _make_ddd_xis(dd_lambdas, down_pions):
    return ParticleCombiner(
        [dd_lambdas, down_pions],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        name="Charm_Hyperons_Xim_DDD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        CombinationCut=F.require_all(
            F.math.in_range(1272 * MeV, F.MASS, 1372 * MeV),
            F.PT > 0.9 * GeV,
            F.MAXDOCACUT(5 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1292 * MeV, F.MASS, 1352 * MeV),
            F.PT > 1 * GeV,
            F.CHI2DOF < 24.0,
            _DZ_CHILD(1) > -5 * mm,
            F.math.in_range(250 * mm, F.END_VZ, 2485 * mm),
            F.OWNPVVDRHO > 6 * mm,
        ),
    )


# This will always ever be called with ll_lambdas=_make_ll_lambdas_for_hyperon.
def _make_lll_omegas(ll_lambdas, kaons):
    return ParticleCombiner(
        [ll_lambdas, kaons],
        DecayDescriptor="[Omega- -> Lambda0 K-]cc",
        name="Charm_Hyperons_Omegam_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        CombinationCut=F.require_all(
            F.math.in_range(1630 * MeV, F.MASS, 1710 * MeV),
            F.PT > 650 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1647 * MeV, F.MASS, 1697 * MeV),
            F.PT > 750 * MeV,
            F.CHI2DOF < 16.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 2 * mm,
            F.OWNPVVDRHO > 0.7 * mm,
            F.OWNPVDIRA > 0.9997,
        ),
    )


# This will always ever be called with dd_lambdas=_make_dd_lambdas.
def _make_ddl_omegas(dd_lambdas, kaons):
    return ParticleCombiner(
        [dd_lambdas, kaons],
        DecayDescriptor="[Omega- -> Lambda0 K-]cc",
        name="Charm_Hyperons_Omegam_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        CombinationCut=F.require_all(
            F.math.in_range(1630 * MeV, F.MASS, 1710 * MeV),
            F.PT > 1.4 * GeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1647 * MeV, F.MASS, 1697 * MeV),
            F.PT > 1.1 * GeV,
            F.CHI2DOF < 16.0,
            F.OWNPVVDZ > 2 * mm,
            F.OWNPVVDRHO > 0.7 * mm,
        ),
    )


# This will always ever be called with dd_lambdas=_make_dd_lambdas.
def _make_ddd_omegas(dd_lambdas, down_kaons):
    return ParticleCombiner(
        [dd_lambdas, down_kaons],
        DecayDescriptor="[Omega- -> Lambda0 K-]cc",
        name="Charm_Hyperons_Omegam_DDD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        CombinationCut=F.require_all(
            F.math.in_range(1630 * MeV, F.MASS, 1710 * MeV),
            F.PT > 0.9 * MeV,
            F.MAXDOCACUT(5 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1647 * MeV, F.MASS, 1697 * MeV),
            F.PT > 1 * GeV,
            F.CHI2DOF < 24.0,
            _DZ_CHILD(1) > -5 * mm,
            F.math.in_range(250 * mm, F.END_VZ, 2485 * mm),
            F.OWNPVVDRHO > 6 * mm,
        ),
    )


def _make_d_to_kpipi():
    pions = _make_long_pions_for_d()
    return ParticleCombiner(
        [_make_long_kaons_for_d(), pions, pions],
        DecayDescriptor="[D+ -> K- pi+ pi+]cc",
        name="Charm_Hyperons_DpToKmPipPip_{hash}",
        Combination12Cut=F.require_all(
            F.MASS < 1850 * MeV,
            F.MAXSDOCACUT(150 * um),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(1750 * MeV, F.MASS, 1990 * MeV),
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            F.MAXSDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1770 * MeV, F.MASS, 1970 * MeV),
            F.PT > 1.6 * GeV,
            F.CHI2DOF < 9.0,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 6.0,
        ),
    )


def _make_ds_to_kkpi():
    kaons = _make_long_kaons_for_d()
    return ParticleCombiner(
        [kaons, kaons, _make_long_pions_for_d()],
        DecayDescriptor="[D_s+ -> K- K+ pi+]cc",
        name="Charm_Hyperons_DspToKmKpPip_{hash}",
        Combination12Cut=F.require_all(
            F.MASS < 1950 * MeV,
            F.MAXSDOCACUT(150 * um),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(1750 * MeV, F.MASS, 2090 * MeV),
            F.PT > 1.6 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXSDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1770 * MeV, F.MASS, 2070 * MeV),
            F.PT > 1.7 * GeV,
            F.CHI2DOF < 9.0,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 5.0,
        ),
    )


#########################################################
## Local bachelor particle filters used at least twice ##
#########################################################
def _make_loose_pions_for_charm():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 180 * MeV, F.OWNPVIPCHI2 > 2.5, F.PROBNN_PI > 0.1)
        ),
    )


def _make_std_pions_for_charm():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 280 * MeV, F.OWNPVIPCHI2 > 4.0, F.PROBNN_PI > 0.1)
        ),
    )


def _make_tight_pions_for_charm():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 400 * MeV, F.OWNPVIPCHI2 > 6.0, F.PROBNN_PI > 0.1)
        ),
    )


def _make_verytight_pions_for_charm():
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 550 * MeV, F.OWNPVIPCHI2 > 9.0, F.PROBNN_PI > 0.1)
        ),
    )


def _make_loose_kaons_for_charm():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 360 * MeV, F.OWNPVIPCHI2 > 3.0, F.PROBNN_K > 0.1)
        ),
    )


def _make_std_kaons_for_charm():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 420 * MeV, F.OWNPVIPCHI2 > 4.0, F.PROBNN_K > 0.3)
        ),
    )


def _make_tight_kaons_for_charm():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 550 * MeV, F.OWNPVIPCHI2 > 6.0, F.PROBNN_K > 0.5)
        ),
    )


def _make_verytight_kaons_for_charm():
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 700 * MeV, F.OWNPVIPCHI2 > 9.0, F.PROBNN_K > 0.7)
        ),
    )


def _make_long_tracks_for_beauty():
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(F.require_all(F.PT > 500 * MeV, F.OWNPVIPCHI2 > 9.0)),
    )


#################################################
## Local "for-b" combiners used at least twice ##
#################################################
def _make_bbaryon_to_cbaryonttrack(cbaryon, track, descriptor, mass_constraints):
    return ParticleCombiner(
        [cbaryon, track],
        DecayDescriptor=descriptor,
        name="Charm_Hyperons_XbToXcTrack_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=mass_constraints),
        CombinationCut=F.require_all(
            F.MASS < 6.4 * GeV,
            F.PT > 2.4 * GeV,
            F.SUM(F.PT) > 4 * GeV,
            F.MAXSDOCACUT(120 * um),
        ),
        CompositeCut=F.require_all(
            F.MASS < 6.3 * GeV,
            F.PT > 2.5 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 0 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 5.0,
        ),
    )


def _make_xib0_to_xicppim(xicp, pim):
    return ParticleCombiner(
        [xicp, pim],
        DecayDescriptor="[Xi_b0 -> Xi_c+ pi-]cc",
        name="Charm_Hyperons_Xib0ToXicpPim_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi_c+"]),
        CombinationCut=F.require_all(
            F.math.in_range(5.3 * GeV, F.MASS, 6.1 * GeV),
            F.PT > 3.4 * GeV,
            F.SUM(F.PT) > 4 * GeV,
            F.MAXSDOCACUT(120 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(5.4 * GeV, F.MASS, 6 * GeV),
            F.PT > 3.6 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 0 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 4.2,
        ),
    )


def _make_bbaryon_to_cbaryond(xc, d, descriptor, mass_constraints):
    return ParticleCombiner(
        [xc, d],
        DecayDescriptor=descriptor,
        name="Charm_Hyperons_XbToXcD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=mass_constraints),
        CombinationCut=F.require_all(
            F.math.in_range(4.9 * GeV, F.MASS, 6.4 * GeV),
            F.PT > 3.8 * GeV,
            F.SUM(F.PT) > 4.5 * GeV,
            F.MAXSDOCACUT(150 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(5 * GeV, F.MASS, 6.3 * GeV),
            F.PT > 4 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 0 * mm,
            _DZ_CHILD(2) > 0.5 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 5.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.999,
        ),
    )


def _make_detached_lc_to_lpi_ll(ll_lambdas, pions):
    return ParticleCombiner(
        [ll_lambdas, pions],
        DecayDescriptor="[Lambda_c+ -> Lambda0 pi+]cc",
        name="Charm_Hyperons_DetachedLcpToL0Pip_LL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        CombinationCut=F.require_all(
            F.math.in_range(2080 * MeV, F.MASS, 2405 * MeV),
            F.PT > 1.9 * GeV,
            F.SUM(F.PT) > 2.8 * GeV,
            F.MAXDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2100 * MeV, F.MASS, 2385 * MeV),
            F.PT > 2 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 8 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 5.0,
        ),
    )


def _make_detached_lc_to_lpi_dd(dd_lambdas, pions):
    return ParticleCombiner(
        [dd_lambdas, pions],
        DecayDescriptor="[Lambda_c+ -> Lambda0 pi+]cc",
        name="Charm_Hyperons_DetachedLcpToL0Pip_DD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        CombinationCut=F.require_all(
            F.math.in_range(2080 * MeV, F.MASS, 2405 * MeV),
            F.PT > 2.1 * GeV,
            F.SUM(F.PT) > 2.8 * GeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.SUM(F.PT) > 2 * GeV,
            F.math.in_range(2100 * MeV, F.MASS, 2385 * MeV),
            F.PT > 2.2 * GeV,
            F.CHI2DOF < 9.0,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 4.5,
        ),
    )


def _make_detached_xicp_to_lpi_ll(ll_lambdas, pions):
    return ParticleCombiner(
        [ll_lambdas, pions],
        DecayDescriptor="[Xi_c+ -> Lambda0 pi+]cc",
        name="Charm_Hyperons_DetachedXicpToL0Pip_LL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        CombinationCut=F.require_all(
            F.math.in_range(2260 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.3 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.MAXDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2280 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2.4 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 8 * mm,
            F.OWNPVVDZ > 1.5 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 6.0,
        ),
    )


def _make_detached_xicp_to_lpi_dd(dd_lambdas, pions):
    return ParticleCombiner(
        [dd_lambdas, pions],
        DecayDescriptor="[Xi_c+ -> Lambda0 pi+]cc",
        name="Charm_Hyperons_DetachedXicpToL0Pip_DD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        CombinationCut=F.require_all(
            F.math.in_range(2360 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.4 * GeV,
            F.SUM(F.PT) > 2.8 * GeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2380 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2.5 * GeV,
            F.CHI2DOF < 9.0,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 5.5,
        ),
    )


def _make_detached_xicz_to_ximpi_lll(lll_xis, pions):
    return ParticleCombiner(
        [lll_xis, pions],
        DecayDescriptor="[Xi_c0 -> Xi- pi+]cc",
        name="Charm_Hyperons_DetachedXic0ToXimPip_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXDOCACUT(150 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 4.8,
        ),
    )


def _make_detached_xicz_to_ximpi_ddl(ddl_xis, pions):
    return ParticleCombiner(
        [ddl_xis, pions],
        DecayDescriptor="[Xi_c0 -> Xi- pi+]cc",
        name="Charm_Hyperons_DetachedXic0ToXimPip_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 4.8,
        ),
    )


def _make_detached_xicz_to_ximk_lll(lll_xis, kaons):
    return ParticleCombiner(
        [lll_xis, kaons],
        DecayDescriptor="[Xi_c0 -> Xi- K+]cc",
        name="Charm_Hyperons_DetachedXic0ToXimKp_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXDOCACUT(150 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 4.8,
        ),
    )


def _make_detached_xicz_to_ximk_ddl(ddl_xis, kaons):
    return ParticleCombiner(
        [ddl_xis, kaons],
        DecayDescriptor="[Xi_c0 -> Xi- K+]cc",
        name="Charm_Hyperons_DetachedXic0ToXimKp_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 4.8,
        ),
    )


def _make_detached_oc_to_ximk_lll(lll_xis, kaons):
    return ParticleCombiner(
        [lll_xis, kaons],
        DecayDescriptor="[Omega_c0 -> Xi- K+]cc",
        name="Charm_Hyperons_DetachedOc0ToXimKp_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXDOCACUT(150 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 5.0,
        ),
    )


def _make_detached_oc_to_ximk_ddl(ddl_xis, kaons):
    return ParticleCombiner(
        [ddl_xis, kaons],
        DecayDescriptor="[Omega_c0 -> Xi- K+]cc",
        name="Charm_Hyperons_DetachedOc0ToXimKp_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDZ > 80 * um,
            F.OWNPVDLS > 5.0,
        ),
    )


def _make_detached_oc_to_ompi_lll(lll_oms, pions):
    return ParticleCombiner(
        [lll_oms, pions],
        DecayDescriptor="[Omega_c0 -> Omega- pi+]cc",
        name="Charm_Hyperons_DetachedOc0ToOmPip_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Omega-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXDOCACUT(150 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 5.0,
        ),
    )


def _make_detached_oc_to_ompi_ddl(ddl_oms, pions):
    return ParticleCombiner(
        [ddl_oms, pions],
        DecayDescriptor="[Omega_c0 -> Omega- pi+]cc",
        name="Charm_Hyperons_DetachedOc0ToOmPip_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Omega-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2.3 * GeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVDLS > 5.0,
        ),
    )


def _make_detached_xicp_to_ximpipi_lll(lll_xis, pions):
    return ParticleCombiner(
        [lll_xis, pions, pions],
        DecayDescriptor="[Xi_c+ -> Xi- pi+ pi+]cc",
        name="Charm_Hyperons_DetachedXicpToXimPipPip_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(200 * um),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.DOCA(1, 3) < 250 * um,
            F.DOCA(2, 3) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.6 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 5.0,
        ),
    )


def _make_detached_xicp_to_ximpipi_ddl(ddl_xis, pions):
    return ParticleCombiner(
        [ddl_xis, pions, pions],
        DecayDescriptor="[Xi_c+ -> Xi- pi+ pi+]cc",
        name="Charm_Hyperons_DetachedXicpToXimPipPip_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.9 * GeV,
            F.SUM(F.PT) > 2.4 * GeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 150 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 5.0,
        ),
    )


def _make_detached_xicz_to_lkpi_ll(ll_lambdas, kaons, pions):
    return ParticleCombiner(
        [ll_lambdas, kaons, pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- pi+]cc",
        name="Charm_Hyperons_DetachedXic0ToL0KmPip_LL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        Combination12Cut=F.MASS < 2450 * MeV,
        CombinationCut=F.require_all(
            F.math.in_range(2360 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.8 * GeV,
            F.SUM(F.PT) > 3.6 * GeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2380 * MeV, F.MASS, 2570 * MeV),
            F.PT > 3 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 1.5 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 4.8,
        ),
    )


def _make_detached_xicz_to_lkpi_dd(dd_lambdas, kaons, pions):
    return ParticleCombiner(
        [dd_lambdas, kaons, pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- pi+]cc",
        name="Charm_Hyperons_DetachedXic0ToL0KmPip_DD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2360 * MeV, F.MASS, 2590 * MeV),
            F.PT > 3 * GeV,
            F.SUM(F.PT) > 4 * GeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 200 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2380 * MeV, F.MASS, 2570 * MeV),
            F.PT > 3.4 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 1.5 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 4.8,
        ),
    )


###################
## trigger lines ##
###################
# Expected rates are simple estimates using as inst. lumi 2*10^33 /(cm^2 s) measured or estimated branching fractions,
# and the following cross-section estimates: Lc 300 mub, Xic 100 mub, Omegac 40 mub (arxiv:2105.05187 and arxiv:2012.12001).
# The cross-sections should be within the acceptance, so we only have to correct for what is reconstructible as LL(L) or DDL.
# For Lambda from charm this is about 60%, Xi- (-> Lambda pi) from c 20%, Omega- (-> Lambda K) from c 40%.
all_lines = {}


###############
## inclusive ##
###############
# Xi- -> L pi- with matched Velo track
@register_line_builder(all_lines)
def xi_to_lpi_match(name="Hlt2Charm_XimToL0Pim_VeloMatch_SP"):
    dd_lambdas = _make_dd_lambdas()
    ddd_xis = _make_ddd_xis(dd_lambdas, _make_down_pions_for_lambda())
    matched_xis = _match_ddd_hyperon_to_velo_track(ddd_xis)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddd_xis, matched_xis],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


# Omega- -> L K- with matched Velo track
@register_line_builder(all_lines)
def om_to_lk_match(name="Hlt2Charm_OmToL0Km_VeloMatch_SP"):
    dd_lambdas = _make_dd_lambdas()
    ddd_omegas = _make_ddd_omegas(dd_lambdas, _make_down_kaons_for_omega())
    matched_omegas = _match_ddd_hyperon_to_velo_track(ddd_omegas)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddd_omegas, matched_omegas],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


# Sigma+ -> p mu mu
@register_line_builder(all_lines)
def sp_to_pmumu_match(name="Hlt2Charm_SpToPpMumMup_VeloMatch_SP"):
    protons = ParticleFilter(
        make_has_rich_down_protons(),
        F.FILTER(
            F.require_all(
                F.PT > 350 * MeV, F.PROBNN_P * (1 - F.PROBNN_K) > 0.05, F.PROBNN_P > 0.1
            )
        ),
    )
    muons = ParticleFilter(
        make_down_muons(),
        F.FILTER(F.require_all(F.ISMUON, F.PT > 80 * MeV, F.PROBNN_MU > 0.2)),
    )
    sigmaps = ParticleCombiner(
        [protons, muons, muons],
        DecayDescriptor="[Sigma+ -> p+ mu- mu+]cc",
        name="Charm_Hyperons_SpToPpMumMup_DDD_{hash}",
        CombinationCut=F.require_all(
            F.MASS < 1270 * MeV,
            F.PT > 500 * MeV,
            F.MAXDOCACUT(5 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 1250 * MeV,
            F.PT > 600 * MeV,
            F.CHI2DOF < 24.0,
            F.math.in_range(250 * mm, F.END_VZ, 2485 * mm),
            F.OWNPVVDRHO > 12 * mm,
        ),
    )
    matched_sigmas = _match_ddd_hyperon_to_velo_track(sigmaps)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [sigmaps, matched_sigmas],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


# Xi- -> Lambda mu nu
@register_line_builder(all_lines)
def xi_to_lmu_match(name="Hlt2Charm_XimToL0MumNu_VeloMatch_SP"):
    muons = ParticleFilter(
        make_down_muons(),
        F.FILTER(F.require_all(F.ISMUON, F.PT > 150 * MeV, F.PROBNN_MU > 0.4)),
    )
    xis = ParticleCombiner(
        [_make_dd_lambdas(), muons],
        DecayDescriptor="[Xi- -> Lambda0 mu-]cc",
        name="Charm_Hyperons_XimToL0MumNu_DDD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        CombinationCut=F.require_all(
            F.MASS < 1400 * MeV,
            F.PT > 1.1 * GeV,
            F.MAXDOCACUT(5 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 1380 * MeV,
            F.PT > 1.2 * GeV,
            F.CHI2DOF < 16.0,
            F.math.in_range(250 * mm, F.END_VZ, 2485 * mm),
            F.OWNPVVDRHO > 10 * mm,
        ),
    )
    matched_xis = _match_ddd_hyperon_to_velo_track(xis)
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xis, matched_xis],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


#######################
## (quasi-) two-body ##
#######################
# Lc -> L pi and partially reconstructed Lc -> Sigma0 pi. Expected rate 6.2 kHz
@register_line_builder(all_lines)
def lc_to_lpi_ll_line(name="Hlt2Charm_LcpToL0Pip_LL"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    lcs = ParticleCombiner(
        [ll_lambdas, _make_verytight_pions_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Lambda0 pi+]cc",
        name="Charm_Hyperons_LcpToL0Pip_LL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        CombinationCut=F.require_all(
            F.math.in_range(2080 * MeV, F.MASS, 2405 * MeV),
            F.PT > 1.9 * GeV,
            F.SUM(F.PT) > 2.6 * GeV,
            F.MAXDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2100 * MeV, F.MASS, 2385 * MeV),
            F.PT > 2 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 4.0,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.998,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lcs])


@register_line_builder(all_lines)
def lc_to_lpi_dd_line(name="Hlt2Charm_LcpToL0Pip_DD"):
    dd_lambdas = _make_dd_lambdas()
    lcs = ParticleCombiner(
        [dd_lambdas, _make_verytight_pions_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Lambda0 pi+]cc",
        name="Charm_Hyperons_LcpToL0Pip_DD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        CombinationCut=F.require_all(
            F.math.in_range(2080 * MeV, F.MASS, 2405 * MeV),
            F.PT > 2.1 * GeV,
            F.SUM(F.PT) > 2.8 * GeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2100 * MeV, F.MASS, 2385 * MeV),
            F.PT > 2.2 * GeV,
            F.CHI2DOF < 9.0,
            F.OWNPVVDZ > -0.5 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 3.8,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, lcs])


@register_line_builder(all_lines)
def lc_to_lpi_llinclb_line(name="Hlt2Charm_LcpToL0Pip_LL_Inclb_SP"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    lcs = _make_detached_lc_to_lpi_ll(ll_lambdas, _make_verytight_pions_for_charm())
    b_to_lch = _make_bbaryon_to_cbaryonttrack(
        lcs,
        _make_long_tracks_for_beauty(),
        "[Lambda_b0 -> Lambda_c+ pi-]cc",
        ["Lambda_c+"],
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lcs, b_to_lch],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


@register_line_builder(all_lines)
def lc_to_lpi_ddinclb_line(name="Hlt2Charm_LcpToL0Pip_DD_Inclb_SP"):
    dd_lambdas = _make_dd_lambdas()
    lc_dd = _make_detached_lc_to_lpi_dd(dd_lambdas, _make_verytight_pions_for_charm())
    b_to_lch = _make_bbaryon_to_cbaryonttrack(
        lc_dd,
        _make_long_tracks_for_beauty(),
        "[Lambda_b0 -> Lambda_c+ pi-]cc",
        ["Lambda_c+"],
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, lc_dd, b_to_lch],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


@register_line_builder(all_lines)
def lb_to_lcdm_lc_to_lpi_ll_line(
    name="Hlt2Charm_Lb0ToLcpDm_LcpToL0Pip_DmToKpPimPim_LL",
):
    ll_lambdas = _make_ll_lambdas_for_charm()
    lcs = _make_detached_lc_to_lpi_ll(ll_lambdas, _make_verytight_pions_for_charm())
    dm = _make_d_to_kpipi()
    b_to_lch = _make_bbaryon_to_cbaryond(
        lcs, dm, "[Lambda_b0 -> Lambda_c+ D-]cc", ["Lambda_c+", "D-"]
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lcs, b_to_lch])


@register_line_builder(all_lines)
def lb_to_lcdm_lc_to_lpi_dd_line(
    name="Hlt2Charm_Lb0ToLcpDm_LcpToL0Pip_DmToKpPimPim_DD",
):
    dd_lambdas = _make_dd_lambdas()
    lc_dd = _make_detached_lc_to_lpi_dd(dd_lambdas, _make_verytight_pions_for_charm())
    dm = _make_d_to_kpipi()
    b_to_lch = _make_bbaryon_to_cbaryond(
        lc_dd, dm, "[Lambda_b0 -> Lambda_c+ D-]cc", ["Lambda_c+", "D-"]
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, lc_dd, b_to_lch])


@register_line_builder(all_lines)
def lb_to_lcdsm_lc_to_lpi_ll_line(
    name="Hlt2Charm_Lb0ToLcpDsm_LcpToL0Pip_DmDsmToKmKpPim_LL",
):
    ll_lambdas = _make_ll_lambdas_for_charm()
    lcs = _make_detached_lc_to_lpi_ll(ll_lambdas, _make_verytight_pions_for_charm())
    dsm = _make_ds_to_kkpi()
    b_to_lch = _make_bbaryon_to_cbaryond(
        lcs, dsm, "[Lambda_b0 -> Lambda_c+ D_s-]cc", ["Lambda_c+", "D_s-"]
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lcs, b_to_lch])


@register_line_builder(all_lines)
def lb_to_lcdsm_lc_to_lpi_dd_line(
    name="Hlt2Charm_Lb0ToLcpDsm_LcpToL0Pip_DmDsmToKmKpPim_DD",
):
    dd_lambdas = _make_dd_lambdas()
    lc_dd = _make_detached_lc_to_lpi_dd(dd_lambdas, _make_verytight_pions_for_charm())
    dsm = _make_ds_to_kkpi()
    b_to_lch = _make_bbaryon_to_cbaryond(
        lc_dd, dsm, "[Lambda_b0 -> Lambda_c+ D_s-]cc", ["Lambda_c+", "D_s-"]
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, lc_dd, b_to_lch])


@register_line_builder(all_lines)
def lc_to_lk_ll_line(name="Hlt2Charm_LcpXicpToL0Kp_LL"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    lcs = ParticleCombiner(
        [ll_lambdas, _make_verytight_kaons_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Lambda0 K+]cc",
        name="Charm_Hyperons_LcpXicpToL0Kp_LL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        CombinationCut=F.require_all(
            F.math.in_range(2080 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.9 * GeV,
            F.SUM(F.PT) > 2.6 * GeV,
            F.MAXDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2100 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 8 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDZ > 80 * um,
            F.OWNPVDLS > 4.0,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.998,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lcs])


@register_line_builder(all_lines)
def lc_to_lk_dd_line(name="Hlt2Charm_LcpXicpToL0Kp_DD"):
    dd_lambdas = _make_dd_lambdas()
    lcs = ParticleCombiner(
        [dd_lambdas, _make_verytight_kaons_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Lambda0 K+]cc",
        name="Charm_Hyperons_LcpToL0Kp_DD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        CombinationCut=F.require_all(
            F.math.in_range(2080 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.1 * GeV,
            F.SUM(F.PT) > 2.8 * GeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2100 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2.2 * GeV,
            F.CHI2DOF < 9.0,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 3.8,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, lcs])


@register_line_builder(all_lines)
def lc_to_lk_llinclb_line(name="Hlt2Charm_LcpToL0Kp_LL_Inclb_SP"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    lcs = ParticleCombiner(
        [ll_lambdas, _make_verytight_kaons_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Lambda0 K+]cc",
        name="Charm_Hyperons_DetachedLcpToL0Kp_LL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        CombinationCut=F.require_all(
            F.math.in_range(2080 * MeV, F.MASS, 2405 * MeV),
            F.PT > 1.9 * GeV,
            F.SUM(F.PT) > 2.6 * GeV,
            F.MAXDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2100 * MeV, F.MASS, 2385 * MeV),
            F.PT > 2 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 8 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 5.0,
        ),
    )
    b_to_lch = _make_bbaryon_to_cbaryonttrack(
        lcs,
        _make_long_tracks_for_beauty(),
        "[Lambda_b0 -> Lambda_c+ pi-]cc",
        ["Lambda_c+"],
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lcs, b_to_lch],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


@register_line_builder(all_lines)
def lc_to_lk_ddinclb_line(name="Hlt2Charm_LcpToL0Kp_DD_Inclb_SP"):
    dd_lambdas = _make_dd_lambdas()
    lcs = ParticleCombiner(
        [dd_lambdas, _make_verytight_kaons_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Lambda0 K+]cc",
        name="Charm_Hyperons_DetachedLcpToL0Kp_DD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        CombinationCut=F.require_all(
            F.math.in_range(2080 * MeV, F.MASS, 2405 * MeV),
            F.PT > 1.9 * GeV,
            F.SUM(F.PT) > 2.6 * GeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2100 * MeV, F.MASS, 2385 * MeV),
            F.PT > 2 * GeV,
            F.CHI2DOF < 9.0,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 4.5,
        ),
    )
    b_to_lch = _make_bbaryon_to_cbaryonttrack(
        lcs,
        _make_long_tracks_for_beauty(),
        "[Lambda_b0 -> Lambda_c+ pi-]cc",
        ["Lambda_c+"],
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [lcs, b_to_lch],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


# Xic+ -> L pi and partially reconstructed Xic -> Sigma0 pi. Expected rate 320 Hz + a bit of Lc -> L pi
@register_line_builder(all_lines)
def xicp_to_lpi_ll_line(name="Hlt2Charm_XicpToL0Pip_LL"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    xicps = ParticleCombiner(
        [ll_lambdas, _make_verytight_pions_for_charm()],
        DecayDescriptor="[Xi_c+ -> Lambda0 pi+]cc",
        name="Charm_Hyperons_XicpToL0Pip_LL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        CombinationCut=F.require_all(
            F.math.in_range(2260 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.3 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.MAXDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2280 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 4.8,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.999,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, xicps])


@register_line_builder(all_lines)
def xicp_to_lpi_dd_line(name="Hlt2Charm_XicpToL0Pip_DD"):
    dd_lambdas = _make_dd_lambdas()
    long_xicp_pions = _make_verytight_pions_for_charm()
    xicps = ParticleCombiner(
        [dd_lambdas, long_xicp_pions],
        DecayDescriptor="[Xi_c+ -> Lambda0 pi+]cc",
        name="Charm_Hyperons_XicpToL0Pip_DD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        CombinationCut=F.require_all(
            F.math.in_range(2260 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.3 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2380 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2.4 * GeV,
            F.CHI2DOF < 12.0,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 4.8,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, xicps])


@register_line_builder(all_lines)
def xicp_to_lpi_llbinc_line(name="Hlt2Charm_XicpToL0Pip_LL_Inclb_SP"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    xicps = _make_detached_xicp_to_lpi_ll(ll_lambdas, _make_verytight_pions_for_charm())
    b_to_xich = _make_bbaryon_to_cbaryonttrack(
        xicps, _make_long_tracks_for_beauty(), "[Xi_b0 -> Xi_c+ pi-]cc", ["Xi_c+"]
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, xicps, b_to_xich],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


@register_line_builder(all_lines)
def xicp_to_lpi_ddbinc_line(name="Hlt2Charm_XicpToL0Pip_DD_Inclb_SP"):
    dd_lambdas = _make_dd_lambdas()
    xicps = _make_detached_xicp_to_lpi_dd(dd_lambdas, _make_verytight_pions_for_charm())
    b_to_xich = _make_bbaryon_to_cbaryonttrack(
        xicps, _make_long_tracks_for_beauty(), "[Xi_b0 -> Xi_c+ pi-]cc", ["Xi_c+"]
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, xicps, b_to_xich],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


@register_line_builder(all_lines)
def xibz_to_xicpdm_xicp_to_lpi_ll_line(
    name="Hlt2Charm_Xib0ToXicpDm_XicpToL0Pip_DmToKpPimPim_LL",
):
    ll_lambdas = _make_ll_lambdas_for_charm()
    xicps = _make_detached_xicp_to_lpi_ll(ll_lambdas, _make_verytight_pions_for_charm())
    dm = _make_d_to_kpipi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xicps, dm, "[Xi_b0 -> Xi_c+ D-]cc", ["Xi_c+", "D-"]
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, xicps, b_to_xich])


@register_line_builder(all_lines)
def xibz_to_xicpdm_xicp_to_lpi_dd_line(
    name="Hlt2Charm_Xib0ToXicpDm_XicpToL0Pip_DmToKpPimPim_DD",
):
    dd_lambdas = _make_dd_lambdas()
    xicps = _make_detached_xicp_to_lpi_dd(dd_lambdas, _make_verytight_pions_for_charm())
    dm = _make_d_to_kpipi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xicps, dm, "[Xi_b0 -> Xi_c+ D-]cc", ["Xi_c+", "D-"]
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, xicps, b_to_xich])


@register_line_builder(all_lines)
def xibz_to_xicpdsm_xicp_to_lpi_ll_line(
    name="Hlt2Charm_Xib0ToXicpDsm_XicpToL0Pip_DmDsmToKmKpPim_LL",
):
    ll_lambdas = _make_ll_lambdas_for_charm()
    xicps = _make_detached_xicp_to_lpi_ll(ll_lambdas, _make_verytight_pions_for_charm())
    dsm = _make_ds_to_kkpi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xicps, dsm, "[Xi_b0 -> Xi_c+ D_s-]cc", ["Xi_c+", "D_s-"]
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, xicps, b_to_xich])


@register_line_builder(all_lines)
def xibz_to_xicpdsm_xicp_to_lpi_dd_line(
    name="Hlt2Charm_Xib0ToXicpDsm_XicpToL0Pip_DmDsmToKmKpPim_DD",
):
    dd_lambdas = _make_dd_lambdas()
    xicps = _make_detached_xicp_to_lpi_dd(dd_lambdas, _make_verytight_pions_for_charm())
    dsm = _make_ds_to_kkpi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xicps, dsm, "[Xi_b0 -> Xi_c+ D_s-]cc", ["Xi_c+", "D_s-"]
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, xicps, b_to_xich])


# Xi_c0 -> Sigma- pi+. Expected rate negligible.
@register_line_builder(all_lines)
def xic0_to_smpip_longsm_line(name="Hlt2Charm_Xic0ToSmPip_LongSm_Inclb_SP"):
    sigmas = ParticleFilter(
        make_long_sigmams(),
        F.FILTER(
            F.require_all(
                F.PT > 1.6 * GeV,
                F.P > 22 * GeV,
                F.OWNPVIPCHI2 > 9.0,
                F.PROBNN_P > 0.75,
                F.PROBNN_P * (1.0 - F.PROBNN_K) > 0.5,
            )
        ),
    )
    xic0s = ParticleCombiner(
        [sigmas, _make_verytight_pions_for_charm()],
        DecayDescriptor="[Xi_c0 -> Sigma- pi+]cc",
        name="Charm_Hyperons_Xic0ToSmpPip_{hash}",
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 3.3 * GeV,
            F.SUM(F.PT) > 4 * GeV,
            F.MAXDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 3.5 * GeV,
            F.CHI2DOF < 9.0,
            F.OWNPVVDZ > 2 * mm,
            F.OWNPVVDRHO > 0.2 * mm,
            F.OWNPVDLS > 6.0,
        ),
    )
    xibs = _make_bbaryon_to_cbaryonttrack(
        xic0s, _make_long_tracks_for_beauty(), "[Xi_b- -> Xi_c0 pi+]cc", ["Xi_c0"]
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [sigmas, xic0s, xibs],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


# Xic0 -> Xi- pi+.  Expected rate 380 Hz
@register_line_builder(all_lines)
def xicz_to_ximpi_lll_line(name="Hlt2Charm_Xic0ToXimPip_LLL"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    xic0s = ParticleCombiner(
        [lll_xis, _make_verytight_pions_for_charm()],
        DecayDescriptor="[Xi_c0 -> Xi- pi+]cc",
        name="Charm_Hyperons_Xic0ToXimPip_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXDOCACUT(150 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVVDRHO > 50 * um,
            F.OWNPVDLS > 3.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, xic0s])


@register_line_builder(all_lines)
def xicz_to_ximpi_ddl_line(name="Hlt2Charm_Xic0ToXimPip_DDL"):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    long_xicz_pions = _make_verytight_pions_for_charm()
    xic0s = ParticleCombiner(
        [ddl_xis, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Xi- pi+]cc",
        name="Charm_Hyperons_Xic0ToXimPip_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 0.0 * mm,
            F.OWNPVVDRHO > 50 * um,
            F.OWNPVDLS > 3.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, xic0s])


@register_line_builder(all_lines)
def xicz_to_ximpi_lllinclb_line(name="Hlt2Charm_Xic0ToXimPip_LLL_Inclb_SP"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    xic0sl = _make_detached_xicz_to_ximpi_lll(
        lll_xis, _make_verytight_pions_for_charm()
    )
    b_to_xich = _make_bbaryon_to_cbaryonttrack(
        xic0sl, _make_long_tracks_for_beauty(), "[Xi_b- -> Xi_c0 pi-]cc", ["Xi_c0"]
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, xic0sl, b_to_xich],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


@register_line_builder(all_lines)
def xicz_to_ximpi_ddlinclb_line(name="Hlt2Charm_Xic0ToXimPip_DDL_Inclb_SP"):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    xic0sl = _make_detached_xicz_to_ximpi_ddl(
        ddl_xis, _make_verytight_pions_for_charm()
    )
    b_to_xich = _make_bbaryon_to_cbaryonttrack(
        xic0sl, _make_long_tracks_for_beauty(), "[Xi_b- -> Xi_c0 pi-]cc", ["Xi_c0"]
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, xic0sl, b_to_xich],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


@register_line_builder(all_lines)
def xibm_to_xiczdm_xicz_to_ximpi_lll_line(
    name="Hlt2Charm_XibmToXic0Dm_Xic0ToXimPip_DmToKpPimPim_LLL",
):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    xic0sl = _make_detached_xicz_to_ximpi_lll(
        lll_xis, _make_verytight_pions_for_charm()
    )
    dm = _make_d_to_kpipi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0sl, dm, "[Xi_b- -> Xi_c0 D-]cc", ["Xi_c0", "D-"]
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, xic0sl, b_to_xich]
    )


@register_line_builder(all_lines)
def xibm_to_xiczdm_xicz_to_ximpi_ddl_line(
    name="Hlt2Charm_XibmToXic0Dm_Xic0ToXimPip_DmToKpPimPim_DDL",
):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    xic0sl = _make_detached_xicz_to_ximpi_ddl(
        ddl_xis, _make_verytight_pions_for_charm()
    )
    dm = _make_d_to_kpipi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0sl, dm, "[Xi_b- -> Xi_c0 D-]cc", ["Xi_c0", "D-"]
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, xic0sl, b_to_xich]
    )


@register_line_builder(all_lines)
def xibm_to_xiczdsm_xicz_to_ximpi_lll_line(
    name="Hlt2Charm_XibmToXic0Dsm_Xic0ToXimPip_DmDsmToKmKpPim_LLL",
):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    xic0sl = _make_detached_xicz_to_ximpi_lll(
        lll_xis, _make_verytight_pions_for_charm()
    )
    dsm = _make_ds_to_kkpi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0sl, dsm, "[Xi_b- -> Xi_c0 D_s-]cc", ["Xi_c0", "D_s-"]
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, xic0sl, b_to_xich]
    )


@register_line_builder(all_lines)
def xibm_to_xiczdsm_xicz_to_ximpi_ddl_line(
    name="Hlt2Charm_XibmToXic0Dsm_Xic0ToXimPip_DmDsmToKmKpPim_DDL",
):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    xic0sl = _make_detached_xicz_to_ximpi_ddl(
        ddl_xis, _make_verytight_pions_for_charm()
    )
    dsm = _make_ds_to_kkpi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0sl, dsm, "[Xi_b- -> Xi_c0 D_s-]cc", ["Xi_c0", "D_s-"]
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, xic0sl, b_to_xich]
    )


# Xic0 -> Xi- K+.  Expected rate 10 Hz
@register_line_builder(all_lines)
def xicz_to_ximk_lll_line(name="Hlt2Charm_Xic0ToXimKp_LLL"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    xic0s = ParticleCombiner(
        [lll_xis, _make_tight_kaons_for_charm()],
        DecayDescriptor="[Xi_c0 -> Xi- K+]cc",
        name="Charm_Hyperons_Xic0ToXimKp_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXDOCACUT(150 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVVDRHO > 50 * um,
            F.OWNPVDLS > 3.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, xic0s])


@register_line_builder(all_lines)
def xicz_to_ximk_ddl_line(name="Hlt2Charm_Xic0ToXimKp_DDL"):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    xic0s = ParticleCombiner(
        [ddl_xis, _make_tight_kaons_for_charm()],
        DecayDescriptor="[Xi_c0 -> Xi- K+]cc",
        name="Charm_Hyperons_Xic0ToXimKp_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVVDRHO > 50 * um,
            F.OWNPVDLS > 3.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, xic0s])


@register_line_builder(all_lines)
def xicz_to_ximk_lllinclb_line(name="Hlt2Charm_Xic0ToXimKp_LLL_Inclb_SP"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    xic0sl = _make_detached_xicz_to_ximk_lll(lll_xis, _make_tight_kaons_for_charm())
    b_to_xich = _make_bbaryon_to_cbaryonttrack(
        xic0sl, _make_long_tracks_for_beauty(), "[Xi_b- -> Xi_c0 pi-]cc", ["Xi_c0"]
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, xic0sl, b_to_xich],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


@register_line_builder(all_lines)
def xicz_to_ximk_ddlinclb_line(name="Hlt2Charm_Xic0ToXimKp_DDL_Inclb_SP"):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    xic0sl = _make_detached_xicz_to_ximk_ddl(ddl_xis, _make_tight_kaons_for_charm())
    b_to_xich = _make_bbaryon_to_cbaryonttrack(
        xic0sl, _make_long_tracks_for_beauty(), "[Xi_b- -> Xi_c0 pi-]cc", ["Xi_c0"]
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, xic0sl, b_to_xich],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


@register_line_builder(all_lines)
def xibm_to_xiczdm_xicz_to_ximkm_lll_line(
    name="Hlt2Charm_XibmToXic0Dm_Xic0ToXimKp_DmToKpPimPim_LLL",
):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    xic0sl = _make_detached_xicz_to_ximk_lll(lll_xis, _make_tight_kaons_for_charm())
    dm = _make_d_to_kpipi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0sl, dm, "[Xi_b- -> Xi_c0 D-]cc", ["Xi_c0", "D-"]
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, xic0sl, b_to_xich]
    )


@register_line_builder(all_lines)
def xibm_to_xiczdm_xicz_to_ximkm_ddl_line(
    name="Hlt2Charm_XibmToXic0Dm_Xic0ToXimKp_DmToKpPimPim_DDL",
):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    xic0sl = _make_detached_xicz_to_ximk_ddl(ddl_xis, _make_tight_kaons_for_charm())
    dm = _make_d_to_kpipi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0sl, dm, "[Xi_b- -> Xi_c0 D-]cc", ["Xi_c0", "D-"]
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, xic0sl, b_to_xich]
    )


@register_line_builder(all_lines)
def xibm_to_xiczdsm_xicz_to_ximkm_lll_line(
    name="Hlt2Charm_XibmToXic0Dsm_Xic0ToXimKp_DmDsmToKmKpPim_LLL",
):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    xic0sl = _make_detached_xicz_to_ximk_lll(lll_xis, _make_tight_kaons_for_charm())
    dsm = _make_ds_to_kkpi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0sl, dsm, "[Xi_b- -> Xi_c0 D_s-]cc", ["Xi_c0", "D_s-"]
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, xic0sl, b_to_xich]
    )


@register_line_builder(all_lines)
def xibm_to_xiczdsm_xicz_to_ximkm_ddl_line(
    name="Hlt2Charm_XibmToXic0Dsm_Xic0ToXimKp_DmDsmToKmKpPim_DDL",
):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    xic0sl = _make_detached_xicz_to_ximk_ddl(ddl_xis, _make_tight_kaons_for_charm())
    dsm = _make_ds_to_kkpi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0sl, dsm, "[Xi_b- -> Xi_c0 D_s-]cc", ["Xi_c0", "D_s-"]
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, xic0sl, b_to_xich]
    )


# Xic0 -> Omega- K+. Expected rate 150 Hz
@register_line_builder(all_lines)
def xicz_to_omk_lll_line(name="Hlt2Charm_Xic0ToOmKp_LLL"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_oms = _make_lll_omegas(ll_lambdas, _make_long_kaons_for_omega())
    xic0s = ParticleCombiner(
        [lll_oms, _make_tight_kaons_for_charm()],
        DecayDescriptor="[Xi_c0 -> Omega- K+]cc",
        name="Charm_Hyperons_Xic0ToOmKp_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Omega-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXDOCACUT(150 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.99,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lll_oms, xic0s])


@register_line_builder(all_lines)
def xicz_to_omk_ddl_line(name="Hlt2Charm_Xic0ToOmKp_DDL"):
    dd_lambdas = _make_dd_lambdas()
    ddl_oms = _make_ddl_omegas(dd_lambdas, _make_long_kaons_for_omega())
    xic0s = ParticleCombiner(
        [ddl_oms, _make_tight_kaons_for_charm()],
        DecayDescriptor="[Xi_c0 -> Omega- K+]cc",
        name="Charm_Hyperons_Xic0ToOmKp_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Omega-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.0,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.99,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, ddl_oms, xic0s])


# Xic0 -> L0 KS0 ; LLLL + DDLL + LLDD + DDDD combinations
@register_line_builder(all_lines)
def xicz_to_l0ll_kshortsll_line(name="Hlt2Charm_Xic0ToL0Ks_LLLL", prescale=1):
    ll_lambdas = _make_ll_lambdas_for_charm()
    xic0s = ParticleCombiner(
        [ll_lambdas, _make_ll_kshorts()],
        DecayDescriptor="[Xi_c0 -> Lambda0 KS0]cc",
        name="Charm_Hyperons_Xic0ToL0Ks_LLLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        CombinationCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2390 * MeV, F.MASS, 2550 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 2 * mm,
            _DZ_CHILD(2) > 1 * mm,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVVDRHO > 20 * um,
            F.OWNPVDLS > 2.5,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.99,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, xic0s], prescale=prescale
    )


@register_line_builder(all_lines)
def xicz_to_l0dd_kshortsll_line(name="Hlt2Charm_Xic0ToL0Ks_DDLL", prescale=1):
    dd_lambdas = _make_dd_lambdas()
    xic0s = ParticleCombiner(
        [dd_lambdas, _make_ll_kshorts()],
        DecayDescriptor="[Xi_c0 -> Lambda0 KS0]cc",
        name="Charm_Hyperons_Xic0ToL0Ks_DDLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        CombinationCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2390 * MeV, F.MASS, 2550 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(2) > 1 * mm,
            F.OWNPVVDZ > -5 * mm,
            F.OWNPVVDRHO > 20 * um,
            F.OWNPVDLS > 2.5,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.99,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, xic0s], prescale=prescale
    )


@register_line_builder(all_lines)
def xicz_to_l0ll_kshortsdd_line(name="Hlt2Charm_Xic0ToL0Ks_LLDD", prescale=1):
    dd_kshorts = _make_dd_kshorts()
    xic0s = ParticleCombiner(
        [_make_ll_lambdas_for_charm(), dd_kshorts],
        DecayDescriptor="[Xi_c0 -> Lambda0 KS0]cc",
        name="Charm_Hyperons_Xic0ToL0Ks_LLDD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        CombinationCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MAXDOCACUT(2 * mm),
            F.math.in_range(2390 * MeV, F.MASS, 2550 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > -5 * mm,
            F.OWNPVVDRHO > 20 * um,
            F.OWNPVDLS > 2.5,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.99,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_kshorts, xic0s], prescale=prescale
    )


@register_line_builder(all_lines)
def xicz_to_l0dd_kshortsdd_line(name="Hlt2Charm_Xic0ToL0Ks_DDDD", prescale=1):
    dd_lambdas = _make_dd_lambdas()
    xic0s = ParticleCombiner(
        [dd_lambdas, _make_dd_kshorts()],
        DecayDescriptor="[Xi_c0 -> Lambda0 KS0]cc",
        name="Charm_Hyperons_Xic0ToL0Ks_DDDD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        CombinationCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.MAXDOCACUT(5 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2390 * MeV, F.MASS, 2550 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            F.OWNPVVDZ > -10 * mm,
            F.OWNPVVDRHO > 20 * um,
            F.OWNPVDLS > 2.5,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.99,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, xic0s], prescale=prescale
    )


# Omegac0 -> Xi- pi+. Expected rate 75 Hz
@register_line_builder(all_lines)
def oc_to_ximpi_lll_line(name="Hlt2Charm_Oc0ToXimPip_LLL"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    oc0s = ParticleCombiner(
        [lll_xis, _make_verytight_pions_for_charm()],
        DecayDescriptor="[Omega_c0 -> Xi- pi+]cc",
        name="Charm_Hyperons_Oc0ToXimPip_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXDOCACUT(150 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVVDRHO > 70 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, oc0s])


@register_line_builder(all_lines)
def oc_to_ximpi_ddl_line(name="Hlt2Charm_Oc0ToXimPip_DDL"):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    oc0s = ParticleCombiner(
        [ddl_xis, _make_verytight_pions_for_charm()],
        DecayDescriptor="[Omega_c0 -> Xi- pi+]cc",
        name="Charm_Hyperons_Oc0ToXimPip_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVVDRHO > 70 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, oc0s])


# Omegac0 -> Xi- K+.  Expected rate 7.5 Hz
@register_line_builder(all_lines)
def oc_to_ximk_lll_line(name="Hlt2Charm_Oc0ToXimKp_LLL"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    oc0s = ParticleCombiner(
        [lll_xis, _make_tight_kaons_for_charm()],
        DecayDescriptor="[Omega_c0 -> Xi- K+]cc",
        name="Charm_Hyperons_Oc0ToXimKp_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXDOCACUT(150 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVVDRHO > 70 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, oc0s])


@register_line_builder(all_lines)
def oc_to_ximk_ddl_line(name="Hlt2Charm_Oc0ToXimKp_DDL"):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    oc0s = ParticleCombiner(
        [ddl_xis, _make_tight_kaons_for_charm()],
        DecayDescriptor="[Omega_c0 -> Xi- K+]cc",
        name="Charm_Hyperons_Oc0ToXimKp_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.MAXDOCACUT(200 * um),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVVDRHO > 70 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, oc0s])


@register_line_builder(all_lines)
def oc_to_ximk_lllinclb_line(name="Hlt2Charm_Oc0ToXimKp_LLL_Inclb_SP"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    oc_lll = _make_detached_oc_to_ximk_lll(lll_xis, _make_tight_kaons_for_charm())
    b_to_och = _make_bbaryon_to_cbaryonttrack(
        oc_lll,
        _make_long_tracks_for_beauty(),
        "[Omega_b- -> Omega_c0 pi-]cc",
        ["Omega_c0"],
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, oc_lll, b_to_och],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


@register_line_builder(all_lines)
def oc_to_ximk_ddlinclb_line(name="Hlt2Charm_Oc0ToXimKp_DDL_Inclb_SP"):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    oc0s = _make_detached_oc_to_ximk_ddl(ddl_xis, _make_tight_kaons_for_charm())
    b_to_och = _make_bbaryon_to_cbaryonttrack(
        oc0s,
        _make_long_tracks_for_beauty(),
        "[Omega_b- -> Omega_c0 pi-]cc",
        ["Omega_c0"],
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, oc0s, b_to_och],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


@register_line_builder(all_lines)
def obm_to_ocdm_oc_to_ximkm_lll_line(
    name="Hlt2Charm_ObmToOc0Dm_Oc0ToXimKp_DmToKpPimPim_LLL",
):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    oc_lll = _make_detached_oc_to_ximk_lll(lll_xis, _make_tight_kaons_for_charm())
    b_to_och = _make_bbaryon_to_cbaryond(
        oc_lll, _make_d_to_kpipi(), "[Omega_b- -> Omega_c0 D-]cc", ["Omega_c0", "D-"]
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, oc_lll, b_to_och]
    )


@register_line_builder(all_lines)
def obm_to_ocdm_oc_to_ximkm_ddl_line(
    name="Hlt2Charm_ObmToOc0Dm_Oc0ToXimKp_DmToKpPimPim_DDL",
):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    oc0s = _make_detached_oc_to_ximk_ddl(ddl_xis, _make_tight_kaons_for_charm())
    b_to_och = _make_bbaryon_to_cbaryond(
        oc0s, _make_d_to_kpipi(), "[Omega_b- -> Omega_c0 D-]cc", ["Omega_c0", "D-"]
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, oc0s, b_to_och]
    )


@register_line_builder(all_lines)
def obm_to_ocdsm_oc_to_ximkm_lll_line(
    name="Hlt2Charm_ObmToOc0Dsm_Oc0ToXimKp_DmDsmToKmKpPim_LLL",
):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    oc_lll = _make_detached_oc_to_ximk_lll(lll_xis, _make_tight_kaons_for_charm())
    b_to_och = _make_bbaryon_to_cbaryond(
        oc_lll,
        _make_ds_to_kkpi(),
        "[Omega_b- -> Omega_c0 D_s-]cc",
        ["Omega_c0", "D_s-"],
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, oc_lll, b_to_och]
    )


@register_line_builder(all_lines)
def obm_to_ocdsm_oc_to_ximkm_ddl_line(
    name="Hlt2Charm_ObmToOc0Dsm_Oc0ToXimKp_DmDsmToKmKpPim_DDL",
):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    oc0s = _make_detached_oc_to_ximk_ddl(ddl_xis, _make_tight_kaons_for_charm())
    b_to_och = _make_bbaryon_to_cbaryond(
        oc0s, _make_ds_to_kkpi(), "[Omega_b- -> Omega_c0 D_s-]cc", ["Omega_c0", "D_s-"]
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, oc0s, b_to_och]
    )


# Omegac0 -> Omega- pi+. Expected rate 140 Hz
@register_line_builder(all_lines)
def oc_to_ompi_lll_line(name="Hlt2Charm_Oc0ToOmPip_LLL"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_oms = _make_lll_omegas(ll_lambdas, _make_long_kaons_for_omega())
    long_oc_pions = _make_verytight_pions_for_charm()
    oc0s = ParticleCombiner(
        [lll_oms, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Omega- pi+]cc",
        name="Charm_Hyperons_Oc0ToOmPip_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Omega-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXDOCACUT(120 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.2,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lll_oms, oc0s])


@register_line_builder(all_lines)
def oc_to_ompi_ddl_line(name="Hlt2Charm_Oc0ToOmPip_DDL"):
    dd_lambdas = _make_dd_lambdas()
    ddl_oms = _make_ddl_omegas(dd_lambdas, _make_long_kaons_for_omega())
    oc0s = ParticleCombiner(
        [ddl_oms, _make_verytight_pions_for_charm()],
        DecayDescriptor="[Omega_c0 -> Omega- pi+]cc",
        name="Charm_Hyperons_Oc0ToOmPip_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Omega-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.2,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, ddl_oms, oc0s])


@register_line_builder(all_lines)
def oc_to_ompi_lllinclb_line(name="Hlt2Charm_Oc0ToOmPip_LLL_Inclb_SP"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_oms = _make_lll_omegas(ll_lambdas, _make_long_kaons_for_omega())
    oc_lll = _make_detached_oc_to_ompi_lll(lll_oms, _make_verytight_pions_for_charm())
    b_to_och = _make_bbaryon_to_cbaryonttrack(
        oc_lll,
        _make_long_tracks_for_beauty(),
        "[Omega_b- -> Omega_c0 pi-]cc",
        ["Omega_c0"],
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_oms, oc_lll, b_to_och],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


@register_line_builder(all_lines)
def oc_to_ompi_ddlinclb_line(name="Hlt2Charm_Oc0ToOmPip_DDL_Inclb_SP"):
    dd_lambdas = _make_dd_lambdas()
    ddl_oms = _make_ddl_omegas(dd_lambdas, _make_long_kaons_for_omega())
    oc0s = _make_detached_oc_to_ompi_ddl(ddl_oms, _make_verytight_pions_for_charm())
    b_to_och = _make_bbaryon_to_cbaryonttrack(
        oc0s,
        _make_long_tracks_for_beauty(),
        "[Omega_b- -> Omega_c0 pi-]cc",
        ["Omega_c0"],
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_oms, oc0s, b_to_och],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


@register_line_builder(all_lines)
def obm_to_ocdm_oc_to_opi_lll_line(
    name="Hlt2Charm_ObmToOc0Dm_Oc0ToOmPip_DmToKpPimPim_LLL",
):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_oms = _make_lll_omegas(ll_lambdas, _make_long_kaons_for_omega())
    oc_lll = _make_detached_oc_to_ompi_lll(lll_oms, _make_verytight_pions_for_charm())
    dm = _make_d_to_kpipi()
    b_to_och = _make_bbaryon_to_cbaryond(
        oc_lll, dm, "[Omega_b- -> Omega_c0 D-]cc", ["Omega_c0", "D-"]
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_oms, oc_lll, b_to_och]
    )


@register_line_builder(all_lines)
def obm_to_ocdm_oc_to_opi_ddl_line(
    name="Hlt2Charm_ObmToOc0Dm_Oc0ToOmPip_DmToKpPimPim_DDL",
):
    dd_lambdas = _make_dd_lambdas()
    ddl_oms = _make_ddl_omegas(dd_lambdas, _make_long_kaons_for_omega())
    oc0s = _make_detached_oc_to_ompi_ddl(ddl_oms, _make_verytight_pions_for_charm())
    dm = _make_d_to_kpipi()
    b_to_och = _make_bbaryon_to_cbaryond(
        oc0s, dm, "[Omega_b- -> Omega_c0 D-]cc", ["Omega_c0", "D-"]
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_oms, oc0s, b_to_och]
    )


@register_line_builder(all_lines)
def obm_to_ocdsm_oc_to_opi_lll_line(
    name="Hlt2Charm_ObmToOc0Dsm_Oc0ToOmPip_DmDsmToKmKpPim_LLL",
):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_oms = _make_lll_omegas(ll_lambdas, _make_long_kaons_for_omega())
    oc_lll = _make_detached_oc_to_ompi_lll(lll_oms, _make_verytight_pions_for_charm())
    dsm = _make_ds_to_kkpi()
    b_to_och = _make_bbaryon_to_cbaryond(
        oc_lll, dsm, "[Omega_b- -> Omega_c0 D_s-]cc", ["Omega_c0", "D_s-"]
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_oms, oc_lll, b_to_och]
    )


@register_line_builder(all_lines)
def obm_to_ocdsm_oc_to_opi_ddl_line(
    name="Hlt2Charm_ObmToOc0Dsm_Oc0ToOmPip_DmDsmToKmKpPim_DDL",
):
    dd_lambdas = _make_dd_lambdas()
    ddl_oms = _make_ddl_omegas(dd_lambdas, _make_long_kaons_for_omega())
    oc0s = _make_detached_oc_to_ompi_ddl(ddl_oms, _make_verytight_pions_for_charm())
    dsm = _make_ds_to_kkpi()
    b_to_och = _make_bbaryon_to_cbaryond(
        oc0s, dsm, "[Omega_b- -> Omega_c0 D_s-]cc", ["Omega_c0", "D_s-"]
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_oms, oc0s, b_to_och]
    )


# Omegac0 -> Omega- K+. Expected rate 7 Hz
@register_line_builder(all_lines)
def oc_to_omk_lll_line(name="Hlt2Charm_Oc0ToOmKp_LLL"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_oms = _make_lll_omegas(ll_lambdas, _make_long_kaons_for_omega())
    oc0s = ParticleCombiner(
        [lll_oms, _make_tight_kaons_for_charm()],
        DecayDescriptor="[Omega_c0 -> Omega- K+]cc",
        name="Charm_Hyperons_Oc0ToOmKp_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Omega-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXDOCACUT(150 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lll_oms, oc0s])


@register_line_builder(all_lines)
def oc_to_omk_ddl_line(name="Hlt2Charm_Oc0ToOmKp_DDL"):
    dd_lambdas = _make_dd_lambdas()
    ddl_oms = _make_ddl_omegas(dd_lambdas, _make_long_kaons_for_omega())
    oc0s = ParticleCombiner(
        [ddl_oms, _make_tight_kaons_for_charm()],
        DecayDescriptor="[Omega_c0 -> Omega- K+]cc",
        name="Charm_Hyperons_Oc0ToOmKp_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Omega-"]),
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.MAXDOCACUT(200 * um),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, ddl_oms, oc0s])


# Omegac0 -> L0 KS0 ; LLLL + DDLL + LLDD + DDDD combinations
@register_line_builder(all_lines)
def oc_to_l0ll_kshortsll_line(name="Hlt2Charm_Oc0ToL0Ks_LLLL", prescale=1):
    ll_lambdas = _make_ll_lambdas_for_charm()
    oc0s = ParticleCombiner(
        [ll_lambdas, _make_ll_kshorts()],
        DecayDescriptor="[Omega_c0 -> Lambda0 KS0]cc",
        name="Charm_Hyperons_Oc0ToL0Ks_LLLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 2 * mm,
            _DZ_CHILD(2) > 1 * mm,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVVDRHO > 30 * um,
            F.OWNPVDLS > 2.7,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.99,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, oc0s], prescale=prescale
    )


@register_line_builder(all_lines)
def oc_to_l0dd_kshortsll_line(name="Hlt2Charm_Oc0ToL0Ks_DDLL", prescale=1):
    dd_lambdas = _make_dd_lambdas()
    oc0s = ParticleCombiner(
        [dd_lambdas, _make_ll_kshorts()],
        DecayDescriptor="[Omega_c0 -> Lambda0 KS0]cc",
        name="Charm_Hyperons_Oc0ToL0Ks_DDLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(2) > 1 * mm,
            F.OWNPVVDZ > -5 * mm,
            F.OWNPVVDRHO > 20 * um,
            F.OWNPVDLS > 2.5,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.99,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, oc0s], prescale=prescale
    )


@register_line_builder(all_lines)
def oc_to_l0ll_kshortsdd_line(name="Hlt2Charm_Oc0ToL0Ks_LLDD", prescale=1):
    dd_kshorts = _make_dd_kshorts()
    oc0s = ParticleCombiner(
        [_make_ll_lambdas_for_charm(), dd_kshorts],
        DecayDescriptor="[Omega_c0 -> Lambda0 KS0]cc",
        name="Charm_Hyperons_Oc0ToL0Ks_LLDD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > -5 * mm,
            F.OWNPVVDRHO > 20 * um,
            F.OWNPVDLS > 2.5,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.99,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_kshorts, oc0s], prescale=prescale
    )


@register_line_builder(all_lines)
def oc_to_l0dd_kshortsdd_line(name="Hlt2Charm_Oc0ToL0Ks_DDDD", prescale=1):
    dd_lambdas = _make_dd_lambdas()
    oc0s = ParticleCombiner(
        [dd_lambdas, _make_dd_kshorts()],
        DecayDescriptor="[Omega_c0 -> Lambda0 KS0]cc",
        name="Charm_Hyperons_Oc0ToL0Ks_DDDD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.MAXDOCACUT(5 * mm),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 12.0,
            F.OWNPVVDZ > -10 * mm,
            F.OWNPVVDRHO > 20 * um,
            F.OWNPVDLS > 2.5,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.99,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, oc0s], prescale=prescale
    )


#########################
## (quasi-) three-body ##
#########################
# L_c+ -> Sigma+ pi+ pi-. Expected rate negligible.
@register_line_builder(all_lines)
def Lcp_to_SpPipPim_longsp_line(name="Hlt2Charm_LcpToSpPimPip_LongSp"):
    pions = _make_verytight_pions_for_charm()
    sigmaps = ParticleFilter(
        make_long_sigmaps(),
        F.FILTER(
            F.require_all(
                F.PT > 600 * MeV,
                F.P > 200 * GeV,
                F.OWNPVIPCHI2 > 9.0,
                (F.PID_P - F.PID_K) > 5.0,
            ),
        ),
    )
    Lcps = ParticleCombiner(
        [
            sigmaps,
            pions,
            pions,
        ],
        DecayDescriptor="[Lambda_c+ -> Sigma+ pi+ pi-]cc",
        name="Charm_Hyperons_LcpToSpPimPip_{hash}",
        Combination12Cut=F.require_all(
            F.MASS < 2260 * MeV,
            F.MAXDOCACUT(100 * um),
        ),
        CombinationCut=F.require_all(
            F.MASS > 2170 * MeV,
            F.MASS < 2400 * MeV,
            F.PT > 1.3 * GeV,
            F.P > 200 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            F.MAXDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.MASS > 2200 * MeV,
            F.MASS < 2400 * MeV,
            F.CHI2DOF < 7.0,
            F.OWNPVVDZ > 0.0 * mm,
            F.OWNPVFDCHI2 > 8.0,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [sigmaps, Lcps])


# L_c+ -> Sigma- pi+ pi+. Expected rate negligible.
@register_line_builder(all_lines)
def Lcp_to_SmPipPip_longsm_line(name="Hlt2Charm_LcpToSmPipPip_LongSm"):
    pions = _make_verytight_pions_for_charm()
    sigmams = ParticleFilter(
        make_long_sigmams(),
        F.FILTER(
            F.require_all(
                F.PT > 100 * MeV,
                F.P > 100 * GeV,
                F.OWNPVIPCHI2 > 9.0,
                (F.PID_P - F.PID_K) > 5.0,
            ),
        ),
    )
    Lcps = ParticleCombiner(
        [
            sigmams,
            pions,
            pions,
        ],
        DecayDescriptor="[Lambda_c+ -> Sigma- pi+ pi+]cc",
        name="Charm_Hyperons_LcpToSmPipPip_{hash}",
        Combination12Cut=F.require_all(
            F.MASS < 2270 * MeV,
            F.MAXDOCACUT(100 * um),
        ),
        CombinationCut=F.require_all(
            F.MASS > 2170 * MeV,
            F.MASS < 2400 * MeV,
            F.PT > 1.3 * GeV,
            F.P > 100 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            F.MAXDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.MASS > 2200 * MeV,
            F.MASS < 2400 * MeV,
            F.CHI2DOF < 7.0,
            F.OWNPVVDZ > 0.0 * mm,
            F.OWNPVFDCHI2 > 8.0,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [sigmams, Lcps])


# Omega -> Xi pi pi. Expected rate negligible
@register_line_builder(all_lines)
def o_to_xipipi_line(name="Hlt2Charm_OmToXimPimPip_LongXim_SP"):
    xis = ParticleFilter(
        make_long_xis(),
        F.FILTER(
            F.require_all(
                F.PT > 600 * MeV,
                F.P > 25 * GeV,
                F.OWNPVIPCHI2 > 24.0,
                F.PROBNN_P > 0.8,
                F.PROBNN_P * (1.0 - F.PROBNN_K) > 0.6,
            )
        ),
    )
    pions = ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(F.PT > 80 * MeV, F.OWNPVIPCHI2 > 24.0, F.PROBNN_PI > 0.1)
        ),
    )
    omegas = ParticleCombiner(
        [xis, pions, pions],
        DecayDescriptor="[Omega- -> Xi- pi- pi+]cc",
        name="Charm_Hyperons_OmToXimPimPip_{hash}",
        Combination12Cut=F.MASS < 1631 * MeV,
        CombinationCut=F.require_all(
            F.math.in_range(1630 * MeV, F.MASS, 1710 * MeV),
            F.PT > 0.9 * GeV,
            F.require_any(
                F.CHILD(2, F.OWNPVIPCHI2) > 64.0, F.CHILD(3, F.OWNPVIPCHI2) > 64.0
            ),
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1647 * MeV, F.MASS, 1697 * MeV),
            F.PT > 1 * GeV,
            F.CHI2DOF < 12.0,
            F.OWNPVVDZ > 100 * mm,
            F.OWNPVVDRHO > 1 * mm,
            F.OWNPVIPCHI2 < 6.0,
            F.OWNPVDIRA > 0.9999,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xis, omegas],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


# Lc -> L KS K and partially reconstructed Lc -> Sigma0 KS K. Expected rate 1.2 kHz
@register_line_builder(all_lines)
def lc_to_lksk_ll_line(name="Hlt2Charm_LcpToL0KsKp_LLLL"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    ll_kshorts = _make_ll_kshorts()
    lcs = ParticleCombiner(
        [ll_lambdas, ll_kshorts, _make_std_kaons_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Lambda0 KS0 K+]cc",
        name="Charm_Hyperons_LcpToL0KsKp_LLLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 1915 * MeV,
            F.MAXDOCACUT(200 * um),
        ),
        CombinationCut=F.require_all(
            F.MASS < 2405 * MeV,
            F.PT > 2.3 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.DOCA(1, 3) < 120 * um,
            F.DOCA(2, 3) < 150 * um,
        ),
        CompositeCut=F.require_all(
            F.MASS < 2385 * MeV,
            F.PT > 2.4 * GeV,
            F.CHI2DOF < 5.0,
            _DZ_CHILD(1) > 4 * mm,
            _DZ_CHILD(2) > 2 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, ll_kshorts, lcs])


@register_line_builder(all_lines)
def lc_to_lksk_dl_line(name="Hlt2Charm_LcpToL0KsKp_DDLL"):
    dd_lambdas = _make_dd_lambdas()
    ll_kshorts = _make_ll_kshorts()
    lcs = ParticleCombiner(
        [dd_lambdas, ll_kshorts, _make_std_kaons_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Lambda0 KS0 K+]cc",
        name="Charm_Hyperons_LcpToL0KsKp_DDLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 1915 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        CombinationCut=F.require_all(
            F.MASS < 2405 * MeV,
            F.PT > 2.4 * GeV,
            F.SUM(F.PT) > 2.8 * GeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.MASS < 2385 * MeV,
            F.PT > 2.6 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(2) > 2 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, ll_kshorts, lcs])


@register_line_builder(all_lines)
def lc_to_lksk_ld_line(name="Hlt2Charm_LcpToL0KsKp_LLDD"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    dd_kshorts = _make_dd_kshorts()
    lcs = ParticleCombiner(
        [ll_lambdas, dd_kshorts, _make_tight_kaons_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Lambda0 KS0 K+]cc",
        name="Charm_Hyperons_LcpToL0KsKp_LLDD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 1915 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        CombinationCut=F.require_all(
            F.MASS < 2405 * MeV,
            F.PT > 2.4 * GeV,
            F.SUM(F.PT) > 2.8 * GeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 2 * mm,
        ),
        CompositeCut=F.require_all(
            F.MASS < 2385 * MeV,
            F.PT > 2.6 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_kshorts, ll_lambdas, lcs])


# Lb0 -> Lc+ pi- with Lc+ -> S- pi+ pi+. Expected rate negligible
@register_line_builder(all_lines)
def lcp_to_smkpi_longsm_line(name="Hlt2Charm_Lb0ToLcpPim_LcpToSmPipPip_LongSm_SP"):
    pions = _make_verytight_pions_for_charm()
    sigmas = ParticleFilter(
        make_long_sigmams(),
        F.FILTER(
            F.require_all(
                F.PT > 1.6 * GeV,
                F.P > 22 * GeV,
                F.OWNPVIPCHI2 > 16.0,
                F.PROBNN_P > 0.75,
                F.PROBNN_P * (1.0 - F.PROBNN_K) > 0.5,
            )
        ),
    )
    lcs = ParticleCombiner(
        [sigmas, pions, pions],
        DecayDescriptor="[Lambda_c+ -> Sigma- pi+ pi+]cc",
        name="Charm_Hyperons_LcpToSmPipPip_{hash}",
        Combination12Cut=F.MASS < 2450 * MeV,
        CombinationCut=F.require_all(
            F.math.in_range(2165 * MeV, F.MASS, 2405 * MeV),
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.MAXSDOCACUT(120 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2185 * MeV, F.MASS, 2385 * MeV),
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 12.0,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 7.0,
        ),
    )
    lbs = ParticleCombiner(
        [lcs, _make_long_tracks_for_beauty()],
        DecayDescriptor="[Lambda_b0 -> Lambda_c+ pi-]cc",
        name="Charm_Hyperons_Lb0ToLcpPim_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda_c+"]),
        CombinationCut=F.require_all(
            F.math.in_range(5 * GeV, F.MASS, 6 * GeV),
            F.PT > 3.8 * GeV,
            F.SUM(F.PT) > 4 * GeV,
            F.MAXSDOCACUT(120 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(5.1 * GeV, F.MASS, 5.9 * GeV),
            F.PT > 4 * GeV,
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > -0.5 * mm,
            F.OWNPVVDZ > 0 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 6.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9999,
        ),
    )

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [sigmas, lcs, lbs],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


# Lc -> Xi K pi. Expected rate 500 Hz
@register_line_builder(all_lines)
def lc_to_ximkpi_lll_line(name="Hlt2Charm_LcpToXimKpPip_LLL"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    lcs = ParticleCombiner(
        [lll_xis, _make_loose_kaons_for_charm(), _make_std_pions_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Xi- K+ pi+]cc",
        name="Charm_Hyperons_LcpToXimKpPip_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        Combination12Cut=F.MASS < 2265 * MeV,
        CombinationCut=F.require_all(
            F.math.in_range(2165 * MeV, F.MASS, 2405 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2185 * MeV, F.MASS, 2385 * MeV),
            F.PT > 1.4 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 0.8 * mm,
            F.OWNPVVDRHO > 75 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.999,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, lcs])


@register_line_builder(all_lines)
def lc_to_ximkpi_ddl_line(name="Hlt2Charm_LcpToXimKpPip_DDL"):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    lcs = ParticleCombiner(
        [ddl_xis, _make_loose_kaons_for_charm(), _make_std_pions_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Xi- K+ pi+]cc",
        name="Charm_Hyperons_LcpToXimKpPip_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        Combination12Cut=F.require_all(
            F.MASS < 2265 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2165 * MeV, F.MASS, 2405 * MeV),
            F.PT > 1.7 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 120 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2185 * MeV, F.MASS, 2385 * MeV),
            F.PT > 1.8 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 0.8 * mm,
            F.OWNPVVDRHO > 75 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.999,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, lcs])


# Xic+ -> L KS pi and partially reconstructed Xic+ -> Sigma0 KS pi. Expected rate 2.9 kHz
@register_line_builder(all_lines)
def xicp_to_lkspi_ll_line(name="Hlt2Charm_XicpToL0KsPip_LLLL"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    ll_kshorts = _make_ll_kshorts()
    xicps = ParticleCombiner(
        [ll_lambdas, ll_kshorts, _make_tight_pions_for_charm()],
        DecayDescriptor="[Xi_c+ -> Lambda0 KS0 pi+]cc",
        name="Charm_Hyperons_XicpToL0KsPip_LLLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(200 * um),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2260 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.3 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.DOCA(1, 3) < 120 * um,
            F.DOCA(2, 3) < 150 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2280 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2.4 * GeV,
            F.CHI2DOF < 5.0,
            _DZ_CHILD(1) > 4 * mm,
            _DZ_CHILD(2) > 2 * mm,
            F.OWNPVVDZ > 1.2 * mm,
            F.OWNPVVDRHO > 150 * um,
            F.OWNPVDLS > 4.2,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9997,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, ll_kshorts, xicps]
    )


@register_line_builder(all_lines)
def xicp_to_lkspi_dl_line(name="Hlt2Charm_XicpToL0KsPip_DDLL"):
    dd_lambdas = _make_dd_lambdas()
    ll_kshorts = _make_ll_kshorts()
    xicps = ParticleCombiner(
        [dd_lambdas, ll_kshorts, _make_verytight_pions_for_charm()],
        DecayDescriptor="[Xi_c+ -> Lambda0 KS0 pi+]cc",
        name="Charm_Hyperons_XicpToL0KsPip_DDLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2260 * MeV, F.MASS, 2590 * MeV),
            F.PT > 3 * GeV,
            F.SUM(F.PT) > 3.4 * GeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 120 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2280 * MeV, F.MASS, 2570 * MeV),
            F.PT > 3.2 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(2) > 2 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 150 * um,
            F.OWNPVDLS > 4.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ll_kshorts, xicps]
    )


@register_line_builder(all_lines)
def xicp_to_lkspi_ld_line(name="Hlt2Charm_XicpToL0KsPip_LLDD"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    dd_kshorts = _make_dd_kshorts()
    xicps = ParticleCombiner(
        [ll_lambdas, dd_kshorts, _make_tight_pions_for_charm()],
        DecayDescriptor="[Xi_c+ -> Lambda0 KS0 pi+]cc",
        name="Charm_Hyperons_XicpToL0KsPip_LLDD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2260 * MeV, F.MASS, 2590 * MeV),
            F.PT > 3 * GeV,
            F.SUM(F.PT) > 3.4 * GeV,
            F.DOCA(1, 3) < 120 * um,
            F.DOCA(2, 3) < 2 * mm,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2280 * MeV, F.MASS, 2570 * MeV),
            F.PT > 3.2 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 150 * um,
            F.OWNPVDLS > 4.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_kshorts, ll_lambdas, xicps]
    )


# Xic+ -> L KS K and partially reconstructed Xic+ -> Sigma0 KS K. Expected rate 80 Hz
@register_line_builder(all_lines)
def xicp_to_lksk_ll_line(name="Hlt2Charm_XicpToL0KsKp_LLLL"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    ll_kshorts = _make_ll_kshorts()
    xicps = ParticleCombiner(
        [ll_lambdas, ll_kshorts, _make_std_kaons_for_charm()],
        DecayDescriptor="[Xi_c+ -> Lambda0 KS0 K+]cc",
        name="Charm_Hyperons_XicpToL0KsKp_LLLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV,
            F.MAXDOCACUT(200 * um),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2295 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.3 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.DOCA(1, 3) < 120 * um,
            F.DOCA(2, 3) < 150 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2305 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2.4 * GeV,
            F.CHI2DOF < 7.0,
            _DZ_CHILD(1) > 4 * mm,
            _DZ_CHILD(2) > 2 * mm,
            F.OWNPVVDZ > 1.2 * mm,
            F.OWNPVVDRHO > 150 * um,
            F.OWNPVDLS > 4.2,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9997,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, ll_kshorts, xicps]
    )


@register_line_builder(all_lines)
def xicp_to_lksk_dl_line(name="Hlt2Charm_XicpToL0KsKp_DDLL"):
    dd_lambdas = _make_dd_lambdas()
    ll_kshorts = _make_ll_kshorts()
    xicps = ParticleCombiner(
        [dd_lambdas, ll_kshorts, _make_std_kaons_for_charm()],
        DecayDescriptor="[Xi_c+ -> Lambda0 KS0 K+]cc",
        name="Charm_Hyperons_XicpToL0KsKp_DDLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV,
            F.MAXDOCACUT(3 * um),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2295 * MeV, F.MASS, 2590 * MeV),
            F.PT > 3 * GeV,
            F.SUM(F.PT) > 3.4 * GeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 120 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2305 * MeV, F.MASS, 2570 * MeV),
            F.PT > 3.2 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(2) > 2 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 150 * um,
            F.OWNPVDLS > 4.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ll_kshorts, xicps]
    )


@register_line_builder(all_lines)
def xicp_to_lksk_ld_line(name="Hlt2Charm_XicpToL0KsKp_LLDD"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    dd_kshorts = _make_dd_kshorts()
    xicps = ParticleCombiner(
        [ll_lambdas, dd_kshorts, _make_std_kaons_for_charm()],
        DecayDescriptor="[Xi_c+ -> Lambda0 KS0 K+]cc",
        name="Charm_Hyperons_XicpToL0KsKp_LLDD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2295 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.DOCA(1, 3) < 120 * um,
            F.DOCA(2, 3) < 2 * mm,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2305 * MeV, F.MASS, 2570 * MeV),
            F.PT > 3.4 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 150 * um,
            F.OWNPVDLS > 4.2,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_kshorts, ll_lambdas, xicps]
    )


# Xic+ -> Xi- pi+ pi+. Expected rate 800 Hz
@register_line_builder(all_lines)
def xicp_to_ximpipi_lll_line(name="Hlt2Charm_XicpToXimPipPip_LLL"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    long_xicp_pions = _make_std_pions_for_charm()
    xicps = ParticleCombiner(
        [lll_xis, long_xicp_pions, long_xicp_pions],
        DecayDescriptor="[Xi_c+ -> Xi- pi+ pi+]cc",
        name="Charm_Hyperons_XicpToXimPipPip_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        Combination12Cut=F.MASS < 2450 * MeV,
        CombinationCut=F.require_all(
            F.math.in_range(2390 * MeV, F.MASS, 2550 * MeV),
            F.PT > 1.3 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.require_any(
                F.CHILD(2, F.OWNPVIPCHI2) > 9.0, F.CHILD(3, F.OWNPVIPCHI2) > 9.0
            ),
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2410 * MeV, F.MASS, 2530 * MeV),
            F.PT > 1.4 * GeV,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.CHI2DOF < 9.0,
            F.OWNPVDLS > 4.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, xicps])


@register_line_builder(all_lines)
def xicp_to_ximpipi_ddl_line(name="Hlt2Charm_XicpToXimPipPip_DDL"):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    long_xicp_pions = _make_std_pions_for_charm()
    xicps = ParticleCombiner(
        [ddl_xis, long_xicp_pions, long_xicp_pions],
        DecayDescriptor="[Xi_c+ -> Xi- pi+ pi+]cc",
        name="Charm_Hyperons_XicpToXimPipPip_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.7 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.require_any(
                F.CHILD(2, F.OWNPVIPCHI2) > 12.0, F.CHILD(3, F.OWNPVIPCHI2) > 12.0
            ),
            F.DOCA(2, 3) < 120 * um,
            F.DOCA(1, 3) < 2 * mm,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.8 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 4.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, xicps])


@register_line_builder(all_lines)
def xicp_to_ximpipi_lllinclb_line(name="Hlt2Charm_XicpToXimPipPip_LLL_Inclb_SP"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    xicps = _make_detached_xicp_to_ximpipi_lll(lll_xis, _make_tight_pions_for_charm())
    b_to_xich = _make_bbaryon_to_cbaryonttrack(
        xicps, _make_long_tracks_for_beauty(), "[Xi_b0 -> Xi_c+ pi-]cc", ["Xi_c+"]
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, lll_xis, xicps, b_to_xich],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


@register_line_builder(all_lines)
def xicp_to_ximpipi_ddlinclb_line(name="Hlt2Charm_XicpToXimPipPip_DDL_Inclb_SP"):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    xicps = _make_detached_xicp_to_ximpipi_ddl(ddl_xis, _make_tight_pions_for_charm())
    b_to_xich = _make_bbaryon_to_cbaryonttrack(
        xicps, _make_long_tracks_for_beauty(), "[Xi_b0 -> Xi_c+ pi-]cc", ["Xi_c+"]
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, ddl_xis, xicps, b_to_xich],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


@register_line_builder(all_lines)
def xibz_to_xicpdm_xicp_to_ximpipi_lll_line(
    name="Hlt2Charm_Xib0ToXicpDm_XicpToXimPipPip_DmToKpPimPim_LLL",
):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    xicps = _make_detached_xicp_to_ximpipi_lll(lll_xis, _make_tight_pions_for_charm())
    dm = _make_d_to_kpipi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xicps, dm, "[Xi_b0 -> Xi_c+ D-]cc", ["Xi_c+", "D-"]
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, xicps, b_to_xich]
    )


@register_line_builder(all_lines)
def xibz_to_xicpdm_xicp_to_ximpipi_ddl_line(
    name="Hlt2Charm_Xib0ToXicpDm_XicpToXimPipPip_DmToKpPimPim_DDL",
):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    xicps = _make_detached_xicp_to_ximpipi_ddl(ddl_xis, _make_tight_pions_for_charm())
    dm = _make_d_to_kpipi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xicps, dm, "[Xi_b0 -> Xi_c+ D-]cc", ["Xi_c+", "D-"]
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, xicps, b_to_xich]
    )


@register_line_builder(all_lines)
def xibz_to_xicpdsm_xicp_to_ximpipi_lll_line(
    name="Hlt2Charm_Xib0ToXicpDsm_XicpToXimPipPip_DmDsmToKmKpPim_LLL",
):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    xicps = _make_detached_xicp_to_ximpipi_lll(lll_xis, _make_tight_pions_for_charm())
    dsm = _make_ds_to_kkpi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xicps, dsm, "[Xi_b0 -> Xi_c+ D_s-]cc", ["Xi_c+", "D_s-"]
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, xicps, b_to_xich]
    )


@register_line_builder(all_lines)
def xibz_to_xicpdsm_xicp_to_ximpipi_ddl_line(
    name="Hlt2Charm_Xib0ToXicpDsm_XicpToXimPipPip_DmDsmToKmKpPim_DDL",
):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    xicps = _make_detached_xicp_to_ximpipi_ddl(ddl_xis, _make_tight_pions_for_charm())
    dsm = _make_ds_to_kkpi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xicps, dsm, "[Xi_b0 -> Xi_c+ D_s-]cc", ["Xi_c+", "D_s-"]
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, xicps, b_to_xich]
    )


@register_line_builder(all_lines)
def xicp_to_ximpipi_longxim_line(
    name="Hlt2Charm_Xib0ToXicpPim_XicpToXimPipPip_LongXi_SP",
):
    pions = _make_tight_pions_for_charm()
    xis = ParticleFilter(
        make_long_xis(),
        F.FILTER(
            F.require_all(
                F.PT > 1.5 * GeV,
                F.P > 25 * GeV,
                F.OWNPVIPCHI2 > 16.0,
                F.PROBNN_P > 0.75,
                F.PROBNN_P * (1.0 - F.PROBNN_K) > 0.5,
            )
        ),
    )
    xicps = ParticleCombiner(
        [xis, pions, pions],
        DecayDescriptor="[Xi_c+ -> Xi- pi+ pi+]cc",
        name="Charm_Hyperons_XicpToXiPipPip_XiLong_{hash}",
        Combination12Cut=F.MASS < 2450 * MeV,
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.3 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.MAXSDOCACUT(150 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2.4 * GeV,
            F.CHI2DOF < 9.0,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 6.0,
        ),
    )
    xib0s = _make_xib0_to_xicppim(xicps, _make_long_tracks_for_beauty())

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [xis, xicps, xib0s],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


# Xic+ -> Xi- K+ pi+. Expected rate 15 Hz
@register_line_builder(all_lines)
def xicp_to_ximkpi_lll_line(name="Hlt2Charm_XicpToXimKpPip_LLL"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    xicps = ParticleCombiner(
        [lll_xis, _make_std_kaons_for_charm(), _make_std_pions_for_charm()],
        DecayDescriptor="[Xi_c+ -> Xi- K+ pi+]cc",
        name="Charm_Hyperons_XicpToXimKpPip_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        Combination12Cut=F.MASS < 2450 * MeV,
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.6 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 4.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, xicps])


@register_line_builder(all_lines)
def xicp_to_ximkpi_ddl_line(name="Hlt2Charm_XicpToXimKpPip_DDL"):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    xicps = ParticleCombiner(
        [ddl_xis, _make_std_kaons_for_charm(), _make_std_pions_for_charm()],
        DecayDescriptor="[Xi_c+ -> Xi- K+ pi+]cc",
        name="Charm_Hyperons_XicpToXimKpPip_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.9 * GeV,
            F.SUM(F.PT) > 2.4 * GeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 120 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 4.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, xicps])


# Xic+ -> Omega- K+ pi+. Expected rate 75 Hz
@register_line_builder(all_lines)
def xicp_to_omkpi_lll_line(name="Hlt2Charm_XicpToOmKpPip_LLL"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_oms = _make_lll_omegas(ll_lambdas, _make_long_kaons_for_omega())
    xicps = ParticleCombiner(
        [lll_oms, _make_std_kaons_for_charm(), _make_std_pions_for_charm()],
        DecayDescriptor="[Xi_c+ -> Omega- K+ pi+]cc",
        name="Charm_Hyperons_XicpToOmKpPip_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Omega-"]),
        Combination12Cut=F.MASS < 2450 * MeV,
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.5 * GeV,
            F.SUM(F.PT) > 2 * GeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.6 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 1 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lll_oms, xicps])


@register_line_builder(all_lines)
def xicp_to_omkpi_ddl_line(name="Hlt2Charm_XicpToOmKpPip_DDL"):
    dd_lambdas = _make_dd_lambdas()
    ddl_oms = _make_ddl_omegas(dd_lambdas, _make_long_kaons_for_omega())
    xicps = ParticleCombiner(
        [ddl_oms, _make_std_kaons_for_charm(), _make_std_pions_for_charm()],
        DecayDescriptor="[Xi_c+ -> Omega- K+ pi+]cc",
        name="Charm_Hyperons_XicpToOmKpPip_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Omega-"]),
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 1.9 * GeV,
            F.SUM(F.PT) > 2.4 * GeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 120 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 1 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, ddl_oms, xicps])


@register_line_builder(all_lines)
def xicp_to_omkpi_longom_line(name="Hlt2Charm_Xib0ToXicpPim_XicpToOmKpPip_LongOm_SP"):
    kaons = _make_tight_kaons_for_charm()
    pions = _make_tight_pions_for_charm()
    omegas = ParticleFilter(
        make_long_omegas(),
        F.FILTER(
            F.require_all(
                F.PT > 1.6 * GeV,
                F.P > 32 * GeV,
                F.OWNPVIPCHI2 > 16.0,
                F.PROBNN_P > 0.75,
                F.PROBNN_P * (1.0 - F.PROBNN_K) > 0.5,
            )
        ),
    )
    xicps = ParticleCombiner(
        [omegas, kaons, pions],
        DecayDescriptor="[Xi_c+ -> Omega- K+ pi+]cc",
        name="Charm_Hyperons_XicpToOmKpPip_OmLong_{hash}",
        Combination12Cut=F.MASS < 2450 * MeV,
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.4 * GeV,
            F.SUM(F.PT) > 1.8 * GeV,
            F.MAXSDOCACUT(150 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2.5 * GeV,
            F.CHI2DOF < 9.0,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 6.0,
        ),
    )
    xib0s = _make_xib0_to_xicppim(xicps, _make_long_tracks_for_beauty())

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [omegas, xicps, xib0s],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


# Xic0 -> L pi- pi+ and partially reconstructed Xic0 -> Sigma0 pi- pi+. Expected rate 1.6 kHz
@register_line_builder(all_lines)
def xicz_to_lpipi_ll_line(name="Hlt2Charm_Xic0ToL0PimPip_LL"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    long_xicz_pions = _make_tight_pions_for_charm()
    xic0s = ParticleCombiner(
        [ll_lambdas, long_xicz_pions, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 pi- pi+]cc",
        name="Charm_Hyperons_Xic0ToL0PimPip_LL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        Combination12Cut=F.MASS < 2450 * MeV,
        CombinationCut=F.require_all(
            F.math.in_range(2250 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.5 * GeV,
            F.require_any(
                F.CHILD(2, F.OWNPVIPCHI2) > 12.0, F.CHILD(3, F.OWNPVIPCHI2) > 12.0
            ),
            F.SUM(F.PT) > 3 * GeV,
            F.MAXDOCACUT(150 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2270 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2.6 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 4.0,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, xic0s])


@register_line_builder(all_lines)
def xicz_to_lpipi_dd_line(name="Hlt2Charm_Xic0ToL0PimPip_DD"):
    dd_lambdas = _make_dd_lambdas()
    long_xicz_pions = _make_tight_pions_for_charm()
    xic0s = ParticleCombiner(
        [dd_lambdas, long_xicz_pions, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 pi- pi+]cc",
        name="Charm_Hyperons_Xic0ToL0PimPip_DD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2250 * MeV, F.MASS, 2590 * MeV),
            F.require_any(
                F.CHILD(2, F.OWNPVIPCHI2) > 12.0, F.CHILD(3, F.OWNPVIPCHI2) > 12.0
            ),
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 150 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2270 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 4.0,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, xic0s])


# Xic0 -> L K- pi+ and partially reconstructed Xic0 -> Sigma0 K- pi+. Expected rate 2.3 kHz
# Oc -> L K- pi+ and partially reconstructed Oc -> Sigma0 K- pi+. Expected rate 450 Hz
@register_line_builder(all_lines)
def xiczoc_to_lkpi_ll_line(name="Hlt2Charm_Xic0Oc0ToL0KmPip_LL"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    xic0s = ParticleCombiner(
        [ll_lambdas, _make_std_kaons_for_charm(), _make_std_pions_for_charm()],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- pi+]cc",
        name="Charm_Hyperons_Xic0Oc0ToL0KmPip_LL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        Combination12Cut=F.MASS < 2675 * MeV,
        CombinationCut=F.require_all(
            F.math.in_range(2260 * MeV, F.MASS, 2815 * MeV),
            F.require_any(
                F.CHILD(2, F.OWNPVIPCHI2) > 9.0, F.CHILD(3, F.OWNPVIPCHI2) > 9.0
            ),
            F.PT > 2.5 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.MAXDOCACUT(200 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2280 * MeV, F.MASS, 2795 * MeV),
            F.PT > 2.6 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 4.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, xic0s])


@register_line_builder(all_lines)
def xiczoc_to_lkpi_dd_line(name="Hlt2Charm_Xic0Oc0ToL0KmPip_DD"):
    dd_lambdas = _make_dd_lambdas()
    xic0s = ParticleCombiner(
        [dd_lambdas, _make_std_kaons_for_charm(), _make_std_pions_for_charm()],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- pi+]cc",
        name="Charm_Hyperons_Xic0Oc0ToL0KmPip_DD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2260 * MeV, F.MASS, 2815 * MeV),
            F.require_any(
                F.CHILD(2, F.OWNPVIPCHI2) > 9.0, F.CHILD(3, F.OWNPVIPCHI2) > 9.0
            ),
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 200 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2280 * MeV, F.MASS, 2795 * MeV),
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 4.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, xic0s])


@register_line_builder(all_lines)
def xicz_to_lkpi_llinclb_line(name="Hlt2Charm_Xic0ToL0KmPip_LL_Inclb_SP"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    xic0s = _make_detached_xicz_to_lkpi_ll(
        ll_lambdas, _make_tight_kaons_for_charm(), _make_tight_pions_for_charm()
    )
    b_to_xich = _make_bbaryon_to_cbaryonttrack(
        xic0s, _make_long_tracks_for_beauty(), "[Xi_b- -> Xi_c0 pi-]cc", ["Xi_c0"]
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ll_lambdas, xic0s, b_to_xich],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


@register_line_builder(all_lines)
def xicz_to_lkpi_ddinclb_line(name="Hlt2Charm_Xic0ToL0KmPip_DD_Inclb_SP"):
    dd_lambdas = _make_dd_lambdas()
    xic0s = _make_detached_xicz_to_lkpi_dd(
        dd_lambdas, _make_tight_kaons_for_charm(), _make_tight_pions_for_charm()
    )
    b_to_xich = _make_bbaryon_to_cbaryonttrack(
        xic0s, _make_long_tracks_for_beauty(), "[Xi_b- -> Xi_c0 pi-]cc", ["Xi_c0"]
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dd_lambdas, xic0s, b_to_xich],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


@register_line_builder(all_lines)
def xibm_to_xiczdm_xicz_to_lkpi_ll_line(
    name="Hlt2Charm_XibmToXic0Dm_Xic0ToL0KmPip_DmToKpPimPim_LL",
):
    ll_lambdas = _make_ll_lambdas_for_charm()
    xic0s = _make_detached_xicz_to_lkpi_ll(
        ll_lambdas, _make_tight_kaons_for_charm(), _make_tight_pions_for_charm()
    )
    dm = _make_d_to_kpipi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0s, dm, "[Xi_b- -> Xi_c0 D-]cc", ["Xi_c0", "D-"]
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, xic0s, b_to_xich])


@register_line_builder(all_lines)
def xibm_to_xiczdm_xicz_to_lkpi_dd_line(
    name="Hlt2Charm_XibmToXic0Dm_Xic0ToL0KmPip_DmToKpPimPim_DD",
):
    dd_lambdas = _make_dd_lambdas()
    xic0s = _make_detached_xicz_to_lkpi_dd(
        dd_lambdas, _make_tight_kaons_for_charm(), _make_tight_pions_for_charm()
    )
    dm = _make_d_to_kpipi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0s, dm, "[Xi_b- -> Xi_c0 D-]cc", ["Xi_c0", "D-"]
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, xic0s, b_to_xich])


@register_line_builder(all_lines)
def xibm_to_xiczdsm_xicz_to_lkpi_ll_line(
    name="Hlt2Charm_XibmToXic0Dsm_Xic0ToL0KmPip_DmDsmToKmKpPim_LL",
):
    ll_lambdas = _make_ll_lambdas_for_charm()
    xic0s = _make_detached_xicz_to_lkpi_ll(
        ll_lambdas, _make_tight_kaons_for_charm(), _make_tight_pions_for_charm()
    )
    dsm = _make_ds_to_kkpi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0s, dsm, "[Xi_b- -> Xi_c0 D_s-]cc", ["Xi_c0", "D_s-"]
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, xic0s, b_to_xich])


@register_line_builder(all_lines)
def xibm_to_xiczdsm_xicz_to_lkpi_dd_line(
    name="Hlt2Charm_XibmToXic0Dsm_Xic0ToL0KmPip_DmDsmToKmKpPim_DD",
):
    dd_lambdas = _make_dd_lambdas()
    xic0s = _make_detached_xicz_to_lkpi_dd(
        dd_lambdas, _make_tight_kaons_for_charm(), _make_tight_pions_for_charm()
    )
    dsm = _make_ds_to_kkpi()
    b_to_xich = _make_bbaryon_to_cbaryond(
        xic0s, dsm, "[Xi_b- -> Xi_c0 D_s-]cc", ["Xi_c0", "D_s-"]
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, xic0s, b_to_xich])


# Xic0 -> L K K and partially reconstructed Xic0 -> Sigma0 K- K+. Expected rate 128 Hz
# Oc -> L K K and partially reconstructed Oc -> Sigma0 K- K+. Expected rate 45 Hz
@register_line_builder(all_lines)
def xiczoc_to_lkk_ll_line(name="Hlt2Charm_Xic0Oc0ToL0KmKp_LL"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    long_xicz_kaons = _make_std_kaons_for_charm()
    xic0s = ParticleCombiner(
        [ll_lambdas, long_xicz_kaons, long_xicz_kaons],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- K+]cc",
        name="Charm_Hyperons_Xic0Oc0ToL0KmKp_LL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        Combination12Cut=F.MASS < 2320 * MeV,
        CombinationCut=F.require_all(
            F.math.in_range(2280 * MeV, F.MASS, 2815 * MeV),
            F.PT > 2.5 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.require_any(
                F.CHILD(2, F.OWNPVIPCHI2) > 9.0, F.CHILD(3, F.OWNPVIPCHI2) > 9.0
            ),
            F.MAXDOCACUT(150 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2300 * MeV, F.MASS, 2795 * MeV),
            F.PT > 2.6 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 4.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, xic0s])


@register_line_builder(all_lines)
def xiczoc_to_lkk_dd_line(name="Hlt2Charm_Xic0Oc0ToL0KmKp_DD"):
    dd_lambdas = _make_dd_lambdas()
    long_xicz_kaons = _make_std_kaons_for_charm()
    xic0s = ParticleCombiner(
        [dd_lambdas, long_xicz_kaons, long_xicz_kaons],
        DecayDescriptor="[Xi_c0 -> Lambda0 K- K+]cc",
        name="Charm_Hyperons_Xic0Oc0ToL0KmKp_DD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2320 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2280 * MeV, F.MASS, 2815 * MeV),
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.require_any(
                F.CHILD(2, F.OWNPVIPCHI2) > 9.0, F.CHILD(3, F.OWNPVIPCHI2) > 9.0
            ),
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 150 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2300 * MeV, F.MASS, 2795 * MeV),
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 4.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, xic0s])


# Xi_c0 -> Sigma- KS pi+. Expected rate negligible.
@register_line_builder(all_lines)
def xic0_to_smkspip_ll_longsm_inclb_line(
    name="Hlt2Charm_Xic0ToSmKsPip_LL_LongSm_Inclb_SP",
):
    sigmas = ParticleFilter(
        make_long_sigmams(),
        F.FILTER(
            F.require_all(
                F.PT > 1.6 * GeV,
                F.P > 22 * GeV,
                F.OWNPVIPCHI2 > 9.0,
                F.PROBNN_P > 0.75,
                F.PROBNN_P * (1.0 - F.PROBNN_K) > 0.5,
            )
        ),
    )
    xic0s = ParticleCombiner(
        [sigmas, _make_ll_kshorts(), _make_std_pions_for_charm()],
        DecayDescriptor="[Xi_c0 -> Sigma- KS0 pi+]cc",
        name="Charm_Hyperons_Xic0ToSmKspPip_LL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(200 * um),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.DOCA(1, 3) < 100 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 2 * mm,
            F.OWNPVVDRHO > 0.2 * mm,
            F.OWNPVDLS > 6.0,
        ),
    )
    xibs = _make_bbaryon_to_cbaryonttrack(
        xic0s, _make_long_tracks_for_beauty(), "[Xi_b- -> Xi_c0 pi+]cc", ["Xi_c0"]
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [sigmas, xic0s, xibs],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


@register_line_builder(all_lines)
def xic0_to_smkspip_dd_longsm_inclb_line(
    name="Hlt2Charm_Xic0ToSmKsPip_DD_LongSm_Inclb_SP",
):
    sigmas = ParticleFilter(
        make_long_sigmams(),
        F.FILTER(
            F.require_all(
                F.PT > 1.6 * GeV,
                F.P > 22 * GeV,
                F.OWNPVIPCHI2 > 9.0,
                F.PROBNN_P > 0.75,
                F.PROBNN_P * (1.0 - F.PROBNN_K) > 0.5,
            )
        ),
    )
    xic0s = ParticleCombiner(
        [sigmas, _make_dd_kshorts(), _make_std_pions_for_charm()],
        DecayDescriptor="[Xi_c0 -> Sigma- KS0 pi+]cc",
        name="Charm_Hyperons_Xic0ToSmKspPip_DD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.9 * GeV,
            F.SUM(F.PT) > 3.4 * GeV,
            F.DOCA(1, 3) < 100 * um,
            F.DOCA(2, 3) < 2 * mm,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 3 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 2 * mm,
            F.OWNPVVDRHO > 0.2 * mm,
            F.OWNPVDLS > 6.0,
        ),
    )
    xibs = _make_bbaryon_to_cbaryonttrack(
        xic0s, _make_long_tracks_for_beauty(), "[Xi_b- -> Xi_c0 pi+]cc", ["Xi_c0"]
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [sigmas, xic0s, xibs],
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


# Xic0 -> Xi KS pi. Expected rate 10 Hz
# Oc -> Xi KS pi. Expected rate 140 Hz
@register_line_builder(all_lines)
def xiczoc_to_ximkspi_ll_line(name="Hlt2Charm_Xic0Oc0ToXimKsPip_LLLLL"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    ll_kshorts = _make_ll_kshorts()
    xic0s = ParticleCombiner(
        [lll_xis, ll_kshorts, _make_std_pions_for_charm()],
        DecayDescriptor="[Xi_c0 -> Xi- KS0 pi+]cc",
        name="Charm_Hyperons_Xic0Oc0ToXimKsPip_LLLLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.MAXDOCACUT(250 * um),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2815 * MeV),
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.DOCA(1, 3) < 150 * um,
            F.DOCA(2, 3) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2795 * MeV),
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 2 * mm,
            _DZ_CHILD(2) > 2 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.999,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, ll_kshorts, xic0s]
    )


@register_line_builder(all_lines)
def xiczoc_to_ximkspi_ld_line(name="Hlt2Charm_Xic0Oc0ToXimKsPip_LLLDD"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    dd_kshorts = _make_dd_kshorts()
    long_xicz_pions = _make_std_pions_for_charm()
    xic0s = ParticleCombiner(
        [lll_xis, dd_kshorts, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Xi- KS0 pi+]cc",
        name="Charm_Hyperons_Xic0Oc0ToXimKsPip_LLLDD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2815 * MeV),
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.DOCA(1, 3) < 150 * um,
            F.DOCA(2, 3) < 2 * mm,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2795 * MeV),
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.999,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, dd_kshorts, xic0s]
    )


@register_line_builder(all_lines)
def xiczoc_to_ximkspi_dl_line(name="Hlt2Charm_Xic0Oc0ToXimKsPip_DDLLL"):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    ll_kshorts = _make_ll_kshorts()
    xic0s = ParticleCombiner(
        [ddl_xis, ll_kshorts, _make_std_pions_for_charm()],
        DecayDescriptor="[Xi_c0 -> Xi- KS0 pi+]cc",
        name="Charm_Hyperons_Xic0Oc0ToXimKsPip_DDLLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2815 * MeV),
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2795 * MeV),
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 2 * mm,
            _DZ_CHILD(2) > 2 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.999,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, ll_kshorts, xic0s]
    )


@register_line_builder(all_lines)
def xiczoc_to_ximkspi_dd_line(name="Hlt2Charm_Xic0Oc0ToXimKsPip_DDLDD"):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    dd_kshorts = _make_dd_kshorts()
    xic0s = ParticleCombiner(
        [ddl_xis, dd_kshorts, _make_std_pions_for_charm()],
        DecayDescriptor="[Xi_c0 -> Xi- KS0 pi+]cc",
        name="Charm_Hyperons_Xic0Oc0ToXimKsPip_DDLDD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2815 * MeV),
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 2 * mm,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2795 * MeV),
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.999,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, dd_kshorts, xic0s]
    )


# Xic0 -> Xi KS K. Expected rate 15 Hz
@register_line_builder(all_lines)
def xicz_to_ximksk_ll_line(name="Hlt2Charm_Xic0ToXimKsKp_LLLLL"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    ll_kshorts = _make_ll_kshorts()
    xic0s = ParticleCombiner(
        [lll_xis, ll_kshorts, _make_std_kaons_for_charm()],
        DecayDescriptor="[Xi_c0 -> Xi- KS0 K+]cc",
        name="Charm_Hyperons_Xic0ToXimKsKp_LLLLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV,
            F.MAXDOCACUT(250 * um),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.DOCA(1, 3) < 150 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 2 * mm,
            _DZ_CHILD(2) > 2 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.999,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, ll_kshorts, xic0s]
    )


@register_line_builder(all_lines)
def xicz_to_ximksk_ld_line(name="Hlt2Charm_Xic0ToXimKsKp_LLLDD"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    dd_kshorts = _make_dd_kshorts()
    xic0s = ParticleCombiner(
        [lll_xis, dd_kshorts, _make_std_kaons_for_charm()],
        DecayDescriptor="[Xi_c0 -> Xi- KS0 K+]cc",
        name="Charm_Hyperons_Xic0ToXimKsKp_LLLDD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.DOCA(1, 3) < 150 * um,
            F.DOCA(2, 3) < 2 * mm,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.999,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, dd_kshorts, xic0s]
    )


@register_line_builder(all_lines)
def xicz_to_ximksk_dl_line(name="Hlt2Charm_Xic0ToXimKsKp_DDLLL"):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    ll_kshorts = _make_ll_kshorts()
    long_xicz_kaons = _make_std_kaons_for_charm()
    xic0s = ParticleCombiner(
        [ddl_xis, ll_kshorts, long_xicz_kaons],
        DecayDescriptor="[Xi_c0 -> Xi- KS0 K+]cc",
        name="Charm_Hyperons_Xic0ToXimKsKp_DDLLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 2 * mm,
            _DZ_CHILD(2) > 2 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.999,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, ll_kshorts, xic0s]
    )


@register_line_builder(all_lines)
def xicz_to_ximksk_dd_line(name="Hlt2Charm_Xic0ToXimKsKp_DDLDD"):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    dd_kshorts = _make_dd_kshorts()
    xic0s = ParticleCombiner(
        [ddl_xis, dd_kshorts, _make_std_kaons_for_charm()],
        DecayDescriptor="[Xi_c0 -> Xi- KS0 K+]cc",
        name="Charm_Hyperons_Xic0ToXimKsKp_DDLDD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 2 * mm,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.999,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, dd_kshorts, xic0s]
    )


########################
## (quasi-) four-body ##
########################
# Lc -> L pi+ pi+ pi-. Expected rate 15 kHz
# TO-DO: Maybe include D2DVVDCHI2_MIN = 60.0 cut present at Run2
@register_line_builder(all_lines)
def lc_to_l0pipipi_ll_line(name="Hlt2Charm_LcpToL0PimPipPip_LL"):
    protons = ParticleFilter(
        make_has_rich_long_protons(),
        F.FILTER(
            F.require_all(
                F.PT > 400 * MeV, F.P > 9 * GeV, F.OWNPVIPCHI2 > 6.0, F.PID_P > -5.0
            )
        ),
    )

    long_lambda_pions = ParticleFilter(
        make_long_pions(),
        F.FILTER(F.require_all(F.PT > 100 * MeV, F.OWNPVIPCHI2 > 32.0)),
    )

    ll_lambdas = ParticleCombiner(
        [protons, long_lambda_pions],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name="Charm_Hyperons_OLDLambdaFromC_LL_{hash}",
        CombinationCut=F.require_all(
            F.math.in_range(1082 * MeV, F.MASS, 1155 * MeV),
            F.PT > 500 * MeV,
            F.MAXDOCACUT(100 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(1095 * MeV, F.MASS, 1135 * MeV),
            F.PT > 600 * MeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 8 * mm,
            F.OWNPVFDCHI2 > 120.0,
        ),
    )
    long_lc_pions = ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 300 * MeV, F.P > 2 * GeV, F.OWNPVIPCHI2 > 5.0, F.PID_K < 5.0
            )
        ),
    )

    lcs = ParticleCombiner(
        [ll_lambdas, long_lc_pions, long_lc_pions, long_lc_pions],
        DecayDescriptor="[Lambda_c+ -> Lambda0 pi- pi+ pi+]cc",
        name="Charm_Hyperons_LcpToL0PimPipPip_LL_{hash}",
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV,
            F.MAXDOCACUT(200 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2240 * MeV,
            F.DOCA(1, 3) < 250 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2201 * MeV, F.MASS, 2371 * MeV),
            # F.PT > None, # 1.3 * GeV # Possible cuts for future tunning
            # F.P > None, # 28 * GeV # Possible cuts for future tunning
            F.SUM(F.PT) > 1.8 * GeV,
            F.MAXDOCACUT(250.0 * um),
            F.MAXDOCACHI2CUT(12.0),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2211 * MeV, F.MASS, 2361 * MeV),
            # F.PT > None, # 1.4 * GeV # Possible cuts for future tunning
            # F.P > None, # 30 * GeV # Possible cuts for future tunning
            F.CHI2DOF < 12.0,
            _DZ_CHILD(1) > 4 * mm,
            # F.OWNPVVDZ > None, # 0.25 * mm # Possible cuts for future tunning
            F.OWNPVFDCHI2 > 4.0,
            # F.OWNPVIPCHI2 < None, # 10. # Possible cuts for future tunning
            F.OWNPVDIRA > 0.999,
            F.OWNPVLTIME > 0.1 * ps,
        ),
    )

    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lcs])


@register_line_builder(all_lines)
def lc_to_l0pipipi_dd_line(name="Hlt2Charm_LcpToL0PimPipPip_DD"):
    protons = ParticleFilter(
        make_has_rich_down_protons(),
        F.FILTER(F.require_all(F.PT > 500 * MeV, F.P > 9 * GeV, F.PID_P > -2.0)),
    )

    down_lambda_pions = ParticleFilter(
        make_has_rich_down_pions(),
        F.FILTER(F.require_all(F.PT > 100 * MeV, F.PID_K < 5.0)),
    )

    dd_lambdas = ParticleCombiner(
        [protons, down_lambda_pions],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name="Charm_Hyperons_OLDLambda_DD_{hash}",
        CombinationCut=F.require_all(
            F.MASS < 1150 * MeV,
            F.PT > 500 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        CompositeCut=F.require_all(
            F.MASS < 1140 * MeV,
            F.PT > 600 * MeV,
            F.CHI2DOF < 24.0,
            F.math.in_range(250 * mm, F.END_VZ, 2485 * mm),
        ),
    )

    long_lc_pions = ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 300 * MeV, F.P > 2 * GeV, F.OWNPVIPCHI2 > 4.0, F.PID_K < 5.0
            )
        ),
    )
    lcs = ParticleCombiner(
        [dd_lambdas, long_lc_pions, long_lc_pions, long_lc_pions],
        DecayDescriptor="[Lambda_c+ -> Lambda0 pi- pi+ pi+]cc",
        name="Charm_Hyperons_LcpToL0PimPipPip_DD_{hash}",
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2240 * MeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(2, 4) < 250 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2201 * MeV, F.MASS, 2371 * MeV),
            # F.PT > None, # 1.3 * GeV # Possible cuts for future tunning
            # F.P > None, # 28 * GeV # Possible cuts for future tunning
            F.SUM(F.PT) > 1.8 * GeV,
            F.MAXDOCACUT(2 * mm),
            F.MAXDOCACHI2CUT(12.0),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2211 * MeV, F.MASS, 2361 * MeV),
            # F.PT > None, # 1.4 * GeV # Possible cuts for future tunning
            # F.P > None, # 30 * GeV # Possible cuts for future tunning
            F.CHI2DOF < 12,
            # F.OWNPVVDZ > None, # 0.25 * mm # Possible cuts for future tunning
            F.OWNPVFDCHI2 > 4.0,
            # F.OWNPVIPCHI2 < None, # 10. # Possible cuts for future tunning
            F.OWNPVDIRA > 0.996,
            F.OWNPVLTIME > 0.1 * ps,
        ),
    )

    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, lcs])


# Lc -> L K- K+ pi+. Expected rate 480 Hz
# Xic+ -> L K- K+ pi+ and partially reconstructed Xic+ -> Sigma0 K- K+ pi+. Expected rate 30 Hz
@register_line_builder(all_lines)
def lcxic_to_lkkpi_ll_line(name="Hlt2Charm_LcpXicpToL0KmKpPip_LL"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    long_lc_kaons = _make_loose_kaons_for_charm()
    lcs = ParticleCombiner(
        [ll_lambdas, long_lc_kaons, long_lc_kaons, _make_loose_pions_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Lambda0 K- K+ pi+]cc",
        name="Charm_Hyperons_LcpXicpToL0KmKpPip_LL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        Combination12Cut=F.require_all(
            F.MASS < 1975 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.MASS < 2590 * MeV,
            F.PT > 2.3 * GeV,
            F.SUM(F.PT) > 2.8 * GeV,
            F.require_any(
                F.CHILD(2, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(3, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(4, F.OWNPVIPCHI2) > 12.0,
            ),
            F.require_any(
                F.CHILD(2, F.PT) > 600, F.CHILD(3, F.PT) > 600, F.CHILD(4, F.PT) > 600
            ),
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.MASS < 2570 * MeV,
            F.PT > 2.4 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.9997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lcs])


@register_line_builder(all_lines)
def lcxic_to_lkkpi_dd_line(name="Hlt2Charm_LcpXicpToL0KmKpPip_DD"):
    dd_lambdas = _make_dd_lambdas()
    long_lc_kaons = _make_loose_kaons_for_charm()
    lcs = ParticleCombiner(
        [dd_lambdas, long_lc_kaons, long_lc_kaons, _make_loose_pions_for_charm()],
        DecayDescriptor="[Lambda_c+ -> Lambda0 K- K+ pi+]cc",
        name="Charm_Hyperons_LcpXicpToL0KmKpPip_DD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        Combination12Cut=F.require_all(
            F.MASS < 1975 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.MASS < 2590 * MeV,
            F.PT > 2.5 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.require_any(
                F.CHILD(2, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(3, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(4, F.OWNPVIPCHI2) > 12.0,
            ),
            F.require_any(
                F.CHILD(2, F.PT) > 600, F.CHILD(3, F.PT) > 600, F.CHILD(4, F.PT) > 600
            ),
            F.DOCA(1, 4) < 2 * mm,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.MASS < 2570 * MeV,
            F.PT > 2.6 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.9997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, lcs])


# Lc -> Xi KS pi pi. Expected rate 40 Hz
@register_line_builder(all_lines)
def lc_to_ximkspipi_ll_line(name="Hlt2Charm_LcpToXimKsPipPip_LLLLL"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    ll_kshorts = _make_ll_kshorts()
    long_lc_pions = _make_loose_pions_for_charm()
    lcs = ParticleCombiner(
        [lll_xis, ll_kshorts, long_lc_pions, long_lc_pions],
        DecayDescriptor="[Lambda_c+ -> Xi- KS0 pi+ pi+]cc",
        name="Charm_Hyperons_LcpToXimKsPipPip_LLLLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2125 * MeV,
            F.MAXDOCACUT(250 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2265 * MeV,
            F.DOCA(1, 3) < 150 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2165 * MeV, F.MASS, 2405 * MeV),
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.DOCA(1, 4) < 250 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2185 * MeV, F.MASS, 2385 * MeV),
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 2 * mm,
            _DZ_CHILD(2) > 2 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, ll_kshorts, lcs]
    )


@register_line_builder(all_lines)
def lc_to_ximkspipi_ld_line(name="Hlt2Charm_LcpToXimKsPipPip_LLLDD"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    dd_kshorts = _make_dd_kshorts()
    long_lc_pions = _make_loose_pions_for_charm()
    lcs = ParticleCombiner(
        [lll_xis, dd_kshorts, long_lc_pions, long_lc_pions],
        DecayDescriptor="[Lambda_c+ -> Xi- KS0 pi+ pi+]cc",
        name="Charm_Hyperons_LcpToXimKsPipPip_LLLDD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2125 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2265 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 2 * mm,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2165 * MeV, F.MASS, 2405 * MeV),
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 2 * mm,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2185 * MeV, F.MASS, 2385 * MeV),
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, dd_kshorts, lcs]
    )


@register_line_builder(all_lines)
def lc_to_ximkspipi_dl_line(name="Hlt2Charm_LcpToXimKsPipPip_DDLLL"):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    ll_kshorts = _make_ll_kshorts()
    long_lc_pions = _make_loose_pions_for_charm()
    lcs = ParticleCombiner(
        [ddl_xis, ll_kshorts, long_lc_pions, long_lc_pions],
        DecayDescriptor="[Lambda_c+ -> Xi- KS0 pi+ pi+]cc",
        name="Charm_Hyperons_LcpToXimKsPipPip_DDLLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2125 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2265 * MeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2165 * MeV, F.MASS, 2405 * MeV),
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.DOCA(1, 4) < 2 * mm,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2185 * MeV, F.MASS, 2385 * MeV),
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 2 * mm,
            _DZ_CHILD(2) > 2 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, ll_kshorts, lcs]
    )


@register_line_builder(all_lines)
def lc_to_ximkspipi_dd_line(name="Hlt2Charm_LcpToXimKsPipPip_DDLDD"):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    dd_kshorts = _make_dd_kshorts()
    long_lc_pions = _make_loose_pions_for_charm()
    lcs = ParticleCombiner(
        [ddl_xis, dd_kshorts, long_lc_pions, long_lc_pions],
        DecayDescriptor="[Lambda_c+ -> Xi- KS0 pi+ pi+]cc",
        name="Charm_Hyperons_LcpToXimKsPipPip_DDLDD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2125 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2265 * MeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 2 * mm,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2165 * MeV, F.MASS, 2405 * MeV),
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.DOCA(1, 4) < 2 * mm,
            F.DOCA(2, 4) < 2 * mm,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2185 * MeV, F.MASS, 2385 * MeV),
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 60 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.9995,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, dd_kshorts, lcs]
    )


# Xic+ -> L K- pi+ pi+ and partially reconstructed Xic+ -> Sigma0 K- pi+ pi+. Expected rate 1.5 kHz
@register_line_builder(all_lines)
def xicp_to_lkpipi_ll_line(name="Hlt2Charm_XicpToL0KmPipPip_LL"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    long_xicp_pions = _make_loose_pions_for_charm()
    xicps = ParticleCombiner(
        [ll_lambdas, _make_loose_kaons_for_charm(), long_xicp_pions, long_xicp_pions],
        DecayDescriptor="[Xi_c+ -> Lambda0 K- pi+ pi+]cc",
        name="Charm_Hyperons_XicpToL0KmPipPip_LL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2310 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2270 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.3 * GeV,
            F.SUM(F.PT) > 2.8 * GeV,
            F.require_any(
                F.CHILD(2, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(3, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(4, F.OWNPVIPCHI2) > 12.0,
            ),
            F.require_any(
                F.CHILD(2, F.PT) > 600, F.CHILD(3, F.PT) > 600, F.CHILD(4, F.PT) > 600
            ),
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2290 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2.4 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.9997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, xicps])


@register_line_builder(all_lines)
def xicp_to_lkpipi_dd_line(name="Hlt2Charm_XicpToL0KmPipPip_DD"):
    dd_lambdas = _make_dd_lambdas()
    long_xicp_pions = _make_loose_pions_for_charm()
    xicp_DD = ParticleCombiner(
        [dd_lambdas, _make_loose_kaons_for_charm(), long_xicp_pions, long_xicp_pions],
        DecayDescriptor="[Xi_c+ -> Lambda0 K- pi+ pi+]cc",
        name="Charm_Hyperons_XicpToL0KmPipPip_DD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2310 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2270 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.5 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.require_any(
                F.CHILD(2, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(3, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(4, F.OWNPVIPCHI2) > 12.0,
            ),
            F.require_any(
                F.CHILD(2, F.PT) > 600, F.CHILD(3, F.PT) > 600, F.CHILD(4, F.PT) > 600
            ),
            F.DOCA(1, 4) < 2 * mm,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2290 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2.6 * GeV,
            F.CHI2DOF < 6.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.9997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, xicp_DD])


# Xic0 -> L KS pi pi and partially reconstructed Xic0 -> Sigma0 KS pi pi. Expected rate 300 Hz
@register_line_builder(all_lines)
def xicz_to_lkspipi_ll_line(name="Hlt2Charm_Xic0ToL0KsPimPip_LLLL"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    ll_kshorts = _make_ll_kshorts()
    long_xicp_pions = _make_loose_pions_for_charm()
    xicps = ParticleCombiner(
        [ll_lambdas, ll_kshorts, long_xicp_pions, long_xicp_pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 KS0 pi- pi+]cc",
        name="Charm_Hyperons_Xic0ToL0KsPimPip_LLLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2310 * MeV,
            F.MAXDOCACUT(250 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 250 * um,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2270 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.8 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.require_any(
                F.CHILD(3, F.OWNPVIPCHI2) > 9.0, F.CHILD(4, F.OWNPVIPCHI2) > 9.0
            ),
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 250 * um,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2290 * MeV, F.MASS, 2570 * MeV),
            F.PT > 3 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 4 * mm,
            _DZ_CHILD(2) > 2 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.999,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, ll_kshorts, xicps]
    )


@register_line_builder(all_lines)
def xicz_to_lkspipi_dl_line(name="Hlt2Charm_Xic0ToL0KsPimPip_DDLL"):
    dd_lambdas = _make_dd_lambdas()
    ll_kshorts = _make_ll_kshorts()
    long_xicp_pions = _make_loose_pions_for_charm()
    xicps = ParticleCombiner(
        [dd_lambdas, ll_kshorts, long_xicp_pions, long_xicp_pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 KS0 pi- pi+]cc",
        name="Charm_Hyperons_Xic0ToL0KsPimPip_DDLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2310 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 250 * um,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2270 * MeV, F.MASS, 2590 * MeV),
            F.PT > 3 * GeV,
            F.SUM(F.PT) > 3.4 * GeV,
            F.require_any(
                F.CHILD(3, F.OWNPVIPCHI2) > 9.0, F.CHILD(4, F.OWNPVIPCHI2) > 9.0
            ),
            F.DOCA(1, 4) < 2 * mm,
            F.DOCA(2, 4) < 250 * um,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2290 * MeV, F.MASS, 2570 * MeV),
            F.PT > 3.2 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(2) > 2 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.999,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_lambdas, ll_kshorts, xicps]
    )


@register_line_builder(all_lines)
def xicz_to_lkspipi_ld_line(name="Hlt2Charm_Xic0ToL0KsPimPip_LLDD"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    dd_kshorts = _make_dd_kshorts()
    long_xicp_pions = _make_loose_pions_for_charm()
    xicps = ParticleCombiner(
        [ll_lambdas, dd_kshorts, long_xicp_pions, long_xicp_pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 KS0 pi- pi+]cc",
        name="Charm_Hyperons_Xic0ToL0KsPimPip_LLDD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2310 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 2 * mm,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2270 * MeV, F.MASS, 2590 * MeV),
            F.PT > 3 * GeV,
            F.SUM(F.PT) > 3.4 * GeV,
            F.require_any(
                F.CHILD(3, F.OWNPVIPCHI2) > 9.0, F.CHILD(4, F.OWNPVIPCHI2) > 9.0
            ),
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 2 * mm,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2290 * MeV, F.MASS, 2570 * MeV),
            F.PT > 3.2 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 0.5 * mm,
            F.OWNPVVDRHO > 80 * um,
            F.OWNPVDLS > 3.6,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.999,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [dd_kshorts, ll_lambdas, xicps]
    )


# Xic0 -> Xi- pi- pi+ pi+.  Expected rate 1.3 kHz
@register_line_builder(all_lines)
def xicz_to_xim3pi_lll_line(name="Hlt2Charm_Xic0ToXimPimPipPip_LLL"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    long_xicz_pions = _make_loose_pions_for_charm()
    xic0_LLL = ParticleCombiner(
        [lll_xis, long_xicz_pions, long_xicz_pions, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Xi- pi- pi+ pi+]cc",
        name="Charm_Hyperons_Xic0ToXimPimPipPip_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        Combination12Cut=F.require_all(
            F.MASS < 2310 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.require_any(
                F.CHILD(2, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(3, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(4, F.OWNPVIPCHI2) > 12.0,
            ),
            F.require_any(
                F.CHILD(2, F.PT) > 600, F.CHILD(3, F.PT) > 600, F.CHILD(4, F.PT) > 600
            ),
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 4.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 4.2,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.9997,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, xic0_LLL]
    )


@register_line_builder(all_lines)
def xicz_to_xim3pi_ddl_line(name="Hlt2Charm_Xic0ToXimPimPipPip_DDL"):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    long_xicz_pions = _make_loose_pions_for_charm()
    xic0s = ParticleCombiner(
        [ddl_xis, long_xicz_pions, long_xicz_pions, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Xi- pi- pi+ pi+]cc",
        name="Charm_Hyperons_Xic0ToXimPimPipPip_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        Combination12Cut=F.require_all(
            F.MASS < 2310 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2350 * MeV, F.MASS, 2590 * MeV),
            F.PT > 2.9 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.DOCA(1, 4) < 2 * mm,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2370 * MeV, F.MASS, 2570 * MeV),
            F.PT > 3 * GeV,
            F.CHI2DOF < 4.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 4.2,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.9997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, xic0s])


# Xic0 -> Omega- K+ pi- pi+. Expected rate 35 Hz
# Oc -> Omega- K+ pi- pi+.  Expected rate 7 Hz
@register_line_builder(all_lines)
def xicz_to_omkppippim_lll_line(name="Hlt2Charm_Xic0Oc0ToOmKpPimPip_LLL"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_oms = _make_lll_omegas(ll_lambdas, _make_long_kaons_for_omega())
    long_xicz_kaons = _make_loose_kaons_for_charm()
    long_xicz_pions = _make_loose_pions_for_charm()
    xic0_LLL = ParticleCombiner(
        [lll_oms, long_xicz_kaons, long_xicz_pions, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Omega- K+ pi- pi+]cc",
        name="Charm_Hyperons_Xic0Oc0ToOmKpPimPip_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Omega-"]),
        Combination12Cut=F.require_all(
            F.MASS < 2515 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.MASS < 2815 * MeV,
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.require_any(
                F.CHILD(2, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(3, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(4, F.OWNPVIPCHI2) > 12.0,
            ),
            F.require_any(
                F.CHILD(2, F.PT) > 600, F.CHILD(3, F.PT) > 600, F.CHILD(4, F.PT) > 600
            ),
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.MASS < 2795 * MeV,
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 4.0,
            _DZ_CHILD(1) > 1 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 4.2,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.9997,
        ),
    )
    return Hlt2Line(
        name=name, algs=charm_prefilters() + [ll_lambdas, lll_oms, xic0_LLL]
    )


@register_line_builder(all_lines)
def xicz_to_omkppippim_ddl_line(name="Hlt2Charm_Xic0Oc0ToOmKpPimPip_DDL"):
    dd_lambdas = _make_dd_lambdas()
    ddl_oms = _make_ddl_omegas(dd_lambdas, _make_long_kaons_for_omega())
    long_xicz_kaons = _make_loose_kaons_for_charm()
    long_xicz_pions = _make_loose_pions_for_charm()
    xic0s = ParticleCombiner(
        [ddl_oms, long_xicz_kaons, long_xicz_pions, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Omega- K+ pi- pi+]cc",
        name="Charm_Hyperons_Xic0Oc0ToOmKpPimPip_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Omega-"]),
        Combination12Cut=F.require_all(
            F.MASS < 2515 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.MASS < 2815 * MeV,
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.require_any(
                F.CHILD(2, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(3, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(4, F.OWNPVIPCHI2) > 12.0,
            ),
            F.require_any(
                F.CHILD(2, F.PT) > 600, F.CHILD(3, F.PT) > 600, F.CHILD(4, F.PT) > 600
            ),
            F.DOCA(1, 4) < 2 * mm,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.MASS < 2795 * MeV,
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 4.0,
            _DZ_CHILD(1) > 1 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 4.2,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.9997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, ddl_oms, xic0s])


# Oc -> L KS K- pi+ and partially reconstructed Oc -> Sigma0 KS K- pi+. Expected rate 400 Hz
@register_line_builder(all_lines)
def oc_to_lkskpi_ll_line(name="Hlt2Charm_Oc0ToL0KsKmPip_LLLL"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    ll_kshorts = _make_ll_kshorts()
    long_oc_kaons = _make_loose_kaons_for_charm()
    long_oc_pions = _make_std_pions_for_charm()
    oc0s = ParticleCombiner(
        [ll_lambdas, ll_kshorts, long_oc_kaons, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Lambda0 KS0 K- pi+]cc",
        name="Charm_Hyperons_Oc0ToL0KsKmPip_LLLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2182 * MeV,
            F.MAXDOCACUT(250 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 250 * um,
        ),
        CombinationCut=F.require_all(
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 250 * um,
            F.DOCA(3, 4) < 250 * um,
            F.math.in_range(2470 * MeV, F.MASS, 2815 * MeV),
            F.PT > 2.3 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2490 * MeV, F.MASS, 2795 * MeV),
            F.PT > 2.4 * GeV,
            F.CHI2DOF < 5.0,
            _DZ_CHILD(1) > 4 * mm,
            _DZ_CHILD(2) > 2 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 4.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.999,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, ll_kshorts, oc0s])


@register_line_builder(all_lines)
def oc_to_lkskpi_dl_line(name="Hlt2Charm_Oc0ToL0KsKmPip_DDLL"):
    dd_lambdas = _make_dd_lambdas()
    ll_kshorts = _make_ll_kshorts()
    long_oc_kaons = _make_loose_kaons_for_charm()
    long_oc_pions = _make_std_pions_for_charm()
    oc0s = ParticleCombiner(
        [dd_lambdas, ll_kshorts, long_oc_kaons, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Lambda0 KS0 K- pi+]cc",
        name="Charm_Hyperons_Oc0ToL0KsKmPip_DDLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2182 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 250 * um,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2470 * MeV, F.MASS, 2815 * MeV),
            F.PT > 3.2 * GeV,
            F.SUM(F.PT) > 3.6 * GeV,
            F.DOCA(1, 4) < 2 * mm,
            F.DOCA(2, 4) < 250 * um,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2490 * MeV, F.MASS, 2795 * MeV),
            F.PT > 3.4 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(2) > 2 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 4.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.999,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, ll_kshorts, oc0s])


@register_line_builder(all_lines)
def oc_to_lkskpi_ld_line(name="Hlt2Charm_Oc0ToL0KsKmPip_LLDD"):
    ll_lambdas = _make_ll_lambdas_for_charm()
    dd_kshorts = _make_dd_kshorts()
    long_oc_kaons = _make_loose_kaons_for_charm()
    long_oc_pions = _make_std_pions_for_charm()
    oc0s = ParticleCombiner(
        [ll_lambdas, dd_kshorts, long_oc_kaons, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Lambda0 KS0 K- pi+]cc",
        name="Charm_Hyperons_Oc0ToL0KsKmPip_LLDD_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Lambda0", "KS0"]),
        Combination12Cut=F.require_all(
            F.MASS < 2182 * MeV,
            F.MAXDOCACUT(3 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 2 * mm,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2470 * MeV, F.MASS, 2815 * MeV),
            F.PT > 3.2 * GeV,
            F.SUM(F.PT) > 3.6 * GeV,
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 2 * mm,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2490 * MeV, F.MASS, 2795 * MeV),
            F.PT > 3.4 * GeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 4 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 4.0,
            F.OWNPVIPCHI2 < 12.0,
            F.OWNPVDIRA > 0.999,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_kshorts, ll_lambdas, oc0s])


# Omegac0 -> Xi- K- pi+ pi+.  Expected rate 64 Hz
@register_line_builder(all_lines)
def oc_to_ximkpipi_lll_line(name="Hlt2Charm_Oc0ToXimKmPipPip_LLL"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_xis = _make_lll_xis(ll_lambdas, _make_long_pions_for_xi())
    long_oc_pions = _make_loose_pions_for_charm()
    oc0s = ParticleCombiner(
        [lll_xis, _make_loose_kaons_for_charm(), long_oc_pions, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Xi- K- pi+ pi+]cc",
        name="Charm_Hyperons_Oc0ToXimKmPipPip_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        Combination12Cut=F.require_all(
            F.MASS < 2535 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.require_any(
                F.CHILD(2, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(3, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(4, F.OWNPVIPCHI2) > 12.0,
            ),
            F.require_any(
                F.CHILD(2, F.PT) > 600, F.CHILD(3, F.PT) > 600, F.CHILD(4, F.PT) > 600
            ),
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 4.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 4.0,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.9997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lll_xis, oc0s])


@register_line_builder(all_lines)
def oc_to_ximkpipi_ddl_line(name="Hlt2Charm_Oc0ToXimKmPipPip_DDL"):
    dd_lambdas = _make_dd_lambdas()
    ddl_xis = _make_ddl_xis(dd_lambdas, _make_long_pions_for_xi())
    long_oc_pions = _make_loose_pions_for_charm()
    oc0s = ParticleCombiner(
        [ddl_xis, _make_loose_kaons_for_charm(), long_oc_pions, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Xi- K- pi+ pi+]cc",
        name="Charm_Hyperons_Oc0ToXimKmPipPip_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Xi-"]),
        Combination12Cut=F.require_all(
            F.MASS < 2535 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 2.9 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.require_any(
                F.CHILD(2, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(3, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(4, F.OWNPVIPCHI2) > 12.0,
            ),
            F.require_any(
                F.CHILD(2, F.PT) > 600, F.CHILD(3, F.PT) > 600, F.CHILD(4, F.PT) > 600
            ),
            F.DOCA(1, 4) < 2 * mm,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 3 * GeV,
            F.CHI2DOF < 4.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 4.0,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.9997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, ddl_xis, oc0s])


# Oc -> Omega- pi- pi+ pi+.  Expected rate 42 Hz
@register_line_builder(all_lines)
def oc_to_om3pi_lll_line(name="Hlt2Charm_Oc0ToOmPimPipPip_LLL"):
    ll_lambdas = _make_ll_lambdas_for_hyperon()
    lll_oms = _make_lll_omegas(ll_lambdas, _make_long_kaons_for_omega())
    long_oc_pions = _make_loose_pions_for_charm()
    oc0s = ParticleCombiner(
        [lll_oms, long_oc_pions, long_oc_pions, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Omega- pi- pi+ pi+]cc",
        name="Charm_Hyperons_Oc0ToOmPimPipPip_LLL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Omega-"]),
        Combination12Cut=F.require_all(
            F.MASS < 2515 * MeV,
            F.MAXDOCACUT(150 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.DOCA(1, 3) < 200 * um,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 2.7 * GeV,
            F.SUM(F.PT) > 3 * GeV,
            F.require_any(
                F.CHILD(2, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(3, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(4, F.OWNPVIPCHI2) > 12.0,
            ),
            F.require_any(
                F.CHILD(2, F.PT) > 600, F.CHILD(3, F.PT) > 600, F.CHILD(4, F.PT) > 600
            ),
            F.DOCA(1, 4) < 200 * um,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 2.8 * GeV,
            F.CHI2DOF < 4.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 4.2,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.9997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [ll_lambdas, lll_oms, oc0s])


@register_line_builder(all_lines)
def oc_to_om3pi_ddl_line(name="Hlt2Charm_Oc0ToOmPimPipPip_DDL"):
    dd_lambdas = _make_dd_lambdas()
    ddl_oms = _make_ddl_omegas(dd_lambdas, _make_long_kaons_for_omega())
    long_oc_pions = _make_std_pions_for_charm()
    oc0s = ParticleCombiner(
        [ddl_oms, long_oc_pions, long_oc_pions, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Omega- pi- pi+ pi+]cc",
        name="Charm_Hyperons_Oc0ToOmPimPipPip_DDL_{hash}",
        ParticleCombiner=ParticleVertexFitter(MassConstraints=["Omega-"]),
        Combination12Cut=F.require_all(
            F.MASS < 2515 * MeV,
            F.MAXDOCACUT(2 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.DOCA(1, 3) < 2 * mm,
            F.DOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 2.9 * GeV,
            F.SUM(F.PT) > 3.2 * GeV,
            F.require_any(
                F.CHILD(2, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(3, F.OWNPVIPCHI2) > 12.0,
                F.CHILD(4, F.OWNPVIPCHI2) > 12.0,
            ),
            F.require_any(
                F.CHILD(2, F.PT) > 600, F.CHILD(3, F.PT) > 600, F.CHILD(4, F.PT) > 600
            ),
            F.DOCA(1, 4) < 1 * mm,
            F.DOCA(2, 4) < 200 * um,
            F.DOCA(3, 4) < 250 * um,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 2795 * MeV),
            F.PT > 3 * GeV,
            F.CHI2DOF < 4.0,
            _DZ_CHILD(1) > 2 * mm,
            F.OWNPVVDZ > 1 * mm,
            F.OWNPVVDRHO > 0.1 * mm,
            F.OWNPVDLS > 4.2,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVDIRA > 0.9997,
        ),
    )
    return Hlt2Line(name=name, algs=charm_prefilters() + [dd_lambdas, ddl_oms, oc0s])
