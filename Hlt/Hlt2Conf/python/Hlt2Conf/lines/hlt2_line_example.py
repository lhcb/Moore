###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define HLT2 lines for
``Lambda_b0 -> Lambda_c+ pi-``       Hlt2Tutorial_Lb0ToLcpPim_LcpToPpKmPip
``Lambda_b0 -> Lambda_c+ mu- (nu)``  Hlt2Tutorial_Lb0ToLcpMumNu_LcpToPpKmPip
Both use ``Lambda_c+ -> p+ K- pi+``.

With a stack set up, run as
Moore/run gaudirun.py '$HLT2CONFROOT/options/run_hlt2_line_example.py'
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.event_filters import require_pvs
from RecoConf.reconstruction_objects import (
    make_pvs,
    upfront_reconstruction,
)

# For code inside files under Hlt2Conf/python/lines you should reference
# the following modules using relative imports:
#     from RecoConf.standard_particles import (...)
#     from ...algorithms_thor
# We don't use relative imports here since this module is used to build the documentation.
from RecoConf.standard_particles import (
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
    make_has_rich_long_protons,
    make_ismuon_long_muon,
)


# We define the basic building blocks for this module, following
# https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/hlt2_line.html#code-design-guidelines
def _protons_for_charm():
    pvs = make_pvs()
    cut = F.require_all(
        F.PT > 0.5 * GeV,
        F.MINIPCHI2(pvs) > 9.0,
        F.PID_P > 5.0,
    )
    return ParticleFilter(
        make_has_rich_long_protons(),
        F.FILTER(cut),
        # We would usually not give a name to ParticleFilter. This is for development purposes.
        name="Tutorial_protons_for_charm_{hash}",
    )


def _kaons_for_charm():
    pvs = make_pvs()
    cut = F.require_all(
        F.PT > 0.5 * GeV,
        F.MINIPCHI2(pvs) > 9.0,
        F.PID_K > 5.0,
    )
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(cut),
        # We would usually not give a name to ParticleFilter. This is for development purposes.
        name="Tutorial_kaons_for_charm_{hash}",
    )


# For development purposes, we want to study the pT cut.
# Once tuned, it should be moved inside the function body.
def _pions_for_charm_and_beauty(pt_min=0.5 * GeV):
    pvs = make_pvs()
    cut = F.require_all(
        F.PT > pt_min,
        F.MINIPCHI2(pvs) > 9.0,
        # PID_X is a likelihood ratio of X w.r.t. the pion hypothesis. PID_PI is 0 by definition.
        F.PID_K < 5.0,
    )
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(cut),
        # We would usually not give a name to ParticleFilter. This is for development purposes.
        name="Tutorial_pions_for_charm_and_beauty_{hash}",
    )


def _make_lambdacs_for_beauty(protons, kaons, pions, pvs):
    two_body_combination_code = F.require_all(
        F.MAXSDOCACHI2CUT(9.0), F.MAXSDOCACUT(0.1 * mm)
    )
    combination_code = F.require_all(
        in_range(2080 * MeV, F.MASS, 2480 * MeV),  # mass of the combination
        F.PT > 1.4 * GeV,  # pT of the 3-track combination
        F.SUM(F.PT) > 2 * GeV,
        F.MAXSDOCACHI2CUT(9.0),
        F.MAXSDOCACUT(0.1 * mm),
    )
    vertex_code = F.require_all(
        in_range(2100 * MeV, F.MASS, 2460 * MeV),  # mass after the vertex fit
        F.PT > 1.6 * GeV,  # pT after the vertex fit
        F.CHI2DOF < 10.0,
        F.BPVFDCHI2(pvs) > 25.0,
    )
    return ParticleCombiner(
        [protons, kaons, pions],
        DecayDescriptor="[Lambda_c+ -> p+ K- pi+]cc",
        name="Tutorial_Lcp_Combiner_{hash}",
        Combination12Cut=two_body_combination_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def _make_lambdabs(lcps, bachelors, pvs, decay_descriptor):
    combination_code = F.require_all(
        in_range(5 * GeV, F.MASS, 7 * GeV),
        F.SUM(F.PT) > 4 * GeV,
        F.MAXSDOCACUT(0.1 * mm),
    )
    vertex_code = F.require_all(
        F.CHI2DOF < 10.0,
        F.BPVFDCHI2(pvs) > 16.0,
    )
    return ParticleCombiner(
        [lcps, bachelors],
        name="Tutorial_Lb0_Combiner_{hash}_{hash}",
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


all_lines = {}


@register_line_builder(all_lines)
def lb0_to_lcpim_line(name="Hlt2Tutorial_Lb0ToLcpPim_LcpToPpKmPip", prescale=1):
    pvs = make_pvs()
    protons = _protons_for_charm()
    kaons = _kaons_for_charm()
    pions = _pions_for_charm_and_beauty()
    lcs = _make_lambdacs_for_beauty(protons, kaons, pions, pvs)
    lbs = _make_lambdabs(lcs, pions, pvs, "[Lambda_b0 -> Lambda_c+ pi-]cc")

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), lbs],
        # Passing `prescale` to `Hlt2Line` is optional; The default value is 1
        prescale=prescale,
    )


# The order of decorators matters!
@register_line_builder(all_lines)
@configurable
def lb0tolcpmum_line(
    name="Hlt2Tutorial_Lb0ToLcpMumNu_LcpToPpKmPip", pi_pt_min=0.5 * GeV
):
    pvs = make_pvs()
    protons = _protons_for_charm()
    kaons = _kaons_for_charm()
    pions = _pions_for_charm_and_beauty(pi_pt_min)
    lcs = _make_lambdacs_for_beauty(protons, kaons, pions, pvs)
    muon_cut = F.require_all(
        F.PT > 1 * GeV,
        F.MINIPCHI2(pvs) > 9.0,
        F.PID_MU > 0.0,
    )
    muons = ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(muon_cut),
        # We would usually not give a name to ParticleFilter. This is for development purposes.
        name="Tutorial_muons_for_lb0_{hash}",
    )
    lbs = _make_lambdabs(lcs, muons, pvs, "[Lambda_b0 -> Lambda_c+ mu-]cc")

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), muons, lbs],
    )
