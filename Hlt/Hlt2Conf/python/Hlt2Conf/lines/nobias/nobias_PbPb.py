###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from Moore.streams import DETECTORS
from RecoConf.event_filters import require_gec

all_lines = {}


def _pbpb_prefilters():
    return [require_gec(cut=30_000, skipUT=True)]


@register_line_builder(all_lines)
def hlt2_nobias_line(name="Hlt2NoBias_NoBias", prescale=1.0):
    """Line to select unbiased events and save reconstruction objects.

    The prescale takes into account that the HLT1 lumi line also selects
    eb, be and ee.

    """
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code="Hlt1ODIN1kHzLumiDecision",
        persistreco=False,
        raw_banks=DETECTORS,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def hlt2_nobias_gec_line(name="Hlt2NoBias_NoBiasGec", prescale=1.0):
    """Line to select unbiased events and save reconstruction objects.

    The prescale takes into account that the HLT1 lumi line also selects
    eb, be and ee.

    """
    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters(),
        hlt1_filter_code="Hlt1ODIN1kHzLumiDecision",
        persistreco=True,
        raw_banks=DETECTORS,
        prescale=prescale,
    )
