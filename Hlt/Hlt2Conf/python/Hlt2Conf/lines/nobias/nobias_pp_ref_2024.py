###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from Moore.streams import DETECTORS

all_lines = {}


@register_line_builder(all_lines)
def hlt2_nobias_line(name="Hlt2NoBias_NoBias", prescale=0.33333):
    """Line to select unbiased events and save reconstruction objects.

    The prescale takes into account that the HLT1 lumi line also
    selects eb, be and ee. The target rate is 7.5 kHz for bb during
    the 2024 pp reference run, corresponding to an integrated
    luminosity of about 50/nb.

    """
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code="Hlt1ODINLumiDecision",
        persistreco=True,
        raw_banks=DETECTORS,
        prescale=prescale,
    )
