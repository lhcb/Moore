###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from Moore.streams import DETECTORS

all_lines = {}


@register_line_builder(all_lines)
def hlt2_nobias_line(name="Hlt2NoBias_NoBias", prescale=0.266666):
    """Line to select unbiased events and save reconstruction objects.

    The prescale takes into account that the HLT1 lumi line also selects
    eb, be and ee. The target rate is 200 Hz for bb.

    """
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code="Hlt1ODIN1kHzLumiDecision",
        persistreco=True,
        raw_banks=DETECTORS,
        prescale=prescale,
    )
