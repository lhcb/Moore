###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of Bu/Bd/Bs/Lb... meson decays to JpsiX0
Definition of B -> J/psi X0 HLT2/Sprucing lines for B2CC.
"""

import Functors as F
from Functors import require_all
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond
from RecoConf.algorithms_thor import ParticleCombiner

from Hlt2Conf.lines.b_to_charmonia.builders import basic_builder, x0_builder
from Hlt2Conf.lines.charmonium_to_dimuon_detached import make_detached_jpsi

####################################
# generic combiners with neutrals  #
####################################


def make_B2JpsiX0(
    particles,
    descriptor,
    name="B2CC_B2JpsiX0_Combiner_{hash}",
    comb_m_min=4600 * MeV,
    comb_m_max=7000 * MeV,
    vtx_m_min=4600 * MeV,
    vtx_m_max=7000 * MeV,
    pt_min=None,
    lifetime=None,
    ip_max=0.2,
    ipchi2_max=20,
    dira_min=0.9995,
    vtxchi2pdof_max=10,
    delta_mass=None,
):
    """
    A generic B->MuMu(H)(HH)(G)(GG) decay maker.
    """
    combination_code = in_range(comb_m_min, F.MASS, comb_m_max)
    vertex_code = require_all(in_range(vtx_m_min, F.MASS, vtx_m_max))

    if lifetime:
        vertex_code = require_all(vertex_code, F.OWNPVLTIME > lifetime)

    if dira_min:
        vertex_code = require_all(vertex_code, F.OWNPVDIRA > dira_min)

    if ipchi2_max:
        vertex_code = require_all(vertex_code, F.OWNPVIPCHI2 < ipchi2_max)

    if ip_max:
        vertex_code = require_all(vertex_code, F.OWNPVIP < ip_max)

    if vtxchi2pdof_max:
        vertex_code = require_all(vertex_code, F.CHI2DOF < vtxchi2pdof_max)

    if pt_min:
        vertex_code = require_all(vertex_code, F.PT > pt_min)

    if delta_mass:
        # for fast, but close proxy to mass constraints
        vertex_code = require_all(
            vertex_code,
            in_range(
                delta_mass[0], F.MASS - F.CHILD(delta_mass[2], F.MASS), delta_mass[1]
            ),
        )

    return ParticleCombiner(
        name=name,
        Inputs=particles,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def make_B2JpsiKst2KPi0R_line(
    process,
    comb_m_min=4700 * MeV,
    comb_m_max=6100 * MeV,
    vtx_m_min=4800 * MeV,
    vtx_m_max=6000 * MeV,
    pi0pt_min=1000.0 * MeV,
):
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    jpsi = basic_builder.make_selected_jpsi2mumu()

    kstr = x0_builder.make_selected_kst2kpi0r(pi0pt_min=pi0pt_min)

    b2jpsikstr = make_B2JpsiX0(
        particles=[jpsi, kstr],
        descriptor="[B+ -> J/psi(1S) K*(892)+]cc",
        name="B2CC_B2JpsiKst2KPi0R_Combiner_{hash}",
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
    )

    return [jpsi, kstr, b2jpsikstr]


def make_B2Chic2JpsiGK_line(
    process,
    comb_m_min=4700 * MeV,
    comb_m_max=6100 * MeV,
    vtx_m_min=4800 * MeV,
    vtx_m_max=6000 * MeV,
    gpt_min=500.0 * MeV,
    pid_k=0,
    pt_k=500.0 * MeV,
):
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    chic = x0_builder.make_selected_chic2jpsig(gpt_min=gpt_min)

    kaon = basic_builder.make_kaons(pid=pid_k, pt=pt_k)

    b2chick = make_B2JpsiX0(
        particles=[chic, kaon],
        descriptor="[B+ -> chi_c1(1P) K+]cc",
        name="B2CC_B2Chic2JpsiGK_Combiner_{hash}",
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
    )

    return [chic, kaon, b2chick]


####################################
# B -> Jpsi[MuMu] eta[Gamma] decays #
####################################


def make_B2JpsiEtaGG_line(
    process,
    comb_m_min=4700 * MeV,
    comb_m_max=6100 * MeV,
    vtx_m_min=4800 * MeV,
    vtx_m_max=6000 * MeV,
):
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    jpsi = basic_builder.make_selected_jpsi2mumu()

    eta = x0_builder.make_resolved_etas()

    b2jpsieta = make_B2JpsiX0(
        particles=[jpsi, eta],
        descriptor="B0 -> J/psi(1S) eta",
        name="B2CC_B2JpsiEtaGG_Combiner_{hash}",
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
    )

    return [jpsi, eta, b2jpsieta]


####################################
# B -> Jpsi[MuMu] pi0[GammaGamma] decays #
####################################


def make_B2JpsiPi0GG_line(
    process,
    comb_m_min=4700 * MeV,
    comb_m_max=6500 * MeV,
    vtx_m_min=4800 * MeV,
    vtx_m_max=6400 * MeV,
):
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    jpsi = basic_builder.make_selected_jpsi2mumu()
    pi0 = x0_builder.make_resolved_pizeros()

    b2jpsipi0 = make_B2JpsiX0(
        particles=[jpsi, pi0],
        descriptor="B0 -> J/psi(1S) pi0",
        name="B2CC_B2JpsiPi0GG_Combiner_{hash}",
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
    )

    return [jpsi, pi0, b2jpsipi0]


#########################################################
# Lambda_b0 -> Jpsi[MuMu] Sigma0 [Lambda0 Gamma] decays #
#########################################################


def make_Lb0ToJpsiSigma0_LL_line(process):
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    jpsi = basic_builder.make_selected_jpsi2mumu()

    sigma0 = x0_builder.make_sigma0_ll()

    line_alg = make_B2JpsiX0(
        name="B2CC_Lb0ToJpsiSigma0_LL_Combiner_{hash}",
        particles=[jpsi, sigma0],
        descriptor="Lambda_b0 -> J/psi(1S) Sigma0",
        comb_m_min=5089 * MeV,
        comb_m_max=6130 * MeV,
        vtx_m_min=5119 * MeV,
        vtx_m_max=6105 * MeV,
        pt_min=1500 * MeV,
        dira_min=0.995,
        lifetime=0.4 * picosecond,
    )

    return [sigma0, jpsi, line_alg]


def make_Lb0ToJpsiSigma0_DD_line(process):
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    jpsi = basic_builder.make_selected_jpsi2mumu()

    sigma0 = x0_builder.make_sigma0_dd()

    line_alg = make_B2JpsiX0(
        name="B2CC_Lb0ToJpsiSigma0_DD_Combiner_{hash}",
        particles=[jpsi, sigma0],
        descriptor="Lambda_b0 -> J/psi(1S) Sigma0",
        comb_m_min=5079 * MeV,
        comb_m_max=6130 * MeV,
        vtx_m_min=5109 * MeV,
        vtx_m_max=6105 * MeV,
        pt_min=1500 * MeV,
        dira_min=0.995,
        lifetime=0.4 * picosecond,
    )

    return [sigma0, jpsi, line_alg]


##################################
# Bu -> Jpsi[MuMu] Eta Pi+ decays #
##################################


def make_BuToJpsimumuEtaPiplus_line(
    process,
    comb_m_min=4800 * MeV,
    comb_m_max=6000 * MeV,
    vtx_m_min=4800 * MeV,
    vtx_m_max=6000 * MeV,
    delta_mass=(
        4250 * MeV,
        5450 * MeV,
        2,
    ),  # 2 here for cut on the second child i.e Eta.
    lifetime=0.2 * picosecond,
    dira_min=0.9999,
    ipchi2_max=10,
    ip_max=0.1,
    vtxchi2pdof_max=5,
):
    assert process in ["hlt2", "spruce"], (
        "Line must be defined as Hlt2 or Sprucing line!"
    )

    jpsi = make_detached_jpsi()
    eta = eta = x0_builder.make_resolved_etas(mass_window=105 * MeV)
    pip = basic_builder.make_pions(pid=1.0, pt=300 * MeV, p=1.5 * GeV, mipchi2_min=11)

    line_alg = make_B2JpsiX0(
        name="B2CC_BuToJpsimumuEtaPiplus_Combiner_{hash}",
        particles=[jpsi, eta, pip],
        descriptor="[B+ -> J/psi(1S) eta pi+]cc",
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
        delta_mass=delta_mass,
        lifetime=lifetime,
        dira_min=dira_min,
        ipchi2_max=ipchi2_max,
        ip_max=ip_max,
        vtxchi2pdof_max=vtxchi2pdof_max,
    )
    return [jpsi, eta, line_alg]
