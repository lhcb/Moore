###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Booking of B2CC sprucing lines, notice PROCESS = 'spruce'

Output:
updated dictionary of sprucing_lines

"""

from Moore.config import SpruceLine, register_line_builder
from PyConf import configurable

from Hlt2Conf.lines.b_to_charmonia import b_to_jpsix, b_to_jpsix0

from . import b2cc_isolation as isolation
from .prefilters import b2cc_prefilters

PROCESS = "spruce"
sprucing_lines = {}


@register_line_builder(sprucing_lines)
@configurable
def BsToJpsiPhi_JpsiToMuMu_sprucing_line(
    name="SpruceB2CC_BsToJpsiPhi_Detached", prescale=1
):
    """
    Bs0 --> Jpsi(-> mu+ mu-) phi spruce line
    """
    line_alg = b_to_jpsix.make_BsToJpsiPhi_detached_line(process=PROCESS)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        pv_tracks=True,
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


@register_line_builder(sprucing_lines)
@configurable
def BsToJpsiPhi_extramuon_sprucing_line(
    name="SpruceB2CC_BsToJpsiPhi_ExtraMuon", prescale=1
):
    line_alg = b_to_jpsix.make_BsToJpsiPhi_extramuonline(process=PROCESS)
    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        pv_tracks=True,
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
    )


@register_line_builder(sprucing_lines)
@configurable
def BsToJpsiKstarWide_line(name="SpruceB2CC_BsToJpsiKstarWide", prescale=1):
    """
    Bs0 --> Jpsi(-> mu+ mu-) Kst spruce line
    """
    line_alg = b_to_jpsix.make_BsToJpsiKstar_line(process=PROCESS)

    return SpruceLine(name=name, prescale=prescale, algs=b2cc_prefilters() + line_alg)


@register_line_builder(sprucing_lines)
@configurable
def Lb0ToJpsiSigma0_LL_line(name="SpruceB2CC_Lb0ToJpsiSigma0_LL", prescale=1):
    """
    Lambda_b0 -> J/psi(1S) (-> mu+ mu-) Sigma0 (-> Lambda0 gamma) LL spruce line
    """
    line_alg = b_to_jpsix0.make_Lb0ToJpsiSigma0_LL_line(process=PROCESS)
    iso_parts = isolation.make_iso_particles(
        line_alg, name="Lb", DownstreamTrackIso=False, coneangle=0.7
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        pv_tracks=True,
        extra_outputs=iso_parts,
    )


@register_line_builder(sprucing_lines)
@configurable
def Lb0ToJpsiSigma0_DD_line(name="SpruceB2CC_Lb0ToJpsiSigma0_DD", prescale=1):
    """
    Lambda_b0 -> J/psi(1S) (-> mu+ mu-) Sigma0 (-> Lambda0 gamma) DD spruce line
    """
    line_alg = b_to_jpsix0.make_Lb0ToJpsiSigma0_DD_line(process=PROCESS)
    iso_parts = isolation.make_iso_particles(
        line_alg, name="Lb", DownstreamTrackIso=False, coneangle=0.7
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        pv_tracks=True,
        extra_outputs=iso_parts,
    )


@register_line_builder(sprucing_lines)
@configurable
def BuToJpsiEtaPiplus_JpsiToMuMu_sprucing_line(
    name="SpruceB2CC_BuToJpsiEtaPiplus_Detached", prescale=1
):
    line_alg = b_to_jpsix0.make_BuToJpsimumuEtaPiplus_line(process=PROCESS)

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=b2cc_prefilters() + line_alg,
        tagging_particles=True,
        pv_tracks=True,
    )
