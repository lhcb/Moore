###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of B2CC basic objects: pions, kaons, ..."""

import Functors as F
from Functors import require_all
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, ps
from PyConf import configurable
from RecoConf.algorithms_thor import (
    ParticleCombiner,
    ParticleContainersMerger,
    ParticleFilter,
)
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    _make_dielectron_with_brem,
    make_down_pions_for_V0,
    make_down_protons_for_V0,
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
    make_has_rich_long_protons,
    make_ismuon_long_muon,
    make_KsDD,
    make_KsLD,
    make_KsLL,
    make_KsUL,
    make_long_and_upstream_electrons_no_brem,
    make_long_electrons_no_brem,
    make_long_pions_for_V0,
    make_up_pions_for_V0,
)

####################################
# Track selections                 #
####################################


@configurable
def make_selected_particles(
    make_particles=make_has_rich_long_pions,
    make_pvs=make_pvs,
    mipchi2_min=0,
    pt_min=250 * MeV,
    p_min=0 * GeV,
    pt_with_brem_min=None,
    max_eta=None,
    pid=None,
    max_trghostprob=None,
    isMuon=None,
    nPVs=None,
):
    code = require_all(F.PT > pt_min, F.P > p_min)
    if mipchi2_min is not None:
        code = code & (F.MINIPCHI2(make_pvs()) > mipchi2_min)
    if pid is not None:
        code &= pid
    if isMuon is not None:
        code &= isMuon
    if pt_with_brem_min is not None:
        code &= F.PT_WITH_BREM > pt_with_brem_min
    if max_eta is not None:
        code &= F.ETA < max_eta
    if nPVs is not None:
        code &= F.SIZE(make_pvs()) < nPVs
    if max_trghostprob is not None:
        code &= F.GHOSTPROB < max_trghostprob

    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_pions(pid=5, pt=200 * MeV, p=1 * GeV, mipchi2_min=0, **decay_arguments):
    """Return pions filtered by thresholds common to B2CC decay product selections."""

    if pid is not None:
        pid = F.PID_K < pid

    return make_selected_particles(
        make_particles=make_has_rich_long_pions,
        pt_min=pt,
        p_min=p,
        pid=pid,
        mipchi2_min=mipchi2_min,
        **decay_arguments,
    )


@configurable
def make_kaons(pid=-3, pt=200 * MeV, p=2 * GeV, **decay_arguments):
    """Return kaons filtered by thresholds common to B2CC decay product selections."""

    return make_selected_particles(
        make_particles=make_has_rich_long_kaons,
        pt_min=pt,
        p_min=p,
        pid=(F.PID_K > pid),
        **decay_arguments,
    )


@configurable
def make_muons(
    pid=None,
    pt=500 * MeV,
    p=0 * GeV,
    mipchi2_min=0,
    max_eta_mu=None,
    max_nPVs=None,
    ismuon=F.ISMUON,
    **decay_arguments,
):
    """Return muons filtered by thresholds common to B2CC decay product selections."""

    if pid is not None:
        pid = F.PID_MU > pid

    return make_selected_particles(
        make_particles=make_ismuon_long_muon,
        mipchi2_min=mipchi2_min,
        pt_min=pt,
        p_min=p,
        pid=pid,
        max_eta=max_eta_mu,
        nPVs=max_nPVs,
        isMuon=ismuon,
        **decay_arguments,
    )


@configurable
def make_electrons(
    particles=make_long_electrons_no_brem,
    pid=0,
    pt_with_brem=250 * MeV,
    mipchi2_min=0,
    max_trghostprob=None,
    **decay_arguments,
):
    """Return electrons filtered by thresholds common to B2CC decay product Jpsi->ee selections."""
    return make_selected_particles(
        make_particles=particles,
        pt_min=0.0,
        pt_with_brem_min=pt_with_brem,
        mipchi2_min=mipchi2_min,
        pid=(F.PID_E > pid),
        max_trghostprob=max_trghostprob,
        **decay_arguments,
    )


@configurable
def make_protons(
    pid_p=0.0, delta_pid_pK=-10.0, p_min=0 * MeV, mipchi2_min=4.0, **decay_arguments
):
    """Return protons filtered by thresholds common to B2CC decay product selections."""
    return make_selected_particles(
        make_particles=make_has_rich_long_protons,
        p_min=p_min,
        mipchi2_min=mipchi2_min,
        pid=((F.PID_P > pid_p) & ((F.PID_P - F.PID_K) > delta_pid_pK)),
        **decay_arguments,
    )


@configurable
def make_down_pions(p_min=2.0 * GeV, pt_min=250 * MeV, mipchi2_min=4.0, pid=None):
    """
    Return downstream hadrons with pion mass hypothesis.
    """
    return make_selected_particles(
        make_particles=make_down_pions_for_V0,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min,
        pid=pid,
    )


@configurable
def make_down_protons(p_min=2.0 * GeV, pt_min=250 * MeV, mipchi2_min=4.0, pid=None):
    """
    Return downstream hadrons with proton mass hypothesis.
    """
    return make_selected_particles(
        make_particles=make_down_protons_for_V0,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2_min=mipchi2_min,
        pid=pid,
    )


####################################
# 2-body decays                    #
####################################


@configurable
def make_twobody(
    particles,
    descriptor,
    comb_m_min,
    comb_m_max,
    vtx_m_min,
    vtx_m_max,
    name="B2CC_TwoBody_Combiner_{hash}",
    pt=500.0 * MeV,
    pt_sum=0.0 * MeV,
    max_docachi2=30.0,
    max_vchi2pdof=10,
):
    """
    Filter phi candiates for B2CC.  Default cuts refered to Bs2JpsiPhi.
    """

    combination_code = require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.MAXSDOCACHI2CUT(max_docachi2),
        F.SUM(F.PT) > pt_sum,
    )

    ## F.CHI2 = VCHI2/CHI2VX = F.CHI2DOF, tiny difference from LOKI functor, see check: https://indico.cern.ch/event/995287/contributions/4633380/attachments/2354933/4018715/WP3%20JieWu%2020211129.pdf
    vertex_code = require_all(
        F.PT > pt, F.CHI2 < max_vchi2pdof, in_range(vtx_m_min, F.MASS, vtx_m_max)
    )

    return ParticleCombiner(
        name=name,
        Inputs=particles,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_selected_phi(
    name="B2CC_Phi2KK_Filter_{hash}",
    descriptor="phi(1020) -> K+ K-",
    comb_m_min=980.0 * MeV,
    comb_m_max=1060.0 * MeV,
    vtx_m_min=980.0 * MeV,
    vtx_m_max=1060.0 * MeV,
    pt=500.0 * MeV,
    max_docachi2=30.0,
    max_vchi2pdof=25.0,
    pid_k=0,
    pt_k=500.0 * MeV,
    p_k=2.0 * GeV,
):
    kaons = make_kaons(pid=pid_k, pt=pt_k, p=p_k)

    return make_twobody(
        particles=[kaons, kaons],
        descriptor=descriptor,
        name=name,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
        pt=pt,
        max_docachi2=max_docachi2,
        max_vchi2pdof=max_vchi2pdof,
    )


@configurable
def make_selected_phi_bs2jpsieephi(
    name="B2CC_Phi2KK_Bs2JpsiEEPhi_{hash}",
    descriptor_rs="phi(1020) -> K- K+",
    descriptor_ws="[phi(1020) -> K- K-]cc",
    comb_m_min=980.0 * MeV,
    comb_m_max=1060.0 * MeV,
    vtx_m_min=980.0 * MeV,
    vtx_m_max=1060.0 * MeV,
    max_vchi2pdof=15,
    pt=1000.0 * MeV,
    max_docachi2=30.0,
    pid_k=1,
    pt_k=400.0 * MeV,
    p_k=4.0 * GeV,
):
    """
    Filter phi candiates for B2CC.  Default cuts refered to Bs2JpsieePhi.
    """
    kaons = make_kaons(pid=pid_k, pt=pt_k, p=p_k)
    phi2kk = make_twobody(
        particles=[kaons, kaons],
        descriptor=descriptor_rs,
        name=name + "_rightsign",
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
        pt=pt,
        max_docachi2=max_docachi2,
        max_vchi2pdof=max_vchi2pdof,
    )
    phi2kk_ws = make_twobody(
        particles=[kaons, kaons],
        descriptor=descriptor_ws,
        name=name + "_wrongsize",
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
        pt=pt,
        max_docachi2=max_docachi2,
        max_vchi2pdof=max_vchi2pdof,
    )

    return ParticleContainersMerger([phi2kk, phi2kk_ws], name=name + "_Merge")


@configurable
def make_selected_ks(input_ks, name="B2CC_Ks_Filter_{hash}", chi2vx=30, bpvvdchi2=9.0):
    """
    Filters Kshort candidates for B2CC. Default cuts correspond to VeryLooseKSLL refered to
    B2OC group
    """
    code = require_all(F.CHI2 < chi2vx)
    if bpvvdchi2 is not None:
        code = code & (F.OWNPVFDCHI2 > bpvvdchi2)
    return ParticleFilter(input_ks, F.FILTER(code))


@configurable
def make_ks_LL(
    make_ks=make_KsLL, pi_pmin=2 * GeV, pi_mipchi2pv=9.0, chi2vx=30, bpvvdchi2=5.0
):
    """
    Builds LL Kshorts, currently corresponding to the Run2
    StdVeryLooseKSLL.
    """
    return make_selected_ks(
        input_ks=make_ks(
            make_selected_particles(
                make_particles=make_long_pions_for_V0,
                pt_min=0 * MeV,
                p_min=pi_pmin,
                mipchi2_min=pi_mipchi2pv,
            )
        ),
        name="B2CC_KsLL_Filter_{hash}",
        chi2vx=chi2vx,
        bpvvdchi2=bpvvdchi2,
    )


@configurable
def make_ks_DD(make_ks=make_KsDD, pi_pmin=2 * GeV, chi2vx=30, bpvvdchi2=5.0):
    """
    Builds DD Kshorts, currently corresponding to the Run2
    StdVeryLooseKSDD.
    """
    return make_selected_ks(
        input_ks=make_ks(
            make_selected_particles(
                make_particles=make_down_pions_for_V0,
                pt_min=0 * MeV,
                p_min=pi_pmin,
                mipchi2_min=None,
            )
        ),
        name="B2CC_KsDD_Filter_{hash}",
        chi2vx=chi2vx,
        bpvvdchi2=bpvvdchi2,
    )


@configurable
def make_ks_LD(
    make_ks=make_KsLD, pi_pmin=2 * GeV, pi_mipchi2pv=4.0, chi2vx=30, bpvvdchi2=5.0
):
    """
    Builds LD Kshorts, currently corresponding to the Run2
    StdLooseKSDD.
    """
    return make_selected_ks(
        input_ks=make_ks(
            pions_down=make_selected_particles(
                make_particles=make_down_pions_for_V0,
                pt_min=0 * MeV,
                p_min=pi_pmin,
                mipchi2_min=None,
            ),
            pions_long=make_selected_particles(
                make_particles=make_long_pions_for_V0,
                pt_min=0 * MeV,
                p_min=pi_pmin,
                mipchi2_min=pi_mipchi2pv,
            ),
        ),
        name="B2CC_KsLD_Filter_{hash}",
        chi2vx=chi2vx,
        bpvvdchi2=bpvvdchi2,
    )


@configurable
def make_ks_UL(
    make_ks=make_KsUL,
    pi_pmin=2 * GeV,
    pi_u_pmin=1 * GeV,
    pi_mipchi2pv=4.0,
    chi2vx=30,
    bpvvdchi2=5.0,
):
    """
    Builds UL Kshorts, currently corresponding to the Run2
    StdLooseKSDD but with loose pion momentum requirement.
    """
    return make_selected_ks(
        input_ks=make_ks(
            pions_long=make_selected_particles(
                make_particles=make_long_pions_for_V0,
                pt_min=0 * MeV,
                p_min=pi_pmin,
                mipchi2_min=pi_mipchi2pv,
            ),
            pions_up=make_selected_particles(
                make_particles=make_up_pions_for_V0,
                pt_min=0 * MeV,
                p_min=pi_u_pmin,
                mipchi2_min=pi_mipchi2pv,
            ),
        ),
        name="B2CC_KsUL_Filter_{hash}",
        chi2vx=chi2vx,
        bpvvdchi2=bpvvdchi2,
    )


@configurable
def make_selected_jpsi2mumu(
    name="B2CC_Jpsi2MuMu_Filter_{hash}",
    descriptor="J/psi(1S) -> mu+ mu-",
    comb_m_min=2700.0 * MeV,
    comb_m_max=3400.0 * MeV,
    vtx_m_min=2950.0 * MeV,
    vtx_m_max=3250.0 * MeV,
    max_vchi2pdof=16.0,
    pt=500.0 * MeV,
    max_docachi2=20.0,
    pt_mu=500.0 * MeV,
    p_mu=0 * MeV,
    max_eta_mu=5.0,
    pid_mu=-5,
    mipchi2_mu=0,
    max_nPVs=None,
):
    """
    Filter jpsi(mumu) candiates for B2CC.  Default cuts refered to Bs2JpsiPhi.
    """
    muons = make_muons(
        pid=pid_mu,
        pt=pt_mu,
        p=p_mu,
        mipchi2_min=mipchi2_mu,
        max_eta_mu=max_eta_mu,
        max_nPVs=max_nPVs,
    )

    return make_twobody(
        particles=[muons, muons],
        descriptor=descriptor,
        name=name,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
        pt=pt,
        max_docachi2=max_docachi2,
        max_vchi2pdof=max_vchi2pdof,
    )


@configurable
def make_selected_jpsi_bd2jpsimumukshort(
    muons, admass=100 * MeV, admass_vtx=100 * MeV, max_vchi2pdof=20, psi2S=False
):
    """
    Filter jpsi candiates for B2CC.  Default cuts refered to Bd2JpsimumuKshort.
    """
    if psi2S:
        pname = "psi(2S)"
        reference_mass = 3686.1 * MeV
    else:
        pname = "J/psi(1S)"
        reference_mass = 3096.9 * MeV

    descriptors = "{pname} -> mu- mu+".format(pname=pname)

    combination_code = require_all(
        in_range(reference_mass - admass, F.MASS, reference_mass + admass)
    )
    vertex_code = require_all(
        in_range(reference_mass - admass_vtx, F.MASS, reference_mass + admass_vtx),
        F.CHI2DOF < max_vchi2pdof,
    )

    return ParticleCombiner(
        [muons, muons],
        DecayDescriptor=descriptors,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def filter_muons_loose(
    particles, pt_min=500 * MeV, pidmu=0, mu_pidk=None, mu_pidp=None
):
    """Returns loosely preselected muons"""
    code = require_all(
        F.ISMUON, F.PID_MU > pidmu, F.PT > pt_min
    )  # , F.MAXSDOCACUT(docachi2_max_mu))
    if mu_pidk is not None:
        code = code & (F.PID_K < mu_pidk)
    if mu_pidp is not None:
        code = code & (F.PID_P < mu_pidp)
    return ParticleFilter(particles, F.FILTER(code))


@configurable
def make_selected_jpsi2ee(
    name="B2CC_Jpsi2EE_Filter_{hash}",
    descriptor="J/psi(1S) -> e- e+",
    vtx_m_min=2100.0 * MeV,
    vtx_m_max=3500.0 * MeV,
    max_vchi2pdof=15.0,
    pt=1000.0 * MeV,
    electron_particles=make_long_and_upstream_electrons_no_brem,
    max_trghostprob=None,
    max_docachi2=30.0,
    pt_e=750 * MeV,
    max_pt_e1e2=1000 * MeV,
    pid_e=2,
    mipchi2_e=None,
    opposite_sign=True,
):
    """
    Filter jpsi(ee) candiates for B2CC.  Default cuts refered to Bs2JpsieePhi.
    """
    electrons = make_electrons(
        particles=electron_particles,
        pid=pid_e,
        pt_with_brem=pt_e,
        mipchi2_min=mipchi2_e,
        max_trghostprob=max_trghostprob,
    )

    detached_dielectron_with_brem = (
        _make_dielectron_with_brem(  ## will add bremsstrahlung photons in this function
            electrons,
            pt_diE=pt,
            m_diE_min=vtx_m_min,
            m_diE_max=vtx_m_max,
            opposite_sign=opposite_sign,
        )
    )

    # only allow LL/LU combinations
    ALLOWED_TRACKTYPES = F.CHILD(1, F.TRACKISLONG @ F.TRACK) | F.CHILD(
        2, F.TRACKISLONG @ F.TRACK
    )

    code_dielectron = F.require_all(
        F.MAXSDOCACHI2CUT(float(max_docachi2)),
        F.CHI2DOF < max_vchi2pdof,
        F.SUBCOMB(Functor=F.MAX(F.PT), Indices=[1, 2]) > max_pt_e1e2,
        ALLOWED_TRACKTYPES,
    )

    return ParticleFilter(detached_dielectron_with_brem, F.FILTER(code_dielectron))


def make_selected_jpsi2ee_wrongsign(
    name="B2CC_Jpsi2EE_WrongSign_{hash}", descriptor="[J/psi(1S) -> e- e-]cc", **kwargs
):
    return make_selected_jpsi2ee(
        name=name, descriptor=descriptor, opposite_sign=False, **kwargs
    )


@configurable
def make_selected_kstar2kpi(
    name="B2CC_Kstar2KPi_Filter_{hash}",
    descriptor="[K*(892)0 -> K+ pi-]cc",
    comb_m_min=826.0 * MeV,
    comb_m_max=966.0 * MeV,
    vtx_m_min=842.0 * MeV,
    vtx_m_max=926.0 * MeV,
    max_vchi2pdof=20,
    pt=1500.0 * MeV,
    max_docachi2=30.0,
    pid_k=1,
    pt_k=400.0 * MeV,
    p_k=4.0 * GeV,
    pid_pi=3,
    pt_pi=300.0 * MeV,
    p_pi=3.0 * GeV,
):
    """
    Filter kstar->kpi candiates for B2CC.  Default cuts refered to Bs2JpsieeKstar.
    """

    kaons = make_kaons(pid=pid_k, pt=pt_k, p=p_k)
    pions = make_pions(pid=pid_pi, pt=pt_pi, p=p_pi)

    return make_twobody(
        particles=[kaons, pions],
        descriptor=descriptor,
        name=name,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
        pt=pt,
        max_docachi2=max_docachi2,
        max_vchi2pdof=max_vchi2pdof,
    )


@configurable
def make_selected_kstar2kpi_wrongsign(
    name="B2CC_Kstar2KPi_WrongSign_{hash}",
    descriptor="[K*(892)0 -> K- pi-]cc",
    comb_m_min=826.0 * MeV,
    comb_m_max=966.0 * MeV,
    vtx_m_min=842.0 * MeV,
    vtx_m_max=926.0 * MeV,
    max_vchi2pdof=20,
    pt=1500.0 * MeV,
    max_docachi2=30.0,
    pid_k=1,
    pt_k=400.0 * MeV,
    p_k=4.0 * GeV,
    pid_pi=3,
    pt_pi=300.0 * MeV,
    p_pi=3.0 * GeV,
):
    """
    Filter kstar->kpi candiates wrong size for B2CC.  Default cuts refered to Bs2JpsieeKstar.
    """

    kaons = make_kaons(pid=pid_k, pt=pt_k, p=p_k)
    pions = make_pions(pid=pid_pi, pt=pt_pi, p=p_pi)

    return make_twobody(
        particles=[kaons, pions],
        descriptor=descriptor,
        name=name,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
        pt=pt,
        max_docachi2=max_docachi2,
        max_vchi2pdof=max_vchi2pdof,
    )


@configurable
def make_selected_kstar2kpi_widerange(
    name="B2CC_Kstar2KPi_WideRange_Filter_{hash}",
    descriptor="[K*(892)0 -> K+ pi-]cc",
    comb_m_min=692.0 * MeV,
    comb_m_max=1900.0 * MeV,
    vtx_m_min=692.0 * MeV,
    vtx_m_max=1900.0 * MeV,
    pt=0.0 * MeV,
    pt_sum=1000.0 * MeV,
    max_docachi2=30.0,
    max_vchi2pdof=25.0,
    pid_pi=10.0,
    pt_pi=250.0 * MeV,
    mipchi2_min_pi=0.0,
    pid_k=0.0,
    pt_k=250.0 * MeV,
    mipchi2_min_k=0.0,
):
    """
    Filter kstar->kpi candiates for B2CC.  Default cuts refered to Bs2JpsiKstar.
    """
    pions = make_pions(pid=pid_pi, pt=pt_pi, mipchi2_min=mipchi2_min_pi)
    kaons = make_kaons(pid=pid_k, pt=pt_k, mipchi2_min=mipchi2_min_k)
    return make_twobody(
        particles=[kaons, pions],
        descriptor=descriptor,
        name=name,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
        pt=pt,
        pt_sum=pt_sum,
        max_docachi2=max_docachi2,
        max_vchi2pdof=max_vchi2pdof,
    )


@configurable
def make_selected_f0(
    name="B2CC_f0_Filter_{hash}",
    descriptor="f_0(980) -> pi+ pi-",
    comb_m_min=278.0 * MeV,
    comb_m_max=2700.0 * MeV,
    vtx_m_min=278.0 * MeV,
    vtx_m_max=2700.0 * MeV,
    pt=0.0 * MeV,
    pt_sum=1000.0 * MeV,
    maxmax_docachi2=20.0,
    max_vchi2pdof=16.0,
    pid_pi=10.0,
    mipchi2_min_pi=4.0,
    pt_pi=250 * MeV,
):
    """
    Filter f0->pi pi candiates for B2CC. Default cuts refered to Bs2Jpsif0.
    """
    pions = make_pions(pid=pid_pi, pt=pt_pi, mipchi2_min=mipchi2_min_pi)
    return make_twobody(
        particles=[pions, pions],
        descriptor=descriptor,
        name=name,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
        pt=pt,
        pt_sum=pt_sum,
        max_docachi2=maxmax_docachi2,
        max_vchi2pdof=max_vchi2pdof,
    )


@configurable
def make_selected_f0kaon(
    name="B2CC_f0Kaon_Filter_{hash}",
    descriptor="f_0(980) -> K+ K-",
    comb_m_min=986.0 * MeV,
    comb_m_max=2700.0 * MeV,
    vtx_m_min=986.0 * MeV,
    vtx_m_max=2700.0 * MeV,
    pt=0.0 * MeV,
    pt_sum=1000.0 * MeV,
    max_docachi2=20.0,
    max_vchi2pdof=16.0,
    pid_k=0.0,
    mipchi2_min_k=4.0,
    pt_k=250.0 * MeV,
):
    """
    Filter f0->K+ K- candiates for B2CC. Default cuts refered to Bs2Jpsif0Kaon.
    """
    kaons = make_kaons(pid=pid_k, pt=pt_k, mipchi2_min=mipchi2_min_k)
    return make_twobody(
        particles=[kaons, kaons],
        descriptor=descriptor,
        name=name,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
        pt=pt,
        pt_sum=pt_sum,
        max_docachi2=max_docachi2,
        max_vchi2pdof=max_vchi2pdof,
    )


@configurable
def make_selected_f0ws(
    name="B2CC_f0ws_Filter_{hash}",
    descriptor="[f_0(980) -> pi- pi-]cc",
    comb_m_min=278.0 * MeV,
    comb_m_max=2700.0 * MeV,
    vtx_m_min=278.0 * MeV,
    vtx_m_max=2700.0 * MeV,
    pt=0.0 * MeV,
    pt_sum=1000.0 * MeV,
    max_docachi2=20.0,
    max_vchi2pdof=16.0,
    pid_pi=10.0,
    mipchi2_min_pi=4.0,
    pt_pi=250.0 * MeV,
):
    """
    Filter f0->pi+ pi- wrong sign candiates for B2CC. Default cuts refered to Bs2Jpsif0ws.
    """
    pions = make_pions(pid=pid_pi, pt=pt_pi, mipchi2_min=mipchi2_min_pi)
    return make_twobody(
        particles=[pions, pions],
        descriptor=descriptor,
        name=name,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
        pt=pt,
        pt_sum=pt_sum,
        max_docachi2=max_docachi2,
        max_vchi2pdof=max_vchi2pdof,
    )


@configurable
def make_selected_f0Unbiased(
    name="B2CC_f0unbiased_Filter_{hash}",
    name_pions="B2CC_f0unbiased_Filter_Pions",
    name_kaons="B2CC_f0unbiased_Filter_Kaons",
    descriptor_pions="f_0(980) -> pi+ pi-",
    descriptor_kaons="f_0(980) -> K+ K-",
    mipchi2_min_pi=0.0,
    mipchi2_min_k=0.0,
):
    """
    Filter f0->pi+ pi- , f0->K+ K- unbiased candiates for B2CC. Default cuts refered to Bs2Jpsif0Unbiased.
    """
    f0_to_pipi = make_selected_f0(
        name=name_pions, descriptor=descriptor_pions, mipchi2_min_pi=mipchi2_min_pi
    )
    f0_to_kk = make_selected_f0kaon(
        name=name_kaons, descriptor=descriptor_kaons, mipchi2_min_k=mipchi2_min_k
    )
    return ParticleContainersMerger([f0_to_pipi, f0_to_kk], name=name)


@configurable
def make_selected_lambda0_pk(
    name="B2CC_Lambda0_pK_Filter_{hash}",
    descriptor="[Lambda(1520)0 -> p+ K-]cc",
    comb_m_min=1430.0 * MeV,
    comb_m_max=3100.0 * MeV,
    vtx_m_min=1430.0 * MeV,
    vtx_m_max=3100.0 * MeV,
    pt=0.0 * MeV,
    pt_sum=1000.0 * MeV,
    max_docachi2=20.0,
    max_vchi2pdof=16.0,
    pid_k=0.0,
    pt_k=250.0 * MeV,
    mipchi2_min_k=4.0,
):
    """
    Filter lambda0 -> p K candiates for B2CC.  Default cuts refered to Lb2JpsipH.
    """
    protons = make_protons()
    kaons = make_kaons(pid=pid_k, pt=pt_k, mipchi2_min=mipchi2_min_k)
    return make_twobody(
        particles=[protons, kaons],
        descriptor=descriptor,
        name=name,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
        pt=pt,
        pt_sum=pt_sum,
        max_docachi2=max_docachi2,
        max_vchi2pdof=max_vchi2pdof,
    )


@configurable
def make_selected_lambda0_pkws(
    name="B2CC_Lambda0_pKws_Filter_{hash}",
    descriptor="[Lambda(1520)0 -> p+ K+]cc",
    comb_m_min=1430.0 * MeV,
    comb_m_max=3100.0 * MeV,
    vtx_m_min=1430.0 * MeV,
    vtx_m_max=3100.0 * MeV,
    pt=0.0 * MeV,
    pt_sum=1000.0 * MeV,
    max_docachi2=20.0,
    max_vchi2pdof=16.0,
    pid_k=0.0,
    pt_k=250.0 * MeV,
    mipchi2_min_k=4.0,
):
    """
    Filter lambda0 -> p K wrong sign candiates for B2CC.  Default cuts refered to Lb2JpsipH.
    """
    protons = make_protons()
    kaons = make_kaons(pid=pid_k, pt=pt_k, mipchi2_min=mipchi2_min_k)
    return make_twobody(
        particles=[protons, kaons],
        descriptor=descriptor,
        name=name,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
        pt=pt,
        pt_sum=pt_sum,
        max_docachi2=max_docachi2,
        max_vchi2pdof=max_vchi2pdof,
    )


@configurable
def make_selected_lambda0_ppi(
    name="B2CC_Lambda0_pPi_Filter_{hash}",
    descriptor="[Lambda(1520)0 -> p+ pi-]cc",
    comb_m_min=1077.0 * MeV,
    comb_m_max=3100.0 * MeV,
    vtx_m_min=1077.0 * MeV,
    vtx_m_max=3100.0 * MeV,
    pt=0.0 * MeV,
    pt_sum=1000.0 * MeV,
    max_docachi2=20.0,
    max_vchi2pdof=16.0,
    pid_pi=10.0,
    pt_pi=250.0 * MeV,
    mipchi2_min_pi=4.0,
):
    """
    Filter lambda0 -> p pi candiates for B2CC.  Default cuts refered to Lb2JpsipH.
    """
    protons = make_protons()
    pions = make_pions(pid=pid_pi, pt=pt_pi, mipchi2_min=mipchi2_min_pi)
    return make_twobody(
        particles=[protons, pions],
        descriptor=descriptor,
        name=name,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
        pt=pt,
        pt_sum=pt_sum,
        max_docachi2=max_docachi2,
        max_vchi2pdof=max_vchi2pdof,
    )


@configurable
def make_selected_lambda0_ppiws(
    name="B2CC_Lambda0_pPiws_Filter_{hash}",
    descriptor="[Lambda(1520)0 -> p+ pi+]cc",
    comb_m_min=1077.0 * MeV,
    comb_m_max=3100.0 * MeV,
    vtx_m_min=1077.0 * MeV,
    vtx_m_max=3100.0 * MeV,
    pt=0.0 * MeV,
    pt_sum=1000.0 * MeV,
    max_docachi2=20.0,
    max_vchi2pdof=16.0,
    pid_pi=10.0,
    pt_pi=250.0 * MeV,
    mipchi2_min_pi=4.0,
):
    """
    Filter lambda0 -> p pi wrong sign candiates for B2CC.  Default cuts refered to Lb2JpsipH.
    """
    protons = make_protons()
    pions = make_pions(pid=pid_pi, pt=pt_pi, mipchi2_min=mipchi2_min_pi)
    return make_twobody(
        particles=[protons, pions],
        descriptor=descriptor,
        name=name,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        vtx_m_min=vtx_m_min,
        vtx_m_max=vtx_m_max,
        pt=pt,
        pt_sum=pt_sum,
        max_docachi2=max_docachi2,
        max_vchi2pdof=max_vchi2pdof,
    )


@configurable
def make_selected_lambda0(name="B2CC_Lambda0_Filter_{hash}"):
    """
    Filter lambda0 -> p K, lambda0 -> p Pi candiates for B2CC.  Default cuts refered to Lb2JpsipH.
    """
    lambda_to_pk = make_selected_lambda0_pk()
    lambda_to_pkws = make_selected_lambda0_pkws()
    lambda_to_ppi = make_selected_lambda0_ppi()
    lambda_to_ppiws = make_selected_lambda0_ppiws()
    return ParticleContainersMerger(
        [lambda_to_pk, lambda_to_pkws, lambda_to_ppi, lambda_to_ppiws], name=name
    )


####################################
# Lambda0 -> p+ pi- LL/DD decays   #
####################################


@configurable
def make_dedicated_lambda_LL(
    name="B2CC_veryloose_lambda_LL",
    make_protons=make_protons,
    make_pions=make_pions,
    mass_window_comb_min=1085.0 * MeV,
    mass_window_comb_max=1145.0 * MeV,
    mass_window_min=1095.0 * MeV,
    mass_window_max=1135.0 * MeV,
    lambda_pt_min=100.0 * MeV,
    pi_p_min=2.0 * GeV,
    p_p_min=2.0 * GeV,
    p_pt_min=0.0,
    pi_pt_min=0.0,
    pi_ipchi2_min=9.0,
    p_ipchi2_min=9.0,
    adocachi2cut=30.0,
    bpvfdchi2_min=50.0,
    bpvvdz_min=8 * mm,
    bpvltime_min=1.0 * ps,
    vchi2pdof_max=15.0,
    endvz_min=0 * mm,
):
    protons = make_protons(
        p_min=p_p_min,
        pt_min=p_pt_min,
        mipchi2_min=p_ipchi2_min,
        pid_p=-10,
        delta_pid_pK=-100,
    )
    pions = make_pions(p=pi_p_min, pt=pi_pt_min, mipchi2_min=pi_ipchi2_min, pid=None)
    descriptor = "[Lambda0 -> p+ pi-]cc"
    combination_code = require_all(
        in_range(mass_window_comb_min, F.MASS, mass_window_comb_max),
        F.MAXDOCACHI2CUT(adocachi2cut),
    )
    vertex_code = require_all(
        in_range(mass_window_min, F.MASS, mass_window_max),
        F.PT > lambda_pt_min,
        F.CHI2DOF < vchi2pdof_max,
        F.END_VZ > endvz_min,
        F.OWNPVLTIME > bpvltime_min,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
        F.OWNPVVDZ > bpvvdz_min,
    )
    return ParticleCombiner(
        [protons, pions],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_dedicated_lambda_DD(
    name="B2CC_loose_lambda_DD",
    make_protons=make_down_protons,
    make_pions=make_down_pions,
    mass_window_comb_min=1075.0 * MeV,
    mass_window_comb_max=1165.0 * MeV,
    mass_window_min=1090.0 * MeV,
    mass_window_max=1140.0 * MeV,
    lambda_pt_min=100.0 * MeV,
    pi_p_min=2.0 * GeV,
    p_p_min=2.0 * GeV,
    p_pt_min=0.0,
    pi_pt_min=0.0,
    pi_ipchi2_min=4.0,
    p_ipchi2_min=4.0,
    adocachi2cut=25.0,
    bpvltime_min=1.0 * ps,
    vchi2pdof_max=15.0,
    vt_p_min=5000.0 * MeV,
    vt_bpvfdchi2_min=50.0,
    endvz_min=250 * mm,
    endvz_max=2485 * mm,
):
    protons = make_protons(
        p_min=p_p_min, pt_min=p_pt_min, mipchi2_min=p_ipchi2_min, pid=None
    )
    pions = make_pions(
        p_min=pi_p_min, mipchi2_min=pi_ipchi2_min, pt_min=pi_pt_min, pid=None
    )
    descriptor = "[Lambda0 -> p+ pi-]cc"
    combination_code = require_all(
        in_range(mass_window_comb_min, F.MASS, mass_window_comb_max),
        F.MAXDOCACHI2CUT(adocachi2cut),
    )
    vertex_code = require_all(
        in_range(mass_window_min, F.MASS, mass_window_max),
        F.PT > lambda_pt_min,
        F.math.in_range(endvz_min, F.END_VZ, endvz_max),
        F.P > vt_p_min,
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVLTIME > bpvltime_min,
        F.OWNPVFDCHI2 > vt_bpvfdchi2_min,
    )

    return ParticleCombiner(
        [protons, pions],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
