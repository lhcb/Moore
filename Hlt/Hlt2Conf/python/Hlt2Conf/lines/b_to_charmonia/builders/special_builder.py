###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of special descriptor for non-default Bs meson decays
that can be shared in the B2CC selections, and therefore are defined centrally.
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, picosecond
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner
from RecoConf.reconstruction_objects import make_pvs


@configurable
def make_X2BsLep(
    particles, descriptor, name="B2CC_X2Bsmu_Combiner_{hash}", pvs=make_pvs
):
    """
    A X->BLep decay maker, where X
    is a dummy particle
    """
    combination_code = F.ALL
    vertex_code = F.ALL
    return ParticleCombiner(
        name=name,
        Inputs=particles,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_JpsiPP_X(
    particles,
    descriptor,
    name="B2CC_JpsiPP_X_Combiner_{hash}",
    am_min_mass=5.0 * GeV,
    am_max_mass=5.6 * GeV,
    vtx_min_mass=5.05 * GeV,
    vtx_max_mass=5.55 * GeV,
    max_ppK_mass=5.2 * GeV,
    pp_min_sumpt=750.0 * MeV,
    pp_min_sump=7000.0 * MeV,
    pp_min_p=4.0 * GeV,
    p_min_pt=400.0 * MeV,
    vtx_min_sumpt=3000.0 * MeV,
    vtx_min_pt=2000.0 * MeV,
    vtx_max_chi2=9.0,
    vtx_min_dira=0.9997,
    sdocachi2_max=20.0,
    b_min_lft=0.2 * picosecond,
    X_kstar=True,
):
    combination12_code = F.require_all(  # Jpsi->ppbar combination
        F.math.in_range(
            (3096.900 - 220.0) * MeV, F.MASS, (3096.900 + 220.0) * MeV
        ),  # J/psi mass range comb
        F.SUM(F.PT) > pp_min_sumpt,
        F.SUM(F.P) > pp_min_sump,
        F.MAX(F.P) > pp_min_p,
        F.MAX(F.PT) > p_min_pt,
        F.CHILD(1, F.PROBNN_P) * F.CHILD(2, F.PROBNN_P) > 0.01,
        F.MAXDOCACHI2CUT(sdocachi2_max),
    )

    combination123_code = F.require_all(  # ppbarK+ combination
        F.MASS < max_ppK_mass, F.MAXDOCACHI2CUT(sdocachi2_max)
    )

    combination_code = F.require_all(
        F.math.in_range(am_min_mass, F.MASS, am_max_mass),
        F.MAXDOCACHI2CUT(sdocachi2_max),
        F.MAXDOCACUT(0.25 * mm),
    )

    if X_kstar is True:
        combination_code &= F.require_all(  ## ppbarKstar(->K+pi-) combination
            F.SUBCOMB(
                Functor=F.math.in_range(
                    (891.67 - 200.0) * MeV, F.MASS, (891.67 + 200.0) * MeV
                ),
                Indices=[3, 4],
            )
        )
    else:
        combination_code &= F.require_all(  ## ppbarPhi(1020)(->K+K-) combination
            F.SUBCOMB(
                Functor=F.math.in_range(
                    (1019.461 - 200.0) * MeV, F.MASS, (1019.461 + 200.0) * MeV
                ),
                Indices=[3, 4],
            )
        )

    vertex_code = F.require_all(  ## fit to the B0(s)->J/psi(->ppbar)hh, K*(890)->Kpi or Phi(1020)->KK
        F.math.in_range(vtx_min_mass, F.MASS, vtx_max_mass),
        F.CHI2DOF < vtx_max_chi2,
        F.OWNPVDIRA > vtx_min_dira,
        F.PT > vtx_min_pt,
        F.SUM(F.PT) > vtx_min_sumpt,
        F.OWNPVIP < 0.2 * mm,
        F.OWNPVLTIME > b_min_lft,
        F.OWNPVFDCHI2 > 100,
    )
    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        Combination123Cut=combination123_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
