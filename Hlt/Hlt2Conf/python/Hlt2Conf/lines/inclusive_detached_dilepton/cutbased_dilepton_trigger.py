###############################################################################
# (c) Copyright 2022-24 CERN for the benefit of the LHCb Collaboration        #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the cut-based inclusive detached dilepton trigger lines
"""

from Moore.config import Hlt2Line, register_line_builder
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.inclusive_detached_dilepton.cutbased_dilepton_builders import (
    _double_fake_prescale,
    _single_fake_prescale,
    make_inclusive_cut_dielectron,
    make_inclusive_cut_dilepton_plus_tracks,
    make_inclusive_cut_dimuon,
    make_inclusive_cut_electronmuon,
)
from Hlt2Conf.lines.rd.builders.rd_prefilters import (
    _RD_MONITORING_VARIABLES,
    rd_prefilter,
)

all_lines = {}

################################################
# 2-body inclusive cut-based dilepton lines    #
################################################

####################################
# 2-body lines (opposite-sign)     #
####################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectron(
    name="Hlt2CutBasedInclDielectron", prescale=1, persistreco=True
):
    """
    HLT2 line for cut-based inclusive selection of 2-body dielectron events with opposite-sign electrons, e.g. B_s0 -> e+ e-
    Line categories:   - 2-body   - dielectron
    """

    dielectrons = make_inclusive_cut_dielectron(parent_id="B_s0")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuon(
    name="Hlt2CutBasedInclElectronMuon", prescale=1, persistreco=True
):
    """
    HLT2 line for cut-based inclusive selection of 2-body electron-muon events with opposite-sign leptons, e.g. B_s0 -> e+ mu-
    Line categories:   - 2-body   - electron-muon
    """

    electronmuons = make_inclusive_cut_electronmuon(parent_id="B_s0")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [electronmuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuon(name="Hlt2CutBasedInclDimuon", prescale=1, persistreco=True):
    """
    HLT2 line for cut-based inclusive selection of 2-body dimuon events with opposite-sign muon, e.g. B_s0 -> mu+ mu-
    Line categories:   - 2-body   - dimuon
    """

    dimuons = make_inclusive_cut_dimuon(parent_id="B_s0")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


############################################
# 2-body single fake lines (opposite-sign) #
############################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronFakeE(
    name="Hlt2CutBasedInclDielectronFakeE",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 2-body dielectron events with opposite-sign electrons, e.g. B_s0 -> e+ e-
    Line categories:   - 2-body   - dielectron  - fake e
    """

    dielectrons = make_inclusive_cut_dielectron(parent_id="B_s0", fake_electrons=1)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonFakeE(
    name="Hlt2CutBasedInclElectronMuonFakeE",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 2-body electron-muon events with opposite-sign electrons, e.g. B_s0 -> mu+ e-
    Line categories:   - 2-body   - electron-muon   - fake e
    """

    dileptons = make_inclusive_cut_electronmuon(parent_id="B_s0", fake_electrons=1)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonFakeMu(
    name="Hlt2CutBasedInclElectronMuonFakeMu",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 2-body electron-muon events with opposite-sign electrons, e.g. B_s0 -> mu+ e-
    Line categories:   - 2-body   - electron-muon   - fake mu
    """

    dileptons = make_inclusive_cut_electronmuon(parent_id="B_s0", fake_muons=1)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonFakeMu(
    name="Hlt2CutBasedInclDimuonFakeMu",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 2-body dimuon events with opposite-sign electrons, e.g. B_s0 -> mu+ mu-
    Line categories:   - 2-body   - dimuon  - fake mu
    """

    dimuons = make_inclusive_cut_dimuon(parent_id="B_s0", fake_muons=1)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


############################################
# 2-body double fake lines (opposite-sign) #
############################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronFakeEE(
    name="Hlt2CutBasedInclDielectronFakeEE",
    prescale=_double_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 2-body dielectron events with opposite-sign electrons, e.g. B_s0 -> e+ e-
    Line categories:   - 2-body   - dielectron  - fake ee
    """

    dielectrons = make_inclusive_cut_dielectron(parent_id="B_s0", fake_electrons=2)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonFakeEMu(
    name="Hlt2CutBasedInclElectronMuonFakeEMu",
    prescale=_double_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 2-body electron-muon events with opposite-sign electrons, e.g. B_s0 -> mu+ e-
    Line categories:   - 2-body   - electron-muon   - fake emu
    """

    dileptons = make_inclusive_cut_electronmuon(
        parent_id="B_s0", fake_electrons=1, fake_muons=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonFakeMuMu(
    name="Hlt2CutBasedInclDimuonFakeMuMu",
    prescale=_double_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 2-body dimuon events with opposite-sign electrons, e.g. B_s0 -> mu+ mu-
    Line categories:   - 2-body   - dimuon  - fake mumu
    """

    dimuons = make_inclusive_cut_dimuon(parent_id="B_s0", fake_muons=2)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


#############################
# 2-body lines (same-sign)  #
#############################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronSS(
    name="Hlt2CutBasedInclDielectronSS", prescale=1, persistreco=True
):
    """
    HLT2 line for cut-based inclusive selection of 2-body dielectron events with same-sign electrons, e.g. B_s0 -> e+ e+
    Line categories:   - 2-body   - dielectron   - same-sign
    """

    dielectrons = make_inclusive_cut_dielectron(parent_id="B_s0", same_sign=True)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonSS(
    name="Hlt2CutBasedInclElectronMuonSS", prescale=1, persistreco=True
):
    """
    HLT2 line for cut-based inclusive selection of 2-body electron-muon events with same-sign leptons, e.g. B_s0 -> e+ mu+
    Line categories:   - 2-body   - electron-muon   - same-sign
    """

    electronmuons = make_inclusive_cut_electronmuon(parent_id="B_s0", same_sign=True)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [electronmuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonSS(
    name="Hlt2CutBasedInclDimuonSS", prescale=1, persistreco=True
):
    """
    HLT2 line for cut-based inclusive selection of 2-body dimuon events with same-sign muons, e.g. B_s0 -> mu+ mu+
    Line categories:   - 2-body   - dimuon   - same-sign
    """

    dimuons = make_inclusive_cut_dimuon(parent_id="B_s0", same_sign=True)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


########################################
# 2-body single fake lines (same-sign) #
########################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronFakeESS(
    name="Hlt2CutBasedInclDielectronFakeESS",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 2-body dielectron events with opposite-sign electrons, e.g. B_s0 -> e+ e-
    Line categories:   - 2-body   - dielectron  - same-sign - fake e
    """

    dielectrons = make_inclusive_cut_dielectron(
        parent_id="B_s0", same_sign=True, fake_electrons=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonFakeESS(
    name="Hlt2CutBasedInclElectronMuonFakeESS",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 2-body electron-muon events with opposite-sign electrons, e.g. B_s0 -> mu+ e-
    Line categories:   - 2-body   - electron-muon   - same-sign - fake e
    """

    dileptons = make_inclusive_cut_electronmuon(
        parent_id="B_s0", same_sign=True, fake_electrons=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonFakeMuSS(
    name="Hlt2CutBasedInclElectronMuonFakeMuSS",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 2-body electron-muon events with opposite-sign electrons, e.g. B_s0 -> mu+ e-
    Line categories:   - 2-body   - electron-muon   - same-sign - fake mu
    """

    dileptons = make_inclusive_cut_electronmuon(
        parent_id="B_s0", same_sign=True, fake_muons=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonFakeMuSS(
    name="Hlt2CutBasedInclDimuonFakeMuSS",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 2-body dimuon events with opposite-sign electrons, e.g. B_s0 -> mu+ mu-
    Line categories:   - 2-body   - dimuon  - same-sign - fake mu
    """

    dimuons = make_inclusive_cut_dimuon(parent_id="B_s0", same_sign=True, fake_muons=1)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


########################################
# 2-body double fake lines (same-sign) #
########################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronFakeEESS(
    name="Hlt2CutBasedInclDielectronFakeEESS",
    prescale=_double_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 2-body dielectron events with opposite-sign electrons, e.g. B_s0 -> e+ e-
    Line categories:   - 2-body   - dielectron  - same-sign - fake ee
    """

    dielectrons = make_inclusive_cut_dielectron(
        parent_id="B_s0", same_sign=True, fake_electrons=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonFakeEMuSS(
    name="Hlt2CutBasedInclElectronMuonFakeEMuSS",
    prescale=_double_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 2-body electron-muon events with opposite-sign electrons, e.g. B_s0 -> mu+ e-
    Line categories:   - 2-body   - electron-muon   - same-sign - fake emu
    """

    dileptons = make_inclusive_cut_electronmuon(
        parent_id="B_s0", same_sign=True, fake_electrons=1, fake_muons=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonFakeMuMuSS(
    name="Hlt2CutBasedInclDimuonFakeMuMuSS",
    prescale=_double_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 2-body dimuon events with opposite-sign electrons, e.g. B_s0 -> mu+ mu-
    Line categories:   - 2-body   - dimuon  - same-sign - fake mumu
    """

    dimuons = make_inclusive_cut_dimuon(parent_id="B_s0", same_sign=True, fake_muons=2)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons],
        persistreco=persistreco,
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


################################################
# 3-body inclusive cut-based dilepton lines    #
################################################

####################################
# 3-body lines (opposite-sign)     #
####################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronPlusTrack(
    name="Hlt2CutBasedInclDielectronPlusTrack", prescale=1, persistreco=True
):
    """
    HLT2 line for cut-based inclusive selection of 3-body dielectron events with opposite-sign electrons, e.g. B+ -> J/psi(1S)  (-> e+ e-) pi+
    Line categories:   - 3-body   - dielectron
    """
    pvs = make_pvs()

    dielectrons = make_inclusive_cut_dielectron(parent_id="J/psi(1S)")
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons, pvs, DecayDescriptor="[B+ -> J/psi(1S) pi+]cc", n_tracks=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonPlusTrack(
    name="Hlt2CutBasedInclElectronMuonPlusTrack", prescale=1, persistreco=True
):
    """
    HLT2 line for cut-based inclusive selection of 3-body electron-muon events with opposite-sign leptons, e.g. B+ -> J/psi(1S)  (-> e+ mu-) pi+
    Line categories:   - 3-body   - electron-muon
    """
    pvs = make_pvs()

    dileptons = make_inclusive_cut_electronmuon(parent_id="J/psi(1S)")
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor="[B+ -> J/psi(1S) pi+]cc", n_tracks=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonPlusTrack(
    name="Hlt2CutBasedInclDimuonPlusTrack", prescale=1, persistreco=True
):
    """
    HLT2 line for cut-based inclusive selection of 3-body dimuon events with opposite-sign muon, e.g. B+ -> J/psi(1S) (-> mu+ mu-) pi+
    Line categories:   - 3-body   - dimuon
    """
    pvs = make_pvs()

    dimuons = make_inclusive_cut_dimuon(parent_id="J/psi(1S)")
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor="[B+ -> J/psi(1S) pi+]cc", n_tracks=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


############################################
# 3-body single fake lines (opposite-sign) #
############################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronFakeEPlusTrack(
    name="Hlt2CutBasedInclDielectronFakeEPlusTrack",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 3-body dielectron events with opposite-sign electrons, e.g. B+ -> J/psi(1S) (-> e+ e-) pi+
    Line categories:   - 3-body   - dielectron  - fake e
    """

    pvs = make_pvs()
    dielectrons = make_inclusive_cut_dielectron(parent_id="J/psi(1S)", fake_electrons=1)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons, pvs, DecayDescriptor="[B+ -> J/psi(1S) pi+]cc", n_tracks=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonFakeEPlusTrack(
    name="Hlt2CutBasedInclElectronMuonFakeEPlusTrack",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 3-body electron-muon events with opposite-sign leptons, e.g. B+ -> J/psi(1S) (-> e+ mu-) pi+
    Line categories:   - 3-body   - electron-muon   - fake e
    """

    pvs = make_pvs()
    dileptons = make_inclusive_cut_electronmuon(parent_id="J/psi(1S)", fake_electrons=1)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor="[B+ -> J/psi(1S) pi+]cc", n_tracks=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonFakeMuPlusTrack(
    name="Hlt2CutBasedInclElectronMuonFakeMuPlusTrack",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 3-body electron-muon events with opposite-sign leptons, e.g. B+ -> J/psi(1S) (-> e+ mu-) pi+
    Line categories:   - 3-body   - electron-muon   - fake mu
    """

    pvs = make_pvs()
    dileptons = make_inclusive_cut_electronmuon(parent_id="J/psi(1S)", fake_muons=1)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor="[B+ -> J/psi(1S) pi+]cc", n_tracks=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonFakeMuPlusTrack(
    name="Hlt2CutBasedInclDimuonFakeMuPlusTrack",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 3-body dimuon events with opposite-sign muons, e.g. B_s0 -> e+ e-
    Line categories:   - 3-body   - dielectron - single fake mu
    """

    pvs = make_pvs()
    dimuons = make_inclusive_cut_dimuon(parent_id="J/psi(1S)", fake_muons=1)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor="[B+ -> J/psi(1S) pi+]cc", n_tracks=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


############################################
# 3-body double fake lines (opposite-sign) #
############################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronFakeEEPlusTrack(
    name="Hlt2CutBasedInclDielectronFakeEEPlusTrack",
    prescale=_double_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 3-body dielectron events with opposite-sign electrons, e.g. B+ -> J/psi(1S) (-> e+ e-) pi+
    Line categories:   - 3-body   - dielectron  - fake ee
    """

    pvs = make_pvs()
    dielectrons = make_inclusive_cut_dielectron(parent_id="J/psi(1S)", fake_electrons=2)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons, pvs, DecayDescriptor="[B+ -> J/psi(1S) pi+]cc", n_tracks=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonFakeEMuPlusTrack(
    name="Hlt2CutBasedInclElectronMuonFakeEMuPlusTrack",
    prescale=_double_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 3-body electron-muon events with opposite-sign leptons, e.g. B+ -> J/psi(1S) (-> mu+ e-) pi+
    Line categories:   - 3-body   - electron-muon   - fake emu
    """

    pvs = make_pvs()
    dileptons = make_inclusive_cut_electronmuon(
        parent_id="J/psi(1S)", fake_electrons=1, fake_muons=1
    )
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor="[B+ -> J/psi(1S) pi+]cc", n_tracks=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonFakeMuMuPlusTrack(
    name="Hlt2CutBasedInclDimuonFakeMuMuPlusTrack",
    prescale=_double_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 3-body dimuon events with opposite-sign muons, e.g. B+ -> J/psi(1S) (-> mu+ mu-) pi+
    Line categories:   - 3-body   - dimuon  - fake mumu
    """

    pvs = make_pvs()
    dimuons = make_inclusive_cut_dimuon(parent_id="J/psi(1S)", fake_muons=2)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor="[B+ -> J/psi(1S) pi+]cc", n_tracks=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


####################################
# 3-body lines (same-sign)         #
####################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronPlusTrackSS(
    name="Hlt2CutBasedInclDielectronPlusTrackSS", prescale=1, persistreco=True
):
    """
    HLT2 line for cut-based inclusive selection of 3-body dielectron events with opposite-sign electrons, e.g. B+ -> J/psi(1S)  (-> e+ e+) pi+
    Line categories:   - 3-body   - dielectron
    """
    pvs = make_pvs()

    dielectrons = make_inclusive_cut_dielectron(parent_id="J/psi(1S)", same_sign=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons, pvs, DecayDescriptor="[B+ -> J/psi(1S) pi+]cc", n_tracks=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonPlusTrackSS(
    name="Hlt2CutBasedInclElectronMuonPlusTrackSS", prescale=1, persistreco=True
):
    """
    HLT2 line for cut-based inclusive selection of 3-body electron-muon events with opposite-sign leptons, e.g. B+ -> J/psi(1S)  (-> e+ mu+) pi+
    Line categories:   - 3-body   - electron-muon
    """
    pvs = make_pvs()

    dileptons = make_inclusive_cut_electronmuon(parent_id="J/psi(1S)", same_sign=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor="[B+ -> J/psi(1S) pi+]cc", n_tracks=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonPlusTrackSS(
    name="Hlt2CutBasedInclDimuonPlusTrackSS", prescale=1, persistreco=True
):
    """
    HLT2 line for cut-based inclusive selection of 3-body dimuon events with opposite-sign muon, e.g. B+ -> J/psi(1S) (-> mu+ mu+) pi+
    Line categories:   - 3-body   - dimuon
    """
    pvs = make_pvs()

    dimuons = make_inclusive_cut_dimuon(parent_id="J/psi(1S)", same_sign=True)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor="[B+ -> J/psi(1S) pi+]cc", n_tracks=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


############################################
# 3-body single fake lines (same-sign)     #
############################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronFakeEPlusTrackSS(
    name="Hlt2CutBasedInclDielectronFakeEPlusTrackSS",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 3-body dielectron events with opposite-sign electrons, e.g. B+ -> J/psi(1S) (-> e+ e+) pi+
    Line categories:   - 3-body   - dielectron  - fake e
    """

    pvs = make_pvs()
    dielectrons = make_inclusive_cut_dielectron(
        parent_id="J/psi(1S)", same_sign=True, fake_electrons=1
    )
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons, pvs, DecayDescriptor="[B+ -> J/psi(1S) pi+]cc", n_tracks=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonFakeEPlusTrackSS(
    name="Hlt2CutBasedInclElectronMuonFakeEPlusTrackSS",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 3-body electron-muon events with opposite-sign leptons, e.g. B+ -> J/psi(1S) (-> e+ mu+) pi+
    Line categories:   - 3-body   - electron-muon   - fake e
    """

    pvs = make_pvs()
    dileptons = make_inclusive_cut_electronmuon(
        parent_id="J/psi(1S)", same_sign=True, fake_electrons=1
    )
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor="[B+ -> J/psi(1S) pi+]cc", n_tracks=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonFakeMuPlusTrackSS(
    name="Hlt2CutBasedInclElectronMuonFakeMuPlusTrackSS",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 3-body electron-muon events with opposite-sign leptons, e.g. B+ -> J/psi(1S) (-> e+ mu+) pi+
    Line categories:   - 3-body   - electron-muon   - fake mu
    """

    pvs = make_pvs()
    dileptons = make_inclusive_cut_electronmuon(
        parent_id="J/psi(1S)", same_sign=True, fake_muons=1
    )
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor="[B+ -> J/psi(1S) pi+]cc", n_tracks=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonFakeMuPlusTrackSS(
    name="Hlt2CutBasedInclDimuonFakeMuPlusTrackSS",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 3-body dimuon events with opposite-sign muons, e.g. B+ -> J/psi(1S) (-> mu+ mu+) pi+
    Line categories:   - 3-body   - dielectron - single fake mu
    """

    pvs = make_pvs()
    dimuons = make_inclusive_cut_dimuon(
        parent_id="J/psi(1S)", same_sign=True, fake_muons=1
    )
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor="[B+ -> J/psi(1S) pi+]cc", n_tracks=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


############################################
# 3-body double fake lines (same-sign)     #
############################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronFakeEEPlusTrackSS(
    name="Hlt2CutBasedInclDielectronFakeEEPlusTrackSS",
    prescale=_double_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 3-body dielectron events with opposite-sign electrons, e.g. B+ -> J/psi(1S) (-> e+ e+) pi+
    Line categories:   - 3-body   - dielectron  - fake ee
    """

    pvs = make_pvs()
    dielectrons = make_inclusive_cut_dielectron(
        parent_id="J/psi(1S)", same_sign=True, fake_electrons=2
    )
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons, pvs, DecayDescriptor="[B+ -> J/psi(1S) pi+]cc", n_tracks=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonFakeEMuPlusTrackSS(
    name="Hlt2CutBasedInclElectronMuonFakeEMuPlusTrackSS",
    prescale=_double_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 3-body electron-muon events with opposite-sign leptons, e.g. B+ -> J/psi(1S) (-> e+ mu+) pi+
    Line categories:   - 3-body   - electron-muon   - fake emu
    """

    pvs = make_pvs()
    dileptons = make_inclusive_cut_electronmuon(
        parent_id="J/psi(1S)", same_sign=True, fake_electrons=1, fake_muons=1
    )
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor="[B+ -> J/psi(1S) pi+]cc", n_tracks=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonFakeMuMuPlusTrackSS(
    name="Hlt2CutBasedInclDimuonFakeMuMuPlusTrackSS",
    prescale=_double_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 3-body dimuon events with opposite-sign muons, e.g. B+ -> J/psi(1S) (-> mu+ mu+) pi+
    Line categories:   - 3-body   - dimuon  - fake mumu
    """

    pvs = make_pvs()
    dimuons = make_inclusive_cut_dimuon(
        parent_id="J/psi(1S)", same_sign=True, fake_muons=2
    )
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor="[B+ -> J/psi(1S) pi+]cc", n_tracks=1
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


################################################
# 4-body inclusive cut-based dilepton lines    #
################################################

####################################
# 4-body lines (opposite-sign)     #
####################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronPlusTwoTrack(
    name="Hlt2CutBasedInclDielectronPlusTwoTrack", prescale=1, persistreco=True
):
    """
    HLT2 line for cut-based inclusive selection of 4-body dielectron events with opposite-sign electrons, e.g. B_s0 -> J/psi(1S)  (-> e+ e-) pi+
    Line categories:   - 4-body   - dielectron
    """
    pvs = make_pvs()

    dielectrons = make_inclusive_cut_dielectron(parent_id="J/psi(1S)")
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons, pvs, DecayDescriptor="[B_s0 -> J/psi(1S) pi+ pi-]cc", n_tracks=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonPlusTwoTrack(
    name="Hlt2CutBasedInclElectronMuonPlusTwoTrack", prescale=1, persistreco=True
):
    """
    HLT2 line for cut-based inclusive selection of 4-body electron-muon events with opposite-sign leptons, e.g. B_s0 -> J/psi(1S)  (-> e+ mu-) pi+
    Line categories:   - 4-body   - electron-muon
    """

    pvs = make_pvs()
    dileptons = make_inclusive_cut_electronmuon(parent_id="J/psi(1S)")
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor="[B_s0 -> J/psi(1S) pi+ pi-]cc", n_tracks=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonPlusTwoTrack(
    name="Hlt2CutBasedInclDimuonPlusTwoTrack", prescale=1, persistreco=True
):
    """
    HLT2 line for cut-based inclusive selection of 4-body dimuon events with opposite-sign muon, e.g. B_s0 -> J/psi(1S) (-> mu+ mu-) pi+
    Line categories:   - 4-body   - dimuon
    """

    pvs = make_pvs()
    dimuons = make_inclusive_cut_dimuon(parent_id="J/psi(1S)")
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor="[B_s0 -> J/psi(1S) pi+ pi-]cc", n_tracks=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


############################################
# 4-body single fake lines (opposite-sign) #
############################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronFakeEPlusTwoTrack(
    name="Hlt2CutBasedInclDielectronFakeEPlusTwoTrack",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 4-body dielectron events with opposite-sign electrons, e.g. B_s0 -> J/psi(1S) (-> e+ e-) pi+
    Line categories:   - 4-body   - dielectron  - fake e
    """

    pvs = make_pvs()
    dielectrons = make_inclusive_cut_dielectron(parent_id="J/psi(1S)", fake_electrons=1)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons, pvs, DecayDescriptor="[B_s0 -> J/psi(1S) pi+ pi-]cc", n_tracks=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonFakeEPlusTwoTrack(
    name="Hlt2CutBasedInclElectronMuonFakeEPlusTwoTrack",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 4-body electron-muon events with opposite-sign leptons, e.g. B_s0 -> J/psi(1S) (-> e+ mu-) pi+
    Line categories:   - 4-body   - electron-muon   - fake e
    """

    pvs = make_pvs()
    dileptons = make_inclusive_cut_electronmuon(parent_id="J/psi(1S)", fake_electrons=1)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor="[B_s0 -> J/psi(1S) pi+ pi-]cc", n_tracks=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonFakeMuPlusTwoTrack(
    name="Hlt2CutBasedInclElectronMuonFakeMuPlusTwoTrack",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 4-body electron-muon events with opposite-sign leptons, e.g. B_s0 -> J/psi(1S) (-> e+ mu-) pi+
    Line categories:   - 4-body   - electron-muon   - fake mu
    """

    pvs = make_pvs()
    dileptons = make_inclusive_cut_electronmuon(parent_id="J/psi(1S)", fake_muons=1)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor="[B_s0 -> J/psi(1S) pi+ pi-]cc", n_tracks=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonFakeMuPlusTwoTrack(
    name="Hlt2CutBasedInclDimuonFakeMuPlusTwoTrack",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 4-body dimuon events with opposite-sign muons, e.g. B_s0 -> e+ e-
    Line categories:   - 4-body   - dielectron - single fake mu
    """

    pvs = make_pvs()
    dimuons = make_inclusive_cut_dimuon(parent_id="J/psi(1S)", fake_muons=1)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor="[B_s0 -> J/psi(1S) pi+ pi-]cc", n_tracks=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


############################################
# 4-body double fake lines (opposite-sign) #
############################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronFakeEEPlusTwoTrack(
    name="Hlt2CutBasedInclDielectronFakeEEPlusTwoTrack",
    prescale=_double_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 4-body dielectron events with opposite-sign electrons, e.g. B_s0 -> J/psi(1S) (-> e+ e-) pi+
    Line categories:   - 4-body   - dielectron  - fake ee
    """

    pvs = make_pvs()
    dielectrons = make_inclusive_cut_dielectron(parent_id="J/psi(1S)", fake_electrons=2)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons, pvs, DecayDescriptor="[B_s0 -> J/psi(1S) pi+ pi-]cc", n_tracks=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonFakeEMuPlusTwoTrack(
    name="Hlt2CutBasedInclElectronMuonFakeEMuPlusTwoTrack",
    prescale=_double_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 4-body electron-muon events with opposite-sign leptons, e.g. B_s0 -> J/psi(1S) (-> mu+ e-) pi+
    Line categories:   - 4-body   - electron-muon   - fake emu
    """

    pvs = make_pvs()
    dileptons = make_inclusive_cut_electronmuon(
        parent_id="J/psi(1S)", fake_electrons=1, fake_muons=1
    )
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor="[B_s0 -> J/psi(1S) pi+ pi-]cc", n_tracks=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonFakeMuMuPlusTwoTrack(
    name="Hlt2CutBasedInclDimuonFakeMuMuPlusTwoTrack",
    prescale=_double_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 4-body dimuon events with opposite-sign muons, e.g. B_s0 -> J/psi(1S) (-> mu+ mu-) pi+
    Line categories:   - 4-body   - dimuon  - fake mumu
    """

    pvs = make_pvs()
    dimuons = make_inclusive_cut_dimuon(parent_id="J/psi(1S)", fake_muons=2)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor="[B_s0 -> J/psi(1S) pi+ pi-]cc", n_tracks=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


################################
# 4-body lines (same-sign)     #
################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronPlusTwoTrackSS(
    name="Hlt2CutBasedInclDielectronPlusTwoTrackSS", prescale=1, persistreco=True
):
    """
    HLT2 line for cut-based inclusive selection of 4-body dielectron events with same-sign electrons, e.g. B_s0 -> J/psi(1S) (-> e+ e+) pi+ pi-
    Line categories:   - 4-body   - dielectron
    """
    pvs = make_pvs()

    dielectrons = make_inclusive_cut_dielectron(parent_id="J/psi(1S)")
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons, pvs, DecayDescriptor="[B_s0 -> J/psi(1S) pi+ pi-]cc", n_tracks=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonPlusTwoTrackSS(
    name="Hlt2CutBasedInclElectronMuonPlusTwoTrackSS", prescale=1, persistreco=True
):
    """
    HLT2 line for cut-based inclusive selection of 4-body electron-muon events with same-sign leptons, e.g. B_s0 -> J/psi(1S) (-> e+ mu+) pi+ pi-
    Line categories:   - 4-body   - electron-muon
    """

    pvs = make_pvs()
    dileptons = make_inclusive_cut_electronmuon(parent_id="J/psi(1S)")
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor="[B_s0 -> J/psi(1S) pi+ pi-]cc", n_tracks=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonPlusTwoTrackSS(
    name="Hlt2CutBasedInclDimuonPlusTwoTrackSS", prescale=1, persistreco=True
):
    """
    HLT2 line for cut-based inclusive selection of 4-body dimuon events with same-sign muon, e.g. B_s0 -> J/psi(1S) (-> mu+ mu+) pi+ pi-
    Line categories:   - 4-body   - dimuon
    """

    pvs = make_pvs()
    dimuons = make_inclusive_cut_dimuon(parent_id="J/psi(1S)")
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor="[B_s0 -> J/psi(1S) pi+ pi-]cc", n_tracks=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


########################################
# 4-body single fake lines (same-sign) #
########################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronFakeEPlusTwoTrackSS(
    name="Hlt2CutBasedInclDielectronFakeEPlusTwoTrackSS",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 4-body dielectron events with same-sign electrons, e.g. B_s0 -> J/psi(1S) (-> e+ e+) pi+ pi-
    Line categories:   - 4-body   - dielectron  - fake e
    """

    pvs = make_pvs()
    dielectrons = make_inclusive_cut_dielectron(parent_id="J/psi(1S)", fake_electrons=1)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons, pvs, DecayDescriptor="[B_s0 -> J/psi(1S) pi+ pi-]cc", n_tracks=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonFakeEPlusTwoTrackSS(
    name="Hlt2CutBasedInclElectronMuonFakeEPlusTwoTrackSS",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 4-body electron-muon events with same-sign leptons, e.g. B_s0 -> J/psi(1S) (-> e+ mu+) pi+ pi-
    Line categories:   - 4-body   - electron-muon   - fake e
    """

    pvs = make_pvs()
    dileptons = make_inclusive_cut_electronmuon(parent_id="J/psi(1S)", fake_electrons=1)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor="[B_s0 -> J/psi(1S) pi+ pi-]cc", n_tracks=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonFakeMuPlusTwoTrackSS(
    name="Hlt2CutBasedInclElectronMuonFakeMuPlusTwoTrackSS",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 4-body electron-muon events with same-sign leptons, e.g. B_s0 -> J/psi(1S) (-> e+ mu+) pi+ pi-
    Line categories:   - 4-body   - electron-muon   - fake mu
    """

    pvs = make_pvs()
    dileptons = make_inclusive_cut_electronmuon(parent_id="J/psi(1S)", fake_muons=1)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor="[B_s0 -> J/psi(1S) pi+ pi-]cc", n_tracks=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonFakeMuPlusTwoTrackSS(
    name="Hlt2CutBasedInclDimuonFakeMuPlusTwoTrackSS",
    prescale=_single_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 4-body dimuon events with same-sign muons, e.g. B_s0 -> e+ e+
    Line categories:   - 4-body   - dielectron - single fake mu
    """

    pvs = make_pvs()
    dimuons = make_inclusive_cut_dimuon(parent_id="J/psi(1S)", fake_muons=1)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor="[B_s0 -> J/psi(1S) pi+ pi-]cc", n_tracks=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


########################################
# 4-body double fake lines (same-sign) #
########################################


@register_line_builder(all_lines)
def Hlt2CutBasedInclDielectronFakeEEPlusTwoTrackSS(
    name="Hlt2CutBasedInclDielectronFakeEEPlusTwoTrackSS",
    prescale=_double_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 4-body dielectron events with same-sign electrons, e.g. B_s0 -> J/psi(1S) (-> e+ e+) pi+ pi-
    Line categories:   - 4-body   - dielectron  - fake ee
    """

    pvs = make_pvs()
    dielectrons = make_inclusive_cut_dielectron(parent_id="J/psi(1S)", fake_electrons=2)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dielectrons, pvs, DecayDescriptor="[B_s0 -> J/psi(1S) pi+ pi-]cc", n_tracks=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dielectrons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclElectronMuonFakeEMuPlusTwoTrackSS(
    name="Hlt2CutBasedInclElectronMuonFakeEMuPlusTwoTrackSS",
    prescale=_double_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 4-body electron-muon events with same-sign leptons, e.g. B_s0 -> J/psi(1S) (-> e+ mu+) pi+
    Line categories:   - 4-body   - electron-muon   - fake emu
    """

    pvs = make_pvs()
    dileptons = make_inclusive_cut_electronmuon(
        parent_id="J/psi(1S)", fake_electrons=1, fake_muons=1
    )
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dileptons, pvs, DecayDescriptor="[B_s0 -> J/psi(1S) pi+ pi-]cc", n_tracks=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dileptons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
def Hlt2CutBasedInclDimuonFakeMuMuPlusTwoTrackSS(
    name="Hlt2CutBasedInclDimuonFakeMuMuPlusTwoTrackSS",
    prescale=_double_fake_prescale,
    persistreco=True,
):
    """
    HLT2 line for cut-based inclusive selection of 4-body dimuon events with same-sign muons, e.g. B_s0 -> J/psi(1S) (-> mu+ mu+) pi+
    Line categories:   - 4-body   - dimuon  - fake mumu
    """

    pvs = make_pvs()
    dimuons = make_inclusive_cut_dimuon(parent_id="J/psi(1S)", fake_muons=2)
    Bs = make_inclusive_cut_dilepton_plus_tracks(
        dimuons, pvs, DecayDescriptor="[B_s0 -> J/psi(1S) pi+ pi-]cc", n_tracks=2
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter(require_GEC=True) + [dimuons, Bs],
        prescale=prescale,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        persistreco=persistreco,
    )
