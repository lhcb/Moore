###############################################################################
# (c) Copyright 2022-24 CERN for the benefit of the LHCb Collaboration        #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Builders for cut-based inclusive dilepton selection lines. Each dilepton builder has keyword arguments for same-sign and fake variants.

Author: Jamie Gooding (jamie.gooding@cern.ch)
Date: 31.01.2024
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.rdbuilder_thor import make_rd_detached_pions
from RecoConf.standard_particles import (
    make_detached_dielectron_with_brem,
    make_detached_mue_with_brem,
    make_detached_mumu,
)

_single_fake_prescale = 0.002
_double_fake_prescale = 0.0001


@configurable
def make_inclusive_cut_dielectron(
    name="make_inclusive_cut_dielectron_{hash}",
    parent_id="J/psi(1S)",
    # electron cuts
    ipchi2_electron_min=36.0,
    pide_electron_min=2.0,
    pt_electron_min=400.0 * MeV,
    p_electron_min=2.0 * GeV,
    # dielectron cuts
    pt_dielectron_min=0.0 * MeV,
    adocachi2cut_dielectron_max=30.0,
    bpvFDchi2_dielectron_min=64.0,  # <- this is bpvvdchi2_min in the MVA-based lines
    vchi2pdof_dielectron_max=20.0,  # <- this is vfaspfchi2ndof_max in the MVA-based lines
    m_dielectron_min=0.0 * MeV,
    m_dielectron_max=1000.0 * GeV,
    # line categories
    same_sign: bool = False,
    fake_electrons: int = 0,
):
    """
    Make the detached dielectron from opposite/same-sign electrons, accounting for Bremsstrahlung, with inclusive cuts.
    """

    dielectrons = make_detached_dielectron_with_brem(
        dielectron_ID=parent_id,
        # electron cuts
        minipchi2=ipchi2_electron_min,
        PIDe_min=pide_electron_min,
        pt_e=pt_electron_min,
        p_e=p_electron_min,
        # dielectron cuts
        pt_diE=pt_dielectron_min,
        adocachi2cut=adocachi2cut_dielectron_max,  # IS APPLIED AS A MAX
        bpvvdchi2=bpvFDchi2_dielectron_min,
        vfaspfchi2ndof=vchi2pdof_dielectron_max,
        m_diE_min=m_dielectron_min,
        m_diE_max=m_dielectron_max,
        # line categories
        opposite_sign=not (same_sign),
        fake_electrons=fake_electrons,
    )
    code = F.require_all(in_range(m_dielectron_min, F.MASS, m_dielectron_max))

    return ParticleFilter(dielectrons, F.FILTER(code), name=name)


@configurable
def make_inclusive_cut_electronmuon(
    name="make_inclusive_cut_electronmuon_{hash}",
    parent_id="J/psi(1S)",
    # lepton cuts
    ipchi2_lepton_min=36.0,
    pide_electron_min=2.0,
    pidmu_muon_min=0.0,
    ismuon_muon=True,
    pt_lepton_min=400.0 * MeV,
    p_lepton_min=2.0 * GeV,
    # electronmuon cuts
    pt_electronmuon_min=0.0 * MeV,
    adocachi2cut_electronmuon_max=30.0,
    bpvFDchi2_electronmuon_min=64.0,
    vchi2pdof_electronmuon_max=20.0,
    m_electronmuon_min=0.0 * MeV,
    m_electronmuon_max=1000.0 * GeV,
    # line categories
    same_sign: bool = False,
    fake_electrons: int = 0,
    fake_muons: int = 0,
):
    """
    Make the detached electron-muon pair from opposite/same-sign electrons/muons, accounting for Bremsstrahlung, with inclusive cuts.
    """

    electronmuons = make_detached_mue_with_brem(
        dilepton_ID=parent_id,
        # lepton cuts
        minipchi2_track=ipchi2_lepton_min,
        min_PIDe=pide_electron_min,
        min_PIDmu=pidmu_muon_min,
        IsMuon=ismuon_muon,
        min_pt_e=pt_lepton_min,
        min_pt_mu=pt_lepton_min,
        # dilepton cuts
        max_adocachi2=adocachi2cut_electronmuon_max,
        min_bpvvdchi2=bpvFDchi2_electronmuon_min,
        max_vchi2ndof=vchi2pdof_electronmuon_max,
        # line categories
        opposite_sign=not (same_sign),
        fake_electrons=fake_electrons,
        fake_muons=fake_muons,
    )
    code = F.require_all(
        in_range(m_electronmuon_min, F.MASS, m_electronmuon_max),
        F.PT > pt_electronmuon_min,
    )

    return ParticleFilter(electronmuons, F.FILTER(code), name=name)


@configurable
def make_inclusive_cut_dimuon(
    name="make_inclusive_cut_dimuon_{hash}",
    parent_id="J/psi(1S)",
    # muon cuts
    ipchi2_muon_min=36.0,
    pidmu_muon_min=0.0,
    ismuon_muon=True,
    pt_muon_min=400.0 * MeV,
    p_muon_min=2.0 * GeV,
    # dimuon cuts
    pt_dimuon_min=0.0 * MeV,
    adocachi2cut_dimuon_max=30.0,
    bpvFDchi2_dimuon_min=64.0,
    vchi2pdof_dimuon_max=20.0,
    m_dimuon_min=0.0 * MeV,
    m_dimuon_max=1000.0 * GeV,
    # line categories
    same_sign: bool = False,
    fake_muons: int = 0,
):
    """
    Make the detached dimuon from opposite/same-sign muons with inclusive cuts.
    """

    dimuons = make_detached_mumu(
        dilepton_ID=parent_id,
        # muon cuts
        minipchi2=ipchi2_muon_min,
        pid_mu=pidmu_muon_min,
        IsMuon=ismuon_muon,
        pt_mu=pt_muon_min,
        p_mu=p_muon_min,
        # dimuon cuts
        adocachi2cut=adocachi2cut_dimuon_max,
        bpvvdchi2=bpvFDchi2_dimuon_min,
        vfaspfchi2ndof=vchi2pdof_dimuon_max,
        # line categories
        opposite_sign=not (same_sign),
        fake_muons=fake_muons,
    )
    code = F.require_all(
        in_range(m_dimuon_min, F.MASS, m_dimuon_max), F.PT > pt_dimuon_min
    )

    return ParticleFilter(dimuons, F.FILTER(code), name=name)


@configurable
def make_inclusive_cut_dilepton_plus_tracks(
    dileptons,
    pvs,
    DecayDescriptor,
    name="make_inclusive_cut_dilepton_plustracks_{hash}",
    n_tracks=1,
    # track cuts
    p_track_min=2.0 * GeV,
    pt_track_min=400.0 * MeV,
    ipchi2_track_min=36.0,
    # b cuts
    pt_B_min=0.0 * MeV,
    vchi2pdof_B_max=20.0,
    bpvFDchi2_B_min=64.0,
):
    """
    Make a candidate from a dilepton (see builders above) object and n many tracks (e.g. for 3 and 4 body dilepton decays).
    """

    assert n_tracks > 0, ValueError(
        "Constructing inclusive dilepton candidate with additional tracks requires n_tracks > 0; if trying to create 2-body candidate, use the individual dilepton builders."
    )
    tracks = make_rd_detached_pions(
        name="make_inclusive_cut_dilepton_tracks_{hash}",
        p_min=p_track_min,
        pt_min=pt_track_min,
        mipchi2dvprimary_min=ipchi2_track_min,
        pid=None,
    )

    combination_code = F.require_all(F.SUM(F.PT) > pt_B_min)

    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_B_max, F.BPVFDCHI2(pvs) > bpvFDchi2_B_min
    )

    particles = [dileptons] + [tracks] * n_tracks

    return ParticleCombiner(
        particles,
        DecayDescriptor=DecayDescriptor,
        CombinationCut=combination_code,
        name=name,
        CompositeCut=vertex_code,
    )
