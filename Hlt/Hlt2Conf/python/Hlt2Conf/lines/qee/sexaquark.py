###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Set of HLT2 lines to search for longlived Sexaquarks with detached annihilation vertices.
- S~ p+ -> Lambda~0 K+ (LLl, DDl, DDd)
- S~ n0  -> Lambda~0 KS0 (LLLL, LLDD, DDLL, DDDD)
"""

import Functors as F
import Functors.math as fmath
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, mm
from GaudiKernel.SystemOfUnits import picosecond as ps
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.algorithms_thor import (
    ParticleCombiner,
    ParticleContainersMerger,
    ParticleFilter,
)
from RecoConf.event_filters import require_pvs
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    _make_V0LL,
    make_down_pions_for_V0,
    make_has_rich_down_kaons,
    make_has_rich_long_kaons,
    make_KsDD,
    make_LambdaDD,
    make_long_pions_for_V0,
    make_long_protons_for_V0,
    masses,
)

from Hlt2Conf.isolation import extra_outputs_for_isolation

all_lines = {}

#########################
### template builders ###
#########################


@configurable
def make_displaced_long_kaon():
    code = F.require_all(F.OWNPVIPCHI2 > 20000, F.PID_K > 3)
    return ParticleFilter(make_has_rich_long_kaons(), F.FILTER(code))


@configurable
def make_displaced_down_kaon():
    code = F.require_all(F.OWNPVIPCHI2 > 100, F.PID_K > 3)
    return ParticleFilter(make_has_rich_down_kaons(), F.FILTER(code))


@configurable
def make_V0LambdaLL():
    """Redefined to avoid OWNPVLTIME cut as fit fails too often for displaced particles."""
    pions = make_long_pions_for_V0()
    protons = make_long_protons_for_V0()
    descriptors = "[Lambda0 -> p+ pi-]cc"
    return _make_V0LL(
        particles=[protons, pions],
        descriptors=descriptors,
        nominal_mass=masses["Lambda0"],
        m_dmass=12 * MeV,
        vchi2pdof_max=9,
        bpvltime_min=None,
    )


@configurable
def make_displaced_LambdaLL():
    code = F.require_all(F.OWNPVIPCHI2 > 20000, F.OWNPVDIRA > 0)
    return ParticleFilter(
        make_V0LambdaLL(), F.FILTER(code), name="qee_make_displaced_LambdaLL_{hash}"
    )


@configurable
def make_displaced_LambdaDD():
    code = F.require_all(
        F.CHI2DOF < 9,
        F.OWNPVIPCHI2 > 100,
        F.OWNPVDIRA > 0,
        in_range(masses["Lambda0"] - 12 * MeV, F.MASS, masses["Lambda0"] + 12 * MeV),
    )
    return ParticleFilter(
        make_LambdaDD(), F.FILTER(code), name="qee_make_displaced_LambdaDD_{hash}"
    )


@configurable
def make_V0KsLL():
    """Redefined to avoid OWNPVLTIME cut as fit fails too often for displaced particles."""
    pions = make_long_pions_for_V0()
    descriptors = "KS0 -> pi+ pi-"
    return _make_V0LL(
        particles=[pions, pions],
        descriptors=descriptors,
        m_dmass=36 * MeV,
        vchi2pdof_max=9,
        bpvltime_min=None,
    )


@configurable
def make_displaced_KsLL():
    code = F.require_all(F.OWNPVIPCHI2 > 20000, F.OWNPVDIRA > 0)
    return ParticleFilter(
        make_V0KsLL(), F.FILTER(code), name="qee_make_displaced_KsLL_{hash}"
    )


@configurable
def make_displaced_KsDD():
    code = F.require_all(
        F.CHI2DOF < 9,
        F.OWNPVIPCHI2 > 100,
        F.OWNPVDIRA > 0,
        in_range(masses["KS0"] - 36 * MeV, F.MASS, masses["KS0"] + 36 * MeV),
    )
    return ParticleFilter(
        make_KsDD(), F.FILTER(code), name="qee_make_displaced_KsDD_{hash}"
    )


def _DZ_CHILD(i):  # i starts at 1 denoting 1st daughter
    return F.CHILD(i, F.END_VZ) - F.END_VZ


@configurable
def make_sexaquark(
    name,
    descriptor,
    particles,
    maxsdoca_cut=10 * mm,
    extra_cuts=None,
):
    combination_cut = F.require_all(F.MAXSDOCA < maxsdoca_cut)
    composite_cut = F.require_all(
        F.END_VRHO > 3 * mm,
        F.CHI2DOF < 9,
        F.OWNPVDIRA > 0,
        F.OWNPVLTIME > 10 * ps,
        _DZ_CHILD(1) > 2 * mm,
    )
    if descriptor == "[n0 -> Lambda~0 KS0]cc":
        composite_cut &= _DZ_CHILD(2) > 2 * mm

    if extra_cuts is not None:
        composite_cut &= extra_cuts

    return ParticleCombiner(
        Inputs=particles,
        DecayDescriptor=descriptor,
        name=name,
        AllowDiffInputsForSameIDChildren=True,
        CombinationCut=combination_cut,
        CompositeCut=composite_cut,
    )


#######################
### Sexaquark maker ###
#######################


### Proton interaction
@configurable
def make_S_LLl():
    return make_sexaquark(
        name="qee_make_S_to_L0Kp_LLl_{hash}",
        descriptor="[p+ -> Lambda~0 K+]cc",
        particles=[make_displaced_LambdaLL(), make_displaced_long_kaon()],
        maxsdoca_cut=0.1 * mm,
        extra_cuts=F.OWNPVDLS > 25,
    )


@configurable
def make_S_DDl():
    return make_sexaquark(
        name="qee_make_S_to_L0Kp_DDl_{hash}",
        descriptor="[p+ -> Lambda~0 K+]cc",
        particles=[make_displaced_LambdaDD(), make_displaced_long_kaon()],
        maxsdoca_cut=3 * mm,
    )


@configurable
def make_S_DDd():
    return make_sexaquark(
        name="qee_make_S_to_L0Kp_DDd_{hash}",
        descriptor="[p+ -> Lambda~0 K+]cc",
        particles=[make_displaced_LambdaDD(), make_displaced_down_kaon()],
        maxsdoca_cut=5 * mm,
        extra_cuts=F.OWNPVDLS > 15,
    )


### Neutron interaction
@configurable
def make_S_LLLL():
    return make_sexaquark(
        name="qee_make_S_to_L0Ks_LLLL_{hash}",
        descriptor="[n0 -> Lambda~0 KS0]cc",
        particles=[make_displaced_LambdaLL(), make_displaced_KsLL()],
        maxsdoca_cut=0.1 * mm,
        extra_cuts=F.OWNPVDLS > 25,
    )


@configurable
def make_S_LLDD():
    return make_sexaquark(
        name="qee_make_S_to_L0Ks_LLDD_{hash}",
        descriptor="[n0 -> Lambda~0 KS0]cc",
        particles=[make_displaced_LambdaLL(), make_displaced_KsDD()],
        maxsdoca_cut=3 * mm,
    )


@configurable
def make_S_DDLL():
    return make_sexaquark(
        name="qee_make_S_to_L0Ks_DDLL_{hash}",
        descriptor="[n0 -> Lambda~0 KS0]cc",
        particles=[make_displaced_LambdaDD(), make_displaced_KsLL()],
        maxsdoca_cut=3 * mm,
    )


@configurable
def make_S_DDDD():
    return make_sexaquark(
        name="qee_make_S_to_L0Ks_DDDD_{hash}",
        descriptor="[n0 -> Lambda~0 KS0]cc",
        particles=[make_displaced_LambdaDD(), make_displaced_KsDD()],
        maxsdoca_cut=3 * mm,
        extra_cuts=F.OWNPVDLS > 10,
    )


###################
### Extra pions ###
###################

import math

LOG10_E = math.log10(math.e)


@configurable
def make_two_body_combination(
    candidate, extra_particles, tes_name, use_fiducial_cut: bool = True
):
    """
    Based on rd_isolation.make_two_body_combination()
    Make fake two body combination of S and extra pions.

    Args:
    candidate: Containers of reference particles
    tes_name: String for the TES location
    use_fiducial_cut(bool): Use preselection requirement for the combination (default value True)

    Returns: TES location of two body combination of candidate and extra_particles
    """

    ## Set default values
    pv_dist_min = 0.0  # mm
    # cone_min = 0.25  #0.5**2
    log_ipchi2_wrtSV_max = 9  # 5

    fiducial_cut_pv_dist = F.ALL
    fiducial_cut_cone = F.ALL
    fiducial_cut_sv_ipchi2 = F.ALL

    if use_fiducial_cut:
        # displaced in z
        fiducial_cut_pv_dist = (
            F.MAGNITUDE @ (F.ENDVERTEX_POS - F.OWNPV_POS)
        ) * fmath.sign(F.END_VZ - F.OWNPVZ) > pv_dist_min
        # outside cone
        # DETA = F.CHILD(1, F.ETA) - F.CHILD(2, F.ETA)
        # DPHI = F.ADJUST_ANGLE @ (F.CHILD(1, F.PHI) - F.CHILD(2, F.PHI))
        # fiducial_cut_cone = (DETA * DETA + DPHI * DPHI) > cone_min
        # check wether S and pion tracks match
        fiducial_cut_sv_ipchi2 = (
            fmath.log(F.IPCHI2.bind(F.CHILD(1, F.ENDVERTEX), F.CHILD(2, F.FORWARDARGS)))
            * LOG10_E
            < log_ipchi2_wrtSV_max
        )

    # first combiner: Proton interaction with negative pions
    comb_1 = ParticleCombiner(
        Inputs=[candidate, extra_particles],
        name="add_proton_pions_neg_{hash}",
        DecayDescriptor="[D0 -> p+ pi-]cc",
        CombinationCut=F.require_all(fiducial_cut_cone, fiducial_cut_sv_ipchi2),
        CompositeCut=fiducial_cut_pv_dist,
    )
    # second combiner: Proton interaction with positive pions
    comb_2 = ParticleCombiner(
        Inputs=[candidate, extra_particles],
        name="add_proton_pions_pos_{hash}",
        DecayDescriptor="[Delta++ -> p+ pi+]cc",
        CombinationCut=F.require_all(fiducial_cut_cone, fiducial_cut_sv_ipchi2),
        CompositeCut=fiducial_cut_pv_dist,
    )
    # third combiner: Neutron interaction
    comb_3 = ParticleCombiner(
        Inputs=[candidate, extra_particles],
        name="add_neutron_pions_{hash}",
        DecayDescriptor="[D+ -> n0 pi+]cc",
        CombinationCut=F.require_all(fiducial_cut_cone, fiducial_cut_sv_ipchi2),
        CompositeCut=fiducial_cut_pv_dist,
    )
    # merge combiners
    comb = ParticleContainersMerger([comb_1, comb_2, comb_3], name=tes_name + "_{hash}")

    return comb


@configurable
def select_combinations_for_vertex_isolation(
    name, candidate, extra_particles, max_two_body_vtx_cut: float = 9.0
):
    """
    Based on rd_isolation.select_combinations_for_vertex_isolation()
    Add to the extra_outputs vertex isolations when adding one track to the vertex fit.
    By default only two-body combinations with vertex fit chi2 smaller than 9. are selected.
    Returns TES location with two-body combinations for vertex isolation

    Args:
        name: String with prefix of the TES location for extra selection
        candidate: Container of reference particles to relate extra particles
        max_two_body_vtx_cut(float): Set the maximum value of the 2-body vertex fit chi2 (default value 9.)
    """
    return extra_outputs_for_isolation(
        name=name + "_OneTrackCombination_VertexIsolation",
        extra_particles=make_two_body_combination(
            candidate, extra_particles, "Sexaquark_VertexIsolation_Pions"
        ),
        ref_particles=candidate,
        selection=(
            F.ABS @ (F.CHI2DOF() @ F.FORWARDARG1() - F.CHI2DOF() @ F.FORWARDARG0())
            < max_two_body_vtx_cut
        ),
    )  # CHI2DOF instead of CHI2


@configurable
def extra_long_pions(sexaquark):
    return select_combinations_for_vertex_isolation(
        name="S_long",
        candidate=sexaquark,
        extra_particles=make_long_pions_for_V0(),
        max_two_body_vtx_cut=9.0,
    )


@configurable
def extra_down_pions(sexaquark):
    return select_combinations_for_vertex_isolation(
        name="S_down",
        candidate=sexaquark,
        extra_particles=make_down_pions_for_V0(),
        max_two_body_vtx_cut=9.0,
    )


@configurable
def extra_pions(sexaquark):
    pions = extra_long_pions(sexaquark)
    pions += extra_down_pions(sexaquark)
    return pions


#####################
### Line builders ###
#####################


### Proton interaction
@register_line_builder(all_lines)
def SpToL0Kp_LLl_line(name="Hlt2QEE_SexaquarkProtonToLambda0Kp_LLl", prescale=1.0):
    alg = make_S_LLl()
    return Hlt2Line(
        name=name,
        algs=[require_pvs(make_pvs())] + [alg],
        prescale=prescale,
        extra_outputs=extra_long_pions(alg),
    )


@register_line_builder(all_lines)
def SpToL0Kp_DDl_line(name="Hlt2QEE_SexaquarkProtonToLambda0Kp_DDl", prescale=1.0):
    alg = make_S_DDl()
    return Hlt2Line(
        name=name,
        algs=[require_pvs(make_pvs())] + [alg],
        prescale=prescale,
        extra_outputs=extra_long_pions(alg),
    )


@register_line_builder(all_lines)
def SpToL0Kp_DDd_line(name="Hlt2QEE_SexaquarkProtonToLambda0Kp_DDd", prescale=0.1):
    alg = make_S_DDd()
    return Hlt2Line(
        name=name,
        algs=[require_pvs(make_pvs())] + [alg],
        prescale=prescale,
        extra_outputs=extra_down_pions(alg),
    )


### Neutron interaction
@register_line_builder(all_lines)
def SnToL0Ks_LLLL_line(name="Hlt2QEE_SexaquarkNeutronToLambda0KS_LLLL", prescale=1.0):
    alg = make_S_LLLL()
    return Hlt2Line(
        name=name,
        algs=[require_pvs(make_pvs())] + [alg],
        prescale=prescale,
        extra_outputs=extra_long_pions(alg),
    )


@register_line_builder(all_lines)
def SnToL0Ks_LLDD_line(name="Hlt2QEE_SexaquarkNeutronToLambda0KS_LLDD", prescale=1.0):
    alg = make_S_LLDD()
    return Hlt2Line(
        name=name,
        algs=[require_pvs(make_pvs())] + [alg],
        prescale=prescale,
        extra_outputs=extra_long_pions(alg),
    )


@register_line_builder(all_lines)
def SnToL0Ks_DDLL_line(name="Hlt2QEE_SexaquarkNeutronToLambda0KS_DDLL", prescale=1.0):
    alg = make_S_DDLL()
    return Hlt2Line(
        name=name,
        algs=[require_pvs(make_pvs())] + [alg],
        prescale=prescale,
        extra_outputs=extra_long_pions(alg),
    )


@register_line_builder(all_lines)
def SnToL0Ks_DDDD_line(name="Hlt2QEE_SexaquarkNeutronToLambda0KS_DDDD", prescale=1.0):
    alg = make_S_DDDD()
    return Hlt2Line(
        name=name,
        algs=[require_pvs(make_pvs())] + [alg],
        prescale=prescale,
        extra_outputs=extra_pions(alg),
    )
