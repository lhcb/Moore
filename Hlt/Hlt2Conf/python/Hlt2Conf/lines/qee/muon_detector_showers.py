###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Inclusive lines that run a BDT selection on the number of hits in different muon stations
"""

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from PyConf.Algorithms import DeterministicPrescaler
from PyConf.application import make_odin
from PyConf.control_flow import CompositeNode, NodeLogic
from RecoConf.muonid import make_muon_hits
from SelAlgorithms import monitoring

all_lines = {}


# monitor the MVA output
def mds_BDT_Monitoring(mva, prescale=0.1):
    prescaler = DeterministicPrescaler(
        name="Monitor_MDS_MVAPrescaler",
        AcceptFraction=(1.0 - prescale),
        SeedName="Monitor_MDS-MVAPrescaler",
        ODINLocation=make_odin(),
    )
    name = "Hlt2QEE_MDS_BDT_nHits"
    histogram_BDT_predictions = monitoring.histogram_1d(
        "Monitoring" + name + "_MVA_output",
        "Prediction of the BDT for MDS",
        mva,
        bins=500,
        range=(-1, 1),
        label="BDT prediction",
    )
    global_monitor = monitoring.monitor(histograms=[histogram_BDT_predictions])
    return CompositeNode(
        "mds_BDT_Monitoring",
        [prescaler, global_monitor],
        combine_logic=NodeLogic.LAZY_OR,
        force_order=True,
    )


@register_line_builder(all_lines)
@configurable
def mds_BDT_nHits_line(
    name="Hlt2QEE_MDS_BDT_nHits", MVACut=0.997, prescale=0.01, persistreco=False
):
    import Functors as F
    from PyConf.Algorithms import VoidFilter

    muon_hits = make_muon_hits()
    # stations M3, M4, and M5
    inputs = {
        f"M{station + 2}_region_{region}": F.NHITSINMUON(
            muon_hits, station=station, region=region
        )
        for station in [1, 2, 3]
        for region in [0, 1, 2, 3]
    }
    mva = F.MVA(
        MVAType="TMVA",
        Config={
            "XMLFile": "paramfile://data/BDT_nHits_MDS.xml",
            "Name": "BDT_nHits_qee_MDS",
        },
        Inputs=inputs,
    )

    from RecoConf.standard_particles import make_long_muons

    long_muons = make_long_muons()
    return Hlt2Line(
        name=name,
        algs=[
            muon_hits,
            long_muons,
            mds_BDT_Monitoring(mva, prescale=0.1),
            VoidFilter(name="nHits_BDT_filter", Cut=mva > MVACut),
        ],
        prescale=prescale,
        persistreco=persistreco,
        extra_outputs=[("MuonLongTracks", long_muons)],
        raw_banks=["Muon"],
        monitoring_variables=(),
    )
