###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Registration of all the QEE HLT2 lines for the 2024 pp reference run.
"""

from Hlt2Conf.lines.qee import (
    diphoton,
    drellyan_pp_ref_2024,
    high_mass_dielec,
    high_mass_dimuon,
    jets,
    quarkonia,
    single_high_pt_electron,
    single_high_pt_muon,
)

full_lines = {}
full_lines.update(single_high_pt_muon.all_lines)
full_lines.update(single_high_pt_electron.all_lines)
full_lines.update(jets.all_reference_run_lines)
full_lines.update(high_mass_dimuon.all_lines)
full_lines.update(high_mass_dielec.all_lines)

turbo_lines = {}
turbo_lines.update(quarkonia.all_lines)
turbo_lines.update(diphoton.all_lines)
turbo_lines.update(drellyan_pp_ref_2024.all_lines)
