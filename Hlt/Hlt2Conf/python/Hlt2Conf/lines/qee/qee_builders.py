###############################################################################
# Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration          #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of useful QEE filters and builders
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, degree, meter
from PyConf import configurable
from RecoConf import rdbuilder_thor
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.event_filters import filter_max_pvs
from RecoConf.rdbuilder_thor import make_rd_detached_kaons
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    make_down_electrons_no_brem,
    make_has_rich_long_pions,
    make_ismuon_long_muon,
    make_long_electrons_with_brem,
    make_long_muons,
    make_mva_ttrack_kaons,
    make_mva_ttrack_muons,
    make_mva_ttrack_pions,
    make_photons,
)
from RecoConf.ttrack_selections_reco import PVF_with_single_extrapolation

from Hlt2Conf.lines.qee.high_mass_dielec import make_dielec_novxt
from Hlt2Conf.lines.qee.high_mass_dimuon import make_dimuon_novxt
from Hlt2Conf.standard_jets import make_jets


@configurable
def make_filter_tracks(
    make_particles=make_has_rich_long_pions,
    make_pvs=make_pvs,
    name="qee_has_rich_long_pions",
    pt_min=0.25 * GeV,
    p_min=2.0 * GeV,
    trchi2dof_max=3,  # TBC with Reco
    trghostprob_max=0.4,  # TBC with Reco
    mipchi2dvprimary_min=None,
    pid=None,
    additionalCuts=None,
):
    """
    Build generic long tracks.
    """
    code = F.require_all(
        F.SIZE(make_pvs()) > 0,
        F.PT > pt_min,
        F.P > p_min,
    )
    if pid is not None:
        code &= pid
    if additionalCuts is not None:
        code &= additionalCuts
    if mipchi2dvprimary_min is not None:
        pvs = make_pvs()
        code &= F.MINIPCHI2(pvs) > mipchi2dvprimary_min

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def muon_filter(min_pt=0.0 * GeV, require_muID=True):
    # A muon filter: PT

    code = F.PT > min_pt
    particles = make_ismuon_long_muon() if require_muID else make_long_muons()

    return ParticleFilter(particles, F.FILTER(code))


@configurable
def elec_filter(min_pt=0.0 * GeV, min_electron_id=-1):
    # An electron filter: PT
    code = F.require_all(F.PT > min_pt, F.PID_E > min_electron_id)
    particles = make_long_electrons_with_brem()

    return ParticleFilter(particles, F.FILTER(code))


@configurable
def elec_filter_down(min_pt=0.0 * GeV, min_electron_id=-1):
    # An down type electron filter: PT

    code = F.require_all(F.PT > min_pt, F.PID_E > min_electron_id)
    particles = make_down_electrons_no_brem()

    return ParticleFilter(particles, F.FILTER(code))


@configurable
def make_qee_photons(name="qee_photons", make_particles=make_photons, pt_min=5.0 * GeV):
    code = F.PT > pt_min
    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_qee_gamma_DD(
    name="qee_gamma_DD",
    descriptor="gamma -> e+ e-",
    am_max=0.5 * GeV,
    m_max=0.1 * GeV,
    pt_min=5.0 * GeV,
    min_elec_pt=0.0 * GeV,
    min_elec_id=0.0,
    maxVertexChi2=16,
):
    electrons = elec_filter_down(min_pt=min_elec_pt, min_electron_id=min_elec_id)
    combination_code = F.MASS < am_max
    vertex_code = F.require_all(F.PT > pt_min, F.MASS < am_max, F.CHI2 < maxVertexChi2)

    return ParticleCombiner(
        name=name,
        Inputs=[electrons, electrons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_qee_gamma_LL(
    name="qee_gamma_LL",
    descriptor="gamma -> e+ e-",
    am_max=0.5 * GeV,
    m_max=0.1 * GeV,
    pt_min=5.0 * GeV,
    min_elec_pt=0.0 * GeV,
    min_elec_id=0.0,
    maxVertexChi2=16,
):
    electrons = elec_filter(min_pt=min_elec_pt, min_electron_id=min_elec_id)
    combination_code = F.MASS < am_max
    vertex_code = F.require_all(F.PT > pt_min, F.MASS < am_max, F.CHI2 < maxVertexChi2)

    return ParticleCombiner(
        name=name,
        Inputs=[electrons, electrons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_photons(photon_type="DD", pt_min=10.0 * GeV):
    if photon_type == "LL":
        gamma = make_qee_gamma_LL()
    elif photon_type == "DD":
        gamma = make_qee_gamma_DD()
    else:
        gamma = make_qee_photons()

    code = F.require_all(F.PT > pt_min)

    return ParticleFilter(gamma, F.FILTER(code))


@configurable
def make_gamma_jet(
    photon_type="DD",
    descriptor="Z0 -> gamma CELLjet",
    pt_jet_min=15 * GeV,
    pt_gamma_min=5 * GeV,
    min_dphi=90 * degree,
):
    jets = make_jets(pt_min=pt_jet_min)
    photons = make_photons(photon_type=photon_type, pt_min=pt_gamma_min)

    if min_dphi > 0:
        delta = F.ADJUST_ANGLE @ (F.CHILD(1, F.PHI) - F.CHILD(2, F.PHI))
        combination_code = F.require_all(
            F.ABS @ delta > min_dphi, F.CHILD(1, F.OWNPV) == F.CHILD(2, F.OWNPV)
        )
    else:
        combination_code = F.require_all(F.CHILD(1, F.OWNPV) == F.CHILD(2, F.OWNPV))

    return ParticleCombiner(
        Inputs=[photons, jets],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=F.ALL,
        ParticleCombiner="ParticleAdder",
    )


@configurable
def lepton_filter(lepton_type="mu", min_pt=10.0 * GeV):
    if lepton_type == "mu":
        lepts = muon_filter(min_pt=min_pt)
    else:
        lepts = elec_filter(min_pt=min_pt)

    return lepts


@configurable
def Z_To_lepts_filter(lepton_type="mu", min_pt=10.0 * GeV):
    if lepton_type == "mu":
        muons_for_Z = muon_filter(min_pt=min_pt)
        zcand = make_dimuon_novxt(
            input_muon1=muons_for_Z,
            input_muon2=muons_for_Z,
            decaydescriptor="Z0 -> mu+ mu-",
            min_combination_mass=30.0 * GeV,
            min_composite_mass=40.0 * GeV,
        )
    else:
        electrons_for_Z = elec_filter(min_pt=min_pt)
        zcand = make_dielec_novxt(
            input_elecs=electrons_for_Z,
            decaydescriptor="Z0 -> e+ e-",
            min_combination_mass=30.0 * GeV,
            min_composite_mass=40.0 * GeV,
        )

    return zcand


# For HNL lines


@configurable
def filter_neutral_hadrons(
    particles,
    pvs,
    pt_min=0.5 * GeV,
    ipchi2_min=0.0,
    name="qee_rad_incl_neutral_hadrons_{hash}_{hash}",
):
    """Returns extra neutral hadrons with the inclusive_radiative_b selections"""
    code = F.require_all(
        F.PT > pt_min,
        F.MINIPCHI2CUT(IPChi2Cut=ipchi2_min, Vertices=pvs),
    )
    return ParticleFilter(particles, F.FILTER(code), name=name)


def hnl_prefilter(require_GEC=False):
    from RecoConf.event_filters import require_gec, require_pvs
    from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction

    """
    Args:
        require_GEC (bool, optional): require the General Event Cut.
    """
    gec = [require_gec()] if require_GEC else []
    return upfront_reconstruction() + gec + [require_pvs(make_pvs())]


@configurable
def make_majorana_lepton(
    leptons,
    pvs,
    name="standard_lepton_for_majorana",
    pt_min=0.5 * GeV,
    p_min=0 * GeV,
    mipchi2dvprimary_min=25.0,
    pid=None,
):
    """
    Filter default leptons for HNL
    """
    return make_filter_tracks(
        make_particles=leptons,
        make_pvs=pvs,
        name=name,
        pt_min=pt_min,
        p_min=p_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid,
    )


@configurable
def make_majorana(
    child1,
    child2,
    name="Generic_Majorana",
    descriptor="",
    am_min=0.2 * GeV,
    am_max=7.0 * GeV,
    adocachi2=16.0,
    pt_min=0.7 * GeV,
    vtxchi2_max=9.0,
):
    """
    Make HNL -> lep +  pi.
    """
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.MAXSDOCACHI2CUT(adocachi2)
    )
    majorana_code = F.require_all(F.PT > pt_min, F.CHI2 < vtxchi2_max)

    return ParticleCombiner(
        [child1, child2],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=majorana_code,
    )


@configurable
def make_bhadron_majorana(
    majoranas,
    bachelor,
    pvs,
    name="Generic_B_2_Majorana",
    descriptor="",
    am_min=4.3 * GeV,
    am_max=7.2 * GeV,
    m_min=4.5 * GeV,
    m_max=6.8 * GeV,
    adocachi2=25.0,
    vtxchi2_max=9.0,
    mipchi2_max=16.0,
    ownpvdls_min=4.0,
    dira_min=0.0,
):
    """
    Make B-> lep + HNL
    """
    # majoranas = make_majorana()
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max), F.MAXSDOCACHI2CUT(adocachi2)
    )
    b_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.OWNPVDLS > ownpvdls_min,
        F.CHI2 < vtxchi2_max,
        F.OWNPVDIRA > dira_min,
        F.MINIPCHI2(pvs()) < mipchi2_max,
    )
    return ParticleCombiner(
        [majoranas, bachelor],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=b_code,
    )


@configurable
def make_long_lepton_forHNL(
    make_long,
    name="long_lepton_forHNL_{hash}",
    pt_min=500.0 * MeV,
    p_min=3.0 * GeV,
    ipchi2_min=25.0,
    pid=None,
):
    return make_filter_tracks(
        make_particles=make_long,
        name=name,
        pt_min=pt_min,
        p_min=p_min,
        mipchi2dvprimary_min=ipchi2_min,
        pid=pid,
    )


@configurable
def make_ttrack_forHNL(
    make_ttrack,
    name="filter_Ttrack_forHLN_{hash}",
    pt_min=750.0 * MeV,
    p_min=5.0 * GeV,
    ipchi2_min=500.0,
    pid=None,
):
    return make_filter_tracks(
        make_particles=make_ttrack,
        name=name,
        pt_min=pt_min,
        p_min=p_min,
        mipchi2dvprimary_min=ipchi2_min,
        pid=pid,
    )


@configurable
def make_HNL_TT(
    pions,
    leptons,
    name="Generic_HNL_TT",
    descriptor="",
    twobody_yz_intersection=1.5 * meter,
    adocachi2max=300.0,
    vtx_zmin=2.0 * meter,
    vtx_zmax=8.0 * meter,
    vtxchi2_max=10.0,
    mass_min=1.5 * GeV,
    mass_max=6.5 * GeV,
    pt_min=1.0 * GeV,
    p_min=30.0 * GeV,
):
    """
    Make HNL -> pi + lep (with TT combiner)
    """
    combination_cut = F.require_all(
        F.TWOBODY_YZ_INTERSECTION_Z > twobody_yz_intersection,
        F.MAXSDOCACHI2CUT(adocachi2max),
    )

    vertex_cut = F.require_all(
        F.math.in_range(vtx_zmin, F.END_VZ, vtx_zmax),
        F.CHI2 < vtxchi2_max,
        F.math.in_range(mass_min, F.MASS, mass_max),
        F.PT > pt_min,
        F.P > p_min,
    )
    return ParticleCombiner(
        Inputs=[leptons, pions],
        DecayDescriptor=descriptor,
        CombinationCut=combination_cut,
        CompositeCut=vertex_cut,
        ParticleCombiner=PVF_with_single_extrapolation(),
        name=name,
    )


@configurable
def make_BToLHNL_TT(
    long_leptons,
    hnl_tt,
    name="Generic_B_LHNL",
    descriptor="",
    amass_min=4.0 * GeV,
    amass_max=6.5 * GeV,
    mass_min=4.2 * GeV,
    mass_max=6.3 * GeV,
    z_vtx_max=1.0 * meter,
    pt_min=1.5 * GeV,
):
    """
    Make B-> lep + HNL (with TT combiner)
    """
    combination_cut = F.require_all(F.math.in_range(amass_min, F.MASS, amass_max))
    vertex_cut = F.require_all(
        F.END_VZ < z_vtx_max, F.math.in_range(mass_min, F.MASS, mass_max), F.PT > pt_min
    )
    return ParticleCombiner(
        Inputs=[long_leptons, hnl_tt],
        DecayDescriptor=descriptor,
        CombinationCut=combination_cut,
        CompositeCut=vertex_cut,
        ParticleCombiner=PVF_with_single_extrapolation(),
        name=name,
    )


### basic particle filters for Dark Higgs searches
## T tracks
@configurable
def filter_ttrack_muons_for_high_mass(
    pvs,
    make_muons=make_mva_ttrack_muons,
    ghostprob_max=0.6,
    pt_min=750 * MeV,
    p_min=5000 * MeV,
    track_min=10,
    chi2_max=15.0,
    pid_mu_min=-10.0,
    muondll_mu_min=-5,
    muonchi2_max=10.0,
    pid_k_max=10.0,
    pid_p_max=12.0,
    minipchi2_min=25.0,
):
    muons = make_muons()
    filter_code_high_mass = F.require_all(
        F.GHOSTPROB < ghostprob_max,
        (F.MUONLLMU - F.MUONLLBG) > muondll_mu_min,
        F.MUONCHI2CORR < muonchi2_max,
        F.PT > pt_min,
        F.P > p_min,
        F.NFTHITS @ F.TRACK > track_min,
        F.CHI2 < chi2_max,
        F.PID_MU > pid_mu_min,
        F.PID_K < pid_k_max,
        F.PID_P < pid_p_max,
        F.ISMUON,
        F.MINIPCHI2(pvs) > minipchi2_min,
    )
    return ParticleFilter(muons, F.FILTER(filter_code_high_mass))


@configurable
def filter_ttrack_kaons_for_high_mass(
    pvs,
    make_kaons=make_mva_ttrack_kaons,
    ghostprob_max=0.4,
    pt_min=1000 * MeV,
    p_min=10000 * MeV,
    pid_p_max=20.0,
    pid_k_max=25.0,
    pid_k_min=-2.0,
    track_min=10,
    pid_mu_max=1.0,
    pid_mu_min=-5.0,
    minipchi2_min=25.0,
    chi2_max=12.0,
):
    kaons = make_kaons()
    filter_code_high_mass = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.GHOSTPROB < ghostprob_max,
        F.NFTHITS @ F.TRACK > track_min,
        F.math.in_range(pid_k_min, F.PID_K, pid_k_max),
        F.PID_P < pid_p_max,
        F.PID_MU < pid_mu_max,
        F.CHI2 < chi2_max,
        F.PID_MU > pid_mu_min,
        F.MINIPCHI2(pvs) > minipchi2_min,
    )
    return ParticleFilter(kaons, F.FILTER(filter_code_high_mass))


@configurable
def filter_ttrack_pions_for_high_mass(
    pvs,
    make_pions=make_mva_ttrack_pions,
    ghostprob_max=0.4,
    pt_min=1250 * MeV,
    p_min=15000 * MeV,
    pid_p_max=12.0,
    pid_k_max=15.0,
    pid_mu_max=1.0,
    track_min=10,
    minipchi2_min=25.0,
    chi2_max=10.0,
):
    pions = make_pions()
    filter_code_high_mass = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.GHOSTPROB < ghostprob_max,
        F.NFTHITS @ F.TRACK > track_min,
        F.PID_P < pid_p_max,
        F.PID_K < pid_k_max,
        F.PID_MU < pid_mu_max,
        F.CHI2 < chi2_max,
        F.MINIPCHI2(pvs) > minipchi2_min,
    )
    return ParticleFilter(pions, F.FILTER(filter_code_high_mass))


### Dimuon/kaon combiners
@configurable
def qee_ttrack_combiner_highmass(
    filtered_particle1,
    filtered_particle2,
    pvs,
    decay_descriptor,
    yz_intersection_z_min=1500.0,
    yz_intersection_z_max=8000.0,
    maxdoca=400.0,
    maxdocachi2=20000.0,
    vertex_z_max=8 * meter,
    ownpv_dira_min=0.9996,
    max_chi2=20.0,
    ownpv_ip_max=150.0,
    ownpv_ip_chi2_max=200.0,
    ownpv_vdrho_min=80.0,
    mass_min=2000.0,
    mass_max=100000.0,
):
    """
    used for T track combinations with a mass range > 2 GeV
    """
    combination_cut_high_mass = F.require_all(
        F.math.in_range(
            yz_intersection_z_min, F.TWOBODY_YZ_INTERSECTION_Z, yz_intersection_z_max
        ),
        F.MAXSDOCACUT(maxdoca),
        F.MAXSDOCACHI2CUT(maxdocachi2),
    )

    vertex_cut = F.require_all(
        F.END_VZ < vertex_z_max,
        F.OWNPVDIRA > ownpv_dira_min,
        F.CHI2 < max_chi2,
        F.OWNPVIP < ownpv_ip_max,
        F.OWNPVIPCHI2 < ownpv_ip_chi2_max,
        F.OWNPVVDRHO > ownpv_vdrho_min,
        F.math.in_range(mass_min, F.MASS, mass_max),
    )  # TODO: upper limit?

    return ParticleCombiner(
        Inputs=[filtered_particle1, filtered_particle2],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cut_high_mass,
        CompositeCut=vertex_cut,
        ParticleCombiner=PVF_with_single_extrapolation(),
        name="qee_ttrack_combiner_highmass_{hash}",
    )


## Filter on long kaons, to limit multiple candidates
@configurable
def qee_filtered_long_kaons(
    ghostprob_max=0.4, pt_min=300 * MeV, p_min=3500.0 * MeV, chi2_max=60.0
):
    long_kaons = make_rd_detached_kaons()
    filter_multiple_kaons = F.require_all(
        F.GHOSTPROB < ghostprob_max, F.PT > pt_min, F.P > p_min, F.CHI2 < chi2_max
    )
    return ParticleFilter(long_kaons, F.FILTER(filter_multiple_kaons))


@configurable
def make_XtoTT_muons(pvs, mass_min=2000.0):
    filtered_TT_muons = filter_ttrack_muons_for_high_mass(pvs)
    return qee_ttrack_combiner_highmass(
        filtered_TT_muons,
        filtered_TT_muons,
        pvs,
        "[KS0 -> mu+ mu-]cc",
        mass_min=mass_min,
    )


@configurable
def make_XtoTT_kaons(pvs):
    filtered_TT_kaons = filter_ttrack_kaons_for_high_mass(pvs)
    return qee_ttrack_combiner_highmass(
        filtered_TT_kaons, filtered_TT_kaons, pvs, "[KS0 -> K+ K-]cc"
    )


@configurable
def make_XtoTT_pimu(pvs):
    filtered_TT_pions = filter_ttrack_pions_for_high_mass(pvs)
    filtered_TT_muons = filter_ttrack_muons_for_high_mass(pvs)
    return qee_ttrack_combiner_highmass(
        filtered_TT_pions,
        filtered_TT_muons,
        pvs,
        "[KS0 -> pi+ mu-]cc",
    )


@configurable
def qee_set_max_pvs(pvs, max_pvs=12):
    return filter_max_pvs(pvs, max_pvs=max_pvs)


@configurable
def qee_BtoLX_TT(
    long_particles,
    ttrack_particles,
    decay_descriptor,
    pvs,
    vertex_z_max=1000.0,
    mass_min=2200.0,
    mass_max=8300.0,
    pt_min=1500.0,
    maxdoca=80.0,
    maxdocachi2=10.0,
    max_chi2=6.0,
    ownpv_ip_max=30.0,
    ownpv_vdrho_max=75.0,
):
    combination_cut = F.require_all(
        F.PT > pt_min,
        F.math.in_range(mass_min, F.MASS, mass_max),
        F.MAXDOCA < maxdoca,
        F.MAXDOCACHI2 < maxdocachi2,
    )
    vertex_cut = F.require_all(
        F.END_VZ < vertex_z_max,
        F.CHI2 < max_chi2,
        F.OWNPVIP < ownpv_ip_max,
        F.OWNPVVDRHO < ownpv_vdrho_max,
        F.math.in_range(mass_min, F.MASS, mass_max),
    )
    return ParticleCombiner(
        Inputs=[long_particles, ttrack_particles],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cut,
        CompositeCut=vertex_cut,
        ParticleCombiner=PVF_with_single_extrapolation(),
        name="qee_BtoLX_TT_{hash}",
    )


@configurable
def make_qee_btokhh(
    hadrons,
    intermediate_state,
    pvs,
    decay_descriptor,
    name="make_qee_btokhh_{hash}",
    m_min=4800 * MeV,
    m_max=5800 * MeV,
    dira_min=0.9999,
    ipchi2_max=9,
    vtxchi2dof_max=9,
    fdchi2_min=120,
):
    """
    Builder for B->K(*)DB(->hh) with H' = DB = dark boson,
    originally from RD line of B->K(*)DB(->hh)
    """
    combination_code = F.math.in_range(m_min, F.MASS, m_max)
    vertex_code = F.require_all(
        F.CHI2DOF < vtxchi2dof_max,
        F.BPVIPCHI2(pvs) < ipchi2_max,
        F.BPVFDCHI2(pvs) > fdchi2_min,
        F.BPVDIRA(pvs) > dira_min,
    )

    return ParticleCombiner(
        [intermediate_state, hadrons],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        name=name,
    )


@configurable
def make_qee_detached_dihadron(
    name="make_qee_detached_dihadrons_{hash}",
    parent_id="KS0",
    same_sign=False,
    pid=None,
    min_pt_track=250 * MeV,
    min_p_track=3000 * MeV,
    min_ipchi2_track=25.0,
    max_adocachi2=30.0,
    max_vchi2ndof=9.0,
    am_min=250 * MeV,
    am_max=5000 * MeV,
    min_bpvvdchi2=225.0,
    min_ipchi2_dihadron=25.0,
    min_pt_dihadron=500 * MeV,
    min_DIRA_dihadron=0.999,
):
    """
    Make the detached dihadron, opposite-sign or same-sign.
    """
    descriptor = f"{parent_id} -> pi+ pi-"
    if same_sign:
        descriptor = f"[{parent_id} -> pi+ pi+]cc"
    pvs = make_pvs()
    pions = rdbuilder_thor.make_rd_detached_pions(
        mipchi2dvprimary_min=min_ipchi2_track,
        pt_min=min_pt_track,
        p_min=min_p_track,
        pid=pid,
    )
    combination_code = F.MAXSDOCACHI2CUT(max_adocachi2)
    vertex_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.CHI2DOF < max_vchi2ndof,
        F.BPVFDCHI2(pvs) > min_bpvvdchi2,
        F.BPVIPCHI2(pvs) > min_ipchi2_dihadron,
        F.BPVDIRA(pvs) > min_DIRA_dihadron,
        F.PT > min_pt_dihadron,
    )
    return ParticleCombiner(
        Inputs=[pions, pions],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
