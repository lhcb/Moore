###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the HLT2 single high-PT muon lines.
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from PyConf.Algorithms import WeightedRelTableAlg
from RecoConf.algorithms_thor import ParticleFilter
from RecoConf.reconstruction_objects import upfront_reconstruction
from RecoConf.standard_particles import make_long_pions

from Hlt2Conf.lines.qee.qee_builders import muon_filter
from Hlt2Conf.standard_jets import make_onlytrack_particleflow

all_lines = {}

threshold_map = {
    "standard": 15.0 * GeV,
    "prescale": 10.0 * GeV,
    "iso": 12.5 * GeV,
    "nomuid": 15.0 * GeV,
    "tight": 18.0 * GeV,
    "vtight": 20.0 * GeV,
}

hlt2_isolation_config = {
    "max_cone_pt": 5.0 * GeV,
    "cone_radius": 0.5,
    "use_pflow": True,
}


@configurable
def make_isolated_muons(
    high_pt_muons,
    max_cone_pt: float,
    cone_radius: float,
    use_pflow: bool,
    name: str = "HighPtIsolatedMuonMaker_{hash}",
):
    cone_cands = make_onlytrack_particleflow if use_pflow else make_long_pions
    # ~F.SHARE_TRACKS removes the signal track from the cone by ensuring
    # it is not in InputCandidates
    ftAlg = WeightedRelTableAlg(
        ReferenceParticles=high_pt_muons,
        InputCandidates=cone_cands(),
        Cut=F.require_all((F.DR2 < (cone_radius**2)), ~F.SHARE_TRACKS()),
    )

    ftAlg_Rels = ftAlg.OutputRelations

    code = (F.VALUE_OR(0) @ F.SUMCONE(Functor=F.PT, Relations=ftAlg_Rels)) < max_cone_pt

    return ParticleFilter(high_pt_muons, F.FILTER(code), name=name)


@register_line_builder(all_lines)
@configurable
def single_muon_highpt_line(
    name="Hlt2QEE_SingleHighPtMuonFull", prescale=1, persistreco=True
):
    """High PT single muon line"""

    high_pt_muons = muon_filter(min_pt=threshold_map["standard"], require_muID=True)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [high_pt_muons],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("pt", "eta", "n_candidates"),
    )


@register_line_builder(all_lines)
@configurable
def single_muon_highpt_prescale_line(
    name="Hlt2QEE_SingleHighPtMuonPrescaleFull", prescale=0.05, persistreco=True
):
    """High PT single muon line with lower pT threshold, prescaled to reduce the rate."""

    high_pt_muons = muon_filter(min_pt=threshold_map["prescale"], require_muID=True)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [high_pt_muons],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("pt", "eta", "n_candidates"),
    )


@register_line_builder(all_lines)
@configurable
def single_muon_highpt_iso_line(
    name="Hlt2QEE_SingleHighPtMuonIsoFull", prescale=1, persistreco=True
):
    """High PT single muon line with lower pT threshold, and isolation to keep the rate down.
    Make PT cut first to drastically reduce number of times isolation algorithm is called."""

    high_pt_muons = muon_filter(min_pt=threshold_map["iso"], require_muID=True)
    high_pt_isolated_muons = make_isolated_muons(high_pt_muons, **hlt2_isolation_config)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [high_pt_muons, high_pt_isolated_muons],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("pt", "eta", "n_candidates"),
    )


@register_line_builder(all_lines)
@configurable
def single_muon_highpt_nomuid_line(
    name="Hlt2QEE_SingleHighPtMuonNoMuIDFull", prescale=0.1
):
    """High PT single muon line with No ISMUON requirement for background and MuonID efficiency studies
    . Prescale required to control the rate."""

    high_pt_nomuid_muons = muon_filter(
        min_pt=threshold_map["nomuid"], require_muID=False
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [high_pt_nomuid_muons],
        prescale=prescale,
        persistreco=True,
        monitoring_variables=("pt", "eta", "n_candidates"),
        hlt1_filter_code=["Hlt1SingleHighPtMuonNoMuIDDecision"],
    )
