###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definitions of the QEE sprucing lines for the 2024 pp reference run.
"""

# Necessary imports to hlt2_filter robustly;
from inspect import signature

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV
from Moore.config import SpruceLine, register_line_builder
from PyConf import configurable
from PyConf.Algorithms import SelectionFromRelationTable, WeightedRelTableAlg
from PyConf.reading import get_particles
from RecoConf.event_filters import require_pvs
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction

from Hlt2Conf.lines.qee.diboson import (
    make_Wleptgamma_cand,
    make_WW_emu_cand,
    make_WZ_Zll_Wlnu_cand,
    make_Zllgamma_cand,
    make_ZZ_Zllplusll_cand,
)
from Hlt2Conf.lines.qee.dielectron_persist_photons import (
    charged_pion_filter,
    dielectron_maker_displaced,
    dielectron_maker_prompt,
    dielectron_sp_displaced_line_full,
    dielectron_sp_prompt_line_full,
    photon_filter,
)
from Hlt2Conf.lines.qee.high_mass_dielec import (
    make_Zee_cand,
    make_Zeess_cand,
    same_sign_dielec_line,
    z_to_e_e_line,
)
from Hlt2Conf.lines.qee.high_mass_dimuon import (
    make_Z_cand,
    make_Z_cand_DoubleNoMuID,
    make_Z_cand_SingleNoMuID,
    make_Zss_cand,
    same_sign_dimuon_line,
    z_to_mu_mu_double_nomuid_line,
    z_to_mu_mu_line,
    z_to_mu_mu_single_nomuid_line,
)
from Hlt2Conf.lines.qee.jets import (
    DiJetIncSVTag_line,
    IncDiJet15GeV_line,
    IncDiJet20GeV_line,
    IncDiJet25GeV_line,
    IncDiJet30GeV_line,
    IncDiJet35GeV_line,
    IncJet15GeV_line,
    IncJet25GeV_line,
    IncJet35GeV_line,
    IncJet45GeV_line,
    diSVTagJet10GeV_line,
    diSVTagJet15GeV_line,
    diSVTagJet20GeV_line,
    diSVTagJet25GeV_line,
    diSVTagJet30GeV_line,
    diSVTagJet35GeV_line,
    diTopoTagJet10GeV_line,
    diTopoTagJet15GeV_line,
    diTopoTagJet20GeV_line,
    diTopoTagJet25GeV_line,
    diTopoTagJet30GeV_line,
    diTopoTagJet35GeV_line,
    make_dijets,
    make_Trijets_cand,
    make_TrijetsTwoSVTag_cand,
)
from Hlt2Conf.lines.qee.qee_builders import make_gamma_jet, muon_filter
from Hlt2Conf.lines.qee.single_high_pt_electron import (
    ecal_deposit_fractions as elec_ecal_deposit_fractions,
)
from Hlt2Conf.lines.qee.single_high_pt_electron import (
    hcal_deposit_fractions as elec_hcal_deposit_fractions,
)
from Hlt2Conf.lines.qee.single_high_pt_electron import (
    make_highpt_electrons,
    make_highpt_isolated_electrons,
    single_electron_highpt_iso_line,
    single_electron_highpt_line,
    single_electron_highpt_prescale_line,
)
from Hlt2Conf.lines.qee.single_high_pt_electron import (
    pt_thresholds as elec_pt_thresholds,
)
from Hlt2Conf.lines.qee.single_high_pt_muon import (
    hlt2_isolation_config,
    make_isolated_muons,
    single_muon_highpt_iso_line,
    single_muon_highpt_line,
    single_muon_highpt_nomuid_line,
    single_muon_highpt_prescale_line,
)
from Hlt2Conf.lines.qee.single_high_pt_muon import threshold_map as muon_thresholds
from Hlt2Conf.lines.qee.top_muon_elec import (
    make_ttbar_MuE_cand,
    make_ttbar_MuEBjet_cand,
)
from Hlt2Conf.lines.qee.vjets import (
    make_ssdimuonjet_cand,
    make_W_elec_jet_cand,
    make_W_elec_jetjet_cand,
    make_W_elec_SVjet_cand,
    make_W_elec_SVjetSVjet_cand,
    make_Wjet_cand,
    make_Wjetjet_cand,
    make_WSVjet_cand,
    make_WSVjetSVjet_cand,
    make_Z0_elec_jet_cand,
    make_Z0_elec_jetjet_cand,
    make_Z0_elec_SVjet_cand,
    make_Z0_elec_SVjetSVjet_cand,
    make_Z0jet_cand,
    make_Z0jetjet_cand,
    make_Z0SVjet_cand,
    make_Z0SVjetSVjet_cand,
)
from Hlt2Conf.standard_jets import make_jets, make_onlytrack_particleflow

sprucing_lines = {}


def _hlt2_decision_regex(line_defn):
    to_decision = lambda k: f"{k}Decision"
    previous_hlt2_line_names = {
        # Lines that have been renamed over the years - here we store their old names mapped to current
        # Advice: try not to do so much renaming that this becomes an enormous dictionary of hard-coded strings...
        "Hlt2QEE_DiMuonNoIP_Full": ["Hlt2QEE_DiMuonNoIP_prescaledFullEventFull"],
        "Hlt2QEE_DiMuonNoIP_ssFull": ["Hlt2QEE_DiMuonNoIP_prescaledFullEvent_ssFull"],
        "Hlt2QEE_DiMuonHighMassSameSignFull": ["Hlt2QEE_DiMuonSameSignFull"],
        "Hlt2QEE_DiElectronHighMassSameSignFull": ["Hlt2QEE_DiElectronSameSignFull"],
    }
    line_name = signature(line_defn).parameters["name"].default
    # "Full" only appended to line names after 2023 data-taking. These regexes catch decision names from before then.
    # "FULL" catches the previous naming of the full-stream DiElectron lines
    # Other special cases are handled in previous_hlt2_line_names
    regexes = [
        to_decision(pfx)
        for pfx in [
            line_name,
            line_name.replace("Full", ""),
            line_name.replace("Full", "FULL"),
        ]
    ]
    if line_name in previous_hlt2_line_names.keys():
        regexes += [to_decision(pfx) for pfx in previous_hlt2_line_names[line_name]]
    return regexes


################ Z -> ll lines ################
@register_line_builder(sprucing_lines)
@configurable
def z_to_mu_mu_sprucing_line(name="SpruceQEE_ZToMuMu", prescale=1):
    """Z0 boson decay to two muons line, both requiring ismuon, passthrough after Hlt2QEE_ZToMuMuFull"""

    line_alg = make_Z_cand()
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_regex(z_to_mu_mu_line),
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def z_to_mu_mu_single_nomuid_sprucing_line(
    name="SpruceQEE_ZToMuMu_SingleNoMuID", prescale=1
):
    """Z0 boson decay to two muons line, where one requires ismuon, for MuonID efficiency studies. passthrough after Hlt2QEE_ZToMuMu_SingleNoMuIDFull"""

    line_alg = make_Z_cand_SingleNoMuID()
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_regex(z_to_mu_mu_single_nomuid_line),
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def z_to_mu_mu_double_nomuid_sprucing_line(
    name="SpruceQEE_ZToMuMu_DoubleNoMuID", prescale=1
):
    """Z0 boson decay to two muons line, where neither requires ismuon, passthrough after Hlt2QEE_ZToMuMu_DoubleNoMuIDFull"""

    line_alg = make_Z_cand_DoubleNoMuID()
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_regex(z_to_mu_mu_double_nomuid_line),
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def same_sign_dimuon_sprucing_line(name="SpruceQEE_DiMuonSameSignHighMass", prescale=1):
    """Z0 boson decay to two same sign muons line, passthrough after Hlt2QEE_DiMuonSameSignHighMassFull"""

    line_alg = make_Zss_cand()
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_regex(same_sign_dimuon_line),
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def z_to_e_e_sprucing_line(name="SpruceQEE_ZToEE", prescale=1):
    """Z0 boson decay to two electrons line, passthrough after Hlt2QEE_ZToEEFull"""

    line_alg = make_Zee_cand()
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_regex(z_to_e_e_line),
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def same_sign_dielec_sprucing_line(
    name="SpruceQEE_DiElectronSameSignHighMass", prescale=1
):
    """Z0 boson decay to two same sign electrons line, passthrough after Hlt2QEE_DiElectronSameSignHighMassFull"""

    line_alg = make_Zeess_cand()
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_regex(same_sign_dielec_line),
        prescale=prescale,
    )


################ Gamma + Jet lines ################
@register_line_builder(sprucing_lines)
@configurable
def gamma_dd_jet_sprucing_line(name="SpruceQEE_GammaDDJet", prescale=1):
    """Converted photon + jet line"""

    line_alg = make_gamma_jet(photon_type="DD")
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code="Hlt2QEE_GammaDDJetFullDecision",
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def gamma_ll_jet_sprucing_line(name="SpruceQEE_GammaLLJet", prescale=1):
    """Converted photon + jet line"""

    line_alg = make_gamma_jet(photon_type="LL")
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code="Hlt2QEE_GammaLLJetFullDecision",
        prescale=prescale,
    )


################ Z + Jet lines ################
@register_line_builder(sprucing_lines)
@configurable
def z_jet_sprucing_line(name="SpruceQEE_ZJet", prescale=1):
    """Z0 boson decay to two muons + jet line"""

    line_alg = make_Z0jet_cand()
    return SpruceLine(
        name=name, algs=upfront_reconstruction() + [line_alg], prescale=prescale
    )


@register_line_builder(sprucing_lines)
@configurable
def z_jet_persistreco_sprucing_line(name="SpruceQEE_ZJetPersistReco", prescale=1):
    """Z0 boson decay to two muons + jet line full event information are persisted"""

    line_alg = make_Z0jet_cand()
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def ssdimuon_jet_persistreco_sprucing_line(
    name="SpruceQEE_SSDiMuonJetPersistReco", prescale=1
):
    """same sign dimuons + jet line full event information are persisted"""

    line_alg = make_ssdimuonjet_cand()
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def z_svjet_sprucing_line(name="SpruceQEE_ZSVJet", prescale=1):
    """Z0 boson decay to two muons + SV jet line"""

    line_alg = make_Z0SVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def z_jetjet_sprucing_line(name="SpruceQEE_ZJetJet", prescale=1):
    """Z0 boson decay to two muons + jet jet line"""

    line_alg = make_Z0jetjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def z_svjetsvjet_sprucing_line(name="SpruceQEE_ZSVJetSVJet", prescale=1):
    """Z0 boson decay to two muons + SV jet SV jet line"""

    line_alg = make_Z0SVjetSVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale,
    )


################ W + Jet lines ################
@register_line_builder(sprucing_lines)
@configurable
def w_jet_sprucing_line(name="SpruceQEE_WJet", prescale=1):
    """High pt muon + jet line"""

    line_alg = make_Wjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def w_svjet_sprucing_line(name="SpruceQEE_WSVJet", prescale=1):
    """High pt muon + SV jet line"""

    line_alg = make_WSVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def w_jetjet_sprucing_line(name="SpruceQEE_WJetJet", prescale=1):
    """high pt muon + jet jet line"""

    line_alg = make_Wjetjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def w_svjetsvjet_sprucing_line(name="SpruceQEE_WSVJetSVJet", prescale=1):
    """high pt muon + SV jet SV jet line"""

    line_alg = make_WSVjetSVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def zee_jet_sprucing_line(name="SpruceQEE_ZEEJet", prescale=1):
    """Z0 boson decay to two electrons + jet line"""

    line_alg = make_Z0_elec_jet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale,
    )


################ Z -> ll + Jet lines ################
@register_line_builder(sprucing_lines)
@configurable
def zee_svjet_sprucing_line(name="SpruceQEE_ZEESVJet", prescale=1):
    """Z0 boson decay to two electrons + SV jet line"""

    line_alg = make_Z0_elec_SVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def zee_jetjet_sprucing_line(name="SpruceQEE_ZEEJetJet", prescale=1):
    """Z0 boson decay to two electrons + jet jet line"""

    line_alg = make_Z0_elec_jetjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def zee_svjetsvjet_sprucing_line(name="SpruceQEE_ZEESVJetSVJet", prescale=1):
    """Z0 boson decay to two electrons + SV jet SV jet line"""

    line_alg = make_Z0_elec_SVjetSVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale,
    )


################ W -> l + Jet lines ################
@register_line_builder(sprucing_lines)
@configurable
def we_jet_sprucing_line(name="SpruceQEE_WEJet", prescale=0.1):
    """High pt electron + jet line"""

    line_alg = make_W_elec_jet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def we_svjet_sprucing_line(name="SpruceQEE_WESVJet", prescale=0.1):
    """High pt electron + SV jet line"""

    line_alg = make_W_elec_SVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def we_jetjet_sprucing_line(name="SpruceQEE_WEJetJet", prescale=0.1):
    """high pt electrons + jet jet line"""

    line_alg = make_W_elec_jetjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def we_svjetsvjet_sprucing_line(name="SpruceQEE_WESVJetSVJet", prescale=0.1):
    """high pt electrons + SV jet SV jet line"""

    line_alg = make_W_elec_SVjetSVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale,
    )


################ ttbar Jet lines ################
@register_line_builder(sprucing_lines)
@configurable
def ttbar_to_mu_e_sprucing_line(name="SpruceQEE_TTbarToMuE", prescale=1):
    """ttbar to muon-electron line"""

    line_alg = make_ttbar_MuE_cand()
    return SpruceLine(
        name=name, algs=upfront_reconstruction() + [line_alg], prescale=prescale
    )


@register_line_builder(sprucing_lines)
@configurable
def ttbar_to_mu_e_bjet_sprucing_line(name="SpruceQEE_TTbarToMuEBjet", prescale=1):
    """ttbar to muon-electron-bjet line"""

    line_alg = make_ttbar_MuEBjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale,
    )


################ DiBoson lines ################
@register_line_builder(sprucing_lines)
@configurable
def WW_to_e_mu_sprucing_line(name="SpruceQEE_WWToMuE", prescale=1):
    """WW to e mu nu nu line"""

    line_alg = make_WW_emu_cand()
    return SpruceLine(
        name=name, algs=upfront_reconstruction() + [line_alg], prescale=prescale
    )


for lepton_type1, lepton_type2, line_suffix in zip(
    ["mu", "mu", "e", "e"], ["e", "mu", "mu", "e"], ["MuMuE", "MuMuMu", "EEMu", "EEE"]
):

    @register_line_builder(sprucing_lines)
    @configurable
    def DiBoson_WZ_lll_sprucing_line(
        name=f"SpruceQEE_WZTo{line_suffix}",
        lepton_type1=f"{lepton_type1}",
        lepton_type2=f"{lepton_type2}",
        prescale=1,
    ):
        f"""WZ to {lepton_type1} {lepton_type1} {lepton_type2} line"""

        line_alg = make_WZ_Zll_Wlnu_cand(
            lepton_type1=lepton_type1, lepton_type2=lepton_type2
        )
        return SpruceLine(
            name=name, algs=upfront_reconstruction() + [line_alg], prescale=prescale
        )


for lepton_type1, lepton_type2, line_suffix in zip(
    ["mu", "mu", "e", "e"],
    ["mu", "e", "e", "mu"],
    ["MuMuMuMu", "MuMuEE", "EEEE", "EEMuMu"],
):

    @register_line_builder(sprucing_lines)
    @configurable
    def ZZ_to_llll_sprucing_line(
        name=f"SpruceQEE_ZZTo{line_suffix}",
        lepton_type1=f"{lepton_type1}",
        lepton_type2=f"{lepton_type2}",
        prescale=1,
    ):
        f"""ZZ to {lepton_type1} {lepton_type1} {lepton_type2} {lepton_type2} line"""

        line_alg = make_ZZ_Zllplusll_cand(
            lepton_type1=lepton_type1, lepton_type2=lepton_type2
        )
        return SpruceLine(
            name=name, algs=upfront_reconstruction() + [line_alg], prescale=prescale
        )


for lepton_type, lepton_suffix in zip(["mu", "e"], ["Mu", "E"]):
    for photon_type, line_suffix in zip(["LL", "DD", "All"], ["LL", "DD", ""]):

        @register_line_builder(sprucing_lines)
        @configurable
        def WGamma_to_lgamma_sprucing_line(
            name=f"SpruceQEE_WGammaTo{lepton_suffix}Photon{line_suffix}",
            lepton_type=f"{lepton_type}",
            photon_type=f"{photon_type}",
            prescale=1,
        ):
            f"""Wgamma to {lepton_type} gamma ({photon_type}) line"""

            line_alg = make_Wleptgamma_cand(
                lepton_type=lepton_type, photon_type=photon_type
            )
            pvs = make_pvs()
            return SpruceLine(
                name=name,
                algs=upfront_reconstruction() + [require_pvs(pvs), line_alg],
                prescale=prescale,
            )

        @register_line_builder(sprucing_lines)
        @configurable
        def ZGamma_to_mu_mu_gamma_LL_sprucing_line(
            name=f"SpruceQEE_ZGammaTo{lepton_suffix}{lepton_suffix}Photon{line_suffix}",
            lepton_type=f"{lepton_type}",
            photon_type=f"{photon_type}",
            prescale=1,
        ):
            f"""Zgamma to {lepton_suffix} {lepton_suffix} gamma {line_suffix}"""

            line_alg = make_Zllgamma_cand(
                lepton_type=lepton_type, photon_type=photon_type
            )
            pvs = make_pvs()
            return SpruceLine(
                name=name,
                algs=upfront_reconstruction() + [require_pvs(pvs), line_alg],
                prescale=prescale,
            )


################ Single muon lines ################
@register_line_builder(sprucing_lines)
@configurable
def single_muon_highpt_sprucing_line(
    name="SpruceQEE_SingleHighPtMuon", persistreco=True, prescale=1
):
    """High PT Single Muon line, slightly tighter pT cut on top of Hlt2QEE_SingleHighPtMuonFull"""

    line_alg = muon_filter(min_pt=muon_thresholds["tight"])
    return SpruceLine(
        name=name,
        persistreco=persistreco,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_regex(single_muon_highpt_line),
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def single_muon_highpt_prescale_sprucing_line(
    name="SpruceQEE_SingleHighPtMuonPrescale", persistreco=True, prescale=1
):
    """High PT single muon line with lower pT threshold, prescaled to reduce the rate, passthrough after Hlt2QEE_SingleHighPtMuonPrescaleFull"""

    line_alg = muon_filter(min_pt=muon_thresholds["prescale"])
    return SpruceLine(
        name=name,
        persistreco=persistreco,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_regex(single_muon_highpt_prescale_line),
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def single_muon_highpt_iso_sprucing_line(
    name="SpruceQEE_SingleHighPtMuonIso", persistreco=True, prescale=0.1
):
    """High PT single muon line with lower pT threshold, using isolation variables to reduce the rate, passthrough after Hlt2QEE_SingleHighPtMuonIso"""

    filtered_muons = muon_filter(min_pt=muon_thresholds["iso"])
    line_alg = make_isolated_muons(filtered_muons, **hlt2_isolation_config)
    return SpruceLine(
        name=name,
        persistreco=persistreco,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_regex(single_muon_highpt_iso_line),
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def single_muon_highpt_nomuid_sprucing_line(
    name="SpruceQEE_SingleHighPtMuonNoMuID", persistreco=True, prescale=0.5
):
    """High PT single muon line without ISMUON requirement, for background and MuonID efficiency studies, passthrough after Hlt2QEE_SingleHighPtMuonNoMuIDFull"""

    line_alg = muon_filter(min_pt=muon_thresholds["nomuid"], require_muID=False)
    return SpruceLine(
        name=name,
        persistreco=persistreco,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_regex(single_muon_highpt_nomuid_line),
        prescale=prescale,
    )


################ Single electron lines ################
@register_line_builder(sprucing_lines)
@configurable
def single_electron_highpt_sprucing_line(
    name="SpruceQEE_SingleHighPtElectron", prescale=1, persistreco=True
):
    """High PT Single Electron line, passthrough after Hlt2QEE_SingleHighPtElectronFull"""
    line_alg = make_highpt_electrons(
        min_electron_pt=elec_pt_thresholds["standard"],
        ecal_deposit_fraction=elec_ecal_deposit_fractions["standard"],
        hcal_deposit_fraction=elec_hcal_deposit_fractions["standard"],
    )
    return SpruceLine(
        name=name,
        persistreco=persistreco,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_regex(single_electron_highpt_line),
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def single_electron_highpt_prescale_sprucing_line(
    name="SpruceQEE_SingleHighPtElectronPrescale", prescale=0.1, persistreco=True
):
    """High PT single electron line with lower pT threshold, prescaled to reduce the rate, passthrough after Hlt2QEE_SingleHighPtElectronPrescaleFull"""
    line_alg = make_highpt_electrons(
        min_electron_pt=elec_pt_thresholds["prescale"],
        ecal_deposit_fraction=elec_ecal_deposit_fractions["standard"],
        hcal_deposit_fraction=elec_hcal_deposit_fractions["standard"],
    )
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        persistreco=persistreco,
        hlt2_filter_code=_hlt2_decision_regex(single_electron_highpt_prescale_line),
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def single_electron_highpt_iso_sprucing_line(
    name="SpruceQEE_SingleHighPtElectronIso", prescale=0.05, persistreco=True
):
    """High PT single electron line with lower pT threshold, using isolation variables to reduce the rate, passthrough after Hlt2QEE_SingleHighPtElectronIsoFull"""
    filtered_electrons = make_highpt_electrons(
        min_electron_pt=elec_pt_thresholds["iso"],
        ecal_deposit_fraction=elec_ecal_deposit_fractions["iso"],
    )
    line_alg = make_highpt_isolated_electrons(
        filtered_electrons, pflow_output=make_onlytrack_particleflow
    )
    return SpruceLine(
        name=name,
        persistreco=persistreco,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_regex(single_electron_highpt_iso_line),
        prescale=prescale,
    )


################ Single Jet lines ################
for jet_pt_min, hlt2_dec, pre_scale in zip(
    ["15", "25", "35", "45"],
    [IncJet15GeV_line, IncJet25GeV_line, IncJet35GeV_line, IncJet45GeV_line],
    [1.0, 1.0, 1.0, 1.0],
):

    @register_line_builder(sprucing_lines)
    @configurable
    def InclusiveSingleJet_sprucing_line(
        name=f"SpruceQEE_SingleJet{jet_pt_min}",
        jet_pt_min=jet_pt_min,
        hlt2_dec=hlt2_dec,
        prescale=pre_scale,
    ):
        f"""Inclusive single jet sprucing line with jet_min_pt of {jet_pt_min} GeV"""

        line_alg = make_jets()
        return SpruceLine(
            name=name,
            algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
            calo_clusters=True,
            persistreco=True,
            hlt2_filter_code=_hlt2_decision_regex(hlt2_dec),
            prescale=prescale,
        )


################ SV Di/Tri-Jet lines ################
from Hlt2Conf.lines.qee.jets import _hlt1_light_jet_filter

# SV di-jets
for jet_pt_min, hlt2_dec, pre_scale in zip(
    ["10", "15", "20", "25", "30", "35"],
    [
        diSVTagJet10GeV_line,
        diSVTagJet15GeV_line,
        diSVTagJet20GeV_line,
        diSVTagJet25GeV_line,
        diSVTagJet30GeV_line,
        diSVTagJet35GeV_line,
    ],
    [1, 1, 1, 1, 1, 1],
):

    @register_line_builder(sprucing_lines)
    @configurable
    def DiSVTaggedJets_sprucing_line(
        name=f"SpruceQEE_diSVTag{jet_pt_min}{jet_pt_min}",
        jet_pt_min=jet_pt_min,
        hlt2_dec=hlt2_dec,
        prescale=pre_scale,
    ):
        f"""SV-Tagged dijet sprucing line with jet_min_pt of {jet_pt_min} GeV per child"""

        line_alg = make_dijets(
            tagpair=("SV", "SV"), prod_pt_min=int(jet_pt_min) * GeV, min_dphi=1.5
        )
        return SpruceLine(
            name=name,
            algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
            calo_clusters=True,
            persistreco=True,
            hlt2_filter_code=_hlt2_decision_regex(hlt2_dec),
            prescale=prescale,
        )


# Topo di-jets
for jet_pt_min, hlt2_dec in zip(
    ["10", "15", "20", "25", "30", "35"],
    [
        diTopoTagJet10GeV_line,
        diTopoTagJet15GeV_line,
        diTopoTagJet20GeV_line,
        diTopoTagJet25GeV_line,
        diTopoTagJet30GeV_line,
        diTopoTagJet35GeV_line,
    ],
):

    @register_line_builder(sprucing_lines)
    @configurable
    def DiTopoTaggedJets_sprucing_line(
        name=f"SpruceQEE_diTopoTag{jet_pt_min}{jet_pt_min}",
        jet_pt_min=jet_pt_min,
        hlt2_dec=hlt2_dec,
        prescale=1,
    ):
        f"""Topo-Tagged dijet sprucing line with jet_min_pt of {jet_pt_min} GeV per child"""

        line_alg = make_dijets(
            tagpair=("TOPO", "TOPO"), prod_pt_min=int(jet_pt_min) * GeV, min_dphi=1.5
        )
        return SpruceLine(
            name=name,
            algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
            hlt2_filter_code=_hlt2_decision_regex(hlt2_dec),
            prescale=prescale,
        )


# Asymmetric Sprucing Line
@register_line_builder(sprucing_lines)
@configurable
def SVTagDijets_sprucing_line(
    name="SpruceQEE_IncSVTagDijets", hlt2_dec=DiJetIncSVTag_line, prescale=1
):
    """Inc+SV Tag Dijets sprucing line"""

    line_alg = make_dijets(
        tagpair=(None, "SV"),
        min_dijet_mass=40 * GeV,
        prod_pt_min=int(25.0) * GeV,
        min_dphi=1.5,
    )
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        persistreco=True,
        calo_clusters=True,
        hlt2_filter_code=_hlt2_decision_regex(hlt2_dec),
        prescale=prescale,
    )


# Inclusive di-jets
for jet_pt_min, hlt2_dec in zip(
    ["15", "20", "25", "30", "35"],
    [
        IncDiJet15GeV_line,
        IncDiJet20GeV_line,
        IncDiJet25GeV_line,
        IncDiJet30GeV_line,
        IncDiJet35GeV_line,
    ],
):

    @register_line_builder(sprucing_lines)
    @configurable
    def Dijets_sprucing_line(
        name=f"SpruceQEE_Dijets{jet_pt_min}{jet_pt_min}",
        jet_pt_min=jet_pt_min,
        hlt2_dec=hlt2_dec,
        prescale=1,
    ):
        f"""Dijet sprucing line with jet_min_pt of {jet_pt_min} GeV per child"""

        line_alg = make_dijets(
            tagpair=(None, None), prod_pt_min=int(jet_pt_min) * GeV, min_dphi=1.5
        )
        return SpruceLine(
            name=name,
            algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
            calo_clusters=True,
            persistreco=True,
            hlt2_filter_code=_hlt2_decision_regex(hlt2_dec),
            prescale=prescale,
        )


@register_line_builder(sprucing_lines)
@configurable
def Trijets_sprucing_line(name="SpruceQEE_Trijets", prescale=1):
    """Trijets sprucing line"""

    line_alg = make_Trijets_cand()
    return SpruceLine(
        name=name,
        hlt1_filter_code=_hlt1_light_jet_filter,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale,
    )


@register_line_builder(sprucing_lines)
@configurable
def TrijetsTwoSVTag_sprucing_line(name="SpruceQEE_TrijetsTwoSVTag", prescale=1):
    """Trijets sprucing line"""

    line_alg = make_TrijetsTwoSVTag_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale,
    )


################ Pi0/eta -> e+e-gamma lines ################


@register_line_builder(sprucing_lines)
@configurable
def dielectron_sp_prompt_sprucing_line(
    name="SpruceQEE_DiElectronPrompt_PersistPhotons", prescale=0.1
):
    """Sprucing of full stream lines selecting prompt pi0/eta -> e+e-gamma
    decays, allowing full-event studies and the e+e- + random gamma background
    studies since charged pions in the same event are persisted (needed to
    deduce 4-vector of photon)."""

    pvs = make_pvs()

    dielectrons_prompt = dielectron_maker_prompt(e_minpt=0.0, isOS=True)

    photons_table = WeightedRelTableAlg(
        InputCandidates=photon_filter(),
        ReferenceParticles=dielectrons_prompt,
        Cut=in_range(5.0 * MeV, F.COMB_MASS(), 600.0 * MeV),
    )

    charged_pions = charged_pion_filter(pi_minpt=75.0 * MeV)

    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dielectrons_prompt],
        extra_outputs=[
            (
                "DiElectronPrompt_Photons",
                SelectionFromRelationTable(
                    InputRelations=photons_table.OutputRelations
                ).OutputLocation,
            ),
            ("ChargedPions", charged_pions),
        ],
        prescale=prescale,
        hlt2_filter_code=_hlt2_decision_regex(dielectron_sp_prompt_line_full),
    )


@register_line_builder(sprucing_lines)
@configurable
def dielectron_sp_displaced_sprucing_line(
    name="SpruceQEE_DiElectronDisplaced_PersistPhotons", prescale=0.1
):
    """Sprucing of full stream lines selecting displaced pi0/eta -> e+e-gamma
    decays, allowing full-event studies and the e+e- + random gamma background
    studies since charged pions in the same event are persisted (needed to
    deduce 4-vector of photon)."""

    pvs = make_pvs()

    dielectrons_displaced = dielectron_maker_displaced(e_minpt=0.0, isOS=True)

    photons_table = WeightedRelTableAlg(
        InputCandidates=photon_filter(),
        ReferenceParticles=dielectrons_displaced,
        Cut=in_range(5.0 * MeV, F.COMB_MASS(), 600.0 * MeV),
    )

    charged_pions = charged_pion_filter(pi_minpt=75.0 * MeV)

    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dielectrons_displaced],
        extra_outputs=[
            (
                "DiElectronDisplaced_Photons",
                SelectionFromRelationTable(
                    InputRelations=photons_table.OutputRelations
                ).OutputLocation,
            ),
            ("ChargedPions", charged_pions),
        ],
        prescale=prescale,
        hlt2_filter_code=_hlt2_decision_regex(dielectron_sp_displaced_line_full),
    )


############# H' -> mu+ mu- line #######################################


@register_line_builder(sprucing_lines)
@configurable
def HtoMuMu_TT_sprucing_line(name="SpruceQEE_HtoMuMu_TT", prescale=1):
    """Dark Higgs boson decay to two TTrack muons line, passthrough after Hlt2QEE_HtoMuMu_TTFull"""

    pvs = make_pvs()
    # The fitted T tracks + associated PID are not part of persistreco, thus have to access candidates using get_particles
    line_alg = get_particles("/Event/HLT2/Hlt2QEE_HtoMuMu_TTFull/HtoTT_muons/Particles")
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), line_alg],
        hlt2_filter_code="Hlt2QEE_HtoMuMu_TTFullDecision",
        prescale=prescale,
    )
