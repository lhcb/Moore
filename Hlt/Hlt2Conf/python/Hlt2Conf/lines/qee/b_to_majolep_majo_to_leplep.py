###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
This defines HLT2 lines for B+(-)/B_c+(-) -> (X) mu N, N-> l l' nu where N is a neutral lepton
N has no lifetime hypothesis applied. Combination of the neutral lepton daugthers is limited between 200(to cut conversion)-7000 MeV
All leptons from B are Long. Daughters of the neutral lepton can be both Long and Downstream.
Loose and Tight lines have a different PID cut looseness on pions and electrons. Note, PID_E is applied to pions too, so there is a need for a no_brem added calibration.
Tight lines will hopefully have raw event information saved, specifically tracking infor.

List of lines (LL - Long, DD - downstream):

Hlt2QEE_BpToMajoMu_MajoToMuMu_LL_Tight: tight PID cut line for the B(_c)+ -> mu HNL(-> mu mu, LL)
Hlt2QEE_BpToMajoMu_MajoToMuMu_DD_Tight: tight PID cut line for the B(_c)+ -> mu HNL(-> mu mu, DD)
Hlt2QEE_BpToMajoMu_MajoToMuE_SS_LL_Tight: tight PID cut line for the B(_c)+ -> mu HNL(-> mu+ e, LL)
Hlt2QEE_BpToMajoMu_MajoToMuE_SS_DD_Tight: tight PID cut line for the B(_c)+ -> mu HNL(-> mu+ e, DD)
Hlt2QEE_BpToMajoMu_MajoToMuE_OS_LL_Tight: tight PID cut line for the B(_c)+ -> mu HNL(-> mu- e, LL)
Hlt2QEE_BpToMajoMu_MajoToMuE_OS_DD_Tight: tight PID cut line for the B(_c)+ -> mu HNL(-> mu- e, DD)

Hlt2QEE_BpToMajoE_MajoToMuMu_LL_Tight: tight PID cut line for the B(_c)+ -> e HNL(-> mu mu, LL)
Hlt2QEE_BpToMajoE_MajoToMuMu_DD_Tight: tight PID cut line for the B(_c)+ -> e HNL(-> mu mu, DD)
Hlt2QEE_BpToMajoE_MajoToMuE_SS_LL_Tight: tight PID cut line for the B(_c)+ -> e HNL(-> mu+ e, LL)
Hlt2QEE_BpToMajoE_MajoToMuE_SS_DD_Tight: tight PID cut line for the B(_c)+ -> e HNL(-> mu+ e, DD)
Hlt2QEE_BpToMajoE_MajoToMuE_OS_LL_Tight: tight PID cut line for the B(_c)+ -> e HNL(-> mu- e, LL)
Hlt2QEE_BpToMajoE_MajoToMuE_OS_DD_Tight: tight PID cut line for the B(_c)+ -> e HNL(-> mu- e, DD)

Contact: Louis Henry, louis.henry@cern.ch

"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.algorithms_thor import ParticleCombiner
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    make_down_electrons_no_brem,
    make_down_muons,
    make_long_electrons_with_brem,
    make_long_muons,
)

from Hlt2Conf.lines.qee.qee_builders import hnl_prefilter, make_filter_tracks
from Hlt2Conf.lines.qee.qee_monitors import qee_monitors1d

all_lines = {}

_HNL_DEFAULT_MONITORING_VARIABLES = ("pt", "eta", "vchi2", "ipchi2", "n_candidates")


# Define Custom monitoring
def _custom_monitoring_params(pvs, ptName):
    default_params_all = {  # title : { key : value }
        "P": {"functor": F.P, "xmin": 0.0 * GeV, "xmax": 15.0 * GeV, "nbins": 750},
        "PT": {"functor": F.PT, "xmin": 0.0 * GeV, "xmax": 3.0 * GeV, "nbins": 750},
        "MINIPCHI2": {
            "functor": F.MINIPCHI2(pvs()),
            "xmin": 0.0,
            "xmax": 1000,
            "nbins": 500,
        },
    }
    default_params_child = {  # title : { key : value }
        "TRCHI2NDOF": {
            "functor": F.CHI2DOF,
            "xmin": 0.0,
            "xmax": 10,
            "nbins": 100,
        },
        "GhostProb": {"functor": F.GHOSTPROB, "xmin": 0.0, "xmax": 1.0, "nbins": 100},
    }
    params_dicts = (
        [default_params_all, default_params_child]
        if "Child" in ptName
        else [default_params_all]
    )
    pt_specific_params = {
        f"{ptName}_{title}": params
        for params_dict in params_dicts
        for title, params in params_dict.items()
    }
    return pt_specific_params


defaultCutDict = {
    "LL": {
        "AllTracks": {"P": 2000.0 * MeV},
        "Children": {"MINIPCHI2PV": 200},
        "Bachelor": {},
        "Child1": {},
        "Child2": {},
        "Majo": {
            "FDCHI2MIN": 2000,
            "MASS": [0.2 * GeV, 6.0 * GeV],
            "MINIPCHI2PV": 20,
        },
        "B": {"MASS": [4.0 * GeV, 7.0 * GeV]},
    },
    "DD": {
        "AllTracks": {"P": 2000.0 * MeV},
        "Children": {},
        "Bachelor": {},
        "Child1": {},
        "Child2": {},
        "Majo": {"MAXDOCA": 1},
        "B": {"MASS": [3.0 * GeV, 7.0 * GeV], "FDCHI2MAX": 3000.0},
    },
}

child_muon_pidCut = [F.ISMUON, F.PID_MU > 0.0]
bach_muon_pidCut = [F.PID_MU > 2]

electron_pidCut = [F.PID_E > -2]

particle_makers = {
    "L": {"e": make_long_electrons_with_brem, "mu": make_long_muons},
    "D": {"e": make_down_electrons_no_brem, "mu": make_down_muons},
}


# Build a cut from a list of functors
def buildFromList(cutList):
    toReturn = F.ALL
    for i in range(0, len(cutList)):
        toReturn &= cutList[i]
    return toReturn


# Build a cut from a dictionnary
def build_cut_on_track(cutDict, pvs):
    listOfFunctors = []
    for cut, cutVal in cutDict.items():
        if cut == "PIDCuts":
            listOfFunctors += cutVal
        elif cut == "MASS":
            listOfFunctors.append(in_range(cutVal[0], F.MASS, cutVal[1]))
        else:
            functors = {
                "P": F.P,
                "PT": F.PT,
                "MINIPCHI2PV": F.MINIPCHI2(pvs()),
                "FDCHI2MIN": F.OWNPVFDCHI2,
                "FDCHI2MAX": F.OWNPVFDCHI2,
                "MAXDOCA": F.MAXDOCA(),
            }
            if "MAX" in cut:
                listOfFunctors.append(functors[cut] < cutVal)
            else:
                listOfFunctors.append(functors[cut] > cutVal)

    # Build the cut
    return buildFromList(listOfFunctors)


def build_combination_cut(cutDict, pvs):
    listOfFunctors = []
    for cut, cutVal in cutDict.items():
        if cut == "MASS":
            listOfFunctors.append(in_range(cutVal[0], F.MASS, cutVal[1]))
    # Build the cut
    return buildFromList(listOfFunctors)


def builder_BToMajoL_line(
    name, bachelorName, children, cutDict={}, prescale=1, persistreco=True
):
    pvs = make_pvs
    # Interpret name into a B decay
    majoDecayDescr = "KS0 -> " + children[0] + "+ " + children[1] + "-"
    recoMode = "LL" if "LL" in name else "DD"
    # Build the cut dictionnaries

    for key1, val1 in defaultCutDict[recoMode].items():
        if key1 not in cutDict:
            cutDict[key1] = val1
        else:
            for key2, val2 in val1.items():
                if key2 not in cutDict[key1]:
                    cutDict[key1][key2] = val2
    # Apply Children and AllTracks cuts
    for key, val in cutDict["Children"].items():
        for child in ["Child1", "Child2"]:
            if key not in cutDict[child]:
                cutDict[child][key] = val
    for key, val in cutDict["AllTracks"].items():
        for recTrack in ["Bachelor", "Child1", "Child2"]:
            if key not in cutDict[recTrack]:
                cutDict[recTrack][key] = val
    # Apply PID cuts
    cutDict["Bachelor"]["PIDCuts"] = (
        bach_muon_pidCut if bachelorName == "mu" else electron_pidCut
    )
    cutDict["Child1"]["PIDCuts"] = (
        child_muon_pidCut if children[0] == "mu" else electron_pidCut
    )
    cutDict["Child2"]["PIDCuts"] = (
        child_muon_pidCut if children[1] == "mu" else electron_pidCut
    )

    # Make particles and decays
    firstChild = make_filter_tracks(
        make_particles=particle_makers[recoMode[0]][children[0]],
        name="majo_" + children[0] + "_{hash}",
        additionalCuts=build_cut_on_track(cutDict["Child1"], pvs),
    )
    secondChild = make_filter_tracks(
        make_particles=particle_makers[recoMode[1]][children[1]],
        name="majo_" + children[1] + "_{hash}",
        additionalCuts=build_cut_on_track(cutDict["Child2"], pvs),
    )
    majo = ParticleCombiner(
        [firstChild, secondChild],
        name="Majo2" + children[0] + children[1] + "_" + recoMode + "_{hash}",
        DecayDescriptor=majoDecayDescr,
        CombinationCut=build_combination_cut(cutDict["Majo"], pvs),
        CompositeCut=build_cut_on_track(cutDict["Majo"], pvs),
    )
    # Bachelor
    bachelor = make_filter_tracks(
        make_particles=particle_makers["L"][bachelorName],
        name="bachelor_{hash}",
        additionalCuts=build_cut_on_track(cutDict["Bachelor"], pvs),
    )
    # Decay
    b2LN = ParticleCombiner(
        [majo, bachelor],
        name=name + "_{hash}",
        DecayDescriptor="[B+ -> KS0 " + bachelorName + "+]cc",
        CombinationCut=build_combination_cut(cutDict["B"], pvs),
        CompositeCut=build_cut_on_track(cutDict["B"], pvs),
    )

    mons = []
    for ptName, pt in [("Child1", firstChild), ("Child2", secondChild), ("N", majo)]:
        pt_specific_params = _custom_monitoring_params(pvs=pvs, ptName=ptName)
        mons.append(
            qee_monitors1d(
                particles=pt, linename=name, monitoring_params=pt_specific_params
            )
        )
    return Hlt2Line(
        name=name,
        algs=hnl_prefilter(require_GEC=True) + [b2LN] + mons,
        prescale=prescale,
        monitoring_variables=_HNL_DEFAULT_MONITORING_VARIABLES,
    )


for recoMode in ["LL", "DD"]:
    for bachelor in ["mu", "e"]:
        for children in [("mu", "mu"), ("mu", "e"), ("e", "mu"), ("e", "e")]:
            name = f"Hlt2QEE_BpToMajo{bachelor.capitalize()}_MajoTo{children[0].capitalize()}{children[1].capitalize()}_{recoMode}_Tight"

            @register_line_builder(all_lines)
            def make_line(name=name, bachelor=bachelor, children=children):
                return builder_BToMajoL_line(
                    name,
                    bachelor,
                    children,
                    cutDict={}
                    if children != ("e", "e")
                    else {
                        "Children": {"MINIPCHI2PV": 500, "PT": 500 * MeV},
                        "Majo": {
                            "FDCHI2MIN": 3000,
                        },
                    },
                )
