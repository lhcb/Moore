###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the HLT2 Z->MuMu and same-sign dimuon (control sample) lines.
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.reconstruction_objects import upfront_reconstruction
from RecoConf.standard_particles import make_ismuon_long_muon, make_long_muons

from Hlt2Conf.lines.qee.qee_monitors import _default_Z_params as z_monitoring_defaults
from Hlt2Conf.lines.qee.qee_monitors import (
    pseudomass1_functor_expression,
    pseudomass2_functor_expression,
    qee_monitors1d,
)

all_lines = {}

_default_monitoring_variables = ("pt", "eta", "n_candidates")
z_min_combination_mass, z_min_composite_mass = 30.0 * GeV, 40.0 * GeV
zss_min_combination_mass = 16.0 * GeV
zss_min_composite_mass = zss_min_combination_mass


@configurable
def muon_filter_for_Z(particles, min_pt=0.0 * GeV):
    """A muon filter: PT"""
    return ParticleFilter(particles, F.FILTER(F.PT > min_pt))


@configurable
def make_Z_cand():
    muons_for_Z = muon_filter_for_Z(make_ismuon_long_muon(), min_pt=3.0 * GeV)
    return make_dimuon_novxt(muons_for_Z, muons_for_Z, "Z0 -> mu+ mu-")


@configurable
def make_Z_cand_SingleNoMuID():
    muon_ID, muon_noID = make_ismuon_long_muon(), make_long_muons()
    muons_for_Z_ID = muon_filter_for_Z(muon_ID, min_pt=10.0 * GeV)
    muons_for_Z_noID = muon_filter_for_Z(muon_noID, min_pt=10.0 * GeV)
    return make_dimuon_novxt(muons_for_Z_ID, muons_for_Z_noID, "[Z0 -> mu+ mu-]cc")


@configurable
def make_Z_cand_DoubleNoMuID():
    muons_for_Z = muon_filter_for_Z(make_long_muons(), min_pt=10.0 * GeV)
    return make_dimuon_novxt(muons_for_Z, muons_for_Z, "Z0 -> mu+ mu-")


@configurable
def make_Zss_cand():
    muons = muon_filter_for_Z(make_ismuon_long_muon(), min_pt=3.0 * GeV)
    return make_dimuon_novxt(
        muons,
        muons,
        "[Z0 -> mu- mu-]cc",
        min_combination_mass=zss_min_combination_mass,
        min_composite_mass=zss_min_composite_mass,
    )


@configurable
def make_dimuon_novxt(
    input_muon1,
    input_muon2,
    decaydescriptor,
    min_combination_mass=z_min_combination_mass,
    min_composite_mass=z_min_composite_mass,
):
    """Dimuon combination with only a mass cut"""

    # pre-vertex fit, useful for controlling the combinatorics
    combination_code = F.MASS > min_combination_mass
    # post-vertex fit
    composite_code = F.MASS > min_composite_mass

    return ParticleCombiner(
        [input_muon1, input_muon2],
        DecayDescriptor=decaydescriptor,
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


@register_line_builder(all_lines)
@configurable
def z_to_mu_mu_line(name="Hlt2QEE_ZToMuMuFull", prescale=1, persistreco=True):
    """Z0 boson decay to two muons line, both requiring ismuon"""

    z02mumu = make_Z_cand()
    z_monitoring_params = {
        **z_monitoring_defaults,
        **{
            # positive: 1, negative: 2 from the decay descriptor.
            "pseudomass_positive": {
                "functor": pseudomass1_functor_expression,
                "xmin": 30.0 * GeV,
                "xmax": 140.0 * GeV,
                "nbins": 110,
            },
            "pseudomass_negative": {
                "functor": pseudomass2_functor_expression,
                "xmin": 30.0 * GeV,
                "xmax": 140.0 * GeV,
                "nbins": 110,
            },
        },
    }
    monitor_z0 = qee_monitors1d(
        particles=z02mumu, linename=name, monitoring_params=z_monitoring_params
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [z02mumu, monitor_z0],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_default_monitoring_variables,
    )


@register_line_builder(all_lines)
@configurable
def z_to_mu_mu_single_nomuid_line(
    name="Hlt2QEE_ZToMuMu_SingleNoMuIDFull", prescale=1, persistreco=True
):
    """Z0 boson decay to two muons line, where one requires ismuon, for efficiency studies"""

    z02mumu = make_Z_cand_SingleNoMuID()
    monitor_z0 = qee_monitors1d(
        particles=z02mumu, linename=name, monitoring_params=z_monitoring_defaults
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [z02mumu, monitor_z0],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_default_monitoring_variables,
    )


@register_line_builder(all_lines)
@configurable
def z_to_mu_mu_double_nomuid_line(
    name="Hlt2QEE_ZToMuMu_DoubleNoMuIDFull", prescale=0.01, persistreco=True
):
    """Z0 boson decay to two muons line, where neither requires ismuon, for background studies"""

    z02mumu = make_Z_cand_DoubleNoMuID()
    monitor_z0 = qee_monitors1d(
        particles=z02mumu, linename=name, monitoring_params=z_monitoring_defaults
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [z02mumu, monitor_z0],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_default_monitoring_variables,
    )


@register_line_builder(all_lines)
@configurable
def same_sign_dimuon_line(
    name="Hlt2QEE_DiMuonHighMassSameSignFull", prescale=1, persistreco=True
):
    """Z0 boson decay to two same-sign muons line"""

    mumuss = make_Zss_cand()
    monitor_mumuss = qee_monitors1d(
        particles=mumuss, linename=name, monitoring_params=z_monitoring_defaults
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [mumuss, monitor_mumuss],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_default_monitoring_variables,
    )
