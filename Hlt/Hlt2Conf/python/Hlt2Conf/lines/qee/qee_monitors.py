###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from GaudiKernel.SystemOfUnits import GeV
from SelAlgorithms.monitoring import histogram_1d, monitor

rapidity_functor_expression = 0.5 * F.math.log((F.ENERGY + F.PZ) / (F.ENERGY - F.PZ))

pseudomass1_functor_expression = F.MASS * (F.CHILD(1, F.PT) / F.CHILD(2, F.PT)) ** 0.5
pseudomass2_functor_expression = F.MASS * (F.CHILD(2, F.PT) / F.CHILD(1, F.PT)) ** 0.5

_default_Z_params = {  # title : {key : value}
    "mass": {"functor": F.MASS, "xmin": 0.0 * GeV, "xmax": 150.0 * GeV, "nbins": 100},
    "rapidity": {
        "functor": rapidity_functor_expression,
        "xmin": 1,
        "xmax": 5,
        "nbins": 40,
    },
}


def qee_monitors1d(
    particles,
    linename="qee_dummy_line_formonitoring",
    monitoring_params=_default_Z_params,
):
    return monitor(
        data=particles,
        histograms=[
            _hist1d(linename=linename, title=key, **params)
            for key, params in monitoring_params.items()
        ],
    )


def _hist1d(linename, functor, title, xmin, xmax, nbins):
    return histogram_1d(
        f"/{linename}/{title}",
        f"Monitor_{linename}_{title}",
        functor,
        nbins,
        (xmin, xmax),
        title,
    )
