###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Set of HLT2 inclusive lines to search for longlived particles with downstream tracks.
- V0 -> H H
"""

import Functors as F
from GaudiKernel.SystemOfUnits import MeV, mm
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.event_filters import require_pvs
from RecoConf.reconstruction_objects import make_pvs as _make_pvs
from RecoConf.reconstruction_objects import upfront_reconstruction
from RecoConf.standard_particles import make_down_pions

turbo_lines = {}


@configurable
def make_DownstreamBuScaInclusive2H_DD(
    particles,
    descriptors="KS0 -> pi+ pi-",
    name="make_DownstreamBuScaInclusive2H_DD_{hash}",
    make_pvs=_make_pvs,
    minP=1400 * MeV,
    minPT=175 * MeV,
    minZ=500 * mm,
    maxChi2=25,
    maxOWNPVIP=100.0,
):
    busca_tracks = particles
    # Very simple cuts to filter soft tracks
    busca_tracks_filter_code = F.require_all(F.P > minP, F.PT > minPT)
    filtered_busca_tracks = ParticleFilter(
        busca_tracks, F.FILTER(busca_tracks_filter_code)
    )
    # Do vertexing
    vertex_code = F.require_all(
        F.CHI2DOF < maxChi2, F.END_VZ > minZ, F.OWNPVVDRHO < maxOWNPVIP
    )
    busca_svs = ParticleCombiner(
        Inputs=[filtered_busca_tracks, filtered_busca_tracks],
        DecayDescriptor=descriptors,
        name=name,
        CompositeCut=vertex_code,
    )
    # Require TOS in vertex
    return busca_svs


#####################
### Line builders ###
#####################


@register_line_builder(turbo_lines)
@configurable
def DownstreamBuScaInclusive2H_DD(
    name="Hlt2QEE_DownstreamBuScaInclusive2H_DD", prescale=1.0
):
    # We actually doesn't require any PID cuts, so it's inclusive
    pions = make_down_pions()

    algs = (
        upfront_reconstruction()
        + [require_pvs(_make_pvs())]
        + [make_DownstreamBuScaInclusive2H_DD(pions)]
    )
    return Hlt2Line(
        name=name,
        algs=algs,
        hlt1_filter_code=["Hlt1DownstreamBuSca.*Decision"],
        prescale=prescale,
        monitoring_variables=("pt", "eta", "m", "vchi2", "ipchi2", "n_candidates"),
    )
