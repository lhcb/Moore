###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define HLT2 line for ``DiMuonNoIP``.
"""

import Functors as F
from Functors import require_all
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.event_filters import require_pvs
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction
from RecoConf.standard_particles import make_ismuon_long_muon

turbo_lines = {}
full_lines = {}


def filter_muons(particles, pvs, min_ipchi2, pid_mu, p_min=5 * GeV):
    cut = require_all(
        F.P > p_min,
        F.MINIPCHI2CUT(IPChi2Cut=min_ipchi2, Vertices=pvs),
    )
    if pid_mu:
        cut = (F.PID_MU > pid_mu) & cut
    return ParticleFilter(particles, F.FILTER(cut))


hlt1_dimuonnoip_filter = ["Hlt1DiMuonNoIPDecision"]
hlt1_dimuonnoipss_filter = ["Hlt1DiMuonNoIP_SSDecision"]


@configurable
def make_dimuons(
    pvs,
    comb_maxdoca=0.1 * mm,
    vchi2pdof_max=9,
    min_pt_prod=1 * GeV * GeV,
    min_ipchi2=0,
    min_mass=2 * F.PDG_MASS("mu+"),
    max_mass=110 * GeV,
    opposite_sign=True,
    pid_mu=-10,
    ownpvvdchi2_min=0,
):
    muons = make_ismuon_long_muon()
    filtered_muons = filter_muons(muons, pvs, min_ipchi2, pid_mu)
    combination_code = require_all(
        F.MAXSDOCACUT(comb_maxdoca),
        F.CHILD(1, F.PT) * F.CHILD(2, F.PT) > min_pt_prod,
        in_range(min_mass, F.MASS, max_mass),
    )
    vertex_code = require_all(
        F.CHI2DOF < vchi2pdof_max, F.OWNPVFDCHI2 > ownpvvdchi2_min
    )
    if opposite_sign:
        DecayDescriptor = "J/psi(1S) -> mu+ mu-"
    else:
        DecayDescriptor = "[J/psi(1S) -> mu+ mu+]cc"
    return ParticleCombiner(
        Inputs=[filtered_muons, filtered_muons],
        DecayDescriptor=DecayDescriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@register_line_builder(turbo_lines)
def dimuonnoip_massrange1_line(name="Hlt2QEE_DiMuonNoIP_massRange1", prescale=1):
    pvs = make_pvs()
    dimuonnoip = make_dimuons(pvs, max_mass=740 * MeV)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dimuonnoip],
        prescale=prescale,
        hlt1_filter_code=hlt1_dimuonnoip_filter,
    )


@register_line_builder(turbo_lines)
def dimuonnoip_massrange2_line(name="Hlt2QEE_DiMuonNoIP_massRange2", prescale=1):
    pvs = make_pvs()
    dimuonnoip = make_dimuons(pvs, min_mass=740 * MeV, max_mass=1.1 * GeV)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dimuonnoip],
        prescale=prescale,
        hlt1_filter_code=hlt1_dimuonnoip_filter,
    )


@register_line_builder(turbo_lines)
def dimuonnoip_massrange3_line(name="Hlt2QEE_DiMuonNoIP_massRange3", prescale=1):
    pvs = make_pvs()
    dimuonnoip = make_dimuons(pvs, min_mass=1.1 * GeV, max_mass=3 * GeV)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dimuonnoip],
        prescale=prescale,
        hlt1_filter_code=hlt1_dimuonnoip_filter,
    )


@register_line_builder(turbo_lines)
def dimuonnoip_massrange4_line(name="Hlt2QEE_DiMuonNoIP_massRange4", prescale=1):
    pvs = make_pvs()
    dimuonnoip = make_dimuons(pvs, min_mass=3 * GeV, max_mass=3.2 * GeV)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dimuonnoip],
        prescale=prescale,
        hlt1_filter_code=hlt1_dimuonnoip_filter,
    )


@register_line_builder(turbo_lines)
def dimuonnoip_massrange5_line(name="Hlt2QEE_DiMuonNoIP_massRange5", prescale=1):
    pvs = make_pvs()
    dimuonnoip = make_dimuons(pvs, min_mass=3.2 * GeV, max_mass=9 * GeV)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dimuonnoip],
        prescale=prescale,
        hlt1_filter_code=hlt1_dimuonnoip_filter,
    )


@register_line_builder(turbo_lines)
def dimuonnoip_massrange6_line(name="Hlt2QEE_DiMuonNoIP_massRange6", prescale=1):
    pvs = make_pvs()
    dimuonnoip = make_dimuons(pvs, min_mass=9 * GeV)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dimuonnoip],
        prescale=prescale,
        hlt1_filter_code=hlt1_dimuonnoip_filter,
    )


@register_line_builder(turbo_lines)
def displaceddimuon_line(name="Hlt2QEE_DisplacedDiMuon", prescale=1):
    pvs = make_pvs()
    dimuonnoip = make_dimuons(pvs, min_ipchi2=16, ownpvvdchi2_min=30)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dimuonnoip],
        prescale=prescale,
    )


@register_line_builder(turbo_lines)
def dimuonnoip_allmasses_line(
    name="Hlt2QEE_DiMuonNoIP_prescaledAllMass", prescale=0.1
):  # prescale down to an acceptable rate
    pvs = make_pvs()
    dimuonnoip = make_dimuons(pvs)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dimuonnoip],
        prescale=prescale,
        hlt1_filter_code=hlt1_dimuonnoip_filter,
    )


@register_line_builder(turbo_lines)
def dimuonnoip_massrange1_ss_line(
    name="Hlt2QEE_DiMuonNoIP_massRange1_ss", prescale=0.1
):
    pvs = make_pvs()
    dimuonnoip = make_dimuons(pvs, max_mass=740 * MeV, opposite_sign=False)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dimuonnoip],
        prescale=prescale,
        hlt1_filter_code=hlt1_dimuonnoipss_filter,
    )


@register_line_builder(turbo_lines)
def dimuonnoip_massrange2_ss_line(
    name="Hlt2QEE_DiMuonNoIP_massRange2_ss", prescale=0.1
):
    pvs = make_pvs()
    dimuonnoip = make_dimuons(
        pvs, min_mass=740 * MeV, max_mass=1.1 * GeV, opposite_sign=False
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dimuonnoip],
        prescale=prescale,
        hlt1_filter_code=hlt1_dimuonnoipss_filter,
    )


@register_line_builder(turbo_lines)
def dimuonnoip_massrange3_ss_line(
    name="Hlt2QEE_DiMuonNoIP_massRange3_ss", prescale=0.1
):
    pvs = make_pvs()
    dimuonnoip = make_dimuons(
        pvs, min_mass=1.1 * GeV, max_mass=3 * GeV, opposite_sign=False
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dimuonnoip],
        prescale=prescale,
        hlt1_filter_code=hlt1_dimuonnoipss_filter,
    )


@register_line_builder(turbo_lines)
def dimuonnoip_massrange4_ss_line(
    name="Hlt2QEE_DiMuonNoIP_massRange4_ss", prescale=0.1
):
    pvs = make_pvs()
    dimuonnoip = make_dimuons(
        pvs, min_mass=3 * GeV, max_mass=3.2 * GeV, opposite_sign=False
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dimuonnoip],
        prescale=prescale,
        hlt1_filter_code=hlt1_dimuonnoipss_filter,
    )


@register_line_builder(turbo_lines)
def dimuonnoip_massrange5_ss_line(
    name="Hlt2QEE_DiMuonNoIP_massRange5_ss", prescale=0.1
):
    pvs = make_pvs()
    dimuonnoip = make_dimuons(
        pvs, min_mass=3.2 * GeV, max_mass=9 * GeV, opposite_sign=False
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dimuonnoip],
        prescale=prescale,
        hlt1_filter_code=hlt1_dimuonnoipss_filter,
    )


@register_line_builder(turbo_lines)
def dimuonnoip_massrange6_ss_line(
    name="Hlt2QEE_DiMuonNoIP_massRange6_ss", prescale=0.1
):
    pvs = make_pvs()
    dimuonnoip = make_dimuons(pvs, min_mass=9 * GeV, opposite_sign=False)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dimuonnoip],
        prescale=prescale,
        hlt1_filter_code=hlt1_dimuonnoipss_filter,
    )


@register_line_builder(turbo_lines)
def displaceddimuon_ss_line(name="Hlt2QEE_DisplacedDiMuon_ss", prescale=0.1):
    pvs = make_pvs()
    dimuonnoip = make_dimuons(
        pvs, min_ipchi2=16, ownpvvdchi2_min=30, opposite_sign=False
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dimuonnoip],
        prescale=prescale,
    )


@register_line_builder(full_lines)
def dimuonnoip_full_event_line(
    name="Hlt2QEE_DiMuonNoIP_Full", prescale=0.004
):  # prescale down to an acceptable rate
    pvs = make_pvs()
    dimuonnoip = make_dimuons(pvs, pid_mu=None)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dimuonnoip],
        prescale=prescale,
        persistreco=True,
        hlt1_filter_code=hlt1_dimuonnoip_filter,
    )


@register_line_builder(full_lines)
def dimuonnoip_full_event_ss_line(
    name="Hlt2QEE_DiMuonNoIP_ssFull", prescale=0.004
):  # prescale down to an acceptable rate
    pvs = make_pvs()
    dimuonnoip = make_dimuons(pvs, opposite_sign=False, pid_mu=None)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dimuonnoip],
        prescale=prescale,
        hlt1_filter_code=hlt1_dimuonnoipss_filter,
        persistreco=True,
    )
