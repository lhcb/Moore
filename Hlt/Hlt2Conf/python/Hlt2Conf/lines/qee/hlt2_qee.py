###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Registration of all the QEE HLT2 lines.
"""

from Hlt2Conf.lines.qee import (
    b_to_majolep_majo_to_leplep,
    b_to_majolep_majo_to_leppi,
    busca,
    dielectron_persist_photons,
    dimuon_no_ip,
    diphoton,
    drellyan,
    high_mass_dielec,
    high_mass_dimuon,
    jets,
    muon_detector_showers,
    quarkonia,
    sexaquark,
    single_high_pt_electron,
    single_high_pt_muon,
    ttrack_llps,
    wz_boson_rare_decays,
)

full_lines = {}
full_lines.update(single_high_pt_muon.all_lines)
full_lines.update(single_high_pt_electron.all_lines)
full_lines.update(jets.all_lines)
full_lines.update(high_mass_dimuon.all_lines)
full_lines.update(high_mass_dielec.all_lines)
full_lines.update(dimuon_no_ip.full_lines)
full_lines.update(dielectron_persist_photons.full_lines)
full_lines.update(ttrack_llps.full_lines)

turbo_lines = {}
turbo_lines.update(quarkonia.all_lines)
turbo_lines.update(b_to_majolep_majo_to_leplep.all_lines)
turbo_lines.update(b_to_majolep_majo_to_leppi.all_lines)
turbo_lines.update(diphoton.all_lines)
turbo_lines.update(dimuon_no_ip.turbo_lines)
turbo_lines.update(drellyan.all_lines)
turbo_lines.update(dielectron_persist_photons.turbo_lines)
turbo_lines.update(wz_boson_rare_decays.all_lines)
turbo_lines.update(ttrack_llps.turbo_lines)
turbo_lines.update(muon_detector_showers.all_lines)
turbo_lines.update(sexaquark.all_lines)
turbo_lines.update(busca.turbo_lines)
