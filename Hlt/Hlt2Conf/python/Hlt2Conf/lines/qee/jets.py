###############################################################################
# (c) Copyright 2020-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.event_filters import require_pvs
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction

from Hlt2Conf.lines.qee.qee_builders import make_gamma_jet
from Hlt2Conf.standard_jets import make_dijets, make_jets, make_Trijets

all_lines = {}
all_reference_run_lines = {}

_hlt1_light_jet_filter = ["Hlt1ConeJet(15|30|50|100)GeVDecision"]
_default_monitoring_variables = ("pt", "eta", "n_candidates", "ipchi2", "vchi2")


##### Functions for sprucing lines #####
@configurable
def make_SVTagDijets_cand():
    line_alg = make_dijets(tagpair=("SV", "SV"), prod_pt_min=30 * GeV, min_dphi=1.5)
    return line_alg


@configurable
def make_Trijets_cand():
    line_alg = make_Trijets(tags=(None, None, None), prod_pt_min=30 * GeV)
    return line_alg


@configurable
def make_TrijetsTwoSVTag_cand():
    line_alg = make_Trijets(tags=("SV", "SV", None), prod_pt_min=30 * GeV)
    return line_alg


##### Template functions for hlt2 lines #####
@configurable
def _dijet_line_template(
    name,
    prescale,
    prod_pt_min,
    persistreco=True,
    hlt1_filter=False,
    tags=None,
    min_dijet_mass=0.0 * GeV,
):
    jets = make_dijets(
        tagpair=(None, None) if tags is None else tags,
        prod_pt_min=prod_pt_min,
        min_dphi=0.0,
        min_dijet_mass=min_dijet_mass,
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        hlt1_filter_code=_hlt1_light_jet_filter if hlt1_filter else "",
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco,
        monitoring_variables=_default_monitoring_variables,
    )


@configurable
def _incjet_line_template(name, prescale, prod_pt_min, persistreco=True):
    jets = make_jets(pt_min=prod_pt_min)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), jets],
        hlt1_filter_code=_hlt1_light_jet_filter,
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco,
        monitoring_variables=_default_monitoring_variables,
    )


############ SV Tag ####################
@register_line_builder(all_lines)
@configurable
def diSVTagJet10GeV_line(name="Hlt2QEE_DiSVTagJet10GeVFull", prescale=0.0025):
    return _dijet_line_template(
        name=name, prescale=prescale, prod_pt_min=10 * GeV, tags=("SV", "SV")
    )


@register_line_builder(all_lines)
@configurable
def diSVTagJet15GeV_line(name="Hlt2QEE_DiSVTagJet15GeVFull", prescale=0.01):
    return _dijet_line_template(
        name=name, prescale=prescale, prod_pt_min=15 * GeV, tags=("SV", "SV")
    )


@register_line_builder(all_lines)
@configurable
def diSVTagJet20GeV_line(name="Hlt2QEE_DiSVTagJet20GeVFull", prescale=0.05):
    return _dijet_line_template(
        name=name, prescale=prescale, prod_pt_min=20 * GeV, tags=("SV", "SV")
    )


@register_line_builder(all_lines)
@configurable
def diSVTagJet25GeV_line(name="Hlt2QEE_DiSVTagJet25GeVFull", prescale=0.1):
    return _dijet_line_template(
        name=name, prescale=prescale, prod_pt_min=25 * GeV, tags=("SV", "SV")
    )


@register_line_builder(all_lines)
@configurable
def diSVTagJet30GeV_line(name="Hlt2QEE_DiSVTagJet30GeVFull", prescale=0.25):
    return _dijet_line_template(
        name=name, prescale=prescale, prod_pt_min=30 * GeV, tags=("SV", "SV")
    )


@register_line_builder(all_lines)
@configurable
def diSVTagJet35GeV_line(name="Hlt2QEE_DiSVTagJet35GeVFull", prescale=1.0):
    return _dijet_line_template(
        name=name, prescale=prescale, prod_pt_min=35 * GeV, tags=("SV", "SV")
    )


############ Asymmetric dijet lines ################
@register_line_builder(all_lines)
@configurable
def DiJetIncSVTag_line(name="Hlt2QEE_DiJetIncSVTag_pT25M40Full", prescale=1):
    return _dijet_line_template(
        name=name,
        prescale=prescale,
        prod_pt_min=25 * GeV,
        tags=(None, "SV"),
        min_dijet_mass=40 * GeV,
    )


############ Topo Tag ####################
@register_line_builder(all_lines)
@configurable
def diTopoTagJet10GeV_line(name="Hlt2QEE_DiTopoTagJet10GeVFull", prescale=0.001):
    return _dijet_line_template(
        name=name, prescale=prescale, prod_pt_min=10 * GeV, tags=("TOPO", "TOPO")
    )


@register_line_builder(all_lines)
@configurable
def diTopoTagJet15GeV_line(name="Hlt2QEE_DiTopoTagJet15GeVFull", prescale=0.001):
    return _dijet_line_template(
        name=name, prescale=prescale, prod_pt_min=15 * GeV, tags=("TOPO", "TOPO")
    )


@register_line_builder(all_lines)
@configurable
def diTopoTagJet20GeV_line(name="Hlt2QEE_DiTopoTagJet20GeVFull", prescale=0.005):
    return _dijet_line_template(
        name=name, prescale=prescale, prod_pt_min=20 * GeV, tags=("TOPO", "TOPO")
    )


@register_line_builder(all_lines)
@configurable
def diTopoTagJet25GeV_line(name="Hlt2QEE_DiTopoTagJet25GeVFull", prescale=0.05):
    return _dijet_line_template(
        name=name, prescale=prescale, prod_pt_min=25 * GeV, tags=("TOPO", "TOPO")
    )


@register_line_builder(all_lines)
@configurable
def diTopoTagJet30GeV_line(name="Hlt2QEE_DiTopoTagJet30GeVFull", prescale=0.05):
    return _dijet_line_template(
        name=name, prescale=prescale, prod_pt_min=30 * GeV, tags=("TOPO", "TOPO")
    )


@register_line_builder(all_lines)
@configurable
def diTopoTagJet35GeV_line(name="Hlt2QEE_DiTopoTagJet35GeVFull", prescale=0.1):
    return _dijet_line_template(
        name=name, prescale=prescale, prod_pt_min=35 * GeV, tags=("TOPO", "TOPO")
    )


############# Inclusive jets ####################
@register_line_builder(all_lines)
@configurable
def IncJet15GeV_line(name="Hlt2QEE_IncJet15GeVFull", prescale=0.05):
    return _incjet_line_template(name=name, prescale=prescale, prod_pt_min=15 * GeV)


@register_line_builder(all_lines)
@configurable
def IncJet25GeV_line(name="Hlt2QEE_IncJet25GeVFull", prescale=0.1):
    return _incjet_line_template(name=name, prescale=prescale, prod_pt_min=25 * GeV)


@register_line_builder(all_lines)
@configurable
def IncJet35GeV_line(name="Hlt2QEE_IncJet35GeVFull", prescale=0.5):
    return _incjet_line_template(name=name, prescale=prescale, prod_pt_min=35 * GeV)


@register_line_builder(all_lines)
@configurable
def IncJet45GeV_line(name="Hlt2QEE_IncJet45GeVFull", prescale=1.0):
    return _incjet_line_template(name=name, prescale=prescale, prod_pt_min=45 * GeV)


@register_line_builder(all_lines)
@configurable
def IncDiJet15GeV_line(name="Hlt2QEE_IncDiJet15GeVFull", prescale=0.5):
    return _dijet_line_template(
        name=name, prescale=prescale, prod_pt_min=15 * GeV, hlt1_filter=True
    )


@register_line_builder(all_lines)
@configurable
def IncDiJet20GeV_line(name="Hlt2QEE_IncDiJet20GeVFull", prescale=1.0):
    return _dijet_line_template(
        name=name, prescale=prescale, prod_pt_min=20 * GeV, hlt1_filter=True
    )


@register_line_builder(all_lines)
@configurable
def IncDiJet25GeV_line(name="Hlt2QEE_IncDiJet25GeVFull", prescale=1.0):
    return _dijet_line_template(
        name=name, prescale=prescale, prod_pt_min=25 * GeV, hlt1_filter=True
    )


@register_line_builder(all_lines)
@configurable
def IncDiJet30GeV_line(name="Hlt2QEE_IncDiJet30GeVFull", prescale=1.0):
    return _dijet_line_template(
        name=name, prescale=prescale, prod_pt_min=30 * GeV, hlt1_filter=True
    )


@register_line_builder(all_lines)
@configurable
def IncDiJet35GeV_line(name="Hlt2QEE_IncDiJet35GeVFull", prescale=1.0):
    return _dijet_line_template(
        name=name, prescale=prescale, prod_pt_min=35 * GeV, hlt1_filter=True
    )


# Gamma + jet lines.
# TODO: For now, only add these for the pp reference run.
all_reference_run_lines.update(all_lines)


@register_line_builder(all_reference_run_lines)
@configurable
def GammaLLJet_line(
    name="Hlt2QEE_GammaLLJetFull", prescale=1, persistreco=True, hlt1_filter=True
):
    combos = make_gamma_jet(photon_type="LL")
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), combos],
        hlt1_filter_code=_hlt1_light_jet_filter if hlt1_filter else "",
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco,
        monitoring_variables=_default_monitoring_variables,
    )


@register_line_builder(all_reference_run_lines)
@configurable
def GammaDDJet_line(
    name="Hlt2QEE_GammaDDJetFull", prescale=1, persistreco=True, hlt1_filter=True
):
    combos = make_gamma_jet(photon_type="DD")
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), combos],
        hlt1_filter_code=_hlt1_light_jet_filter if hlt1_filter else "",
        prescale=prescale,
        calo_clusters=True,
        persistreco=persistreco,
        monitoring_variables=_default_monitoring_variables,
    )
