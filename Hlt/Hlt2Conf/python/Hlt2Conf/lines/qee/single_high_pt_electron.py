###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the HLT2 single high-PT electron lines.
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from PyConf.Algorithms import WeightedRelTableAlg
from RecoConf.algorithms_thor import ParticleFilter
from RecoConf.reconstruction_objects import upfront_reconstruction
from RecoConf.standard_particles import make_long_electrons_with_brem

from Hlt2Conf.standard_jets import make_onlytrack_particleflow

all_lines = {}

pt_thresholds = {
    "standard": 17.5 * GeV,
    "prescale": 10.0 * GeV,
    "iso": 15.0 * GeV,
}
ecal_deposit_fractions = {"standard": 0.1, "iso": 0.05}
hcal_deposit_fractions = {"standard": 0.05}


@configurable
def make_highpt_electrons(
    name="HighPtElectronMaker_{hash}",
    min_electron_pt=pt_thresholds["standard"],
    ecal_deposit_fraction=ecal_deposit_fractions["standard"],
    hcal_deposit_fraction=hcal_deposit_fractions["standard"],
    min_electron_id=-1,
):
    code = F.require_all(
        F.PT > min_electron_pt,
        F.PID_E > min_electron_id,
        F.ELECTRONSHOWEREOP > ecal_deposit_fraction,
        F.HCALEOP < hcal_deposit_fraction,
    )
    return ParticleFilter(
        make_long_electrons_with_brem(),
        F.FILTER(code),
        name=name,
    )


@configurable
def make_highpt_isolated_electrons(
    high_pt_electrons,
    name="HighPtIsolatedElectronMaker_{hash}",
    max_cone_pt=10.0 * GeV,
    pflow_output=make_onlytrack_particleflow,
):
    ftAlg = WeightedRelTableAlg(
        ReferenceParticles=high_pt_electrons,
        InputCandidates=pflow_output(),
        Cut=in_range(0.1**2, F.DR2, 0.5**2),
    )  # Specifies size of "hollow" cone to avoid the electron's track

    ftAlg_Rels = ftAlg.OutputRelations

    code = F.require_all(F.SUMCONE(Functor=F.PT, Relations=ftAlg_Rels) < max_cone_pt)

    return ParticleFilter(
        high_pt_electrons,
        F.FILTER(code),
        name=name,
    )


@register_line_builder(all_lines)
@configurable
def single_electron_highpt_line(
    name="Hlt2QEE_SingleHighPtElectronFull", prescale=1, persistreco=True
):
    """High PT single electron line"""

    high_pt_electrons = make_highpt_electrons(min_electron_pt=pt_thresholds["standard"])

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [high_pt_electrons],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("pt", "eta", "n_candidates"),
    )


@register_line_builder(all_lines)
@configurable
def single_electron_highpt_prescale_line(
    name="Hlt2QEE_SingleHighPtElectronPrescaleFull", prescale=0.05, persistreco=True
):
    """High PT single electron line with lower pT threshold, prescaled to reduce the rate."""

    high_pt_electrons = make_highpt_electrons(min_electron_pt=pt_thresholds["prescale"])

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [high_pt_electrons],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("pt", "eta", "n_candidates"),
    )


@register_line_builder(all_lines)
@configurable
def single_electron_highpt_iso_line(
    name="Hlt2QEE_SingleHighPtElectronIsoFull", prescale=1, persistreco=True
):
    """High PT single electron line with lower pT threshold, and isolation to keep the rate down.
    Make PT cut first to drastically reduce number of times isolation algorithm is called."""

    # pflow algo populates the HltParticleFlow/Particles path for SUMCONE
    high_pt_electrons = make_highpt_electrons(
        min_electron_pt=pt_thresholds["iso"],
        ecal_deposit_fraction=ecal_deposit_fractions["iso"],
        name="HighPtElectronMakerForIso",
    )
    high_pt_isolated_electrons = make_highpt_isolated_electrons(
        high_pt_electrons, pflow_output=make_onlytrack_particleflow
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [high_pt_electrons, high_pt_isolated_electrons],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=("pt", "eta", "n_candidates"),
    )
