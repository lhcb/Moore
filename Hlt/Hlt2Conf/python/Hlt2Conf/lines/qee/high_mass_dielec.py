###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of the HLT2 Z->ee and same-sign dielectron (control sample) line.
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.reconstruction_objects import upfront_reconstruction
from RecoConf.standard_particles import make_long_electrons_with_brem

from Hlt2Conf.lines.qee.qee_monitors import _default_Z_params as z_monitoring_defaults
from Hlt2Conf.lines.qee.qee_monitors import qee_monitors1d

all_lines = {}

_default_monitoring_variables = ("pt", "eta", "n_candidates")
z_min_combination_mass, z_min_composite_mass = 30.0 * GeV, 40.0 * GeV


@configurable
def elec_filter_for_Z(particles, min_pt=0.0 * GeV):
    """An electron filter: PT, ELECTRONSHOWEREOP, HCALEOP"""
    code = F.require_all(F.PT > min_pt, F.ELECTRONSHOWEREOP > 0.1, F.HCALEOP < 0.05)
    return ParticleFilter(particles, F.FILTER(code))


@configurable
def make_Zee_cand():
    elecs = elec_filter_for_Z(make_long_electrons_with_brem(), min_pt=10.0 * GeV)
    line_alg = make_dielec_novxt(elecs, "Z0 -> e+ e-")
    return line_alg


@configurable
def make_Zeess_cand():
    elecs = elec_filter_for_Z(make_long_electrons_with_brem(), min_pt=10.0 * GeV)
    line_alg = make_dielec_novxt(elecs, "[Z0 -> e- e-]cc")
    return line_alg


@configurable
def make_dielec_novxt(
    input_elecs,
    decaydescriptor,
    min_combination_mass=z_min_combination_mass,
    min_composite_mass=z_min_composite_mass,
):
    """DiElec without any requirements but mass"""

    # pre-vertex fit, useful for controlling the combinatorics
    combination_code = F.MASS > min_combination_mass
    # post-vertex fit
    composite_code = F.MASS > min_composite_mass

    return ParticleCombiner(
        [input_elecs, input_elecs],
        DecayDescriptor=decaydescriptor,
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


@register_line_builder(all_lines)
@configurable
def z_to_e_e_line(name="Hlt2QEE_ZToEEFull", prescale=1, persistreco=True):
    """Z0 boson decay to two elecs line"""

    z02ee = make_Zee_cand()
    monitor_z02ee = qee_monitors1d(
        particles=z02ee, linename=name, monitoring_params=z_monitoring_defaults
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [z02ee, monitor_z02ee],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_default_monitoring_variables,
    )


@register_line_builder(all_lines)
@configurable
def same_sign_dielec_line(
    name="Hlt2QEE_DiElectronHighMassSameSignFull", prescale=1, persistreco=True
):
    """Z0 boson decay to two same-sign elecs line"""

    eess = make_Zeess_cand()
    monitor_eess = qee_monitors1d(
        particles=eess, linename=name, monitoring_params=z_monitoring_defaults
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [eess, monitor_eess],
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_default_monitoring_variables,
    )
