###############################################################################
# (c) Copyright 2021-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of lines to be used for testing sprucing machinery. For these lines must be set PROCESS = spruce
"""

from math import cos

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Moore.config import Hlt2Line, SpruceLine
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.event_filters import require_gec, require_pvs
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction
from RecoConf.standard_particles import (
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
    make_photons,
)
from SelAlgorithms.monitoring import histogram_1d, monitor

sprucing_lines = {}


#########################################################
### Standalone code to make B->Kstgamma Sprucing line ###
def make_filter_tracks(
    make_particles=make_has_rich_long_pions,
    make_pvs=make_pvs,
    name="SpruceTest_has_rich_long_pions",
    pt_min=250.0 * MeV,
    p_min=2.0 * GeV,
    mipchi2dvprimary_min=None,
    pid=None,
):
    """
    Build generic long tracks.
    """
    code = F.require_all(F.PT > pt_min, F.P > p_min)

    if pid is not None:
        code &= pid

    if mipchi2dvprimary_min:
        pvs = make_pvs()
        code &= F.MINIPCHI2(pvs) > mipchi2dvprimary_min

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


def make_test_has_rich_detached_pions(
    name="test_has_rich_detached_pions_{hash}",
    p_min=2.0 * GeV,
    pt_min=250.0 * MeV,
    mipchi2dvprimary_min=4.0,
    pid=(F.PID_K <= 0.0),
):
    """
    Return RD detached pions with hasRich.
    """
    return make_filter_tracks(
        name=name,
        make_particles=make_has_rich_long_pions,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid,
    )


def make_test_has_rich_detached_kaons(
    name="SpruceTest_has_rich_detached_kaons_{hash}",
    p_min=2.0 * GeV,
    pt_min=250.0 * MeV,
    mipchi2dvprimary_min=4.0,
    pid=(F.PID_K > 0.0),
):
    """
    Return detached kaons with hasRich.
    """
    return make_filter_tracks(
        name=name,
        make_particles=make_has_rich_long_kaons,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid,
    )


def make_test_detached_kstar0s(
    name="test_detached_kstar0s_{hash}",
    make_pvs=make_pvs,
    am_min=0 * MeV,
    am_max=2600.0 * MeV,
    pi_p_min=2.0 * GeV,
    pi_pt_min=250.0 * MeV,
    pi_ipchi2_min=9.0,
    # if the RICH can not distinguish between kaons and pions then
    # the PID will be set to zero, and most often these will correspond
    # to real pions
    pi_pid=F.PID_K < 1e-5,
    k_p_min=2.0 * GeV,
    k_pt_min=250.0 * MeV,
    k_ipchi2_min=9.0,
    k_pid=(F.PID_K > 2.0),
    kstar0_pt_min=400.0 * MeV,
    adocachi2cut=30.0,
    maxdoca=0.5 * mm,
    vchi2pdof_max=16.0,
    bpvipchi2_min=None,
    same_sign=False,
):
    """
    Build Kstar0 candidates. Approximately corresponding to the Run2
    "StdVeryLooseDetachedKstar" cuts.
    """

    pions = make_test_has_rich_detached_pions(
        p_min=pi_p_min, pt_min=pi_pt_min, mipchi2dvprimary_min=pi_ipchi2_min, pid=pi_pid
    )
    kaons = make_test_has_rich_detached_kaons(
        p_min=k_p_min, pt_min=k_pt_min, mipchi2dvprimary_min=k_ipchi2_min, pid=k_pid
    )
    descriptor = "[K*(892)0 -> K+ pi-]cc"
    if same_sign:
        descriptor = "[K*(892)0 -> K+ pi+]cc"
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.MAXSDOCACHI2CUT(adocachi2cut),
        F.MAXSDOCACUT(maxdoca),
    )
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max, F.PT > kstar0_pt_min)

    if bpvipchi2_min is not None:
        pvs = make_pvs()
        vertex_code &= F.BPVIPCHI2(pvs) > bpvipchi2_min

    return ParticleCombiner(
        [kaons, pions],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def make_test_photons(
    name="SpruceTest_photons_{hash}",
    make_particles=make_photons,
    IsPhoton_min=0.0,
    IsNotH_min=0.0,
    E19_min=0.0,
    e_min=0.0 * MeV,
    et_min=50.0 * MeV,
):
    """For the time being just a preliminary selection while the neutrals calibration in progress"""

    code = F.require_all(
        F.PT > et_min,
        F.P > e_min,
        F.IS_PHOTON > IsPhoton_min,
        F.IS_NOT_H > IsNotH_min,
        F.CALO_NEUTRAL_1TO9_ENERGY_RATIO > E19_min,
    )
    return ParticleFilter(make_particles(), F.FILTER(code), name=name)


def make_test_b2xgamma_excl(
    intermediate,
    photons,
    pvs,
    descriptor,
    dira_min,
    name,
    comb_m_min=4200 * MeV,
    comb_m_max=6400 * MeV,
    pt_min=2000 * MeV,
    bpv_ipchi2_max=12,
):
    combination_code = F.require_all(
        F.PT > pt_min,
        in_range(comb_m_min, F.MASS, comb_m_max),
    )
    vertex_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.PT > pt_min,
        F.BPVDIRA(pvs) > dira_min,
        F.BPVIPCHI2(pvs) < bpv_ipchi2_max,
    )
    # return particle combiner
    return ParticleCombiner(
        name=name,
        Inputs=[intermediate, photons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def b2kstgamma_line():
    pvs = make_pvs()

    kst = make_test_detached_kstar0s(
        name="Test_BdToKstGamma_detached_kstar0s",
        pi_p_min=2.0 * GeV,
        pi_pt_min=500.0 * MeV,
        k_p_min=3.0 * GeV,
        k_pt_min=500.0 * MeV,
        k_ipchi2_min=11.0,
        pi_ipchi2_min=11.0,
        k_pid=(F.PID_K > 0.0),
        kstar0_pt_min=1500.0 * MeV,
        vchi2pdof_max=15.0,
        am_min=705.0 * MeV,
        am_max=1085.0 * MeV,
    )

    photons = make_test_photons(
        et_min=2.0 * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2
    )

    b0 = make_test_b2xgamma_excl(
        intermediate=kst,
        photons=photons,
        pvs=pvs,
        descriptor="[B0 -> K*(892)0 gamma]cc",
        dira_min=cos(0.045),
        name="Test_BdToKstGamma_Combiner",
    )

    return kst, b0


#########################################################
#########################################################


def SpruceTest_prefilter(require_GEC=False):
    """
    Args:
        require_GEC (bool, optional): require the General Event Cut. Default to False.
    """
    gec = [require_gec()] if require_GEC else []
    return upfront_reconstruction() + gec + [require_pvs(make_pvs())]


def Test_sprucing_line(name="SpruceTest_SpruceTest", prescale=1):
    kst, b0 = b2kstgamma_line()
    kst_mon = monitor(
        name=name + "_kst_Monitor",
        data=kst,
        histograms=[
            histogram_1d(
                name="m",
                title="m",
                label="MASS",
                functor=F.MASS,
                bins=50,
                range=((900 - 5 * 5.0) * MeV, (1100 + 5 * 5.0) * MeV),
            )
        ],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        raw_banks=["Calo"],
        algs=SpruceTest_prefilter() + [kst_mon, kst, b0],
    )


def Test_sprucing_BdToKstGamma(name="SpruceTest_BdToKstGamma", prescale=1.0):
    kst, b0 = b2kstgamma_line()

    return SpruceLine(
        name=name,
        prescale=prescale,
        raw_banks=["Calo"],
        algs=SpruceTest_prefilter() + [kst, b0],
    )


def Test_extraoutputs_sprucing_line(name="SpruceTest_ExtraOutputs", prescale=1):
    from RecoConf.standard_particles import make_long_pions

    kst, b0 = b2kstgamma_line()

    pvs = make_pvs()

    LongT = ParticleFilter(make_long_pions(), F.FILTER(F.MINIPCHI2(pvs) > 0))

    return SpruceLine(
        name=name,
        prescale=prescale,
        extra_outputs=[("LongTracks", LongT)],
        calo_clusters=True,
        calo_digits=True,
        pv_tracks=True,
        track_ancestors=True,
        persistreco=True,
        algs=SpruceTest_prefilter() + [kst, b0],
    )


# This lives here as its used to test sprucing
# Bespoke version of `b2kstgamma_line` that has extra_outputs and all the other persistency options set to true and Rich rawbank persisted
def Test_extraoutputs_hlt2_line(name="Hlt2Test_ExtraOutputs", prescale=1):
    from RecoConf.standard_particles import make_long_pions

    kst, b0 = b2kstgamma_line()

    pvs = make_pvs()

    LongT = ParticleFilter(make_long_pions(), F.FILTER(F.MINIPCHI2(pvs) > 0))
    LongerT = ParticleFilter(make_long_pions(), F.FILTER(F.MINIPCHI2(pvs) > 0.1))

    return Hlt2Line(
        name=name,
        prescale=prescale,
        extra_outputs=[("LongTracks", LongT), ("LongerTracks", LongerT)],
        calo_clusters=True,
        calo_digits=True,
        pv_tracks=True,
        track_ancestors=True,
        persistreco=True,
        algs=SpruceTest_prefilter() + [kst, b0],
        raw_banks=["Rich"],
    )


def Test_persistreco_sprucing_line(name="SpruceTest_PersistReco", prescale=1):
    kst, b0 = b2kstgamma_line()

    return SpruceLine(
        name=name,
        prescale=prescale,
        persistreco=True,
        algs=SpruceTest_prefilter() + [kst, b0],
    )


### line for testing selective persistency of detector raw banks
### currently it is asking for one addiitonal 'FT' bank
def Test_sprucing_rawbank_sp_line(name="SpruceTest_rawbank_sp", prescale=1):
    kst, b0 = b2kstgamma_line()

    return SpruceLine(
        name=name,
        prescale=prescale,
        raw_banks=["FT"],
        algs=SpruceTest_prefilter() + [kst, b0],
    )
