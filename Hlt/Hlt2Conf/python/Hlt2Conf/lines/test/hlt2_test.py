###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Generic lines used for testing purposes. Some are snapshots of production lines, but are not used for production.
Follow https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/tutorials/hlt2_line.html#code-design-guidelines

Note on naming: this module names all combiners their names and names of lines begin with "Test_" and "Hlt2Test_", respectively, to be able to easily group them in performace tests and spot them when inspecting log files.
Filters are not named, see https://gitlab.cern.ch/lhcb/Moore/-/issues/378 and https://gitlab.cern.ch/lhcb/Moore/-/issues/380
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, picosecond, ps
from GaudiKernel.SystemOfUnits import micrometer as um
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from Moore.persistence.persistreco import persistreco_line_outputs
from PyConf.Algorithms import (
    AdvancedCloneKiller,
    ParticleUnbiasedPVAdder,
    Run2SSKaonTagger,
)
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.event_filters import require_gec, require_pvs
from RecoConf.reconstruction_objects import (
    make_extended_pvs,
    make_pvs,
    reconstruction,
    upfront_reconstruction,
)
from RecoConf.standard_particles import (
    make_down_pions,
    make_has_rich_down_protons,
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
    make_has_rich_long_protons,
    make_ismuon_long_muon,
    make_long_electrons_with_brem,
    make_long_kaons,
    make_long_muons,
    make_long_pions,
    make_phi2kk,
    make_photons,
)
from SelAlgorithms.monitoring import histogram_1d, monitor

from Hlt2Conf.flavourTagging import run2_all_taggers

hlt2_test_lines = {}


def prefilters():
    return [require_pvs(make_pvs())]


# re-definitions of functors
def _DZ_CHILD(i):
    return F.CHILD(i, F.END_VZ) - F.END_VZ


def _MIPCHI2_MIN(cut, pvs=make_pvs):
    return F.MINIPCHI2CUT(IPChi2Cut=cut, Vertices=pvs())


# Dummy line, included to trigger neutral reconstruction.
@register_line_builder(hlt2_test_lines)
def photons_line(name="Hlt2Test_Photons", prescale=1):
    photons = make_photons()
    line_alg = ParticleFilter(Input=photons, Cut=F.FILTER(F.P > 40 * GeV))
    # test monitoring of neutral basics
    photon_mon = monitor(
        name="Monitor_Test_Photons_IsPhoton",
        data=photons,
        histograms=[
            histogram_1d(
                functor=F.VALUE_OR(F.NaN) @ F.IS_PHOTON,
                name="/Test_Photons/IsPhoton",
                title="IsPhoton",
                label="IsPhoton",
                bins=100,
                range=(0, 1),
            )
        ],
    )
    return Hlt2Line(name=name, prescale=prescale, algs=[line_alg, photon_mon])


# Dummy line, testing on top of neutrals the AdditionalInfo functors
@register_line_builder(hlt2_test_lines)
def filtered_photons_line(name="Hlt2Test_FilteredPhotons", prescale=1):
    photons = make_photons()
    code = F.require_all(
        F.P > 40 * GeV,
        F.IS_PHOTON > 0.5,
        F.IS_NOT_H > 0.3,
        F.CALO_NEUTRAL_SHOWER_SHAPE < 1500,
    )
    line_alg = ParticleFilter(Input=photons, Cut=F.FILTER(code))
    return Hlt2Line(name=name, prescale=prescale, algs=[line_alg])


# Dummy line, testing the algorithms in Rec creating and cutting on multiple relation tables
from PyConf.Algorithms import (
    LHCb__Phys__RelationTables__FilterRelTables as FilterRelTables,
)
from PyConf.Algorithms import (
    LHCb__Phys__RelationTables__MakeRelTables as MakeRelTables,
)


@register_line_builder(hlt2_test_lines)
def mRT_photons_line(name="Hlt2Test_mRTPhotons", prescale=1):
    def tr(**outputs):
        return {"OutputLocation": [v for (k, v) in outputs.items()]}

    ai = ["IsPhoton", "ShowerShape", "IsNotH"]
    tables = MakeRelTables(
        InputParticles=make_photons(),
        AdditionalInfo=ai,
        outputs={prop: None for prop in ai},
        output_transform=tr,
    ).outputs
    predicateCut = (
        (F.COLUMN(ColumnLabel=tables["IsPhoton"].location) > 0.5)
        & (F.COLUMN(ColumnLabel=tables["ShowerShape"].location) < 1500)
        & (F.COLUMN(ColumnLabel=tables["IsNotH"].location) > 0.3)
    )
    photons = FilterRelTables(
        InputTables=[dh for dh in tables.values()], RelCuts=predicateCut
    ).OutputLocation
    code = F.require_all(F.P > 40 * GeV)
    line_alg = ParticleFilter(photons, F.FILTER(code))
    return Hlt2Line(name=name, prescale=prescale, algs=[line_alg])


# Dummy line, testing the algorithms in Rec creating and cutting on single relation tables
from PyConf.Algorithms import (
    LHCb__Phys__RelationTables__AiToRelTable as AiToRelTable,
)
from PyConf.Algorithms import (
    LHCb__Phys__RelationTables__FilterRelTable as FilterRelTable,
)


@register_line_builder(hlt2_test_lines)
def sRT_photons_line(name="Hlt2Test_sRTPhotons", prescale=1):
    relTab = AiToRelTable(
        InputParticles=make_photons(), AdditionalInfo="ShowerShape"
    ).OutputLocation
    predicateCut = F.IDENTITY < 1500
    photons = FilterRelTable(InputTable=relTab, Predicate=predicateCut).OutputLocation
    code = F.require_all(
        F.P > 40 * GeV,
        F.IS_PHOTON > 0.5,
        F.IS_NOT_H > 0.3,
    )
    line_alg = ParticleFilter(photons, F.FILTER(code))
    return Hlt2Line(name=name, prescale=prescale, algs=[line_alg])


# From Hlt2Conf/lines/monitoring/pi0_line.py
@register_line_builder(hlt2_test_lines)
def pi0_to_gammagamma_line(name="Hlt2Test_Pi0ToGammaGamma", prescale=1):
    code = F.require_all(F.PT > 250 * MeV, F.P > 2 * GeV, F.IS_NOT_H > 0.2)
    photons = ParticleFilter(Input=make_photons(), Cut=F.FILTER(code))

    combination_code = F.require_all(
        F.PT > 1400 * MeV,
        F.math.in_range(100 * MeV, F.MASS, 200 * MeV),
    )
    pi0s = ParticleCombiner(
        name="Test_Pi0ToGammaGamma_Combiner",
        Inputs=[photons, photons],
        ParticleCombiner="ParticleAdder",
        DecayDescriptor="pi0 -> gamma gamma",
        CombinationCut=combination_code,
        CompositeCut=F.ALL,
    )
    return Hlt2Line(name=name, algs=[pi0s], prescale=prescale)


# From Hlt2Conf/lines/charm/hyperons.py
@register_line_builder(hlt2_test_lines)
def xicz_to_lpipi_dd_line(name="Hlt2Test_Xic0ToL0PimPip_DD"):
    pvs = make_pvs()
    down_pions = ParticleFilter(make_down_pions(), F.FILTER(F.PT > 180 * MeV))
    down_protons = ParticleFilter(
        make_has_rich_down_protons(),
        F.FILTER(F.require_all(F.PT > 700 * MeV, F.P > 10 * GeV, F.PID_P > -5.0)),
    )
    dd_lambdas = ParticleCombiner(
        [down_protons, down_pions],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name="Test_Hyperons_Lambda_DD",
        CombinationCut=F.require_all(
            F.MASS < 1190 * MeV,
            F.MAXSDOCACUT(2 * mm),
            F.MAXSDOCACHI2CUT(12.0),
            F.PT > 0.9 * GeV,
            F.P > 13 * GeV,
            F.SUM(F.PT) > 1 * GeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 1165 * MeV,
            F.PT > 1 * GeV,
            F.P > 14 * GeV,
            F.CHI2DOF < 9.0,
        ),
    )
    long_xicz_pions = ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 400 * MeV, F.P > 3 * GeV, _MIPCHI2_MIN(6.0), F.PID_K < 5.0
            ),
        ),
    )
    xic0s = ParticleCombiner(
        [dd_lambdas, long_xicz_pions, long_xicz_pions],
        DecayDescriptor="[Xi_c0 -> Lambda0 pi- pi+]cc",
        name="Test_Hyperons_Xic0ToL0PimPip_DD",
        Combination12Cut=F.require_all(
            F.MASS < 2450 * MeV,
            F.MAXSDOCACUT(800 * um),
        ),
        CombinationCut=F.require_all(
            F.SDOCA(1, 3) < 800 * um,
            F.SDOCA(2, 3) < 150 * um,
            F.math.in_range(2250 * MeV, F.MASS, 2590 * MeV),
            F.MAX(F.MINIPCHI2(pvs)) > 9.0,
            F.PT > 1.3 * GeV,
            F.P > 19 * GeV,
            F.SUM(F.PT) > 2.2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2270 * MeV, F.MASS, 2570 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 20 * GeV,
            F.CHI2DOF < 7.0,
            F.BPVVDZ(pvs) > -0.5 * mm,
            F.BPVFDCHI2(pvs) > 6.0,
            F.BPVIPCHI2(pvs) < 6.0,
            F.BPVDIRA(pvs) > 0.996,
        ),
    )
    return Hlt2Line(
        name=name, algs=upfront_reconstruction() + [require_pvs(pvs), dd_lambdas, xic0s]
    )


# From Hlt2Conf/lines/charm/hyperons.py
@register_line_builder(hlt2_test_lines)
def oc_to_ximkpipi_lll_line(name="Hlt2Test_Oc0ToXimKmPipPip_LLL"):
    pvs = make_pvs()
    long_lambda_pions = ParticleFilter(
        make_long_pions(), F.FILTER(F.require_all(F.PT > 100 * MeV, _MIPCHI2_MIN(32.0)))
    )
    long_lambda_protons = ParticleFilter(
        make_has_rich_long_protons(),
        F.FILTER(
            F.require_all(
                F.PT > 400 * MeV, F.P > 9 * GeV, _MIPCHI2_MIN(6.0), F.PID_P > -5.0
            )
        ),
    )
    ll_lambdas = ParticleCombiner(
        [long_lambda_protons, long_lambda_pions],
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        name="Test_Hyperons_LambdaFromHyperon_LL",
        CombinationCut=F.require_all(
            F.MASS < 1190 * MeV,
            F.MAXSDOCACUT(100 * um),
            F.PT > 450 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 1165 * MeV,
            F.PT > 550 * MeV,
            F.CHI2DOF < 6.0,
            F.BPVVDZ(make_pvs()) > 8 * mm,
            F.BPVFDCHI2(make_pvs()) > 480.0,
        ),
    )
    long_xi_pions = ParticleFilter(
        make_long_pions(), F.FILTER(F.require_all(F.PT > 100 * MeV, _MIPCHI2_MIN(8.0)))
    )
    lll_xis = ParticleCombiner(
        [ll_lambdas, long_xi_pions],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        name="Test_Hyperons_Xim_LLL",
        CombinationCut=F.require_all(
            F.MASS < 1400 * MeV,
            F.MAXSDOCACUT(150 * um),
            F.PT > 500 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS < 1380 * MeV,
            F.PT > 600 * MeV,
            F.CHI2DOF < 9.0,
            _DZ_CHILD(1) > 8 * mm,
            F.BPVVDZ(pvs) > 4 * mm,
            F.BPVFDCHI2(pvs) > 16.0,
        ),
    )
    long_oc_kaons = ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.PT > 400 * MeV, F.P > 5 * GeV, _MIPCHI2_MIN(3.0), F.PID_K > 0.0
            ),
        ),
    )
    long_oc_pions = ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 250 * MeV, F.P > 2 * GeV, _MIPCHI2_MIN(3.0), F.PID_K < 5.0
            ),
        ),
    )
    oc0s = ParticleCombiner(
        [lll_xis, long_oc_kaons, long_oc_pions, long_oc_pions],
        DecayDescriptor="[Omega_c0 -> Xi- K- pi+ pi+]cc",
        name="Test_Hyperons_Oc0ToXimKmPipPip_LLL",
        Combination12Cut=F.require_all(
            F.MASS < 2535 * MeV,
            F.MAXSDOCACUT(150 * um),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2675 * MeV,
            F.SDOCA(1, 3) < 200 * um,
            F.SDOCA(2, 3) < 200 * um,
        ),
        CombinationCut=F.require_all(
            F.SDOCA(1, 4) < 200 * um,
            F.SDOCA(2, 4) < 200 * um,
            F.SDOCA(3, 4) < 250 * um,
            F.math.in_range(2575 * MeV, F.MASS, 2815 * MeV),
            F.PT > 1.3 * GeV,
            F.P > 28 * GeV,
            F.SUM(F.PT) > 2 * GeV,
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2595 * MeV, F.MASS, 275 * MeV),
            F.PT > 1.4 * GeV,
            F.P > 30 * GeV,
            F.CHI2DOF < 8.0,
            _DZ_CHILD(1) > 4 * mm,
            F.BPVVDZ(pvs) > 0 * mm,
            F.BPVFDCHI2(pvs) > 4.0,
            F.BPVIPCHI2(pvs) < 9.0,
            F.BPVDIRA(pvs) > 0.997,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), ll_lambdas, lll_xis, oc0s],
    )


# From former LoKi example line
def _make_phi():
    kaons = make_long_kaons()
    filteredKaons = ParticleFilter(
        name="FilteredPIDkaons", Input=kaons, Cut=F.FILTER(F.PID_K > -5)
    )
    combination_code = F.require_all(
        F.math.in_range(980.0 * MeV, F.MASS, 1060.0 * MeV),
        F.MAXSDOCACHI2CUT(30.0),
        F.SUM(F.CHI2DOF < 5.0) > 0,
    )
    vertex_code = F.require_all(
        F.CHI2 < 10, F.PT > 1 * GeV, F.CHI2DOF < 15, F.BPVFDCHI2(make_pvs()) > 0
    )
    return ParticleCombiner(
        name="Test_Phi2KKMaker",
        Inputs=[kaons, filteredKaons],
        DecayDescriptor="phi(1020) -> K+ K-",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def _make_bs2jpsiphi(jpsi, phi, pvs):
    combination_code = F.math.in_range(5100 * MeV, F.MASS, 5600 * MeV)
    vertex_code = F.require_all(
        F.math.in_range(5100 * MeV, F.MASS, 5600 * MeV),
        F.CHI2DOF < 20,
        F.BPVLTIME(pvs) > 0.01 * ps,
        F.PT < 32 * GeV,
        F.OWNPVIPCHI2 < 50,
    )
    return ParticleCombiner(
        name="Test_Bs2JPsiPhiCombiner_{hash}",
        Inputs=[jpsi, phi],
        DecayDescriptor="B_s0 -> J/psi(1S) phi(1020)",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def _make_BdsToPhiPhi(phi, pvs):
    combination_code = F.require_all(
        in_range(5000, F.MASS, 6000),
        (F.CHILD(1, F.PT) * F.CHILD(2, F.PT)) > 1.5 * GeV * GeV,
    )
    vertex_code = F.require_all(
        F.CHI2DOF < 15, in_range(5000, F.MASS, 6000), F.BPVLTIME(pvs) > 0.2 * picosecond
    )
    return ParticleCombiner(
        name="Test_BdsToPhiPhiCombiner",
        Inputs=[phi, phi],
        DecayDescriptor="B_s0 -> phi(1020) phi(1020)",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@register_line_builder(hlt2_test_lines)
def bs2jpsiphi_jpsi2mumu_phi2kk_line(
    name="Hlt2Test_Bs0ToJpsiPhi_JPsiToMupMum", prescale=1
):
    pvs = make_pvs()
    # mstahl: (almost) all production lines should use make_ismuon_long_muon instead of make_long_muons
    muons = make_long_muons()
    filteredMuons = ParticleFilter(
        name="FilteredPIDMuons", Input=muons, Cut=F.FILTER(F.PID_MU > 0.0)
    )
    mother_code = F.CHI2 < 16.0
    combination_code = F.require_all(
        F.math.in_range(2900 * MeV, F.MASS, 3300 * MeV),
        F.MAXSDOCACHI2
        < 20,  # mstahl: mixture of MAXSDOCACHI2 and MAXSDOCACHI2CUT. the latter should be preferred
        F.SUM(F.PT > 0.5 * GeV) > 0,
    )
    jpsi = ParticleCombiner(
        name="Test_MassConstrJpsi2MuMuMaker",
        Inputs=[muons, filteredMuons],
        DecayDescriptor="J/psi(1S) -> mu+ mu-",  # remove 'cc' and no use for cc in case of self-conjugate decays.
        CombinationCut=combination_code,
        CompositeCut=mother_code,
    )

    phi = _make_phi()
    phi_mon = monitor(
        name="Monitor_Phi_for_Test_Bs0ToJpsiPhi_m",
        data=phi,
        histograms=[
            histogram_1d(
                functor=F.MASS,
                name="/Phi_for_Test_Bs0ToJpsiPhi/m",
                title="m",
                label="MASS",
                bins=100,
                range=(980 * MeV, 1060 * MeV),
            )
        ],
    )

    bs = _make_bs2jpsiphi(jpsi, phi, pvs)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), phi_mon, jpsi, bs],
        prescale=prescale,
        monitoring_variables=(),
    )


@register_line_builder(hlt2_test_lines)
def bs2jpsiphi_jpsi2mumu_phi2kk_pvunbiasing_with_refit_line(
    name="Hlt2Test_Bs0ToJpsiPhi_JPsiToMupMum_PVUnbiasingWithRefit", prescale=1
):
    pvs = make_pvs()
    # mstahl: (almost) all production lines should use make_ismuon_long_muon instead of make_long_muons
    muons = make_long_muons()
    filteredMuons = ParticleFilter(
        name="FilteredPIDMuons", Input=muons, Cut=F.FILTER(F.PID_MU > 0.0)
    )
    mother_code = F.CHI2 < 16.0
    combination_code = F.require_all(
        F.math.in_range(2900 * MeV, F.MASS, 3300 * MeV),
        F.MAXSDOCACHI2
        < 20,  # mstahl: mixture of MAXSDOCACHI2 and MAXSDOCACHI2CUT. the latter should be preferred
        F.SUM(F.PT > 0.5 * GeV) > 0,
    )
    jpsi = ParticleCombiner(
        name="Test_MassConstrJpsi2MuMuMaker",
        Inputs=[muons, filteredMuons],
        DecayDescriptor="J/psi(1S) -> mu+ mu-",
        CombinationCut=combination_code,
        CompositeCut=mother_code,
    )

    phi = _make_phi()
    bs = _make_bs2jpsiphi(jpsi, phi, pvs)

    # create a new B list with unbiased PVs, with default mode=1
    # "The unbias mode (0:unbias,1:refit,2:unimplemented)"};
    bs_unbiasedpv = ParticleUnbiasedPVAdder(
        InputParticles=bs, PrimaryVertices=make_extended_pvs()
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), jpsi, bs, bs_unbiasedpv],
        prescale=prescale,
        monitoring_variables=(),
    )


@register_line_builder(hlt2_test_lines)
def bs2jpsiphi_jpsi2mumu_phi2kk_pvunbiasing_line(
    name="Hlt2Test_Bs0ToJpsiPhi_JPsiToMupMum_PVUnbiasing", prescale=1
):
    pvs = make_pvs()
    # mstahl: (almost) all production lines should use make_ismuon_long_muon instead of make_long_muons
    muons = make_long_muons()
    filteredMuons = ParticleFilter(
        name="FilteredPIDMuons", Input=muons, Cut=F.FILTER(F.PID_MU > 0.0)
    )
    mother_code = F.CHI2 < 16.0
    combination_code = F.require_all(
        F.math.in_range(2900 * MeV, F.MASS, 3300 * MeV),
        F.MAXSDOCACHI2
        < 20,  # mstahl: mixture of MAXSDOCACHI2 and MAXSDOCACHI2CUT. the latter should be preferred
        F.SUM(F.PT > 0.5 * GeV) > 0,
    )
    jpsi = ParticleCombiner(
        name="Test_MassConstrJpsi2MuMuMaker",
        Inputs=[muons, filteredMuons],
        DecayDescriptor="J/psi(1S) -> mu+ mu-",
        CombinationCut=combination_code,
        CompositeCut=mother_code,
    )

    phi = _make_phi()
    bs = _make_bs2jpsiphi(jpsi, phi, pvs)

    # create a new B list with unbiased PVs, without refitting mode=0
    # "The unbias mode (0:unbias,1:refit,2:unimplemented)"};
    bs_unbiasedpv = ParticleUnbiasedPVAdder(
        InputParticles=bs, PrimaryVertices=make_extended_pvs(), Mode=0
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), jpsi, bs, bs_unbiasedpv],
        prescale=prescale,
        monitoring_variables=(),
    )


@register_line_builder(hlt2_test_lines)
def bs2jpsiphi_jpsi2ee_phi2kk_line(
    name="Hlt2Test_BsToJpsiPhi_JPsi2ee_PhiToKK", prescale=1
):
    pvs = make_pvs()
    electrons = make_long_electrons_with_brem()
    filteredElectrons = ParticleFilter(
        name="FilteredPIDElectrons", Input=electrons, Cut=F.FILTER(F.PID_E > 0.0)
    )
    mother_code = F.CHI2 < 16.0
    combination_code = F.require_all(
        F.math.in_range(2900 * MeV, F.MASS, 3300 * MeV),
        F.MAXSDOCACHI2 < 20,
        F.SUM(F.PT > 0.5 * GeV) > 0,
    )
    jpsi = ParticleCombiner(
        name=name,
        Inputs=[electrons, filteredElectrons],
        DecayDescriptor="[J/psi(1S) -> e+ e-]cc",
        CombinationCut=combination_code,
        CompositeCut=mother_code,
    )

    phi = _make_phi()
    bs = _make_bs2jpsiphi(jpsi, phi, pvs)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), jpsi, bs],
        prescale=prescale,
    )


def make_jpsi(
    DecayDescriptor="J/psi(1S) -> mu+ mu-",
    name="Hlt2Test_makeJpsi",
    comb_m_min=2900.0 * MeV,
    comb_m_max=3300.0 * MeV,
    vtx_m_min=2950.0 * MeV,
    vtx_m_max=3250.0 * MeV,
    minPt_muon=300 * MeV,
    minP_muon=0 * MeV,
    minPIDmu=-5,
    minPt_Jpsi=0 * MeV,
    maxVertexChi2=25,
    pt=500.0 * MeV,
    pt_sum=0.0 * MeV,
    max_docachi2=30.0,
    max_vchi2pdof=10,
):
    muon_code = F.require_all(
        F.ISMUON, F.PT > minPt_muon, F.P > minP_muon, F.PID_MU > minPIDmu
    )

    muons = ParticleFilter(
        make_ismuon_long_muon(), name=f"{name}_muons", Cut=F.FILTER(muon_code)
    )

    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.MAXSDOCACHI2CUT(max_docachi2),
        F.SUM(F.PT) > pt_sum,
    )

    vertex_code = F.require_all(
        F.PT > pt, F.CHI2 < max_vchi2pdof, in_range(vtx_m_min, F.MASS, vtx_m_max)
    )

    jpsi = ParticleCombiner(
        name="Test_Jpsi2MuMuMaker",
        Inputs=[muons, muons],
        DecayDescriptor="J/psi(1S) -> mu+ mu-",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
    return jpsi


def make_detached_kaons(
    make_particles=make_has_rich_long_kaons,
    name="detached_kaons_{hash}",
    pt_min=200.0 * MeV,
    p_min=2.5 * GeV,
    p_max=150.0 * GeV,
    eta_min=2.0,
    eta_max=5.0,
    mipchi2dvprimary_min=0,
    pid=(F.PID_K > 0.0),
):
    pvs = make_pvs()

    code = F.require_all(
        F.PT > pt_min,
        in_range(p_min, F.P, p_max),
        in_range(eta_min, F.ETA, eta_max),
        F.MINIPCHI2(pvs) > mipchi2dvprimary_min,
    )
    code &= pid

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


def make_b_hadron(
    particles,
    descriptor,
    name="b_hadron_{hash}",
    am_min=5100 * MeV,
    am_max=5550 * MeV,
    m_min=5140 * MeV,
    m_max=5510 * MeV,
    achi2_doca_max=25,
    vtx_chi2pdof_max=20,
    bpvltime_min=0.2 * picosecond,
    bpvdira_min=0.995,
    bpvfdchi2_min=0,
    minVDz=0.0 * mm,
    minRho=0.0 * mm,
):
    combination_code = F.require_all(in_range(am_min, F.MASS, am_max))
    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < vtx_chi2pdof_max,
        F.BPVLTIME() > bpvltime_min,
        F.BPVVDRHO() > minRho,
        F.BPVVDZ() > minVDz,
        F.BPVFDCHI2() > bpvfdchi2_min,
        F.BPVDIRA() > bpvdira_min,
    )

    return ParticleCombiner(
        name=name,
        Inputs=particles,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@register_line_builder(hlt2_test_lines)
def default_b2jpsik_line_with_flavourtagging(
    name="Hlt2Test_B2JpsiK_withFlavourTagging", prescale=1.0
):
    jpsi = make_jpsi()
    kaons = make_detached_kaons()
    b2jpsik = make_b_hadron(
        particles=[jpsi, kaons], descriptor="[B+ -> J/psi(1S) K+]cc"
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + prefilters() + [b2jpsik],
        prescale=1,
        tagging_particles=True,
        extra_outputs=[("FlavourTags", run2_all_taggers(b2jpsik).OutputFlavourTags)],
    )


@register_line_builder(hlt2_test_lines)
def modified_b2jpsik_line_with_flavourtagging(
    name="Hlt2Test_B2JpsiKModified_withFlavourTagging", prescale=1.0
):
    jpsi = make_jpsi()
    kaons = make_detached_kaons()
    b2jpsik = make_b_hadron(
        m_min=5350 * MeV, particles=[jpsi, kaons], descriptor="[B+ -> J/psi(1S) K+]cc"
    )
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + prefilters() + [b2jpsik],
        prescale=1,
        tagging_particles=True,
        extra_outputs=[("FlavourTags", run2_all_taggers(b2jpsik).OutputFlavourTags)],
    )


def make_sameside_tagging_kaons(
    make_particles=make_has_rich_long_kaons,
    pvs=make_pvs,
    p_min=2 * GeV,
    p_max=200 * GeV,
    pt_max=10 * GeV,
    eta_max=5.12,
    ghostprob_max=0.5,
):
    selection_code = F.require_all(
        F.P > p_min,
        F.P < p_max,
        F.PT < pt_max,
        F.GHOSTPROB < ghostprob_max,
        F.ETA < eta_max,
    )

    long_kaons = ParticleFilter(make_particles(), Cut=F.FILTER(selection_code))

    return AdvancedCloneKiller(InputParticles=[long_kaons])


@register_line_builder(hlt2_test_lines)
def default_bs2jpsiphi_line_with_flavourtagging(
    name="Hlt2Test_Bs2JpsiPhi_withFlavourTagging", prescale=1.0
):
    jpsi = make_jpsi()
    phi = make_phi2kk()
    bs2jpsiphi = make_b_hadron(
        particles=[jpsi, phi], descriptor="B_s0 -> J/psi(1S) phi(1020)"
    )

    pvs = make_pvs()

    ssKaons = make_sameside_tagging_kaons()
    ssKaonTagging = Run2SSKaonTagger(
        BCandidates=bs2jpsiphi, TaggingKaons=ssKaons, PrimaryVertices=pvs
    ).OutputFlavourTags

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + prefilters() + [bs2jpsiphi],
        prescale=prescale,
        tagging_particles=True,
        extra_outputs=[("SSKaonFlavourTags", ssKaonTagging)],
    )


@register_line_builder(hlt2_test_lines)
def modified_bs2jpsiphi_line_with_flavourtagging(
    name="Hlt2Test_B2JpsiPhiModified_withFlavourTagging", prescale=1
):
    jpsi = make_jpsi()
    phi = make_phi2kk()
    bs2jpsiphi = make_b_hadron(
        m_min=5350 * MeV,
        particles=[jpsi, phi],
        descriptor="B_s0 -> J/psi(1S) phi(1020)",
    )

    pvs = make_pvs()

    ssKaons = make_sameside_tagging_kaons()
    ssKaonTagging = Run2SSKaonTagger(
        BCandidates=bs2jpsiphi, TaggingKaons=ssKaons, PrimaryVertices=pvs
    ).OutputFlavourTags

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + prefilters() + [bs2jpsiphi],
        prescale=prescale,
        tagging_particles=True,
        extra_outputs=[("SSKaonFlavourTags", ssKaonTagging)],
    )


# From Hlt2Conf/lines/charm/d0_to_hh.py


def _make_dstars(dzeros, decay_descriptor, name):
    pions = ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 150 * MeV,
                F.P > 1 * GeV,
                F.PID_K < 5.0,
            ),
        ),
    )
    return ParticleCombiner(
        [dzeros, pions],
        DecayDescriptor=decay_descriptor,
        name=name,
        CombinationCut=F.MASS - F.CHILD(1, F.MASS) < 165 * MeV,
        CompositeCut=F.require_all(
            F.MASS - F.CHILD(1, F.MASS) < 160 * MeV, F.CHI2DOF < (16.0)
        ),
    )


def _make_sl_bs(dzeros, muons):
    pvs = make_pvs()
    return ParticleCombiner(
        [dzeros, muons],
        DecayDescriptor="[B- -> D0 mu-]cc",
        name="Test_Taggers_BBuilderFor_D0_{hash}",
        CombinationCut=F.require_all(
            in_range(2.3 * GeV, F.MASS, 10 * GeV), F.MAXSDOCACHI2CUT(10.0)
        ),
        CompositeCut=F.require_all(
            F.CHI2DOF < 9.0,
            in_range(2.3 * GeV, F.MASS, 10 * GeV),
            in_range(2.8 * GeV, F.BPVCORRM(pvs), 8.5 * GeV),
            F.BPVDIRA(pvs) > 0.999,
            F.CHILD(1, F.END_VZ) - F.END_VZ > -3 * mm,
        ),
    )


def _make_dzeros(pvs):
    kaons = ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                _MIPCHI2_MIN(4.0),
                F.PT > 800 * MeV,
                F.P > 5 * GeV,
                F.PID_K > 5.0,
            ),
        ),
    )

    return ParticleCombiner(
        [kaons, kaons],
        DecayDescriptor="[D0 -> K+ K-]cc",
        name="Test_D0ToKpKm_Builder_{hash}",
        CombinationCut=F.require_all(
            in_range(1685 * MeV, F.MASS, 2045 * MeV),
            F.PT > 2 * GeV,
            F.MAX(F.PT) > 1200 * MeV,
            F.MAXSDOCACUT(0.1 * mm),
        ),
        CompositeCut=F.require_all(
            in_range(1715 * MeV, F.MASS, 2015 * MeV),
            F.CHI2DOF < 10.0,
            F.BPVFDCHI2(pvs) > 25.0,
            F.BPVDIRA(pvs) > 0.99985,
        ),
    )


@register_line_builder(hlt2_test_lines)
def dzero2kpkm_line(name="Hlt2Test_D0ToKpKm", prescale=1):
    pvs = make_pvs()
    dzeros = _make_dzeros(pvs)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), dzeros],
        prescale=prescale,
    )


@register_line_builder(hlt2_test_lines)
def dstarp2dzeropip_dzero2kpkm_line(name="Hlt2Test_DstpToD0Pip_D0ToKpKm", prescale=1):
    pvs = make_pvs()
    dzeros = _make_dzeros(pvs)
    dstars = _make_dstars(
        dzeros,
        decay_descriptor="[D*(2010)+ -> D0 pi+]cc",
        name="Test_D0ToHH_D0ToKpKm_{hash}",
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction()
        + [require_gec()]
        + [require_pvs(pvs), dzeros, dstars],
        prescale=prescale,
    )


@register_line_builder(hlt2_test_lines)
def b2d0mu_dzero2kpkm_line(name="Hlt2Test_BToD0MumX_D0ToKpKm", prescale=1):
    pvs = make_pvs()
    dzeros = _make_dzeros(pvs)
    muons = ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.MINIPCHI2CUT(IPChi2Cut=9.0, Vertices=pvs),
                F.PT > 1 * GeV,
                F.P > 2 * GeV,
                F.PID_MU > 0.0,
            ),
        ),
    )
    bs = _make_sl_bs(dzeros, muons)
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction()
        + [require_gec()]
        + [require_pvs(pvs), dzeros, bs],
        prescale=prescale,
    )


# From Hlt2Conf/lines/bnoc/hlt2_bnoc.py
@register_line_builder(hlt2_test_lines)
def Bds_PhiPhi_line(name="Hlt2Test_BdsToPhiPhi", prescale=1):
    pvs = make_pvs()
    phi = _make_phi()
    BdsToPhiPhi = _make_BdsToPhiPhi(phi, pvs)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=upfront_reconstruction() + [require_pvs(pvs), phi, BdsToPhiPhi],
    )


# From Hlt2Conf/lines/bnoc/hlt2_bnoc.py
@register_line_builder(hlt2_test_lines)
def Bds_PhiPhi_line_with_SP_reco(name="Hlt2Test_BdsToPhiPhi_with_SP_reco", prescale=1):
    pvs = make_pvs()
    phi = _make_phi()
    BdsToPhiPhi = _make_BdsToPhiPhi(phi, pvs)

    # Selective persistreco as line output
    # first get the reco objects
    reco = reconstruction()
    # get the persistreco locations for these objects which is version dependent
    # listed in LHCb/PyConf/python/PyConf/persistency_locations.py
    pr_objs = persistreco_line_outputs(reco)

    return Hlt2Line(
        name=name,
        prescale=prescale,
        algs=upfront_reconstruction() + [require_pvs(pvs), phi, BdsToPhiPhi],
        # When selecting persistreco location as extra_output, don't give a location component
        # these will be saved in same location for all lines if ""
        # if a location is provided, a copy will be made at the selected location
        extra_outputs=[("", pr_objs["CaloElectrons"]), ("", pr_objs["LongProtos"])],
    )
