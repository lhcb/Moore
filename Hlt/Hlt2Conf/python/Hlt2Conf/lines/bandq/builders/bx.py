###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of B&Q xibc decays
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleContainersMerger

from Hlt2Conf.lines.bandq.builders import (
    b_for_spectroscopy,
    b_hadrons,
    c_to_hadrons,
    charged_hadrons,
    longlived_hadrons,
)
from Hlt2Conf.lines.charmonium_to_dimuon import (
    make_charmonium_muons as make_bandq_muons,
)

"""
For various combinations of B + h, B + h1 + h2 coming from PV
where B - all possible B mesons,
h - pi,K,p (+Ks, Lambda, Xi, Omega)
"""


@configurable
def make_bx_prompt(
    particles,
    descriptor,
    name="bandq_bx_prompt_{hash}",
    am_min=0 * MeV,
    am_max=6200 * MeV,
    m_min=0 * MeV,
    m_max=6000 * MeV,
    sum_pt_min=5 * GeV,
    achi2_doca_max=16,
    vtx_chi2pdof_max=10,
    ownpvltime_max=0.5 * picosecond,
    ownpvfdchi2_max=5,
    ownpvipchi2_max=5,
):
    comb_cut_add = F.require_all(F.SUM(F.PT) > sum_pt_min)

    vtx_cut_add = F.require_all(
        F.OWNPVLTIME < ownpvltime_max,
        F.OWNPVFDCHI2 < ownpvfdchi2_max,
        F.OWNPVIPCHI2 < ownpvipchi2_max,
    )

    return b_hadrons._make_generic(
        particles=particles,
        descriptor=descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        achi2_doca_max=achi2_doca_max,
        vtx_chi2pdof_max=vtx_chi2pdof_max,
        comb_cut_add=comb_cut_add,
        vtx_cut_add=vtx_cut_add,
    )


@configurable
def make_bx_detached(
    particles,
    descriptor,
    name="bandq_bx_detached_{hash}",
    am_min=0 * MeV,
    am_max=6200 * MeV,
    m_min=0 * MeV,
    m_max=6000 * MeV,
    sum_pt_min=5 * GeV,
    achi2_doca_max=16,
    vtx_chi2pdof_max=10,
    ownpvltime_min=0.2 * picosecond,
    ownpvfdchi2_min=16,
    ownpvipchi2_min=0,
):
    comb_cut_add = F.require_all(F.SUM(F.PT) > sum_pt_min)

    vtx_cut_add = F.require_all(
        F.OWNPVLTIME > ownpvltime_min, F.OWNPVFDCHI2 > ownpvfdchi2_min
    )

    if ownpvipchi2_min > 0:
        vtx_cut_add &= F.OWNPVIPCHI2 > ownpvipchi2_min

    return b_hadrons._make_generic(
        particles=particles,
        descriptor=descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        achi2_doca_max=achi2_doca_max,
        vtx_chi2pdof_max=vtx_chi2pdof_max,
        comb_cut_add=comb_cut_add,
        vtx_cut_add=vtx_cut_add,
    )


from Hlt2Conf.lines.bandq.builders.helper import descriptor_parser, get_mass, make_name


def particle_maker(p_id, prompt=True, SL=False):
    p_id = p_id.strip()
    process = "spruce"

    if SL:
        if p_id in ["B+", "B-", "B0", "B~0"]:
            return b_for_spectroscopy.make_Bud_SL(process=process)
        if p_id in ["B_s0", "B_s~0"]:
            return b_for_spectroscopy.make_Bs_SL(process=process)
        if p_id in ["B_c+", "B_c-"]:
            return b_for_spectroscopy.make_Bc_SL(process=process)
        if p_id in ["Lambda_b0", "Lambda_b~0"]:
            return b_for_spectroscopy.make_Lb_SL(process=process)
        if p_id in ["Xi_b0", "Xi_b~0", "Xi_b-", "Xi_b~+"]:
            return b_for_spectroscopy.make_Xib_SL(process=process)
        if p_id in ["Omega_b-", "Omega_b~+"]:
            return b_for_spectroscopy.make_Lb_SL(process=process)

    # else: i.e. if SL==False
    if p_id in ["B+", "B-"]:
        return b_for_spectroscopy.make_Bu(process=process)
    elif p_id in ["B0", "B~0"]:
        return b_for_spectroscopy.make_Bd(process=process)
    elif p_id in ["B_s0", "B_s~0"]:
        return b_for_spectroscopy.make_Bs(process=process)
    elif p_id in ["B_c+", "B_c-"]:
        return b_for_spectroscopy.make_Bc(process=process)
    elif p_id in ["Lambda_b0", "Lambda_b~0"]:
        return b_for_spectroscopy.make_Lb(process=process)
    elif p_id in ["Xi_b0", "Xi_b~0"]:
        return b_for_spectroscopy.make_Xib0(process=process)
    elif p_id in ["Xi_b-", "Xi_b~+"]:
        return b_for_spectroscopy.make_Xibm(process=process)
    elif p_id in ["Omega_b-", "Omega_b~+"]:
        return b_for_spectroscopy.make_Omegab(process=process)
    elif p_id in ["Beauty"]:
        return b_for_spectroscopy.make_AllB(process=process)
    ##

    if p_id in ["D0", "D~0"]:
        return c_to_hadrons.make_tight_Dz()
    if p_id in ["D+", "D-"]:
        return c_to_hadrons.make_tight_DpToKmPipPip()
    if p_id in ["D_s+", "D_s-"]:
        return c_to_hadrons.make_tight_DspToKmKpPip()
    if p_id in ["Lambda_c+", "Lambda_c~-"]:
        return c_to_hadrons.make_tight_LcToPpKmPip()

    ##
    if p_id in ["pi+", "pi-"]:
        if prompt:
            return charged_hadrons.make_prompt_pions()
        else:
            return charged_hadrons.make_detached_pions()
    elif p_id in ["K+", "K-"]:
        if prompt:
            return charged_hadrons.make_prompt_kaons()
        else:
            return charged_hadrons.make_detached_kaons()
    elif p_id in ["p+", "p~-"]:
        if prompt:
            return charged_hadrons.make_prompt_protons()
        else:
            return charged_hadrons.make_detached_protons()
    elif p_id in ["KS0", "KS~0"]:
        return longlived_hadrons.make_Ks_merged()
    elif p_id in ["Lambda0", "Lambda~0"]:
        return longlived_hadrons.make_Lambda_merged()
    elif p_id in ["Xi-", "Xi~+"]:
        return longlived_hadrons.make_XiToLambdaPi()
    elif p_id in ["Omega-", "Omega~+"]:
        return longlived_hadrons.make_OmegaToLambdaK()
    elif p_id in ["mu+", "mu-"]:
        return make_bandq_muons()
    else:
        return None


@configurable
def make_BX(name, descriptor, prompt=True, SL=False):
    mother_id, daughter_ids = descriptor_parser(descriptor)

    mass_min = sum([get_mass(particle) for particle in daughter_ids])
    mass_max = mass_min + 0.75 * GeV
    if ("mu+" in daughter_ids) or ("mu-" in daughter_ids):
        mass_max += 0.75 * GeV  # to allow for larger energy release in c-quark decay
    am_min, am_max, m_min, m_max = 0.0, mass_max + 200 * MeV, 0.0, mass_max

    particles = []
    for daughter_id in daughter_ids:
        particle = particle_maker(daughter_id, prompt, SL)
        if particle is not None:
            particles += [particle]

    name = "make_bandq_"
    name += make_name(daughter_ids)

    if prompt:
        name += "_prompt"
    else:
        name += "_detached"
    if SL:
        name += "_SL"
    name += "_{hash}"

    if prompt:
        make_bx_ = make_bx_prompt
    else:
        make_bx_ = make_bx_detached

    line_alg = make_bx_(
        name=name,
        particles=particles,
        descriptor=descriptor,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
    )
    return line_alg


@configurable
def make_BX_combination(name, descriptors, prompt=True, SL=False):
    assert len(descriptors) > 0
    algs = []
    for descriptor in descriptors:
        algs.append(make_BX(name, descriptor, prompt=prompt, SL=SL))
    return ParticleContainersMerger(algs, name)


@configurable
def make_BX_SL_combination(name, descriptors, prompt=True):
    return make_BX_combination(name, descriptors, prompt=prompt, SL=True)


@configurable
def make_BX_detached_combination(name, descriptors, SL=False):
    return make_BX_combination(name, descriptors, prompt=False, SL=SL)


@configurable
def make_BX_detached_SL_combination(name, descriptors):
    return make_BX_combination(name, descriptors, prompt=False, SL=True)
