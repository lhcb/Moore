###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make B&Q B -> ccbar (->hadron) X combinations.
"""

import Functors as F
from GaudiKernel.SystemOfUnits import MeV, mm
from PyConf import configurable

from Hlt2Conf.lines.bandq.builders import b_hadrons, charged_hadrons, qqbar_to_hadrons

####################################################
# Customize tighter cuts for b2etacX lines          #
# Rate for etac->hadron usually higher than dimuon  #
####################################################


@configurable
def make_lb_for_etacX(
    name,
    particles,
    descriptor,
    ownpvfdchi2_min=50.0,
    achi2_doca_max=25,
    vtx_chi2pdof_max=20,
    comb_cut_add=None,
    am_min=5350 * MeV,
    am_max=5850 * MeV,
    m_min=5390 * MeV,
    m_max=5810 * MeV,
):
    return b_hadrons.make_lb(
        name=name,
        particles=particles,
        descriptor=descriptor,
        minVDz=2.0 * mm,
        minRho=0.1 * mm,
        ownpvdira_min=0.9999,
        ownpvfdchi2_min=ownpvfdchi2_min,
        achi2_doca_max=achi2_doca_max,
        vtx_chi2pdof_max=vtx_chi2pdof_max,
        comb_cut_add=comb_cut_add,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
    )


@configurable
def make_bu_for_etacX(
    name,
    particles,
    descriptor,
    ownpvfdchi2_min=50.0,
    achi2_doca_max=25,
    vtx_chi2pdof_max=20,
    comb_cut_add=None,
):
    return b_hadrons.make_bu(
        name=name,
        particles=particles,
        descriptor=descriptor,
        minVDz=2.0 * mm,
        minRho=0.1 * mm,
        ownpvdira_min=0.9999,
        ownpvfdchi2_min=ownpvfdchi2_min,
        achi2_doca_max=achi2_doca_max,
        vtx_chi2pdof_max=vtx_chi2pdof_max,
        comb_cut_add=comb_cut_add,
    )


######################
# Line registers start#
######################

ghostProb_max_forEtacline = 0.5
pt_min_forEtacline = 250 * MeV


@configurable
def make_LbToEtacPpKm_EtacToHHHH(name="bandq_LbToEtacPpKm_EtacToHHHH_{hash}"):
    kaons = charged_hadrons.make_detached_kaons_tightpid(
        pt_min=pt_min_forEtacline, ghostProb_max=ghostProb_max_forEtacline
    )
    protons = charged_hadrons.make_detached_protons_tightpid(
        pt_min=pt_min_forEtacline, ghostProb_max=ghostProb_max_forEtacline
    )
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToHHHH(
        dira_min=0.9,
        pt_min_HHHH=pt_min_forEtacline,
        ghostProb_max=ghostProb_max_forEtacline,
    )
    comb_cut_add = F.require_all(F.CHILD(2, F.PT) + F.CHILD(3, F.PT) > 900.0 * MeV)

    line_alg = make_lb_for_etacX(
        name=name,
        particles=[etac, protons, kaons],
        descriptor="[Lambda_b0 -> eta_c(1S) p+ K-]cc",
        vtx_chi2pdof_max=9,
        ownpvfdchi2_min=150,
        comb_cut_add=comb_cut_add,
        am_max=5950 * MeV,
        m_max=5920 * MeV,
    )

    return line_alg


@configurable
def make_LbToEtacPpKm_EtacToKsLLKpPim(name="bandq_LbToEtacPpKm_EtacToKsLLKpPim_{hash}"):
    kaons = charged_hadrons.make_detached_kaons_tightpid(pt_min=pt_min_forEtacline)
    protons = charged_hadrons.make_detached_protons_tightpid(pt_min=pt_min_forEtacline)
    etac = qqbar_to_hadrons.make_detached_ccbarToKsLLKpPim(
        dira_min=0.9, pt_min_daughters=pt_min_forEtacline
    )
    comb_cut_add = F.require_all(F.CHILD(2, F.PT) + F.CHILD(3, F.PT) > 900.0 * MeV)

    line_alg = make_lb_for_etacX(
        name=name,
        particles=[etac, protons, kaons],
        descriptor="[Lambda_b0 -> eta_c(1S) p+ K-]cc",
        vtx_chi2pdof_max=10,
        ownpvfdchi2_min=150,
        comb_cut_add=comb_cut_add,
        am_max=6000 * MeV,
        m_max=5950 * MeV,
    )

    return line_alg


@configurable
def make_BdToEtacKpPim_EtacToKsLLKpPim(
    name="bandq_BdToEtacKpPim_EtacToKsLLKpPim_{hash}",
):
    kaons = charged_hadrons.make_detached_kaons_tightpid(pt_min=pt_min_forEtacline)
    pions = charged_hadrons.make_detached_pions_tightpid(pt_min=pt_min_forEtacline)
    etac = qqbar_to_hadrons.make_detached_ccbarToKsLLKpPim(
        dira_min=0.9, pt_min_daughters=pt_min_forEtacline
    )
    comb_cut_add = F.require_all(F.CHILD(2, F.PT) + F.CHILD(3, F.PT) > 900.0 * MeV)

    line_alg = make_bu_for_etacX(
        name=name,
        particles=[etac, kaons, pions],
        descriptor="[B0 -> eta_c(1S) K+ pi-]cc",
        vtx_chi2pdof_max=10,
        ownpvfdchi2_min=150,
        comb_cut_add=comb_cut_add,
    )

    return line_alg


@configurable
def make_LbToEtacPpKm_EtacToKsDDKpPim(name="bandq_LbToEtacPpKm_EtacToKsDDKpPim_{hash}"):
    kaons = charged_hadrons.make_detached_kaons_tightpid(pt_min=pt_min_forEtacline)
    protons = charged_hadrons.make_detached_protons_tightpid(pt_min=pt_min_forEtacline)
    etac = qqbar_to_hadrons.make_detached_ccbarToKsDDKpPim(
        dira_min=0.9, pt_min_daughters=pt_min_forEtacline
    )
    comb_cut_add = F.require_all(F.CHILD(2, F.PT) + F.CHILD(3, F.PT) > 900.0 * MeV)

    line_alg = make_lb_for_etacX(
        name=name,
        particles=[etac, protons, kaons],
        descriptor="[Lambda_b0 -> eta_c(1S) p+ K-]cc",
        vtx_chi2pdof_max=10,
        ownpvfdchi2_min=150,
        comb_cut_add=comb_cut_add,
        am_max=6000 * MeV,
        m_max=5950 * MeV,
    )

    return line_alg


@configurable
def make_BdToEtacKpPim_EtacToKsDDKpPim(
    name="bandq_BdToEtacKpPim_EtacToKsDDKpPim_{hash}",
):
    kaons = charged_hadrons.make_detached_kaons_tightpid(pt_min=pt_min_forEtacline)
    pions = charged_hadrons.make_detached_pions_tightpid(pt_min=pt_min_forEtacline)
    etac = qqbar_to_hadrons.make_detached_ccbarToKsDDKpPim(
        dira_min=0.9, pt_min_daughters=pt_min_forEtacline
    )
    comb_cut_add = F.require_all(F.CHILD(2, F.PT) + F.CHILD(3, F.PT) > 900.0 * MeV)

    line_alg = make_bu_for_etacX(
        name=name,
        particles=[etac, kaons, pions],
        descriptor="[B0 -> eta_c(1S) K+ pi-]cc",
        vtx_chi2pdof_max=10,
        ownpvfdchi2_min=150,
        comb_cut_add=comb_cut_add,
    )

    return line_alg


@configurable
def make_BdToEtacKpPim_EtacToHHHH(name="bandq_BdToEtacKpPim_EtacToHHHH_{hash}"):
    kaons = charged_hadrons.make_detached_kaons_tightpid(
        pt_min=pt_min_forEtacline, ghostProb_max=ghostProb_max_forEtacline
    )
    pions = charged_hadrons.make_detached_pions_tightpid(
        pt_min=pt_min_forEtacline, ghostProb_max=ghostProb_max_forEtacline
    )
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToHHHH(
        dira_min=0.9,
        pt_min_HHHH=pt_min_forEtacline,
        ghostProb_max=ghostProb_max_forEtacline,
    )
    comb_cut_add = F.require_all(F.CHILD(2, F.PT) + F.CHILD(3, F.PT) > 900.0 * MeV)

    line_alg = make_bu_for_etacX(
        name=name,
        particles=[etac, kaons, pions],
        descriptor="[B0 -> eta_c(1S) K+ pi-]cc",
        vtx_chi2pdof_max=9,
        ownpvfdchi2_min=150,
        comb_cut_add=comb_cut_add,
    )
    return line_alg


@configurable
def make_BuToEtacKpPhi_EtacToHHHH(name="bandq_BuToEtacKpPhi_EtacToHHHH_{hash}"):
    kaons = charged_hadrons.make_detached_kaons_tightpid()
    phi = charged_hadrons.make_detached_phi()
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToHHHH(
        dira_min=0.9, pt_min_HHHH=pt_min_forEtacline
    )
    line_alg = make_bu_for_etacX(
        name=name,
        particles=[etac, phi, kaons],
        descriptor="[B0 -> eta_c(1S) phi(1020) K+]cc",
    )
    return line_alg


@configurable
def make_LbToEtacPpKm_EtacToPpPm(name="bandq_LbToEtacPpKm_EtacToPpPm_{hash}"):
    kaons = charged_hadrons.make_detached_kaons_tightpid(pt_min=pt_min_forEtacline)
    protons = charged_hadrons.make_detached_protons_tightpid(pt_min=pt_min_forEtacline)
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToPpPm(
        dira_min=0.9, pt_min_proton=pt_min_forEtacline
    )
    line_alg = make_lb_for_etacX(
        name=name,
        particles=[etac, protons, kaons],
        descriptor="[Lambda_b0 -> eta_c(1S) p+ K-]cc",
        am_max=6000 * MeV,
        m_max=5950 * MeV,
    )
    return line_alg


@configurable
def make_BdToEtacKpPim_EtacToPpPm(name="bandq_BdToEtacKpPim_EtacToPpPm_{hash}"):
    kaons = charged_hadrons.make_detached_kaons_tightpid(pt_min=pt_min_forEtacline)
    pions = charged_hadrons.make_detached_pions_tightpid(pt_min=pt_min_forEtacline)
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToPpPm(
        dira_min=0.9, pt_min_proton=pt_min_forEtacline
    )
    line_alg = make_bu_for_etacX(
        name=name,
        particles=[etac, kaons, pions],
        descriptor="[B0 -> eta_c(1S) K+ pi-]cc",
    )
    return line_alg


@configurable
def make_BuToEtacKpPhi_EtacToPpPm(name="bandq_BuToEtacKpPhi_EtacToPpPm_{hash}"):
    kaons = charged_hadrons.make_detached_kaons_tightpid()
    phi = charged_hadrons.make_detached_phi()
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToPpPm()
    line_alg = make_bu_for_etacX(
        name=name,
        particles=[etac, phi, kaons],
        descriptor="[B0 -> eta_c(1S) phi(1020) K+]cc",
    )
    return line_alg


@configurable
def make_LbToEtacPpKm_EtacToKpKm(name="bandq_LbToEtacPpKm_EtacToKpKm_{hash}"):
    kaons = charged_hadrons.make_detached_kaons_tightpid()
    protons = charged_hadrons.make_detached_protons_tightpid()
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToKpKm()
    line_alg = make_lb_for_etacX(
        name=name,
        particles=[etac, protons, kaons],
        descriptor="[Lambda_b0 -> eta_c(1S) p+ K-]cc",
    )
    return line_alg


@configurable
def make_BdToEtacKpPim_EtacToKpKm(name="bandq_BdToEtacKpPim_EtacToKpKm_{hash}"):
    kaons = charged_hadrons.make_detached_kaons_tightpid()
    pions = charged_hadrons.make_detached_pions_tightpid()
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToKpKm()
    line_alg = make_bu_for_etacX(
        name=name,
        particles=[etac, kaons, pions],
        descriptor="[B0 -> eta_c(1S) K+ pi-]cc",
    )
    return line_alg


@configurable
def make_BuToEtacKpPhi_EtacToKpKm(name="bandq_BuToEtacKpPhi_EtacToKpKm_{hash}"):
    kaons = charged_hadrons.make_detached_kaons_tightpid()
    phi = charged_hadrons.make_detached_phi()
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToKpKm()
    line_alg = make_bu_for_etacX(
        name=name,
        particles=[etac, phi, kaons],
        descriptor="[B0 -> eta_c(1S) phi(1020) K+]cc",
    )
    return line_alg


@configurable
def make_LbToEtacPpKm_EtacToPipPim(name="bandq_LbToEtacPpKm_EtacToPipPim_{hash}"):
    kaons = charged_hadrons.make_detached_kaons_tightpid()
    protons = charged_hadrons.make_detached_protons_tightpid()
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToPipPim()
    line_alg = make_lb_for_etacX(
        name=name,
        particles=[etac, protons, kaons],
        descriptor="[Lambda_b0 -> eta_c(1S) p+ K-]cc",
    )
    return line_alg


@configurable
def make_BdToEtacKpPim_EtacToPipPim(name="bandq_BdToEtacKpPim_EtacToPipPim_{hash}"):
    kaons = charged_hadrons.make_detached_kaons_tightpid()
    pions = charged_hadrons.make_detached_pions_tightpid()
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToPipPim()
    line_alg = make_bu_for_etacX(
        name=name,
        particles=[etac, kaons, pions],
        descriptor="[B0 -> eta_c(1S) K+ pi-]cc",
    )
    return line_alg


@configurable
def make_BuToEtacKpPhi_EtacToPipPim(name="bandq_BuToEtacKpPhi_EtacToPipPim_{hash}"):
    kaons = charged_hadrons.make_detached_kaons_tightpid()
    phi = charged_hadrons.make_detached_phi()
    etac = qqbar_to_hadrons.make_detached_Etac1S2SToPipPim()
    line_alg = make_bu_for_etacX(
        name=name,
        particles=[etac, phi, kaons],
        descriptor="[B0 -> eta_c(1S) phi(1020) K+]cc",
    )
    return line_alg
