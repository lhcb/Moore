###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make B&Q double charm combinations.
"""

import Functors as F
from Functors import require_all
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, mm
from PyConf import ConfigurationError, configurable
from RecoConf.algorithms_thor import ParticleCombiner, ParticleContainersMerger
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.bandq.builders import c_to_hadrons
from Hlt2Conf.lines.charm import (
    cbaryon_spectroscopy,
    d0_to_hh,
    d0_to_hhhh,
    d0_to_kshh,
    d_to_hhh,
)
from Hlt2Conf.lines.charmonium_to_dimuon import make_jpsi, make_psi2s


@configurable
def _make_doublecharm(
    particles,
    process,
    descriptor,
    name="bandq_doublecharm_singledecay_template_{hash}",
    allowDiffInputsForSameIDChildren=False,
    max_mass=None,
    max_ipchi2=None,
    max_chi2ndof=None,
):
    if process not in ["hlt2", "spruce"]:
        raise ConfigurationError(
            "process for _make_doublecharm must be hlt2 or spruce. Please check !"
        )
    if process == "spruce":
        combination_code = require_all(
            in_range(-10 * mm, (F.CHILD(1, F.OWNPVZ) - (F.CHILD(2, F.OWNPVZ))), 10 * mm)
        )
    elif process == "hlt2":
        combination_code = require_all(
            F.ALL
        )  # PN: note for 2025; should the sprucing combination_code cut also go here to save HLT2 bandwidth?
    vertex_code = require_all(F.ALL)
    if max_mass is not None:
        vertex_code = require_all(vertex_code, F.MASS < max_mass)
    if max_ipchi2 is not None:
        vertex_code = require_all(vertex_code, F.OWNPVIPCHI2 < max_ipchi2)
    if max_chi2ndof is not None:
        vertex_code = require_all(vertex_code, F.CHI2DOF < max_chi2ndof)

    return ParticleCombiner(
        name=name,
        Inputs=particles,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        AllowDiffInputsForSameIDChildren=allowDiffInputsForSameIDChildren,
    )


@configurable
def make_doublecharm(
    particles,
    process,
    descriptors,
    name="bandq_doublecharm_template_{hash}",
    allowDiffInputsForSameIDChildren_dc=False,
    max_mass=None,
    max_ipchi2=None,
    max_chi2ndof=None,
):
    assert len(descriptors) > 0
    c_hadrons = []
    for descriptor in descriptors:
        c_hadrons.append(
            _make_doublecharm(
                particles=particles,
                process=process,
                descriptor=descriptor,
                allowDiffInputsForSameIDChildren=allowDiffInputsForSameIDChildren_dc,
                max_mass=max_mass,
                max_ipchi2=max_ipchi2,
                max_chi2ndof=max_chi2ndof,
            )
        )
    return ParticleContainersMerger(c_hadrons, name=name)


@configurable
def make_doublecharm_samesign(process, name="bandq_doublecharm_samesign_{hash}"):
    dz = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(), "[D0 -> K- pi+]cc"
    )  # taken from Hlt2Charm_D0ToKmPip line in charm area
    dzKppipipi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        "[D0 -> K+ pi- pi- pi+]cc",
    )
    dp = d_to_hhh._make_d_or_ds_to_hhh(
        [d_to_hhh._make_kaons(), d_to_hhh._make_pions(), d_to_hhh._make_pions()],
        # name='DToHHH_DpDspToKmPipPip_forDoubleCharm_{hash}',
        name="Charm_DToHHH_DpDspToKmPipPip_{hash}",
        descriptor="[D+ -> K- pi+ pi+]cc",
    )  # taken from Hlt2Charm_DpDspToKmPipPip
    ds = d_to_hhh._make_d_or_ds_to_hhh(
        [d_to_hhh._make_kaons(), d_to_hhh._make_kaons(), d_to_hhh._make_pions()],
        # name='DToHHH_DpDspToKmKpPip_forDoubleCharm_{hash}',
        name="Charm_DToHHH_DpDspToKmKpPip_{hash}",
        descriptor="[D_s+ -> K- K+ pi+]cc",
    )  # taken from Hlt2Charm_DpDspToKmKpPip
    lc = cbaryon_spectroscopy._make_lcp_pKpi()  # taken from Hlt2Charm_LcpToPpKmPip_PR
    xicz = (
        cbaryon_spectroscopy._make_xic0_pkkpi()
    )  # taken from Hlt2Charm_Xic0ToPpKmKmPip_PR
    omegac = c_to_hadrons.make_TightOmegaczToPpKmKmPip()  # taken from BandQ combiner
    jpsi = make_jpsi()  # taken from BandQ & B2CC shared combiner
    psi2s = make_psi2s()  # taken from BandQ & B2CC shared combiner
    c_hadron = ParticleContainersMerger(
        [dz, dzKppipipi, dp, ds, lc, xicz, omegac, jpsi, psi2s],
        name="bandq_charmToHadrons_{hash}",
    )
    line_alg = make_doublecharm(
        name=name,
        particles=[c_hadron, c_hadron],
        process=process,
        descriptors=[
            "[psi(3770) -> D0 D0]cc",
            "[psi(3770) -> D0 D+]cc",
            "[psi(3770) -> D0 D_s+]cc",
            "[psi(3770) -> D0 Lambda_c+]cc",
            "[psi(3770) -> D0 Xi_c0]cc",
            "[psi(3770) -> D0 Omega_c0]cc",
            "[psi(3770) -> D+ D+]cc",
            "[psi(3770) -> D+ D_s+]cc",
            "[psi(3770) -> D+ Lambda_c+]cc",
            "[psi(3770) -> D+ Xi_c0]cc",
            "[psi(3770) -> D+ Omega_c0]cc",
            "[psi(3770) -> D_s+ D_s+]cc",
            "[psi(3770) -> D_s+ Lambda_c+]cc",
            "[psi(3770) -> D_s+ Xi_c0]cc",
            "[psi(3770) -> D_s+ Omega_c0]cc",
            "[psi(3770) -> Lambda_c+ Lambda_c+]cc",
            "[psi(3770) -> Lambda_c+ Xi_c0]cc",
            "[psi(3770) -> Lambda_c+ Omega_c0]cc",
            "[psi(3770) -> Xi_c0 Xi_c0]cc",
            "[psi(3770) -> Xi_c0 Omega_c0]cc",
            "[psi(3770) -> Omega_c0 Omega_c0]cc",
            "[psi(3770) -> J/psi(1S) D0]cc",
            "[psi(3770) -> J/psi(1S) D+]cc",
            "[psi(3770) -> J/psi(1S) D_s+]cc",
            "[psi(3770) -> J/psi(1S) Lambda_c+]cc",
            "[psi(3770) -> J/psi(1S) Xi_c0]cc",
            "[psi(3770) -> J/psi(1S) Omega_c0]cc",
            "[psi(3770) -> psi(2S) D0]cc",
            "[psi(3770) -> psi(2S) D+]cc",
            "[psi(3770) -> psi(2S) D_s+]cc",
            "[psi(3770) -> psi(2S) Lambda_c+]cc",
            "[psi(3770) -> psi(2S) Xi_c0]cc",
            "[psi(3770) -> psi(2S) Omega_c0]cc",
        ],
    )
    return line_alg


@configurable
def make_doublecharm_oppositesign(
    process, name="bandq_doublecharm_oppositesign_{hash}"
):
    dz = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(), "[D0 -> K- pi+]cc"
    )  # taken from Hlt2Charm_D0ToKmPip line in charm area
    dzKppipipi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        "[D0 -> K+ pi- pi- pi+]cc",
    )
    dp = d_to_hhh._make_d_or_ds_to_hhh(
        [d_to_hhh._make_kaons(), d_to_hhh._make_pions(), d_to_hhh._make_pions()],
        # name='DToHHH_DpDspToKmPipPip_forDoubleCharm_{hash}',
        name="Charm_DToHHH_DpDspToKmPipPip_{hash}",
        descriptor="[D+ -> K- pi+ pi+]cc",
    )  # taken from Hlt2Charm_DpDspToKmPipPip
    ds = d_to_hhh._make_d_or_ds_to_hhh(
        [d_to_hhh._make_kaons(), d_to_hhh._make_kaons(), d_to_hhh._make_pions()],
        # name='DToHHH_DpDspToKmKpPip_forDoubleCharm_{hash}',
        name="Charm_DToHHH_DpDspToKmKpPip_{hash}",
        descriptor="[D_s+ -> K- K+ pi+]cc",
    )  # taken from Hlt2Charm_DpDspToKmKpPip
    lc = cbaryon_spectroscopy._make_lcp_pKpi()  # taken from Hlt2Charm_LcpToPpKmPip_PR
    xicz = (
        cbaryon_spectroscopy._make_xic0_pkkpi()
    )  # taken from Hlt2Charm_Xic0ToPpKmKmPip_PR
    omegac = c_to_hadrons.make_TightOmegaczToPpKmKmPip()  # taken from BandQ combiner
    c_hadron = ParticleContainersMerger(
        [dz, dzKppipipi, dp, ds, lc, xicz, omegac], name="bandq_charmToHadrons_{hash}"
    )
    line_alg = make_doublecharm(
        name=name,
        particles=[c_hadron, c_hadron],
        process=process,
        descriptors=[
            "[psi(3770) -> D~0 D0]cc",
            "[psi(3770) -> D~0 D+]cc",
            "[psi(3770) -> D~0 D_s+]cc",
            "[psi(3770) -> D~0 Lambda_c+]cc",
            "[psi(3770) -> D~0 Xi_c0]cc",
            "[psi(3770) -> D~0 Omega_c0]cc",
            "[psi(3770) -> D- D+]cc",
            "[psi(3770) -> D- D_s+]cc",
            "[psi(3770) -> D- Lambda_c+]cc",
            "[psi(3770) -> D- Xi_c0]cc",
            "[psi(3770) -> D- Omega_c0]cc",
            "[psi(3770) -> D_s- D_s+]cc",
            "[psi(3770) -> D_s- Lambda_c+]cc",
            "[psi(3770) -> D_s- Xi_c0]cc",
            "[psi(3770) -> D_s- Omega_c0]cc",
            "[psi(3770) -> Lambda_c~- Lambda_c+]cc",
            "[psi(3770) -> Lambda_c~- Xi_c0]cc",
            "[psi(3770) -> Lambda_c~- Omega_c0]cc",
            "[psi(3770) -> Xi_c~0 Xi_c0]cc",
            "[psi(3770) -> Xi_c~0 Omega_c0]cc",
            "[psi(3770) -> Omega_c~0 Omega_c0]cc",
        ],
    )
    return line_alg


"""
Make B&Q quantum-correlated D0 anti-D0 combinations.
"""


@configurable
def make_doublecharm_D0ToHH(  # 2x2-body D0 decay modes  # note that this line contains D0 -> Kpi anti-D0 -> Kpi which is also triggered above.
    process, name="bandq_doublecharm_{hash}"
):
    dz = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(), "[D0 -> K- pi+]cc"
    )
    dzb = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(), "[D0 -> K+ pi-]cc"
    )
    dzPiPi = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(), "D0 -> pi- pi+"
    )
    dzKK = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_kaons(), "D0 -> K- K+"
    )

    D0_meson_HH = ParticleContainersMerger(
        [dz, dzb, dzPiPi, dzKK], name="bandq_charmToXD0_2body_{hash}"
    )
    line_alg = make_doublecharm(
        name=name,
        particles=[D0_meson_HH, D0_meson_HH],
        process=process,
        # max_chi2ndof=8,
        # max_ipchi2=16,
        descriptors=["psi(3770) -> D0 D0"],
    )
    return line_alg


@configurable
def make_doublecharm_D0ToKsLLHH_D0ToHH(
    process, name="bandq_doublecharm_{hash}"
):  # 3(LL)x2-body D0 decay modes
    dz = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(), "[D0 -> K- pi+]cc"
    )
    dzb = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(), "[D0 -> K+ pi-]cc"
    )
    dzPiPi = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(), "D0 -> pi- pi+"
    )
    dzKK = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_kaons(), "D0 -> K- K+"
    )
    pions = d0_to_kshh.make_charm_pions()
    kaons = d0_to_kshh.make_charm_kaons()
    kshortsll = d0_to_kshh.make_kshort_ll()
    dzKsPiPill = d0_to_kshh.make_dzeros(kshortsll, pions, pions, "D0 -> KS0 pi- pi+")
    dzKsKKll = d0_to_kshh.make_dzeros(kshortsll, kaons, kaons, "D0 -> KS0 K- K+")
    dzKsKmPipll = d0_to_kshh.make_dzeros(
        kshortsll, kaons, pions, "[D0 -> KS0 K- pi+]cc"
    )
    dzKsKpPimll = d0_to_kshh.make_dzeros(
        kshortsll, kaons, pions, "[D0 -> KS0 K+ pi-]cc"
    )

    D0_meson_HH = ParticleContainersMerger(
        [dz, dzb, dzPiPi, dzKK], name="bandq_charmToXD0_2body_{hash}"
    )
    D0_meson_KsLLHH = ParticleContainersMerger(
        [dzKsPiPill, dzKsKKll, dzKsKmPipll, dzKsKpPimll],
        name="bandq_charmToXD0_3bodyll_{hash}",
    )
    line_alg = make_doublecharm(
        name=name,
        particles=[D0_meson_KsLLHH, D0_meson_HH],
        process=process,
        descriptors=[
            "psi(3770) -> D0 D0"  # contains all possible combinations ending with a KS. "KS" is pi+pi-, contains both KS and antiparticle.
        ],
        allowDiffInputsForSameIDChildren_dc=True,
        max_mass=5000 * MeV,
        max_ipchi2=30,
        max_chi2ndof=30,
    )
    return line_alg


@configurable
def make_doublecharm_D0ToKsDDHH_D0ToHH(
    process, name="bandq_doublecharm_{hash}"
):  # 3(DD)x2-body D0 decay modes
    dz = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(), "[D0 -> K- pi+]cc"
    )
    dzb = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(), "[D0 -> K+ pi-]cc"
    )
    dzPiPi = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(), "D0 -> pi- pi+"
    )
    dzKK = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_kaons(), "D0 -> K- K+"
    )
    pions = d0_to_kshh.make_charm_pions()
    kaons = d0_to_kshh.make_charm_kaons()
    kshortsdd = d0_to_kshh.make_kshort_dd()
    dzKsPiPidd = d0_to_kshh.make_dzeros(kshortsdd, pions, pions, "D0 -> KS0 pi- pi+")
    dzKsKKdd = d0_to_kshh.make_dzeros(kshortsdd, kaons, kaons, "D0 -> KS0 K- K+")
    dzKsKmPipdd = d0_to_kshh.make_dzeros(
        kshortsdd, kaons, pions, "[D0 -> KS0 K- pi+]cc"
    )
    dzKsKpPimdd = d0_to_kshh.make_dzeros(
        kshortsdd, kaons, pions, "[D0 -> KS0 K+ pi-]cc"
    )

    D0_meson_HH = ParticleContainersMerger(
        [dz, dzb, dzPiPi, dzKK], name="bandq_charmToXD0_2body_{hash}"
    )
    D0_meson_KsDDHH = ParticleContainersMerger(
        [dzKsPiPidd, dzKsKKdd, dzKsKmPipdd, dzKsKpPimdd],
        name="bandq_charmToXD0_3bodydd_{hash}",
    )
    line_alg = make_doublecharm(
        name=name,
        particles=[D0_meson_KsDDHH, D0_meson_HH],
        process=process,
        descriptors=[
            "psi(3770) -> D0 D0"  # contains all possible combinations ending with a KS. "KS" is pi+pi-, contains both KS and antiparticle.
        ],
        allowDiffInputsForSameIDChildren_dc=True,
        max_mass=5000 * MeV,
        max_ipchi2=30,
        max_chi2ndof=30,
    )
    return line_alg


@configurable
def make_doublecharm_D0ToKsLLHH(
    process, name="bandq_doublecharm_{hash}"
):  # 3(LL)x3(LL)-body D0 decay modes
    pions = d0_to_kshh.make_charm_pions()
    kaons = d0_to_kshh.make_charm_kaons()
    kshortsll = d0_to_kshh.make_kshort_ll()
    dzKsPiPill = d0_to_kshh.make_dzeros(kshortsll, pions, pions, "D0 -> KS0 pi- pi+")
    dzKsKKll = d0_to_kshh.make_dzeros(kshortsll, kaons, kaons, "D0 -> KS0 K- K+")
    dzKsKmPipll = d0_to_kshh.make_dzeros(
        kshortsll, kaons, pions, "[D0 -> KS0 K- pi+]cc"
    )
    dzKsKpPimll = d0_to_kshh.make_dzeros(
        kshortsll, kaons, pions, "[D0 -> KS0 K+ pi-]cc"
    )

    D0_meson_KsLLHH = ParticleContainersMerger(
        [dzKsPiPill, dzKsKKll, dzKsKmPipll, dzKsKpPimll],
        name="bandq_charmToXD0_3bodyll_{hash}",
    )
    line_alg = make_doublecharm(
        name=name,
        particles=[D0_meson_KsLLHH, D0_meson_KsLLHH],
        process=process,
        descriptors=[
            "psi(3770) -> D0 D0"  # contains all possible combinations ending with two KS. "KS" is pi+pi-, contains both KS and antiparticle.
        ],
        max_mass=5000 * MeV,
        max_ipchi2=30,
        max_chi2ndof=30,
    )
    return line_alg


@configurable
def make_doublecharm_D0ToKsLLHH_D0ToKsDDHH(
    process, name="bandq_doublecharm_{hash}"
):  # 3(LL)x3(DD)-body D0 decay modes
    pions = d0_to_kshh.make_charm_pions()
    kaons = d0_to_kshh.make_charm_kaons()
    kshortsll = d0_to_kshh.make_kshort_ll()
    dzKsPiPill = d0_to_kshh.make_dzeros(kshortsll, pions, pions, "D0 -> KS0 pi- pi+")
    dzKsKKll = d0_to_kshh.make_dzeros(kshortsll, kaons, kaons, "D0 -> KS0 K- K+")
    dzKsKmPipll = d0_to_kshh.make_dzeros(
        kshortsll, kaons, pions, "[D0 -> KS0 K- pi+]cc"
    )
    dzKsKpPimll = d0_to_kshh.make_dzeros(
        kshortsll, kaons, pions, "[D0 -> KS0 K+ pi-]cc"
    )
    kshortsdd = d0_to_kshh.make_kshort_dd()
    dzKsPiPidd = d0_to_kshh.make_dzeros(kshortsdd, pions, pions, "D0 -> KS0 pi- pi+")
    dzKsKKdd = d0_to_kshh.make_dzeros(kshortsdd, kaons, kaons, "D0 -> KS0 K- K+")
    dzKsKmPipdd = d0_to_kshh.make_dzeros(
        kshortsdd, kaons, pions, "[D0 -> KS0 K- pi+]cc"
    )
    dzKsKpPimdd = d0_to_kshh.make_dzeros(
        kshortsdd, kaons, pions, "[D0 -> KS0 K+ pi-]cc"
    )

    D0_meson_KsLLHH = ParticleContainersMerger(
        [dzKsPiPill, dzKsKKll, dzKsKmPipll, dzKsKpPimll],
        name="bandq_charmToXD0_3bodyll_{hash}",
    )
    D0_meson_KsDDHH = ParticleContainersMerger(
        [dzKsPiPidd, dzKsKKdd, dzKsKmPipdd, dzKsKpPimdd],
        name="bandq_charmToXD0_3bodydd_{hash}",
    )
    line_alg = make_doublecharm(
        name=name,
        particles=[D0_meson_KsLLHH, D0_meson_KsDDHH],
        process=process,
        descriptors=[
            "psi(3770) -> D0 D0"  # contains all possible combinations ending with two KS. "KS" is pi+pi-, contains both KS and antiparticle.
        ],
        allowDiffInputsForSameIDChildren_dc=True,
        max_mass=5000 * MeV,
        max_ipchi2=30,
        max_chi2ndof=30,
    )
    return line_alg


@configurable
def make_doublecharm_D0ToKsDDHH(
    process, name="bandq_doublecharm_{hash}"
):  # 3(DD)x3(DD)-body D0 decay modes
    pions = d0_to_kshh.make_charm_pions()
    kaons = d0_to_kshh.make_charm_kaons()
    kshortsdd = d0_to_kshh.make_kshort_dd()
    dzKsPiPidd = d0_to_kshh.make_dzeros(kshortsdd, pions, pions, "D0 -> KS0 pi- pi+")
    dzKsKKdd = d0_to_kshh.make_dzeros(kshortsdd, kaons, kaons, "D0 -> KS0 K- K+")
    dzKsKmPipdd = d0_to_kshh.make_dzeros(
        kshortsdd, kaons, pions, "[D0 -> KS0 K- pi+]cc"
    )
    dzKsKpPimdd = d0_to_kshh.make_dzeros(
        kshortsdd, kaons, pions, "[D0 -> KS0 K+ pi-]cc"
    )

    D0_meson_KsDDHH = ParticleContainersMerger(
        [dzKsPiPidd, dzKsKKdd, dzKsKmPipdd, dzKsKpPimdd],
        name="bandq_charmToXD0_3bodydd_{hash}",
    )
    line_alg = make_doublecharm(
        name=name,
        particles=[D0_meson_KsDDHH, D0_meson_KsDDHH],
        process=process,
        descriptors=[
            "psi(3770) -> D0 D0"  # contains all possible combinations ending with two KS. "KS" is pi+pi-, contains both KS and antiparticle.
        ],
        max_mass=5000 * MeV,
        max_ipchi2=25,
        max_chi2ndof=25,
    )
    return line_alg


@configurable
def make_doublecharm_D0ToHHHH_D0ToHH(
    process, name="bandq_doublecharm_{hash}"
):  # 4x2-body D0 decay modes
    dz = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(), "[D0 -> K- pi+]cc"
    )
    dzb = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_pions(), "[D0 -> K+ pi-]cc"
    )
    dzPiPi = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_pions(), d0_to_hh.make_charm_pions(), "D0 -> pi- pi+"
    )
    dzKK = d0_to_hh.make_dzeros(
        d0_to_hh.make_charm_kaons(), d0_to_hh.make_charm_kaons(), "D0 -> K- K+"
    )
    dzKmpipipi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        "[D0 -> K- pi- pi+ pi+]cc",
    )
    dzKppipipi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        "[D0 -> K+ pi- pi- pi+]cc",
    )
    dzKKpipi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        "D0 -> K- K+ pi- pi+",
    )
    dzpipipipi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        "D0 -> pi- pi- pi+ pi+",
    )
    dzKmKKpi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        "[D0 -> K- K- K+ pi+]cc",
    )
    dzKpKKpi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        "[D0 -> K- K+ K+ pi-]cc",
    )

    D0_meson_HH = ParticleContainersMerger(
        [dz, dzb, dzPiPi, dzKK], name="bandq_charmToXD0_2body_{hash}"
    )
    D0_meson_HHHH = ParticleContainersMerger(
        [dzpipipipi, dzKmpipipi, dzKppipipi, dzKKpipi, dzKmKKpi, dzKpKKpi],
        name="bandq_charmToXD0_4body_{hash}",
    )
    line_alg = make_doublecharm(
        name=name,
        particles=[D0_meson_HHHH, D0_meson_HH],
        process=process,
        descriptors=[
            "psi(3770) -> D0 D0"  # contains all possible combinations of final states
        ],
        allowDiffInputsForSameIDChildren_dc=True,
        max_mass=5000 * MeV,
        max_ipchi2=25,
        max_chi2ndof=25,
    )
    return line_alg


@configurable
def make_doublecharm_D0ToHHHH_D0ToKsLLHH(
    process, name="bandq_doublecharm_{hash}"
):  # 4x3(LL)-body D0 decay modes
    pions = d0_to_kshh.make_charm_pions()
    kaons = d0_to_kshh.make_charm_kaons()
    kshortsll = d0_to_kshh.make_kshort_ll()
    dzKsPiPill = d0_to_kshh.make_dzeros(kshortsll, pions, pions, "D0 -> KS0 pi- pi+")
    dzKsKKll = d0_to_kshh.make_dzeros(kshortsll, kaons, kaons, "D0 -> KS0 K- K+")
    dzKsKmPipll = d0_to_kshh.make_dzeros(
        kshortsll, kaons, pions, "[D0 -> KS0 K- pi+]cc"
    )
    dzKsKpPimll = d0_to_kshh.make_dzeros(
        kshortsll, kaons, pions, "[D0 -> KS0 K+ pi-]cc"
    )
    dzKmpipipi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        "[D0 -> K- pi- pi+ pi+]cc",
    )
    dzKppipipi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        "[D0 -> K+ pi- pi- pi+]cc",
    )
    dzKKpipi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        "D0 -> K- K+ pi- pi+",
    )
    dzpipipipi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        "D0 -> pi- pi- pi+ pi+",
    )
    dzKmKKpi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        "[D0 -> K- K- K+ pi+]cc",
    )
    dzKpKKpi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        "[D0 -> K- K+ K+ pi-]cc",
    )

    D0_meson_HHHH = ParticleContainersMerger(
        [dzpipipipi, dzKmpipipi, dzKppipipi, dzKKpipi, dzKmKKpi, dzKpKKpi],
        name="bandq_charmToXD0_4body_{hash}",
    )
    D0_meson_KsLLHH = ParticleContainersMerger(
        [dzKsPiPill, dzKsKKll, dzKsKmPipll, dzKsKpPimll],
        name="bandq_charmToXD0_3bodyll_{hash}",
    )
    line_alg = make_doublecharm(
        name=name,
        particles=[D0_meson_HHHH, D0_meson_KsLLHH],
        process=process,
        descriptors=[
            "psi(3770) -> D0 D0"  # contains all final-state combinations. KS->pi+pi- contains both KS and its antiparticle.
        ],
        allowDiffInputsForSameIDChildren_dc=True,
        max_mass=5000 * MeV,
        max_ipchi2=30,
        max_chi2ndof=30,
    )
    return line_alg


@configurable
def make_doublecharm_D0ToHHHH_D0ToKsDDHH(
    process, name="bandq_doublecharm_{hash}"
):  # 4x3(DD)-body D0 decay modes
    pions = d0_to_kshh.make_charm_pions()
    kaons = d0_to_kshh.make_charm_kaons()
    kshortsdd = d0_to_kshh.make_kshort_dd()
    dzKsPiPidd = d0_to_kshh.make_dzeros(kshortsdd, pions, pions, "D0 -> KS0 pi- pi+")
    dzKsKKdd = d0_to_kshh.make_dzeros(kshortsdd, kaons, kaons, "D0 -> KS0 K- K+")
    dzKsKmPipdd = d0_to_kshh.make_dzeros(
        kshortsdd, kaons, pions, "[D0 -> KS0 K- pi+]cc"
    )
    dzKsKpPimdd = d0_to_kshh.make_dzeros(
        kshortsdd, kaons, pions, "[D0 -> KS0 K+ pi-]cc"
    )
    dzKmpipipi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        "[D0 -> K- pi- pi+ pi+]cc",
    )
    dzKppipipi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        "[D0 -> K+ pi- pi- pi+]cc",
    )
    dzKKpipi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        "D0 -> K- K+ pi- pi+",
    )
    dzpipipipi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        "D0 -> pi- pi- pi+ pi+",
    )
    dzKmKKpi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        "[D0 -> K- K- K+ pi+]cc",
    )
    dzKpKKpi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        "[D0 -> K- K+ K+ pi-]cc",
    )

    D0_meson_HHHH = ParticleContainersMerger(
        [dzpipipipi, dzKmpipipi, dzKppipipi, dzKKpipi, dzKmKKpi, dzKpKKpi],
        name="bandq_charmToXD0_4body_{hash}",
    )
    D0_meson_KsDDHH = ParticleContainersMerger(
        [dzKsPiPidd, dzKsKKdd, dzKsKmPipdd, dzKsKpPimdd],
        name="bandq_charmToXD0_3bodydd_{hash}",
    )
    line_alg = make_doublecharm(
        name=name,
        particles=[D0_meson_HHHH, D0_meson_KsDDHH],
        process=process,
        descriptors=[
            "psi(3770) -> D0 D0"  # contains all final-state combinations. KS->pi+pi- contains both KS and its antiparticle.
        ],
        allowDiffInputsForSameIDChildren_dc=True,
        max_mass=5000 * MeV,
        max_ipchi2=25,
        max_chi2ndof=25,
    )
    return line_alg


@configurable
def make_doublecharm_D0ToHHHH(
    process, name="bandq_doublecharm_{hash}"
):  # 4x4-body D0 decay modes
    dzKmpipipi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        "[D0 -> K- pi- pi+ pi+]cc",
    )
    dzKppipipi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        "[D0 -> K+ pi- pi- pi+]cc",
    )
    dzKKpipi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        "D0 -> K- K+ pi- pi+",
    )
    dzpipipipi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        d0_to_hhhh.make_charm_pions(),
        "D0 -> pi- pi- pi+ pi+",
    )
    dzKmKKpi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        "[D0 -> K- K- K+ pi+]cc",
    )
    dzKpKKpi = d0_to_hhhh.make_dzeros(
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_kaons(),
        d0_to_hhhh.make_charm_pions(),
        "[D0 -> K- K+ K+ pi-]cc",
    )

    D0_meson_HHHH = ParticleContainersMerger(
        [dzpipipipi, dzKmpipipi, dzKppipipi, dzKKpipi, dzKmKKpi, dzKpKKpi],
        name="bandq_charmToXD0_4body_{hash}",
    )
    line_alg = make_doublecharm(
        name=name,
        particles=[D0_meson_HHHH, D0_meson_HHHH],
        process=process,
        descriptors=[
            "psi(3770) -> D0 D0"  # contains all final-state combinations. KS->pi+pi- contains both KS and its antiparticle.
        ],
        max_mass=5000 * MeV,
        max_ipchi2=20,
        max_chi2ndof=20,
    )
    return line_alg
