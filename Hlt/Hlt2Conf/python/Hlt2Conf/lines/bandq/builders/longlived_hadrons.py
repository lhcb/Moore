###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Create long-lived strange hadrons
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV
from PyConf import configurable
from RecoConf.algorithms_thor import (
    ParticleCombiner,
    ParticleContainersMerger,
    ParticleFilter,
)
from RecoConf.standard_particles import (
    make_down_kaons_for_V0,
    make_down_pions_for_V0,
    make_KsDD,
    make_KsLL,
    make_LambdaDD,
    make_LambdaLL,
)

from Hlt2Conf.lines.bandq.builders import charged_hadrons
from Hlt2Conf.lines.config_pid import nopid_hadrons

##############
# KS0->pi+ pi-#
##############


@configurable
def make_Ks_merged(name="bandq_Ks_merged_{hash}"):
    Ks_LL = make_KsLL()
    Ks_DD = make_KsDD()
    return ParticleContainersMerger([Ks_LL, Ks_DD], name=name)


##############
# Lambda->p pi#
##############


@configurable
def make_Lambda_merged(name="bandq_Lambda_merged_{hash}"):
    Lambda_LL = make_LambdaLL()
    Lambda_DD = make_LambdaDD()
    return ParticleContainersMerger([Lambda_LL, Lambda_DD], name=name)


### is it kaons & pions for other long-lived particles ?
@configurable
def make_pions_merged(name="bandq_pions_merged_{hash}"):
    pions_long = charged_hadrons.make_detached_pions()
    pions_down = make_down_pions_for_V0()
    return ParticleContainersMerger([pions_long, pions_down], name=name)


@configurable
def make_kaons_merged(name="bandq_kaons_merged_{hash}"):
    kaons_long = charged_hadrons.make_detached_kaons()
    kaons_down = make_down_kaons_for_V0()
    return ParticleContainersMerger([kaons_long, kaons_down], name=name)


#############
# Xi->Lambda pi and Omega -> Lambda K
#############


@configurable
def make_Lambda_fromLongLived(
    name="bandq_Lambda_fromLongLived_{hash}",
    ownpvdls_min=5.0,
    maxVertexChi2=20.0,
    m_min=1100.7 * MeV,
    m_max=1130.7 * MeV,
):
    code = F.require_all(
        F.CHI2DOF < maxVertexChi2,
        F.OWNPVDLS > ownpvdls_min,
        in_range(m_min, F.MASS, m_max),
    )

    make_particles = make_Lambda_merged()

    return ParticleFilter(make_particles, name=name, Cut=F.FILTER(code))


@configurable
def make_pion_fromLongLived(
    name="bandq_pion_fromLongLived_{hash}", pid=(F.PID_K < 5.0)
):
    if nopid_hadrons():
        cut_pid = None
    else:
        cut_pid = F.FILTER(pid)

    make_particles = make_pions_merged()
    return ParticleFilter(make_particles, name=name, Cut=cut_pid)


@configurable
def make_kaon_fromLongLived(
    name="bandq_kaon_fromLongLived_{hash}", pid=(F.PID_K < 5.0)
):
    if nopid_hadrons():
        cut_pid = None
    else:
        cut_pid = F.FILTER(pid)

    make_particles = make_kaons_merged()
    return ParticleFilter(make_particles, name=name, Cut=cut_pid)


@configurable
def make_XiToLambdaPi(
    name="bandq_XiToLambaPi_{hash}",
    am_min=1284.8 * MeV,
    am_max=1344.8 * MeV,
    ownpvdls_min=5.0,
    maxVertexChi2=25.0,
):
    Lambda = make_Lambda_fromLongLived()
    pion = make_pion_fromLongLived()

    combination_code = in_range(am_min, F.MASS, am_max)

    vertex_code = F.require_all(F.CHI2DOF < maxVertexChi2, F.OWNPVDLS > ownpvdls_min)

    return ParticleCombiner(
        name=name,
        Inputs=[Lambda, pion],
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_OmegaToLambdaK(
    name="bandq_OmegaToLambaK_{hash}",
    am_min=1642.4 * MeV,
    am_max=1702.4 * MeV,
    ownpvdls_min=5.0,
    maxVertexChi2=25.0,
):
    Lambda = make_Lambda_fromLongLived()
    kaon = make_kaon_fromLongLived()

    combination_code = in_range(am_min, F.MASS, am_max)

    vertex_code = F.require_all(F.CHI2DOF < maxVertexChi2, F.OWNPVDLS > ownpvdls_min)

    return ParticleCombiner(
        name=name,
        Inputs=[Lambda, kaon],
        DecayDescriptor="[Omega- -> Lambda0 K-]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
