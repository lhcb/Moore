###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make B&Q B meson to double charm combinations use MVA-based selector for Xic and Lc.
"""

from PyConf import configurable
from RecoConf.algorithms_thor import ParticleContainersMerger

from Hlt2Conf.lines.bandq.builders import b_hadrons, c_to_hadrons


@configurable
def make_bds_to_doublecharm(
    particles,
    descriptors,
    name="bandq_make_bds_to_doublecharm_template_{hash}",
    **decay_arguments,
):
    assert len(descriptors) > 0
    bds_hadrons = []
    for descriptor in descriptors:
        bds_hadrons.append(
            b_hadrons.make_bds(
                particles=particles, descriptor=descriptor, name=name, **decay_arguments
            )
        )
    return ParticleContainersMerger(bds_hadrons, name=name)


def make_B2XcXc_oppositesign(process, name="bandq_B2XcXc_oppositesign_{hash}"):
    lcp = c_to_hadrons.make_LcToPpKmPip()
    xicp = c_to_hadrons.make_XicpToPpKmPip()
    c_hadron = ParticleContainersMerger([lcp, xicp], name="bandq_XcToHadrons_{hash}")
    line_alg = make_bds_to_doublecharm(
        particles=[c_hadron, c_hadron],
        descriptors=[
            "B0 -> Lambda_c~- Lambda_c+",
            "B0 -> Xi_c~- Xi_c+",
            "[B0 -> Lambda_c+ Xi_c~-]cc",
        ],
        name=name,
    )
    return line_alg
