###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of B&Q Tcc
"""

import Functors as F
from Functors import require_all
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, mm, picosecond
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.bandq.builders import (
    b_hadrons,
    c_to_hadrons,
    charged_hadrons,
    longlived_hadrons,
)


########################
# Tetraquark weak decay mode
########################
@configurable
def _make_charm_for_ds(
    particles,
    descriptor,
    name="bandq_Charm_for_ds",
    am_min=1800 * MeV,
    am_max=1940 * MeV,
    m_min=1810 * MeV,
    m_max=1930 * MeV,
    asumpt_min=3000 * MeV,  # to discuss
    apt_min=950 * MeV,
    comb_pt_min_at_least_one=1000.0 * MeV,
    comb_pt_min_at_least_two=400.0 * MeV,
    comb_mipchi2_min_at_least_one=8.0,
    comb_mipchi2_min_at_least_two=6.0,
    pt_min=1000 * MeV,
    achi2_doca_max=16,
    vtx_chi2pdof_max=9,
    ownpvltime_min=0.2 * picosecond,
    ownpvvdchi2_min=25,
    ownpvdira_min=0.95,
):
    combination_code = require_all(
        in_range(am_min, F.MASS, am_max),
        F.PT > apt_min,
        F.SUM(F.PT) > asumpt_min,
        F.SUM(F.PT > comb_pt_min_at_least_one) >= 1,
        F.SUM(F.PT > comb_pt_min_at_least_two) >= 2,
        F.SUM(F.OWNPVIPCHI2 > comb_mipchi2_min_at_least_one) >= 1,
        F.SUM(F.OWNPVIPCHI2 > comb_mipchi2_min_at_least_two) >= 2,
    )

    vertex_code = require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.CHI2DOF < vtx_chi2pdof_max,
        F.OWNPVLTIME > ownpvltime_min,
        F.OWNPVFDCHI2 > ownpvvdchi2_min,
        F.OWNPVDIRA > ownpvdira_min,
    )

    if len(particles) == 2:
        return ParticleCombiner(
            name=name,
            Inputs=particles,
            DecayDescriptor=descriptor,
            CombinationCut=combination_code,
            CompositeCut=vertex_code,
        )

    elif len(particles) == 3:
        combination12_code = require_all(
            F.MASS < am_max, F.SDOCACHI2(1, 2) < achi2_doca_max
        )

        combination_code &= require_all(
            F.SDOCACHI2(1, 3) < achi2_doca_max, F.SDOCACHI2(2, 3) < achi2_doca_max
        )

        return ParticleCombiner(
            name=name,
            Inputs=particles,
            DecayDescriptor=descriptor,
            Combination12Cut=combination12_code,
            CombinationCut=combination_code,
            CompositeCut=vertex_code,
        )

    if len(particles) == 4:
        combination12_code = require_all(
            F.MASS < am_max, F.SDOCACHI2(1, 2) < achi2_doca_max
        )

        combination123_code = require_all(
            F.MASS < am_max,
            F.SDOCACHI2(1, 3) < achi2_doca_max,
            F.SDOCACHI2(2, 3) < achi2_doca_max,
        )

        combination_code &= require_all(
            F.SDOCACHI2(1, 4) < achi2_doca_max,
            F.SDOCACHI2(2, 4) < achi2_doca_max,
            F.SDOCACHI2(3, 4) < achi2_doca_max,
        )

    return ParticleCombiner(
        name=name,
        Inputs=particles,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        Combination123Cut=combination123_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_ds_for_Zcc(
    particles,
    descriptor,
    name="bandq_Ds_for_Zcc{hash}",
    am_min=1900 * MeV,
    am_max=2035 * MeV,
    m_min=1910 * MeV,
    m_max=2025 * MeV,
    **decay_arguments,
):
    return _make_charm_for_ds(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments,
    )


@configurable
def make_DspToKmKpPip_tightip(name="bandq_DspToKmKpPip_tightip_{hash}"):
    kaons = charged_hadrons.make_detached_kaons(mipchi2dvprimary_min=16.0)
    pions = charged_hadrons.make_detached_pions(mipchi2dvprimary_min=16.0)
    line_alg = make_ds_for_Zcc(
        name=name, particles=[kaons, kaons, pions], descriptor="[D_s+ -> K- K+ pi+]cc"
    )
    return line_alg


@configurable
def make_ZccToD0PhiPip(name="bandq_ZccToD0PhiPip_{hash}"):
    phi = charged_hadrons.make_detached_phi()
    pion = charged_hadrons.make_detached_pions(
        mipchi2dvprimary_min=12.0, pt_min=100.0 * MeV
    )
    Dz = c_to_hadrons.make_Dz_tightip()
    line_alg = b_hadrons.make_bu(
        name=name,
        particles=[Dz, phi, pion],
        descriptor="[B+ -> D0 phi(1020) pi+ ]cc",
        ownpvdira_min=0.999,
        am_min=3250 * MeV,
        am_max=4550 * MeV,
        m_min=3300 * MeV,
        m_max=4500 * MeV,
        ownpvltime_min=0.03 * picosecond,
    )
    return line_alg


@configurable
def make_ZccToDpPhiPip(name="bandq_ZccToDpPhiPip_{hash}"):
    phi = charged_hadrons.make_detached_phi()
    pion = charged_hadrons.make_detached_pions(
        mipchi2dvprimary_min=12.0, pt_min=100.0 * MeV
    )
    Dp = c_to_hadrons.make_DpToKmPipPip()
    line_alg = b_hadrons.make_bu(
        name=name,
        particles=[Dp, phi, pion],
        descriptor="[B+ -> D+ phi(1020) pi+ ]cc",
        ownpvdira_min=0.999,
        am_min=3250 * MeV,
        am_max=4550 * MeV,
        m_min=3300 * MeV,
        m_max=4500 * MeV,
        ownpvltime_min=0.03 * picosecond,
    )
    return line_alg


@configurable
def make_ZccToDspKmPip(name="bandq_ZccToDspKmPip_{hash}"):
    kaon = charged_hadrons.make_detached_kaons(
        mipchi2dvprimary_min=12.0, pt_min=500.0 * MeV
    )
    pion = charged_hadrons.make_detached_pions(
        mipchi2dvprimary_min=12.0, pt_min=500.0 * MeV
    )
    Dsp = make_DspToKmKpPip_tightip()
    myzcc = b_hadrons.make_bu_tight(
        name=name,
        particles=[Dsp, kaon, pion],
        descriptor="[B+ -> D_s+ K- pi+ ]cc",
        ownpvdira_min=0.999,
        am_min=3250 * MeV,
        am_max=4550 * MeV,
        m_min=3300 * MeV,
        m_max=4500 * MeV,
        ownpvltime_min=0.03 * picosecond,
    )
    code = F.require_all(F.CHILD(1, F.END_VZ) - F.END_VZ > 0.01 * mm)
    line_alg = ParticleFilter(myzcc, F.FILTER(code))
    return line_alg


@configurable
def make_ZccToDspKSPip(name="bandq_ZccToDspKSPip_{hash}"):
    Ks = longlived_hadrons.make_Ks_merged()
    pion = charged_hadrons.make_detached_pions(
        mipchi2dvprimary_min=12.0, pt_min=500.0 * MeV
    )
    Dsp = make_DspToKmKpPip_tightip()
    myzcc = b_hadrons.make_bu_tight(
        name=name,
        particles=[Dsp, Ks, pion],
        descriptor="[B+ -> D_s+ KS0 pi+ ]cc",
        ownpvdira_min=0.999,
        am_min=3250 * MeV,
        am_max=4550 * MeV,
        m_min=3300 * MeV,
        m_max=4500 * MeV,
        ownpvltime_min=0.03 * picosecond,
    )
    code = F.require_all(F.CHILD(1, F.END_VZ) - F.END_VZ > 0.01 * mm)
    line_alg = ParticleFilter(myzcc, F.FILTER(code))
    return line_alg
