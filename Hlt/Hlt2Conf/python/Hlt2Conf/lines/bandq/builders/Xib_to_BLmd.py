###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from Functors import require_all
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner

from Hlt2Conf.lines.bandq.builders import b_for_spectroscopy, longlived_hadrons


@configurable
def make_exited_Xib(
    particles,
    descriptor,
    name="bandq_exited_Xib_{hash}",
    am_min=6200 * MeV,
    am_max=8500 * MeV,
    m_min=6300 * MeV,
    m_max=8000 * MeV,
    achi2_doca_max=16,
    vtx_chi2pdof_max=10.0,
    ownpvipchi2_max=15,
    ownpvdira_min=0.995,
):
    combination_code = require_all(
        in_range(am_min, F.MASS, am_max), F.SDOCACHI2(1, 2) < achi2_doca_max
    )

    vertex_code = require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < vtx_chi2pdof_max,
        F.OWNPVIPCHI2 < ownpvipchi2_max,
        F.OWNPVDIRA > ownpvdira_min,
    )

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_XibmToBuLmd(process, name="bandq_XibmToBuLmd_{hash}"):
    Bu = b_for_spectroscopy.make_Bu(process)
    Lambda = longlived_hadrons.make_Lambda_fromLongLived()
    return make_exited_Xib(
        name=name,
        particles=[Bu, Lambda],
        descriptor="[Xi_b- -> B- Lambda0]cc",
    )


@configurable
def make_XibpToBuLmd(process, name="bandq_XibmToBuLmd_{hash}"):
    Bu = b_for_spectroscopy.make_Bu(process)
    Lambda = longlived_hadrons.make_Lambda_fromLongLived()
    return make_exited_Xib(
        name=name,
        particles=[Bu, Lambda],
        descriptor="[Xi_b~+ -> B+ Lambda0]cc",
    )


@configurable
def make_Xib0ToBdLmd(process, name="bandq_Xib0ToBdLmd_{hash}"):
    Bd = b_for_spectroscopy.make_Bd(process)
    Lambda = longlived_hadrons.make_Lambda_fromLongLived()
    return make_exited_Xib(
        name=name,
        particles=[Bd, Lambda],
        descriptor="[Xi_b0 -> B0 Lambda0]cc",
    )
