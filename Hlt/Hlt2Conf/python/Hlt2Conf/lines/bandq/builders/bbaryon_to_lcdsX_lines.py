###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make B&Q Lb -> Lc+ Ds- X combinations.
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from PyConf import configurable

from Hlt2Conf.lines.bandq.builders import (
    b_for_spectroscopy,
    c_to_hadrons,
    charged_hadrons,
)

########################################
# Lc+->p+K-pi+,    Ds-->K+K-pi-        #
# directly start from registers        #
########################################


@configurable
def make_bbaryon2lcdsX(
    particles,
    descriptor,
    name="bandq_bBaryonToLcDsmX_{hash}",
    am_min=5400 * MeV,
    am_max=5940 * MeV,
    m_min=5500 * MeV,
    m_max=5840 * MeV,
    sum_pt_min=5 * GeV,
    bcvtx_sep_min=-1 * mm,
    **decay_arguments,
):
    if bcvtx_sep_min is not None:
        Lc_vertex_code = F.CHILD(1, F.END_VZ) - F.END_VZ > bcvtx_sep_min
        Ds_vertex_code = F.CHILD(2, F.END_VZ) - F.END_VZ > bcvtx_sep_min
        vertex_code = Lc_vertex_code & Ds_vertex_code

    return b_for_spectroscopy.make_b2cx_for_spectroscopy(
        particles=particles,
        descriptor=descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        sum_pt_min=sum_pt_min,
        bcvtx_sep_min=bcvtx_sep_min,
        vtx_cut_add=vertex_code,
        **decay_arguments,
    )


@configurable
def make_LbToLcDsm(name="bandq_LbToLcDsm_{hash}"):
    lc = c_to_hadrons.make_tight_LcToPpKmPip()
    ds = c_to_hadrons.make_tight_DspToKmKpPip()
    line_alg = make_bbaryon2lcdsX(
        name=name,
        particles=[lc, ds],
        descriptor="[ Lambda_b0 -> Lambda_c+ D_s-]cc",
        am_min=5400.0 * MeV,
        am_max=5940.0 * MeV,
        m_min=5450.0 * MeV,
        m_max=5900.0 * MeV,
    )
    return line_alg


@configurable
def make_LbToLcDsmPiPi(name="bandq_LbToLcDsmPiPi_{hash}"):
    lc = c_to_hadrons.make_tight_LcToPpKmPip()
    ds = c_to_hadrons.make_tight_DspToKmKpPip()
    pion = charged_hadrons.make_detached_pions()
    line_alg = make_bbaryon2lcdsX(
        name=name,
        particles=[lc, ds, pion, pion],
        descriptor="[ Lambda_b0 -> Lambda_c+ D_s- pi+ pi- ]cc",
        am_min=5400.0 * MeV,
        am_max=5940.0 * MeV,
        m_min=5500.0 * MeV,
        m_max=5840.0 * MeV,
    )
    return line_alg


@configurable
def make_LbToLcDsmKK(name="bandq_LbToLcDsmKK_{hash}"):
    lc = c_to_hadrons.make_tight_LcToPpKmPip()
    ds = c_to_hadrons.make_tight_DspToKmKpPip()
    kaon = charged_hadrons.make_detached_kaons()
    line_alg = make_bbaryon2lcdsX(
        name=name,
        particles=[lc, ds, kaon, kaon],
        descriptor="[ Lambda_b0 -> Lambda_c+ D_s- K+ K- ]cc",
        am_min=5400.0 * MeV,
        am_max=5940.0 * MeV,
        m_min=5500.0 * MeV,
        m_max=5840.0 * MeV,
    )
    return line_alg


@configurable
def make_XibToLcDsmK(name="bandq_XibToLcDsmK_{hash}"):
    lc = c_to_hadrons.make_tight_LcToPpKmPip()
    ds = c_to_hadrons.make_tight_DspToKmKpPip()
    kaon = charged_hadrons.make_detached_kaons()
    line_alg = make_bbaryon2lcdsX(
        name=name,
        particles=[lc, ds, kaon],
        descriptor="[ Xi_b- -> Lambda_c+ D_s- K- ]cc",
        am_min=5580.0 * MeV,
        am_max=6120.0 * MeV,
        m_min=5680.0 * MeV,
        m_max=6020.0 * MeV,
    )
    return line_alg
