###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Set of constants/lists/dicts for B&Q
"""

from GaudiKernel.SystemOfUnits import MeV

# according to PDG 2022
_JPSI_PDG_MASS_ = 3096.90 * MeV
_PSI2S_PDG_MASS_ = 3686.10 * MeV

_PIP_PDG_MASS_ = 139.570 * MeV
_PI0_PDG_MASS_ = 134.977 * MeV

_KP_PDG_MASS_ = 493.677 * MeV
_K0_PDG_MASS_ = 497.611 * MeV

_MU_PDG_MASS_ = 105.658 * MeV

_PROTON_PDG_MASS_ = 938.272 * MeV
_LAMBDA_PDG_MASS_ = 1115.683 * MeV

_XIM_PDG_MASS_ = 1321.71 * MeV
_OMEGAM_PDG_MASS_ = 1672.45 * MeV

_BP_PDG_MASS_ = 5279.34 * MeV
_B0_PDG_MASS_ = 5279.66 * MeV
_BS_PDG_MASS_ = 5366.92 * MeV
_BC_PDG_MASS_ = 6274.47 * MeV
_LB_PDG_MASS_ = 5619.60 * MeV
_XIB0_PDG_MASS_ = 5791.9 * MeV
_XIBM_PDG_MASS_ = 5797.0 * MeV
_OMEGAB_PDG_MASS_ = 6045.2 * MeV

_DP_PDG_MASS_ = 1869.66 * MeV
_D0_PDG_MASS_ = 1864.84 * MeV
_DS_PDG_MASS_ = 1968.35 * MeV
_LC_PDG_MASS_ = 2286.46 * MeV
_XIC0_PDG_MASS_ = 2470.44 * MeV
_XICP_PDG_MASS_ = 2467.71 * MeV
_OMEGAC_PDG_MASS_ = 2695.2 * MeV

b_hadrons_list = {
    "B+": ("Bp", _BP_PDG_MASS_),
    "B-": ("Bm", _BP_PDG_MASS_),
    "B0": ("B0", _B0_PDG_MASS_),
    "B~0": ("B0b", _B0_PDG_MASS_),
    "B_s0": ("Bs", _BS_PDG_MASS_),
    "B_s~0": ("Bsbar", _BS_PDG_MASS_),
    "B_c+": ("Bc", _BC_PDG_MASS_),
    "B_c-": ("Bcbar", _BC_PDG_MASS_),
    "Lambda_b0": ("Lb", _LB_PDG_MASS_),
    "Lambda_b~0": ("Lbbar", _LB_PDG_MASS_),
    "Xi_b0": ("Xib0", _XIB0_PDG_MASS_),
    "Xi_b~0": ("Xib0bar", _XIB0_PDG_MASS_),
    "Xi_b-": ("Xibm", _XIBM_PDG_MASS_),
    "Xi_b~+": ("Xibp", _XIBM_PDG_MASS_),
    "Omega_b-": ("Omb", _OMEGAB_PDG_MASS_),
    "Omega_b~+": ("Ombbar", _OMEGAB_PDG_MASS_),
}
b_hadrons_only_particles = [
    "B+",
    "B0",
    "B_s0",
    "B_c+",
    "Lambda_b0",
    "Xi_b0",
    "Xi_b-",
    "Omega_b-",
]

c_hadrons_list = {
    "D+": ("Dp", _DP_PDG_MASS_),
    "D-": ("Dm", _DP_PDG_MASS_),
    "D0": ("D0", _D0_PDG_MASS_),
    "D~0": ("D0b", _D0_PDG_MASS_),
    "D_s+": ("Dsp", _DS_PDG_MASS_),
    "D_s-": ("Dsm", _DS_PDG_MASS_),
    "Lambda_c+": ("Lcp", _LC_PDG_MASS_),
    "Lambda_c~-": ("Lcm", _LC_PDG_MASS_),
    "Xi_c+": ("Xicp", _XICP_PDG_MASS_),
    "Xi_c~-": ("Xicm", _XICP_PDG_MASS_),
    "Xi_c0": ("Xic0", _XIC0_PDG_MASS_),
    "Xi_c~0": ("Xic0bar", _XIC0_PDG_MASS_),
    "Omega_c0": ("Omc", _OMEGAC_PDG_MASS_),
    "Omega_c~0": ("Omcbar", _OMEGAC_PDG_MASS_),
}

psis_list = {
    # code : (name, mass)
    "J/psi(1S)": ("Jpsi", _JPSI_PDG_MASS_),
    "psi(2S)": ("psi2S", _PSI2S_PDG_MASS_),
}

muons = {"mu+": ("Mup", _MU_PDG_MASS_), "mu-": ("Mum", _MU_PDG_MASS_)}

pions = {"pi+": ("Pip", _PIP_PDG_MASS_), "pi-": ("Pim", _PIP_PDG_MASS_)}
kaons = {"K+": ("Kp", _KP_PDG_MASS_), "K-": ("Km", _KP_PDG_MASS_)}
protons = {"p+": ("Pp", _PROTON_PDG_MASS_), "p~-": ("Pm", _PROTON_PDG_MASS_)}

charged_hadrons_list = {}
charged_hadrons_list.update(pions)
charged_hadrons_list.update(kaons)
charged_hadrons_list.update(protons)

ks = {"KS0": ("Ks", _K0_PDG_MASS_)}
lambdas = {
    "Lambda0": ("Lambda", _LAMBDA_PDG_MASS_),
    "Lambda~0": ("Lambdabar", _LAMBDA_PDG_MASS_),
}
xis = {"Xi-": ("Xim", _XIM_PDG_MASS_), "Xi~+": ("Xip", _XIM_PDG_MASS_)}
omegas = {
    "Omega-": ("Omegam", _OMEGAM_PDG_MASS_),
    "Omega~+": ("Omegap", _OMEGAM_PDG_MASS_),
}

longlived_hadrons_list = {}
longlived_hadrons_list.update(ks)
longlived_hadrons_list.update(lambdas)
longlived_hadrons_list.update(xis)
longlived_hadrons_list.update(omegas)

hadrons_list = {}
hadrons_list.update(charged_hadrons_list)
hadrons_list.update(longlived_hadrons_list)

all_particles_list = {}
all_particles_list.update(b_hadrons_list)
all_particles_list.update(c_hadrons_list)
all_particles_list.update(psis_list)
all_particles_list.update(hadrons_list)
all_particles_list.update(muons)

# set preferred particle order according to mass & charge
particles_order = [  # "Beauty",
    "B_c+",
    "B_c-",
    "Omega_b-",
    "Omega_b~+",
    "Xi_b-",
    "Xi_b~+",
    "Xi_b0",
    "Xi_b~0",
    "Lambda_b0",
    "Lambda_b~0",
    "B_s0",
    "B_s~0",
    "B0",
    "B~0",
    "B+",
    "B-",
    "psi(2S)",
    "J/psi",
    "Omega_c+",
    "Omega_c~-",
    "Xi_c0",
    "Xi_c~0",
    "Xi_c+",
    "Xi_c~-",
    "Lambda_c+",
    "Lambda_c-",
    "D_s+",
    "D_s-",
    "D+",
    "D-",
    "D0",
    "D~0",
    "Omega-",
    "Omega~+",
    "Xi-",
    "Xi~+",
    "Lambda0",
    "Lambda~0",
    "p+",
    "p~-",
    "KS0",
    "K+",
    "K-",
    "pi+",
    "pi-",
    "mu+",
    "mu-",
]


def get_order(particle):
    if particle in particles_order:
        return particles_order.index(particle)
    else:
        return -1


def exclude_high_mass(particles_list, mass_max):
    new_particles_list = {}
    for particle, particle_attrib in particles_list.items():
        particle_mass = particles_list[particle][1]
        if particle_mass <= mass_max:
            new_particles_list[particle] = particles_list[particle]
    return new_particles_list


def get_mass(particle):
    return all_particles_list[particle][1]


def get_mass_min(particles):
    return min([get_mass(particle) for particle in particles])


def descriptor_parser(descriptor):
    # returns mother_id, daughter_ids list
    # as from "[B+ -> J/psi(1S) K+ K-]cc" returns ("B+" , ["J/psi(1S)", "K+", "K-"])

    desc = (
        descriptor.replace("[", "").replace("]", "").replace("cc", "").replace("CC", "")
    )
    mother_id = desc.split("->")[0].strip()
    daughter_ids = [d.strip() for d in desc.split("->")[1].split()]

    return mother_id, daughter_ids


def make_name(particles):
    name = ""
    for particle in particles:
        name += all_particles_list[particle][0]
    return name
