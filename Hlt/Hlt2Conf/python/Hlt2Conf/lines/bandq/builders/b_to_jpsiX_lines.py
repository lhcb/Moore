###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make B&Q B -> dimuon X combinations.
"""

import Functors as F
from Functors import require_all
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV
from PyConf import ConfigurationError, configurable
from RecoConf.algorithms_thor import ParticleContainersMerger, ParticleFilter
from RecoConf.standard_particles import make_KsDD, make_KsLL

from Hlt2Conf.lines.bandq.builders import b_hadrons, charged_hadrons, longlived_hadrons
from Hlt2Conf.lines.charmonium_to_dimuon import (
    make_charmonium_muons as make_bandq_muons,
)
from Hlt2Conf.lines.charmonium_to_dimuon_detached import (
    make_detached_jpsi,
    make_detached_psi2s,
)

########################################
# Jpsi->mumu helps to control the rate #
# directly start from registers        #
########################################

jpsi_tight_m_min = 3020 * MeV
jpsi_tight_m_max = 3135 * MeV
psi2s_tight_m_min = 3600 * MeV
psi2s_tight_m_max = 3730 * MeV
psi_tight_probnnmu_min = 0.05
psi_tight_muons_avmips_min = 9


@configurable
def make_detached_jpsi_tight(
    name="bandq_detached_jpsi_tight_{hash}",
    muons_avmips_min=psi_tight_probnnmu_min,
    probnnmu_min=psi_tight_muons_avmips_min,
    tight_mass=False,
):
    jpsi = make_detached_jpsi()

    code = F.ALL

    if muons_avmips_min > 0:
        code &= (
            F.CHILD(1, F.OWNPVIPCHI2) + F.CHILD(2, F.OWNPVIPCHI2)
        ) / 2 > muons_avmips_min

    if probnnmu_min > 0:
        code &= require_all(
            F.CHILD(1, F.PROBNN_MU) > probnnmu_min,
            F.CHILD(2, F.PROBNN_MU) > probnnmu_min,
        )

    if tight_mass:
        code &= in_range(jpsi_tight_m_min, F.MASS, jpsi_tight_m_max)

    return ParticleFilter(jpsi, name=name, Cut=F.FILTER(code))


@configurable
def make_detached_psi2s_tight(
    name="bandq_detached_psi2s_tight_{hash}",
    muons_avmips_min=psi_tight_probnnmu_min,
    probnnmu_min=psi_tight_muons_avmips_min,
    tight_mass=False,
):
    psi2s = make_detached_psi2s()

    code = F.ALL

    if muons_avmips_min > 0:
        code &= (
            F.CHILD(1, F.OWNPVIPCHI2) + F.CHILD(2, F.OWNPVIPCHI2)
        ) / 2 > muons_avmips_min

    if probnnmu_min > 0:
        code &= require_all(
            F.CHILD(1, F.PROBNN_MU) > probnnmu_min,
            F.CHILD(2, F.PROBNN_MU) > probnnmu_min,
        )

    if tight_mass:
        code &= in_range(psi2s_tight_m_min, F.MASS, psi2s_tight_m_max)

    return ParticleFilter(psi2s, name=name, Cut=F.FILTER(code))


##########


@configurable
def make_BuToJpsiKp_JpsiToMuMu(
    process,
    name="bandq_BuToJpsiKp_JpsiToMuMu_{hash}",
    psi_tight=False,
    **decay_arguments,
):
    if process not in ["hlt2", "spruce"]:
        raise ConfigurationError(
            "process for make_BuToJpsiKp_JpsiToMuMu must be hlt2 or spruce. Please check !"
        )
    if process == "spruce":
        kaons = charged_hadrons.make_detached_kaons()
    elif process == "hlt2":
        kaons = charged_hadrons.make_detached_kaons(pid=(F.PID_K > 1.0))

    if not psi_tight:
        jpsi = make_detached_jpsi()
    else:
        jpsi = make_detached_jpsi_tight()

    line_alg = b_hadrons.make_bu(
        name=name,
        particles=[jpsi, kaons],
        descriptor="[B+ -> J/psi(1S) K+]cc",
        **decay_arguments,
    )
    return line_alg


@configurable
def make_BuToPsi2SKp_Psi2SToMuMu(
    process,
    name="bandq_BuToPsi2SKp_Psi2SToMuMu_{hash}",
    psi_tight=False,
    **decay_arguments,
):
    if process not in ["hlt2", "spruce"]:
        raise ConfigurationError(
            "process fomake_LbToEtacKp_Etac2SToKpKmPipPii2SKp_Psi2SToMuMur make_LbToEtacKp_Etac2SToKpKmPipPim must be hlt2 or spruce. Please check !"
        )
    if process == "spruce":
        kaons = charged_hadrons.make_detached_kaons()
    elif process == "hlt2":
        kaons = charged_hadrons.make_detached_kaons(pid=(F.PID_K > 1.0))

    if not psi_tight:
        psi = make_detached_psi2s()
    else:
        psi = make_detached_psi2s_tight()

    line_alg = b_hadrons.make_bu(
        name=name,
        particles=[psi, kaons],
        descriptor="[B+ -> psi(2S) K+]cc",
        **decay_arguments,
    )
    return line_alg


@configurable
def make_BuToJpsiPip_JpsiToMuMu(
    process, name="bandq_BuToJpsiPip_JpsiToMuMu_{hash}", psi_tight=False
):
    if process not in ["hlt2", "spruce"]:
        raise ConfigurationError(
            "process for make_BuToJpsiPip_JpsiToMuMu must be hlt2 or spruce. Please check !"
        )
    if process == "spruce":
        pions = charged_hadrons.make_detached_pions()
    elif process == "hlt2":
        pions = charged_hadrons.make_detached_pions(pid=(F.PID_K < -1.0))

    if not psi_tight:
        jpsi = make_detached_jpsi()
    else:
        jpsi = make_detached_jpsi_tight()

    line_alg = b_hadrons.make_bu(
        name=name, particles=[jpsi, pions], descriptor="[B+ -> J/psi(1S) pi+]cc"
    )
    return line_alg


@configurable
def make_BuToPsi2SPip_Psi2SToMuMu(
    process, name="bandq_BuToPsi2SPip_Psi2SToMuMu_{hash}", psi_tight=False
):
    if process not in ["hlt2", "spruce"]:
        raise ConfigurationError(
            "process for make_BuToPsi2SPip_Psi2SToMuMu must be hlt2 or spruce. Please check !"
        )
    if process == "spruce":
        pions = charged_hadrons.make_detached_pions()
    elif process == "hlt2":
        pions = charged_hadrons.make_detached_pions(pid=(F.PID_K < -1.0))

    if not psi_tight:
        psi = make_detached_psi2s()
    else:
        psi = make_detached_psi2s_tight()

    line_alg = b_hadrons.make_bu(
        name=name, particles=[psi, pions], descriptor="[B+ -> psi(2S) pi+]cc"
    )
    return line_alg


@configurable
def make_BdToJpsiKpPim_JpsiToMuMu(
    process,
    name="bandq_BdToJpsiKpPim_JpsiToMuMu_{hash}",
    psi_tight=False,
    **decay_arguments,
):
    if process == "spruce":
        kaons = charged_hadrons.make_detached_kaons()
        pions = charged_hadrons.make_detached_pions()
    elif process == "hlt2":
        kaons = charged_hadrons.make_detached_kaons(pid=(F.PID_K > 1.0))
        pions = charged_hadrons.make_detached_pions()

    if not psi_tight:
        jpsi = make_detached_jpsi()
    else:
        jpsi = make_detached_jpsi_tight()

    line_alg = b_hadrons.make_bds(
        name=name,
        particles=[jpsi, kaons, pions],
        descriptor="[B0 -> J/psi(1S) K+ pi-]cc",
        **decay_arguments,
    )
    return line_alg


@configurable
def make_BsToJpsiKpKm_JpsiToMuMu(
    process,
    name="bandq_BsToJpsiKpKm_JpsiToMuMu_{hash}",
    psi_tight=False,
    **decay_arguments,
):
    if process == "spruce":
        kaons = charged_hadrons.make_detached_kaons()
    elif process == "hlt2":
        kaons = charged_hadrons.make_detached_kaons(pid=(F.PID_K > 1.0))

    if not psi_tight:
        jpsi = make_detached_jpsi()
    else:
        jpsi = make_detached_jpsi_tight()

    line_alg = b_hadrons.make_bds(
        name=name,
        particles=[jpsi, kaons, kaons],
        descriptor="[B_s0 -> J/psi(1S) K+ K-]cc",
        **decay_arguments,
    )
    return line_alg


@configurable
def make_LbToJpsiPKm_JpsiToMuMu(
    process,
    name="bandq_LbToJpsiPKm_JpsiToMuMu_{hash}",
    psi_tight=False,
    **decay_arguments,
):
    if process == "spruce":
        kaons = charged_hadrons.make_detached_kaons()
        protons = charged_hadrons.make_detached_protons()
    elif process == "hlt2":
        kaons = charged_hadrons.make_detached_kaons(pid=(F.PID_K > 1.0))
        protons = charged_hadrons.make_detached_protons()

    if not psi_tight:
        jpsi = make_detached_jpsi()
    else:
        jpsi = make_detached_jpsi_tight()

    line_alg = b_hadrons.make_lb(
        name=name,
        particles=[jpsi, protons, kaons],
        descriptor="[Lambda_b0 -> J/psi(1S) p+ K-]cc",
        **decay_arguments,
    )
    return line_alg


@configurable
def make_BcToJpsiPip_JpsiToMuMu(
    process,
    name="bandq_BcToJpsiPip_JpsiToMuMu_{hash}",
    psi_tight=False,
    pi_pt_min=400.0 * MeV,
    pi_mipchi2_min=6,
    **decay_arguments,
):
    if process not in ["hlt2", "spruce"]:
        raise ConfigurationError(
            "process for make_BcToJpsiPip_JpsiToMuMu must be hlt2 or spruce. Please check !"
        )

    if process == "spruce":
        pions = charged_hadrons.make_detached_pions(
            pt_min=pi_pt_min, mipchi2dvprimary_min=pi_mipchi2_min
        )
    elif process == "hlt2":
        pions = charged_hadrons.make_detached_pions(
            pt_min=pi_pt_min, mipchi2dvprimary_min=pi_mipchi2_min, pid=(F.PID_K < -1.0)
        )

    if not psi_tight:
        jpsi = make_detached_jpsi()
    else:
        jpsi = make_detached_jpsi_tight()

    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[jpsi, pions],
        descriptor="[B_c+ -> J/psi(1S) pi+]cc",
        **decay_arguments,
    )
    return line_alg


@configurable
def make_BcToJpsiPipPipPim_JpsiToMuMu(
    process,
    name="bandq_BcToJpsiPipPipPim_JpsiToMuMu_{hash}",
    psi_tight=False,
    pi_pt_min=400.0 * MeV,
    pi_mipchi2_min=6,
    hhh_sumpt_min=2.0 * GeV,
    comb_cut_add=None,
    **decay_arguments,
):
    if process not in ["hlt2", "spruce"]:
        raise ConfigurationError("process must be hlt2 or spruce. Please check !")

    if process == "spruce":
        pions = charged_hadrons.make_detached_pions(
            pt_min=pi_pt_min, mipchi2dvprimary_min=pi_mipchi2_min
        )
    elif process == "hlt2":
        pions = charged_hadrons.make_detached_pions(
            pt_min=pi_pt_min, mipchi2dvprimary_min=pi_mipchi2_min, pid=(F.PID_K < -1.0)
        )

    if not psi_tight:
        jpsi = make_detached_jpsi()
    else:
        jpsi = make_detached_jpsi_tight()

    hhh_sumpt = (F.SUM(F.PT) - F.CHILD(1, F.PT)) > hhh_sumpt_min
    if comb_cut_add is None:
        comb_cut_add = hhh_sumpt
    else:
        comb_cut_add &= hhh_sumpt

    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[jpsi, pions, pions, pions],
        descriptor="[B_c+ -> J/psi(1S) pi+ pi+ pi-]cc",
        comb_cut_add=comb_cut_add,
        **decay_arguments,
    )
    return line_alg


@configurable
def make_BcToJpsi5Pi_JpsiToMuMu(
    process,
    name="bandq_BcToJpsi5Pi_JpsiToMuMu_{hash}",
    psi_tight=False,
    **decay_arguments,
):
    if process not in ["hlt2", "spruce"]:
        raise ConfigurationError("process must be hlt2 or spruce. Please check !")
    if process == "spruce":
        pions = charged_hadrons.make_detached_pions()
    elif process == "hlt2":
        pions = charged_hadrons.make_detached_pions(pid=(F.PID_K < -1.0))

    if not psi_tight:
        jpsi = make_detached_jpsi()
    else:
        jpsi = make_detached_jpsi_tight()

    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[jpsi, pions, pions, pions, pions, pions],
        descriptor="[B_c+ -> J/psi(1S) pi+ pi+ pi+ pi- pi-]cc",
        **decay_arguments,
    )
    return line_alg


@configurable
def make_BcToPsi2SPip_Psi2SToMuMu(
    process,
    name="bandq_BcToPsi2SPip_Psi2SToMuMu_{hash}",
    psi_tight=False,
):
    if process not in ["hlt2", "spruce"]:
        raise ConfigurationError(
            "process for make_BcToPsi2SPip_Psi2SToMuMu must be hlt2 or spruce. Please check !"
        )
    if process == "spruce":
        pions = charged_hadrons.make_detached_pions()
    elif process == "hlt2":
        pions = charged_hadrons.make_detached_pions(pid=(F.PID_K < -1.0))

    if not psi_tight:
        psi = make_detached_psi2s()
    else:
        psi = make_detached_psi2s_tight()

    line_alg = b_hadrons.make_bc(
        name=name, particles=[psi, pions], descriptor="[B_c+ -> psi(2S) pi+]cc"
    )
    return line_alg


@configurable
def make_BcToPsi2SPipPipPim_Psi2SToMuMu(
    process,
    name="bandq_BcToPsi2SPipPipPim_Psi2SToMuMu_{hash}",
    psi_tight=False,
):
    if process not in ["hlt2", "spruce"]:
        raise ConfigurationError(
            "process for make_BcToPsi2SPip_Psi2SToMuMu must be hlt2 or spruce. Please check !"
        )
    if process == "spruce":
        pions = charged_hadrons.make_detached_pions()
    elif process == "hlt2":
        pions = charged_hadrons.make_detached_pions(pid=(F.PID_K < -1.0))

    if not psi_tight:
        psi = make_detached_psi2s()
    else:
        psi = make_detached_psi2s_tight()

    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[psi, pions, pions, pions],
        descriptor="[B_c+ -> psi(2S) pi+ pi+ pi-]cc",
    )
    return line_alg


@configurable
def make_BcToJpsiMu_JpsiToMuMu(
    process,
    name="bandq_BcToJpsiMu_JpsiToMuMu_{hash}",
    psi_tight=False,
    minPt_muon=500 * MeV,
    minIPChi2_muon=4,
    minPIDmu=-5,
    am_min=3100 * MeV,
    am_max=7200 * MeV,
    m_min=3200 * MeV,
    m_max=7000 * MeV,
    **decay_arguments,
):
    if not psi_tight:
        jpsi = make_detached_jpsi()
    else:
        jpsi = make_detached_jpsi_tight()

    # muon = make_bandq_muons(minPt_muon=2500. * MeV, minPIDmu=-10.) # isn't 2.5 GeV too tight? and minPIDmu>-10 too loose?
    # also need a detached muon
    muon = make_bandq_muons(
        minPt_muon=minPt_muon, minIPChi2_muon=minIPChi2_muon, minPIDmu=minPIDmu
    )
    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[jpsi, muon],
        descriptor="[ B_c+ -> J/psi(1S) mu+ ]cc",
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments,
    )
    return line_alg


@configurable
def make_XibToJpsiXi_JpsiToMuMu(
    process,
    name="bandq_XibToJpsiXi_JpsiToMuMu_{hash}",
    psi_tight=False,
    am_min=5395.0 * MeV,
    am_max=6195.0 * MeV,
    m_min=5495.0 * MeV,
    m_max=6095.0 * MeV,
    **decay_arguments,
):
    if not psi_tight:
        jpsi = make_detached_jpsi()
    else:
        jpsi = make_detached_jpsi_tight()

    Xi = longlived_hadrons.make_XiToLambdaPi()
    line_alg = b_hadrons.make_b_hadron(
        name=name,
        particles=[jpsi, Xi],
        descriptor="[Xi_b- -> J/psi(1S) Xi-]cc",
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments,
    )
    return line_alg


@configurable
def make_XibToJpsiXiPi_JpsiToMuMu(
    process,
    name="bandq_XibToJpsiXiPi_JpsiToMuMu_{hash}",
    psi_tight=False,
    am_min=5195.0 * MeV,
    am_max=6395.0 * MeV,
    m_min=5295.0 * MeV,
    m_max=6295.0 * MeV,
    **decay_arguments,
):
    if not psi_tight:
        jpsi = make_detached_jpsi()
    else:
        jpsi = make_detached_jpsi_tight()

    Xi = longlived_hadrons.make_XiToLambdaPi()
    pions = charged_hadrons.make_detached_pions()
    line_alg = b_hadrons.make_b_hadron(
        name=name,
        particles=[jpsi, Xi, pions],
        descriptor="[Xi_b0 -> J/psi(1S) Xi- pi+]cc",
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments,
    )
    return line_alg


@configurable
def make_XibToJpsiPKK_JpsiToMuMu(
    process,
    name="bandq_XibToJpsiPKK_JpsiToMuMu_{hash}",
    psi_tight=False,
    am_min=5395.0 * MeV,
    am_max=6195.0 * MeV,
    m_min=5495.0 * MeV,
    m_max=6095.0 * MeV,
    **decay_arguments,
):
    if not psi_tight:
        jpsi = make_detached_jpsi()
    else:
        jpsi = make_detached_jpsi_tight()

    protons = charged_hadrons.make_detached_protons()
    kaons = charged_hadrons.make_detached_kaons()
    line_alg = b_hadrons.make_b_hadron(
        name=name,
        particles=[jpsi, protons, kaons, kaons],
        descriptor="[Xi_b- -> J/psi(1S) p+ K- K-]cc",
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments,
    )
    return line_alg


@configurable
def make_XibToJpsiPKKPi_JpsiToMuMu(
    process,
    name="bandq_XibToJpsiPKKPi_JpsiToMuMu_{hash}",
    psi_tight=False,
    am_min=5395.0 * MeV,
    am_max=6195.0 * MeV,
    m_min=5495.0 * MeV,
    m_max=6095.0 * MeV,
    **decay_arguments,
):
    if not psi_tight:
        jpsi = make_detached_jpsi()
    else:
        jpsi = make_detached_jpsi_tight()

    protons = charged_hadrons.make_detached_protons()
    kaons = charged_hadrons.make_detached_kaons()
    pions = charged_hadrons.make_detached_pions()
    line_alg = b_hadrons.make_b_hadron(
        name=name,
        particles=[jpsi, protons, kaons, kaons, pions],
        descriptor="[Xi_b0 -> J/psi(1S) p+ K- K- pi+]cc",
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments,
    )
    return line_alg


@configurable
def make_OmegabToJpsiOmega_JpsiToMuMu(
    process,
    name="bandq_OmegabToJpsiOmega_JpsiToMuMu_{hash}",
    psi_tight=False,
    am_min=5449.0 * MeV,
    am_max=6649.0 * MeV,
    m_min=5549.0 * MeV,
    m_max=6549.0 * MeV,
    **decay_arguments,
):
    if not psi_tight:
        jpsi = make_detached_jpsi()
    else:
        jpsi = make_detached_jpsi_tight()

    Omega = longlived_hadrons.make_OmegaToLambdaK()
    line_alg = b_hadrons.make_b_hadron(
        name=name,
        particles=[jpsi, Omega],
        descriptor="[Omega_b- -> J/psi(1S) Omega-]cc",
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments,
    )
    return line_alg


@configurable
def make_OmegabToJpsiPKKKPi_JpsiToMuMu(
    process,
    name="bandq_OmegabToJpsiPKKKPi_JpsiToMuMu_{hash}",
    psi_tight=False,
    am_min=5449.0 * MeV,
    am_max=6649.0 * MeV,
    m_min=5549.0 * MeV,
    m_max=6549.0 * MeV,
    **decay_arguments,
):
    if not psi_tight:
        jpsi = make_detached_jpsi()
    else:
        jpsi = make_detached_jpsi_tight()

    protons = charged_hadrons.make_detached_protons()
    kaons = charged_hadrons.make_detached_kaons()
    pions = charged_hadrons.make_detached_pions()
    line_alg = b_hadrons.make_b_hadron(
        name=name,
        particles=[jpsi, protons, kaons, kaons, kaons, pions],
        descriptor="[Omega_b- -> J/psi(1S) p+ K- K- K- pi+]cc",
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments,
    )
    return line_alg


#########################
# Xib -> Jpsi + X       #
#########################


@configurable
def make_XibToJpsiPKsLLK_JpsiToMuMu(
    name="bandq_XibToJpsiPKsLLK_JpsiToMuMu",
    psi_tight=False,
):
    if not psi_tight:
        jpsi = make_detached_jpsi()
    else:
        jpsi = make_detached_jpsi_tight()

    proton = charged_hadrons.make_detached_protons()
    ks = make_KsLL()
    kaon = charged_hadrons.make_detached_kaons()
    line_alg = b_hadrons.make_b_baryons(
        name=name,
        particles=[jpsi, proton, ks, kaon],
        descriptor="[ Xi_b0 -> J/psi(1S) p+ KS0 K- ]cc",
        am_min=5395.0 * MeV,
        am_max=6195.0 * MeV,
        m_min=5495.0 * MeV,
        m_max=6095.0 * MeV,
    )
    return line_alg


@configurable
def make_XibToJpsiPKsDDK_JpsiToMuMu(
    name="bandq_XibToJpsiPKsDDK_JpsiToMuMu",
    psi_tight=False,
):
    if not psi_tight:
        jpsi = make_detached_jpsi()
    else:
        jpsi = make_detached_jpsi_tight()

    proton = charged_hadrons.make_detached_protons()
    ks = make_KsDD()
    kaon = charged_hadrons.make_detached_kaons()
    line_alg = b_hadrons.make_b_baryons(
        name=name,
        particles=[jpsi, proton, ks, kaon],
        descriptor="[ Xi_b0 -> J/psi(1S) p+ KS0 K- ]cc",
        am_min=5395.0 * MeV,
        am_max=6195.0 * MeV,
        m_min=5495.0 * MeV,
        m_max=6095.0 * MeV,
    )
    return line_alg


@configurable
def make_XibToJpsiPKPi_JpsiToMuMu(
    name="bandq_XibToJpsiPKPi_JpsiToMuMu",
    psi_tight=False,
):
    if not psi_tight:
        jpsi = make_detached_jpsi()
    else:
        jpsi = make_detached_jpsi_tight()

    proton = charged_hadrons.make_detached_protons()
    kaon = charged_hadrons.make_detached_kaons()
    pion = charged_hadrons.make_detached_pions()
    line_alg = b_hadrons.make_b_baryons(
        name=name,
        particles=[jpsi, proton, kaon, pion],
        descriptor="[ Xi_b- -> J/psi(1S) p+ K- pi- ]cc",
        am_min=5445.0 * MeV,
        am_max=6145.0 * MeV,
        m_min=5545.0 * MeV,
        m_max=6045.0 * MeV,
    )
    return line_alg


########################################
# Generic Hexa_b -> Jpsi + X builder   #
########################################


def particle_maker(p_id):
    p_id = p_id.strip()
    if p_id in ["J/psi(1S)"]:
        return make_detached_jpsi()
    elif p_id in ["psi(2S)"]:
        return make_detached_psi2s()
    elif p_id in ["pi+", "pi-"]:
        return charged_hadrons.make_detached_pions()
    elif p_id in ["K+", "K-"]:
        return charged_hadrons.make_detached_kaons()
    elif p_id in ["p+", "p~-"]:
        return charged_hadrons.make_detached_protons()
    elif p_id in ["KS0", "KS~0"]:
        return longlived_hadrons.make_Ks_merged()
    elif p_id in ["Lambda0", "Lambda~0"]:
        return longlived_hadrons.make_Lambda_merged()
    elif p_id in ["Xi-", "Xi~+"]:
        return longlived_hadrons.make_XiToLambdaPi()
    elif p_id in ["Omega-", "Omega~+"]:
        return longlived_hadrons.make_OmegaToLambdaK()
    else:
        return None


def mass_limits(mother_id):
    mother_id = mother_id.strip()
    if mother_id in ["B+", "B-", "B0", "B~0", "B_s0", "B_s~0"]:
        m_min, m_max, dm = 5140 * MeV, 5510 * MeV, 40 * MeV
    elif mother_id in ["B_c+", "B_c-"]:
        m_min, m_max, dm = 6090 * MeV, 6510 * MeV, 50 * MeV
    elif mother_id in ["Lambda_b0", "Lambda_b~0"]:
        m_min, m_max, dm = 5390 * MeV, 5810 * MeV, 40 * MeV
    elif mother_id in ["Xi_b0", "Xi_b~0", "Xi_b-", "Xi_b~+", "Omega_b-", "Omega_b~+"]:
        m_min, m_max, dm = 5390 * MeV, 6410 * MeV, 50 * MeV
    elif mother_id in [
        "Xi_bc0",
        "Xi_bc~0",
        "Xi_bc+",
        "Xi_bc~-",
        "Omega_bc0",
        "Omega_bc~0",
    ]:
        m_min, m_max, dm = 6290 * MeV, 7950 * MeV, 75 * MeV
    elif mother_id == "ALL":
        m_min, m_max, dm = 5140 * MeV, 7950 * MeV, 75 * MeV
    else:
        print(mother_id)

    am_min = m_min - dm
    am_max = m_max + dm
    return am_min, am_max, m_min, m_max


from Hlt2Conf.lines.bandq.builders.helper import descriptor_parser, make_name


@configurable
def make_HbToPsiX_PsiToMuMu(name, descriptor, mass_limits_from_mother_id=False):
    mother_id, daughter_ids = descriptor_parser(descriptor)

    if mass_limits_from_mother_id:
        am_min, am_max, m_min, m_max = mass_limits(mother_id)
    else:
        am_min, am_max, m_min, m_max = mass_limits("ALL")

    particles = []
    for daughter_id in daughter_ids:
        particle = particle_maker(daughter_id)
        if particle is not None:
            particles += [particle]

    name = "make_" + make_name(daughter_ids)
    name += "_{hash}"

    line_alg = b_hadrons.make_b_hadron(
        name=name,
        particles=particles,
        descriptor=descriptor,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        achi2_doca_max=16,
        vtx_chi2pdof_max=10,
    )
    return line_alg


@configurable
def make_HbToPsiX_PsiToMuMu_combination(
    name, descriptors, mass_limits_from_mother_id=False
):
    assert len(descriptors) > 0
    algs = []
    for descriptor in descriptors:
        algs.append(
            make_HbToPsiX_PsiToMuMu(
                name, descriptor, mass_limits_from_mother_id=mass_limits_from_mother_id
            )
        )
    return ParticleContainersMerger(algs, name)
