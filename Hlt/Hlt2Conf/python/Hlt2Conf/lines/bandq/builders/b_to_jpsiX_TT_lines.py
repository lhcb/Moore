###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define a set of B -> dimuon X combinations, where X is reconstructed using T-tracks
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.standard_particles import make_KsTT_gated, make_LambdaTT_gated

from Hlt2Conf.lines.charmonium_to_dimuon_detached import make_detached_jpsi


def make_jpsi_for_jpsiXTT(input_jpsi=make_detached_jpsi):
    cut = F.require_all(
        F.CHI2 < 4,
        F.MAXSDOCA < 0.15 * mm,
        F.P > 10 * GeV,
        F.CHILD(1, F.PID_MU) > 0,
        F.CHILD(2, F.PID_MU) > 0,
    )
    return ParticleFilter(
        Input=input_jpsi(), Cut=F.FILTER(cut), name="bandq_filter_jpsi_for_jpsiXTT"
    )


@configurable
def make_Lb2JpsiLambdaTT(
    make_jpsi=make_jpsi_for_jpsiXTT,
    make_lambda=make_LambdaTT_gated,
    mass_combination_min=3 * GeV,
    mass_combination_max=10 * GeV,
    ownpvdira_min=0.985,
    ownpvip_max=0.2,
    ownpvipchi2_max=150.0,
    vertex_chi2_max=50.0,
    pt_min=600 * MeV,
    apply_vertex_mass_cut=True,
    mass_vertex_min=4.5 * GeV,
    mass_vertex_max=6.5 * GeV,
    name="make_Lb2JpsiLambdaTT",
):
    """
    Lb -> Jpsi Lambda(->p pi) combiner, where the Lambda decays after the UT.
    authors: Izaac Sanderswood, Brandon Roldan Tomei
    """
    decay_descriptor = "[Lambda_b0 -> J/psi(1S) Lambda0]cc"

    Jpsi = make_jpsi()
    lambdas = make_lambda()

    combination_cuts = F.require_all(
        in_range(mass_combination_min, F.MASS, mass_combination_max),
        F.MAXSDOCACUT(50.0),
        F.MAXSDOCACHI2CUT(30.0),
    )

    vertex_cuts = F.require_all(
        F.OWNPVDIRA > ownpvdira_min,
        F.OWNPVIP < ownpvip_max,
        F.OWNPVIPCHI2 < ownpvipchi2_max,
        F.CHI2 < vertex_chi2_max,
        F.PT > pt_min,
    )

    if apply_vertex_mass_cut:
        vertex_cuts &= in_range(mass_vertex_min, F.MASS, mass_vertex_max)

    return ParticleCombiner(
        Inputs=[Jpsi, lambdas],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name,
    )


@configurable
def make_Bd2JpsiKsTT(
    make_jpsi=make_jpsi_for_jpsiXTT,
    make_ks=make_KsTT_gated,
    mass_combination_min=2.6 * GeV,
    mass_combination_max=9.6 * GeV,
    ownpvdira_min=0.985,
    ownpvip_max=0.2,
    ownpvipchi2_max=150.0,
    vertex_chi2_max=50.0,
    pt_min=500 * MeV,
    apply_vertex_mass_cut=True,
    mass_vertex_min=4.5 * GeV,
    mass_vertex_max=6.1 * GeV,
    name="make_Bd2JpsiKsTT",
):
    """
    B0 -> Jpsi Ks0(->pi pi) combiner, where the Ks0 decays after the UT.
    authors: Izaac Sanderswood
    """
    decay_descriptor = "B0 -> J/psi(1S) KS0"

    Jpsi = make_jpsi()
    ks = make_ks()

    combination_cuts = F.require_all(
        in_range(mass_combination_min, F.MASS, mass_combination_max),
        F.MAXSDOCACUT(50.0),
        F.MAXSDOCACHI2CUT(30.0),
    )

    vertex_cuts = F.require_all(
        F.OWNPVDIRA > ownpvdira_min,
        F.OWNPVIP < ownpvip_max,
        F.OWNPVIPCHI2 < ownpvipchi2_max,
        F.CHI2 < vertex_chi2_max,
        F.PT > pt_min,
    )

    if apply_vertex_mass_cut:
        vertex_cuts &= in_range(mass_vertex_min, F.MASS, mass_vertex_max)

    return ParticleCombiner(
        Inputs=[Jpsi, ks],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_cuts,
        name=name,
    )
