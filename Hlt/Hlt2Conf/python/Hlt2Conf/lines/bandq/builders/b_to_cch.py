###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make B&Q double charm and meson combinations.
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from PyConf import configurable
from RecoConf.standard_particles import make_KsDD, make_KsLL

from Hlt2Conf.lines.bandq.builders import b_hadrons, c_to_hadrons, charged_hadrons


@configurable
def make_b_for_cch(
    name,
    particles,
    descriptor,
    ownpvfdchi2_min=0.0,
    achi2_doca_max=25.0,
    vtx_chi2pdof_max=20.0,
    am_min=5000.0 * MeV,
    am_max=6000.0 * MeV,
    m_min=5000.0 * MeV,
    m_max=5800.0 * MeV,
    comb_cut_add=None,
):
    comb_cut_add = F.require_all(F.SUM(F.PT) > 5000.0 * MeV)

    return b_hadrons.make_bds(
        name=name,
        particles=particles,
        descriptor=descriptor,
        minVDz=0.0 * mm,
        minRho=0.0 * mm,
        ownpvfdchi2_min=ownpvfdchi2_min,
        achi2_doca_max=achi2_doca_max,
        vtx_chi2pdof_max=vtx_chi2pdof_max,
        comb_cut_add=comb_cut_add,
    )


@configurable
def make_bu_for_cch(
    name,
    particles,
    descriptor,
    ownpvfdchi2_min=0.0,
    achi2_doca_max=25.0,
    vtx_chi2pdof_max=20.0,
    am_min=5000.0 * MeV,
    am_max=6000.0 * MeV,
    m_min=5000.0 * MeV,
    m_max=5800.0 * MeV,
    comb_cut_add=None,
):
    comb_cut_add = F.require_all(F.SUM(F.PT) > 5000.0 * MeV)

    return b_hadrons.make_bu(
        name=name,
        particles=particles,
        descriptor=descriptor,
        minVDz=0.0 * mm,
        minRho=0.0 * mm,
        ownpvfdchi2_min=ownpvfdchi2_min,
        achi2_doca_max=achi2_doca_max,
        vtx_chi2pdof_max=vtx_chi2pdof_max,
        comb_cut_add=comb_cut_add,
    )


@configurable
def make_lb0_for_cch(
    name,
    particles,
    descriptor,
    ownpvfdchi2_min=0.0,
    achi2_doca_max=25.0,
    vtx_chi2pdof_max=20.0,
    am_min=4900.0 * MeV,
    am_max=6100.0 * MeV,
    m_min=5000.0 * MeV,
    m_max=6000.0 * MeV,
    comb_cut_add=None,
):
    comb_cut_add = F.require_all(F.SUM(F.PT) > 5000.0 * MeV)

    return b_hadrons.make_lb(
        name=name,
        particles=particles,
        descriptor=descriptor,
        minVDz=0.0 * mm,
        minRho=0.0 * mm,
        ownpvfdchi2_min=ownpvfdchi2_min,
        achi2_doca_max=achi2_doca_max,
        vtx_chi2pdof_max=vtx_chi2pdof_max,
        comb_cut_add=comb_cut_add,
    )


def make_pions_from_B():
    return charged_hadrons.make_detached_pions(pt_min=100 * MeV, p_min=1 * GeV)


def make_kaons_from_B():
    return charged_hadrons.make_detached_kaons(pt_min=100 * MeV, p_min=1 * GeV)


"""
B0->Lc Lc KS0LL
"""


@configurable
def make_B0ToLcLcbarKsLL(name="bandq_B0ToLcLcbarKsLL_{hash}"):
    lc = c_to_hadrons.make_LcToPpKmPip()
    ks = make_KsLL()

    line_alg = make_b_for_cch(
        name=name, particles=[lc, lc, ks], descriptor="B0 -> Lambda_c+ Lambda_c~- KS0"
    )
    return line_alg


"""
B0->Lc Lc KS0DD
"""


@configurable
def make_B0ToLcLcbarKsDD(name="bandq_B0ToLcLcbarKsDD_{hash}"):
    lc = c_to_hadrons.make_LcToPpKmPip()
    ks = make_KsDD()

    line_alg = make_b_for_cch(
        name=name, particles=[lc, lc, ks], descriptor="B0 -> Lambda_c+ Lambda_c~- KS0"
    )
    return line_alg


"""
B+ ->Lc Lc Pi+
"""


@configurable
def make_BuToLcLcbarPip(name="bandq_BuToLcLcbarPip_{hash}"):
    lc = c_to_hadrons.make_LcToPpKmPip()
    pion = make_pions_from_B()

    line_alg = make_bu_for_cch(
        name=name,
        particles=[lc, lc, pion],
        descriptor="[B+ -> Lambda_c+ Lambda_c~- pi+]cc",
    )
    return line_alg


"""
B0 ->Lc Lc K- Pi+
"""


@configurable
def make_B0ToLcLcbarKmPip(name="bandq_B0ToLcLcbarKmPip_{hash}"):
    lc = c_to_hadrons.make_LcToPpKmPip()
    pion = make_pions_from_B()
    kaon = make_kaons_from_B()

    line_alg = make_b_for_cch(
        name=name,
        particles=[lc, lc, kaon, pion],
        descriptor="[ B0 -> Lambda_c+ Lambda_c~- K- pi+ ]cc",
    )
    return line_alg


"""
B0 ->Lc+ Xi_c~0 Pi-
"""


@configurable
def make_B0ToLcXic0barPim(name="bandq_B0ToLcXic0barPim_{hash}"):
    lc = c_to_hadrons.make_LcToPpKmPip()
    pion = make_pions_from_B()
    Xic0 = c_to_hadrons.make_XiczToPpKmKmPip()

    line_alg = make_b_for_cch(
        name=name,
        particles=[lc, Xic0, pion],
        descriptor="[ B0 -> Lambda_c+ Xi_c~0 pi- ]cc",
    )
    return line_alg


"""
Bs0 ->Lc+ Xi_c~0 K-
"""


@configurable
def make_BsToLcXic0barKm(name="bandq_BsToLcXic0barKm_{hash}"):
    lc = c_to_hadrons.make_LcToPpKmPip()
    Xic0 = c_to_hadrons.make_XiczToPpKmKmPip()
    kaon = make_kaons_from_B()

    line_alg = make_b_for_cch(
        name=name,
        particles=[lc, Xic0, kaon],
        descriptor="[ B_s0 -> Lambda_c+ Xi_c~0 K- ]cc",
    )
    return line_alg


"""
Bs0 ->Ds+ D~0 K-
"""


@configurable
def make_BsToDspD0barKm(name="bandq_BsToDspD0barKm_{hash}"):
    Ds = c_to_hadrons.make_DspToKmKpPip()
    Dz = c_to_hadrons.make_Dz()
    kaon = make_kaons_from_B()

    line_alg = make_b_for_cch(
        name=name, particles=[Ds, Dz, kaon], descriptor="[ B_s0 -> D_s+ D~0 K- ]cc"
    )
    return line_alg


"""
Lambda_b0 ->D- Xic0 Pi+
"""


@configurable
def make_Lb0ToDmXic0Pip(name="bandq_Lb0ToDmXic0Pip_{hash}"):
    Dp = c_to_hadrons.make_DpToKmPipPip()
    Xic0 = c_to_hadrons.make_XiczToPpKmKmPip()
    pion = make_pions_from_B()

    line_alg = make_lb0_for_cch(
        name=name,
        particles=[Dp, Xic0, pion],
        descriptor="[ Lambda_b0 -> D- Xi_c0 pi+ ]cc",
    )
    return line_alg


"""
Lambda_b0 ->D0bar Xic+ Pi-
"""


@configurable
def make_Lb0ToD0barXicpPim(name="bandq_Lb0ToD0barXicpPim_{hash}"):
    Dz = c_to_hadrons.make_Dz()
    Xicp = c_to_hadrons.make_XicpToPpKmPip()
    pion = make_pions_from_B()

    line_alg = make_lb0_for_cch(
        name=name,
        particles=[Dz, Xicp, pion],
        descriptor="[ Lambda_b0 -> D~0 Xi_c+ pi- ]cc",
    )
    return line_alg


@configurable
def make_Lb0ToD0barLcpKm(name="bandq_Lb0ToD0barLcpKm{hash}"):
    Dz = c_to_hadrons.make_Dz()
    Lc = c_to_hadrons.make_LcToPpKmPip()
    K = make_kaons_from_B()

    line_alg = make_lb0_for_cch(
        name=name,
        particles=[Lc, Dz, K],
        descriptor="[ Lambda_b0 -> Lambda_c+ D~0 K-]cc",
    )
    return line_alg


"""
B+ ->Lc+ Lcbar- K+
"""


@configurable
def make_BuToLcLcbarK(name="bandq_BuToLcLcbarK_{hash}"):
    lc = c_to_hadrons.make_LcToPpKmPip()
    kaon = make_kaons_from_B()

    line_alg = make_bu_for_cch(
        name=name,
        particles=[lc, lc, kaon],
        descriptor="[B+ -> Lambda_c+ Lambda_c~- K+]cc",
    )
    return line_alg


"""
B+ ->Lc+ Xic- Pi+
"""


@configurable
def make_BuToLcXicPip(name="bandq_BuToLcXicPip_{hash}"):
    lc = c_to_hadrons.make_LcToPpKmPip()
    pion = make_pions_from_B()
    Xicp = c_to_hadrons.make_XicpToPpKmPip()

    line_alg = make_bu_for_cch(
        name=name,
        particles=[lc, Xicp, pion],
        descriptor="[B+ -> Lambda_c+ Xi_c~- pi+]cc",
    )
    return line_alg
