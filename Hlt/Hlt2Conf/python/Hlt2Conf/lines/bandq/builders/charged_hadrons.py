###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of B&Q charged hadrons taking into account RICHes acceptance and radiator thresholds.
Use track reduced chi square and ghost probability used in Run 2
Separate detached and prompt categories. Prompt to be cheched for rate.
Use DLL so far for PID response.
"""

import Functors as F
from Functors import require_all
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.standard_particles import make_has_rich_long_kaons as make_kaons
from RecoConf.standard_particles import make_has_rich_long_pions as make_pions
from RecoConf.standard_particles import make_has_rich_long_protons as make_protons

from Hlt2Conf.lines.config_pid import nopid_hadrons

####################################
# Charged hadron selections        #
####################################


@configurable
def make_charged_hadrons(
    make_particles=make_pions,
    name="bandq_charged_hadrons_{hash}",
    pt_min=200.0 * MeV,
    p_min=2.5 * GeV,
    p_max=None,
    eta_min=2.0,
    eta_max=5.0,
    mipchi2dvprimary_min=0,
    ghostProb_max=None,
    pid=None,
):
    code = require_all(
        F.PT > pt_min,
        F.P > p_min,
        in_range(eta_min, F.ETA, eta_max),
        F.OWNPVIPCHI2 > mipchi2dvprimary_min,
    )

    if (pid is not None) and (not nopid_hadrons()):
        code &= pid
    if p_max is not None:
        code &= F.P < p_max
    if ghostProb_max is not None:
        code &= F.GHOSTPROB < ghostProb_max

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


####################################
# Detached                         #
####################################


@configurable
def make_detached_pions(
    name="bandq_detached_pions_{hash}",
    mipchi2dvprimary_min=4.0,
    pt_min=200.0 * MeV,
    p_min=2.5 * GeV,
    pid=(F.PID_K < 0.0),
    ghostProb_max=None,
    **decay_arguments,
):
    """
    Return B&Q detached pions.
    """
    return make_charged_hadrons(
        name=name,
        make_particles=make_pions,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pt_min=pt_min,
        p_min=p_min,
        pid=pid,
        ghostProb_max=ghostProb_max,
        **decay_arguments,
    )


@configurable
def make_detached_pions_soft(
    name="bandq_detached_pions_soft_{hash}",
    mipchi2dvprimary_min=4.0,
    pt_min=100.0 * MeV,
    p_min=2.0 * GeV,
    pid=(F.PID_K < 0.0),
    **decay_arguments,
):
    """
    Return B&Q detached pions.
    """
    return make_charged_hadrons(
        name=name,
        make_particles=make_pions,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pt_min=pt_min,
        p_min=p_min,
        pid=pid,
        **decay_arguments,
    )


@configurable
def make_detached_pions_tightpid(
    name="bandq_detached_pions_tightpid_{hash}",
    pid=(F.PID_K < -5),
    pt_min=200.0 * MeV,
    ghostProb_max=None,
):
    """
    Return B&Q detached pions with tight pid cut.
    """

    if nopid_hadrons():
        cut_pid = None
    else:
        cut_pid = F.FILTER(pid)

    return ParticleFilter(
        make_detached_pions(pt_min=pt_min, ghostProb_max=ghostProb_max),
        name=name,
        Cut=cut_pid,
    )


@configurable
def make_detached_kaons(
    name="bandq_detached_kaons_{hash}",
    mipchi2dvprimary_min=4.0,
    pt_min=200.0 * MeV,
    p_min=2.5 * GeV,
    pid=(F.PID_K > 0.0),
    ghostProb_max=None,
    **decay_arguments,
):
    """
    Return B&Q detached kaons.
    """
    return make_charged_hadrons(
        make_particles=make_kaons,
        name=name,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pt_min=pt_min,
        p_min=p_min,
        pid=pid,
        ghostProb_max=ghostProb_max,
        **decay_arguments,
    )


@configurable
def make_detached_kaons_tightpt(
    name="bandq_detached_kaons_tightpt_{hash}",
    mipchi2dvprimary_min=4.0,
    pt_min=500.0 * MeV,
    p_min=2.5 * GeV,
    pid=(F.PID_K > 0.0),
    **decay_arguments,
):
    """
    Return B&Q detached kaons.
    """
    return make_charged_hadrons(
        make_particles=make_kaons,
        name=name,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pt_min=pt_min,
        p_min=p_min,
        pid=pid,
        **decay_arguments,
    )


@configurable
def make_detached_kaons_tightpid(
    name="bandq_detached_kaons_tightpid_{hash}",
    pid=(F.PID_K > 5),
    pt_min=200 * MeV,
    ghostProb_max=None,
):
    """
    Return B&Q detached kaons with tight pid cut.
    """

    if nopid_hadrons():
        cut_pid = None
    else:
        cut_pid = F.FILTER(pid)

    return ParticleFilter(
        make_detached_kaons(pt_min=pt_min, ghostProb_max=ghostProb_max),
        name=name,
        Cut=cut_pid,
    )


@configurable
def make_detached_protons(
    name="bandq_detached_protons_{hash}",
    mipchi2dvprimary_min=4.0,
    pt_min=200.0 * MeV,
    p_min=10.0 * GeV,
    pid=(F.PID_P > 0.0),
    ghostProb_max=None,
    **decay_arguments,
):
    """
    Return B&Q detached protons.
    """
    return make_charged_hadrons(
        make_particles=make_protons,
        name=name,
        p_min=p_min,
        pt_min=pt_min,
        mipchi2dvprimary_min=mipchi2dvprimary_min,
        pid=pid,
        ghostProb_max=ghostProb_max,
        **decay_arguments,
    )


@configurable
def make_detached_protons_tightpid(
    name="bandq_detached_protons_tightpid_{hash}",
    pid=(F.PID_P > 5),
    pt_min=200 * MeV,
    ghostProb_max=None,
):
    """
    Return B&Q detached protons with tight pid cut.
    """

    if nopid_hadrons():
        cut_pid = None
    else:
        cut_pid = F.FILTER(pid)

    return ParticleFilter(
        make_detached_protons(pt_min=pt_min, ghostProb_max=ghostProb_max),
        name=name,
        Cut=cut_pid,
    )


####################################
# Prompt: namely no IPCHI2 cut     #
####################################


@configurable
def make_prompt_pions(
    name="bandq_prompt_pions_{hash}",
    pid=(F.PID_K < 0.0),
    pt_min=200.0 * MeV,
    p_min=2.5 * GeV,
    **decay_arguments,
):
    """
    Return B&Q prompt pions.
    """
    return make_charged_hadrons(
        make_particles=make_pions,
        name=name,
        p_min=p_min,
        pt_min=pt_min,
        pid=pid,
        **decay_arguments,
    )


@configurable
def make_prompt_kaons(
    name="bandq_prompt_kaons_{hash}",
    pid=(F.PID_K > 0.0),
    pt_min=200.0 * MeV,
    p_min=2.5 * GeV,
    **decay_arguments,
):
    """
    Return B&Q prompt kaons.
    """
    return make_charged_hadrons(
        make_particles=make_kaons,
        name=name,
        p_min=p_min,
        pt_min=pt_min,
        pid=pid,
        **decay_arguments,
    )


@configurable
def make_prompt_protons(
    name="bandq_prompt_protons_{hash}",
    p_min=10.0 * GeV,
    pt_min=200.0 * MeV,
    pid=(F.PID_P > 0.0),
    **decay_arguments,
):
    """
    Return B&Q prompt protons.
    """
    return make_charged_hadrons(
        make_particles=make_protons,
        name=name,
        p_min=p_min,
        pt_min=pt_min,
        pid=pid,
        **decay_arguments,
    )


@configurable
def make_detached_phi(
    name="bandq_detached_phi_{hash}",
    am_min=970 * MeV,
    am_max=1070 * MeV,
    m_min=990 * MeV,
    m_max=1050 * MeV,
    minpt=2000 * MeV,
    vtx_chi2pdof_max=9,
    maxDOCAChi2=30,
):
    kaons = make_detached_kaons()

    combination_code = require_all(
        in_range(am_min, F.MASS, am_max), F.SDOCACHI2(1, 2) < maxDOCAChi2
    )

    vertex_code = require_all(
        F.CHI2DOF < vtx_chi2pdof_max, F.PT > minpt, in_range(m_min, F.MASS, m_max)
    )

    return ParticleCombiner(
        name=name,
        Inputs=[kaons, kaons],
        DecayDescriptor="phi(1020) -> K+ K-",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_prompt_phi(
    name="bandq_prompt_phi_{hash}",
    am_min=970 * MeV,
    am_max=1070 * MeV,
    m_min=990 * MeV,
    m_max=1050 * MeV,
    minpt=2000 * MeV,
    vtx_chi2pdof_max=9,
    maxDOCAChi2=30,
):
    kaons = make_prompt_kaons()

    combination_code = require_all(
        in_range(am_min, F.MASS, am_max), F.SDOCACHI2(1, 2) < maxDOCAChi2
    )

    vertex_code = require_all(
        F.CHI2DOF < vtx_chi2pdof_max, F.PT > minpt, in_range(m_min, F.MASS, m_max)
    )

    return ParticleCombiner(
        name=name,
        Inputs=[kaons, kaons],
        DecayDescriptor="phi(1020) -> K+ K-",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
