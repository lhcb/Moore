###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make B&Q combinations for associated productions of A+B
e.g. Jpsi+Jpsi, Jpsi+D, etc.
DPS is abbreviation for double-parton scatterings.
"""

import Functors as F
from GaudiKernel.SystemOfUnits import MeV
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner

from Hlt2Conf.lines.bandq.builders.dimuon_lines import make_upsilon
from Hlt2Conf.lines.bandq.builders.qqbar_to_hadrons import make_prompt_ccbarToPpPm
from Hlt2Conf.lines.charmonium_to_dimuon import make_jpsi, make_psi2s

####################################
# b-hadron makers                  #
####################################


@configurable
def make_dps(particles, descriptor, name="bandq_dps_{hash}"):
    combination_code = F.ALL

    vertex_code = F.ALL

    return ParticleCombiner(
        name=name,
        Inputs=particles,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_doubledimuon_dps_jpsijpsi():
    jpsi = make_jpsi()
    line_alg = make_dps(
        particles=[jpsi, jpsi],
        descriptor="chi_b2(1P) -> J/psi(1S) J/psi(1S)",
        name="bandq_doubledimuon_dps_jpsijpsi_{hash}",
    )
    return line_alg


@configurable
def make_doubledimuon_dps_psi2spsi2s():
    psi2s = make_psi2s()
    line_alg = make_dps(
        particles=[psi2s, psi2s],
        descriptor="chi_b2(1P) -> psi(2S) psi(2S)",
        name="bandq_doubledimuon_dps_psi2spsi2s_{hash}",
    )
    return line_alg


@configurable
def make_doubledimuon_dps_upsilonupsilon():
    upsilon = make_upsilon()
    line_alg = make_dps(
        particles=[upsilon, upsilon],
        descriptor="chi_b2(1P) -> Upsilon(1S) Upsilon(1S)",
        name="bandq_doubledimuon_dps_upsilonupsilon_{hash}",
    )
    return line_alg


@configurable
def make_doubledimuon_dps_jpsipsi2s():
    jpsi = make_jpsi()
    psi2s = make_psi2s()
    line_alg = make_dps(
        particles=[jpsi, psi2s],
        descriptor="chi_b2(1P) -> J/psi(1S) psi(2S)",
        name="bandq_doubledimuon_dps_jpsipsi2s_{hash}",
    )
    return line_alg


@configurable
def make_doubledimuon_dps_jpsiupsilon():
    jpsi = make_jpsi()
    upsilon = make_upsilon()
    line_alg = make_dps(
        particles=[jpsi, upsilon],
        descriptor="chi_b2(1P) -> J/psi(1S) Upsilon(1S)",
        name="bandq_doubledimuon_dps_jpsiupsilon_{hash}",
    )
    return line_alg


@configurable
def make_doubledimuon_dps_psi2supsilon():
    psi2s = make_psi2s()
    upsilon = make_upsilon()
    line_alg = make_dps(
        particles=[psi2s, upsilon],
        descriptor="chi_b2(1P) -> psi(2S) Upsilon(1S)",
        name="bandq_doubledimuon_dps_psi2supsilon_{hash}",
    )
    return line_alg


@configurable
def make_dimuonppbar_dps_jpsietac():
    jpsi = make_jpsi()
    eta_c = make_prompt_ccbarToPpPm(
        am_min=2650 * MeV,
        am_max=4150 * MeV,
        m_min=2700 * MeV,
        m_max=4100 * MeV,
        pt_min_proton=650 * MeV,
        pt_min=2000 * MeV,
        apt_min=1950 * MeV,
    )
    line_alg = make_dps(
        particles=[jpsi, eta_c],
        descriptor="chi_b2(1P) -> J/psi(1S) eta_c(1S)",
        name="bandq_dimuonppbar_dps_jpsietac_{hash}",
    )
    return line_alg
