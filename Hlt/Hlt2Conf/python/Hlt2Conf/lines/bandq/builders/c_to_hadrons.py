###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make B&Q c-hadrons standard decay modes.
"""

from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleContainersMerger

from Hlt2Conf.lines.bandq.builders import c_hadrons, charged_hadrons

###########################################
# D0 standard decay modes:                #
# - D0 -> K- pi+                          #
# - D0 -> pi+ pi-                         #
# - D0 -> K+ K-                           #
# - D0 -> hh combining the 3 above        #
###########################################


@configurable
def make_DzToKmPip(name="bandq_DzToKmPip_{hash}", **decay_arguments):
    kaons = charged_hadrons.make_detached_kaons(mipchi2dvprimary_min=7.0)
    pions = charged_hadrons.make_detached_pions(mipchi2dvprimary_min=7.0)
    line_alg = c_hadrons.make_dz(
        name=name,
        particles=[kaons, pions],
        descriptor="[D0 -> K- pi+]cc",
        **decay_arguments,
    )
    return line_alg


@configurable
def make_DzToPimPip(name="bandq_DzToPimPip_{hash}"):
    pions = charged_hadrons.make_detached_pions(mipchi2dvprimary_min=7.0)
    line_alg = c_hadrons.make_dz(
        name=name, particles=[pions, pions], descriptor="D0 -> pi- pi+"
    )
    return line_alg


@configurable
def make_DzToKmKp(name="bandq_DzToKmKp_{hash}"):
    kaons = charged_hadrons.make_detached_kaons(mipchi2dvprimary_min=7.0)
    line_alg = c_hadrons.make_dz(
        name=name, particles=[kaons, kaons], descriptor="D0 -> K- K+"
    )
    return line_alg


@configurable
def make_DzToKm3Pi(name="bandq_DzToKm3Pi_{hash}"):
    kaons = charged_hadrons.make_detached_kaons(mipchi2dvprimary_min=4.0)
    pions = charged_hadrons.make_detached_pions(mipchi2dvprimary_min=4.0)
    line_alg = c_hadrons.make_dz4h(
        name=name,
        particles=[kaons, pions, pions, pions],
        descriptor="[D0 -> K- pi- pi+ pi+]cc",
    )
    return line_alg


@configurable
def make_DzToKm3Pi_tightip(name="bandq_DzToKm3Pi_tightip_{hash}", **decay_arguments):
    kaons = charged_hadrons.make_detached_kaons(mipchi2dvprimary_min=12.0)
    pions = charged_hadrons.make_detached_pions(mipchi2dvprimary_min=12.0)
    line_alg = c_hadrons.make_dz4h(
        name=name,
        particles=[kaons, pions, pions, pions],
        descriptor="[D0 -> K- pi- pi+ pi+]cc",
        **decay_arguments,
    )
    return line_alg


@configurable
def make_Dz(name="bandq_Dz_{hash}"):
    ## Cabibbo-favored modes
    Dz_KmPip = make_DzToKmPip()
    Dz_Km3Pi = make_DzToKm3Pi()
    return ParticleContainersMerger([Dz_KmPip, Dz_Km3Pi], name=name)


@configurable
def make_Dz_tightip(name="bandq_Dz_tightip_{hash}", **decay_arguments):
    ## Cabibbo-favored modes
    Dz_KmPip = make_DzToKmPip(**decay_arguments)
    Dz_Km3Pi = make_DzToKm3Pi_tightip(**decay_arguments)
    return ParticleContainersMerger([Dz_KmPip, Dz_Km3Pi], name=name)


@configurable
def make_DzToHH(name="bandq_DzToHH_{hash}"):
    Dz_KmPip = make_DzToKmPip()
    Dz_PimPip = make_DzToPimPip()
    Dz_KmKp = make_DzToKmKp()
    return ParticleContainersMerger([Dz_KmPip, Dz_PimPip, Dz_KmKp], name=name)


###########################################
# D+ standard decay modes:                #
# - D+ -> K- pi+ pi+                      #
###########################################


@configurable
def make_DpToKmPipPip(name="bandq_DpToKmPipPip_{hash}", **decay_arguments):
    kaons = charged_hadrons.make_detached_kaons(mipchi2dvprimary_min=7.0)
    pions = charged_hadrons.make_detached_pions(mipchi2dvprimary_min=7.0)
    line_alg = c_hadrons.make_dp(
        name=name,
        particles=[kaons, pions, pions],
        descriptor="[D+ -> K- pi+ pi+]cc",
        **decay_arguments,
    )
    return line_alg


###########################################
# D*(2010)+ standard decay modes:                #
# - D*(2010)+ -> K- pi+ pi+                      #
###########################################


@configurable
def make_DstarpToKmPipPip(name="bandq_DstarpToKmPipPip_{hash}", **decay_arguments):
    kaons = charged_hadrons.make_detached_kaons(mipchi2dvprimary_min=7.0)
    pions = charged_hadrons.make_detached_pions(mipchi2dvprimary_min=7.0)
    line_alg = c_hadrons.make_dstarp(
        name=name,
        particles=[kaons, pions, pions],
        descriptor="[D*(2010)+ -> K- pi+ pi+]cc",
        **decay_arguments,
    )
    return line_alg


###########################################
# Ds+ standard decay modes:                #
# - Ds+ -> K- K+ pi+                      #
###########################################


@configurable
def make_DspToKmKpPip(name="bandq_DspToKmKpPip_{hash}", **decay_arguments):
    kaons = charged_hadrons.make_detached_kaons(mipchi2dvprimary_min=7.0)
    pions = charged_hadrons.make_detached_pions(mipchi2dvprimary_min=7.0)
    line_alg = c_hadrons.make_ds(
        name=name,
        particles=[kaons, kaons, pions],
        descriptor="[D_s+ -> K- K+ pi+]cc",
        **decay_arguments,
    )
    return line_alg


###########################################
# Lc+ standard decay modes:               #
# - Lc+ -> p K- pi+                       #
###########################################


@configurable
def make_LcToPpKmPip(name="bandq_LcToPpKmPip_{hash}", **decay_arguments):
    protons = charged_hadrons.make_detached_protons(mipchi2dvprimary_min=5.0)
    kaons = charged_hadrons.make_detached_kaons(mipchi2dvprimary_min=5.0)
    pions = charged_hadrons.make_detached_pions(mipchi2dvprimary_min=5.0)
    line_alg = c_hadrons.make_lc(
        name=name,
        particles=[protons, kaons, pions],
        descriptor="[Lambda_c+ -> p+ K- pi+]cc",
        **decay_arguments,
    )
    return line_alg


###########################################
# Xic+ standard decay modes:              #
# - Xic+ -> p K- pi+                      #
###########################################


@configurable
def make_XicpToPpKmPip(name="bandq_XicpToPpKmPip_{hash}"):
    protons = charged_hadrons.make_detached_protons(mipchi2dvprimary_min=5.0)
    kaons = charged_hadrons.make_detached_kaons(mipchi2dvprimary_min=5.0)
    pions = charged_hadrons.make_detached_pions(mipchi2dvprimary_min=5.0)
    line_alg = c_hadrons.make_xic(
        name=name,
        particles=[protons, kaons, pions],
        descriptor="[Xi_c+ -> p+ K- pi+]cc",
    )
    return line_alg


###########################################
# Xic0 standard decay modes:              #
# - Xic0 -> p K- K- pi+                   #
###########################################


@configurable
def make_XiczToPpKmKmPip(name="bandq_XiczToPpKmKmPip_{hash}"):
    protons = charged_hadrons.make_detached_protons(mipchi2dvprimary_min=5.0)
    kaons = charged_hadrons.make_detached_kaons(mipchi2dvprimary_min=5.0)
    pions = charged_hadrons.make_detached_pions(mipchi2dvprimary_min=5.0)
    line_alg = c_hadrons.make_xic(
        name=name,
        particles=[protons, kaons, kaons, pions],
        descriptor="[Xi_c0 -> p+ K- K- pi+]cc",
    )
    return line_alg


###########################################
# Omegac0 standard decay modes:           #
# - Omegac0 -> p K- K- pi+                #
###########################################


@configurable
def make_OmegaczToPpKmKmPip(name="bandq_OmegaczToPpKmKmPip_{hash}"):
    protons = charged_hadrons.make_detached_protons(mipchi2dvprimary_min=5.0)
    kaons = charged_hadrons.make_detached_kaons(mipchi2dvprimary_min=5.0)
    pions = charged_hadrons.make_detached_pions(mipchi2dvprimary_min=5.0)
    line_alg = c_hadrons.make_omegac(
        name=name,
        particles=[protons, kaons, kaons, pions],
        descriptor="[Omega_c0 -> p+ K- K- pi+]cc",
    )
    return line_alg


# should be deprecated ?
@configurable
def make_TightOmegaczToPpKmKmPip(name="bandq_TightOmegaczToPpKmKmPip_{hash}"):
    protons = charged_hadrons.make_detached_protons(mipchi2dvprimary_min=4.0)
    kaons = charged_hadrons.make_detached_kaons(mipchi2dvprimary_min=4.0)
    pions = charged_hadrons.make_detached_pions(mipchi2dvprimary_min=4.0)
    line_alg = c_hadrons.make_tightomegac(
        name=name,
        particles=[protons, kaons, kaons, pions],
        descriptor="[Omega_c0 -> p+ K- K- pi+]cc",
    )
    return line_alg


############################################################################
# Tight version of D-selections,
# to be mainly used for B->Dh(hh) and D->DmuX builders,
# thus cuts on PT are tight as well as on
# lifetime wrt to PV (can afford because of longer B-lifetime)
############################################################################


@configurable
def make_detached_pions_for_tight(name="bandq_detached_pions_for_tight"):
    return charged_hadrons.make_detached_pions(
        mipchi2dvprimary_min=4.0, pt_min=250 * MeV, p_min=3 * GeV
    )


@configurable
def make_detached_kaons_for_tight(name="bandq_detached_pions_for_tight"):
    return charged_hadrons.make_detached_kaons(
        mipchi2dvprimary_min=4.0, pt_min=350 * MeV, p_min=5 * GeV
    )


@configurable
def make_detached_protons_for_tight(name="bandq_detached_protons_for_tight"):
    return charged_hadrons.make_detached_protons(
        mipchi2dvprimary_min=4.0, pt_min=350 * MeV, p_min=10 * GeV
    )


tight_charm_ownpvltime_min = 0.4 * picosecond
tight_charm_ownpvvdchi2_min = 25
tight_charm_ownpvdira_min = (
    0.98  # [AVOID DIRECTION BIAS], 0.98 ok even for D from Tbb->B->D
)
tight_charm_avmips_min = 16


@configurable
def make_tight_DzToKmPip(name="bandq_TightDzToKmPip_{hash}", dm_tight=False):
    kaons = make_detached_kaons_for_tight()
    pions = make_detached_pions_for_tight()

    m_min = 1805 * MeV
    m_max = 1925 * MeV
    if dm_tight:
        m_min = 1835 * MeV
        m_max = 1895 * MeV

    line_alg = c_hadrons.make_dz(
        name=name,
        particles=[kaons, pions],
        descriptor="[D0 -> K- pi+]cc",
        am_min=1790 * MeV,
        am_max=1940 * MeV,
        m_min=m_min,
        m_max=m_max,
        ownpvltime_min=tight_charm_ownpvltime_min,
        ownpvvdchi2_min=tight_charm_ownpvvdchi2_min,
        ownpvdira_min=tight_charm_ownpvdira_min,
        average_mips_min=tight_charm_avmips_min,
    )
    return line_alg


@configurable
def make_tight_DzToKm3Pi(name="bandq_TightDzToKm3Pi_{hash}", dm_tight=False):
    kaons = make_detached_kaons_for_tight()
    pions = make_detached_pions_for_tight()

    m_min = 1825 * MeV
    m_max = 1905 * MeV
    if dm_tight:
        m_min = 1840 * MeV
        m_max = 1890 * MeV

    line_alg = c_hadrons.make_dz4h(
        name=name,
        particles=[kaons, pions, pions, pions],
        descriptor="[D0 -> K- pi- pi+ pi+]cc",
        am_min=1810 * MeV,
        am_max=1920 * MeV,
        m_min=m_min,
        m_max=m_max,
        ownpvltime_min=tight_charm_ownpvltime_min,
        ownpvvdchi2_min=tight_charm_ownpvvdchi2_min,
        ownpvdira_min=tight_charm_ownpvdira_min,
        average_mips_min=tight_charm_avmips_min,
    )
    return line_alg


@configurable
def make_tight_Dz(name="bandq_TightDz_{hash}", dm_tight=False):
    ## Cabibbo-favored modes
    Dz_KmPip = make_tight_DzToKmPip(dm_tight=dm_tight)
    Dz_Km3Pi = make_tight_DzToKm3Pi(dm_tight=dm_tight)
    return ParticleContainersMerger([Dz_KmPip, Dz_Km3Pi], name=name)


@configurable
def make_tight_DpToKmPipPip(name="bandq_TightDpToKmPipPip_{hash}", dm_tight=False):
    kaons = make_detached_kaons_for_tight()
    pions = make_detached_pions_for_tight()

    m_min = 1820 * MeV
    m_max = 1920 * MeV
    if dm_tight:
        m_min = 1840 * MeV
        m_max = 1900 * MeV

    line_alg = c_hadrons.make_dp(
        name=name,
        particles=[kaons, pions, pions],
        descriptor="[D+ -> K- pi+ pi+]cc",
        am_min=1805 * MeV,
        am_max=1935 * MeV,
        m_min=m_min,
        m_max=m_max,
        ownpvltime_min=tight_charm_ownpvltime_min,
        ownpvvdchi2_min=tight_charm_ownpvvdchi2_min,
        ownpvdira_min=tight_charm_ownpvdira_min,
        average_mips_min=tight_charm_avmips_min,
    )
    return line_alg


@configurable
def make_tight_DspToKmKpPip(name="bandq_TightDspToKmKpPip_{hash}", dm_tight=False):
    kaons = make_detached_kaons_for_tight()
    pions = make_detached_pions_for_tight()

    m_min = 1920 * MeV
    m_max = 2020 * MeV
    if dm_tight:
        m_min = 1940 * MeV
        m_max = 2000 * MeV

    line_alg = c_hadrons.make_ds(
        name=name,
        particles=[kaons, kaons, pions],
        descriptor="[D_s+ -> K- K+ pi+]cc",
        m_min=m_min,
        m_max=m_max,
        ownpvltime_min=tight_charm_ownpvltime_min,
        ownpvvdchi2_min=tight_charm_ownpvvdchi2_min,
        ownpvdira_min=tight_charm_ownpvdira_min,
        average_mips_min=tight_charm_avmips_min,
    )
    return line_alg


@configurable
def make_tight_LcToPpKmPip(name="bandq_TightLcToPpKmPip_{hash}", dm_tight=False):
    protons = make_detached_protons_for_tight()
    kaons = make_detached_kaons_for_tight()
    pions = make_detached_pions_for_tight()

    m_min = 2235 * MeV
    m_max = 2335 * MeV
    if dm_tight:
        m_min = 2255 * MeV
        m_max = 2315 * MeV

    line_alg = c_hadrons.make_lc(
        name=name,
        particles=[protons, kaons, pions],
        descriptor="[Lambda_c+ -> p+ K- pi+]cc",
        asumpt_min=2.0 * GeV,
        m_min=m_min,
        m_max=m_max,
        ownpvltime_min=tight_charm_ownpvltime_min,
        ownpvvdchi2_min=tight_charm_ownpvvdchi2_min,
        ownpvdira_min=tight_charm_ownpvdira_min,
        average_mips_min=tight_charm_avmips_min,
    )
    return line_alg


@configurable
def make_tight_XiczToPpKmKmPip(
    name="bandq_TightXiczToPpKmKmPip_{hash}", dm_tight=False
):
    protons = make_detached_protons_for_tight()
    kaons = make_detached_kaons_for_tight()
    pions = make_detached_pions_for_tight()

    m_min = 2435 * MeV
    m_max = 2505 * MeV
    if dm_tight:
        m_min = 2445 * MeV
        m_max = 2495 * MeV

    line_alg = c_hadrons.make_xic(
        name=name,
        particles=[protons, kaons, kaons, pions],
        descriptor="[Xi_c0 -> p+ K- K- pi+]cc",
        asumpt_min=2.2 * GeV,
        m_min=m_min,
        m_max=m_max,
        ownpvltime_min=tight_charm_ownpvltime_min,
        ownpvvdchi2_min=tight_charm_ownpvvdchi2_min,
        ownpvdira_min=tight_charm_ownpvdira_min,
        average_mips_min=tight_charm_avmips_min,
    )
    return line_alg


@configurable
def make_tight_XicpToPpKmPip(name="bandq_TightXicpToPpKmPip_{hash}", dm_tight=False):
    protons = make_detached_protons_for_tight()
    kaons = make_detached_kaons_for_tight()
    pions = make_detached_pions_for_tight()

    m_min = 2410 * MeV
    m_max = 2520 * MeV
    if dm_tight:
        m_min = 2435 * MeV
        m_max = 2500 * MeV

    line_alg = c_hadrons.make_xic(
        name=name,
        particles=[protons, kaons, pions],
        descriptor="[Xi_c+ -> p+ K- pi+]cc",
        asumpt_min=2.2 * GeV,
        m_min=m_min,
        m_max=m_max,
        ownpvltime_min=tight_charm_ownpvltime_min,
        ownpvvdchi2_min=tight_charm_ownpvvdchi2_min,
        ownpvdira_min=tight_charm_ownpvdira_min,
        average_mips_min=tight_charm_avmips_min,
    )
    return line_alg


@configurable
def make_tight_OmegaczToPpKmKmPip(
    name="bandq_TightOmegaczToPpKmKmPip_{hash}", dm_tight=False
):
    protons = make_detached_protons_for_tight()
    kaons = make_detached_kaons_for_tight()
    pions = make_detached_pions_for_tight()

    m_min = 2660 * MeV
    m_max = 2730 * MeV
    if dm_tight:
        m_min = 2670 * MeV
        m_max = 2720 * MeV

    line_alg = c_hadrons.make_omegac(
        name=name,
        particles=[protons, kaons, kaons, pions],
        descriptor="[Omega_c0 -> p+ K- K- pi+]cc",
        asumpt_min=2.3 * GeV,
        m_min=m_min,
        m_max=m_max,
        ownpvltime_min=tight_charm_ownpvltime_min,
        ownpvvdchi2_min=tight_charm_ownpvvdchi2_min,
        ownpvdira_min=tight_charm_ownpvdira_min,
        average_mips_min=tight_charm_avmips_min,
    )
    return line_alg


###########################################
# Excited charm -> charm + hadrons        #
###########################################


@configurable
def make_Ds1ToD0Kp_D0ToKmPip(
    name="bandq_Ds1ToD0Kp_D0ToKmPip_{hash}",
    am_max=3083.5 * MeV,
    m_max=2938.5 * MeV,  # 580 MeV above DK threshold
):
    d0 = make_DzToKmPip()
    kaons = charged_hadrons.make_detached_kaons_tightpid()
    line_alg = c_hadrons.make_dz(
        name=name,
        particles=[d0, kaons],
        descriptor="[D_s1(2536)+ -> D0 K+]cc",
        am_min=0.0 * MeV,  # start from D0K mass threshold
        m_min=0.0 * MeV,
        am_max=am_max,
        m_max=m_max,
        ownpvltime_min=-10.0 * picosecond,  # no default cut on excited Ds1 lifetime
    )
    return line_alg


@configurable
def make_Ds1ToD0Km_D0ToKmPip(
    name="bandq_Ds1ToD0Km_D0ToKmPip_{hash}",
    am_max=3083.5 * MeV,
    m_max=2938.5 * MeV,  # 580 MeV above DK threshold
):
    d0 = make_DzToKmPip()
    kaons = charged_hadrons.make_detached_kaons_tightpid()
    line_alg = c_hadrons.make_dz(
        name=name,
        particles=[d0, kaons],
        descriptor="[D_s1(2536)- -> D0 K-]cc",
        am_min=0.0 * MeV,
        m_min=0.0 * MeV,
        am_max=am_max,
        m_max=m_max,
        ownpvltime_min=-10.0 * picosecond,  # no default cut on excited Ds1 lifetime
    )
    return line_alg


################################################
# Build a single container with all the decays #
################################################


@configurable
def make_charm_to_hadrons(name="bandq_charmToHadrons_{hash}"):
    DzToHH = make_DzToHH()
    DpToKmPipPip = make_DpToKmPipPip()
    DsToKmKpPip = make_DspToKmKpPip()
    LcToPpKmPip = make_LcToPpKmPip()
    XicpToPpKmPip = make_XicpToPpKmPip()
    XiczToPpKmKmPip = make_XiczToPpKmKmPip()
    OmegaczToPpKmKmPip = make_OmegaczToPpKmKmPip()
    return ParticleContainersMerger(
        [
            DzToHH,
            DpToKmPipPip,
            DsToKmKpPip,
            LcToPpKmPip,
            XicpToPpKmPip,
            XiczToPpKmKmPip,
            OmegaczToPpKmKmPip,
        ],
        name=name,
    )


@configurable
def make_tight_charm_to_hadrons(name="bandq_TightCharmToHadrons_{hash}"):
    Dz_KmPip = make_tight_DzToKmPip()
    Dz_Km3Pi = make_tight_DzToKm3Pi()
    DpToKmPipPip = make_tight_DpToKmPipPip()
    DsToKmKpPip = make_tight_DspToKmKpPip()
    LcToPpKmPip = make_tight_LcToPpKmPip()
    XicpToPpKmPip = make_tight_XicpToPpKmPip()
    XiczToPpKmKmPip = make_tight_XiczToPpKmKmPip()
    OmegaczToPpKmKmPip = make_tight_OmegaczToPpKmKmPip()
    return ParticleContainersMerger(
        [
            Dz_KmPip,
            Dz_Km3Pi,
            DpToKmPipPip,
            DsToKmKpPip,
            LcToPpKmPip,
            XicpToPpKmPip,
            XiczToPpKmKmPip,
            OmegaczToPpKmKmPip,
        ],
        name=name,
    )
