###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from PyConf import configurable
from RecoConf.standard_particles import make_LambdaDD, make_LambdaLL

import Hlt2Conf.lines.bandq.builders.c_to_hadrons as c_to_hadrons
from Hlt2Conf.lines.bandq.builders.b_for_spectroscopy import make_lb2cx_for_spectroscopy
from Hlt2Conf.lines.bandq.builders.charged_hadrons import make_detached_pions

make_Lc = c_to_hadrons.make_LcToPpKmPip


@configurable
def make_LbToLcPimLLbarLLLL(process):
    pion = make_detached_pions()
    cbaryon = make_Lc()
    Lambda = make_LambdaLL()
    return make_lb2cx_for_spectroscopy(
        particles=[cbaryon, pion, Lambda, Lambda],
        descriptor="[Lambda_b0 -> Lambda_c+ pi- Lambda0 Lambda~0]cc",
    )


@configurable
def make_LbToLcPimLLbarLLDD(process):
    pion = make_detached_pions()
    cbaryon = make_Lc()
    Lambda = make_LambdaLL()
    _Lambda = make_LambdaDD()
    return make_lb2cx_for_spectroscopy(
        particles=[cbaryon, pion, Lambda, _Lambda],
        descriptor="[Lambda_b0 -> Lambda_c+ pi- Lambda0 Lambda~0]cc",
    )


@configurable
def make_LbToLcPimLLbarDDDD(process):
    pion = make_detached_pions()
    cbaryon = make_Lc()
    _Lambda = make_LambdaDD()
    return make_lb2cx_for_spectroscopy(
        particles=[cbaryon, pion, _Lambda, _Lambda],
        descriptor="[Lambda_b0 -> Lambda_c+ pi- Lambda0 Lambda~0]cc",
    )
