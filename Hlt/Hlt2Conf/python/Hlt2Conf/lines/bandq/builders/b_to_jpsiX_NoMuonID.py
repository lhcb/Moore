###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
A standalone builder for exclusive B+->Jpsi K+, B0->Jpsi K+ pi-, Bs->Jpsi K+ K- decays.
Do not cut on MUON-station-based MuonID info.
Use the line to perform data-driven check of MuonID performance.
Tight kinematic cuts to make rate under control.
"""

import Functors as F
from Functors import require_all
from Functors.math import in_range, log
from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import make_long_muons

from Hlt2Conf.lines.bandq.builders.charged_hadrons import (
    make_detached_kaons,
    make_detached_pions,
)


@configurable
def _bp_jpsik_BDT_functor(pvs, mva_name):
    bp_jpsik_mTag_vars = {
        "BPT": F.PT,
        "logmumT": log(F.CHILD(1, F.CHILD(2, F.PT))),
        "JpsiPT": F.CHILD(1, F.PT),
        "hPT": F.CHILD(2, F.PT),
        "JpsiCHI2": F.CHILD(1, F.CHI2DOF),
        "logMuBPVIPCHI2": log(F.CHILD(1, F.CHILD(2, F.OWNPVIPCHI2))),
        "HBPVIPCHI2": F.CHILD(2, F.OWNPVIPCHI2),
        "logMuCHI2DOF": log(F.CHILD(1, F.CHILD(2, F.CHI2DOF))),
        "logHCHI2DOF": log(F.CHILD(2, F.CHI2DOF)),
        "Bip": F.OWNPVIPCHI2,
        "Bfd": F.OWNPVFDCHI2,
        "Bdof": F.CHI2DOF,
        "BDira": F.OWNPVDIRA,
    }
    bp_jpsik_pTag_vars = {
        "BPT": F.PT,
        "logMUPT": log(F.CHILD(1, F.CHILD(1, F.PT))),
        "JpsiPT": F.CHILD(1, F.PT),
        "hPT": F.CHILD(2, F.PT),
        "JpsiCHI2": F.CHILD(1, F.CHI2DOF),
        "logMuBPVIPCHI2": log(F.CHILD(1, F.CHILD(1, F.OWNPVIPCHI2))),
        "HBPVIPCHI2": F.CHILD(2, F.OWNPVIPCHI2),
        "logMuCHI2DOF": log(F.CHILD(1, F.CHILD(1, F.CHI2DOF))),
        "logHCHI2DOF": log(F.CHILD(2, F.CHI2DOF)),
        "Bip": F.OWNPVIPCHI2,
        "Bfd": F.OWNPVFDCHI2,
        "Bdof": F.CHI2DOF,
        "BDira": F.OWNPVDIRA,
    }

    bdt_vars = {
        "bp_jpsik_mTag": bp_jpsik_mTag_vars,
        "bp_jpsik_pTag": bp_jpsik_pTag_vars,
    }

    xml_files = {
        "bp_jpsik_mTag": "paramfile://data/Hlt2BandQ_Bp2JpsiKp_mTag_MVA_NoMuonID.xml",
        "bp_jpsik_pTag": "paramfile://data/Hlt2BandQ_Bp2JpsiKp_pTag_MVA_NoMuonID.xml",
    }

    return F.MVA(
        MVAType="TMVA",
        Config={
            "XMLFile": xml_files[mva_name],
            "Name": "BDT",
        },
        Inputs=bdt_vars[mva_name],
    )


@configurable
def make_NoMuonID_muons(
    make_particles=make_long_muons,
    name="bandq_NoMuonID_muons_{hash}",
    pt_min=500.0 * MeV,
    p_min=4 * GeV,
    p_max=200.0 * GeV,
    eta_min=2.0,
    eta_max=5.0,
    mipchi2dvprimary_min=9,
    # pid=None):
    pid=require_all(F.RICH_DLL_MU > -10, F.RICH_DLL_MU - F.RICH_DLL_K > -5.0),
):
    code = require_all(
        F.PT > pt_min,
        in_range(p_min, F.P, p_max),
        in_range(eta_min, F.ETA, eta_max),
        F.OWNPVIPCHI2 > mipchi2dvprimary_min,
    )

    if pid is not None:
        code &= pid

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_NoMuonID_NoRICHID_muons(
    make_particles=make_long_muons,
    name="bandq_NoMuonID_muons_{hash}",
    pt_min=500.0 * MeV,
    p_min=4 * GeV,
    p_max=200.0 * GeV,
    eta_min=2.0,
    eta_max=5.0,
    mipchi2dvprimary_min=9,
    pid=None,
):
    code = require_all(
        F.PT > pt_min,
        in_range(p_min, F.P, p_max),
        in_range(eta_min, F.ETA, eta_max),
        F.OWNPVIPCHI2 > mipchi2dvprimary_min,
    )

    if pid is not None:
        code &= pid

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_detached_kaons_forB2JpsiX_NoMuonID(
    make_particles=make_detached_kaons,
    name="bandq_detached_kaons_forB2JpsiX_NoMuonID_{hash}",
    pt_min=300.0 * MeV,
    p_min=4 * GeV,
    p_max=200.0 * GeV,
    eta_min=2.0,
    eta_max=5.0,
    mipchi2dvprimary_min=9,
    pid=(F.PID_K > 5.0),
):
    code = require_all(
        F.PT > pt_min,
        in_range(p_min, F.P, p_max),
        in_range(eta_min, F.ETA, eta_max),
        F.OWNPVIPCHI2 > mipchi2dvprimary_min,
    )

    if pid is not None:
        code &= pid

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_detached_loose_kaons_forB2JpsiX_NoMuonID(
    make_particles=make_detached_kaons,
    name="bandq_detached_loose_kaons_forB2JpsiX_NoMuonID_{hash}",
    pt_min=300.0 * MeV,
    p_min=4 * GeV,
    p_max=200.0 * GeV,
    eta_min=2.0,
    eta_max=5.0,
    mipchi2dvprimary_min=9,
    pid=(F.PID_K > 0.0),
):
    code = require_all(
        F.PT > pt_min,
        in_range(p_min, F.P, p_max),
        in_range(eta_min, F.ETA, eta_max),
        F.OWNPVIPCHI2 > mipchi2dvprimary_min,
    )

    if pid is not None:
        code &= pid

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_detached_pions_forB2JpsiX_NoMuonID(
    make_particles=make_detached_pions,
    name="bandq_detached_pions_forB2JpsiX_NoMuonID_{hash}",
    pt_min=200.0 * MeV,
    p_min=3 * GeV,
    p_max=200.0 * GeV,
    eta_min=2.0,
    eta_max=5.0,
    mipchi2dvprimary_min=9,
    pid=(F.PID_K < -5.0),
):
    code = require_all(
        F.PT > pt_min,
        in_range(p_min, F.P, p_max),
        in_range(eta_min, F.ETA, eta_max),
        F.OWNPVIPCHI2 > mipchi2dvprimary_min,
    )

    if pid is not None:
        code &= pid

    return ParticleFilter(make_particles(), name=name, Cut=F.FILTER(code))


@configurable
def make_detachedKPi_forB2JpsiX_NoMuonID(
    kaons=make_detached_kaons_forB2JpsiX_NoMuonID,
    pions=make_detached_pions_forB2JpsiX_NoMuonID,
    name="bandq_detachedKPi_forB2JpsiX_NoMuonID_{hash}",
    descriptor="[K*(892)0 -> K+ pi-]cc",
    am_max=2500 * MeV,
    maxdocachi2=20.0,
    m_max=2450.0 * MeV,
    ownpvdls_min=3,
):
    combination_code = F.require_all(F.MASS < am_max, F.MAXSDOCACHI2CUT(maxdocachi2))

    vertex_code = F.require_all(F.MASS < m_max, F.OWNPVDLS > ownpvdls_min)

    return ParticleCombiner(
        name=name,
        Inputs=[kaons(), pions()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_detachedKst_forB2JpsiX_NoMuonID(
    KPi_comb=make_detachedKPi_forB2JpsiX_NoMuonID,
    name="bandq_detachedKst_forB2JpsiX_NoMuonID_{hash}",
    m_max=1100.0 * MeV,
    PT_min=600.0 * MeV,
):
    code = F.require_all(F.MASS < m_max, F.PT > PT_min)

    return ParticleFilter(KPi_comb(), name=name, Cut=F.FILTER(code))


@configurable
def make_detachedKK_forB2JpsiX_NoMuonID(
    kaons=make_detached_kaons_forB2JpsiX_NoMuonID,
    name="bandq_detachedKK_forB2JpsiX_NoMuonID_{hash}",
    descriptor="phi(1020) -> K+ K-",
    am_max=2500 * MeV,
    maxdocachi2=20.0,
    m_max=2450.0 * MeV,
    ownpvdls_min=3,
):
    combination_code = F.require_all(F.MASS < am_max, F.MAXSDOCACHI2CUT(maxdocachi2))

    vertex_code = F.require_all(F.MASS < m_max, F.OWNPVDLS > ownpvdls_min)

    return ParticleCombiner(
        name=name,
        Inputs=[kaons(), kaons()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_detachedPhi_forB2JpsiX_NoMuonID(
    KK_comb=make_detachedKK_forB2JpsiX_NoMuonID,
    name="bandq_detachedPhi_forB2JpsiX_NoMuonID_{hash}",
    m_max=1100.0 * MeV,
    PT_min=400.0 * MeV,
):
    code = F.require_all(F.MASS < m_max, F.PT > PT_min)

    return ParticleFilter(KK_comb(), name=name, Cut=F.FILTER(code))


@configurable
def make_Jpsi_pTag_NoMuonID(
    muonp=make_NoMuonID_muons,
    muonm=make_NoMuonID_NoRICHID_muons,
    name="bandq_Jpsi_pTag_NoMuonID_{hash}",
    descriptor="J/psi(1S) -> mu+ mu-",
    am_min=2836.0 * MeV,
    am_max=3356.0 * MeV,
    apt_min=600 * MeV,
    maxdocachi2=20.0,
    m_min=2846.0 * MeV,
    m_max=3346.0 * MeV,
    pt_min=1200 * MeV,
    ownpvdls_min=3,
):
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.MAXSDOCACHI2CUT(maxdocachi2),
        (F.PT > apt_min),
    )

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), (F.PT > pt_min), F.OWNPVDLS > ownpvdls_min
    )

    return ParticleCombiner(
        name=name,
        Inputs=[muonp(), muonm()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_Jpsi_mTag_NoMuonID(
    muonm=make_NoMuonID_muons,
    muonp=make_NoMuonID_NoRICHID_muons,
    name="bandq_Jpsi_mTag_NoMuonID_{hash}",
    descriptor="J/psi(1S) -> mu+ mu-",
    am_min=2836.0 * MeV,
    am_max=3356.0 * MeV,
    apt_min=600 * MeV,
    maxdocachi2=20.0,
    m_min=2846.0 * MeV,
    m_max=3346.0 * MeV,
    pt_min=1200 * MeV,
    ownpvdls_min=3,
):
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.MAXSDOCACHI2CUT(maxdocachi2),
        (F.PT > apt_min),
    )

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), (F.PT > pt_min), F.OWNPVDLS > ownpvdls_min
    )

    return ParticleCombiner(
        name=name,
        Inputs=[muonp(), muonm()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_Jpsi_NoRICHTag_NoMuonID(
    muonm=make_NoMuonID_NoRICHID_muons,
    muonp=make_NoMuonID_NoRICHID_muons,
    name="bandq_Jpsi_NoRICHTag_NoMuonID_{hash}",
    descriptor="J/psi(1S) -> mu+ mu-",
    am_min=2836.0 * MeV,
    am_max=3356.0 * MeV,
    apt_min=600 * MeV,
    maxdocachi2=20.0,
    m_min=2846.0 * MeV,
    m_max=3346.0 * MeV,
    pt_min=1200 * MeV,
    ownpvdls_min=3,
):
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.MAXSDOCACHI2CUT(maxdocachi2),
        (F.PT > apt_min),
    )

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max), (F.PT > pt_min), F.OWNPVDLS > ownpvdls_min
    )

    return ParticleCombiner(
        name=name,
        Inputs=[muonp(), muonm()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_Bp2JpsiKp_mTag_NoMuonID(
    Jpsi=make_Jpsi_mTag_NoMuonID,
    kaons=make_detached_kaons_forB2JpsiX_NoMuonID,
    name="bandq_Bp2JpsiKp_mTag_NoMuonID_{hash}",
    descriptor="[B+ -> J/psi(1S) K+]cc",
    am_min=4740.0 * MeV,
    am_max=5760.0 * MeV,
    apt_min=1200 * MeV,
    m_min=4750.0 * MeV,
    m_max=5750.0 * MeV,
    pt_min=1400 * MeV,
    vtx_chi2pdof_max=16,
    delta_m_min=2050 * MeV,
    delta_m_max=2310 * MeV,
    ownpv_dira_min=0.9995,
    ownpvltime_min=0.19 * picosecond,
):
    combination_code = F.require_all(in_range(am_min, F.MASS, am_max), (F.PT > apt_min))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < vtx_chi2pdof_max,
        (F.PT > pt_min),
        F.OWNPVDIRA > ownpv_dira_min,
        in_range(delta_m_min, F.MASS - F.CHILD(1, F.MASS), delta_m_max),
        F.OWNPVLTIME > ownpvltime_min,
    )

    return ParticleCombiner(
        name=name,
        Inputs=[Jpsi(), kaons()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_Bp2JpsiKp_pTag_NoMuonID(
    Jpsi=make_Jpsi_pTag_NoMuonID,
    kaons=make_detached_kaons_forB2JpsiX_NoMuonID,
    name="bandq_Bp2JpsiKp_pTag_NoMuonID_{hash}",
    descriptor="[B+ -> J/psi(1S) K+]cc",
    am_min=4740.0 * MeV,
    am_max=5760.0 * MeV,
    apt_min=1200 * MeV,
    m_min=4750.0 * MeV,
    m_max=5750.0 * MeV,
    pt_min=1400 * MeV,
    vtx_chi2pdof_max=16,
    delta_m_min=2050 * MeV,
    delta_m_max=2310 * MeV,
    ownpv_dira_min=0.9995,
    ownpvltime_min=0.19 * picosecond,
):
    combination_code = F.require_all(in_range(am_min, F.MASS, am_max), (F.PT > apt_min))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < vtx_chi2pdof_max,
        (F.PT > pt_min),
        F.OWNPVDIRA > ownpv_dira_min,
        in_range(delta_m_min, F.MASS - F.CHILD(1, F.MASS), delta_m_max),
        F.OWNPVLTIME > ownpvltime_min,
    )

    return ParticleCombiner(
        name=name,
        Inputs=[Jpsi(), kaons()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_Bp2JpsiKp_mTag_MVA_NoMuonID(
    Jpsi=make_Jpsi_mTag_NoMuonID,
    kaons=make_detached_loose_kaons_forB2JpsiX_NoMuonID,
    name="bandq_Bp2JpsiKp_mTag_MVA_NoMuonID_{hash}",
    descriptor="[B+ -> J/psi(1S) K+]cc",
    am_min=4740.0 * MeV,
    am_max=5760.0 * MeV,
    apt_min=1200 * MeV,
    m_min=4750.0 * MeV,
    m_max=5750.0 * MeV,
    pt_min=1400 * MeV,
    vtx_chi2pdof_max=16,
    delta_m_min=2050 * MeV,
    delta_m_max=2310 * MeV,
    ownpv_dira_min=0.9995,
    bdt_cut=-0.2,
    ownpvltime_min=0.19 * picosecond,
):
    pvs = make_pvs()

    combination_code = F.require_all(in_range(am_min, F.MASS, am_max), (F.PT > apt_min))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < vtx_chi2pdof_max,
        (F.PT > pt_min),
        F.OWNPVDIRA > ownpv_dira_min,
        in_range(delta_m_min, F.MASS - F.CHILD(1, F.MASS), delta_m_max),
        F.OWNPVLTIME > ownpvltime_min,
        _bp_jpsik_BDT_functor(pvs, "bp_jpsik_mTag") > bdt_cut,
    )

    return ParticleCombiner(
        name=name,
        Inputs=[Jpsi(), kaons()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_Bp2JpsiKp_pTag_MVA_NoMuonID(
    Jpsi=make_Jpsi_pTag_NoMuonID,
    kaons=make_detached_loose_kaons_forB2JpsiX_NoMuonID,
    name="bandq_Bp2JpsiKp_pTag_MVA_NoMuonID_{hash}",
    descriptor="[B+ -> J/psi(1S) K+]cc",
    am_min=4740.0 * MeV,
    am_max=5760.0 * MeV,
    apt_min=1200 * MeV,
    m_min=4750.0 * MeV,
    m_max=5750.0 * MeV,
    pt_min=1400 * MeV,
    vtx_chi2pdof_max=16,
    delta_m_min=2050 * MeV,
    delta_m_max=2310 * MeV,
    ownpv_dira_min=0.9995,
    bdt_cut=-0.2,
    ownpvltime_min=0.19 * picosecond,
):
    pvs = make_pvs()

    combination_code = F.require_all(in_range(am_min, F.MASS, am_max), (F.PT > apt_min))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < vtx_chi2pdof_max,
        (F.PT > pt_min),
        F.OWNPVDIRA > ownpv_dira_min,
        in_range(delta_m_min, F.MASS - F.CHILD(1, F.MASS), delta_m_max),
        F.OWNPVLTIME > ownpvltime_min,
        _bp_jpsik_BDT_functor(pvs, "bp_jpsik_pTag") > bdt_cut,
    )

    return ParticleCombiner(
        name=name,
        Inputs=[Jpsi(), kaons()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_Bz2JpsiKst_pTag_NoMuonID(
    Jpsi=make_Jpsi_pTag_NoMuonID,
    KPi_comb=make_detachedKst_forB2JpsiX_NoMuonID,
    name="bandq_Bz2JpsiKst_pTag_NoMuonID_{hash}",
    descriptor="[B0 -> K*(892)0 J/psi(1S)]cc",
    am_min=4740.0 * MeV,
    am_max=5760.0 * MeV,
    apt_min=1200 * MeV,
    m_min=4750.0 * MeV,
    m_max=5750.0 * MeV,
    pt_min=1400 * MeV,
    vtx_chi2pdof_max=16,
    delta_m_min=2050 * MeV,
    delta_m_max=2310 * MeV,
    ownpv_dira_min=0.9995,
    ownpvltime_min=0.19 * picosecond,
):
    combination_code = F.require_all(in_range(am_min, F.MASS, am_max), (F.PT > apt_min))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < vtx_chi2pdof_max,
        (F.PT > pt_min),
        in_range(delta_m_min, F.MASS - F.CHILD(2, F.MASS), delta_m_max),
        F.OWNPVDIRA > ownpv_dira_min,
        F.OWNPVLTIME > ownpvltime_min,
    )

    return ParticleCombiner(
        name=name,
        Inputs=[KPi_comb(), Jpsi()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_Bz2JpsiKst_mTag_NoMuonID(
    Jpsi=make_Jpsi_mTag_NoMuonID,
    KPi_comb=make_detachedKst_forB2JpsiX_NoMuonID,
    name="bandq_Bz2JpsiKst_mTag_NoMuonID_{hash}",
    descriptor="[B0 -> K*(892)0 J/psi(1S)]cc",
    am_min=4740.0 * MeV,
    am_max=5760.0 * MeV,
    apt_min=1200 * MeV,
    m_min=4750.0 * MeV,
    m_max=5750.0 * MeV,
    pt_min=1400 * MeV,
    vtx_chi2pdof_max=16,
    delta_m_min=2050 * MeV,
    delta_m_max=2310 * MeV,
    ownpv_dira_min=0.9995,
    ownpvltime_min=0.19 * picosecond,
):
    combination_code = F.require_all(in_range(am_min, F.MASS, am_max), (F.PT > apt_min))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < vtx_chi2pdof_max,
        (F.PT > pt_min),
        in_range(delta_m_min, F.MASS - F.CHILD(2, F.MASS), delta_m_max),
        F.OWNPVDIRA > ownpv_dira_min,
        F.OWNPVLTIME > ownpvltime_min,
    )

    return ParticleCombiner(
        name=name,
        Inputs=[KPi_comb(), Jpsi()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_Bs2JpsiPhi_pTag_NoMuonID(
    Jpsi=make_Jpsi_pTag_NoMuonID,
    KK_comb=make_detachedPhi_forB2JpsiX_NoMuonID,
    name="bandq_Bs2JpsiPhi_pTag_NoMuonID_{hash}",
    descriptor="B_s0 -> phi(1020) J/psi(1S) ",
    am_min=4860.0 * MeV,
    am_max=5880.0 * MeV,
    apt_min=1200 * MeV,
    m_min=4870.0 * MeV,
    m_max=5870.0 * MeV,
    pt_min=1400 * MeV,
    vtx_chi2pdof_max=16,
    delta_m_min=2150 * MeV,
    delta_m_max=2400 * MeV,
    ownpv_dira_min=0.9995,
    ownpvltime_min=0.19 * picosecond,
):
    combination_code = F.require_all(in_range(am_min, F.MASS, am_max), (F.PT > apt_min))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < vtx_chi2pdof_max,
        in_range(delta_m_min, F.MASS - F.CHILD(2, F.MASS), delta_m_max),
        (F.PT > pt_min),
        F.OWNPVDIRA > ownpv_dira_min,
        F.OWNPVLTIME > ownpvltime_min,
    )

    return ParticleCombiner(
        name=name,
        Inputs=[KK_comb(), Jpsi()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_Bs2JpsiPhi_mTag_NoMuonID(
    Jpsi=make_Jpsi_mTag_NoMuonID,
    KK_comb=make_detachedPhi_forB2JpsiX_NoMuonID,
    name="bandq_Bs2JpsiPhi_mTag_NoMuonID_{hash}",
    descriptor="B_s0 -> phi(1020) J/psi(1S) ",
    am_min=4860.0 * MeV,
    am_max=5880.0 * MeV,
    apt_min=1200 * MeV,
    m_min=4870.0 * MeV,
    m_max=5870.0 * MeV,
    pt_min=1400 * MeV,
    vtx_chi2pdof_max=16,
    delta_m_min=2150 * MeV,
    delta_m_max=2400 * MeV,
    ownpv_dira_min=0.9995,
    ownpvltime_min=0.19 * picosecond,
):
    combination_code = F.require_all(in_range(am_min, F.MASS, am_max), (F.PT > apt_min))

    vertex_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < vtx_chi2pdof_max,
        in_range(delta_m_min, F.MASS - F.CHILD(2, F.MASS), delta_m_max),
        (F.PT > pt_min),
        F.OWNPVDIRA > ownpv_dira_min,
        F.OWNPVLTIME > ownpvltime_min,
    )

    return ParticleCombiner(
        name=name,
        Inputs=[KK_comb(), Jpsi()],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
