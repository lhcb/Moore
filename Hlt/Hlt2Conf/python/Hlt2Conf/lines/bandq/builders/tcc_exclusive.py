###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Make B&Q exclusive Tcc combinations.
Compared to double charm combinations, the p and pT of K pi are loosened.
"""

import Functors as F
from Functors import require_all
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from PyConf import ConfigurationError, configurable
from RecoConf.algorithms_thor import (
    ParticleCombiner,
    ParticleContainersMerger,
    ParticleFilter,
)
from RecoConf.standard_particles import (
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
)

_PION_M = 139.57061 * MeV  # +/- 0.00024


def d0_to_hh_make_charm_pions():
    """Return pions maker for D0 decay."""
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.OWNPVIPCHI2 > 4.0,
                F.PT > 250 * MeV,
                F.P > 3.2 * GeV,
                F.P < 150 * GeV,
                F.PID_K < 5.0,
            ),
        ),
    )


def d0_to_hh_make_charm_kaons():
    """Return pions maker for D0 decay."""
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.OWNPVIPCHI2 > 4.0,
                F.PT > 250 * MeV,
                F.P > 3.2 * GeV,
                F.P < 150 * GeV,
                F.PID_K > 5.0,
            ),
        ),
    )


def d0_to_hh_make_dzeros(particle1, particle2, descriptor):
    """Return D0 maker with selection tailored for two-body hadronic final
    states.

    Args:
        particles (list of DataHandles): Input particles used in the
                                         combination.
        descriptor (string): Decay descriptor to be reconstructed.
    """
    if descriptor == "[D0 -> K- pi+]cc":
        name = "BandQ_Tcc_D0ToHH_BuilderD0ToKmPip"
    elif descriptor == "D0 -> K- K+":
        name = "BandQ_Tcc_D0ToHH_BuilderD0ToKmKp"
    elif descriptor == "D0 -> pi- pi+":
        name = "BandQ_Tcc_D0ToHH_BuilderD0ToPimPip"
    elif descriptor == "[D0 -> K+ pi-]cc":
        name = "BandQ_Tcc_D0ToHH_BuilderD0ToKpPim"
    else:
        raise RuntimeError(
            "bandq.tcc_exclusive_d0_lines.d0_to_hh_make_dzeros called with unsupported decay descriptor."
        )
    return ParticleCombiner(
        [particle1, particle2],
        DecayDescriptor=descriptor,
        name=name,
        CombinationCut=F.require_all(
            in_range(1685 * MeV, F.MASS, 2045 * MeV),
            F.PT > 1 * GeV,
            F.MAX(F.PT) > 1200 * MeV,
            F.MAXSDOCACUT(0.1 * mm),
        ),
        CompositeCut=F.require_all(
            in_range(1715 * MeV, F.MASS, 2015 * MeV),
            F.CHI2DOF < 10.0,
            F.OWNPVFDCHI2 > 25.0,
            F.OWNPVDIRA > 0.99985,
        ),
    )


def d0_to_hhhh_make_charm_pions():
    """Return pions maker for D0 decay."""
    return ParticleFilter(
        make_has_rich_long_pions(),
        F.FILTER(
            F.require_all(
                F.OWNPVIPCHI2 > 3.5,
                F.PT > 250 * MeV,
                F.PID_K < 5.0,
            ),
        ),
    )


def d0_to_hhhh_make_charm_kaons():
    """Return pions maker for D0 decay."""
    return ParticleFilter(
        make_has_rich_long_kaons(),
        F.FILTER(
            F.require_all(
                F.OWNPVIPCHI2 > 3.5,
                F.PT > 250 * MeV,
                F.PID_K > 5.0,
            ),
        ),
    )


def d0_to_hhhh_make_dzeros(
    particle1, particle2, particle3, particle4, descriptor, m3=_PION_M, m4=_PION_M
):
    """Return D0 maker with selection tailored for four-body hadronic final
    states.

    Args:
        particles (list of DataHandles): Input particles used in the
                                         combination.
        descriptor (string): Decay descriptor to be reconstructed.
        for_b (bool, optional): Sets whether the requirements should be tuned
                                to prompt or SL decays. Defaults to False.
    """
    if descriptor == "[D0 -> K- pi- pi+ pi+]cc":
        name = "BandQ_Tcc_D0ToHHHH_BuilderD0ToKmPimPipPip_{hash}"
    elif descriptor == "[D0 -> K+ pi- pi- pi+]cc":
        name = "BandQ_Tcc_D0ToHHHH_BuilderD0ToKpPimPimPip_{hash}"
    elif descriptor == "D0 -> K- K+ pi- pi+":
        name = "BandQ_Tcc_D0ToHHHH_BuilderD0ToKmKpPimPip_{hash}"
    elif descriptor == "D0 -> pi- pi- pi+ pi+":
        name = "BandQ_Tcc_D0ToHHHH_BuilderD0ToPimPimPipPip_{hash}"
    elif descriptor == "[D0 -> K- K- K+ pi+]cc":
        name = "BandQ_Tcc_D0ToHHHH_BuilderD0ToKmKmKpPip_{hash}"
    elif descriptor == "[D0 -> K- K+ K+ pi-]cc":
        name = "BandQ_Tcc_D0ToHHHH_BuilderD0ToKmKpKpPim_{hash}"
    else:
        raise RuntimeError(
            "bandq.tcc_exclusive_d0_lines.d0_to_hhhh_make_dzeros called with unsupported decay descriptor."
        )

    combination_cut = F.require_all(
        in_range(1790 * MeV, F.MASS, 1940 * MeV),
        F.PT > 1.0 * GeV,
        F.MAXSDOCACUT(0.2 * mm),
        F.require_any(
            F.SDOCA(1, 2) < 0.1 * mm,
            F.SDOCA(1, 3) < 0.1 * mm,
            F.SDOCA(1, 4) < 0.1 * mm,
            F.SDOCA(2, 3) < 0.1 * mm,
            F.SDOCA(2, 4) < 0.1 * mm,
            F.SDOCA(3, 4) < 0.1 * mm,
        ),
    )

    composite_cut = F.require_all(
        in_range(1790 * MeV, F.MASS, 1940 * MeV),
        F.PT > 1.0 * GeV,
        F.CHI2DOF < 6.0,
        F.OWNPVFDCHI2 > 50.0,
        F.SUM(F.PT) > 0.0 * MeV,
        F.OWNPVDIRA > 0.99,
    )

    return ParticleCombiner(
        [particle1, particle2, particle3, particle4],
        DecayDescriptor=descriptor,
        name=name,
        Combination12Cut=F.require_all(
            F.MASS < 2100 * MeV - m3 - m4,
            F.MAXSDOCACUT(0.2 * mm),
        ),
        Combination123Cut=F.require_all(
            F.MASS < 2100 * MeV - m4,
            F.MAXSDOCACUT(0.2 * mm),
        ),
        CombinationCut=combination_cut,
        CompositeCut=composite_cut,
    )


@configurable
def _make_tcc(
    particles,
    process,
    descriptor,
    name="bandq_tcc_singledecay_template_{hash}",
    allowDiffInputsForSameIDChildren=False,
    max_mass=None,
    max_ipchi2=None,
    max_chi2ndof=None,
):
    if process not in ["hlt2", "spruce"]:
        raise ConfigurationError(
            "process for _make_tcc must be hlt2 or spruce. Please check !"
        )
    if process == "spruce":
        combination_code = require_all(
            in_range(-10 * mm, (F.CHILD(1, F.OWNPVZ) - (F.CHILD(2, F.OWNPVZ))), 10 * mm)
        )
    elif process == "hlt2":
        combination_code = require_all(F.ALL)
    vertex_code = require_all(F.ALL)
    if max_mass is not None:
        vertex_code = require_all(vertex_code, F.MASS < max_mass)
    if max_ipchi2 is not None:
        vertex_code = require_all(vertex_code, F.OWNPVIPCHI2 < max_ipchi2)
    if max_chi2ndof is not None:
        vertex_code = require_all(vertex_code, F.CHI2DOF < max_chi2ndof)

    return ParticleCombiner(
        name=name,
        Inputs=particles,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        AllowDiffInputsForSameIDChildren=allowDiffInputsForSameIDChildren,
    )


@configurable
def make_tcc(
    particles,
    process,
    descriptors,
    name="bandq_tcc_template_{hash}",
    allowDiffInputsForSameIDChildren_tcc=False,
    max_mass=4250.0 * MeV,
    max_ipchi2=None,
    max_chi2ndof=None,
):
    assert len(descriptors) > 0
    c_hadrons = []
    for descriptor in descriptors:
        c_hadrons.append(
            _make_tcc(
                particles=particles,
                process=process,
                descriptor=descriptor,
                allowDiffInputsForSameIDChildren=allowDiffInputsForSameIDChildren_tcc,
                max_mass=max_mass,
                max_ipchi2=max_ipchi2,
                max_chi2ndof=max_chi2ndof,
            )
        )
    return ParticleContainersMerger(c_hadrons, name=name)


@configurable
def make_tcc_d0tohh(process, name="bandq_tcc_d0tohh_{hash}"):
    dz = d0_to_hh_make_dzeros(
        d0_to_hh_make_charm_kaons(), d0_to_hh_make_charm_pions(), "[D0 -> K- pi+]cc"
    )
    std_pion = make_has_rich_long_pions()
    line_alg = make_tcc(
        name=name,
        particles=[dz, dz, std_pion],
        process=process,
        descriptors=[
            "[Sigma_b+ -> D0 D0 pi+]cc",
        ],
    )
    return line_alg


@configurable
def make_tcc_d0tohhhh(process, name="bandq_tcc_d0tohhhh_{hash}"):
    dz = d0_to_hhhh_make_dzeros(
        d0_to_hhhh_make_charm_kaons(),
        d0_to_hhhh_make_charm_pions(),
        d0_to_hhhh_make_charm_pions(),
        d0_to_hhhh_make_charm_pions(),
        "[D0 -> K- pi- pi+ pi+]cc",
    )
    std_pion = make_has_rich_long_pions()
    line_alg = make_tcc(
        name=name,
        particles=[dz, dz, std_pion],
        process=process,
        descriptors=[
            "[Sigma_b+ -> D0 D0 pi+]cc",
        ],
    )
    return line_alg


@configurable
def make_tcc_d0tohh_d0tohhhh(process, name="bandq_tcc_d0tohh_d0tohhhh_{hash}"):
    dzhh = d0_to_hh_make_dzeros(
        d0_to_hh_make_charm_kaons(), d0_to_hh_make_charm_pions(), "[D0 -> K- pi+]cc"
    )
    dzhhhh = d0_to_hhhh_make_dzeros(
        d0_to_hhhh_make_charm_kaons(),
        d0_to_hhhh_make_charm_pions(),
        d0_to_hhhh_make_charm_pions(),
        d0_to_hhhh_make_charm_pions(),
        "[D0 -> K- pi- pi+ pi+]cc",
    )
    std_pion = make_has_rich_long_pions()
    line_alg = make_tcc(
        name=name,
        particles=[dzhh, dzhhhh, std_pion],
        process=process,
        descriptors=[
            "[Sigma_b+ -> D0 D0 pi+]cc",
        ],
        allowDiffInputsForSameIDChildren_tcc=True,
    )
    return line_alg
