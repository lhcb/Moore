###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define a set of particle combiners using Ttrack particles as input
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, meter
from PyConf import configurable
from PyConf.Tools import ParticleVertexFitter
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.standard_particles import make_ttrack_pions, make_ttrack_protons
from RecoConf.ttrack_selections_reco import PVF_with_single_extrapolation


@configurable
def make_ttrack_protons_for_V0(
    make_protons=make_ttrack_protons,
    p_min=20.0 * GeV,
    pt_min=400.0 * MeV,
    minip_max=400.0,
    minipchi2_max=30000.0,
    minipchi2_min=0.0,
):
    protons = make_protons()
    proton_cuts = F.require_all(
        F.P > p_min,
        F.PT > pt_min,
        in_range(2.0, F.ETA, 5.0),
        # F.GHOSTPROB < 0.4,
        F.OWNPVIP < minip_max,
        F.OWNPVIPCHI2 < minipchi2_max,
        F.OWNPVIPCHI2 > minipchi2_min,
    )
    return ParticleFilter(protons, F.FILTER(proton_cuts))


@configurable
def make_ttrack_pions_for_V0(
    make_pions=make_ttrack_pions, pt_min=75.0 * MeV, p_min=0.0 * GeV, minipchi2_min=0.0
):
    pions = make_pions()
    pion_cuts = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        # F.ECALPIDE < 1.,  # electron veto
        in_range(2.0, F.ETA, 5.0),
        # F.GHOSTPROB < 0.4,
        F.OWNPVIPCHI2 > minipchi2_min,
    )
    return ParticleFilter(pions, F.FILTER(pion_cuts))


@configurable
def _make_V0TT(
    particles,
    descriptors,
    pvs,
    yz_intersection_z_min=1000.0,
    maxdoca=50.0,
    maxdocachi2=300.0,
    vchi2pdof_max=50,
    p_min=10 * GeV,
    pt_min=1 * GeV,
    max_chi2=150,
    vertex_z_min=2.5 * meter,
    vertex_z_max=8.0 * meter,
    ownpv_dira_min=0.9995,
    ownpv_ip_max=100.0,
    ownpv_ip_chi2_max=200.0,
    ownpv_vdrho_min=80.0,
    RKextrapolation=False,
    name="V0TTCombiner",
):
    """Make T-T V0 -> h+ h'- candidates"""
    if RKextrapolation:
        fitter_tool = PVF_with_single_extrapolation
    else:
        fitter_tool = ParticleVertexFitter

    combination_code = F.require_all(
        F.TWOBODY_YZ_INTERSECTION_Z > yz_intersection_z_min,
        F.TWOBODY_TILT("Y") > 0.0,
        F.CHILD(1, F.math.sign(F.TY)) * F.math.sign(F.POD(F.TWOBODY_YZ_INTERSECTION_Y))
        > 0,
        F.MAXSDOCACUT(maxdoca),
        F.MAXSDOCACHI2CUT(maxdocachi2),
    )

    vertex_code = F.require_all(
        F.P > p_min,
        F.PT > pt_min,
        F.CHI2 < max_chi2,
        F.OWNPVDIRA > ownpv_dira_min,
        F.OWNPVIP < ownpv_ip_max,
        F.OWNPVIPCHI2 < ownpv_ip_chi2_max,
        F.OWNPVVDRHO > ownpv_vdrho_min,
        in_range(vertex_z_min, F.END_VZ, vertex_z_max),
    )

    return ParticleCombiner(
        Inputs=particles,
        DecayDescriptor=descriptors,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        ParticleCombiner=fitter_tool(),
        name=name,
    )


@configurable
def make_LambdaTT(pions, protons, pvs):
    """
    Make Lambda -> p pi (TT) candidates with a linear extrapolation in all iterations of the vertex fit.
    Vertex convergence can help to suppress background, but with the linear extrapolator the vertex position and mass are inaccurate.
    """
    descriptors = "[Lambda0 -> p+ pi-]cc"
    return _make_V0TT(
        particles=[protons, pions],
        descriptors=descriptors,
        pvs=pvs,
        name="Lambda0TTCombiner",
    )


@configurable
def make_LambdaRKTT(pions, protons, pvs):
    """
    Make Lambda -> p pi (TT) candidates with a Runge-Kutta (RK) extrapolation in the first iteration only of vertex fit, linear extrapolation in subsequent iterations.
    Using the RK extrapolator once improves the vertex bias and mass bias compared to linear extrapolator, but mass resolution is still poor.
    This improves efficiency in the first half of the magnet wrt purely linear extrapolation.
    """
    descriptors = "[Lambda0 -> p+ pi-]cc"
    return _make_V0TT(
        particles=[protons, pions],
        descriptors=descriptors,
        pvs=pvs,
        RKextrapolation=True,
        name="Lambda0RKTTCombiner",
    )


@configurable
def make_KsTT(pions, pvs):
    """
    Make KS0 -> pi pi (TT) candidates with a linear extrapolation in all iterations of the vertex fit.
    Vertex convergence can help to suppress background, but with the linear extrapolator the vertex position and mass are inaccurate.
    This method is very inefficient for decays in the first half of magnet.
    """
    descriptors = "KS0 -> pi+ pi-"
    return _make_V0TT(
        particles=[pions, pions], pvs=pvs, descriptors=descriptors, name="KsTTCombiner"
    )


@configurable
def make_KsRKTT(pions, pvs):
    """
    Make KS0 -> pi pi (TT) candidates with a Runge-Kutta (RK) extrapolation in the first iteration only of vertex fit, linear extrapolation in subsequent iterations.
    Using the RK extrapolator once improves the vertex bias and mass bias compared to linear extrapolator, but mass resolution is still poor.
    This improves efficiency in the first half of the magnet wrt purely linear extrapolation.
    """
    descriptors = "KS0 -> pi+ pi-"
    return _make_V0TT(
        particles=[pions, pions],
        pvs=pvs,
        RKextrapolation=True,
        descriptors=descriptors,
        name="KsRKTTCombiner",
        pt_min=1000 * MeV,
        ownpv_vdrho_min=150.0,
        p_min=2 * GeV,
    )
