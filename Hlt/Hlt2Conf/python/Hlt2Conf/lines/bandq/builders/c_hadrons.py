###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of B&Q c-hadrons
"""

import math

import Functors as F
from Functors import require_all
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, mm, picosecond
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner

#############################################
# c-hadron makers for all types of decays   #
# Cuts on mass window, vtxchi2,  detachment #
#############################################


# universal maker
@configurable
def _make_charm(
    particles,
    descriptor,
    name="bandq_Charm",
    am_min=1800 * MeV,
    am_max=1940 * MeV,
    m_min=1810 * MeV,
    m_max=1930 * MeV,
    asumpt_min=1800 * MeV,  # to discuss
    apt_min=950 * MeV,
    pt_min=1000 * MeV,
    achi2_doca_max=16,
    vtx_chi2pdof_max=9,
    ownpvltime_min=0.2 * picosecond,
    ownpvvdchi2_min=25,
    ownpvdira_min=0.95,
    max_mips_min=0.0,
    average_mips_min=0.0,
    minRho=0.0 * mm,
):
    combination_code = require_all(
        in_range(am_min, F.MASS, am_max),
        F.PT > apt_min,
        F.SUM(F.PT) > asumpt_min,
    )

    # if max_mips_min > 0:
    #     combination_code &= (F.MAXTREE(F.ISBASICPARTICLE, F.OWNPVIPCHI2) >
    #                          max_mips_min)

    if average_mips_min > 0:
        if len(particles) == 2:
            combination_code &= (
                F.CHILD(1, F.OWNPVIPCHI2) + F.CHILD(2, F.OWNPVIPCHI2)
            ) / 2 > average_mips_min
        elif len(particles) == 3:
            combination_code &= (
                F.CHILD(1, F.OWNPVIPCHI2)
                + F.CHILD(2, F.OWNPVIPCHI2)
                + F.CHILD(3, F.OWNPVIPCHI2)
            ) / 3 > average_mips_min
        elif len(particles) == 4:
            combination_code &= (
                F.CHILD(1, F.OWNPVIPCHI2)
                + F.CHILD(2, F.OWNPVIPCHI2)
                + F.CHILD(3, F.OWNPVIPCHI2)
                + F.CHILD(4, F.OWNPVIPCHI2)
            ) / 4 > average_mips_min

    vertex_code = require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.CHI2DOF < vtx_chi2pdof_max,
        F.OWNPVLTIME > ownpvltime_min,
        F.OWNPVFDCHI2 > ownpvvdchi2_min,
        F.OWNPVDIRA > ownpvdira_min,
        F.OWNPVVDRHO > minRho,
    )

    if len(particles) == 2:
        return ParticleCombiner(
            name=name,
            Inputs=particles,
            DecayDescriptor=descriptor,
            CombinationCut=combination_code,
            CompositeCut=vertex_code,
        )

    elif len(particles) == 3:
        combination12_code = require_all(
            F.MASS < am_max, F.SDOCACHI2(1, 2) < achi2_doca_max
        )

        combination_code &= require_all(
            F.SDOCACHI2(1, 3) < achi2_doca_max, F.SDOCACHI2(2, 3) < achi2_doca_max
        )

        return ParticleCombiner(
            name=name,
            Inputs=particles,
            DecayDescriptor=descriptor,
            Combination12Cut=combination12_code,
            CombinationCut=combination_code,
            CompositeCut=vertex_code,
        )

    if len(particles) == 4:
        combination12_code = require_all(
            F.MASS < am_max, F.SDOCACHI2(1, 2) < achi2_doca_max
        )

        combination123_code = require_all(
            F.MASS < am_max,
            F.SDOCACHI2(1, 3) < achi2_doca_max,
            F.SDOCACHI2(2, 3) < achi2_doca_max,
        )

        combination_code &= require_all(
            F.SDOCACHI2(1, 4) < achi2_doca_max,
            F.SDOCACHI2(2, 4) < achi2_doca_max,
            F.SDOCACHI2(3, 4) < achi2_doca_max,
        )

    return ParticleCombiner(
        name=name,
        Inputs=particles,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        Combination123Cut=combination123_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


####################################
# D0/D0bar                         #
####################################


@configurable
def make_dz(
    particles,
    descriptor,
    name="bandq_Dz_{hash}",
    am_min=1780 * MeV,
    am_max=1950 * MeV,
    m_min=1790 * MeV,
    m_max=1940 * MeV,
    minRho=0.0 * mm,
    **decay_arguments,
):
    return _make_charm(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        minRho=minRho,
        **decay_arguments,
    )


@configurable
def make_dz4h(
    particles,
    descriptor,
    name="bandq_Dz4h_{hash}",
    am_min=1800 * MeV,
    am_max=1940 * MeV,
    m_min=1810 * MeV,
    m_max=1930 * MeV,
    minRho=0.0 * mm,
    **decay_arguments,
):
    return _make_charm(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        minRho=minRho,
        **decay_arguments,
    )


####################################
# D+/D-                            #
####################################


@configurable
def make_dp(
    particles,
    descriptor,
    name="bandq_Dp_{hash}",
    am_min=1805 * MeV,
    am_max=1935 * MeV,
    m_min=1815 * MeV,
    m_max=1925 * MeV,
    minRho=0.0 * mm,
    **decay_arguments,
):
    return _make_charm(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        minRho=minRho,
        **decay_arguments,
    )


####################################
# D*(2010)+/D*(2010)-                            #
####################################


@configurable
def make_dstarp(
    particles,
    descriptor,
    name="bandq_Dstarp_{hash}",
    am_min=1940 * MeV,
    am_max=2080 * MeV,
    m_min=1950 * MeV,
    m_max=2070 * MeV,
    minRho=0.0 * mm,
    **decay_arguments,
):
    return _make_charm(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        minRho=minRho,
        **decay_arguments,
    )


####################################
# Ds+/Ds-                          #
####################################


@configurable
def make_ds(
    particles,
    descriptor,
    name="bandq_Ds_{hash}",
    am_min=1900 * MeV,
    am_max=2035 * MeV,
    m_min=1910 * MeV,
    m_max=2025 * MeV,
    minRho=0.0 * mm,
    **decay_arguments,
):
    return _make_charm(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        minRho=minRho,
        **decay_arguments,
    )


####################################
# Lc+/Lc-                          #
####################################


@configurable
def make_lc(
    particles,
    descriptor,
    name="bandq_Lc_{hash}",
    am_min=2220 * MeV,
    am_max=2350 * MeV,
    m_min=2230 * MeV,
    m_max=2340 * MeV,
    minRho=0.0 * mm,
    **decay_arguments,
):
    return _make_charm(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        minRho=minRho,
        **decay_arguments,
    )


####################################
# Xic+/Xic-/Xic0                   #
####################################


@configurable
def make_xic(
    particles,
    descriptor,
    name="bandq_Xic_{hash}",
    am_min=2400 * MeV,
    am_max=2530 * MeV,
    m_min=2410 * MeV,
    m_max=2520 * MeV,
    **decay_arguments,
):
    return _make_charm(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments,
    )


####################################
# Omegac0                          #
####################################


@configurable
def make_omegac(
    particles,
    descriptor,
    name="bandq_Omegac_{hash}",
    am_min=2630 * MeV,
    am_max=2750 * MeV,
    m_min=2640 * MeV,
    m_max=2740 * MeV,
    **decay_arguments,
):
    return _make_charm(
        particles,
        descriptor,
        name=name,
        am_min=am_min,
        am_max=am_max,
        m_min=m_min,
        m_max=m_max,
        **decay_arguments,
    )


# deprecated
@configurable
def make_tightomegac(
    particles,
    descriptor,
    name="bandq_tightOmegac_{hash}",
    am_min=2630 * MeV,
    am_max=2750 * MeV,
    comb_sum_pt_min=3000.0 * MeV,
    comb_pt_min_at_least_one=1000.0 * MeV,
    comb_pt_min_at_least_two=400.0 * MeV,
    comb_mipchi2_min_at_least_one=8.0,
    comb_mipchi2_min_at_least_two=6.0,
    pt_min=1000.0 * MeV,
    vtx_chi2pdof_max=10,
    ownpvltime_min=0.2 * picosecond,
    apt_min=950 * MeV,
    ownpvdira_min=math.cos(0.01),
    achi2_doca_max=25,
    m_min=2640 * MeV,
    m_max=2740 * MeV,
):
    combination_code = require_all(
        in_range(am_min, F.MASS, am_max),
        F.PT > apt_min,
        F.SDOCACHI2(1, 2) < achi2_doca_max,
        F.SDOCACHI2(1, 3) < achi2_doca_max,
        F.SDOCACHI2(2, 3) < achi2_doca_max,
        F.SUM(F.PT) > comb_sum_pt_min,
        F.SUM(F.PT > comb_pt_min_at_least_one) >= 1,
        F.SUM(F.PT > comb_pt_min_at_least_two) >= 2,
        F.SUM(F.OWNPVIPCHI2 > comb_mipchi2_min_at_least_one) >= 1,
        F.SUM(F.OWNPVIPCHI2 > comb_mipchi2_min_at_least_two) >= 2,
    )
    vertex_code = require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.CHI2DOF < vtx_chi2pdof_max,
        F.OWNPVDIRA > ownpvdira_min,
        F.OWNPVLTIME > ownpvltime_min,
    )
    return ParticleCombiner(
        name=name,
        Inputs=particles,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
