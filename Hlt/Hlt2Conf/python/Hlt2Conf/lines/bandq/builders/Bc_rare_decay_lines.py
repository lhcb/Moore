###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of B&Q b-hadrons
"""

import Functors as F
from GaudiKernel.SystemOfUnits import MeV, mm
from PyConf import configurable

from Hlt2Conf.lines.bandq.builders import b_hadrons, c_to_hadrons, charged_hadrons

########################
# Bc -> three body#
########################


@configurable
def make_BcToDpKpPim(name="bandq_BcToDpKpPim_{hash}"):
    pion = charged_hadrons.make_detached_pions_soft(
        mipchi2dvprimary_min=12.0, pt_min=500.0 * MeV
    )
    kaon = charged_hadrons.make_detached_kaons(
        mipchi2dvprimary_min=12.0, pt_min=500.0 * MeV
    )
    Dp = c_to_hadrons.make_DpToKmPipPip(
        minRho=0.2 * mm,
    )
    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[Dp, kaon, pion],
        descriptor="[B_c+ -> D+ K+ pi- ]cc",
        ownpvdira_min=0.9999,
        am_min=5950 * MeV,
        m_min=6000 * MeV,
        am_max=6850 * MeV,
        m_max=6800 * MeV,
    )
    return line_alg


@configurable
def make_BcToDstpKpPim(name="bandq_BcToDstpKpPim_{hash}"):
    pion = charged_hadrons.make_detached_pions(
        mipchi2dvprimary_min=12.0, pt_min=500.0 * MeV
    )
    kaon = charged_hadrons.make_detached_kaons(
        mipchi2dvprimary_min=12.0, pt_min=500.0 * MeV
    )
    Dstp = c_to_hadrons.make_DstarpToKmPipPip(
        minRho=0.2 * mm,
    )
    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[Dstp, kaon, pion],
        descriptor="[B_c+ -> D*(2010)+ K+ pi- ]cc",
        ownpvdira_min=0.9999,
        am_min=5950 * MeV,
        m_min=6000 * MeV,
        am_max=6850 * MeV,
        m_max=6800 * MeV,
    )
    return line_alg


@configurable
def make_BcToDspKpKm(name="bandq_BcToDspKpKm_{hash}"):
    kaon = charged_hadrons.make_detached_kaons(
        mipchi2dvprimary_min=12.0, pt_min=500.0 * MeV
    )
    Dsp = c_to_hadrons.make_DspToKmKpPip(
        minRho=0.2 * mm,
    )
    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[Dsp, kaon, kaon],
        descriptor="[B_c+ -> D_s+ K+ K- ]cc",
        ownpvdira_min=0.9999,
        am_min=5950 * MeV,
        m_min=6000 * MeV,
        am_max=6850 * MeV,
        m_max=6800 * MeV,
    )
    return line_alg


@configurable
def make_BcToDspPipPim(name="bandq_BcToDspPipPim_{hash}"):
    pion = charged_hadrons.make_detached_pions_soft(
        mipchi2dvprimary_min=12.0, pt_min=500.0 * MeV
    )
    Dsp = c_to_hadrons.make_DspToKmKpPip(
        minRho=0.2 * mm,
    )
    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[Dsp, pion, pion],
        descriptor="[B_c+ -> D_s+ pi+ pi- ]cc",
        ownpvdira_min=0.9999,
        am_min=5950 * MeV,
        m_min=6000 * MeV,
        am_max=6850 * MeV,
        m_max=6800 * MeV,
    )
    return line_alg


@configurable
def make_BcToDspPpPm(name="bandq_BcToDspPpPm_{hash}"):
    proton = charged_hadrons.make_detached_protons(
        mipchi2dvprimary_min=12.0, pt_min=500.0 * MeV
    )
    Dsp = c_to_hadrons.make_DspToKmKpPip(
        minRho=0.2 * mm,
    )
    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[Dsp, proton, proton],
        descriptor="[B_c+ -> D_s+ p+ p~- ]cc",
        ownpvdira_min=0.9999,
        am_min=5950 * MeV,
        m_min=6000 * MeV,
        am_max=6850 * MeV,
        m_max=6800 * MeV,
    )
    return line_alg


@configurable
def make_BcToLcpPmKp(name="bandq_BcToLcpPmKp_{hash}"):
    kaon = charged_hadrons.make_detached_kaons(
        mipchi2dvprimary_min=12.0, pt_min=500.0 * MeV
    )
    proton = charged_hadrons.make_detached_protons(
        mipchi2dvprimary_min=12.0, pt_min=500.0 * MeV
    )
    Lcp = c_to_hadrons.make_LcToPpKmPip(
        minRho=0.2 * mm,
    )
    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[Lcp, proton, kaon],
        descriptor="[B_c+ -> Lambda_c+ p~- K+]cc",
        ownpvdira_min=0.9999,
        am_min=5950 * MeV,
        m_min=6000 * MeV,
        am_max=6850 * MeV,
        m_max=6800 * MeV,
    )
    return line_alg


########################
# Bc -> four body#
########################


@configurable
def make_BcToD0KpPipPim(name="bandq_BcToD0KpPipPim_{hash}"):
    pion_soft = charged_hadrons.make_detached_pions_soft(mipchi2dvprimary_min=12.0)
    pion = charged_hadrons.make_detached_pions(
        mipchi2dvprimary_min=12.0, pt_min=500.0 * MeV
    )
    kaon = charged_hadrons.make_detached_kaons(
        mipchi2dvprimary_min=12.0, pt_min=500.0 * MeV
    )
    dzero = c_to_hadrons.make_Dz_tightip(
        minRho=0.2 * mm,
    )
    line_alg = b_hadrons.make_bc(
        name=name,
        particles=[dzero, kaon, pion_soft, pion],
        descriptor="[B_c+ -> D0 K+ pi+ pi- ]cc",
        ownpvdira_min=0.9999,
        am_min=5950 * MeV,
        m_min=6000 * MeV,
        am_max=6850 * MeV,
        m_max=6800 * MeV,
    )
    return line_alg
