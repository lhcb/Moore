###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Booking of B&Q sprucing lines, notice PROCESS = 'spruce'

Output:
updated dictionary of sprucing_lines

"""

from Moore.config import SpruceLine, register_line_builder
from PyConf import configurable

from Hlt2Conf.lines.bandq.builders import (
    Bc_lines,
    Bc_rare_decay_lines,
    Xib_to_BLmd,
    b_for_spectroscopy,
    b_to_cch,
    b_to_doublecharm,
    b_to_etacX_lines,
    b_to_jpsiX_lines,
    b_to_jpsiX_NoMuonID,
    bbaryon_to_lcdsX_lines,
    bx,
    dimuon_lines,
    dimuon_sprucing_lines,
    doublecharm,
    qqbar_to_hadrons,
    tcc_exclusive,
    xibc_lines,
)
from Hlt2Conf.lines.bandq.builders.helper import psis_list
from Hlt2Conf.lines.bandq.builders.prefilters import make_prefilters

PROCESS = "spruce"
sprucing_lines = {}

#################################
# dimuon sprucings, tighter cut than hlt2 lines
#################################


@register_line_builder(sprucing_lines)
@configurable
def JpsiToMuMuDetached_sprucing_line(name="SpruceBandQ_JpsiToMuMuDetached", prescale=1):
    """Detached Jpsi -> mu+ mu-"""
    line_alg = dimuon_sprucing_lines.make_detached_jpsi_sprucing()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(sprucing_lines)
@configurable
def Psi2SToMuMuDetached_sprucing_line(
    name="SpruceBandQ_Psi2SToMuMuDetached", prescale=1
):
    """Detached psi(2S) -> mu+ mu-"""
    line_alg = dimuon_sprucing_lines.make_detached_psi2s_sprucing()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(sprucing_lines)
@configurable
def JpsiToMuMuTightPrompt_sprucing_line(
    name="SpruceBandQ_JpsiToMuMuTightPrompt", prescale=1
):
    """Prompt Jpsi -> mu+ mu-"""
    line_alg = dimuon_sprucing_lines.make_tight_prompt_jpsi_sprucing()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(sprucing_lines)
@configurable
def Psi2SToMuMuTightPrompt_sprucing_line(
    name="SpruceBandQ_Psi2SToMuMuTightPrompt", prescale=1
):
    """Prompt psi(2S) -> mu+ mu-"""
    line_alg = dimuon_sprucing_lines.make_tight_prompt_psi2s_sprucing()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
    )


#######################
# Move persistReco=True Turbo dimuon lines into full stream + sprucing, to avoid overlap with other streams
# Remove persistReco=True label in stripping for MDST lines in Run2: https://lhcbdoc.web.cern.ch/lhcbdoc/stripping/config/stripping34/index.html
######################
@register_line_builder(sprucing_lines)
@configurable
def DiMuonSameSignHighMass_sprucing_line(
    name="SpruceBandQ_DiMuonSameSignHighMass", prescale=1
):
    line_alg = dimuon_lines.make_HighMass_samesign_dimuon()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DiMuonSameSignHighMassFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def DiMuonInc_sprucing_line(name="SpruceBandQ_DiMuonInc", prescale=1):
    line_alg = dimuon_lines.make_loose_dimuon()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=False,
        hlt2_filter_code=["Hlt2BandQ_DiMuonIncFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def DiMuonSameSignInc_sprucing_line(name="SpruceBandQ_DiMuonSameSignInc", prescale=1):
    line_alg = dimuon_lines.make_loose_samesign_dimuon()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=False,
        hlt2_filter_code=["Hlt2BandQ_DiMuonSameSignIncFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def DiMuonIncHighPT_sprucing_line(name="SpruceBandQ_DiMuonIncHighPT", prescale=1):
    line_alg = dimuon_lines.make_tight_highpt_dimuon()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=False,
        hlt2_filter_code=["Hlt2BandQ_DiMuonIncHighPTFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def DiMuonSameSignIncHighPT_sprucing_line(
    name="SpruceBandQ_DiMuonSameSignIncHighPT", prescale=1
):
    line_alg = dimuon_lines.make_tight_highpt_samesign_dimuon()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=False,
        hlt2_filter_code=["Hlt2BandQ_DiMuonSameSignIncHighPTFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def DiMuonSoft_sprucing_line(name="SpruceBandQ_DiMuonSoft", prescale=1):
    line_alg = dimuon_lines.make_soft_detached_dimuon()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DiMuonSoftFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def UpsilonToMuMu_sprucing_line(name="SpruceBandQ_DiMuonUpsilon", prescale=1):
    line_alg = dimuon_lines.make_upsilon()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DiMuonUpsilonFullDecision"],
    )


#################################
# Book b->JpsiX sprucing lines  #
#################################

hlt2_lines_detached_jpsi = ["Hlt2_JpsiToMuMuDetachedFullDecision"]
hlt2_lines_detached_psi2s = ["Hlt2_Psi2SToMuMuDetachedFullDecision"]

hlt2_lines_detached_psi = [
    "Hlt2_JpsiToMuMuDetachedFullDecision",
    "Hlt2_Psi2SToMuMuDetachedFullDecision",
]


@register_line_builder(sprucing_lines)
@configurable
def BuToJpsiKp_JpsiToMuMu_sprucing_line(
    name="SpruceBandQ_BpToJpsiKp_JpsiToMuMu", prescale=1
):
    """B+ --> Jpsi(-> mu+ mu-)  K+ line"""
    line_alg = b_to_jpsiX_lines.make_BuToJpsiKp_JpsiToMuMu(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi,
    )


##############################
# Book b->JpsiX NoMuon lines #
##############################
hlt2_lines_b2jpsix_nomuon = ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"]


@register_line_builder(sprucing_lines)
@configurable
def BuToJpsiKp_mTag_MVA_sprucing_line(
    name="SpruceBandQ_BpToJpsiKp_mTag_MVA", prescale=1
):
    """B+ --> Jpsi(-> mu+ mu-)  K+ mTag line"""
    line_alg = b_to_jpsiX_NoMuonID.make_Bp2JpsiKp_mTag_MVA_NoMuonID()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_b2jpsix_nomuon,
    )


@register_line_builder(sprucing_lines)
@configurable
def BuToJpsiKp_pTag_MVA_sprucing_line(
    name="SpruceBandQ_BpToJpsiKp_pTag_MVA", prescale=1
):
    """B+ --> Jpsi(-> mu+ mu-)  K+ pTag line"""
    line_alg = b_to_jpsiX_NoMuonID.make_Bp2JpsiKp_pTag_MVA_NoMuonID()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_b2jpsix_nomuon,
    )


@register_line_builder(sprucing_lines)
@configurable
def BuToJpsiKp_mTag_sprucing_line(name="SpruceBandQ_BpToJpsiKp_mTag", prescale=1):
    """B+ --> Jpsi(-> mu+ mu-)  K+ NoRICHTag line"""
    line_alg = b_to_jpsiX_NoMuonID.make_Bp2JpsiKp_mTag_NoMuonID()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_b2jpsix_nomuon,
    )


@register_line_builder(sprucing_lines)
@configurable
def BuToJpsiKp_pTag_sprucing_line(name="SpruceBandQ_BpToJpsiKp_pTag", prescale=1):
    """B+ --> Jpsi(-> mu+ mu-)  K+ NoRICHTag line"""
    line_alg = b_to_jpsiX_NoMuonID.make_Bp2JpsiKp_pTag_NoMuonID()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_b2jpsix_nomuon,
    )


@register_line_builder(sprucing_lines)
@configurable
def BzToJpsiKst_mTag_sprucing_line(name="SpruceBandQ_BzToJpsiKst_mTag", prescale=1):
    """B+ --> Jpsi(-> mu+ mu-)  Kst(-> Kp pim) mTag line"""
    line_alg = b_to_jpsiX_NoMuonID.make_Bz2JpsiKst_mTag_NoMuonID()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_b2jpsix_nomuon,
    )


@register_line_builder(sprucing_lines)
@configurable
def BzToJpsiKst_pTag_sprucing_line(name="SpruceBandQ_BzToJpsiKst_pTag", prescale=1):
    """B0 --> Jpsi(-> mu+ mu-)  Kst(-> Kp pim) pTag line"""
    line_alg = b_to_jpsiX_NoMuonID.make_Bz2JpsiKst_pTag_NoMuonID()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_b2jpsix_nomuon,
    )


@register_line_builder(sprucing_lines)
@configurable
def BsToJpsiPhi_mTag_sprucing_line(name="SpruceBandQ_BsToJpsiPhi_mTag", prescale=1):
    """Bs --> Jpsi(-> mu+ mu-)  phi(-> Kp Km) mTag line"""
    line_alg = b_to_jpsiX_NoMuonID.make_Bs2JpsiPhi_mTag_NoMuonID()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_b2jpsix_nomuon,
    )


@register_line_builder(sprucing_lines)
@configurable
def BsToJpsiPhi_pTag_sprucing_line(name="SpruceBandQ_BsToJpsiPhi_pTag", prescale=1):
    """Bs --> Jpsi(-> mu+ mu-)  Phi(-> Kp Km) pTag line"""
    line_alg = b_to_jpsiX_NoMuonID.make_Bs2JpsiPhi_pTag_NoMuonID()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_b2jpsix_nomuon,
    )


###################################################
# Book b -> ccbar X, ccbar->hadron sprucing lines #
###################################################


@register_line_builder(sprucing_lines)
@configurable
def LbToEtacPpKm_EtacToKsLLKpPim_line(
    name="SpruceBandQ_LbToEtacPpKm_EtacToKsLLKpPim", prescale=1
):
    """Lambda_b0 --> Etac p K- line"""
    line_alg = b_to_etacX_lines.make_LbToEtacPpKm_EtacToKsLLKpPim()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def LbToEtacPpKm_EtacToKsDDKpPim_line(
    name="SpruceBandQ_LbToEtacPpKm_EtacToKsDDKpPim", prescale=1
):
    """Lambda_b0 --> Etac p K- line"""
    line_alg = b_to_etacX_lines.make_LbToEtacPpKm_EtacToKsDDKpPim()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def LbToEtacPpKm_EtacToPpPm_line(
    name="SpruceBandQ_LbToEtacPpKm_EtacToPpPm", prescale=1
):
    """Lambda_b0 --> Etac p K- line"""
    line_alg = b_to_etacX_lines.make_LbToEtacPpKm_EtacToPpPm()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def LbToEtacPpKm_EtacToHHHH_sprucing_line(
    name="SpruceBandQ_LbToEtacPpKm_EtacToHHHH", prescale=1, B_MVA_cut=0.1
):
    """Lambda_b0 --> Etac p K- line"""
    line_alg = b_to_etacX_lines.make_LbToEtacPpKm_EtacToHHHH()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def BdToEtacKpPim_EtacToHHHH_sprucing_line(
    name="SpruceBandQ_BdToEtacKpPim_EtacToHHHH", prescale=1, B_MVA_cut=0.1
):
    """B0 -> Etac K+ pi- line"""
    line_alg = b_to_etacX_lines.make_BdToEtacKpPim_EtacToHHHH()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def BuToEtacKpPhi_EtacToHHHH_sprucing_line(
    name="SpruceBandQ_BuToEtacKpPhi_EtacToHHHH", prescale=1
):
    """B0 -> Etac phi(1020) K+ line"""
    line_alg = b_to_etacX_lines.make_BuToEtacKpPhi_EtacToHHHH()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def BdToEtacKpPim_EtacToPpPm_sprucing_line(
    name="SpruceBandQ_BdToEtacKpPim_EtacToPpPm", prescale=1
):
    """B0 -> Etac K+ pi- line"""
    line_alg = b_to_etacX_lines.make_BdToEtacKpPim_EtacToPpPm()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def BdToEtacKpPim_EtacToKsLLKpPim_sprucing_line(
    name="SpruceBandQ_BdToEtacKpPim_EtacToKsLLKpPim", prescale=1
):
    """B0 -> Etac K+ pi- line"""
    line_alg = b_to_etacX_lines.make_BdToEtacKpPim_EtacToKsLLKpPim()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def BdToEtacKpPim_EtacToKsDDKpPim_sprucing_line(
    name="SpruceBandQ_BdToEtacKpPim_EtacToKsDDKpPim", prescale=1
):
    """B0 -> Etac K+ pi- line"""
    line_alg = b_to_etacX_lines.make_BdToEtacKpPim_EtacToKsDDKpPim()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def BuToEtacKpPhi_EtacToPpPm_sprucing_line(
    name="SpruceBandQ_BuToEtacKpPhi_EtacToPpPm", prescale=1
):
    """B0 -> Etac phi(1020) K+ line"""
    line_alg = b_to_etacX_lines.make_BuToEtacKpPhi_EtacToPpPm()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def LbToEtacPpKm_EtacToKpKm_sprucing_line(
    name="SpruceBandQ_LbToEtacPpKm_EtacToKpKm", prescale=0.1
):
    """Lambda_b0 --> Etac K- p line"""
    line_alg = b_to_etacX_lines.make_LbToEtacPpKm_EtacToKpKm()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def BdToEtacKpPim_EtacToKpKm_sprucing_line(
    name="SpruceBandQ_BdToEtacKpPim_EtacToKpKm", prescale=0.1
):
    """B0 -> Etac K+ pi- line"""
    line_alg = b_to_etacX_lines.make_BdToEtacKpPim_EtacToKpKm()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def BuToEtacKpPhi_EtacToKpKm_sprucing_line(
    name="SpruceBandQ_BuToEtacKpPhi_EtacToKpKm", prescale=0.1
):
    """B0 -> Etac phi(1020) K+ line"""
    line_alg = b_to_etacX_lines.make_BuToEtacKpPhi_EtacToKpKm()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def LbToEtacPpKm_EtacToPipPim_sprucing_line(
    name="SpruceBandQ_LbToEtacPpKm_EtacToPipPim", prescale=0.1
):
    """Lambda_b0 --> Etac K- p line"""
    line_alg = b_to_etacX_lines.make_LbToEtacPpKm_EtacToPipPim()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def BdToEtacKpPim_EtacToPipPim_sprucing_line(
    name="SpruceBandQ_BdToEtacKpPim_EtacToPipPim", prescale=0.1
):
    """B0 -> Etac K+ pi- line"""
    line_alg = b_to_etacX_lines.make_BdToEtacKpPim_EtacToPipPim()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def BuToEtacKpPhi_EtacToPipPim_sprucing_line(
    name="SpruceBandQ_BuToEtacKpPhi_EtacToPipPim", prescale=0.1
):
    """B0 -> Etac phi(1020) K+ line"""
    line_alg = b_to_etacX_lines.make_BuToEtacKpPhi_EtacToPipPim()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


############################################
# Book detached ccbar->ppbar sprucing line #
############################################
@register_line_builder(sprucing_lines)
@configurable
def ccbarToPpPmDetached_sprucing_line(
    name="SpruceBandQ_ccbarToPpPmDetached", prescale=1
):
    """ccbar->ppbar detached line"""
    line_alg = qqbar_to_hadrons.make_detached_ccbarToPpPm_spruce()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi,
    )


############################################
# Book Xibc/Tbc sprucing lines
############################################


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiLc_sprucing_line(name="SpruceBandQ_XibcToJpsiLc", prescale=1):
    """Xi_bc+ -> J/psi(1S) Lambda_c+ line"""
    line_alg = xibc_lines.make_XibcToJpsiLc()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi,
    )


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiXicz_sprucing_line(name="SpruceBandQ_XibcToJpsiXicz", prescale=1):
    """Xi_bc+ -> J/psi(1S) Xi_c0 line"""
    line_alg = xibc_lines.make_XibcToJpsiXicz()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi,
    )


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiLcKm_sprucing_line(name="SpruceBandQ_XibcToJpsiLcKm", prescale=1):
    """Xi_bc0 -> J/psi(1S) Lambda_c+ K- line"""
    line_alg = xibc_lines.make_XibcToJpsiLcKm()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi,
    )


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiLcKmPip_sprucing_line(name="SpruceBandQ_XibcToJpsiLcKmPip", prescale=1):
    """Xi_bc+ -> J/psi(1S) Lambda_c+ K- pi+ line"""
    line_alg = xibc_lines.make_XibcToJpsiLcKmPip()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi,
    )


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiDzPKm_sprucing_line(name="SpruceBandQ_XibcToJpsiDzPKm", prescale=1):
    """Xi_bc0 -> J/psi(1S) D0 p+ K- line"""
    line_alg = xibc_lines.make_XibcToJpsiDzPKm()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi,
    )


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiDpPKm_sprucing_line(name="SpruceBandQ_XibcToJpsiDpPKm", prescale=1):
    """Xi_bc+ -> J/psi(1S) D+ p+ K- line"""
    line_alg = xibc_lines.make_XibcToJpsiDpPKm()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi,
    )


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiDzPKmPip_sprucing_line(name="SpruceBandQ_XibcToJpsiDzPKmPip", prescale=1):
    """Xi_bc+ -> J/psi(1S) D0 p+ K- pi+ line"""
    line_alg = xibc_lines.make_XibcToJpsiDzPKmPip()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi,
    )


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiDzLam_sprucing_line(name="SpruceBandQ_XibcToJpsiDzLam", prescale=1):
    """Xi_bc0 -> J/psi(1S) D0 Lambda0 line"""
    line_alg = xibc_lines.make_XibcToJpsiDzLam()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi,
    )


@register_line_builder(sprucing_lines)
@configurable
def XibcToJpsiDpLam_sprucing_line(name="SpruceBandQ_XibcToJpsiDpLam", prescale=1):
    """Xi_bc+ -> J/psi(1S) D+ Lambda0 line"""
    line_alg = xibc_lines.make_XibcToJpsiDpLam()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi,
    )


############################################
# Book Tbc sprucing lines
############################################


@register_line_builder(sprucing_lines)
@configurable
def TbcToJpsiDz_sprucing_line(name="SpruceBandQ_TbcToJpsiDz", prescale=1):
    """Xi_bc0 -> J/psi(1S) D0 line"""
    line_alg = xibc_lines.make_TbcToJpsiDz()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi,
    )


@register_line_builder(sprucing_lines)
@configurable
def TbcToJpsiDpKm_sprucing_line(name="SpruceBandQ_TbcToJpsiDpKm", prescale=1):
    """Xi_bc0 -> J/psi(1S) D+ K- line"""
    line_alg = xibc_lines.make_TbcToJpsiDpKm()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi,
    )


@register_line_builder(sprucing_lines)
@configurable
def TbcToJpsiDzKmPip_sprucing_line(name="SpruceBandQ_TbcToJpsiDzKmPip", prescale=1):
    """Xi_bc0 -> J/psi(1S) D0 K- pi+ line"""
    line_alg = xibc_lines.make_TbcToJpsiDzKmPip()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi,
    )


@register_line_builder(sprucing_lines)
@configurable
def TbcToJpsiDzKs_sprucing_line(name="SpruceBandQ_TbcToJpsiDzKs", prescale=1):
    """Xi_bc0 -> J/psi(1S) D0 KS0 line"""
    line_alg = xibc_lines.make_TbcToJpsiDzKs()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi,
    )


############################################
# Book Bc sprucing lines
############################################


### Bc -> Jpsi X
@register_line_builder(sprucing_lines)
@configurable
def BcToJpsiPip_sprucing_line(name="SpruceBandQ_BcToJpsiPip", prescale=1):
    """B_c+ -> J/psi(1S) pi+ line"""
    line_alg = b_to_jpsiX_lines.make_BcToJpsiPip_JpsiToMuMu(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi,
    )


@register_line_builder(sprucing_lines)
@configurable
def BcToJpsiPipPipPim_sprucing_line(name="SpruceBandQ_BcToJpsiPipPipPim", prescale=1):
    """B_c+ -> J/psi(1S) pi+ pi+ pi- line"""
    line_alg = b_to_jpsiX_lines.make_BcToJpsiPipPipPim_JpsiToMuMu(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi,
    )


@register_line_builder(sprucing_lines)
@configurable
def BcToJpsi5Pi_sprucing_line(name="SpruceBandQ_BcToJpsi5Pi", prescale=1):
    """B_c+ -> J/psi(1S) pi+ pi+ pi+ pi- pi- line"""
    line_alg = b_to_jpsiX_lines.make_BcToJpsi5Pi_JpsiToMuMu(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_jpsi,
    )


@register_line_builder(sprucing_lines)
@configurable
def BcToPsi2SPip_sprucing_line(name="SpruceBandQ_BcToPsi2SPip", prescale=1):
    """B_c+ -> psi(2S) pi+ line"""
    line_alg = b_to_jpsiX_lines.make_BcToPsi2SPip_Psi2SToMuMu(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_psi2s,
    )


@register_line_builder(sprucing_lines)
@configurable
def BcToPsi2SPipPipPim_sprucing_line(name="SpruceBandQ_BcToPsi2SPipPipPim", prescale=1):
    """B_c+ -> psi(2S) pi+ pi+ pi- line"""
    line_alg = b_to_jpsiX_lines.make_BcToPsi2SPipPipPim_Psi2SToMuMu(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=hlt2_lines_detached_psi2s,
    )


### Bc+ -> hhh


@register_line_builder(sprucing_lines)
@configurable
def BcToPpPmKp_sprucing_line(name="SpruceBandQ_BcToPpPmKp", prescale=1):
    """B_c+ -> p+ p~- K+ line"""
    line_alg = Bc_lines.make_BcToPpPmKp()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToPpPmPip_sprucing_line(name="SpruceBandQ_BcToPpPmPip", prescale=1):
    """B_c+ -> p+ p~- pi+ line"""
    line_alg = Bc_lines.make_BcToPpPmPip()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToPipPipPim_sprucing_line(name="SpruceBandQ_BcToPipPipPim", prescale=1):
    """B_c+ -> pi+ pi+ pi- line"""
    line_alg = Bc_lines.make_BcToPipPipPim()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToKpKpKm_sprucing_line(name="SpruceBandQ_BcToKpKpKm", prescale=1):
    """B_c+ -> K+ K+ K- line"""
    line_alg = Bc_lines.make_BcToKpKpKm()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToPipPimKp_sprucing_line(name="SpruceBandQ_BcToPipPimKp", prescale=1):
    """B_c+ -> pi+ pi- K+ line"""
    line_alg = Bc_lines.make_BcToPipPimKp()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToKpKmPip_sprucing_line(name="SpruceBandQ_BcToKpKmPip", prescale=1):
    """B_c+ -> K+ K- pi+ line"""
    line_alg = Bc_lines.make_BcToKpKmPip()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToPpPmKp_sprucing_line(name="SpruceBandQ_BuToPpPmKp", prescale=1):
    """B_c+ -> p+ p~- K+ control channel"""
    line_alg = Bc_lines.make_BuToPpPmKp()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToPpPmPip_sprucing_line(name="SpruceBandQ_BuToPpPmPip", prescale=1):
    """B_c+ -> p+ p~- pi+ control channel"""
    line_alg = Bc_lines.make_BuToPpPmPip()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToPipPipPim_sprucing_line(name="SpruceBandQ_BuToPipPipPim", prescale=1):
    """B_c+ -> pi+ pi+ pi- control channel"""
    line_alg = Bc_lines.make_BuToPipPipPim()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToKpKpKm_sprucing_line(name="SpruceBandQ_BuToKpKpKm", prescale=1):
    """B_c+ -> K+ K+ K- control channel"""
    line_alg = Bc_lines.make_BuToKpKpKm()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToPipPimKp_sprucing_line(name="SpruceBandQ_BuToPipPimKp", prescale=1):
    """B_c+ -> pi+ pi- K+ control channel"""
    line_alg = Bc_lines.make_BuToPipPimKp()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToKpKmPip_sprucing_line(name="SpruceBandQ_BuToKpKmPip", prescale=1):
    """B_c+ -> K+ K- pi+ control channel"""
    line_alg = Bc_lines.make_BuToKpKmPip()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


######################################################
# Bc rare decay lines
######################################################


@register_line_builder(sprucing_lines)
@configurable
def BcToDpKpPim_sprucing_line(name="SpruceBandQ_BcToDpKpPim", prescale=1):
    """B_c+ -> D+ K+ pi- line"""
    line_alg = Bc_rare_decay_lines.make_BcToDpKpPim()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def BcToDstpKpPim_sprucing_line(name="SpruceBandQ_BcToDstpKpPim", prescale=1):
    """B_c+ -> D*(2010)+ K+ pi- line"""
    line_alg = Bc_rare_decay_lines.make_BcToDstpKpPim()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def BcToDspKpKm_sprucing_line(name="SpruceBandQ_BcToDspKpKm", prescale=1):
    """B_c+ -> D_s+ K+ K- line"""
    line_alg = Bc_rare_decay_lines.make_BcToDspKpKm()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def BcToDspPipPim_sprucing_line(name="SpruceBandQ_BcToDspPipPim", prescale=1):
    """B_c+ -> D_s+ pi+ pi- line"""
    line_alg = Bc_rare_decay_lines.make_BcToDspPipPim()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def BcToDspPpPm_sprucing_line(name="SpruceBandQ_BcToDspPpPm", prescale=1):
    """B_c+ -> D_s+ p+ p~- line"""
    line_alg = Bc_rare_decay_lines.make_BcToDspPpPm()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def BcToLcpPmKp_sprucing_line(name="SpruceBandQ_BcToLcpPmKp", prescale=1):
    """B_c+ -> Lambda_c+ p~- K+ line"""
    line_alg = Bc_rare_decay_lines.make_BcToLcpPmKp()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def BcToD0KpPipPim_sprucing_line(name="SpruceBandQ_BcToD0KpPipPim", prescale=1):
    """B_c+ -> D0 K+ pi+ pi- line"""
    line_alg = Bc_rare_decay_lines.make_BcToD0KpPipPim()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


######################################################
# comment out lines related to neutral particles and downstream tracks for now
######################################################


@register_line_builder(sprucing_lines)
@configurable
def XibToJpsiXi_sprucing_line(name="SpruceBandQ_XibToJpsiXi", prescale=1):
    """Xi_b- -> Xi- J/psi(1S)"""
    line_alg = b_to_jpsiX_lines.make_XibToJpsiXi_JpsiToMuMu(process=PROCESS)
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def XibToJpsiXiPi_sprucing_line(name="SpruceBandQ_XibToJpsiXiPi", prescale=1):
    """Xi_b0 -> Xi- J/psi(1S) pi+"""
    line_alg = b_to_jpsiX_lines.make_XibToJpsiXiPi_JpsiToMuMu(process=PROCESS)
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def OmegabToJpsiOmega_sprucing_line(name="SpruceBandQ_OmegabToJpsiOmega", prescale=1):
    """Omega_b- -> Omega- J/psi(1S)"""
    line_alg = b_to_jpsiX_lines.make_OmegabToJpsiOmega_JpsiToMuMu(process=PROCESS)
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToDs1Gamma_sprucing_line(name="SpruceBandQ_BcToDs1Gamma", prescale=1):
    """B_c+ -> D_s1(2536)+ gamma"""
    line_alg = Bc_lines.make_BcToDs1Gamma()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BcToDs1GammaWS_sprucing_line(name="SpruceBandQ_BcToDs1GammaWS", prescale=0.3):
    """B_c+ -> D_s1(2536)+ gamma wrong sign D_s1 decay"""
    line_alg = Bc_lines.make_BcToDs1GammaWS()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def XibstToBuLmd_sprucing_line(name="SpruceBandQ_XibstToBuLmd", prescale=1):
    """Xi_b*- -> B- Lambda0"""
    line_alg = Xib_to_BLmd.make_XibmToBuLmd(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=[
            "Hlt2Topo2BodyDecision",
            "Hlt2Topo3BodyDecision",
            "Hlt2BandQ_BuForSpectroscopyFullDecision",
        ],
    )


@register_line_builder(sprucing_lines)
@configurable
def XibstToBuLmdWS_sprucing_line(name="SpruceBandQ_XibstToBuLmdWS", prescale=1):
    """Wrong sign line for background study in Xi_b*- -> B- Lambda0 decay"""
    line_alg = Xib_to_BLmd.make_XibpToBuLmd(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=[
            "Hlt2Topo2BodyDecision",
            "Hlt2Topo3BodyDecision",
            "Hlt2BandQ_BuForSpectroscopyFullDecision",
        ],
    )


@register_line_builder(sprucing_lines)
@configurable
def Xibst0ToBdLmd_sprucing_line(name="SpruceBandQ_Xibst0ToBdLmd", prescale=1):
    """Xi_b*0 -> B0 Lambda0"""
    line_alg = Xib_to_BLmd.make_Xib0ToBdLmd(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        hlt2_filter_code=[
            "Hlt2Topo2BodyDecision",
            "Hlt2Topo3BodyDecision",
            "Hlt2BandQ_BdForSpectroscopyFullDecision",
        ],
    )


####################################################
## automatic creation of Hb -> psi + hadrons lines
###################################################


def make_spruce_lines(
    make_fun, line_dict=sprucing_lines, lines_to_add=None, hlt2_filter_lines=""
):
    if not lines_to_add:
        return
    for decay in lines_to_add:
        descriptor = lines_to_add[decay]  # or actually list of descriptors

        @register_line_builder(line_dict)
        def make_spruce_line(
            name="SpruceBandQ_%s" % decay,
            maker_name="make_%s" % decay,
            descriptor=descriptor,  # or actually list of descriptors
            make_fun=make_fun,
            prescale=1,
        ):
            try:
                line_alg = make_fun(name=maker_name, descriptor=descriptor)
            except TypeError:
                line_alg = make_fun(name=maker_name, descriptors=descriptor)

            return SpruceLine(
                name=name,
                hlt2_filter_code=hlt2_filter_lines,
                algs=make_prefilters() + [line_alg],
                prescale=prescale,
            )


auto_PsiX_lines = {}

for psi in psis_list:
    psi_name = psis_list[psi][0]

    # ------- psiH
    auto_PsiX_lines[f"{psi_name}Pi"] = [f"[B+ -> {psi} pi+]cc"]
    auto_PsiX_lines[f"{psi_name}K"] = [f"[B+ -> {psi} K+]cc"]
    auto_PsiX_lines[f"{psi_name}P"] = [f"[B+ -> {psi} p+]cc"]
    auto_PsiX_lines[f"{psi_name}Ks"] = [f"[B0 -> {psi} KS0]cc"]
    auto_PsiX_lines[f"{psi_name}Lambda"] = [f"[Lambda_b0 -> {psi} Lambda0]cc"]
    auto_PsiX_lines[f"{psi_name}Xi"] = [f"[Xi_b- -> {psi} Xi-]cc"]
    auto_PsiX_lines[f"{psi_name}Omega"] = [f"[Omega_b- -> {psi} Omega-]cc"]

    # ------- psiHH
    auto_PsiX_lines[f"{psi_name}PiPi"] = [
        f"[B_s0 -> {psi} pi+ pi-]cc",
        f"[B_s0 -> {psi} pi+ pi+]cc",
    ]
    auto_PsiX_lines[f"{psi_name}KPi"] = [
        f"[B0 -> {psi} K+ pi-]cc",
        f"[B0 -> {psi} K+ pi+]cc",
    ]
    auto_PsiX_lines[f"{psi_name}KK"] = [
        f"[B_s0 -> {psi} K+ K-]cc",
        f"[B_s0 -> {psi} K+ K+]cc",
    ]
    auto_PsiX_lines[f"{psi_name}PPi"] = [
        f"[Lambda_b0 -> {psi} p+ pi-]cc",
        f"[Lambda_b0 -> {psi} p+ pi+]cc",
    ]
    auto_PsiX_lines[f"{psi_name}PK"] = [
        f"[Lambda_b0 -> {psi} p+ K-]cc",
        f"[Lambda_b0 -> {psi} p+ K+]cc",
    ]
    auto_PsiX_lines[f"{psi_name}PP"] = [
        f"[B_s0 -> {psi} p+ p~-]cc",
        f"[B_s0 -> {psi} p+ p+]cc",
    ]
    auto_PsiX_lines[f"{psi_name}KsPi"] = [f"[B+ -> {psi} KS0 pi+]cc"]
    auto_PsiX_lines[f"{psi_name}KsK"] = [f"[B+ -> {psi} KS0 K+]cc"]
    auto_PsiX_lines[f"{psi_name}PKs"] = [f"[B+ -> {psi} p+ KS0]cc"]
    auto_PsiX_lines[f"{psi_name}KsKs"] = [f"[B0 -> {psi} KS0 KS0]cc"]
    auto_PsiX_lines[f"{psi_name}LambdaPi"] = [
        f"[Lambda_b0 -> {psi} Lambda0 pi+]cc",
        f"[Lambda_b0 -> {psi} Lambda0 pi-]cc",
    ]
    auto_PsiX_lines[f"{psi_name}LambdaK"] = [
        f"[Xi_b- -> {psi} Lambda0 K-]cc",
        f"[Xi_b- -> {psi} Lambda0 K+]cc",
    ]
    auto_PsiX_lines[f"{psi_name}LambdaP"] = [
        f"[B+ -> {psi} Lambda0 p+]cc",
        f"[B+ -> {psi} Lambda0 p~-]cc",
    ]
    auto_PsiX_lines[f"{psi_name}LambdaKs"] = [f"[Lambda_b0 -> {psi} Lambda0 KS0]cc"]
    auto_PsiX_lines[f"{psi_name}LambdaLambda"] = [
        f"[Lambda_b0 -> {psi} Lambda0 Lambda0]cc",
        f"[Lambda_b0 -> {psi} Lambda0 Lambda~0]cc",
    ]
    auto_PsiX_lines[f"{psi_name}XiPi"] = [
        f"[Xi_b0 -> {psi} Xi- pi+]cc",
        f"[Xi_b0 -> {psi} Xi- pi-]cc",
    ]
    auto_PsiX_lines[f"{psi_name}XiK"] = [
        f"[Xi_b0 -> {psi} Xi- K+]cc",
        f"[Xi_b0 -> {psi} Xi- K-]cc",
    ]
    auto_PsiX_lines[f"{psi_name}XiP"] = [
        f"[Xi_b0 -> {psi} Xi- p+]cc",
        f"[Xi_b0 -> {psi} Xi- p~-]cc",
    ]
    auto_PsiX_lines[f"{psi_name}OmegaPi"] = [
        f"[Omega_b- -> {psi} Omega- pi+]cc",
        f"[Omega_b- -> {psi} Omega- pi-]cc",
    ]
    auto_PsiX_lines[f"{psi_name}OmegaK"] = [
        f"[Omega_b- -> {psi} Omega- K+]cc",
        f"[Omega_b- -> {psi} Omega- K-]cc",
    ]
    auto_PsiX_lines[f"{psi_name}OmegaP"] = [
        f"[Omega_b- -> {psi} Omega- p+]cc",
        f"[Omega_b- -> {psi} Omega- p~-]cc",
    ]
    #

    # ------- psiHHH
    auto_PsiX_lines[f"{psi_name}PiPiPi"] = [
        f"[B+ -> {psi} pi+ pi+ pi-]cc",
        f"[B+ -> {psi} pi+ pi+ pi+]cc",
    ]
    auto_PsiX_lines[f"{psi_name}KPiPi"] = [
        f"[B+ -> {psi} K+ pi+ pi-]cc",
        f"[B+ -> {psi} K+ pi+ pi+]cc",
        f"[B+ -> {psi} K+ pi- pi-]cc",
    ]
    auto_PsiX_lines[f"{psi_name}KKPi"] = [
        f"[B+ -> {psi} K+ K- pi+]cc",
        f"[B+ -> {psi} K+ K+ pi+]cc",
        f"[B+ -> {psi} K+ K+ pi-]cc",
    ]
    auto_PsiX_lines[f"{psi_name}KKK"] = [
        f"[B+ -> {psi} K+ K+ K-]cc",
        f"[B+ -> {psi} K+ K+ K+]cc",
    ]
    auto_PsiX_lines[f"{psi_name}PKK"] = [
        f"[Xi_b- -> {psi} p+ K+ K-]cc",
        f"[Xi_b- -> {psi} p+ K+ K+]cc",
        f"[Xi_b- -> {psi} p+ K- K-]cc",
    ]
    auto_PsiX_lines[f"{psi_name}PPiPi"] = [
        f"[Xi_b- -> {psi} p+ pi+ pi-]cc",
        f"[Xi_b- -> {psi} p+ pi+ pi+]cc",
        f"[Xi_b- -> {psi} p+ pi- pi-]cc",
    ]
    auto_PsiX_lines[f"{psi_name}PKPi"] = [
        f"[Xi_b- -> {psi} p+ K+ pi-]cc",
        f"[Xi_b- -> {psi} p+ K+ pi+]cc",
        f"[Xi_b- -> {psi} p+ K- pi-]cc",
        f"[Xi_b- -> {psi} p+ K- pi+]cc",
    ]
    auto_PsiX_lines[f"{psi_name}PPPi"] = [
        f"[Xi_bc+ -> {psi} p+ p+ pi-]cc",
        f"[Xi_bc+ -> {psi} p+ p+ pi+]cc",
        f"[Xi_bc+ -> {psi} p+ p~- pi+]cc",
    ]  # for Hexa_b
    auto_PsiX_lines[f"{psi_name}PPP"] = [
        f"[Xi_bc+ -> {psi} p+ p+ p~-]cc",
        f"[Xi_bc+ -> {psi} p+ p+ p+]cc",
    ]  # for Hexa_b
    auto_PsiX_lines[f"{psi_name}PPK"] = [
        f"[Xi_bc+ -> {psi} p+ p+ K-]cc",
        f"[Xi_bc+ -> {psi} p+ p+ K+]cc",
        f"[Xi_bc+ -> {psi} p+ p~- K+]cc",
    ]  # for Hexa_b

    # with long-lived particles
    auto_PsiX_lines[f"{psi_name}KsKK"] = [
        f"[B0 -> {psi} KS0 K+ K-]cc",
        f"[B0 -> {psi} KS0 K+ K+]cc",
    ]
    auto_PsiX_lines[f"{psi_name}KsKPi"] = [
        f"[B0 -> {psi} KS0 K+ pi-]cc",
        f"[B0 -> {psi} KS0 K+ pi+]cc",
    ]
    auto_PsiX_lines[f"{psi_name}KsPiPi"] = [
        f"[B0 -> {psi} KS0 pi+ pi-]cc",
        f"[B0 -> {psi} KS0 pi+ pi+]cc",
    ]
    auto_PsiX_lines[f"{psi_name}LambdaKK"] = [
        f"[Lambda_b0 -> {psi} Lambda0 K+ K-]cc",
        f"[Lambda_b0 -> {psi} Lambda0 K+ K+]cc",
    ]
    auto_PsiX_lines[f"{psi_name}LambdaKPi"] = [
        f"[Lambda_b0 -> {psi} Lambda0 K+ pi-]cc",
        f"[Lambda_b0 -> {psi} Lambda0 K+ pi+]cc",
    ]
    auto_PsiX_lines[f"{psi_name}LambdaPiPi"] = [
        f"[Lambda_b0 -> {psi} Lambda0 pi+ pi-]cc",
        f"[Lambda_b0 -> {psi} Lambda0 pi+ pi+]cc",
    ]
    auto_PsiX_lines[f"{psi_name}XiPiPi"] = [
        f"[Xi_b- -> {psi} Xi- pi+ pi-]cc",
        f"[Xi_b- -> {psi} Xi- pi+ pi+]cc",
        f"[Xi_b- -> {psi} Xi- pi- pi-]cc",
    ]
    auto_PsiX_lines[f"{psi_name}XiKPi"] = [
        f"[Xi_b- -> {psi} Xi- K+ pi-]cc",
        f"[Xi_b- -> {psi} Xi- K+ pi+]cc",
        f"[Xi_b- -> {psi} Xi- K- pi+]cc",
        f"[Xi_b- -> {psi} Xi- K- pi-]cc",
    ]
    auto_PsiX_lines[f"{psi_name}XiKK"] = [
        f"[Xi_b- -> {psi} Xi- K+ K-]cc",
        f"[Xi_b- -> {psi} Xi- K+ K+]cc",
        f"[Xi_b- -> {psi} Xi- K- K-]cc",
    ]
    auto_PsiX_lines[f"{psi_name}OmegaPiPi"] = [
        f"[Omega_b- -> {psi} Omega- pi+ pi-]cc",
        f"[Omega_b- -> {psi} Omega- pi+ pi+]cc",
        f"[Omega_b- -> {psi} Omega- pi- pi-]cc",
    ]
    auto_PsiX_lines[f"{psi_name}OmegaKPi"] = [
        f"[Omega_b- -> {psi} Omega- K+ pi-]cc",
        f"[Omega_b- -> {psi} Omega- K+ pi+]cc",
        f"[Omega_b- -> {psi} Omega- K- pi-]cc",
    ]
    auto_PsiX_lines[f"{psi_name}OmegaKK"] = [
        f"[Omega_b- -> {psi} Omega- K+ K-]cc",
        f"[Omega_b- -> {psi} Omega- K+ K+]cc",
        f"[Omega_b- -> {psi} Omega- K- K-]cc",
    ]

    #

    # psiHHHH
    auto_PsiX_lines[f"{psi_name}PiPiPiPi"] = [
        f"[B0 -> {psi} pi+ pi+ pi- pi-]cc",
        f"[B0 -> {psi} pi+ pi+ pi+ pi-]cc",
        f"[B0 -> {psi} pi+ pi+ pi+ pi+]cc",
    ]
    auto_PsiX_lines[f"{psi_name}KPiPiPi"] = [
        f"[B0 -> {psi} K+ pi+ pi- pi-]cc",
        f"[B0 -> {psi} K+ pi+ pi+ pi-]cc",
        f"[B0 -> {psi} K+ pi- pi- pi-]cc",
        f"[B0 -> {psi} K+ pi+ pi+ pi+]cc",
    ]
    auto_PsiX_lines[f"{psi_name}KKPiPi"] = [
        f"[B0 -> {psi} K+ K- pi+ pi-]cc",
        f"[B0 -> {psi} K+ K- pi+ pi+]cc",
        f"[B0 -> {psi} K+ K- pi- pi-]cc",
        f"[B0 -> {psi} K+ K+ pi+ pi-]cc",
        f"[B0 -> {psi} K+ K+ pi+ pi+]cc",
        f"[B0 -> {psi} K+ K+ pi- pi-]cc",
    ]
    auto_PsiX_lines[f"{psi_name}KKKPi"] = [
        f"[B0 -> {psi} K+ K+ K- pi-]cc",
        f"[B0 -> {psi} K+ K+ K- pi+]cc",
        f"[B0 -> {psi} K+ K+ K+ pi-]cc",
        f"[B0 -> {psi} K+ K+ K+ pi+]cc",
    ]
    auto_PsiX_lines[f"{psi_name}KKKK"] = [
        f"[B0 -> {psi} K+ K+ K- K-]cc",
        f"[B0 -> {psi} K+ K+ K+ K-]cc",
        f"[B0 -> {psi} K+ K+ K+ K+]cc",
    ]

    auto_PsiX_lines[f"{psi_name}PPKPi"] = [
        f"[Xi_bc0 -> {psi} p+ p+ K- pi-]cc",  # for Hexa_b
        f"[Xi_bc0 -> {psi} p+ p+ K+ pi-]cc",
        f"[Xi_bc0 -> {psi} p+ p+ K- pi+]cc",
        f"[Xi_bc0 -> {psi} p+ p~- K- pi-]cc",
        f"[Xi_bc0 -> {psi} p+ p~- K- pi+]cc",
    ]

    # psiHHHHH
    auto_PsiX_lines[f"{psi_name}PiPiPiPiPi"] = [
        f"[B+ -> {psi} pi+ pi+ pi+ pi- pi-]cc",
        f"[B+ -> {psi} pi+ pi+ pi+ pi+ pi-]cc",
        f"[B+ -> {psi} pi+ pi+ pi+ pi+ pi+]cc",
    ]
    auto_PsiX_lines[f"{psi_name}KPiPiPiPi"] = [
        f"[B+ -> {psi} K+ pi+ pi+ pi- pi-]cc",
        f"[B+ -> {psi} K+ pi+ pi+ pi+ pi-]cc",
        f"[B+ -> {psi} K+ pi+ pi+ pi+ pi+]cc",
        f"[B+ -> {psi} K+ pi+ pi- pi- pi-]cc",
        f"[B+ -> {psi} K+ pi- pi- pi- pi-]cc",
    ]
    auto_PsiX_lines[f"{psi_name}KKPiPiPi"] = [
        f"[B+ -> {psi} K+ K- pi+ pi+ pi-]cc",
        f"[B+ -> {psi} K+ K- pi+ pi+ pi+]cc",
        f"[B+ -> {psi} K+ K- pi+ pi- pi-]cc",
        f"[B+ -> {psi} K+ K- pi- pi- pi-]cc",
        f"[B+ -> {psi} K+ K+ pi+ pi+ pi-]cc",
        f"[B+ -> {psi} K+ K+ pi+ pi+ pi+]cc",
        f"[B+ -> {psi} K+ K+ pi+ pi- pi-]cc",
        f"[B+ -> {psi} K+ K+ pi- pi- pi-]cc",
    ]

make_spruce_lines(
    make_fun=b_to_jpsiX_lines.make_HbToPsiX_PsiToMuMu_combination,
    line_dict=sprucing_lines,
    lines_to_add=auto_PsiX_lines,
    hlt2_filter_lines=hlt2_lines_detached_psi,
)

####################################################
## automatic creation of prompt X->B+hadrons lines
###################################################

BX_mother_id = "Upsilon(4S)"

auto_BX_lines = {}

auto_BX_lines["BeautyPi"] = [
    f"[ {BX_mother_id} -> B+ pi+]cc",
    f"[ {BX_mother_id} -> B+ pi-]cc",
    f"[ {BX_mother_id} -> B0 pi+]cc",
    f"[ {BX_mother_id} -> B0 pi-]cc",
    f"[ {BX_mother_id} -> B_s0 pi+]cc",
    f"[ {BX_mother_id} -> B_s0 pi-]cc",
    f"[ {BX_mother_id} -> B_c+ pi+]cc",
    f"[ {BX_mother_id} -> B_c+ pi-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 pi+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 pi-]cc",
    f"[ {BX_mother_id} -> Xi_b0 pi+]cc",
    f"[ {BX_mother_id} -> Xi_b0 pi-]cc",
    f"[ {BX_mother_id} -> Xi_b- pi+]cc",
    f"[ {BX_mother_id} -> Xi_b- pi-]cc",
    f"[ {BX_mother_id} -> Omega_b- pi+]cc",
    f"[ {BX_mother_id} -> Omega_b- pi-]cc",
]
auto_BX_lines["BeautyK"] = [
    f"[ {BX_mother_id} -> B+ K+]cc",
    f"[ {BX_mother_id} -> B+ K-]cc",
    f"[ {BX_mother_id} -> B0 K+]cc",
    f"[ {BX_mother_id} -> B0 K-]cc",
    f"[ {BX_mother_id} -> B_s0 K+]cc",
    f"[ {BX_mother_id} -> B_s0 K-]cc",
    f"[ {BX_mother_id} -> B_c+ K+]cc",
    f"[ {BX_mother_id} -> B_c+ K-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 K+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 K-]cc",
    f"[ {BX_mother_id} -> Xi_b0 K+]cc",
    f"[ {BX_mother_id} -> Xi_b0 K-]cc",
    f"[ {BX_mother_id} -> Xi_b- K+]cc",
    f"[ {BX_mother_id} -> Xi_b- K-]cc",
    f"[ {BX_mother_id} -> Omega_b- K+]cc",
    f"[ {BX_mother_id} -> Omega_b- K-]cc",
]
auto_BX_lines["BeautyP"] = [
    f"[ {BX_mother_id} -> B+ p+]cc",
    f"[ {BX_mother_id} -> B+ p~-]cc",
    f"[ {BX_mother_id} -> B0 p+]cc",
    f"[ {BX_mother_id} -> B0 p~-]cc",
    f"[ {BX_mother_id} -> B_s0 p+]cc",
    f"[ {BX_mother_id} -> B_s0 p~-]cc",
    f"[ {BX_mother_id} -> B_c+ p+]cc",
    f"[ {BX_mother_id} -> B_c+ p~-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 p+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 p~-]cc",
    f"[ {BX_mother_id} -> Xi_b0 p+]cc",
    f"[ {BX_mother_id} -> Xi_b0 p~-]cc",
    f"[ {BX_mother_id} -> Xi_b- p+]cc",
    f"[ {BX_mother_id} -> Xi_b- p~-]cc",
    f"[ {BX_mother_id} -> Omega_b- p+]cc",
    f"[ {BX_mother_id} -> Omega_b- p~-]cc",
]
auto_BX_lines["BeautyKs"] = [
    f"[ {BX_mother_id} -> B+ KS0]cc",
    f"[ {BX_mother_id} -> B0 KS0]cc",
    f"[ {BX_mother_id} -> B_s0 KS0]cc",
    f"[ {BX_mother_id} -> B_c+ KS0]cc",
    f"[ {BX_mother_id} -> Lambda_b0 KS0]cc",
    f"[ {BX_mother_id} -> Xi_b0 KS0]cc",
    f"[ {BX_mother_id} -> Xi_b- KS0]cc",
    f"[ {BX_mother_id} -> Omega_b- KS0]cc",
]
auto_BX_lines["BeautyLambda"] = [
    f"[ {BX_mother_id} -> B+ Lambda0]cc",
    f"[ {BX_mother_id} -> B+ Lambda~0]cc",
    f"[ {BX_mother_id} -> B0 Lambda0]cc",
    f"[ {BX_mother_id} -> B0 Lambda~0]cc",
    f"[ {BX_mother_id} -> B_s0 Lambda0]cc",
    f"[ {BX_mother_id} -> B_s0 Lambda~0]cc",
    f"[ {BX_mother_id} -> B_c+ Lambda0]cc",
    f"[ {BX_mother_id} -> B_c+ Lambda~0]cc",
    f"[ {BX_mother_id} -> Lambda_b0 Lambda0]cc",
    f"[ {BX_mother_id} -> Lambda_b0 Lambda~0]cc",
    f"[ {BX_mother_id} -> Xi_b0 Lambda0]cc",
    f"[ {BX_mother_id} -> Xi_b0 Lambda~0]cc",
    f"[ {BX_mother_id} -> Xi_b- Lambda0]cc",
    f"[ {BX_mother_id} -> Xi_b- Lambda~0]cc",
    f"[ {BX_mother_id} -> Omega_b- Lambda0]cc",
    f"[ {BX_mother_id} -> Omega_b- Lambda~0]cc",
]
auto_BX_lines["BeautyXi"] = [
    f"[ {BX_mother_id} -> B+ Xi-]cc",
    f"[ {BX_mother_id} -> B+ Xi~+]cc",
    f"[ {BX_mother_id} -> B0 Xi-]cc",
    f"[ {BX_mother_id} -> B0 Xi~+]cc",
    f"[ {BX_mother_id} -> B_s0 Xi-]cc",
    f"[ {BX_mother_id} -> B_s0 Xi~+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 Xi-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 Xi~+]cc",
]
auto_BX_lines["BeautyOmega"] = [
    f"[ {BX_mother_id} -> B+ Omega-]cc",
    f"[ {BX_mother_id} -> B+ Omega~+]cc",
    f"[ {BX_mother_id} -> B0 Omega-]cc",
    f"[ {BX_mother_id} -> B0 Omega~+]cc",
    f"[ {BX_mother_id} -> B_s0 Omega-]cc",
    f"[ {BX_mother_id} -> B_s0 Omega~+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 Omega-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 Omega~+]cc",
]

auto_BX_lines["BeautyPiPi"] = [
    f"[ {BX_mother_id} -> B+ pi+ pi-]cc",
    f"[ {BX_mother_id} -> B+ pi+ pi+]cc",
    f"[ {BX_mother_id} -> B+ pi- pi-]cc",
    f"[ {BX_mother_id} -> B0 pi+ pi-]cc",
    f"[ {BX_mother_id} -> B0 pi+ pi+]cc",
    f"[ {BX_mother_id} -> B0 pi- pi-]cc",
    f"[ {BX_mother_id} -> B_s0 pi+ pi-]cc",
    f"[ {BX_mother_id} -> B_s0 pi+ pi+]cc",
    f"[ {BX_mother_id} -> B_s0 pi- pi-]cc",
    f"[ {BX_mother_id} -> B_c+ pi+ pi+]cc",
    f"[ {BX_mother_id} -> B_c+ pi+ pi-]cc",
    f"[ {BX_mother_id} -> B_c- pi- pi-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 pi+ pi-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 pi+ pi+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 pi- pi-]cc",
    f"[ {BX_mother_id} -> Xi_b0 pi+ pi-]cc",
    f"[ {BX_mother_id} -> Xi_b0 pi+ pi+]cc",
    f"[ {BX_mother_id} -> Xi_b0 pi- pi-]cc",
    f"[ {BX_mother_id} -> Xi_b- pi+ pi-]cc",
    f"[ {BX_mother_id} -> Xi_b- pi+ pi+]cc",
    f"[ {BX_mother_id} -> Xi_b- pi- pi-]cc",
    f"[ {BX_mother_id} -> Omega_b- pi+ pi-]cc",
    f"[ {BX_mother_id} -> Omega_b- pi+ pi+]cc",
    f"[ {BX_mother_id} -> Omega_b- pi- pi-]cc",
]
auto_BX_lines["BeautyKK"] = [
    f"[ {BX_mother_id} -> B+ K+ K-]cc",
    f"[ {BX_mother_id} -> B+ K+ K+]cc",
    f"[ {BX_mother_id} -> B+ K- K-]cc",
    f"[ {BX_mother_id} -> B+ K- K+]cc",
    f"[ {BX_mother_id} -> B0 K+ K-]cc",
    f"[ {BX_mother_id} -> B0 K+ K+]cc",
    f"[ {BX_mother_id} -> B0 K- K-]cc",
    f"[ {BX_mother_id} -> B0 K- K+]cc",
    f"[ {BX_mother_id} -> B_s0 K+ K-]cc",
    f"[ {BX_mother_id} -> B_s0 K+ K+]cc",
    f"[ {BX_mother_id} -> B_s0 K- K-]cc",
    f"[ {BX_mother_id} -> B_s0 K- K+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 K+ K-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 K+ K+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 K- K-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 K- K+]cc",
]
auto_BX_lines["BeautyKPi"] = [
    f"[ {BX_mother_id} -> B+ K- pi+]cc",
    f"[ {BX_mother_id} -> B+ K+ pi-]cc",
    f"[ {BX_mother_id} -> B+ K- pi-]cc",
    f"[ {BX_mother_id} -> B+ K+ pi+]cc",
    f"[ {BX_mother_id} -> B0 K- pi+]cc",
    f"[ {BX_mother_id} -> B0 K+ pi-]cc",
    f"[ {BX_mother_id} -> B0 K- pi-]cc",
    f"[ {BX_mother_id} -> B0 K+ pi+]cc",
    f"[ {BX_mother_id} -> B_s0 K- pi+]cc",
    f"[ {BX_mother_id} -> B_s0 K+ pi-]cc",
    f"[ {BX_mother_id} -> B_s0 K- pi-]cc",
    f"[ {BX_mother_id} -> B_s0 K+ pi+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 K- pi+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 K+ pi-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 K- pi-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 K+ pi+]cc",
]

auto_BX_lines["BeautyPPi"] = [
    f"[ {BX_mother_id} -> B+ p+ pi-]cc",  # for Pb [b~udud]
    f"[ {BX_mother_id} -> B+ p~- pi+]cc",
    f"[ {BX_mother_id} -> B+ p+ pi+]cc",
    f"[ {BX_mother_id} -> B+ p~- pi-]cc",
    f"[ {BX_mother_id} -> B0 p+ pi-]cc",
    f"[ {BX_mother_id} -> B0 p~- pi+]cc",
    f"[ {BX_mother_id} -> B0 p+ pi+]cc",
    f"[ {BX_mother_id} -> B0 p~- pi-]cc",
    f"[ {BX_mother_id} -> B_s0 p+ pi-]cc",
    f"[ {BX_mother_id} -> B_s0 p~- pi+]cc",
    f"[ {BX_mother_id} -> B_s0 p+ pi+]cc",
    f"[ {BX_mother_id} -> B_s0 p~- pi-]cc",
    f"[ {BX_mother_id} -> Lambda_b0 p+ pi-]cc",  # for Hb [bqudud]
    f"[ {BX_mother_id} -> Lambda_b0 p~- pi+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 p+ pi+]cc",
    f"[ {BX_mother_id} -> Lambda_b0 p~- pi-]cc",
    f"[ {BX_mother_id} -> Xi_b0 p+ pi-]cc",  # for Hbs [bsudud]
    f"[ {BX_mother_id} -> Xi_b0 p~- pi+]cc",
    f"[ {BX_mother_id} -> Xi_b0 p+ pi+]cc",
    f"[ {BX_mother_id} -> Xi_b0 p~- pi-]cc",
    f"[ {BX_mother_id} -> Xi_b- p+ pi-]cc",  # for Hbs [bsudud]
    f"[ {BX_mother_id} -> Xi_b- p~- pi+]cc",
    f"[ {BX_mother_id} -> Xi_b- p+ pi+]cc",
    f"[ {BX_mother_id} -> Xi_b- p~- pi-]cc",
]

## for strongly-decaying Tbc [bcu~d~] or Tbcs [bcs~u~]/[bcs~d~]
auto_BX_lines["B0D0"] = [
    f"[ {BX_mother_id} -> B~0 D0]cc",
    f"[ {BX_mother_id} -> B~0 D~0]cc",
]
auto_BX_lines["BmDp"] = [
    f"[ {BX_mother_id} -> B- D+]cc",
    f"[ {BX_mother_id} -> B- D-]cc",
]
auto_BX_lines["B0D0Pi"] = [
    f"[ {BX_mother_id} -> B~0 D0 pi+]cc",
    f"[ {BX_mother_id} -> B~0 D0 pi-]cc",
    f"[ {BX_mother_id} -> B~0 D~0 pi+]cc",
    f"[ {BX_mother_id} -> B~0 D~0 pi-]cc",
]
auto_BX_lines["BmD0Pi"] = [
    f"[ {BX_mother_id} -> B- D0 pi+]cc",
    f"[ {BX_mother_id} -> B- D0 pi-]cc",
    f"[ {BX_mother_id} -> B- D~0 pi+]cc",
    f"[ {BX_mother_id} -> B- D~0 pi-]cc",
]
auto_BX_lines["B0DpPi"] = [
    f"[ {BX_mother_id} -> B~0 D+ pi- ]cc",
    f"[ {BX_mother_id} -> B~0 D- pi+]cc",
    f"[ {BX_mother_id} -> B~0 D+ pi+]cc",
    f"[ {BX_mother_id} -> B~0 D- pi-]cc",
]

auto_BX_lines["B0Ds"] = [
    f"[ {BX_mother_id} -> B~0 D_s+]cc",
    f"[ {BX_mother_id} -> B~0 D_s-]cc",
]
auto_BX_lines["BmDs"] = [
    f"[ {BX_mother_id} -> B- D_s+]cc",
    f"[ {BX_mother_id} -> B- D_s-]cc",
]
auto_BX_lines["BsD0"] = [
    f"[ {BX_mother_id} -> B_s~0 D0]cc",
    f"[ {BX_mother_id} -> B_s~0 D~0]cc",
]
auto_BX_lines["BsDp"] = [
    f"[ {BX_mother_id} -> B_s~0 D+]cc",
    f"[ {BX_mother_id} -> B_s~0 D-]cc",
]
auto_BX_lines["BsD0Pi"] = [
    f"[ {BX_mother_id} -> B_s~0 D0 pi+]cc",
    f"[ {BX_mother_id} -> B_s~0 D0 pi-]cc",
    f"[ {BX_mother_id} -> B_s~0 D~0 pi+]cc",
    f"[ {BX_mother_id} -> B_s~0 D~0 pi-]cc",
]

## for strongly-decaying Pb [b~udud] / Pbs [b~suud] / [b~sudd]
# covered by BeautyP, BeautyLambda, BeautyPPi lines

hlt2_lines_b_for_spectroscopy = [
    "Hlt2BandQ_BuForSpectroscopyFullDecision",
    "Hlt2BandQ_BdForSpectroscopyFullDecision",
    "Hlt2BandQ_BsForSpectroscopyFullDecision",
    "Hlt2BandQ_LbForSpectroscopyFullDecision",
    "Hlt2BandQ_BcForSpectroscopyFullDecision",
    "Hlt2BandQ_XibmForSpectroscopyFullDecision",
    "Hlt2BandQ_Xib0ForSpectroscopyFullDecision",
    "Hlt2BandQ_OmegabForSpectroscopyFullDecision",
]

make_spruce_lines(
    make_fun=bx.make_BX_combination,
    line_dict=sprucing_lines,
    lines_to_add=auto_BX_lines,
    hlt2_filter_lines=hlt2_lines_b_for_spectroscopy,
)

## SemiLeptonic modes for B
auto_BX_SL_lines = {}
for line in auto_BX_lines:
    auto_BX_SL_lines[line + "_SL"] = auto_BX_lines[line]

hlt2_lines_b_for_spectroscopy_SL = [
    "Hlt2BandQ_BudForSpectroscopySLFullDecision",
    "Hlt2BandQ_BsForSpectroscopySLFullDecision",
    "Hlt2BandQ_LbForSpectroscopySLFullDecision",
    "Hlt2BandQ_BcForSpectroscopySLFullDecision",
    "Hlt2BandQ_XibForSpectroscopySLFullDecision",
    "Hlt2BandQ_OmegabForSpectroscopySLFullDecision",
]

make_spruce_lines(
    make_fun=bx.make_BX_SL_combination,
    line_dict=sprucing_lines,
    lines_to_add=auto_BX_SL_lines,
    hlt2_filter_lines=hlt2_lines_b_for_spectroscopy_SL,
)

####################### Detached BX
BX_mother_id = "B_c+"
auto_BX_detached_lines = {}

## Bc decays for study of B* excited states
auto_BX_detached_lines["BcToBpKPi"] = [
    f"[ {BX_mother_id} -> B+ K+ pi-]cc",
    f"[ {BX_mother_id} -> B+ K- pi+]cc",
    f"[ {BX_mother_id} -> B+ K- pi-]cc",
    f"[ {BX_mother_id} -> B+ K+ pi+]cc",
]
auto_BX_detached_lines["BcToBpKs"] = [f"[ {BX_mother_id} -> B+ KS0]cc"]
auto_BX_detached_lines["BcToB0KPiPi"] = [
    f"[ {BX_mother_id} -> B0 K+ pi+ pi-]cc",
    f"[ {BX_mother_id} -> B0 K- pi+ pi-]cc",
    f"[ {BX_mother_id} -> B0 K+ pi- pi-]cc",
    f"[ {BX_mother_id} -> B0 K- pi+ pi+]cc",
]
auto_BX_detached_lines["BcToB0KsPi"] = [
    f"[ {BX_mother_id} -> B0 KS0 pi+]cc",
    f"[ {BX_mother_id} -> B0 KS0 pi-]cc",
]
auto_BX_detached_lines["BcToBsPi"] = [
    f"[ {BX_mother_id} -> B_s0 pi+]cc",
    f"[ {BX_mother_id} -> B_s0 pi-]cc",
]
auto_BX_detached_lines["BcToBsPiPiPi"] = [
    f"[ {BX_mother_id} -> B_s0 pi+ pi+ pi-]cc",
    f"[ {BX_mother_id} -> B_s0 pi+ pi- pi-]cc",
    f"[ {BX_mother_id} -> B_s0 pi+ pi+ pi+]cc",
    f"[ {BX_mother_id} -> B_s0 pi- pi- pi-]cc",
]

## searches for Xi_bc
BX_mother_id = "Xi_bc0"
auto_BX_detached_lines["XibcToLbPi"] = [
    "[ Xi_bc+ -> Lambda_b0 pi+]cc",
    "[ Xi_bc+ -> Lambda_b0 pi-]cc",
]
auto_BX_detached_lines["XibcToXibzPi"] = [
    "[ Xi_bc+ -> Xi_b0 pi+]cc",
    "[ Xi_bc+ -> Xi_b0 pi-]cc",
]
auto_BX_detached_lines["XibcToXibmPi"] = [
    "[ Xi_bc0 -> Xi_b- pi+]cc",
    "[ Xi_bc0 -> Xi_b- pi-]cc",
]
auto_BX_detached_lines["XibcToLbKPi"] = [
    "[ Xi_bc0 -> Lambda_b0 K- pi+]cc",
    "[ Xi_bc0 -> Lambda_b0 K+ pi-]cc",
    "[ Xi_bc0 -> Lambda_b0 K- pi-]cc",
    "[ Xi_bc0 -> Lambda_b0 K+ pi+]cc",
]
auto_BX_detached_lines["XibcToLbKPiPiPi"] = [
    "[ Xi_bc0 -> Lambda_b0 K- pi+ pi+ pi-]cc",
    "[ Xi_bc0 -> Lambda_b0 K+ pi+ pi- pi-]cc",
]
auto_BX_detached_lines["XibcToB0PKPi"] = [
    "[ Xi_bc0 -> B0 p+ K- pi+]cc",
    "[ Xi_bc0 -> B0 p+ K+ pi-]cc",
    "[ Xi_bc0 -> B0 p~- K- pi+]cc",
    "[ Xi_bc0 -> B0 p~- K+ pi-]cc",
]
auto_BX_detached_lines["XibcToB0LambdaPi"] = [
    "[ Xi_bc0 -> B0 Lambda0 pi+]cc",
    "[ Xi_bc0 -> B0 Lambda0 pi-]cc",
    "[ Xi_bc0 -> B0 Lambda~0 pi+]cc",
    "[ Xi_bc0 -> B0 Lambda~0 pi-]cc",
]
auto_BX_detached_lines["XibcToXibzMu"] = [
    "[ Xi_bc+ -> Xi_b0 mu+]cc",
    "[ Xi_bc+ -> Xi_b0 mu-]cc",
]
auto_BX_detached_lines["XibcToXibmMu"] = [
    "[ Xi_bc0 -> Xi_b- mu+]cc",
    "[ Xi_bc0 -> Xi_b- mu-]cc",
]
auto_BX_detached_lines["XibcToLbMu"] = [
    "[ Xi_bc0 -> Lambda_b0 mu+]cc",
    "[ Xi_bc0 -> Lambda_b0 mu-]cc",
]
auto_BX_detached_lines["XibcToLbKMu"] = [
    "[ Xi_bc0 -> Lambda_b0 K- mu+]cc",
    "[ Xi_bc0 -> Lambda_b0 K+ mu-]cc",
    "[ Xi_bc0 -> Lambda_b0 K- mu-]cc",
    "[ Xi_bc0 -> Lambda_b0 K+ mu+]cc",
]

## searches for T_bc
auto_BX_detached_lines["TbcToB0bKPi"] = [
    "[ Xi_bc0 -> B~0 K- pi+]cc",
    "[ Xi_bc0 -> B~0 K+ pi-]cc",
    "[ Xi_bc0 -> B~0 K+ pi+]cc",
    "[ Xi_bc0 -> B~0 K- pi-]cc",
]
auto_BX_detached_lines["TbcToBmKPiPi"] = [
    "[ Xi_bc0 -> B- K- pi+ pi+]cc",
    "[ Xi_bc0 -> B- K- pi+ pi-]cc",
    "[ Xi_bc0 -> B- K+ pi- pi-]cc",
    "[ Xi_bc0 -> B- K+ pi+ pi-]cc",
]
auto_BX_detached_lines["TbcToB0bKPiPiPi"] = [
    "[ Xi_bc0 -> B~0 K- pi+ pi+ pi-]cc",
    "[ Xi_bc0 -> B~0 K+ pi+ pi+ pi-]cc",
    "[ Xi_bc0 -> B~0 K- pi+ pi- pi-]cc",
    "[ Xi_bc0 -> B~0 K+ pi+ pi- pi-]cc",
]
auto_BX_detached_lines["TbcToB0bKMu"] = [
    "[ Xi_bc0 -> B~0 K- mu+]cc",
    "[ Xi_bc0 -> B~0 K+ mu-]cc",
    "[ Xi_bc0 -> B~0 K+ mu+]cc",
    "[ Xi_bc0 -> B~0 K- mu-]cc",
]
auto_BX_detached_lines["TbcToBmKPiMu"] = [
    "[ Xi_bc0 -> B- K- pi+ mu+]cc",
    "[ Xi_bc0 -> B- K- pi+ mu-]cc",
    "[ Xi_bc0 -> B- K+ pi- mu+]cc",
    "[ Xi_bc0 -> B- K+ pi+ mu-]cc",
]

make_spruce_lines(
    make_fun=bx.make_BX_detached_combination,
    line_dict=sprucing_lines,
    lines_to_add=auto_BX_detached_lines,
    hlt2_filter_lines=hlt2_lines_b_for_spectroscopy,
)

## SemiLeptonic modes for B
auto_BX_detached_SL_lines = {}
for line in auto_BX_detached_lines:
    auto_BX_detached_SL_lines[line + "_SL"] = auto_BX_detached_lines[line]

make_spruce_lines(
    make_fun=bx.make_BX_detached_SL_combination,
    line_dict=sprucing_lines,
    lines_to_add=auto_BX_detached_SL_lines,
    hlt2_filter_lines=hlt2_lines_b_for_spectroscopy_SL,
)


#################################################################################
## PersistReco=True bSpectroscopy sprucing lines to cover physics channels not considered above
################################################################################
@register_line_builder(sprucing_lines)
@configurable
def Bu_for_spectroscopy_sprucing_line(name="SpruceBandQ_BuForSpectroscopy", prescale=1):
    line_alg = b_for_spectroscopy.make_Bu(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_BuForSpectroscopyFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def Bd_for_spectroscopy_sprucing_line(name="SpruceBandQ_BdForSpectroscopy", prescale=1):
    line_alg = b_for_spectroscopy.make_Bd(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_BdForSpectroscopyFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def Bs_for_spectroscopy_sprucing_line(name="SpruceBandQ_BsForSpectroscopy", prescale=1):
    line_alg = b_for_spectroscopy.make_Bs(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_BsForSpectroscopyFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def Lb_for_spectroscopy_sprucing_line(name="SpruceBandQ_LbForSpectroscopy", prescale=1):
    line_alg = b_for_spectroscopy.make_Lb(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_LbForSpectroscopyFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def Bc_for_spectroscopy_sprucing_line(name="SpruceBandQ_BcForSpectroscopy", prescale=1):
    line_alg = b_for_spectroscopy.make_Bc(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_BcForSpectroscopyFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def Xibm_for_spectroscopy_sprucing_line(
    name="SpruceBandQ_XibmForSpectroscopy", prescale=1
):
    line_alg = b_for_spectroscopy.make_Xibm(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(sprucing_lines)
@configurable
def Xib0_for_spectroscopy_sprucing_line(
    name="SpruceBandQ_Xib0ForSpectroscopy", prescale=1
):
    line_alg = b_for_spectroscopy.make_Xib0(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(sprucing_lines)
@configurable
def Omegab_for_spectroscopy_sprucing_line(
    name="SpruceBandQ_OmegabForSpectroscopy", prescale=1
):
    line_alg = b_for_spectroscopy.make_Omegab(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_OmegabForSpectroscopyFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def Bud_for_spectroscopy_SL_sprucing_line(
    name="SpruceBandQ_BudForSpectroscopySL", prescale=1
):
    line_alg = b_for_spectroscopy.make_Bud_SL(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_BudForSpectroscopySLFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def Bs_for_spectroscopy_SL_sprucing_line(
    name="SpruceBandQ_BsForSpectroscopySL", prescale=1
):
    line_alg = b_for_spectroscopy.make_Bs_SL(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_BsForSpectroscopySLFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def Lb_for_spectroscopy_SL_sprucing_line(
    name="SpruceBandQ_LbForSpectroscopySL", prescale=1
):
    line_alg = b_for_spectroscopy.make_Lb_SL(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_LbForSpectroscopySLFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def Bc_for_spectroscopy_SL_sprucing_line(
    name="SpruceBandQ_BcForSpectroscopySL", prescale=1
):
    line_alg = b_for_spectroscopy.make_Bc_SL(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_BcForSpectroscopySLFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def Xib_for_spectroscopy_SL_sprucing_line(
    name="SpruceBandQ_XibForSpectroscopySL", prescale=1
):
    line_alg = b_for_spectroscopy.make_Xib_SL(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_XibForSpectroscopySLFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def Omegab_for_spectroscopy_SL_sprucing_line(
    name="SpruceBandQ_OmegabForSpectroscopySL", prescale=1
):
    line_alg = b_for_spectroscopy.make_Omegab_SL(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_OmegabForSpectroscopySLFullDecision"],
    )


########################################################
## Create sprucing lines to read Tcc output
########################################################
@register_line_builder(sprucing_lines)
@configurable
def tcc_d0tohh_sprucing_line(name="SpruceBandQ_tcc_d0tohh", prescale=1):
    line_alg = tcc_exclusive.make_tcc_d0tohh(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=False,
    )


@register_line_builder(sprucing_lines)
@configurable
def tcc_d0tohhhh_sprucing_line(name="SpruceBandQ_tcc_d0tohhhh", prescale=1):
    line_alg = tcc_exclusive.make_tcc_d0tohhhh(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=False,
        hlt2_filter_code=[
            "Hlt2Topo2BodyDecision",
            "Hlt2Topo3BodyDecision",
            "Hlt2BandQ_DoubleCharmSameSignFullDecision",
            "Hlt2BandQ_DoubleCharm_D0ToHHHHFullDecision",
        ],
    )


@register_line_builder(sprucing_lines)
@configurable
def tcc_d0tohh_d0tohhhh_sprucing_line(
    name="SpruceBandQ_tcc_d0tohh_d0tohhhh", prescale=1
):
    line_alg = tcc_exclusive.make_tcc_d0tohh_d0tohhhh(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=False,
        hlt2_filter_code=[
            "Hlt2Topo2BodyDecision",
            "Hlt2Topo3BodyDecision",
            "Hlt2BandQ_DoubleCharmSameSignFullDecision",
            "Hlt2BandQ_DoubleCharm_D0ToHHHH_D0ToHHFullDecision",
        ],
    )


########################################################
## Create sprucing lines to read doubleCharm hlt2 output
########################################################
@register_line_builder(sprucing_lines)
@configurable
def doublecharm_samesign_sprucing_line(
    name="SpruceBandQ_DoubleCharmSameSign", prescale=1
):
    line_alg = doublecharm.make_doublecharm_samesign(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=[
            "Hlt2Topo2BodyDecision",
            "Hlt2Topo3BodyDecision",
            "Hlt2BandQ_DoubleCharmSameSignFullDecision",
            "Hlt2BandQ_DoubleCharm_D0ToHHHHFullDecision",
            "Hlt2BandQ_DoubleCharm_D0ToHHHH_D0ToHHFullDecision",
        ],
    )


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_oppositesign_sprucing_line(
    name="SpruceBandQ_DoubleCharmOppositeSign", prescale=1
):
    line_alg = doublecharm.make_doublecharm_oppositesign(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=[
            "Hlt2Topo2BodyDecision",
            "Hlt2Topo3BodyDecision",
            "Hlt2BandQ_DoubleCharmOppositeSignFullDecision",
            "Hlt2BandQ_DoubleCharm_D0ToHHHHFullDecision",
            "Hlt2BandQ_DoubleCharm_D0ToHHHH_D0ToHHFullDecision",
        ],
    )


@register_line_builder(sprucing_lines)
@configurable
def b_to_doublecharm_oppositesign_sprucing_line(
    name="SpruceBandQ_B2XcXc", prescale=1, B_MVA_cut=0.1
):
    line_alg = b_to_doublecharm.make_B2XcXc_oppositesign(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=False,
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_D0ToHH_sprucing_line(name="SpruceBandQ_DoubleCharm_D0ToHH", prescale=1):
    line_alg = doublecharm.make_doublecharm_D0ToHH(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DoubleCharm_D0ToHHFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_D0ToKsLLHH_D0ToHH_sprucing_line(
    name="SpruceBandQ_DoubleCharm_D0ToKsLLHH_D0ToHH", prescale=1
):
    line_alg = doublecharm.make_doublecharm_D0ToKsLLHH_D0ToHH(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DoubleCharm_D0ToKsLLHH_D0ToHHFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_D0ToKsDDHH_D0ToHH_sprucing_line(
    name="SpruceBandQ_DoubleCharm_D0ToKsDDHH_D0ToHH", prescale=1
):
    line_alg = doublecharm.make_doublecharm_D0ToKsDDHH_D0ToHH(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DoubleCharm_D0ToKsDDHH_D0ToHHFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_D0ToKsLLHH_sprucing_line(
    name="SpruceBandQ_DoubleCharm_D0ToKsLLHH", prescale=1
):
    line_alg = doublecharm.make_doublecharm_D0ToKsLLHH(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DoubleCharm_D0ToKsLLHHFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_D0ToKsLLHH_D0ToKsDDHH_sprucing_line(
    name="SpruceBandQ_DoubleCharm_D0ToKsLLHH_D0ToKsDDHH", prescale=1
):
    line_alg = doublecharm.make_doublecharm_D0ToKsLLHH_D0ToKsDDHH(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DoubleCharm_D0ToKsLLHH_D0ToKsDDHHFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_D0ToKsDDHH_sprucing_line(
    name="SpruceBandQ_DoubleCharm_D0ToKsDDHH", prescale=1
):
    line_alg = doublecharm.make_doublecharm_D0ToKsDDHH(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DoubleCharm_D0ToKsDDHHFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_D0ToHHHH_D0ToHH_sprucing_line(
    name="SpruceBandQ_DoubleCharm_D0ToHHHH_D0ToHH", prescale=1
):
    line_alg = doublecharm.make_doublecharm_D0ToHHHH_D0ToHH(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DoubleCharm_D0ToHHHH_D0ToHHFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_D0ToHHHH_D0ToKsLLHH_sprucing_line(
    name="SpruceBandQ_DoubleCharm_D0ToHHHH_D0ToKsLLHH", prescale=1
):
    line_alg = doublecharm.make_doublecharm_D0ToHHHH_D0ToKsLLHH(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DoubleCharm_D0ToHHHH_D0ToKsLLHHFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_D0ToHHHH_D0ToKsDDHH_sprucing_line(
    name="SpruceBandQ_DoubleCharm_D0ToHHHH_D0ToKsDDHH", prescale=1
):
    line_alg = doublecharm.make_doublecharm_D0ToHHHH_D0ToKsDDHH(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DoubleCharm_D0ToHHHH_D0ToKsDDHHFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def doublecharm_D0ToHHHH_sprucing_line(
    name="SpruceBandQ_DoubleCharm_D0ToHHHH", prescale=1
):
    line_alg = doublecharm.make_doublecharm_D0ToHHHH(process=PROCESS)
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code=["Hlt2BandQ_DoubleCharm_D0ToHHHHFullDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def B0ToLcLcbarKsLL_sprucing_line(name="SpruceBandQ_B0ToLcLcbarKsLL", prescale=1):
    line_alg = b_to_cch.make_B0ToLcLcbarKsLL()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def B0ToLcLcbarKsDD_sprucing_line(name="SpruceBandQ_B0ToLcLcbarKsDD", prescale=1):
    line_alg = b_to_cch.make_B0ToLcLcbarKsDD()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToLcLcbarPip_sprucing_line(name="SpruceBandQ_BuToLcLcbarPip", prescale=1):
    line_alg = b_to_cch.make_BuToLcLcbarPip()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def B0ToLcLcbarKmPip_sprucing_line(name="SpruceBandQ_B0ToLcLcbarKmPip", prescale=1):
    line_alg = b_to_cch.make_B0ToLcLcbarKmPip()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def B0ToLcXic0barPim_sprucing_line(name="SpruceBandQ_B0ToLcXic0barPim", prescale=1):
    line_alg = b_to_cch.make_B0ToLcXic0barPim()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BsToLcXic0barKm_sprucing_line(name="SpruceBandQ_BsToLcXic0barKm", prescale=1):
    line_alg = b_to_cch.make_BsToLcXic0barKm()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BsToDspD0barKm_sprucing_line(name="SpruceBandQ_BsToDspD0barKm", prescale=1):
    line_alg = b_to_cch.make_BsToDspD0barKm()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def Lb0ToDmXic0Pip_sprucing_line(name="SpruceBandQ_Lb0ToDmXic0Pip", prescale=1):
    line_alg = b_to_cch.make_Lb0ToDmXic0Pip()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def Lb0ToD0barXicpPim_sprucing_line(name="SpruceBandQ_Lb0ToD0barXicpPim", prescale=1):
    line_alg = b_to_cch.make_Lb0ToD0barXicpPim()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToLcLcbarK_sprucing_line(name="SpruceBandQ_BuToLcLcbarK", prescale=1):
    line_alg = b_to_cch.make_BuToLcLcbarK()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def BuToLcXicPip_sprucing_line(name="SpruceBandQ_BuToLcXicPip", prescale=1):
    line_alg = b_to_cch.make_BuToLcXicPip()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def Lb0ToD0barLcpKm_sprucing_line(name="SpruceBandQ_Lb0ToD0barLcpKm", prescale=1):
    line_alg = b_to_cch.make_Lb0ToD0barLcpKm()
    return SpruceLine(name=name, algs=make_prefilters() + [line_alg], prescale=prescale)


############################################
# b-Baryon -> Lc Ds (h) (h) spruce lines
############################################


@register_line_builder(sprucing_lines)
@configurable
def LbToLcDsm_line(name="SpruceBandQ_LbToLcDsm"):
    """Lb0 --> Lc+(-> p+ K- pi+)  Ds-(-> K- K+ pi-) line"""
    line_alg = bbaryon_to_lcdsX_lines.make_LbToLcDsm()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )


@register_line_builder(sprucing_lines)
@configurable
def XibToLcDsmK_line(name="SpruceBandQ_XibToLcDsmK"):
    """Xib- --> Lc+(-> p+ K- pi+)  Ds-(-> K- K+ pi-) K- line"""
    line_alg = bbaryon_to_lcdsX_lines.make_XibToLcDsmK()
    return SpruceLine(
        name=name,
        algs=make_prefilters() + [line_alg],
        hlt2_filter_code=["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"],
    )
