###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""

from Hlt2Conf.lines.b_to_open_charm.builders import (
    b_builder,
    basic_builder,
    cbaryon_builder,
    d_builder,
)
from Hlt2Conf.lines.b_to_open_charm.utils import check_process

###########################################################
# Form the Xi_bc+ -> Lc+ D0,  Lc+ -> p K pi,  D0 --> K- pi+
# NB: Here, D0 == K- pi+ AND K+ pi- AND K3pi (provides a WS sample)
##########################################################


@check_process
def make_XibcpToLcpD0_LcpToPKPi_D0ToKPiOrKPiPiPi(process):
    if process == "spruce":
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi()
        cbaryon = cbaryon_builder.make_lc_to_pkpi()
    elif process == "hlt2":
        cmeson = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
        cbaryon = cbaryon_builder.make_tight_lc_to_pkpi_for_xibc()
    line_alg = b_builder.make_xibc2ccx(
        particles=[cbaryon, cmeson],
        descriptors=["Xi_bc+ -> Lambda_c+ D0", "Xi_bc~- -> Lambda_c~- D0"],
    )
    return line_alg


######################################################################
# Form the Omega_bc0 -> Xi_c0 D0,  Xi_c0 -> p K K pi,  D0 --> K- pi+
# NB: Here, D0 == K- pi+ AND K+ pi- (provides a WS sample)
######################################################################


@check_process
def make_Ombc0ToXic0D0_Xic0ToPKKPi_D0ToKPiOrKPiPiPi(process):
    if process == "spruce":
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None
        )
        cbaryon = cbaryon_builder.make_xic0_to_pkkpi()
    elif process == "hlt2":
        cmeson = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
        cbaryon = cbaryon_builder.make_tight_xic0_to_pkkpi_for_xibc()
    line_alg = b_builder.make_xibc2ccx(
        particles=[cbaryon, cmeson],
        descriptors=["Omega_bc0 -> Xi_c0 D0", "Omega_bc~0 -> Xi_c~0 D0"],
    )
    return line_alg


#########################################################################
# Form the Lambda_b0 -> Lambda_c+ D_s-,  Lambda_c+ -> pK- pi+, Ds->HHH
# Can we use only the CF Ds decay here?
#########################################################################
@check_process
def make_LbToLcpDsm_LcpToPKPi_DsmToHHH(process):
    if process == "spruce":
        cmeson = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    elif process == "hlt2":
        cmeson = d_builder.make_dsplus_to_hhh()
    cbaryon = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_lb(
        particles=[cbaryon, cmeson], descriptors=["[Lambda_b0 -> Lambda_c+ D_s-]cc"]
    )
    return line_alg


#############################################################################
# Form the Lambda_b0 -> Lambda_c+ D-,  Lambda_c+ -> pK- pi+, D- -> K+ pi- pi-
#############################################################################
@check_process
def make_LbToLcpDm_LcpToPKPi_DmToPimPimKp(process):
    if process == "spruce":
        cmeson = d_builder.make_dplus_to_kmpippip(pi_pidk_max=None, k_pidk_min=None)
    elif process == "hlt2":
        cmeson = d_builder.make_dplus_to_kmpippip()
    cbaryon = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_lb(
        particles=[cbaryon, cmeson], descriptors=["[Lambda_b0 -> Lambda_c+ D-]cc"]
    )
    return line_alg


#############################################################################
# Form the Lambda_b0 -> Lambda_c+ D-,  Lambda_c+ -> Lambda0 pi+, D- -> K+ pi- pi-
#############################################################################
@check_process
def make_LbToLcpDm_LcpToLambdaLLPip_DmToPimPimKp(process):
    if process == "spruce":
        cmeson = d_builder.make_dplus_to_kmpippip(pi_pidk_max=None, k_pidk_min=None)
    elif process == "hlt2":
        cmeson = d_builder.make_dplus_to_kmpippip()
    lmd = basic_builder.make_lambda_LL()
    cbaryon = cbaryon_builder.make_lc_to_lambdapi(lmd)
    line_alg = b_builder.make_lb(
        particles=[cbaryon, cmeson], descriptors=["[Lambda_b0 -> Lambda_c+ D-]cc"]
    )
    return line_alg


@check_process
def make_LbToLcpDm_LcpToLambdaDDPip_DmToPimPimKp(process):
    if process == "spruce":
        cmeson = d_builder.make_dplus_to_kmpippip(pi_pidk_max=None, k_pidk_min=None)
    elif process == "hlt2":
        cmeson = d_builder.make_dplus_to_kmpippip()
    lmd = basic_builder.make_lambda_DD()
    cbaryon = cbaryon_builder.make_lc_to_lambdapi(lmd)
    line_alg = b_builder.make_lb(
        particles=[cbaryon, cmeson], descriptors=["[Lambda_b0 -> Lambda_c+ D-]cc"]
    )
    return line_alg


#############################################################################
# Form the Lambda_b0 -> Lambda_c+ D-,  Lambda_c+ -> p Ks0, D- -> K+ pi- pi-
#############################################################################
@check_process
def make_LbToLcpDm_LcpTopKsLL_DmToPimPimKp(process):
    if process == "spruce":
        cmeson = d_builder.make_dplus_to_kmpippip(pi_pidk_max=None, k_pidk_min=None)
    elif process == "hlt2":
        cmeson = d_builder.make_dplus_to_kmpippip()
    ks = basic_builder.make_ks_LL()
    cbaryon = cbaryon_builder.make_lc_to_pks(ks)
    line_alg = b_builder.make_lb(
        particles=[cbaryon, cmeson], descriptors=["[Lambda_b0 -> Lambda_c+ D-]cc"]
    )
    return line_alg


@check_process
def make_LbToLcpDm_LcpTopKsDD_DmToPimPimKp(process):
    if process == "spruce":
        cmeson = d_builder.make_dplus_to_kmpippip(pi_pidk_max=None, k_pidk_min=None)
    elif process == "hlt2":
        cmeson = d_builder.make_dplus_to_kmpippip()
    ks = basic_builder.make_ks_DD()
    cbaryon = cbaryon_builder.make_lc_to_pks(ks)
    line_alg = b_builder.make_lb(
        particles=[cbaryon, cmeson], descriptors=["[Lambda_b0 -> Lambda_c+ D-]cc"]
    )
    return line_alg
