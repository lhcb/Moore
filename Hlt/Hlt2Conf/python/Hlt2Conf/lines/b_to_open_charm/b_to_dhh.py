###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC BToDhh lines
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Hlt2Conf.lines.b_to_open_charm.builders import b_builder, basic_builder, d_builder
from Hlt2Conf.lines.b_to_open_charm.filters import b_sigmanet_filter
from Hlt2Conf.lines.b_to_open_charm.utils import check_process

##############################################
# BdToD0hh builders
##############################################


### 2-body lines ###
@check_process
def make_BdToD0PiPi_D0ToHH(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    d = d_builder.make_tight_dzero_to_hh()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. pi+pi- combination
    b = b_builder.make_b2chh(
        particles=[pion, pion, d],
        descriptors=["B0 -> pi+ pi- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BdToD0PiPiSB_D0ToHH(process):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0)
    d = d_builder.make_tight_dzero_to_hh()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. pi+pi- combination
    line_alg = b_builder.make_b2chh(
        particles=[pion, pion, d],
        descriptors=["B0 -> pi+ pi- D0"],
        am_min=5500 * MeV,
        am_min_vtx=5500 * MeV,
        am_max=6000 * MeV,
        am_max_vtx=6000 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0PiPiWS_D0ToHH(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    d = d_builder.make_tight_dzero_to_hh()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. pi+pi- combination
    b = b_builder.make_b2chh(
        particles=[pion, pion, d],
        descriptors=["B0 -> pi+ pi+ D0", "B0 -> pi- pi- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BdToD0KPi_D0ToHH(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    d = d_builder.make_tight_dzero_to_hh()
    b = b_builder.make_b2chh(
        particles=[kaon, pion, d],
        descriptors=["B0 -> K+ pi- D0", "B0 -> K- pi+ D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BdToD0KPiWS_D0ToHH(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    d = d_builder.make_tight_dzero_to_hh()
    b = b_builder.make_b2chh(
        particles=[kaon, pion, d],
        descriptors=["B0 -> K+ pi+ D0", "B0 -> K- pi- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BdToD0KK_D0ToHH(process, MVACut=0.5):
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    d = d_builder.make_tight_dzero_to_hh()
    b = b_builder.make_b2chh(
        particles=[kaon, kaon, d],
        descriptors=["B0 -> K+ K- D0"],
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
        sum_pt_min=6.5 * GeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BdToD0KKWS_D0ToHH(process, MVACut=0.5):
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    d = d_builder.make_tight_dzero_to_hh()
    b = b_builder.make_b2chh(
        particles=[kaon, kaon, d],
        descriptors=["B0 -> K+ K+ D0", "B0 -> K- K- D0"],
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
        sum_pt_min=6.5 * GeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BdToD0PbarP_D0ToHH(process, MVACut=0.3):
    proton = basic_builder.make_soft_protons(
        p_pidp_min=0, p_min=8 * GeV, pt_min=250 * MeV
    )
    d = d_builder.make_dzero_to_hh()
    b = b_builder.make_b2chh(
        particles=[proton, proton, d],
        descriptors=["B0 -> p+ p~- D0"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BdToD0PbarPWS_D0ToHH(process, MVACut=0.3):
    proton = basic_builder.make_soft_protons(
        p_pidp_min=0, p_min=8 * GeV, pt_min=250 * MeV
    )
    d = d_builder.make_dzero_to_hh()
    b = b_builder.make_b2chh(
        particles=[proton, proton, d],
        descriptors=["B0 -> p+ p+ D0", "B0 -> p~- p~- D0"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BdToDstpKsLLPi_DstpToD0Pi_D0ToHH(process):
    ks_ll = basic_builder.make_ks_LL()
    pion = basic_builder.make_soft_pions()
    dzero = d_builder.make_dzero_to_hh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[ks_ll, pion, dst], descriptors=["[B0 -> KS0 pi- D*(2010)+]cc"]
    )
    return line_alg


@check_process
def make_BdToDstpKsDDPi_DstpToD0Pi_D0ToHH(process):
    ks_dd = basic_builder.make_ks_DD()
    pion = basic_builder.make_soft_pions()
    dzero = d_builder.make_dzero_to_hh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[ks_dd, pion, dst], descriptors=["[B0 -> KS0 pi- D*(2010)+]cc"]
    )
    return line_alg


@check_process
def make_BdToDstpKsLLK_DstpToD0Pi_D0ToHH(process):
    ks_ll = basic_builder.make_ks_LL()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[ks_ll, kaon, dst], descriptors=["[B0 -> KS0 K- D*(2010)+]cc"]
    )
    return line_alg


@check_process
def make_BdToDstpKsDDK_DstpToD0Pi_D0ToHH(process):
    ks_dd = basic_builder.make_ks_DD()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[ks_dd, kaon, dst], descriptors=["[B0 -> KS0 K- D*(2010)+]cc"]
    )
    return line_alg


### 4-body lines ###
@check_process
def make_BdToD0PiPi_D0ToHHHH(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    d = d_builder.make_tight_dzero_to_hhhh()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. pi+pi- combination
    b = b_builder.make_b2chh(
        particles=[pion, pion, d],
        descriptors=["B0 -> pi+ pi- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BdToD0PiPiWS_D0ToHHHH(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    d = d_builder.make_tight_dzero_to_hhhh()
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. pi+pi- combination
    b = b_builder.make_b2chh(
        particles=[pion, pion, d],
        descriptors=["B0 -> pi+ pi+ D0", "B0 -> pi- pi- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BdToD0KPi_D0ToHHHH(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    d = d_builder.make_tight_dzero_to_hhhh()
    b = b_builder.make_b2chh(
        particles=[kaon, pion, d],
        descriptors=["B0 -> K+ pi- D0", "B0 -> K- pi+ D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BdToD0KPiWS_D0ToHHHH(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    d = d_builder.make_tight_dzero_to_hhhh()
    b = b_builder.make_b2chh(
        particles=[kaon, pion, d],
        descriptors=["B0 -> K+ pi+ D0", "B0 -> K- pi- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BdToD0KK_D0ToHHHH(process, MVACut=0.5):
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    d = d_builder.make_tight_dzero_to_hhhh()
    b = b_builder.make_b2chh(
        particles=[kaon, kaon, d],
        descriptors=["B0 -> K+ K- D0"],
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
        sum_pt_min=6.5 * GeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BdToD0KKWS_D0ToHHHH(process, MVACut=0.5):
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    d = d_builder.make_tight_dzero_to_hhhh()
    b = b_builder.make_b2chh(
        particles=[kaon, kaon, d],
        descriptors=["B0 -> K+ K+ D0", "B0 -> K- K- D0"],
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
        sum_pt_min=6.5 * GeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BdToD0PbarP_D0ToHHHH(process):
    proton = basic_builder.make_soft_protons(
        p_pidp_min=0, p_min=8 * GeV, pt_min=250 * MeV
    )
    d = d_builder.make_tight_dzero_to_hhhh()
    line_alg = b_builder.make_b2chh(
        particles=[proton, proton, d],
        descriptors=["B0 -> p+ p~- D0"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


@check_process
def make_BdToD0PbarPWS_D0ToHHHH(process):
    proton = basic_builder.make_soft_protons(
        p_pidp_min=0, p_min=8 * GeV, pt_min=250 * MeV
    )
    d = d_builder.make_tight_dzero_to_hhhh()
    line_alg = b_builder.make_b2chh(
        particles=[proton, proton, d],
        descriptors=["B0 -> p+ p+ D0", "B0 -> p~- p~- D0"],
    )
    return line_alg


@check_process
def make_BdToDstpKsLLK_DstpToD0Pi_D0ToHHHH(process):
    ks_ll = basic_builder.make_ks_LL()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hhhh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[ks_ll, kaon, dst], descriptors=["[B0 -> KS0 K- D*(2010)+]cc"]
    )
    return line_alg


@check_process
def make_BdToDstpKsDDK_DstpToD0Pi_D0ToHHHH(process):
    ks_dd = basic_builder.make_ks_DD()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hhhh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[ks_dd, kaon, dst], descriptors=["[B0 -> KS0 K- D*(2010)+]cc"]
    )
    return line_alg


@check_process
def make_BdToDstpKsLLPi_DstpToD0Pi_D0ToHHHH(process):
    ks_ll = basic_builder.make_ks_LL()
    pion = basic_builder.make_soft_pions()
    dzero = d_builder.make_dzero_to_hhhh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[ks_ll, pion, dst], descriptors=["[B0 -> KS0 pi- D*(2010)+]cc"]
    )
    return line_alg


@check_process
def make_BdToDstpKsDDPi_DstpToD0Pi_D0ToHHHH(process):
    ks_dd = basic_builder.make_ks_DD()
    pion = basic_builder.make_soft_pions()
    dzero = d_builder.make_dzero_to_hhhh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[ks_dd, pion, dst], descriptors=["[B0 -> KS0 pi- D*(2010)+]cc"]
    )
    return line_alg


### KsHH (LL) lines ###
@check_process
def make_BdToD0PiPi_D0ToKsLLHH(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. pi+pi- combination
    line_alg = b_builder.make_b2chh(
        particles=[pion, pion, d],
        descriptors=["B0 -> pi+ pi- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0PiPiWS_D0ToKsLLHH(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. pi+pi- combination
    line_alg = b_builder.make_b2chh(
        particles=[pion, pion, d],
        descriptors=["B0 -> pi+ pi+ D0", "B0 -> pi- pi- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0KPi_D0ToKsLLHH(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    line_alg = b_builder.make_b2chh(
        particles=[kaon, pion, d],
        descriptors=["B0 -> K+ pi- D0", "B0 -> K- pi+ D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0KPiWS_D0ToKsLLHH(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    line_alg = b_builder.make_b2chh(
        particles=[kaon, pion, d],
        descriptors=["B0 -> K+ pi+ D0", "B0 -> K- pi- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0KK_D0ToKsLLHH(process, MVACut=0.5):
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    line_alg = b_builder.make_b2chh(
        particles=[kaon, kaon, d],
        descriptors=["B0 -> K+ K- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0KKWS_D0ToKsLLHH(process, MVACut=0.5):
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    line_alg = b_builder.make_b2chh(
        particles=[kaon, kaon, d],
        descriptors=["B0 -> K+ K+ D0", "B0 -> K- K- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


### KsHH (DD) lines ###
@check_process
def make_BdToD0PiPi_D0ToKsDDHH(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. pi+pi- combination
    line_alg = b_builder.make_b2chh(
        particles=[pion, pion, d],
        descriptors=["B0 -> pi+ pi- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0PiPiWS_D0ToKsDDHH(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. pi+pi- combination
    line_alg = b_builder.make_b2chh(
        particles=[pion, pion, d],
        descriptors=["B0 -> pi+ pi+ D0", "B0 -> pi- pi- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0KPi_D0ToKsDDHH(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    line_alg = b_builder.make_b2chh(
        particles=[kaon, pion, d],
        descriptors=["B0 -> K+ pi- D0", "B0 -> K- pi+ D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0KPiWS_D0ToKsDDHH(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    line_alg = b_builder.make_b2chh(
        particles=[kaon, pion, d],
        descriptors=["B0 -> K+ pi+ D0", "B0 -> K- pi- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0KK_D0ToKsDDHH(process, MVACut=0.5):
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    line_alg = b_builder.make_b2chh(
        particles=[kaon, kaon, d],
        descriptors=["B0 -> K+ K- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0KKWS_D0ToKsDDHH(process, MVACut=0.5):
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    d = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    line_alg = b_builder.make_b2chh(
        particles=[kaon, kaon, d],
        descriptors=["B0 -> K+ K+ D0", "B0 -> K- K- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


### charged D lines ###
@check_process
def make_BdToDsstmKsLLPi_DsstmToDsmGamma_DsmToHHH(process):
    ks_ll = basic_builder.make_ks_LL()
    pion = basic_builder.make_tight_pions()
    ds = d_builder.make_dsplus_to_hhh()
    dsst = d_builder.make_dsst_to_dsplusgamma(ds)
    line_alg = b_builder.make_b2x(
        particles=[dsst, ks_ll, pion],
        descriptors=["B0 -> D*_s- KS0 pi+", "B0 -> D*_s+ KS0 pi-"],
    )
    return line_alg


@check_process
def make_BdToDsstmKsDDPi_DsstmToDsmGamma_DsmToHHH(process):
    ks_dd = basic_builder.make_ks_DD()
    pion = basic_builder.make_tight_pions()
    ds = d_builder.make_dsplus_to_hhh()
    dsst = d_builder.make_dsst_to_dsplusgamma(ds)
    line_alg = b_builder.make_b2x(
        particles=[dsst, ks_dd, pion],
        descriptors=["B0 -> D*_s- KS0 pi+", "B0 -> D*_s+ KS0 pi-"],
    )
    return line_alg


@check_process
def make_BdToDsmKsLLPi_DsmToHHH(process):
    ks_ll = basic_builder.make_ks_LL()
    pion = basic_builder.make_tight_pions()
    ds = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[ds, ks_ll, pion],
        descriptors=["B0 -> D_s- KS0 pi+", "B0 -> D_s+ KS0 pi-"],
    )
    return line_alg


@check_process
def make_BdToDsmKsDDPi_DsmToHHH(process):
    ks_dd = basic_builder.make_ks_DD()
    pion = basic_builder.make_tight_pions()
    ds = d_builder.make_dsplus_to_hhh()
    line_alg = b_builder.make_b2x(
        particles=[ds, ks_dd, pion],
        descriptors=["B0 -> D_s- KS0 pi+", "B0 -> D_s+ KS0 pi-"],
    )
    return line_alg


@check_process
def make_BdToDmKsLLPi_DmToHHH(process):
    ks_ll = basic_builder.make_ks_LL()
    d = d_builder.make_dplus_to_hhh()
    pion = basic_builder.make_tight_pions()
    line_alg = b_builder.make_b2x(
        particles=[d, ks_ll, pion], descriptors=["B0 -> D- KS0 pi+", "B0 -> D+ KS0 pi-"]
    )
    return line_alg


@check_process
def make_BdToDmKsDDPi_DmToHHH(process):
    ks_dd = basic_builder.make_ks_DD()
    d = d_builder.make_dplus_to_hhh()
    pion = basic_builder.make_tight_pions()
    line_alg = b_builder.make_b2x(
        particles=[d, ks_dd, pion], descriptors=["B0 -> D- KS0 pi+", "B0 -> D+ KS0 pi-"]
    )
    return line_alg


############################################################################################
# BdToDst0HH_DstToD0(Gamma/Pi0Resolved)_D0To(HH/HHHH/KSLLHH/KSDDHH/Pi0HH(Resolved/Merged)) #
############################################################################################

# D0ToHH


@check_process
def make_BdToDst0KPi_Dst0ToD0Gamma_D0ToHH(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hh()
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi- D*(2007)0", "B0 -> K- pi+ D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPi_Dst0ToD0Gamma_D0ToHH(process):
    pion = basic_builder.make_soft_pions()
    dzero = d_builder.make_dzero_to_hh()
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero], descriptors=["B0 -> pi+ pi- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KK_Dst0ToD0Gamma_D0ToHH(process):
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hh()
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero], descriptors=["B0 -> K+ K- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToHH(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hh()
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi- D*(2007)0", "B0 -> K- pi+ D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToHH(process):
    pion = basic_builder.make_soft_pions()
    dzero = d_builder.make_dzero_to_hh()
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero], descriptors=["B0 -> pi+ pi- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToHH(process):
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hh()
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero], descriptors=["B0 -> K+ K- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KPiWS_Dst0ToD0Gamma_D0ToHH(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hh()
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi+ D*(2007)0", "B0 -> K- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToHH(process):
    pion = basic_builder.make_soft_pions()
    dzero = d_builder.make_dzero_to_hh()
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero],
        descriptors=["B0 -> pi+ pi+ D*(2007)0", "B0 -> pi- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0KKWS_Dst0ToD0Gamma_D0ToHH(process):
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hh()
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero],
        descriptors=["B0 -> K+ K+ D*(2007)0", "B0 -> K- K- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToHH(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hh()
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi+ D*(2007)0", "B0 -> K- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToHH(process):
    pion = basic_builder.make_soft_pions()
    dzero = d_builder.make_dzero_to_hh()
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero],
        descriptors=["B0 -> pi+ pi+ D*(2007)0", "B0 -> pi- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToHH(process):
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hh()
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero],
        descriptors=["B0 -> K+ K+ D*(2007)0", "B0 -> K- K- D*(2007)0"],
    )
    return line_alg


# D0ToHHHH


@check_process
def make_BdToDst0KPi_Dst0ToD0Gamma_D0ToHHHH(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hhhh()
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi- D*(2007)0", "B0 -> K- pi+ D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPi_Dst0ToD0Gamma_D0ToHHHH(process):
    pion = basic_builder.make_soft_pions()
    dzero = d_builder.make_dzero_to_hhhh()
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero], descriptors=["B0 -> pi+ pi- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KK_Dst0ToD0Gamma_D0ToHHHH(process):
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hhhh()
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero], descriptors=["B0 -> K+ K- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToHHHH(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hhhh()
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi- D*(2007)0", "B0 -> K- pi+ D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToHHHH(process):
    pion = basic_builder.make_soft_pions()
    dzero = d_builder.make_dzero_to_hhhh()
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero], descriptors=["B0 -> pi+ pi- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToHHHH(process):
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hhhh()
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero], descriptors=["B0 -> K+ K- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KPiWS_Dst0ToD0Gamma_D0ToHHHH(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hhhh()
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi+ D*(2007)0", "B0 -> K- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToHHHH(process):
    pion = basic_builder.make_soft_pions()
    dzero = d_builder.make_dzero_to_hhhh()
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero],
        descriptors=["B0 -> pi+ pi+ D*(2007)0", "B0 -> pi- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0KKWS_Dst0ToD0Gamma_D0ToHHHH(process):
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hhhh()
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero],
        descriptors=["B0 -> K+ K+ D*(2007)0", "B0 -> K- K- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToHHHH(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hhhh()
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi+ D*(2007)0", "B0 -> K- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToHHHH(process):
    pion = basic_builder.make_soft_pions()
    dzero = d_builder.make_dzero_to_hhhh()
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero],
        descriptors=["B0 -> pi+ pi+ D*(2007)0", "B0 -> pi- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToHHHH(process):
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_hhhh()
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero],
        descriptors=["B0 -> K+ K+ D*(2007)0", "B0 -> K- K- D*(2007)0"],
    )
    return line_alg


# D0ToKsLLHH


@check_process
def make_BdToDst0KPi_Dst0ToD0Gamma_D0ToKsLLHH(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi- D*(2007)0", "B0 -> K- pi+ D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPi_Dst0ToD0Gamma_D0ToKsLLHH(process):
    pion = basic_builder.make_soft_pions()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero], descriptors=["B0 -> pi+ pi- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KK_Dst0ToD0Gamma_D0ToKsLLHH(process):
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero], descriptors=["B0 -> K+ K- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToKsLLHH(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi- D*(2007)0", "B0 -> K- pi+ D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToKsLLHH(process):
    pion = basic_builder.make_soft_pions()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero], descriptors=["B0 -> pi+ pi- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToKsLLHH(process):
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero], descriptors=["B0 -> K+ K- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KPiWS_Dst0ToD0Gamma_D0ToKsLLHH(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi+ D*(2007)0", "B0 -> K- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToKsLLHH(process):
    pion = basic_builder.make_soft_pions()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero],
        descriptors=["B0 -> pi+ pi+ D*(2007)0", "B0 -> pi- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0KKWS_Dst0ToD0Gamma_D0ToKsLLHH(process):
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero],
        descriptors=["B0 -> K+ K+ D*(2007)0", "B0 -> K- K- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToKsLLHH(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi+ D*(2007)0", "B0 -> K- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToKsLLHH(process):
    pion = basic_builder.make_soft_pions()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero],
        descriptors=["B0 -> pi+ pi+ D*(2007)0", "B0 -> pi- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToKsLLHH(process):
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_LL())
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero],
        descriptors=["B0 -> K+ K+ D*(2007)0", "B0 -> K- K- D*(2007)0"],
    )
    return line_alg


# D0ToKsDDHH


@check_process
def make_BdToDst0KPi_Dst0ToD0Gamma_D0ToKsDDHH(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi- D*(2007)0", "B0 -> K- pi+ D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPi_Dst0ToD0Gamma_D0ToKsDDHH(process):
    pion = basic_builder.make_soft_pions()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero], descriptors=["B0 -> pi+ pi- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KK_Dst0ToD0Gamma_D0ToKsDDHH(process):
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero], descriptors=["B0 -> K+ K- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToKsDDHH(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi- D*(2007)0", "B0 -> K- pi+ D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToKsDDHH(process):
    pion = basic_builder.make_soft_pions()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero], descriptors=["B0 -> pi+ pi- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToKsDDHH(process):
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero], descriptors=["B0 -> K+ K- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KPiWS_Dst0ToD0Gamma_D0ToKsDDHH(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi+ D*(2007)0", "B0 -> K- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToKsDDHH(process):
    pion = basic_builder.make_soft_pions()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero],
        descriptors=["B0 -> pi+ pi+ D*(2007)0", "B0 -> pi- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0KKWS_Dst0ToD0Gamma_D0ToKsDDHH(process):
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero],
        descriptors=["B0 -> K+ K+ D*(2007)0", "B0 -> K- K- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToKsDDHH(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi+ D*(2007)0", "B0 -> K- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToKsDDHH(process):
    pion = basic_builder.make_soft_pions()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero],
        descriptors=["B0 -> pi+ pi+ D*(2007)0", "B0 -> pi- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToKsDDHH(process):
    kaon = basic_builder.make_soft_kaons()
    dzero = d_builder.make_dzero_to_kshh(k_shorts=basic_builder.make_ks_DD())
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero],
        descriptors=["B0 -> K+ K+ D*(2007)0", "B0 -> K- K- D*(2007)0"],
    )
    return line_alg


# D0ToHHPi0Resolved


@check_process
def make_BdToDst0KPi_Dst0ToD0Gamma_D0ToHHPi0Resolved(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0)
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi- D*(2007)0", "B0 -> K- pi+ D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPi_Dst0ToD0Gamma_D0ToHHPi0Resolved(process):
    pion = basic_builder.make_soft_pions()
    pi0 = basic_builder.make_resolved_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0)
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero], descriptors=["B0 -> pi+ pi- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KK_Dst0ToD0Gamma_D0ToHHPi0Resolved(process):
    kaon = basic_builder.make_soft_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0)
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero], descriptors=["B0 -> K+ K- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0)
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi- D*(2007)0", "B0 -> K- pi+ D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved(process):
    pion = basic_builder.make_soft_pions()
    pi0 = basic_builder.make_resolved_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0)
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero], descriptors=["B0 -> pi+ pi- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved(process):
    kaon = basic_builder.make_soft_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0)
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero], descriptors=["B0 -> K+ K- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KPiWS_Dst0ToD0Gamma_D0ToHHPi0Resolved(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0)
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi+ D*(2007)0", "B0 -> K- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToHHPi0Resolved(process):
    pion = basic_builder.make_soft_pions()
    pi0 = basic_builder.make_resolved_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0)
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero],
        descriptors=["B0 -> pi+ pi+ D*(2007)0", "B0 -> pi- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0KKWS_Dst0ToD0Gamma_D0ToHHPi0Resolved(process):
    kaon = basic_builder.make_soft_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0)
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero],
        descriptors=["B0 -> K+ K+ D*(2007)0", "B0 -> K- K- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0)
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi+ D*(2007)0", "B0 -> K- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved(process):
    pion = basic_builder.make_soft_pions()
    pi0 = basic_builder.make_resolved_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0)
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero],
        descriptors=["B0 -> pi+ pi+ D*(2007)0", "B0 -> pi- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved(process):
    kaon = basic_builder.make_soft_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0)
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero],
        descriptors=["B0 -> K+ K+ D*(2007)0", "B0 -> K- K- D*(2007)0"],
    )
    return line_alg


# D0ToHHPi0Merged


@check_process
def make_BdToDst0KPi_Dst0ToD0Gamma_D0ToHHPi0Merged(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0)
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi- D*(2007)0", "B0 -> K- pi+ D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPi_Dst0ToD0Gamma_D0ToHHPi0Merged(process):
    pion = basic_builder.make_soft_pions()
    pi0 = basic_builder.make_merged_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0)
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero], descriptors=["B0 -> pi+ pi- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KK_Dst0ToD0Gamma_D0ToHHPi0Merged(process):
    kaon = basic_builder.make_soft_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0)
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero], descriptors=["B0 -> K+ K- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    pi0_merged = basic_builder.make_merged_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0_merged)
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi- D*(2007)0", "B0 -> K- pi+ D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged(process):
    pion = basic_builder.make_soft_pions()
    pi0_merged = basic_builder.make_merged_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0_merged)
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero], descriptors=["B0 -> pi+ pi- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged(process):
    kaon = basic_builder.make_soft_kaons()
    pi0_merged = basic_builder.make_merged_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0_merged)
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero], descriptors=["B0 -> K+ K- D*(2007)0"]
    )
    return line_alg


@check_process
def make_BdToDst0KPiWS_Dst0ToD0Gamma_D0ToHHPi0Merged(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0)
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi+ D*(2007)0", "B0 -> K- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToHHPi0Merged(process):
    pion = basic_builder.make_soft_pions()
    pi0 = basic_builder.make_merged_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0)
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero],
        descriptors=["B0 -> pi+ pi+ D*(2007)0", "B0 -> pi- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0KKWS_Dst0ToD0Gamma_D0ToHHPi0Merged(process):
    kaon = basic_builder.make_soft_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0)
    dstzero = d_builder.make_dzerost_to_dzerogamma(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero],
        descriptors=["B0 -> K+ K+ D*(2007)0", "B0 -> K- K- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged(process):
    pion = basic_builder.make_soft_pions()
    kaon = basic_builder.make_soft_kaons()
    pi0_merged = basic_builder.make_merged_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0_merged)
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dstzero],
        descriptors=["B0 -> K+ pi+ D*(2007)0", "B0 -> K- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged(process):
    pion = basic_builder.make_soft_pions()
    pi0_merged = basic_builder.make_merged_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0_merged)
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dstzero],
        descriptors=["B0 -> pi+ pi+ D*(2007)0", "B0 -> pi- pi- D*(2007)0"],
    )
    return line_alg


@check_process
def make_BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged(process):
    kaon = basic_builder.make_soft_kaons()
    pi0_merged = basic_builder.make_merged_pi0s()
    dzero = d_builder.make_dzero_to_hhpi0(pi0_merged)
    pi0 = basic_builder.make_resolved_pi0s()
    dstzero = d_builder.make_dzerost_to_dzeropi0(dzero, pi0)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dstzero],
        descriptors=["B0 -> K+ K+ D*(2007)0", "B0 -> K- K- D*(2007)0"],
    )
    return line_alg


##############################################
# BuToDhh lines
##############################################


@check_process
def make_BuToDmPiPi_DmToHHH(process, MVACut=0.35):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=200 * MeV)
    d = d_builder.make_dplus_to_hhh(pi_pidk_max=0, k_pidk_min=0)
    # note the order in which we write the decay descriptor because the b2dhh
    # builder makes a cut on the 1,2 i.e. pi+ pi+ combination
    b = b_builder.make_b2chh(
        particles=[pion, pion, d],
        descriptors=["[B+ -> pi+ pi+ D-]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
        bcvtx_sep_min=0.0 * mm,
        sum_pt_min=6 * GeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BuToDmKPi_DmToHHH(process, MVACut=0.35):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=200 * MeV)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=200 * MeV)
    d = d_builder.make_dplus_to_hhh(pi_pidk_max=0, k_pidk_min=0)
    # note the order in which we write the decay descriptor because the b2dhh
    # builder makes a cut on the 1,2 i.e. K+ pi+ combination
    b = b_builder.make_b2chh(
        particles=[kaon, pion, d],
        descriptors=["[B+ -> K+ pi+ D-]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
        bcvtx_sep_min=0.0 * mm,
        sum_pt_min=6 * GeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BuToDmKK_DmToHHH(process, MVACut=0.35):
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=200 * MeV)
    d = d_builder.make_dplus_to_hhh(pi_pidk_max=0, k_pidk_min=0)
    # note the order in which we write the decay descriptor because the b2dhh
    # builder makes a cut on the 1,2 i.e. K+ K+ combination
    b = b_builder.make_b2chh(
        particles=[kaon, kaon, d],
        descriptors=["[B+ -> K+ K+ D-]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
        bcvtx_sep_min=0.0 * mm,
        sum_pt_min=6 * GeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BuToDpPiPi_DpToHHH(process, MVACut=0.35):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=200 * MeV)
    d = d_builder.make_dplus_to_hhh(pi_pidk_max=0, k_pidk_min=0)
    b = b_builder.make_b2chh(
        particles=[pion, pion, d],
        descriptors=["[B+ -> pi+ pi- D+]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
        bcvtx_sep_min=0.0 * mm,
        sum_pt_min=6 * GeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BuToDpKPi_DpToHHH(process, MVACut=0.35):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=200 * MeV)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=200 * MeV)
    d = d_builder.make_dplus_to_hhh(pi_pidk_max=0, k_pidk_min=0)
    b = b_builder.make_b2chh(
        particles=[kaon, pion, d],
        descriptors=["[B+ -> K+ pi- D+]cc", "[B+ -> K- pi+ D+]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
        bcvtx_sep_min=0.0 * mm,
        sum_pt_min=6 * GeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BuToDpKK_DpToHHH(process, MVACut=0.35):
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=200 * MeV)
    d = d_builder.make_dplus_to_hhh(pi_pidk_max=0, k_pidk_min=0)
    b = b_builder.make_b2chh(
        particles=[kaon, kaon, d],
        descriptors=["[B+ -> K+ K- D+]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
        bcvtx_sep_min=0.0 * mm,
        sum_pt_min=6 * GeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BuToDpPbarP_DpToHHH(process, MVACut=0.35):
    proton = basic_builder.make_soft_protons(
        p_pidp_min=0, p_min=8 * GeV, pt_min=250 * MeV
    )
    d = d_builder.make_dplus_to_hhh(pi_pidk_max=0, k_pidk_min=0)
    b = b_builder.make_b2chh(
        particles=[proton, proton, d],
        descriptors=["[B+ -> p+ p~- D+]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
        bcvtx_sep_min=0.0 * mm,
        sum_pt_min=6 * GeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


##############################################
# BuToDshh lines
##############################################


@check_process
def make_BuToDsmPiPi_DsmToHHH(process, MVACut=0.35):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=200 * MeV)
    ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=0, k_pidk_min=0)
    b = b_builder.make_b2chh(
        particles=[pion, pion, ds],
        descriptors=["[B+ -> pi+ pi+ D_s-]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
        bcvtx_sep_min=0.0 * mm,
        sum_pt_min=6 * GeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BuToDsmKPi_DsmToHHH(process, MVACut=0.35):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=200 * MeV)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=200 * MeV)
    ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=0, k_pidk_min=0)
    b = b_builder.make_b2chh(
        particles=[kaon, pion, ds],
        descriptors=["[B+ -> K+ pi+ D_s-]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
        bcvtx_sep_min=0.0 * mm,
        sum_pt_min=6 * GeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BuToDsmKK_DsmToHHH(process, MVACut=0.35):
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=200 * MeV)
    ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=0, k_pidk_min=0)
    b = b_builder.make_b2chh(
        particles=[kaon, kaon, ds],
        descriptors=["[B+ -> K+ K+ D_s-]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
        bcvtx_sep_min=0.0 * mm,
        sum_pt_min=6 * GeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BuToDspPiPi_DspToHHH(process, MVACut=0.35):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=200 * MeV)
    ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=0, k_pidk_min=0)
    b = b_builder.make_b2chh(
        particles=[pion, pion, ds],
        descriptors=["[B+ -> pi+ pi- D_s+]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
        bcvtx_sep_min=0.0 * mm,
        sum_pt_min=6 * GeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BuToDspKPi_DspToHHH(process, MVACut=0.35):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=200 * MeV)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=200 * MeV)
    ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=0, k_pidk_min=0)
    b = b_builder.make_b2chh(
        particles=[kaon, pion, ds],
        descriptors=["[B+ -> K+ pi- D_s+]cc", "[B+ -> K- pi+ D_s+]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
        bcvtx_sep_min=0.0 * mm,
        sum_pt_min=6 * GeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BuToDspKK_DspToHHH(process, MVACut=0.35):
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=200 * MeV)
    ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=0, k_pidk_min=0)
    b = b_builder.make_b2chh(
        particles=[kaon, kaon, ds],
        descriptors=["[B+ -> K+ K- D_s+]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
        bcvtx_sep_min=0.0 * mm,
        sum_pt_min=6 * GeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


@check_process
def make_BuToDspPbarP_DspToHHH(process, MVACut=0.35):
    proton = basic_builder.make_soft_protons(
        p_pidp_min=0, p_min=8 * GeV, pt_min=250 * MeV
    )
    ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=0, k_pidk_min=0)
    b = b_builder.make_b2chh(
        particles=[proton, proton, ds],
        descriptors=["[B+ -> p+ p~- D_s+]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
        bcvtx_sep_min=0.0 * mm,
        sum_pt_min=6 * GeV,
    )
    line_alg = b_sigmanet_filter(b, MVACut)
    return line_alg


##############################################
# BuToDsthh lines
##############################################


@check_process
def make_BuToDstmPiPi_DstmToD0Pi_D0ToHH(process, MVACut=0.35):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=200 * MeV)
    dzero = d_builder.make_dzero_to_hh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dst],
        descriptors=["[B+ -> pi+ pi+ D*(2010)-]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToDstmKPi_DstmToD0Pi_D0ToHH(process, MVACut=0.35):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=200 * MeV)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=200 * MeV)
    dzero = d_builder.make_dzero_to_hh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dst],
        descriptors=["[B+ -> K+ pi+ D*(2010)-]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToDstmKK_DstmToD0Pi_D0ToHH(process, MVACut=0.35):
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=200 * MeV)
    dzero = d_builder.make_dzero_to_hh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dst],
        descriptors=["[B+ -> K+ K+ D*(2010)-]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToDstpPiPi_DstpToD0Pi_D0ToHH(process, MVACut=0.35):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=200 * MeV)
    dzero = d_builder.make_dzero_to_hh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[pion, pion, dst],
        descriptors=["[B+ -> pi+ pi- D*(2010)+]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToDstpKPi_DstpToD0Pi_D0ToHH(process, MVACut=0.35):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=200 * MeV)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=200 * MeV)
    dzero = d_builder.make_dzero_to_hh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, pion, dst],
        descriptors=["[B+ -> K+ pi- D*(2010)+]cc", "[B+ -> K- pi+ D*(2010)+]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToDstpKK_DstpToD0Pi_D0ToHH(process, MVACut=0.35):
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=200 * MeV)
    dzero = d_builder.make_dzero_to_hh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[kaon, kaon, dst],
        descriptors=["[B+ -> K+ K- D*(2010)+]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToDstpPbarP_DstpToD0Pi_D0ToHH(process, MVACut=0.35):
    proton = basic_builder.make_soft_protons(
        p_pidp_min=0, p_min=8 * GeV, pt_min=250 * MeV
    )
    dzero = d_builder.make_dzero_to_hh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2cstarhh(
        particles=[proton, proton, dst],
        descriptors=["[B+ -> p+ p~- D*(2010)+]cc"],
        am_max=5900 * MeV,
        am_max_vtx=5800 * MeV,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


##############################################
# B->DKs0Pi D->hh
##############################################


@check_process
def make_BuToD0KsLLPi_D0ToHH(process, MVACut=0.7):
    ks_ll = basic_builder.make_ks_LL()
    pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_hh(pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzero, ks_ll, pion],
        descriptors=["B+ -> D0 KS0 pi+", "B- -> D0 KS0 pi-"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KsDDPi_D0ToHH(process, MVACut=0.7):
    ks_dd = basic_builder.make_ks_DD()
    dzero = d_builder.make_dzero_to_hh(pi_pidk_max=20, k_pidk_min=-10)
    pion = basic_builder.make_tight_pions()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzero, ks_dd, pion],
        descriptors=["B+ -> D0 KS0 pi+", "B- -> D0 KS0 pi-"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KsLLPi_D0ToHHWS(process, MVACut=0.7):
    ks_ll = basic_builder.make_ks_LL()
    pion = basic_builder.make_tight_pions()
    dzero = d_builder.make_dzero_to_hh_ws(pi_pidk_max=20, k_pidk_min=-10)
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzero, ks_ll, pion],
        descriptors=["B+ -> D0 KS0 pi+", "B- -> D0 KS0 pi-"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KsDDPi_D0ToHHWS(process, MVACut=0.7):
    ks_dd = basic_builder.make_ks_DD()
    dzero = d_builder.make_dzero_to_hh_ws(pi_pidk_max=20, k_pidk_min=-10)
    pion = basic_builder.make_tight_pions()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzero, ks_dd, pion],
        descriptors=["B+ -> D0 KS0 pi+", "B- -> D0 KS0 pi-"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


##############################################
# B->DKs0Pi D->Ks0hh
##############################################


@check_process
def make_BuToD0KsLLPi_D0ToKsLLHH(process, MVACut=0.7):
    ks_ll = basic_builder.make_ks_LL()
    dzero = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
    )
    pion = basic_builder.make_tight_pions()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzero, ks_ll, pion],
        descriptors=["B+ -> D0 KS0 pi+", "B- -> D0 KS0 pi-"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KsDDPi_D0ToKsLLHH(process, MVACut=0.7):
    ks_dd = basic_builder.make_ks_DD()
    dzero = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
    )
    pion = basic_builder.make_tight_pions()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzero, ks_dd, pion],
        descriptors=["B+ -> D0 KS0 pi+", "B- -> D0 KS0 pi-"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KsLLPi_D0ToKsDDHH(process, MVACut=0.7):
    ks_ll = basic_builder.make_ks_LL()
    dzero = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
    )
    pion = basic_builder.make_tight_pions()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzero, ks_ll, pion],
        descriptors=["B+ -> D0 KS0 pi+", "B- -> D0 KS0 pi-"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KsDDPi_D0ToKsDDHH(process, MVACut=0.7):
    ks_dd = basic_builder.make_ks_DD()
    dzero = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
    )
    pion = basic_builder.make_tight_pions()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzero, ks_dd, pion],
        descriptors=["B+ -> D0 KS0 pi+", "B- -> D0 KS0 pi-"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KsLLPi_D0ToKsLLHHWS(process, MVACut=0.7):
    ks_ll = basic_builder.make_ks_LL()
    dzero = d_builder.make_dzero_to_kshh_ws(
        pi_pidk_max=20, k_pidk_min=-10, k_shorts=basic_builder.make_ks_LL()
    )
    pion = basic_builder.make_tight_pions()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzero, ks_ll, pion],
        descriptors=["B+ -> D0 KS0 pi+", "B- -> D0 KS0 pi-"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KsDDPi_D0ToKsLLHHWS(process, MVACut=0.7):
    ks_dd = basic_builder.make_ks_DD()
    dzero = d_builder.make_dzero_to_kshh_ws(
        pi_pidk_max=20, k_pidk_min=-10, k_shorts=basic_builder.make_ks_LL()
    )
    pion = basic_builder.make_tight_pions()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzero, ks_dd, pion],
        descriptors=["B+ -> D0 KS0 pi+", "B- -> D0 KS0 pi-"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KsLLPi_D0ToKsDDHHWS(process, MVACut=0.7):
    ks_ll = basic_builder.make_ks_LL()
    dzero = d_builder.make_dzero_to_kshh_ws(
        pi_pidk_max=20, k_pidk_min=-10, k_shorts=basic_builder.make_ks_DD()
    )
    pion = basic_builder.make_tight_pions()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzero, ks_ll, pion],
        descriptors=["B+ -> D0 KS0 pi+", "B- -> D0 KS0 pi-"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KsDDPi_D0ToKsDDHHWS(process, MVACut=0.7):
    ks_dd = basic_builder.make_ks_DD()
    dzero = d_builder.make_dzero_to_kshh_ws(
        pi_pidk_max=20, k_pidk_min=-10, k_shorts=basic_builder.make_ks_DD()
    )
    pion = basic_builder.make_tight_pions()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzero, ks_dd, pion],
        descriptors=["B+ -> D0 KS0 pi+", "B- -> D0 KS0 pi-"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


##############################################
# B->DKs0Pi D->hhhh
##############################################


@check_process
def make_BuToD0KsLLPi_D0ToHHHH(process, MVACut=0.7):
    ks_ll = basic_builder.make_ks_LL()
    dzero = d_builder.make_dzero_to_hhhh(pi_pidk_max=20, k_pidk_min=-10)
    pion = basic_builder.make_tight_pions()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzero, ks_ll, pion],
        descriptors=["B+ -> D0 KS0 pi+", "B- -> D0 KS0 pi-"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KsDDPi_D0ToHHHH(process, MVACut=0.7):
    ks_dd = basic_builder.make_ks_DD()
    dzero = d_builder.make_dzero_to_hhhh(pi_pidk_max=20, k_pidk_min=-10)
    pion = basic_builder.make_tight_pions()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzero, ks_dd, pion],
        descriptors=["B+ -> D0 KS0 pi+", "B- -> D0 KS0 pi-"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KsLLPi_D0ToHHHHWS(process, MVACut=0.7):
    ks_ll = basic_builder.make_ks_LL()
    dzero = d_builder.make_dzero_to_hhhh_ws(pi_pidk_max=20, k_pidk_min=-10)
    pion = basic_builder.make_tight_pions()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzero, ks_ll, pion],
        descriptors=["B+ -> D0 KS0 pi+", "B- -> D0 KS0 pi-"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KsDDPi_D0ToHHHHWS(process, MVACut=0.7):
    ks_dd = basic_builder.make_ks_DD()
    dzero = d_builder.make_dzero_to_hhhh_ws(pi_pidk_max=20, k_pidk_min=-10)
    pion = basic_builder.make_tight_pions()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[dzero, ks_dd, pion],
        descriptors=["B+ -> D0 KS0 pi+", "B- -> D0 KS0 pi-"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


##############################################
# B->D0hPi0 D->hh
##############################################


@check_process
def make_BuToD0KPi0Resolved_D0ToHH(process, MVACut=0.7):
    d = d_builder.make_dzero_to_hh(pi_pidk_max=20, k_pidk_min=-10)
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon, pi0],
        descriptors=["B+ -> D0 K+ pi0", "B- -> D0 K- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KPi0Merged_D0ToHH(process, MVACut=0.7):
    d = d_builder.make_dzero_to_hh(pi_pidk_max=20, k_pidk_min=-10)
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon, pi0],
        descriptors=["B+ -> D0 K+ pi0", "B- -> D0 K- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0PiPi0Resolved_D0ToHH(process, MVACut=0.7):
    d = d_builder.make_dzero_to_hh(pi_pidk_max=20, k_pidk_min=-10)
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_resolved_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion, pi0],
        descriptors=["B+ -> D0 pi+ pi0", "B- -> D0 pi- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0PiPi0Merged_D0ToHH(process, MVACut=0.7):
    d = d_builder.make_dzero_to_hh(pi_pidk_max=20, k_pidk_min=-10)
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_merged_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion, pi0],
        descriptors=["B+ -> D0 pi+ pi0", "B- -> D0 pi- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KPi0Resolved_D0ToHHWS(process, MVACut=0.7):
    d = d_builder.make_dzero_to_hh_ws(pi_pidk_max=20, k_pidk_min=-10)
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon, pi0],
        descriptors=["B+ -> D0 K+ pi0", "B- -> D0 K- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KPi0Merged_D0ToHHWS(process, MVACut=0.7):
    d = d_builder.make_dzero_to_hh_ws(pi_pidk_max=20, k_pidk_min=-10)
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon, pi0],
        descriptors=["B+ -> D0 K+ pi0", "B- -> D0 K- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0PiPi0Resolved_D0ToHHWS(process, MVACut=0.7):
    d = d_builder.make_dzero_to_hh_ws(pi_pidk_max=20, k_pidk_min=-10)
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_resolved_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion, pi0],
        descriptors=["B+ -> D0 pi+ pi0", "B- -> D0 pi- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0PiPi0Merged_D0ToHHWS(process, MVACut=0.7):
    d = d_builder.make_dzero_to_hh_ws(pi_pidk_max=20, k_pidk_min=-10)
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_merged_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion, pi0],
        descriptors=["B+ -> D0 pi+ pi0", "B- -> D0 pi- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


##############################################
# B->D0hPi0 D->Ks0hh
##############################################


@check_process
def make_BuToD0KPi0Resolved_D0ToKsDDHH(process, MVACut=0.7):
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
    )
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon, pi0],
        descriptors=["B+ -> D0 K+ pi0", "B- -> D0 K- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KPi0Merged_D0ToKsDDHH(process, MVACut=0.7):
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
    )
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon, pi0],
        descriptors=["B+ -> D0 K+ pi0", "B- -> D0 K- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0PiPi0Resolved_D0ToKsDDHH(process, MVACut=0.7):
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
    )
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_resolved_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion, pi0],
        descriptors=["B+ -> D0 pi+ pi0", "B- -> D0 pi- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0PiPi0Merged_D0ToKsDDHH(process, MVACut=0.7):
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
    )
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_merged_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion, pi0],
        descriptors=["B+ -> D0 pi+ pi0", "B- -> D0 pi- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KPi0Resolved_D0ToKsLLHH(process, MVACut=0.7):
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
    )
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon, pi0],
        descriptors=["B+ -> D0 K+ pi0", "B- -> D0 K- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KPi0Merged_D0ToKsLLHH(process, MVACut=0.7):
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
    )
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon, pi0],
        descriptors=["B+ -> D0 K+ pi0", "B- -> D0 K- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0PiPi0Resolved_D0ToKsLLHH(process, MVACut=0.7):
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
    )
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_resolved_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion, pi0],
        descriptors=["B+ -> D0 pi+ pi0", "B- -> D0 pi- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0PiPi0Merged_D0ToKsLLHH(process, MVACut=0.7):
    d = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
    )
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_merged_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion, pi0],
        descriptors=["B+ -> D0 pi+ pi0", "B- -> D0 pi- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KPi0Resolved_D0ToKsDDHHWS(process, MVACut=0.7):
    d = d_builder.make_dzero_to_kshh_ws(
        pi_pidk_max=20, k_pidk_min=-10, k_shorts=basic_builder.make_ks_DD()
    )
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon, pi0],
        descriptors=["B+ -> D0 K+ pi0", "B- -> D0 K- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KPi0Merged_D0ToKsDDHHWS(process, MVACut=0.7):
    d = d_builder.make_dzero_to_kshh_ws(
        pi_pidk_max=20, k_pidk_min=-10, k_shorts=basic_builder.make_ks_DD()
    )
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon, pi0],
        descriptors=["B+ -> D0 K+ pi0", "B- -> D0 K- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0PiPi0Resolved_D0ToKsDDHHWS(process, MVACut=0.7):
    d = d_builder.make_dzero_to_kshh_ws(
        pi_pidk_max=20, k_pidk_min=-10, k_shorts=basic_builder.make_ks_DD()
    )
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_resolved_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion, pi0],
        descriptors=["B+ -> D0 pi+ pi0", "B- -> D0 pi- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0PiPi0Merged_D0ToKsDDHHWS(process, MVACut=0.7):
    d = d_builder.make_dzero_to_kshh_ws(
        pi_pidk_max=20, k_pidk_min=-10, k_shorts=basic_builder.make_ks_DD()
    )
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_merged_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion, pi0],
        descriptors=["B+ -> D0 pi+ pi0", "B- -> D0 pi- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KPi0Resolved_D0ToKsLLHHWS(process, MVACut=0.7):
    d = d_builder.make_dzero_to_kshh_ws(
        pi_pidk_max=20, k_pidk_min=-10, k_shorts=basic_builder.make_ks_LL()
    )
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon, pi0],
        descriptors=["B+ -> D0 K+ pi0", "B- -> D0 K- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KPi0Merged_D0ToKsLLHHWS(process, MVACut=0.7):
    d = d_builder.make_dzero_to_kshh_ws(
        pi_pidk_max=20, k_pidk_min=-10, k_shorts=basic_builder.make_ks_LL()
    )
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon, pi0],
        descriptors=["B+ -> D0 K+ pi0", "B- -> D0 K- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0PiPi0Resolved_D0ToKsLLHHWS(process, MVACut=0.7):
    d = d_builder.make_dzero_to_kshh_ws(
        pi_pidk_max=20, k_pidk_min=-10, k_shorts=basic_builder.make_ks_LL()
    )
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_resolved_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion, pi0],
        descriptors=["B+ -> D0 pi+ pi0", "B- -> D0 pi- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0PiPi0Merged_D0ToKsLLHHWS(process, MVACut=0.7):
    d = d_builder.make_dzero_to_kshh_ws(
        pi_pidk_max=20, k_pidk_min=-10, k_shorts=basic_builder.make_ks_LL()
    )
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_merged_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion, pi0],
        descriptors=["B+ -> D0 pi+ pi0", "B- -> D0 pi- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


##############################################
# B->D0hPi0 D->hhhh
##############################################


@check_process
def make_BuToD0KPi0Resolved_D0ToHHHH(process, MVACut=0.7):
    d = d_builder.make_dzero_to_hhhh(pi_pidk_max=20, k_pidk_min=-10)
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon, pi0],
        descriptors=["B+ -> D0 K+ pi0", "B- -> D0 K- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KPi0Merged_D0ToHHHH(process, MVACut=0.7):
    d = d_builder.make_dzero_to_hhhh(pi_pidk_max=20, k_pidk_min=-10)
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon, pi0],
        descriptors=["B+ -> D0 K+ pi0", "B- -> D0 K- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0PiPi0Resolved_D0ToHHHH(process, MVACut=0.7):
    d = d_builder.make_dzero_to_hhhh(pi_pidk_max=20, k_pidk_min=-10)
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_resolved_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion, pi0],
        descriptors=["B+ -> D0 pi+ pi0", "B- -> D0 pi- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0PiPi0Merged_D0ToHHHH(process, MVACut=0.7):
    d = d_builder.make_dzero_to_hhhh(pi_pidk_max=20, k_pidk_min=-10)
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_merged_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion, pi0],
        descriptors=["B+ -> D0 pi+ pi0", "B- -> D0 pi- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KPi0Resolved_D0ToHHHHWS(process, MVACut=0.7):
    d = d_builder.make_dzero_to_hhhh_ws(pi_pidk_max=20, k_pidk_min=-10)
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon, pi0],
        descriptors=["B+ -> D0 K+ pi0", "B- -> D0 K- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0KPi0Merged_D0ToHHHHWS(process, MVACut=0.7):
    d = d_builder.make_dzero_to_hhhh_ws(pi_pidk_max=20, k_pidk_min=-10)
    kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, kaon, pi0],
        descriptors=["B+ -> D0 K+ pi0", "B- -> D0 K- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0PiPi0Resolved_D0ToHHHHWS(process, MVACut=0.7):
    d = d_builder.make_dzero_to_hhhh_ws(pi_pidk_max=20, k_pidk_min=-10)
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_resolved_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion, pi0],
        descriptors=["B+ -> D0 pi+ pi0", "B- -> D0 pi- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


@check_process
def make_BuToD0PiPi0Merged_D0ToHHHHWS(process, MVACut=0.7):
    d = d_builder.make_dzero_to_hhhh_ws(pi_pidk_max=20, k_pidk_min=-10)
    pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_merged_pi0s()
    line_alg = b_builder.make_loose_mass_b2x(
        particles=[d, pion, pi0],
        descriptors=["B+ -> D0 pi+ pi0", "B- -> D0 pi- pi0"],
        bcvtx_sep_min=0 * mm,
    )
    line_alg = b_sigmanet_filter(line_alg, MVACut)
    return line_alg


############################
# BdToD0HH_D0ToPi0HH lines #
############################


@check_process
def make_BdToD0PiPi_D0ToHHPi0Resolved(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    pi0 = basic_builder.make_resolved_pi0s()
    d = d_builder.make_dzero_to_hhpi0(pi0)
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. pi+pi- combination
    line_alg = b_builder.make_b2chh(
        particles=[pion, pion, d],
        descriptors=["B0 -> pi+ pi- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0KPi_D0ToHHPi0Resolved(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    pi0 = basic_builder.make_resolved_pi0s()
    d = d_builder.make_dzero_to_hhpi0(pi0)
    line_alg = b_builder.make_b2chh(
        particles=[kaon, pion, d],
        descriptors=["B0 -> K+ pi- D0", "B0 -> K- pi+ D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0KK_D0ToHHPi0Resolved(process, MVACut=0.5):
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    pi0 = basic_builder.make_resolved_pi0s()
    d = d_builder.make_dzero_to_hhpi0(pi0)
    line_alg = b_builder.make_b2chh(
        particles=[kaon, kaon, d],
        descriptors=["B0 -> K+ K- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0PiPi_D0ToHHPi0Merged(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    pi0 = basic_builder.make_merged_pi0s()
    d = d_builder.make_dzero_to_hhpi0(pi0)
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. pi+pi- combination
    line_alg = b_builder.make_b2chh(
        particles=[pion, pion, d],
        descriptors=["B0 -> pi+ pi- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0KPi_D0ToHHPi0Merged(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    pi0 = basic_builder.make_merged_pi0s()
    d = d_builder.make_dzero_to_hhpi0(pi0)
    line_alg = b_builder.make_b2chh(
        particles=[kaon, pion, d],
        descriptors=["B0 -> K+ pi- D0", "B0 -> K- pi+ D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0KK_D0ToHHPi0Merged(process, MVACut=0.5):
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    pi0 = basic_builder.make_merged_pi0s()
    d = d_builder.make_dzero_to_hhpi0(pi0)
    line_alg = b_builder.make_b2chh(
        particles=[kaon, kaon, d],
        descriptors=["B0 -> K+ K- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0PiPiWS_D0ToHHPi0Resolved(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    pi0 = basic_builder.make_resolved_pi0s()
    d = d_builder.make_dzero_to_hhpi0(pi0)
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. pi+pi- combination
    line_alg = b_builder.make_b2chh(
        particles=[pion, pion, d],
        descriptors=["B0 -> pi+ pi+ D0", "B0 -> pi- pi- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0KPiWS_D0ToHHPi0Resolved(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    pi0 = basic_builder.make_resolved_pi0s()
    d = d_builder.make_dzero_to_hhpi0(pi0)
    line_alg = b_builder.make_b2chh(
        particles=[kaon, pion, d],
        descriptors=["B0 -> K+ pi+ D0", "B0 -> K- pi- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0KKWS_D0ToHHPi0Resolved(process, MVACut=0.5):
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    pi0 = basic_builder.make_resolved_pi0s()
    d = d_builder.make_dzero_to_hhpi0(pi0)
    line_alg = b_builder.make_b2chh(
        particles=[kaon, kaon, d],
        descriptors=["B0 -> K+ K+ D0", "B0 -> K- K- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0PiPiWS_D0ToHHPi0Merged(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    pi0 = basic_builder.make_merged_pi0s()
    d = d_builder.make_dzero_to_hhpi0(pi0)
    # note the order we write the decay descriptor in because the b2chh builder makes a
    # cut on the 1,2 i.e. pi+pi- combination
    line_alg = b_builder.make_b2chh(
        particles=[pion, pion, d],
        descriptors=["B0 -> pi+ pi+ D0", "B0 -> pi- pi- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0KPiWS_D0ToHHPi0Merged(process, MVACut=0.5):
    pion = basic_builder.make_soft_pions(pi_pidk_max=0, pt_min=150 * MeV)
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    pi0 = basic_builder.make_merged_pi0s()
    d = d_builder.make_dzero_to_hhpi0(pi0)
    line_alg = b_builder.make_b2chh(
        particles=[kaon, pion, d],
        descriptors=["B0 -> K+ pi+ D0", "B0 -> K- pi- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


@check_process
def make_BdToD0KKWS_D0ToHHPi0Merged(process, MVACut=0.5):
    kaon = basic_builder.make_soft_kaons(k_pidk_min=0, pt_min=150 * MeV)
    pi0 = basic_builder.make_merged_pi0s()
    d = d_builder.make_dzero_to_hhpi0(pi0)
    line_alg = b_builder.make_b2chh(
        particles=[kaon, kaon, d],
        descriptors=["B0 -> K+ K+ D0", "B0 -> K- K- D0"],
        sum_pt_min=6.5 * GeV,
        am_min=4750 * MeV,
        am_min_vtx=4750 * MeV,
    )
    return line_alg


#############################################################################
# Form the Tbc -> D0 K- pi+, D0 --> Kpi & K3pi
##############################################################################


@check_process
def make_TbcToD0KmPip_D0ToKPiOrKPiPiPi(process):
    Dz = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
    kaon = basic_builder.make_tightpid_tight_kaons(k_pidk_min=-2)
    pion = basic_builder.make_tightpid_soft_pions()

    ### applying hard cuts on pion if it's not forming D* with any of D0
    Dst_12 = (F.MASS - F.CHILD(1, F.MASS)) < 150 * MeV
    tight2 = (F.CHILD(2, F.PT) > 500 * MeV) & (F.CHILD(2, F.P) > 5 * GeV)
    comb12_cut_add = Dst_12 | tight2

    line_alg = b_builder.make_tbc2cx(
        particles=[Dz, pion, kaon],
        descriptors=["Xi_bc0 -> D0 pi+ K-", "Xi_bc~0 -> D0 pi- K+"],
        sum_pt_hbach_min=2.0 * GeV,
        comb12_cut_add=comb12_cut_add,
    )
    return line_alg


#############################################################################
# Form the Tbc -> D0 pi+ pi-, D0 --> Kpi & K3pi
##############################################################################


@check_process
def make_TbcToD0PipPim_D0ToKPiOrKPiPiPi(process):
    Dz = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
    pion = basic_builder.make_tightpid_soft_pions()

    ### applying hard cuts on pion if it's not forming D* with any of D0
    Dst_12 = (F.MASS - F.CHILD(1, F.MASS)) < 150 * MeV
    Dst_13 = F.SUBCOMB(Functor=F.MASS, Indices=[1, 3]) - F.CHILD(1, F.MASS) < 150 * MeV

    tight2 = (F.CHILD(2, F.PT) > 500 * MeV) & (F.CHILD(2, F.P) > 5 * GeV)
    tight3 = (F.CHILD(3, F.PT) > 500 * MeV) & (F.CHILD(3, F.P) > 5 * GeV)

    comb12_cut_add = Dst_12 | tight2
    comb_cut_add = Dst_13 | tight3

    line_alg = b_builder.make_tbc2cx(
        particles=[Dz, pion, pion],
        descriptors=["Xi_bc0 -> D0 pi+ pi-"],
        sum_pt_hbach_min=2.0 * GeV,
        comb12_cut_add=comb12_cut_add,
        comb_cut_add=comb_cut_add,
    )
    return line_alg
