###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import Functors as F
from PyConf import configurable
from RecoConf.standard_particles import (
    make_down_pions,
    make_long_pions,
    make_merged_pi0s,
    make_photons,
    make_ttrack_pions,
    make_up_pions,
)

from Hlt2Conf.isolation import extra_outputs_for_isolation


@configurable
def select_parts_for_isolation(
    name,
    candidates,
    cut=(F.DR2 < 2.5),
    LongTrackIso=True,
    TTrackIso=False,
    DownstreamTrackIso=False,
    UpstreamTrackIso=False,
    NeutralIso=True,
    PiZerosIso=False,
):
    """
    Add to the extra_outputs different kind of isolations by properly setting the given flag
    Args:
        candidates: List of containers of reference particles to relate extra particles
        cut: Predicate to select extra information to persist. Be default: cone geometry with max dr2=1 and extra particles not signal
        LongTrackIso: Boolean value to make isolation with long tracks
        TTrackIso: Boolean value to make isolation with tt tracks
        DownstreamTrackIso: Boolean value to make isolation with downstream tracks
        UpstreamTrackIso: Boolean value to make isolation with upstream tracks
        NeutralIso: Boolean value to make isolation with neutral particles
        PiZerosIso: Boolean value to make isolation with merged pi0 -> gamma gamma
    """
    extra_outputs = []

    if LongTrackIso:
        extra_outputs += extra_outputs_for_isolation(
            name=name + "_LongTrackIsolation",
            extra_particles=make_long_pions(),
            ref_particles=candidates,
            selection=cut,
        )
    if TTrackIso:
        extra_outputs += extra_outputs_for_isolation(
            name=name + "_TTrackIsolation",
            extra_particles=make_ttrack_pions(),
            ref_particles=candidates,
            selection=cut,
        )
    if DownstreamTrackIso:
        extra_outputs += extra_outputs_for_isolation(
            name=name + "_DownstreamTrackIsolation",
            extra_particles=make_down_pions(),
            ref_particles=candidates,
            selection=cut,
        )
    if UpstreamTrackIso:
        extra_outputs += extra_outputs_for_isolation(
            name=name + "_UpstreamTrackIsolation",
            extra_particles=make_up_pions(),
            ref_particles=candidates,
            selection=cut,
        )
    if NeutralIso:
        extra_outputs += extra_outputs_for_isolation(
            name=name + "_NeutralIsolation",
            extra_particles=make_photons(),
            ref_particles=candidates,
            selection=cut,
        )
    if PiZerosIso:
        extra_outputs += extra_outputs_for_isolation(
            name=name + "_PiZerosIsolation",
            extra_particles=make_merged_pi0s(),
            ref_particles=candidates,
            selection=cut,
        )
    return extra_outputs


@configurable
def make_iso_particles(
    line_alg,
    name="B",
    coneangle=1.0,
    LongTrackIso=True,
    TTrackIso=False,
    DownstreamTrackIso=False,
    UpstreamTrackIso=True,
    NeutralIso=True,
    PiZerosIso=False,
):
    cut = (F.DR2 < coneangle) & ~F.FIND_IN_TREE()

    iso_parts = select_parts_for_isolation(
        name=name,
        candidates=line_alg,
        cut=cut,
        LongTrackIso=LongTrackIso,
        TTrackIso=TTrackIso,
        DownstreamTrackIso=DownstreamTrackIso,
        UpstreamTrackIso=UpstreamTrackIso,
        NeutralIso=NeutralIso,
        PiZerosIso=PiZerosIso,
    )

    return iso_parts
