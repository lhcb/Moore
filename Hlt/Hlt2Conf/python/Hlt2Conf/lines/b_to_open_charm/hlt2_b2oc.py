###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Booking of B2OC hlt2 lines, notice PROCESS = 'hlt2'

Usage:
add line names in corresponding list/dict,
or register lines separately at the very end.

Non trivial imports:
prefilters and line_alg ("bare" line builders) like b_to_dh.make_BdToDsmK_DsmToHHH

Output:
updated dictionary of hlt2_lines

To be noted:
"bare" line builders, like like b_to_dh.make_BdToDsmK_DsmToHHH, have PROCESS as
argument to allow ad hoc settings

"""

from Moore.config import Hlt2Line, register_line_builder
from Moore.streams import DETECTORS
from PyConf import configurable

from Hlt2Conf.lines.b_to_open_charm import (
    b_to_cbaryon_h,
    b_to_cbaryon_hh,
    b_to_cbaryons_h,
    b_to_dd,
    b_to_ddh,
    b_to_ddh_standalone,
    b_to_ddhh,
    b_to_dh,
    b_to_dhh,
    b_to_dhhh,
    b_to_dll,
    b_to_dmunu,
    b_to_dx_ltu,
    bbaryon_to_cbaryon_d,
    bbaryon_to_cbaryon_dh,
    bbaryon_to_cbaryon_dhh,
    bbaryon_to_cbaryon_h,
    bbaryon_to_cbaryon_hh,
    bbaryon_to_cbaryon_hhh,
    bbaryon_to_cbaryons_h,
    bbaryon_to_lightbaryon_d,
    bbaryon_to_lightbaryon_dd,
    bbaryon_to_lightbaryon_ddh,
    bbaryon_to_lightbaryon_dh,
    prefilters,
)
from Hlt2Conf.lines.b_to_open_charm.filters import b_sigmanet_filter
from Hlt2Conf.lines.b_to_open_charm.persistency import make_iso_particles
from Hlt2Conf.lines.b_to_open_charm.utils import update_makers, validate_config

PROCESS = "hlt2"
hlt2_lines = {}
hlt2_calib_lines = {}

##############################################
# Read and store all line makers in one dict #
##############################################

line_makers = {}
update_makers(line_makers, b_to_dh)
update_makers(line_makers, b_to_dx_ltu)
update_makers(line_makers, b_to_dhh)
update_makers(line_makers, b_to_dll)
update_makers(line_makers, b_to_dmunu)
update_makers(line_makers, b_to_dhhh)
update_makers(line_makers, b_to_dd)
update_makers(line_makers, b_to_ddh)
update_makers(line_makers, b_to_ddh_standalone)
update_makers(line_makers, b_to_ddhh)
update_makers(line_makers, b_to_cbaryon_h)
update_makers(line_makers, b_to_cbaryon_hh)
update_makers(line_makers, b_to_cbaryons_h)
update_makers(line_makers, bbaryon_to_cbaryons_h)
update_makers(line_makers, bbaryon_to_cbaryon_h)
update_makers(line_makers, bbaryon_to_cbaryon_hh)
update_makers(line_makers, bbaryon_to_cbaryon_hhh)
update_makers(line_makers, bbaryon_to_cbaryon_d)
update_makers(line_makers, bbaryon_to_cbaryon_dh)
update_makers(line_makers, bbaryon_to_cbaryon_dhh)
update_makers(line_makers, bbaryon_to_lightbaryon_d)
update_makers(line_makers, bbaryon_to_lightbaryon_dh)
update_makers(line_makers, bbaryon_to_lightbaryon_dd)
update_makers(line_makers, bbaryon_to_lightbaryon_ddh)

###default_ft_decoding_version.global_bind(value=6)

############################################
# Define functions for line booking        #
# Make it possible to register lines       #
# outside this file (e.g. in test scripts) #
############################################


# All-in-one B2OC Hlt2 line builder
@configurable
def make_hlt2_lines(
    line_dict=hlt2_lines,
    line_makers=line_makers,
    all_lines=None,
    default_MVACut=0.25,
    extra_config={},
):
    if not all_lines:
        return

    custom_prescales = extra_config.get("prescale", {})
    custom_mva = extra_config.get("mva", {})
    flavour_tagging = extra_config.get("flavour_tagging", [])
    pv_unbiasing = extra_config.get("pv_unbiasing", [])
    isolation = extra_config.get("isolation", [])

    for decay in all_lines:
        # default configs
        prescale = 1.0
        MVACut = -1
        include_ft = False
        include_pv_tracks = False
        include_isolation = False

        # custom configs
        if decay in custom_prescales.keys():
            prescale = custom_prescales[decay]
        if decay in flavour_tagging:
            include_ft = True
            include_pv_tracks = True
        if decay in custom_mva.keys():
            MVACut = custom_mva[decay]  # can be None or a non-default value
        if decay in pv_unbiasing:
            include_pv_tracks = True
        if decay in isolation:
            include_isolation = True

        @register_line_builder(line_dict)
        def make_hlt2_line(
            name="Hlt2B2OC_%s" % decay,
            maker_name="make_%s" % decay,
            prescale=prescale,
            MVACut=MVACut,
            include_ft=include_ft,
            include_pv_tracks=include_pv_tracks,
            include_isolation=include_isolation,
        ):
            extra_outputs = []
            if MVACut == -1:  # use default MVA
                b = line_makers[maker_name](process=PROCESS)
                line_alg = b_sigmanet_filter(b, default_MVACut)
            elif MVACut is None:  # do not apply any MVA
                line_alg = line_makers[maker_name](process=PROCESS)
            else:  # use custom MVA
                line_alg = line_makers[maker_name](process=PROCESS, MVACut=MVACut)
            if include_isolation:
                extra_outputs += make_iso_particles(line_alg, name="B")
            return Hlt2Line(
                name=name,
                prescale=prescale,
                algs=prefilters.b2oc_prefilters() + [line_alg],
                extra_outputs=extra_outputs,
                tagging_particles=include_ft,
                pv_tracks=include_pv_tracks,
            )


###########################################
# Lists and dicts of lines to register    #
# Authors should add decays here          #
# NO `Hlt2B2OC_` prefix or `` suffix #
###########################################

# list of all lines
all_lines = [
    # lines from b_to_dh
    "BuToD0Pi_D0ToHH",
    "BuToD0K_D0ToHH",
    "BuToD0Pi_D0ToHHWS",
    "BuToD0K_D0ToHHWS",
    "BdToDmPi_DmToPimPimKp",
    "BdToDmK_DmToPimPimKp",
    "BdToDsmPi_DsmToKpKmPim",
    "BdToDsmK_DsmToKpKmPim",
    "BuToD0Pi_D0ToKsLLPi0Resolved",
    "BuToD0Pi_D0ToKsDDPi0Resolved",
    #'BuToD0Pi_D0ToKsLLPi0Merged',
    #'BuToD0Pi_D0ToKsDDPi0Merged',
    "BuToD0K_D0ToKsLLPi0Resolved",
    "BuToD0K_D0ToKsDDPi0Resolved",
    #'BuToD0K_D0ToKsLLPi0Merged',
    #'BuToD0K_D0ToKsDDPi0Merged',
    "BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHH",
    "BuToDst0Pi_Dst0ToD0Gamma_D0ToHH",
    "BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHH",
    "BuToDst0K_Dst0ToD0Gamma_D0ToHH",
    "BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHHWS",
    "BuToDst0Pi_Dst0ToD0Gamma_D0ToHHWS",
    "BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHHWS",
    "BuToDst0K_Dst0ToD0Gamma_D0ToHHWS",
    "BuToD0Pi_D0ToKsLLHH",
    "BuToD0Pi_D0ToKsDDHH",
    "BuToD0K_D0ToKsLLHH",
    "BuToD0K_D0ToKsDDHH",
    "BuToD0Pi_D0ToKsLLHHWS",
    "BuToD0Pi_D0ToKsDDHHWS",
    "BuToD0K_D0ToKsLLHHWS",
    "BuToD0K_D0ToKsDDHHWS",
    "BuToD0K_D0ToHHPi0Resolved",
    "BuToD0Pi_D0ToHHPi0Resolved",
    #'BuToD0K_D0ToHHPi0Merged',
    #'BuToD0Pi_D0ToHHPi0Merged',
    "BuToD0Pi_D0ToHHPi0ResolvedWS",
    #'BuToD0Pi_D0ToHHPi0MergedWS',
    "BuToD0K_D0ToHHPi0ResolvedWS",
    #'BuToD0K_D0ToHHPi0MergedWS',
    "BuToD0Pi_D0ToHHHH",
    "BuToD0K_D0ToHHHH",
    "BuToD0Pi_D0ToHHHHWS",
    "BuToD0K_D0ToHHHHWS",
    "BuToD0Pi_D0ToKsLLHHPi0Resolved",
    "BuToD0Pi_D0ToKsDDHHPi0Resolved",  # rate checked to be fine, jessy.daniel@cern.ch
    #'BuToD0Pi_D0ToKsLLHHPi0Merged',
    #'BuToD0Pi_D0ToKsDDHHPi0Merged',
    "BuToD0K_D0ToKsLLHHPi0Resolved",
    "BuToD0K_D0ToKsDDHHPi0Resolved",  # rate checked to be fine, jessy.daniel@cern.ch
    #'BuToD0K_D0ToKsLLHHPi0Merged',
    #'BuToD0K_D0ToKsDDHHPi0Merged',
    "BuToD0Pi_D0ToKsLLHHWSPi0Resolved",
    "BuToD0Pi_D0ToKsDDHHWSPi0Resolved",
    #'BuToD0Pi_D0ToKsLLHHWSPi0Merged',
    #'BuToD0Pi_D0ToKsDDHHWSPi0Merged',
    "BuToD0K_D0ToKsLLHHWSPi0Resolved",
    "BuToD0K_D0ToKsDDHHWSPi0Resolved",
    #'BuToD0K_D0ToKsLLHHWSPi0Merged',
    #'BuToD0K_D0ToKsDDHHWSPi0Merged',
    "TbcToDpKm_DpToKmPipPip",
    "TbcToD0KsLL_D0ToKPiOrKPiPiPi",
    "TbcToD0KsDD_D0ToKPiOrKPiPiPi",
    # lines from b_to_dx_ltu
    "BdToDmPi_DmToPimPimKp_LTU",
    "BdToDsmK_DsmToKpKmPim_LTU",
    "BdToDsmKPiPi_DsmToKmKpPim_LTU",
    # lines from b_to_dhh
    "BdToD0PiPi_D0ToHH",
    "BdToD0KPi_D0ToHH",
    "BdToD0KK_D0ToHH",
    "BdToD0PbarP_D0ToHH",
    "BdToD0PiPi_D0ToKsLLHH",
    "BdToD0PiPi_D0ToKsDDHH",
    "BdToD0KPi_D0ToKsLLHH",
    "BdToD0KPi_D0ToKsDDHH",
    "BdToD0KK_D0ToKsLLHH",
    "BdToD0KK_D0ToKsDDHH",
    #'BdToD0PiPi_D0ToHHPi0Resolved',
    #'BdToD0PiPi_D0ToHHPi0Merged',
    #'BdToD0KPi_D0ToHHPi0Resolved',
    #'BdToD0KPi_D0ToHHPi0Merged',
    #'BdToD0KK_D0ToHHPi0Resolved',
    #'BdToD0KK_D0ToHHPi0Merged',
    "BdToD0PbarP_D0ToHHHH",
    "BdToD0PiPiWS_D0ToHH",
    "BdToD0KPiWS_D0ToHH",
    "BdToD0KKWS_D0ToHH",
    "BdToD0PbarPWS_D0ToHH",
    "BdToD0PiPiWS_D0ToKsLLHH",
    "BdToD0PiPiWS_D0ToKsDDHH",
    "BdToD0KPiWS_D0ToKsLLHH",
    "BdToD0KPiWS_D0ToKsDDHH",
    "BdToD0KKWS_D0ToKsLLHH",
    "BdToD0KKWS_D0ToKsDDHH",
    "BdToD0PbarPWS_D0ToHHHH",
    #'BdToDst0KK_Dst0ToD0Gamma_D0ToHH',
    #'BdToDst0KK_Dst0ToD0Gamma_D0ToHHHH',
    #'BdToDst0KK_Dst0ToD0Gamma_D0ToHHPi0Merged',
    #'BdToDst0KK_Dst0ToD0Gamma_D0ToHHPi0Resolved',
    #'BdToDst0KK_Dst0ToD0Gamma_D0ToKsDDHH',
    #'BdToDst0KK_Dst0ToD0Gamma_D0ToKsLLHH',
    #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToHH',
    #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToHHHH',
    #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged',
    #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved',
    #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToKsDDHH',
    #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToKsLLHH',
    #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToHH',
    #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToHHHH',
    #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToHHPi0Merged',
    #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToHHPi0Resolved',
    #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToKsDDHH',
    #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToKsLLHH',
    #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToHH',
    #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToHHHH',
    #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged',
    #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved',
    #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToKsDDHH',
    #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToKsLLHH',
    #'BdToDst0KPi_Dst0ToD0Gamma_D0ToHH',
    #'BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToHH',
    #'BdToDst0KPi_Dst0ToD0Gamma_D0ToKsLLHH',
    #'BdToDst0KPi_Dst0ToD0Gamma_D0ToKsDDHH',
    #'BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToKsLLHH',
    #'BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToKsDDHH',
    #'BdToDst0KPi_Dst0ToD0Gamma_D0ToHHPi0Resolved',
    #'BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved',
    #'BdToDst0KPi_Dst0ToD0Gamma_D0ToHHPi0Merged',
    #'BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged',
    #'BdToD0PiPiWS_D0ToHHPi0Resolved',
    #'BdToD0PiPiWS_D0ToHHPi0Merged',
    #'BdToD0KPiWS_D0ToHHPi0Resolved',
    #'BdToD0KPiWS_D0ToHHPi0Merged',
    #'BdToD0KKWS_D0ToHHPi0Resolved',
    #'BdToD0KKWS_D0ToHHPi0Merged',
    #'BdToDst0KPi_Dst0ToD0Gamma_D0ToHHHH',
    #'BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToHHHH',
    #'BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToHH',
    #'BdToDst0KPiWS_Dst0ToD0Gamma_D0ToHH',
    #'BdToDst0KKWS_Dst0ToD0Gamma_D0ToHH',
    #'BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToHH',
    #'BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToHH',
    #'BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToHH',
    #'BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToKsLLHH',
    #'BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToKsDDHH',
    #'BdToDst0KPiWS_Dst0ToD0Gamma_D0ToKsLLHH',
    #'BdToDst0KPiWS_Dst0ToD0Gamma_D0ToKsDDHH',
    #'BdToDst0KKWS_Dst0ToD0Gamma_D0ToKsLLHH',
    #'BdToDst0KKWS_Dst0ToD0Gamma_D0ToKsDDHH',
    #'BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToKsLLHH',
    #'BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToKsDDHH',
    #'BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToKsLLHH',
    #'BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToKsDDHH',
    #'BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToKsLLHH',
    #'BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToKsDDHH',
    #'BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToHHPi0Resolved',
    #'BdToDst0KPiWS_Dst0ToD0Gamma_D0ToHHPi0Resolved',
    #'BdToDst0KKWS_Dst0ToD0Gamma_D0ToHHPi0Resolved',
    #'BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved',
    #'BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved',
    #'BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved',
    #'BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToHHPi0Merged',
    #'BdToDst0KPiWS_Dst0ToD0Gamma_D0ToHHPi0Merged',
    #'BdToDst0KKWS_Dst0ToD0Gamma_D0ToHHPi0Merged',
    #'BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged',
    #'BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged',
    #'BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged',
    #'BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToHHHH',
    #'BdToDst0KPiWS_Dst0ToD0Gamma_D0ToHHHH',
    #'BdToDst0KKWS_Dst0ToD0Gamma_D0ToHHHH',
    #'BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToHHHH',
    #'BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToHHHH',
    #'BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToHHHH',
    #'BdToDsstmKsLLPi_DsstmToDsmGamma_DsmToHHH',
    # 'BdToDsstmKsDDPi_DsstmToDsmGamma_DsmToHHH',
    "BuToDmPiPi_DmToHHH",
    "BuToDmKPi_DmToHHH",
    "BuToDmKK_DmToHHH",
    "BuToDpPiPi_DpToHHH",
    "BuToDpKK_DpToHHH",
    "BuToDpPbarP_DpToHHH",
    "BuToDsmPiPi_DsmToHHH",
    "BuToDsmKPi_DsmToHHH",
    "BuToDsmKK_DsmToHHH",
    "BuToDspPiPi_DspToHHH",
    "BuToDspKPi_DspToHHH",
    "BuToDspKK_DspToHHH",
    "BuToDspPbarP_DspToHHH",
    "BuToDstmPiPi_DstmToD0Pi_D0ToHH",
    "BuToDstmKPi_DstmToD0Pi_D0ToHH",
    "BuToDstmKK_DstmToD0Pi_D0ToHH",
    "BuToDstpPiPi_DstpToD0Pi_D0ToHH",
    "BuToDstpKPi_DstpToD0Pi_D0ToHH",
    "BuToDstpKK_DstpToD0Pi_D0ToHH",
    "BuToDstpPbarP_DstpToD0Pi_D0ToHH",
    "TbcToD0KmPip_D0ToKPiOrKPiPiPi",
    "TbcToD0PipPim_D0ToKPiOrKPiPiPi",
    # lines from b_to_dmunu
    "BdToDstmMuNu_DstmToD0Pi_D0ToKsLLHH",
    "BdToDstmMuNu_DstmToD0Pi_D0ToKsDDHH",
    "BdToDstmMuNu_DstmToD0Pi_D0ToHHHH",
    # lines from b_to_dd
    "BuToD0Dp_D0ToHH_DpToHHH",
    "BuToD0Dp_D0ToHHHH_DpToHHH",
    "BuToD0Dsp_D0ToHH_DspToHHH",
    "BuToD0Dsp_D0ToHHHH_DspToHHH",
    "BuToDstD0_DstToD0Pi_D0ToHH_D0ToHH",
    "BuToDstD0_DstToD0Pi_D0ToHH_D0ToHHHH",
    "BuToDstD0_DstToD0Pi_D0ToHHHH_D0ToHH",
    "BuToDstD0_DstToD0Pi_D0ToHHHH_D0ToHHHH",
    "BcToD0Dp_D0ToHH_DpToHHH",
    "BcToD0Dp_D0ToHHHH_DpToHHH",
    "BcToD0Dp_D0ToKsLLHH_DpToHHH",
    "BcToD0Dp_D0ToKsDDHH_DpToHHH",
    "BcToD0Dsp_D0ToHH_DspToHHH",
    "BcToD0Dsp_D0ToHHHH_DspToHHH",
    "BcToD0Dsp_D0ToKsLLHH_DspToHHH",
    "BcToD0Dsp_D0ToKsDDHH_DspToHHH",
    "BcToDstD0_DstToD0Pi_D0ToHH_D0ToHH",
    "BcToDstD0_DstToD0Pi_D0ToHH_D0ToHHHH",
    "BcToDstD0_DstToD0Pi_D0ToHH_D0ToKsLLHH",
    "BcToDstD0_DstToD0Pi_D0ToHH_D0ToKsDDHH",
    "BcToDstD0_DstToD0Pi_D0ToHHHH_D0ToHH",
    "BcToDstD0_DstToD0Pi_D0ToHHHH_D0ToHHHH",
    "BcToDstD0_DstToD0Pi_D0ToHHHH_D0ToKsDDHH",
    "BcToDstD0_DstToD0Pi_D0ToHHHH_D0ToKsLLHH",
    "TbcToD0D0_D0ToKPiOrKPiPiPi",
    # lines from b_to_ddh
    "BdToDstD0K_DstToD0Pi_D0ToHH_D0ToHH",
    "TbcToD0DpPim_D0ToKPiOrKPiPiPi",
    "BuToD0Ds2460p_Ds2460pToDsPiPi_DsToKHH_D0ToKPi",
    "BuToD0Ds2460p_Ds2460pToDsPiPi_DsToKHH_D0ToKPiPiPi",
    "BdToDmDs2460p_Ds2460pToDsPiPi_DsToKHH_DmToHHH",
    "BdToDstmDs2460p_DstmToD0Pi_Ds2460pToDsPiPi_DsToKHH",
    "BdToDsDs2460p_Ds2460pToDsPiPi_DsToKKPi",
    "BdToD0D0Kst_D0ToHH",
    "BdToD0D0Kst_D0ToHH_D0ToKPiPiPi",
    "BdToD0D0Kst_D0ToHHHH",
    "BdToD0D0Kst_D0ToHH_D0ToKsLLHH",
    "BdToD0D0Kst_D0ToHH_D0ToKsDDHH",
    "BdToD0D0Kst_D0ToKsLLHH",
    "BdToD0D0Kst_D0ToKsLLHH_D0ToKsDDHH",
    "BdToD0D0Kst_D0ToKsDDHH",
    "BdToD0D0Kst_D0ToKsLLHH_D0ToHHHH",
    "BdToD0D0Kst_D0ToKsDDHH_D0ToHHHH",
    "BuToD0D0K_D0ToHH",
    "BuToD0D0K_D0ToKsLLHH",
    "BuToD0D0K_D0ToKsDDHH",
    "BuToD0D0K_D0ToHHHH",
    "BuToD0D0K_D0ToHH_D0ToKsLLHH",
    "BuToD0D0K_D0ToHH_D0ToKsDDHH",
    "BuToD0D0K_D0ToHH_D0ToKPiPiPi",
    "BuToD0D0K_D0ToKsLLHH_D0ToKsDDHH",
    "BuToD0D0K_D0ToKsLLHH_D0ToHHHH",
    "BuToD0D0K_D0ToKsDDHH_D0ToHHHH",
    "BuToD0D0Pi_D0ToHH",
    "BuToD0D0Pi_D0ToKsLLHH",
    "BuToD0D0Pi_D0ToKsDDHH",
    "BuToD0D0Pi_D0ToHHHH",
    "BuToD0D0Pi_D0ToHH_D0ToKsLLHH",
    "BuToD0D0Pi_D0ToHH_D0ToKsDDHH",
    "BuToD0D0Pi_D0ToHH_D0ToKPiPiPi",
    "BuToD0D0Pi_D0ToKsLLHH_D0ToKsDDHH",
    "BuToD0D0Pi_D0ToKsLLHH_D0ToHHHH",
    "BuToD0D0Pi_D0ToKsDDHH_D0ToHHHH",
    "BdToD0DK_D0ToKPiOrKPiPiPi_DToHHH",
    "BdToD0DPi_D0ToKPi_DToHHH",
    "BdToD0DPi_D0ToKPiPiPi_DToHHH",
    # lines from b_to_ddh_standalone
    "BdToDpDmKsDD_DpToHHH",
    "BdToDpDmKsLL_DpToHHH",
    "BdToDspDmKsDD_DpToHHH",
    "BdToDspDmKsLL_DpToHHH",
    "BdToDstDmKsDD_DstToD0Pi_D0ToKPiOrKPiPiPi_DmToHHH",
    "BdToDstDmKsLL_DstToD0Pi_D0ToKPiOrKPiPiPi_DmToHHH",
    "BdToDstDsmKsDD_DstToD0Pi_D0ToKPiOrKPiPiPi_DmToHHH",
    "BdToDstDsmKsLL_DstToD0Pi_D0ToKPiOrKPiPiPi_DmToHHH",
    "BdToDstpDstmKsDD_DstpToD0Pi_D0ToKPiorKPiPiPi",
    "BdToDstpDstmKsLL_DstpToD0Pi_D0ToKPiorKPiPiPi",
    "BdToD0D0KsDD_D0ToHH",
    "BdToD0D0KsDD_D0ToKsLLHH",
    "BdToD0D0KsDD_D0ToKsDDHH",
    "BdToD0D0KsDD_D0ToHHHH",
    "BdToD0D0KsDD_D0ToHH_D0ToKsLLHH",
    "BdToD0D0KsDD_D0ToHH_D0ToKsDDHH",
    "BdToD0D0KsDD_D0ToHH_D0ToHHHH",
    "BdToD0D0KsDD_D0ToKsLLHH_D0ToKsDDHH",
    "BdToD0D0KsDD_D0ToKsLLHH_D0ToHHHH",
    "BdToD0D0KsDD_D0ToKsDDHH_D0ToHHHH",
    "BdToD0D0KsLL_D0ToHH",
    "BdToD0D0KsLL_D0ToKsLLHH",
    "BdToD0D0KsLL_D0ToKsDDHH",
    "BdToD0D0KsLL_D0ToHHHH",
    "BdToD0D0KsLL_D0ToHH_D0ToKsLLHH",
    "BdToD0D0KsLL_D0ToHH_D0ToKsDDHH",
    "BdToD0D0KsLL_D0ToHH_D0ToHHHH",
    "BdToD0D0KsLL_D0ToKsLLHH_D0ToKsDDHH",
    "BdToD0D0KsLL_D0ToKsLLHH_D0ToHHHH",
    "BdToD0D0KsLL_D0ToKsDDHH_D0ToHHHH",
    "BuToDpDmK_DpToHHH",
    "BdToDspDmKst_DspToKKPi",
    "BuToDstDK_DstToD0Pi_D0ToKPiOrKPiPiPi_DToHHH",
    "BuToDspDsmK_DspToKHH",
    "BuToDpDmPi_DpToHHH",
    "BuToD0DpKsDD_D0ToKPiOrKPiPiPi_DpToHHH",
    "BuToD0DpKsLL_D0ToKPiOrKPiPiPi_DpToHHH",
    "BuToDstpD0KsDD_DstpToD0Pi_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi",
    "BuToDstpD0KsLL_DstpToD0Pi_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi",
    "BdToDstpDstmPhi_DstpToD0Pi_D0ToKPi_D0ToKPi",
    "BdToDstpDstmPhi_DstpToD0Pi_D0ToKPi_D0ToKPiPiPi",
    "BdToDstpDstmPhi_DstpToD0Pi_D0ToKPiPiPi_D0ToKPi",
    "BdToDstpDstmPhi_DstpToD0Pi_D0ToKPiPiPi_D0ToKPiPiPi",
    "BdToDstD0K_DstToD0Pi_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi",
    "BuToDstpDstmK_DstToD0Pi_D0ToKPiOrKPiPiPi",
    "BdToDsD0Pi_DsToHHH_D0ToKPi",
    "BdToDsD0Pi_DsToHHH_D0ToKPiPiPi",
    "BdToDsD0K_DsToHHH_D0ToKPi",
    "BdToDsD0K_DsToHHH_D0ToKPiPiPi",
    "BuToDsD0Rho0_DspToKHH_D0ToKPi",
    "BuToDsD0Rho0_DspToKHH_D0ToKPiPiPi",
    "BdToDspDsmRho_DspToKKPi",
    "BdToDstD0Pi_DstToD0Pi_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi",
    "BdToDpDmKst_DpToHHH",
    "BdToDstpDmKst_DstpToD0Pi_D0ToKPiOrKPiPiPi_DmToHHH",
    "BdToDstmDpKst_DstmToD0Pi_D0ToKPiOrKPiPiPi_DpToHHH",
    "BdToDspDsmKst_DspToKKPi",
    "BuToDstDPi_DstToD0Pi_D0ToKPiOrKPiPiPi_DToHHH",
    "BuToDstpDstmPi_DstpToD0Pi_D0ToKPiOrKPiPiPi",
    "BuToDspDsmPi_DspToKKPi",
    "BuToD0DpKst_D0ToKPi_DpToHHH",
    "BuToD0DpKst_D0ToKPiPiPi_DpToHHH",
    "BuToDstpD0Kst_DstpToD0Pi_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi",
    "BuToDspDmPi_DspToHHH_DmToHHH",
    "BuToDstmDspPi_DstmToD0Pi_D0ToKPiOrKPiPiPi_DspToHHH",
    "BuToDsD0Phi_DspToKHH_D0ToKPi",
    "BuToDsD0Phi_DspToKHH_D0ToKPiPiPi",
    "BdToD0D0Phi_D0ToHH",
    "BdToD0D0Phi_D0ToKsLLHH",
    "BdToD0D0Phi_D0ToKsDDHH",
    "BdToD0D0Phi_D0ToHHHH",
    "BdToD0D0Phi_D0ToHH_D0ToKsLLHH",
    "BdToD0D0Phi_D0ToHH_D0ToKsDDHH",
    "BdToD0D0Phi_D0ToHH_D0ToHHHH",
    "BdToD0D0Phi_D0ToKsLLHH_D0ToKsDDHH",
    "BdToD0D0Phi_D0ToKsLLHH_D0ToHHHH",
    "BdToD0D0Phi_D0ToKsDDHH_D0ToHHHH",
    "BdToDpDmPhi_DpToHHH",
    "BdToDsDPhi_DsToHHH_DToHHH",
    "BdToDspDsmPhi_DspToHHH",
    "BdToDstDsPhi_DstToD0Pi_D0ToKPiOrKPiPiPi_DsToHHH",
    "BuToD0DPhi_D0ToKPi_DToHHH",
    "BuToD0DPhi_D0ToKPiPiPi_DToHHH",
    "BuToDstD0Phi_DstToD0Pi_D0ToKPiorKPiPiPi_D0ToKPiorKPiPiPi",
    "BdToDstDPhi_DstToD0Pi_D0ToKPiorKPiPiPi_DToHHH",
    "BuToDstpDstmKst_DstpToD0Pi_D0ToKPiOrKPiPiPi",
    "BdToDspDmRho0_DspToKKPi_DmToHHH",
    "BdToDstmDspRho0_DstmToD0Pi_D0ToKPiOrKPiPiPi_DspToKHH",
    # lines from b_to_ddhh
    "TbcToD0D0PipPim_D0ToKPiOrKPiPiPi",
    # lines from b_to_cbaryon_h
    "TbcToXiccpP_XiccpToXic0Pi_Xic0ToPKKPi",
    "TbcToXiccpP_XiccpToLcpKmPip_LcpToPKPi",
    "TbcToXiccpP_XiccpToPDpKm_DpToPipPipKm",
    # lines from b_to_cbaryon_hh
    "BdToXic0PPi_Xic0ToPKKPi",
    "BdToXic0PK_Xic0ToPKKPi",
    "BuToLcmPPi_LcmToPKPi",
    "BuToLcpPPi_LcpToPKPi",
    "BuToLcmPK_LcmToPKPi",
    "BuToLcpPK_LcpToPKPi",
    "BuToXicmPPi_XicmToPKPi",
    "BuToXicpPPi_XicpToPKPi",
    "BuToXicmPK_XicmToPKPi",
    "BuToXicpPK_XicpToPKPi",
    # lines from b_to_cbaryons_h
    "BuToLcpLcmK_LcpToPKPi",
    "BuToLcpXicmPi_LcpToPKPi_XicmToPKPi",
    "BdToScmmLcpK_ScmmToLcmPi_LcmToPKPi_LcpToPKPi",
    # lines from bbaryon_to_cbaryons_h
    "LbToLcpLcmN0",
    "LbToPbarPN0",
    # lines from bbaryon_to_cbaryon_h
    "LbToLcpPi_LcpToPKPi",
    "LbToLcpPi_LcpToPPiPi",
    "LbToLcpPi_LcpToPKK",
    "LbToLcpK_LcpToPKPi",
    "LbToLcpPiWS_LcpToPPiPi",
    "LbToLcpPiWS_LcpToPKK",
    "LbToLcpPi_LcpToLambdaLLPi",
    "LbToLcpPi_LcpToLambdaDDPi",
    "LbToLcpPi_LcpToLambdaLLK",
    "LbToLcpPi_LcpToLambdaDDK",
    "LbToLcpK_LcpToLambdaLLPi",
    "LbToLcpK_LcpToLambdaDDPi",
    "LbToLcpK_LcpToLambdaLLK",
    "LbToLcpK_LcpToLambdaDDK",
    "LbToLcpPi_LcpToPKsLL",
    "LbToLcpPi_LcpToPKsDD",
    "LbToLcpK_LcpToPKsLL",
    "LbToLcpK_LcpToPKsDD",
    "LbToXicpK_XicpToPKPi",
    "Xib0ToXicpPi_XicpToPKPi",
    "Xib0ToXicpK_XicpToPKPi",
    "XibmToXic0Pi_Xic0ToPKKPi",
    "XibmToXic0PiWS_Xic0ToPKKPi",
    "XibmToXic0K_Xic0ToPKKPi",
    "OmbmToOmc0Pi_Omc0ToPKKPi",
    "OmbmToOmc0K_Omc0ToPKKPi",
    "OmbmToOmc0PiWS_Omc0ToPKKPi",
    "Xibc0ToXiccpPi_XiccpToXic0Pi_Xic0ToPKKPi",
    "Xibc0ToXiccpPi_XiccpToLcpKmPip_LcpToPKPi",
    "Xibc0ToXiccpPi_XiccpToPDpKm_DpToPipPipKm",
    "Xibc0ToLcpPi_LcpToPKPi",
    "Xibc0ToLcpK_LcpToPKPi",
    "Xibc0ToXicpPi_XicpToPKPi",
    "XibcpToXiccppPi_XiccppToXicpPi_XicpToPKPi",
    "XibcpToXiccppPi_XiccppToLcpKPiPi_LcpToPKPi",
    "XibcpToXic0Pi_Xic0ToPKKPi",
    "Ombc0ToXicpK_XicpToPKPi",
    # lines from bbaryon_to_cbaryon_hh
    "LbToXic0PiPi_Xic0ToPKKPi",
    "LbToXic0KPi_Xic0ToPKKPi",
    "LbToXic0KK_Xic0ToPKKPi",
    "LbToOmc0PiPi_Omc0ToPKKPi",
    "LbToOmc0KPi_Omc0ToPKKPi",
    "LbToOmc0KK_Omc0ToPKKPi",
    "XibmToLcpPiPi_LcpToPKPi",
    "XibmToLcpKPi_LcpToPKPi",
    "XibmToLcpKK_LcpToPKPi",
    "XibmToXicpPiPi_XicpToPKPi",
    "XibmToXicpKPi_XicpToPKPi",
    "Xibc0ToXic0PiPi_Xic0ToPKKPi",
    "XibcpToLcpKmPip_LcpToPKPi",
    "XibcpToXicpPiPi_XicpToPKPi",
    "XibcpToLcpPiPi_LcpToPKPi",
    "Ombc0ToXic0KmPip_Xic0ToPKKPi",
    "OmbmToXicpKPi_XicpToPKPi",
    "XibmToLcpKPiWS_LcpToPKPi",
    "OmbmToXicpKPiWS_XicpToPKPi",
    # lines from bbaryon_to_cbaryon_hhh
    "LbToLcpPiPiPi_LcpToPKK",
    "LbToLcpKPiPi_LcpToPKPi",
    "LbToLcpPiPiPiWS_LcpToPPiPi",
    "LbToLcpPiPiPiWS_LcpToPKK",
    "XibmToXic0PiPiPi_Xic0ToPKKPi",
    "XibmToXic0KPiPi_Xic0ToPKKPi",
    "XibmToXic0KKPi_Xic0ToPKKPi",
    "XibmToXic0PbarPPi_Xic0ToPKKPi",
    "OmbmToOmc0PiPiPi_Omc0ToPKKPi",
    "OmbmToOmc0KPiPi_Omc0ToPKKPi",
    "OmbmToOmc0KKPi_Omc0ToPKKPi",
    "OmbmToOmc0PbarPPi_Omc0ToPKKPi",
    "OmbmToXic0KmPipPim_Omc0ToPKKPi",
    "OmbmToXic0KmPipPimWS_Omc0ToPKKPi",
    "OmbmToOmc0PiPiPiWS_Omc0ToPKKPi",
    # lines from bbaryon_to_cbaryon_d
    "LbToLcpDm_LcpToPKPi_DmToPimPimKp",
    "LbToLcpDsm_LcpToPKPi_DsmToHHH",
    "XibcpToLcpD0_LcpToPKPi_D0ToKPiOrKPiPiPi",
    "Ombc0ToXic0D0_Xic0ToPKKPi_D0ToKPiOrKPiPiPi",
    "LbToLcpDm_LcpToLambdaLLPip_DmToPimPimKp",
    "LbToLcpDm_LcpToLambdaDDPip_DmToPimPimKp",
    "LbToLcpDm_LcpTopKsLL_DmToPimPimKp",
    "LbToLcpDm_LcpTopKsDD_DmToPimPimKp",
    # lines from bbaryon_to_cbaryon_dh
    "Xibc0ToLcpD0Pi_LcpToPKPi_D0ToKPiOrKPiPiPi",
    "Ombc0ToLcpD0K_LcpToPKPi_D0ToKPiOrKPiPiPi",
    # lines from bbaryon_to_cbaryon_dhh
    "XibcpToLcpD0PipPim_LcpToPKPi_D0ToKPiOrKPiPiPi",
    # lines from bbaryon_to_lightbaryon_d
    "LbToLambdaTTD0_D0ToHH",
    "LbToLambdaTTD0_D0ToHHHH",
    "LbToLambdaTTD0_D0ToHHHHWS",
    "LbToLambdaTTD0_D0ToKsLLHH",
    "LbToLambdaTTD0_D0ToKsTTHH",
    "LbToLambdaTTD0_D0ToKsLLHHWS",
    "LbToLambdaLLD0_D0ToHH",
    "LbToLambdaLLD0_D0ToHHHH",
    "LbToLambdaDDD0_D0ToHH",
    "LbToLambdaDDD0_D0ToHHHH",
    "LbToLambdaLLD0_D0ToKsLLHH",
    "LbToLambdaLLD0_D0ToKsDDHH",
    "LbToLambdaDDD0_D0ToKsLLHH",
    "LbToLambdaDDD0_D0ToKsDDHH",
    "LbToLambdaLLD0_D0ToHHHHWS",
    "LbToLambdaDDD0_D0ToHHHHWS",
    "LbToLambdaLLD0_D0ToKsLLHHWS",
    "LbToLambdaLLD0_D0ToKsDDHHWS",
    "LbToLambdaDDD0_D0ToKsLLHHWS",
    "LbToLambdaDDD0_D0ToKsDDHHWS",
    "LbToXimDsp_DspToKKPi_XimToLambdaLLPi",
    "LbToXimDsp_DspToKKPi_XimToLambdaDDPi",
    "Xib0ToXimDp_DpToKPiPi_XimToLambdaLLPi",
    "Xib0ToXimDp_DpToKPiPi_XimToLambdaDDPi",
    "XibmToXimD0_D0ToKPiOrKPiPiPi_XimToLambdaLLPi",
    "XibmToXimD0_D0ToKPiOrKPiPiPi_XimToLambdaDDPi",
    "OmbmToXimD0_D0ToKPiOrKPiPiPi_XimToLambdaLLPi",
    "OmbmToXimD0_D0ToKPiOrKPiPiPi_XimToLambdaDDPi",
    "OmbmToOmmD0_D0ToKPiOrKPiPiPi_OmmToLambdaLLK",
    "OmbmToOmmD0_D0ToKPiOrKPiPiPi_OmmToLambdaDDK",
    "XibcpToPD0_D0ToKPiOrKPiPiPi",
    "Xibc0ToD0LambdaLL_D0ToKPiOrKPiPiPi",
    "Xibc0ToD0LambdaDD_D0ToKPiOrKPiPiPi",
    "XibcpToDpLambdaLL_DpToKmPipPip",
    "XibcpToDpLambdaDD_DpToKmPipPip",
    # lines from bbaryon_to_lightbaryon_dh
    "LbToD0PPi_D0ToKsLLHH",
    "LbToD0PPi_D0ToKsTTHH",
    "LbToD0PPi_D0ToKsDDHH",
    "LbToD0PK_D0ToKsLLHH",
    "LbToD0PK_D0ToKsTTHH",
    "LbToD0PK_D0ToKsDDHH",
    "LbToD0PPi_D0ToHHHH",
    "LbToD0PK_D0ToHHHH",
    "LbToD0PPi_D0ToKsLLHHWS",
    "LbToD0PPi_D0ToKsDDHHWS",
    "LbToD0PK_D0ToKsLLHHWS",
    "LbToD0PK_D0ToKsDDHHWS",
    "LbToD0PPiWS_D0ToKsLLHH",
    "LbToD0PPiWS_D0ToKsDDHH",
    "LbToD0PKWS_D0ToKsLLHH",
    "LbToD0PKWS_D0ToKsDDHH",
    "LbToD0PPi_D0ToHHHHWS",
    "LbToD0PK_D0ToHHHHWS",
    "LbToD0PPiWS_D0ToHHHH",
    "LbToD0PKWS_D0ToHHHH",
    "XibmToDstmPPi_DstmToD0Pi_D0ToHH",
    "XibmToDstmPK_DstmToD0Pi_D0ToHH",
    "LbToXimD0Kp_D0ToKPiOrKPiPiPi_XimToLambdaLLPi",
    "LbToXimD0Kp_D0ToKPiOrKPiPiPi_XimToLambdaDDPi",
    "Xib0ToXimD0Pip_D0ToKPiOrKPiPiPi_XimToLambdaLLPi",
    "Xib0ToXimD0Pip_D0ToKPiOrKPiPiPi_XimToLambdaDDPi",
    "Xibc0ToPD0K_D0ToKPiOrKPiPiPi",
    "XibcpToPDpK_DpToKmPipPip",
    "XibcpToD0LambdaLLPip_D0ToKPiOrKPiPiPi",
    "XibcpToD0LambdaDDPip_D0ToKPiOrKPiPiPi",
    # lines from bbaryon_to_lightbaryon_dd
    "XibcpToPD0D0_D0ToKPiOrKPiPiPi",
    "LbToD0DsmP_D0ToKPiOrKPiPiPi_DsmToHHH",
    # lines from bbaryon_to_lightbaryon_ddh
    "LbToDpDmPK_DpToHHH",
    "Xibc0ToD0D0PPim_D0ToKPiOrKPiPiPi",
    "XibcpToD0DpPPim_D0ToKPiOrKPiPiPi_DpToKmPipPip",
    # lines for SigmaNet background
    "BdToDmPiSB_DmToPimPimKp",
    "LbToLcpPiSB_LcpToPKPi",
    "BuToD0PiSB_D0ToHH",
    "BdToD0PiPiSB_D0ToHH",
]

# default lines will be booked with:
#     prescale = 1,
#     b_sigmanet_filter, MVACut > default_MVACut,
#     no flavour tagging
# add extra configurations in the following dictionary
extra_config = {
    "prescale": {
        # lines from b_to_dh
        "BuToD0Pi_D0ToHHWS": 0.1,
        "BuToD0K_D0ToHHWS": 0.1,
        "BuToD0Pi_D0ToKsLLHHWS": 0.1,
        "BuToD0Pi_D0ToKsDDHHWS": 0.1,
        "BuToD0K_D0ToKsLLHHWS": 0.1,
        "BuToD0K_D0ToKsDDHHWS": 0.1,
        #'BuToD0Pi_D0ToHHPi0ResolvedWS': 0.1,
        #'BuToD0Pi_D0ToHHPi0MergedWS': 0.1,
        #'BuToD0K_D0ToHHPi0ResolvedWS': 0.1,
        #'BuToD0K_D0ToHHPi0MergedWS': 0.1,
        "BuToD0Pi_D0ToHHHHWS": 0.1,
        "BuToD0K_D0ToHHHHWS": 0.1,
        #'BuToD0Pi_D0ToKsLLHHWSPi0Resolved': 0.1,
        #'BuToD0Pi_D0ToKsDDHHWSPi0Resolved': 0.1,
        #'BuToD0Pi_D0ToKsLLHHWSPi0Merged': 0.1,
        #'BuToD0Pi_D0ToKsDDHHWSPi0Merged': 0.1,
        #'BuToD0K_D0ToKsLLHHWSPi0Resolved': 0.1,
        #'BuToD0K_D0ToKsDDHHWSPi0Resolved': 0.1,
        #'BuToD0K_D0ToKsLLHHWSPi0Merged': 0.1,
        #'BuToD0K_D0ToKsDDHHWSPi0Merged': 0.1,
        # lines from b_to_dx_ltu
        "BdToDmPi_DmToPimPimKp_LTU": 0.01,
        "BdToDsmK_DsmToKpKmPim_LTU": 0.01,
        "BdToDsmKPiPi_DsmToKmKpPim_LTU": 0.001,
        # lines from b_to_dhh
        "BdToD0PiPiWS_D0ToHH": 0.1,
        "BdToD0KPiWS_D0ToHH": 0.1,
        "BdToD0KKWS_D0ToHH": 0.1,
        "BdToD0PiPiWS_D0ToKsLLHH": 0.1,
        "BdToD0PiPiWS_D0ToKsDDHH": 0.1,
        "BdToD0KPiWS_D0ToKsLLHH": 0.1,
        "BdToD0KPiWS_D0ToKsDDHH": 0.1,
        "BdToD0KKWS_D0ToKsLLHH": 0.1,
        "BdToD0KKWS_D0ToKsDDHH": 0.1,
        "BdToD0PbarPWS_D0ToHHHH": 0.1,
        #'BdToD0PiPiWS_D0ToHHPi0Resolved': 0.1,
        #'BdToD0PiPiWS_D0ToHHPi0Merged': 0.1,
        #'BdToD0KPiWS_D0ToHHPi0Resolved': 0.1,
        #'BdToD0KPiWS_D0ToHHPi0Merged': 0.1,
        #'BdToD0KKWS_D0ToHHPi0Resolved': 0.1,
        #'BdToD0KKWS_D0ToHHPi0Merged': 0.1,
        #'BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToHH': 0.1,
        #'BdToDst0KPiWS_Dst0ToD0Gamma_D0ToHH': 0.1,
        #'BdToDst0KKWS_Dst0ToD0Gamma_D0ToHH': 0.1,
        #'BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToHH': 0.1,
        #'BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToHH': 0.1,
        #'BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToHH': 0.1,
        #'BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToKsLLHH': 0.1,
        #'BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToKsDDHH': 0.1,
        #'BdToDst0KPiWS_Dst0ToD0Gamma_D0ToKsLLHH': 0.1,
        #'BdToDst0KPiWS_Dst0ToD0Gamma_D0ToKsDDHH': 0.1,
        #'BdToDst0KKWS_Dst0ToD0Gamma_D0ToKsLLHH': 0.1,
        #'BdToDst0KKWS_Dst0ToD0Gamma_D0ToKsDDHH': 0.1,
        #'BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToKsLLHH': 0.1,
        #'BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToKsDDHH': 0.1,
        #'BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToKsLLHH': 0.1,
        #'BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToKsDDHH': 0.1,
        #'BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToKsLLHH': 0.1,
        #'BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToKsDDHH': 0.1,
        #'BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToHHPi0Resolved': 0.1,
        #'BdToDst0KPiWS_Dst0ToD0Gamma_D0ToHHPi0Resolved': 0.1,
        #'BdToDst0KKWS_Dst0ToD0Gamma_D0ToHHPi0Resolved': 0.1,
        #'BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved': 0.1,
        #'BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved': 0.1,
        #'BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved': 0.1,
        #'BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToHHPi0Merged': 0.1,
        #'BdToDst0KPiWS_Dst0ToD0Gamma_D0ToHHPi0Merged': 0.1,
        #'BdToDst0KKWS_Dst0ToD0Gamma_D0ToHHPi0Merged': 0.1,
        #'BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged': 0.1,
        #'BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged': 0.1,
        #'BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged': 0.1,
        #'BdToDst0PiPiWS_Dst0ToD0Gamma_D0ToHHHH': 0.1,
        #'BdToDst0KPiWS_Dst0ToD0Gamma_D0ToHHHH': 0.1,
        #'BdToDst0KKWS_Dst0ToD0Gamma_D0ToHHHH': 0.1,
        #'BdToDst0PiPiWS_Dst0ToD0Pi0Resolved_D0ToHHHH': 0.1,
        #'BdToDst0KPiWS_Dst0ToD0Pi0Resolved_D0ToHHHH': 0.1,
        #'BdToDst0KKWS_Dst0ToD0Pi0Resolved_D0ToHHHH': 0.1,
        # lines from b_to_dhhh
        # lines from bbaryon_to_cbaryon_h
        "LbToLcpPiWS_LcpToPPiPi": 0.1,
        "LbToLcpPiWS_LcpToPKK": 0.1,
        "OmbmToOmc0PiWS_Omc0ToPKKPi": 0.1,
        # lines from bbaryon_to_cbaryon_hh
        "XibmToLcpKPiWS_LcpToPKPi": 0.1,
        "OmbmToXicpKPiWS_XicpToPKPi": 0.1,
        # lines from bbaryon_to_cbaryon_hhh
        "LbToLcpPiPiPiWS_LcpToPPiPi": 0.1,
        "LbToLcpPiPiPiWS_LcpToPKK": 0.1,
        "OmbmToOmc0PiPiPiWS_Omc0ToPKKPi": 0.1,
        # lines from bbaryon_to_lightbaryon_d
        "LbToLambdaLLD0_D0ToHHHHWS": 0.1,
        "LbToLambdaDDD0_D0ToHHHHWS": 0.1,
        "LbToLambdaLLD0_D0ToKsLLHHWS": 0.1,
        "LbToLambdaLLD0_D0ToKsDDHHWS": 0.1,
        "LbToLambdaDDD0_D0ToKsLLHHWS": 0.1,
        "LbToLambdaDDD0_D0ToKsDDHHWS": 0.1,
        # lines from bbaryon_to_lightbaryon_dh
        "LbToD0PPi_D0ToKsLLHHWS": 0.1,
        "LbToD0PPi_D0ToKsDDHHWS": 0.1,
        "LbToD0PK_D0ToKsLLHHWS": 0.1,
        "LbToD0PK_D0ToKsDDHHWS": 0.1,
        "LbToD0PPiWS_D0ToKsLLHH": 0.1,
        "LbToD0PPiWS_D0ToKsDDHH": 0.1,
        "LbToD0PKWS_D0ToKsLLHH": 0.1,
        "LbToD0PKWS_D0ToKsDDHH": 0.1,
        "LbToD0PPi_D0ToHHHHWS": 0.1,
        "LbToD0PK_D0ToHHHHWS": 0.1,
        "LbToD0PPiWS_D0ToHHHH": 0.1,
        "LbToD0PKWS_D0ToHHHH": 0.1,
        # lines for SigmaNet background
        "BdToDmPiSB_DmToPimPimKp": 0.25,
        "LbToLcpPiSB_LcpToPKPi": 1.0,
        "BuToD0PiSB_D0ToHH": 0.125,
        "BdToD0PiPiSB_D0ToHH": 0.25,
    },
    # custom MVA cut
    # `None` means MVA-unfiltered
    # (no default b_sigmanet_filter > default_MVACut)
    "mva": {
        # lines from b_to_dx_ltu
        "BdToDmPi_DmToPimPimKp_LTU": None,
        "BdToDsmK_DsmToKpKmPim_LTU": None,
        "BdToDsmKPiPi_DsmToKmKpPim_LTU": None,
        # lines for SigmaNet background
        "BdToDmPiSB_DmToPimPimKp": None,
        "LbToLcpPiSB_LcpToPKPi": None,
        "BuToD0PiSB_D0ToHH": None,
        "BdToD0PiPiSB_D0ToHH": None,
        # lines from bbaryon_to_lightbaryon_ddh
        "LbToDpDmPK_DpToHHH": 0.5,
    },
    "flavour_tagging": [
        # lines from b_to_dh
        "BdToDmPi_DmToPimPimKp",
        "BdToDsmPi_DsmToKpKmPim",
        "BdToDsmK_DsmToKpKmPim",
        # lines from b_to_dhh
        "BdToD0KK_D0ToHH",
        #'BdToD0KK_D0ToHHPi0Resolved',
        #'BdToD0KK_D0ToHHPi0Merged',
        "BdToD0KK_D0ToKsLLHH",
        "BdToD0KK_D0ToKsDDHH",
        "BdToD0PiPi_D0ToHH",
        #'BdToD0PiPi_D0ToHHPi0Resolved',
        #'BdToD0PiPi_D0ToHHPi0Merged',
        "BdToD0PiPi_D0ToKsLLHH",
        "BdToD0PiPi_D0ToKsDDHH",
        #'BdToDst0KK_Dst0ToD0Gamma_D0ToHH',
        #'BdToDst0KK_Dst0ToD0Gamma_D0ToHHHH',
        #'BdToDst0KK_Dst0ToD0Gamma_D0ToHHPi0Merged',
        #'BdToDst0KK_Dst0ToD0Gamma_D0ToHHPi0Resolved',
        #'BdToDst0KK_Dst0ToD0Gamma_D0ToKsDDHH',
        #'BdToDst0KK_Dst0ToD0Gamma_D0ToKsLLHH',
        #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToHH',
        #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToHHHH',
        #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged',
        #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved',
        #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToKsDDHH',
        #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToKsLLHH',
        #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToHH',
        #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToHHHH',
        #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToHHPi0Merged',
        #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToHHPi0Resolved',
        #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToKsDDHH',
        #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToKsLLHH',
        #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToHH',
        #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToHHHH',
        #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToHHPi0Merged',
        #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToHHPi0Resolved',
        #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToKsDDHH',
        #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToKsLLHH',
        # lines from b_to_ddh_standalone
        "BdToD0D0KsLL_D0ToHH",
        "BdToD0D0KsLL_D0ToKsLLHH",
        "BdToD0D0KsLL_D0ToKsDDHH",
        "BdToD0D0KsLL_D0ToHHHH",
        "BdToD0D0KsLL_D0ToHH_D0ToKsLLHH",
        "BdToD0D0KsLL_D0ToHH_D0ToKsDDHH",
        "BdToD0D0KsLL_D0ToHH_D0ToHHHH",
        "BdToD0D0KsLL_D0ToKsLLHH_D0ToKsDDHH",
        "BdToD0D0KsLL_D0ToKsLLHH_D0ToHHHH",
        "BdToD0D0KsLL_D0ToKsDDHH_D0ToHHHH",
        "BdToD0D0KsDD_D0ToHH",
        "BdToD0D0KsDD_D0ToKsLLHH",
        "BdToD0D0KsDD_D0ToKsDDHH",
        "BdToD0D0KsDD_D0ToHHHH",
        "BdToD0D0KsDD_D0ToHH_D0ToKsLLHH",
        "BdToD0D0KsDD_D0ToHH_D0ToKsDDHH",
        "BdToD0D0KsDD_D0ToHH_D0ToHHHH",
        "BdToD0D0KsDD_D0ToKsLLHH_D0ToKsDDHH",
        "BdToD0D0KsDD_D0ToKsLLHH_D0ToHHHH",
        "BdToD0D0KsDD_D0ToKsDDHH_D0ToHHHH",
        "BdToD0D0Phi_D0ToHH",
        "BdToD0D0Phi_D0ToKsLLHH",
        "BdToD0D0Phi_D0ToKsDDHH",
        "BdToD0D0Phi_D0ToHHHH",
        "BdToD0D0Phi_D0ToHH_D0ToKsLLHH",
        "BdToD0D0Phi_D0ToHH_D0ToKsDDHH",
        "BdToD0D0Phi_D0ToHH_D0ToHHHH",
        "BdToD0D0Phi_D0ToKsLLHH_D0ToKsDDHH",
        "BdToD0D0Phi_D0ToKsLLHH_D0ToHHHH",
        "BdToD0D0Phi_D0ToKsDDHH_D0ToHHHH",
    ],
    "pv_unbiasing": [
        # PV-unbiasing is automatically added for all FT lines,
        # book line here if FT is not required but PV-unbiasing is required.
        "BdToDmPi_DmToPimPimKp_LTU",
        "BdToDsmK_DsmToKpKmPim_LTU",
        "BdToDsmKPiPi_DsmToKmKpPim_LTU",
        "BuToD0Pi_D0ToHH",
        "LbToLcpPi_LcpToPKPi",
        "OmbmToOmc0Pi_Omc0ToPKKPi",
        "Xib0ToXicpPi_XicpToPKPi",
        "XibmToXic0Pi_Xic0ToPKKPi",
        "XibmToLcpKK_LcpToPKPi",
        "XibmToLcpKPi_LcpToPKPi",
        "XibmToLcpPiPi_LcpToPKPi",
        "XibmToXicpKPi_XicpToPKPi",
        "OmbmToOmc0PiPiPi_Omc0ToPKKPi",
        "XibmToXic0PiPiPi_Xic0ToPKKPi",
    ],
    "isolation": [
        "BuToD0Pi_D0ToHH",
        "BuToD0K_D0ToHH",
        "BuToD0Pi_D0ToHHWS",
        "BuToD0K_D0ToHHWS",
        "BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHH",
        "BuToDst0Pi_Dst0ToD0Gamma_D0ToHH",
        "BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHH",
        "BuToDst0K_Dst0ToD0Gamma_D0ToHH",
        "BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHHWS",
        "BuToDst0Pi_Dst0ToD0Gamma_D0ToHHWS",
        "BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHHWS",
        "BuToDst0K_Dst0ToD0Gamma_D0ToHHWS",
        "BuToD0Pi_D0ToKsLLHH",
        "BuToD0Pi_D0ToKsDDHH",
        "BuToD0K_D0ToKsLLHH",
        "BuToD0K_D0ToKsDDHH",
        "BuToD0Pi_D0ToKsLLHHWS",
        "BuToD0Pi_D0ToKsDDHHWS",
        "BuToD0K_D0ToKsLLHHWS",
        "BuToD0K_D0ToKsDDHHWS",
        "BuToD0K_D0ToHHPi0Resolved",
        "BuToD0Pi_D0ToHHPi0Resolved",
        #'BuToD0K_D0ToHHPi0Merged',
        #'BuToD0Pi_D0ToHHPi0Merged',
        "BuToD0Pi_D0ToHHPi0ResolvedWS",
        #'BuToD0Pi_D0ToHHPi0MergedWS',
        "BuToD0K_D0ToHHPi0ResolvedWS",
        #'BuToD0K_D0ToHHPi0MergedWS',
        "BuToD0Pi_D0ToHHHH",
        "BuToD0K_D0ToHHHH",
        "BuToD0Pi_D0ToHHHHWS",
        "BuToD0K_D0ToHHHHWS",
        "BuToD0Pi_D0ToKsLLHHPi0Resolved",
        "BuToD0Pi_D0ToKsDDHHPi0Resolved",  # rate checked to be fine, jessy.daniel@cern.ch
        #'BuToD0Pi_D0ToKsLLHHPi0Merged',
        #'BuToD0Pi_D0ToKsDDHHPi0Merged',
        "BuToD0K_D0ToKsLLHHPi0Resolved",
        "BuToD0K_D0ToKsDDHHPi0Resolved",  # rate checked to be fine, jessy.daniel@cern.ch
        #'BuToD0K_D0ToKsLLHHPi0Merged',
        #'BuToD0K_D0ToKsDDHHPi0Merged',
        "BuToD0Pi_D0ToKsLLHHWSPi0Resolved",
        "BuToD0Pi_D0ToKsDDHHWSPi0Resolved",
        #'BuToD0Pi_D0ToKsLLHHWSPi0Merged',
        #'BuToD0Pi_D0ToKsDDHHWSPi0Merged',
        "BuToD0K_D0ToKsLLHHWSPi0Resolved",
        "BuToD0K_D0ToKsDDHHWSPi0Resolved",
        #'BuToD0K_D0ToKsLLHHWSPi0Merged',
        #'BuToD0K_D0ToKsDDHHWSPi0Merged',
        "BuToD0K_D0ToKsDDPi0Resolved",
        "BuToD0K_D0ToKsLLPi0Resolved",
        "BuToD0Pi_D0ToKsDDPi0Resolved",
        "BuToD0Pi_D0ToKsLLPi0Resolved",
        "BdToDstmMuNu_DstmToD0Pi_D0ToHHHH",
        "BdToDstmMuNu_DstmToD0Pi_D0ToKsDDHH",
        "BdToDstmMuNu_DstmToD0Pi_D0ToKsLLHH",
    ],
}

################################
# Register lines in hlt2_lines #
# with generic functions       #
################################

validate_config(all_lines, extra_config)

make_hlt2_lines(
    line_dict=hlt2_lines,
    line_makers=line_makers,
    all_lines=all_lines,
    extra_config=extra_config,
)

##################################
# Register lines in hlt2_lines   #
# with unique functions          #
# for lines have special purpose #
##################################


@register_line_builder(hlt2_calib_lines)
def BdToDsmPi_DsmToKpKmPim_Calib(name="Hlt2Calib_BdToDsmPi_DsmToKpKmPim", prescale=1):
    line_alg = b_to_dh.make_BdToDsmPi_DsmToKpKmPim_Calib(process=PROCESS, MVACut=0.5)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=True,
        raw_banks=DETECTORS,
        algs=prefilters.b2oc_prefilters() + [line_alg],
    )


@register_line_builder(hlt2_calib_lines)
def BdToDmPi_DmToPimPimKp_Calib(name="Hlt2Calib_BdToDmPi_DmToPimPimKp", prescale=1):
    line_alg = b_to_dh.make_BdToDmPi_DmToPimPimKp_Calib(process=PROCESS, MVACut=0.5)
    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=True,
        raw_banks=DETECTORS,
        algs=prefilters.b2oc_prefilters() + [line_alg],
    )
