###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of B2OC basic objects: pions, kaons, ..."""

import Functors as F
from Functors import require_all
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from GaudiKernel.SystemOfUnits import micrometer as um
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.standard_particles import (
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
    make_has_rich_long_protons,
    make_ismuon_long_muon,
    make_KsDD,
    make_KsLL,
    make_KsTT,
    make_LambdaDD,
    make_LambdaLL,
    make_LambdaTT,
    make_merged_pi0s,
    make_photons,
    make_resolved_pi0s,
)

from Hlt2Conf.lines.semileptonic.builders.base_builder import make_inmuon_long_muon

####################################
# Track selections                 #
####################################


@configurable
def filter_particles(
    make_particles, trchi2todof_max=None, mipchi2_min=4, pt_min=250 * MeV, p_min=2 * GeV
):
    code = require_all(F.PT > pt_min, F.P > p_min, F.OWNPVIPCHI2 > mipchi2_min)
    if trchi2todof_max is not None:
        code &= F.CHI2DOF < trchi2todof_max
    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_pions(pi_pidk_max=5.0, p_min=2 * GeV, pt_min=250 * MeV, **decay_arguments):
    """Return pions filtered by thresholds common to B2OC decay product selections."""
    pions = filter_particles(
        make_particles=make_has_rich_long_pions,
        p_min=p_min,
        pt_min=pt_min,
        **decay_arguments,
    )
    if pi_pidk_max is not None:
        code = F.PID_K < pi_pidk_max
        pions = ParticleFilter(pions, F.FILTER(code))
    return pions


@configurable
def make_kaons(k_pidk_min=-5.0, p_min=2 * GeV, pt_min=250 * MeV, **decay_arguments):
    """Return kaons filtered by thresholds common to B2OC decay product selections."""
    kaons = filter_particles(
        make_particles=make_has_rich_long_kaons,
        p_min=p_min,
        pt_min=pt_min,
        **decay_arguments,
    )
    if k_pidk_min is not None:
        code = F.PID_K > k_pidk_min
        kaons = ParticleFilter(kaons, F.FILTER(code))
    return kaons


@configurable
def make_protons(
    p_pidp_min=-5, p_pidkmp_max=None, p_min=2 * GeV, pt_min=250 * MeV, **decay_arguments
):
    """Return protons filtered by thresholds common to B2OC decay product selections."""
    protons = filter_particles(
        make_particles=make_has_rich_long_protons,
        p_min=p_min,
        pt_min=pt_min,
        **decay_arguments,
    )
    if p_pidp_min is not None:
        code = F.PID_P > p_pidp_min
        if p_pidkmp_max is not None:
            code &= (F.PID_K - F.PID_P) < p_pidkmp_max
        protons = ParticleFilter(protons, F.FILTER(code))
    return protons


@configurable
def make_tight_pions(
    pi_pidk_max=20.0, p_min=5 * GeV, pt_min=500 * MeV, **decay_arguments
):
    """Return accompanying pions filtered by thresholds common to B2OC selections."""
    pions = filter_particles(
        make_particles=make_has_rich_long_pions, p_min=p_min, pt_min=pt_min
    )
    if pi_pidk_max is not None:
        code = F.PID_K < pi_pidk_max
        pions = ParticleFilter(pions, F.FILTER(code))
    return pions


@configurable
def make_tightpid_tight_pions(pi_pidk_max=5, **decay_arguments):
    """Return accompanying pions filtered by thresholds with tightened PID cut."""
    return make_tight_pions(pi_pidk_max=pi_pidk_max, **decay_arguments)


@configurable
def make_soft_pions(pi_pidk_max=20, p_min=2 * GeV, pt_min=100 * MeV):
    """Return accompanying pions filtered by thresholds common to B2OC very soft selections."""
    pions = filter_particles(
        make_particles=make_has_rich_long_pions, p_min=p_min, pt_min=pt_min
    )
    if pi_pidk_max is not None:
        code = F.PID_K < pi_pidk_max
        pions = ParticleFilter(pions, F.FILTER(code))
    return pions


@configurable
def make_tightpid_soft_pions(pi_pidk_max=5):
    """Return soft accompanying pions filtered by thresholds with tightened PID cut."""
    return make_soft_pions(pi_pidk_max=pi_pidk_max)


@configurable
def make_tight_kaons(
    k_pidk_min=-10.0, p_min=5 * GeV, pt_min=500 * MeV, **decay_arguments
):
    """Return accompanying kaons filtered by thresholds common to B2OC selections."""
    kaons = filter_particles(
        make_particles=make_has_rich_long_kaons,
        p_min=p_min,
        pt_min=pt_min,
        **decay_arguments,
    )
    if k_pidk_min is not None:
        code = F.PID_K > k_pidk_min
        kaons = ParticleFilter(kaons, F.FILTER(code))
    return kaons


@configurable
def make_tightpid_tight_kaons(k_pidk_min=-5):
    """Return accompanying kaons filtered by thresholds with tightened PID cut."""
    return make_tight_kaons(k_pidk_min=k_pidk_min)


@configurable
def make_soft_kaons(k_pidk_min=-10, p_min=2 * GeV, pt_min=100 * MeV):
    """Return accompanying kaons filtered by thresholds common to B2OC very soft selections."""
    kaons = filter_particles(
        make_particles=make_has_rich_long_kaons, p_min=p_min, pt_min=pt_min
    )
    if k_pidk_min is not None:
        code = F.PID_K > k_pidk_min
        kaons = ParticleFilter(kaons, F.FILTER(code))
    return kaons


@configurable
def make_tightpid_soft_kaons(k_pidk_min=-5):
    """Return soft accompanying kaons filtered by thresholds with tightened PID cut."""
    return make_soft_kaons(k_pidk_min=k_pidk_min)


@configurable
def make_tight_protons(
    p_pidp_min=-10,
    p_pidkmp_max=None,
    p_min=8 * GeV,
    pt_min=500 * MeV,
    **decay_arguments,
):
    """Return accompanying protons filtered by thresholds common to B2OC selections."""
    protons = filter_particles(
        make_particles=make_has_rich_long_protons,
        p_min=p_min,
        pt_min=pt_min,
        **decay_arguments,
    )
    if p_pidp_min is not None:
        code = F.PID_P > p_pidp_min
        if p_pidkmp_max is not None:
            code &= (F.PID_K - F.PID_P) < p_pidkmp_max
        protons = ParticleFilter(protons, F.FILTER(code))
    return protons


@configurable
def make_tightpid_tight_protons(p_pidp_min=-5):
    """Return accompanying protons filtered by thresholds with tightened PID cut."""
    return make_tight_protons(p_pidp_min=p_pidp_min)


@configurable
def make_soft_protons(p_pidp_min=-10, p_min=2 * GeV, pt_min=100 * MeV):
    """Return accompanyingprotons filtered by thresholds common to B2OC very soft selections."""
    protons = filter_particles(
        make_particles=make_has_rich_long_protons, p_min=p_min, pt_min=pt_min
    )
    if p_pidp_min is not None:
        code = F.PID_P > p_pidp_min
        protons = ParticleFilter(protons, F.FILTER(code))
    return protons


@configurable
def make_tightpid_soft_protons(p_pidp_min=-5):
    """Return soft accompanying protons filtered by thresholds with tightened PID cut."""
    return make_soft_protons(p_pidp_min=p_pidp_min)


@configurable
def make_muons(
    require_ismuon=True, p_min=5.0 * GeV, pt_min=500.0 * MeV, mu_pidmu_min=-10
):
    """Return muons from b hadron"""
    if require_ismuon:
        make_particles = make_ismuon_long_muon
    else:
        make_particles = make_inmuon_long_muon
    muons = filter_particles(make_particles=make_particles, p_min=p_min, pt_min=pt_min)
    if mu_pidmu_min is not None:
        code = F.PID_MU > mu_pidmu_min
        muons = ParticleFilter(muons, F.FILTER(code))
    return muons


####################################
# Neutral objects selections       #
####################################


@configurable
def make_photons(make_particles=make_photons, CL_min=0.25, et_min=150 * MeV):
    code = require_all(F.IS_NOT_H > CL_min, F.PT > et_min)
    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_resolved_pi0s(
    make_particles=make_resolved_pi0s, pt_min=0.5 * GeV, p_min=None, CL_min=None
):
    code = F.PT > pt_min
    if p_min is not None:
        code &= F.P > p_min
    if CL_min is not None:
        code &= require_all(
            F.CHILD(1, F.IS_NOT_H) > CL_min, F.CHILD(2, F.IS_NOT_H) > CL_min
        )
    return ParticleFilter(make_particles(), F.FILTER(code))


@configurable
def make_merged_pi0s(
    make_particles=make_merged_pi0s, pt_min=2.0 * GeV, p_min=None, CL_min=None
):
    code = F.PT > pt_min
    if p_min is not None:
        code &= F.P > p_min
    if CL_min is not None:
        code &= F.IS_PHOTON < 1.0 - CL_min
    return ParticleFilter(make_particles(), F.FILTER(code))


######################################################
# Generic twobody/threebody/fourbody decay builders, #
# defines default combination cuts                   #
######################################################


@configurable
def make_twobody(
    particles,
    descriptor,
    am_min,
    am_max,
    combiner="ParticleVertexFitter",
    name="B2OCTwoBodyCombiner_{hash}",
    asumpt_min=1800 * MeV,
    adoca12_max=0.2 * mm,
    vchi2pdof_max=10,
    bpvvdchi2_min=36,
    bpvipchi2_min=0,
    bpvdira_min=0,
    vdrho_min=None,
):
    """
    A generic 2body decay maker.

    Parameters
    ----------
    particles
        Maker algorithm instances for input particles.
    descriptor : string
        Decay descriptor to be reconstructed.
    Remaining parameters define thresholds for the selection.
    """
    combination_code = require_all(
        in_range(am_min, F.MASS, am_max), F.SUM(F.PT) > asumpt_min
    )
    if adoca12_max is not None:
        combination_code &= F.SDOCA(1, 2) < adoca12_max

    vertex_code = require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVFDCHI2 > bpvvdchi2_min,
        F.OWNPVIPCHI2 > bpvipchi2_min,
        F.OWNPVDIRA > bpvdira_min,
        in_range(am_min, F.MASS, am_max),
    )

    if vdrho_min is not None:
        vertex_code &= F.OWNPVVDRHO > vdrho_min

    return ParticleCombiner(
        particles,
        name=name,
        ParticleCombiner=combiner,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def _make_threebody(
    particles,
    descriptor,
    am_min,
    am_max,
    name="B2OCThreeBodyCombiner_{hash}",
    asumpt_min=1800 * MeV,
    adoca12_max=0.2 * mm,
    adoca13_max=0.2 * mm,
    adoca23_max=0.2 * mm,
    vchi2pdof_max=10,
    bpvvdchi2_min=36,
    bpvipchi2_min=0,
    bpvdira_min=0,
):
    """
    A generic 3body decay maker.
    To be more efficient the invariant mass of particles 1 and 2 of the
    decay descriptor is required to be < am_max, a DOCAcut on particles
    1 and 2 is also available on demand.

    Parameters
    ----------
    particles
        Maker algorithm instances for input particles.
    descriptor : string
        Decay descriptor to be reconstructed.
    Remaining parameters define thresholds for the selection.
    """
    combination12_code = F.MASS < am_max
    if adoca12_max is not None:
        combination12_code &= F.SDOCA(1, 2) < adoca12_max

    combination_code = require_all(
        in_range(am_min, F.MASS, am_max), F.SUM(F.PT) > asumpt_min
    )
    for i in range(1, 4):
        for j in range(i + 1, 4):
            combination_code &= (1.0 - F.ALV(i, j)) > 1.4e-7
    if adoca13_max is not None:
        combination_code &= F.SDOCA(1, 3) < adoca13_max
    if adoca23_max is not None:
        combination_code &= F.SDOCA(2, 3) < adoca23_max
    vertex_code = require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVFDCHI2 > bpvvdchi2_min,
        F.OWNPVIPCHI2 > bpvipchi2_min,
        F.OWNPVDIRA > bpvdira_min,
        in_range(am_min, F.MASS, am_max),
    )
    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_threebody(
    particles,
    descriptor,
    am_min,
    am_max,
    name="B2OCThreeBodyFilter",
    asumpt_min=1800 * MeV,
    adoca12_max=0.2 * mm,
    adoca13_max=0.2 * mm,
    adoca23_max=0.2 * mm,
    vchi2pdof_max=10,
    bpvvdchi2_min=36,
    bpvipchi2_min=0,
    bpvdira_min=0,
    vdrho_min=None,
):
    preselected = _make_threebody(
        particles,
        descriptor,
        am_min,
        am_max,
        name=name,
        asumpt_min=asumpt_min,
        adoca12_max=adoca12_max,
        adoca13_max=adoca13_max,
        adoca23_max=adoca23_max,
        vchi2pdof_max=min(10, vchi2pdof_max),
        bpvvdchi2_min=min(36, bpvvdchi2_min),
        bpvipchi2_min=min(0, bpvipchi2_min),
        bpvdira_min=min(0, bpvdira_min),
    )

    code = F.ALL
    if vchi2pdof_max < 10:
        code &= F.CHI2DOF < vchi2pdof_max
    if bpvvdchi2_min > 36:
        code &= F.OWNPVFDCHI2 > bpvvdchi2_min
    if bpvipchi2_min > 0:
        code &= F.OWNPVIPCHI2 > bpvipchi2_min
    if bpvdira_min > 0:
        code &= F.OWNPVDIRA > bpvdira_min
    if vdrho_min is not None:
        code &= F.OWNPVVDRHO > vdrho_min

    return ParticleFilter(preselected, F.FILTER(code))


def _make_fourbody(
    particles,
    descriptor,
    am_min,
    am_max,
    name="B2OCFourBodyCombiner_{hash}",
    asumpt_min=1800 * MeV,
    adoca12_max=0.25 * mm,
    adoca13_max=0.25 * mm,
    adoca14_max=0.25 * mm,
    adoca23_max=0.25 * mm,
    adoca24_max=0.25 * mm,
    adoca34_max=0.25 * mm,
    vchi2pdof_max=10,
    bpvvdchi2_min=36,
    bpvipchi2_min=0,
    bpvdira_min=0,
):
    """
    A generic 4body decay maker.
    To be more efficient, the invariant mass of particles 1 2 and 1 2 3 of the
    decay descriptor is required to be < am_max, similarly a DOCAcut is applied
    to the combinations 1 2, 1 3 and 2 3 i.e. on the first 3 particles of the
    on the decay descriptor.
    Reject any candidates where any pair of same charge tracks have an opening
    angle less than 0.03 degrees, 1.4 - cos(theta) < 1.4e-7

    Parameters
    ----------
    particles
        Maker algorithm instances for input particles.
    descriptor : string
        Decay descriptor to be reconstructed.
    Remaining parameters define thresholds for the selection.
    """
    combination12_code = require_all(F.MASS < am_max)
    combination123_code = require_all(F.MASS < am_max)
    combination_code = require_all(
        in_range(am_min, F.MASS, am_max), F.SUM(F.PT) > asumpt_min
    )
    for i in range(1, 5):
        for j in range(i + 1, 5):
            combination_code &= (1.0 - F.ALV(i, j)) > 1.4e-7
    if adoca12_max is not None:
        combination_code &= F.SDOCA(1, 2) < adoca12_max
    if adoca13_max is not None:
        combination_code &= F.SDOCA(1, 3) < adoca13_max
    if adoca14_max is not None:
        combination_code &= F.SDOCA(1, 4) < adoca14_max
    if adoca23_max is not None:
        combination_code &= F.SDOCA(2, 3) < adoca23_max
    if adoca24_max is not None:
        combination_code &= F.SDOCA(2, 4) < adoca24_max
    if adoca34_max is not None:
        combination_code &= F.SDOCA(3, 4) < adoca34_max
    M23 = F.SUBCOMB(Functor=F.MASS, Indices=[2, 3])
    combination23_code = M23 < am_max
    combination_code &= combination23_code
    vertex_code = require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVFDCHI2 > bpvvdchi2_min,
        F.OWNPVIPCHI2 > bpvipchi2_min,
        F.OWNPVDIRA > bpvdira_min,
        in_range(am_min, F.MASS, am_max),
    )
    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        Combination123Cut=combination123_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_fourbody(
    particles,
    descriptor,
    am_min,
    am_max,
    name="B2OCFourBodyFilter",
    asumpt_min=1800 * MeV,
    adoca12_max=0.25 * mm,
    adoca13_max=0.25 * mm,
    adoca14_max=0.25 * mm,
    adoca23_max=0.25 * mm,
    adoca24_max=0.25 * mm,
    adoca34_max=0.25 * mm,
    vchi2pdof_max=10,
    bpvvdchi2_min=36,
    bpvipchi2_min=0,
    bpvdira_min=0,
    vdrho_min=None,
):
    preselected = _make_fourbody(
        particles,
        descriptor,
        am_min,
        am_max,
        name=name,
        asumpt_min=asumpt_min,
        adoca12_max=adoca12_max,
        adoca13_max=adoca13_max,
        adoca14_max=adoca14_max,
        adoca23_max=adoca23_max,
        adoca24_max=adoca24_max,
        adoca34_max=adoca34_max,
        vchi2pdof_max=min(10, vchi2pdof_max),
        bpvvdchi2_min=min(36, bpvvdchi2_min),
        bpvipchi2_min=min(0, bpvipchi2_min),
        bpvdira_min=min(0, bpvdira_min),
    )

    code = F.ALL
    if vchi2pdof_max < 10:
        code &= F.CHI2DOF < vchi2pdof_max
    if bpvvdchi2_min > 36:
        code &= F.OWNPVFDCHI2 > bpvvdchi2_min
    if bpvipchi2_min > 0:
        code &= F.OWNPVIPCHI2 > bpvipchi2_min
    if bpvdira_min > 0:
        code &= F.OWNPVDIRA > bpvdira_min
    if vdrho_min is not None:
        code &= F.OWNPVVDRHO > vdrho_min

    return ParticleFilter(preselected, F.FILTER(code))


####################################
# ks, kstar0, ... 2-body decays    #
####################################


@configurable
def make_selected_ks(
    input_ks,
    name="B2OCKSFilter",
    adocachi2cut=30.0,
    pi_pmin=2 * GeV,
    pi_mipchi2pv=9.0,
    chi2vx=30.0,
    bpvvdchi2_min=None,
    am_min=None,
    am_max=None,
):
    """
    Filters Kshort candidates for B2OC. Default cuts correspond to VeryLooseKSLL from the
    Run2 CommonParticles
    """
    code = require_all(
        F.MAXSDOCACHI2CUT(adocachi2cut),
        F.CHI2DOF < chi2vx,
        F.MIN(F.P) > pi_pmin,
        F.MIN(F.OWNPVIPCHI2) > pi_mipchi2pv,
    )
    if bpvvdchi2_min is not None:
        code = require_all(code, F.OWNPVFDCHI2 > bpvvdchi2_min)
    if am_min is not None:
        code = require_all(code, F.MASS > am_min)
    if am_max is not None:
        code = require_all(code, F.MASS < am_max)
    return ParticleFilter(input_ks, F.FILTER(code), name=name)


@configurable
def make_ks_LL(
    make_ks=make_KsLL,
    adocachi2cut=30.0,
    pi_pmin=2 * GeV,
    pi_mipchi2pv=9.0,
    chi2vx=30.0,
    bpvvdchi2_min=4.0,
    **sel_arguments,
):
    """
    Builds LL Kshorts, currently corresponding to the Run2
    StdVeryLooseKSLL.
    """
    return make_selected_ks(
        input_ks=make_ks(),
        name="B2OCKsLLFilter_{hash}",
        adocachi2cut=adocachi2cut,
        pi_pmin=pi_pmin,
        pi_mipchi2pv=pi_mipchi2pv,
        chi2vx=chi2vx,
        bpvvdchi2_min=bpvvdchi2_min,
        **sel_arguments,
    )


@configurable
def make_ks_TT(
    make_ks=make_KsTT,
    adocachi2cut=30.0,
    pi_pmin=2 * GeV,
    pi_mipchi2pv=9.0,
    chi2vx=30.0,
    bpvvdchi2_min=4.0,
):
    """
    Builds TT Kshorts, currently corresponding to the Run2
    StdVeryLooseKSTT.
    """
    return make_selected_ks(
        input_ks=make_ks(),
        name="B2OCKsTTFilter",
        adocachi2cut=adocachi2cut,
        pi_pmin=pi_pmin,
        pi_mipchi2pv=pi_mipchi2pv,
        chi2vx=chi2vx,
        bpvvdchi2_min=bpvvdchi2_min,
    )


@configurable
def make_ks_DD(
    make_ks=make_KsDD,
    adocachi2cut=25.0,
    pi_pmin=2 * GeV,
    pi_mipchi2pv=4.0,
    chi2vx=25.0,
    **sel_arguments,
):
    """
    Builds DD Kshorts, currently corresponding to the Run2
    StdLooseKSDD.
    """
    return make_selected_ks(
        input_ks=make_ks(),
        name="B2OCKsDDFilter_{hash}",
        adocachi2cut=adocachi2cut,
        pi_pmin=pi_pmin,
        pi_mipchi2pv=pi_mipchi2pv,
        chi2vx=chi2vx,
        **sel_arguments,
    )


# TODO: LD K-shorts? These will need progress on issue #102


@configurable
def make_rho0(
    name="B2OCRho0Combiner_{hash}",
    make_pions=make_pions,
    am_min=250 * MeV,
    am_max=3000 * MeV,
    pi_p_min=2 * GeV,
    pi_pt_min=200 * MeV,
    pi_pidk_max=20,
    adoca12_max=0.5 * mm,
    asumpt_min=500 * MeV,
    bpvvdchi2_min=16,
    vchi2pdof_max=16,
):
    """
    Build Rho0 candidates. Currently corresponding to the Run2
    "HH" cuts.
    """

    pions = make_pions(pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    descriptor = "rho(770)0 -> pi+ pi-"
    combination_code = require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > asumpt_min,
        F.MAXSDOCACUT(adoca12_max),
    )
    vertex_code = require_all(F.CHI2DOF < vchi2pdof_max, F.OWNPVFDCHI2 > bpvvdchi2_min)
    return ParticleCombiner(
        [pions, pions],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_kstar0(
    name="B2OCKstarCombiner_{hash}",
    make_pions=make_pions,
    make_kaons=make_kaons,
    am_min=742 * MeV,
    am_max=1042 * MeV,
    pi_pidk_max=5,
    k_pidk_min=-5,
    pi_p_min=2 * GeV,
    pi_pt_min=200 * MeV,
    k_p_min=2 * GeV,
    k_pt_min=200 * MeV,
    adoca12_max=0.5 * mm,
    asumpt_min=1000 * MeV,
    bpvvdchi2_min=16,
    vchi2pdof_max=16,
):
    """
    Build Kstar0 candidates. Currently corresponding to the Run2
    "HH" cuts.
    """

    pions = make_pions(pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    kaons = make_kaons(k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min)
    descriptor = "[K*(892)0 -> pi- K+]cc"
    combination_code = require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > asumpt_min,
        F.MAXSDOCACUT(adoca12_max),
    )
    vertex_code = require_all(F.CHI2DOF < vchi2pdof_max, F.OWNPVFDCHI2 > bpvvdchi2_min)
    return ParticleCombiner(
        [pions, kaons],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_phi2kk(
    am_min=900.0 * MeV,
    am_max=1060.0 * MeV,
    asumpt_min=500 * MeV,
    kaon_p_min=2500 * MeV,
    kaon_pt_min=250 * MeV,
    kaon_pidk_min=-5,
    adoca12_max=0.3 * mm,
    vchi2=25.0,
    mipchi2_min=None,
    dau_mipchi2_min=4,
    name="B2OCPhiCombiner_{hash}",
):
    kaons = make_kaons(
        k_pidk_min=kaon_pidk_min,
        p_min=kaon_p_min,
        pt_min=kaon_pt_min,
        mipchi2_min=dau_mipchi2_min,
    )
    descriptors = "phi(1020) -> K+ K-"
    combination_code = F.require_all(
        F.MASS > am_min,
        F.MASS < 1.1 * am_max,
        F.SUM(F.PT) > asumpt_min,
        F.MAXSDOCACUT(adoca12_max),
    )
    combination_code &= (1.0 - F.ALV(1, 2)) > 1.4e-7
    vertex_code = F.require_all(F.MASS < am_max, F.CHI2 < vchi2)
    if mipchi2_min is not None:
        vertex_code &= F.OWNPVIPCHI2 > mipchi2_min
    return ParticleCombiner(
        Inputs=[kaons, kaons],
        name=name,
        DecayDescriptor=descriptors,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


##################
# f0980 builder ##
##################


@configurable
def make_f0980_to_pippim(
    am_min=970.0 * MeV,
    am_max=1100.0 * MeV,
    pi_pidk_max=5,
    pi_p_min=2 * GeV,
    pi_pt_min=250 * MeV,
    **decay_arguments,
):
    pions = make_pions(pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min)
    return make_twobody(
        particles=[pions, pions],
        descriptor="f_0(980) -> pi+ pi-",
        am_min=am_min,
        am_max=am_max,
        name="B2OCf09802PipPimCombiner_{hash}",
        **decay_arguments,
    )


######################
# select tracks for FT
######################


@configurable
def get_particles_WithPVs(make_particles=make_has_rich_long_pions, mipchi2_min=6):
    code = F.OWNPVIPCHI2 > mipchi2_min
    return ParticleFilter(make_particles(), F.FILTER(code))


###############################################################################
# Specific hyperon decay builders, overrides default cuts where needed        #
###############################################################################

####################################
# lambda0  2-body decays           #
####################################


@configurable
def make_lambda_LL(
    make_lambda=make_LambdaLL,
    m_max=1166 * MeV,
    m_min=1066 * MeV,
    pt_min=550 * MeV,
    p_min=9 * GeV,
    vchi2pdof_max=6.0,
    bpvvdz_min=15 * mm,
    bpvfdchi2_min=160.0,
):
    code = require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.P > p_min,
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVVDZ > bpvvdz_min,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
    )
    return ParticleFilter(make_lambda(), F.FILTER(code), name="LambdaLLCombiner_{hash}")


@configurable
def make_lambda_TT(
    make_lambda=make_LambdaTT,
    m_max=1166 * MeV,
    m_min=1066 * MeV,
    pt_min=550 * MeV,
    p_min=9 * GeV,
    vchi2pdof_max=6.0,
    bpvvdz_min=15 * mm,
    bpvfdchi2_min=160.0,
):
    code = require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.P > p_min,
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVVDZ > bpvvdz_min,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
    )
    return ParticleFilter(make_lambda(), F.FILTER(code), name="LambdaTTCombiner_{hash}")


@configurable
def make_lambda_DD(
    make_lambda=make_LambdaDD,
    m_max=1146 * MeV,
    m_min=1086 * MeV,
    pt_min=1 * GeV,
    p_min=14 * GeV,
    vchi2pdof_max=9.0,
):
    code = require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.P > p_min,
        F.CHI2DOF < vchi2pdof_max,
    )
    return ParticleFilter(make_lambda(), F.FILTER(code), name="LambdaDDCombiner_{hash}")


@configurable
def make_xim_to_lambda0pim(
    lambdas,
    pions,
    comb_doca_max=150 * um,
    comb_m_min=1260 * MeV,
    comb_m_max=1380 * MeV,
    comb_pt_min=500 * MeV,
    comb_p_min=9.5 * GeV,
    m_min=1280 * MeV,
    m_max=1360 * MeV,
    pt_min=600 * MeV,
    p_min=10 * GeV,
    vchi2pdof_max=8.0,
    bpvvdz_min=5 * mm,
    bpvfdchi2_min=25.0,
):
    """Make Xi- -> Lambda pi- using long pi-."""
    # Todo, tune cuts for Xi- from b, default is from c
    combination_code = require_all(
        F.MAXSDOCACUT(comb_doca_max),
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.PT > comb_pt_min,
        F.P > comb_p_min,
    )
    vertex_code = require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.P > p_min,
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVVDZ > bpvvdz_min,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
    )
    return ParticleCombiner(
        [lambdas, pions],
        name="XimToLambdaPiCombiner_{hash}",
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_omegam_to_lambda0km(
    lambdas,
    kaons,
    comb_doca_max=150 * um,
    comb_m_min=1612 * MeV,
    comb_m_max=1732 * MeV,
    comb_pt_min=550 * MeV,
    comb_p_min=9.5 * GeV,
    m_min=1632 * MeV,
    m_max=1712 * MeV,
    pt_min=650 * MeV,
    p_min=10 * GeV,
    vchi2pdof_max=8.0,
    bpvvdz_min=5 * mm,
    bpvfdchi2_min=16.0,
):
    """Make Omega- -> Lambda K- from long tracks."""
    # Todo, tune cuts for Omega- from b, default is from c
    combination_code = require_all(
        F.MAXSDOCACUT(comb_doca_max),
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.PT > comb_pt_min,
        F.P > comb_p_min,
    )
    vertex_code = require_all(
        in_range(m_min, F.MASS, m_max),
        F.PT > pt_min,
        F.P > p_min,
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVVDZ > bpvvdz_min,
        F.OWNPVFDCHI2 > bpvfdchi2_min,
    )
    return ParticleCombiner(
        [lambdas, kaons],
        name="OmmToLambdaKCombiner_{hash}",
        DecayDescriptor="[Omega- -> Lambda0 K-]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
