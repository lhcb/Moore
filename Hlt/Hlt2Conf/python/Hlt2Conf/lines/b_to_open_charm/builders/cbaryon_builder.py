###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of detached charm baryon decays that are shared between many
B2OC selections, and therefore are defined centrally.
"""

from GaudiKernel.SystemOfUnits import GeV, MeV
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleContainersMerger

from . import basic_builder, d_builder

###############################################################################
# Specific charm baryon decay builders, overrides default cuts where needed   #
###############################################################################


@configurable
def make_lc_to_pkpi(
    am_min=2260 * MeV,
    am_max=2320 * MeV,
    pi_pidk_max=5,
    k_pidk_min=-5,
    p_pidkmp_max=None,
    p_pidp_min=-5,
    pi_p_min=2 * GeV,
    pi_pt_min=250 * MeV,
    k_p_min=2 * GeV,
    k_pt_min=250 * MeV,
    p_p_min=2 * GeV,
    p_pt_min=250 * MeV,
    **decay_arguments,
):
    """
    Return a Lambda_c+ -> p K- pi+ decay maker.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->3body
                        maker
    """
    protons = basic_builder.make_protons(
        p_pidp_min=p_pidp_min, p_pidkmp_max=p_pidkmp_max, p_min=p_p_min, pt_min=p_pt_min
    )
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min
    )
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min
    )

    return basic_builder.make_threebody(
        particles=[protons, kaons, pions],
        descriptor="[Lambda_c+ -> p+ K- pi+]cc",
        am_min=am_min,
        am_max=am_max,
        name="B2OCLc2PKPiCombiner_{hash}",
        **decay_arguments,
    )


@configurable
def make_lc_to_pkk(
    am_min=2260 * MeV,
    am_max=2320 * MeV,
    k_pidk_min=-5,
    p_pidp_min=-5,
    k_p_min=2 * GeV,
    k_pt_min=250 * MeV,
    p_p_min=2 * GeV,
    p_pt_min=250 * MeV,
    **decay_arguments,
):
    """
    Return a Lambda_c+ -> p K- K+ decay maker.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->3body
                        maker
    """
    protons = basic_builder.make_protons(
        p_pidp_min=p_pidp_min, p_min=p_p_min, pt_min=p_pt_min
    )
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min
    )

    return basic_builder.make_threebody(
        particles=[protons, kaons, kaons],
        descriptor="[Lambda_c+ -> p+ K- K+]cc",
        am_min=am_min,
        am_max=am_max,
        name="B2OCLc2PKKCombiner_{hash}",
        **decay_arguments,
    )


@configurable
def make_lc_to_ppipi(
    am_min=2260 * MeV,
    am_max=2320 * MeV,
    pi_pidk_max=5,
    p_pidp_min=-5,
    pi_p_min=2 * GeV,
    pi_pt_min=250 * MeV,
    p_p_min=2 * GeV,
    p_pt_min=250 * MeV,
    **decay_arguments,
):
    """
    Return a Lambda_c+ -> p pi- pi+ decay maker.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->3body
                        maker
    """
    protons = basic_builder.make_protons(
        p_pidp_min=p_pidp_min, p_min=p_p_min, pt_min=p_pt_min
    )
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min
    )

    return basic_builder.make_threebody(
        particles=[protons, pions, pions],
        descriptor="[Lambda_c+ -> p+ pi- pi+]cc",
        am_min=am_min,
        am_max=am_max,
        name="B2OCLc2PPiPiCombiner_{hash}",
        **decay_arguments,
    )


@configurable
def make_lc_to_lambdapi(
    lm,
    am_min=2236 * MeV,
    am_max=2336 * MeV,
    pi_pidk_max=5,
    pi_p_min=2 * GeV,
    pi_pt_min=250 * MeV,
    **decay_arguments,
):
    """
    Return a Lambda_c+ -> Lambda0 pi+ decay maker.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->3body
                        maker
    """
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min
    )

    return basic_builder.make_twobody(
        particles=[lm, pions],
        descriptor="[Lambda_c+ -> Lambda0 pi+]cc",
        am_min=am_min,
        am_max=am_max,
        name="B2OCLc2LambdaPiCombiner_{hash}",
        **decay_arguments,
    )


@configurable
def make_lc_to_lambdak(
    lm,
    am_min=2236 * MeV,
    am_max=2336 * MeV,
    k_pidk_min=-5,
    k_p_min=2 * GeV,
    k_pt_min=250 * MeV,
    **decay_arguments,
):
    """
    Return a Lambda_c+ -> Lambda0 K+ decay maker.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->3body
                        maker
    """
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min
    )

    return basic_builder.make_twobody(
        particles=[lm, kaons],
        descriptor="[Lambda_c+ -> Lambda0 K+]cc",
        am_min=am_min,
        am_max=am_max,
        name="B2OCLc2LambdaKCombiner_{hash}",
        **decay_arguments,
    )


@configurable
def make_lc_to_pks(
    ks,
    am_min=2236 * MeV,
    am_max=2336 * MeV,
    p_pidp_min=-5,
    p_p_min=2 * GeV,
    p_pt_min=250 * MeV,
    **decay_arguments,
):
    """
    Return a Lambda_c+ -> p KS decay maker.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->3body
                        maker
    """
    protons = basic_builder.make_protons(
        p_pidp_min=p_pidp_min, p_min=p_p_min, pt_min=p_pt_min
    )

    return basic_builder.make_twobody(
        particles=[protons, ks],
        descriptor="[Lambda_c+ -> p+ KS0]cc",
        am_min=am_min,
        am_max=am_max,
        name="B2OCLc2PKSCombiner_{hash}",
        **decay_arguments,
    )


@configurable
def make_xicp_to_pkpi(
    am_min=2418 * MeV,
    am_max=2518 * MeV,
    pi_pidk_max=5,
    k_pidk_min=-5,
    p_pidp_min=-5,
    pi_p_min=2 * GeV,
    pi_pt_min=250 * MeV,
    k_p_min=2 * GeV,
    k_pt_min=250 * MeV,
    p_p_min=2 * GeV,
    p_pt_min=250 * MeV,
    **decay_arguments,
):
    """
    Return a Xi_c+ -> p K- pi+ decay maker.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->3body
                        maker
    """
    protons = basic_builder.make_protons(
        p_pidp_min=p_pidp_min, p_min=p_p_min, pt_min=p_pt_min
    )
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min
    )
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min
    )

    return basic_builder.make_threebody(
        particles=[protons, kaons, pions],
        descriptor="[Xi_c+ -> p+ K- pi+]cc",
        am_min=am_min,
        am_max=am_max,
        name="B2OCXicp2PKPiCombiner_{hash}",
        **decay_arguments,
    )


@configurable
def make_xic0_to_pkkpi(
    am_min=2421 * MeV,
    am_max=2521 * MeV,
    pi_pidk_max=5,
    k_pidk_min=-5,
    p_pidp_min=-5,
    pi_p_min=2 * GeV,
    pi_pt_min=250 * MeV,
    k_p_min=2 * GeV,
    k_pt_min=250 * MeV,
    p_p_min=2 * GeV,
    p_pt_min=250 * MeV,
    **decay_arguments,
):
    """
    Return a Xi_c0 -> p K- K- pi+ decay maker.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->4body
                        maker
    """
    protons = basic_builder.make_protons(
        p_pidp_min=p_pidp_min, p_min=p_p_min, pt_min=p_pt_min
    )
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min
    )
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min
    )

    return basic_builder.make_fourbody(
        particles=[protons, kaons, kaons, pions],
        descriptor="[Xi_c0 -> p+ K- K- pi+]cc",
        am_min=am_min,
        am_max=am_max,
        name="B2OCXic02PKKPiCombiner_{hash}",
        **decay_arguments,
    )


@configurable
def make_omegac0_to_pkkpi(
    am_min=2645 * MeV,
    am_max=2745 * MeV,
    pi_pidk_max=5,
    k_pidk_min=-5,
    p_pidp_min=-5,
    pi_p_min=2 * GeV,
    pi_pt_min=250 * MeV,
    k_p_min=2 * GeV,
    k_pt_min=250 * MeV,
    p_p_min=2 * GeV,
    p_pt_min=250 * MeV,
    **decay_arguments,
):
    """
    Return a Omega_c0 -> p K- K- pi+ decay maker.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->4body
                        maker
    """
    protons = basic_builder.make_protons(
        p_pidp_min=p_pidp_min, p_min=p_p_min, pt_min=p_pt_min
    )
    kaons = basic_builder.make_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min
    )
    pions = basic_builder.make_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min
    )

    return basic_builder.make_fourbody(
        particles=[protons, kaons, kaons, pions],
        descriptor="[Omega_c0 -> p+ K- K- pi+]cc",
        am_min=am_min,
        am_max=am_max,
        name="B2OCOmegac02PKKPiCombiner_{hash}",
        **decay_arguments,
    )


@configurable
def make_xiccpp_to_xicppi(
    am_min=3571 * MeV,
    am_max=3671 * MeV,
    xic_pi_pidk_max=5,
    xic_k_pidk_min=-5,
    xic_p_pidp_min=-5,
    xic_pi_p_min=2 * GeV,
    xic_pi_pt_min=250 * MeV,
    xic_k_p_min=2 * GeV,
    xic_k_pt_min=250 * MeV,
    xic_p_p_min=2 * GeV,
    xic_p_pt_min=250 * MeV,
    pi_pidk_max=20,
    pi_p_min=2 * GeV,
    pi_pt_min=100 * MeV,
    **decay_arguments,
):
    """
    Return a Xi_cc++ -> Xi_c+ pi+ decay.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->2body
                        maker
    """
    xicp_to_pkpi = make_xicp_to_pkpi(
        pi_pidk_max=xic_pi_pidk_max,
        k_pidk_min=xic_k_pidk_min,
        p_pidp_min=xic_p_pidp_min,
        pi_p_min=xic_pi_p_min,
        pi_pt_min=xic_pi_pt_min,
        k_p_min=xic_k_p_min,
        k_pt_min=xic_k_pt_min,
        p_p_min=xic_p_p_min,
        p_pt_min=xic_p_pt_min,
    )
    pions = basic_builder.make_soft_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min
    )

    return basic_builder.make_twobody(
        particles=[xicp_to_pkpi, pions],
        descriptor="[Xi_cc++ -> Xi_c+ pi+]cc",
        am_min=am_min,
        am_max=am_max,
        name="B2OCXiccpp2XicpPiCombiner_{hash}",
        **decay_arguments,
    )


@configurable
def make_xiccp_to_xic0pi(
    am_min=3571 * MeV,
    am_max=3671 * MeV,
    xic_pi_pidk_max=5,
    xic_k_pidk_min=-5,
    xic_p_pidp_min=-5,
    xic_pi_p_min=2 * GeV,
    xic_pi_pt_min=250 * MeV,
    xic_k_p_min=2 * GeV,
    xic_k_pt_min=250 * MeV,
    xic_p_p_min=2 * GeV,
    xic_p_pt_min=250 * MeV,
    pi_pidk_max=20,
    pi_p_min=2 * GeV,
    pi_pt_min=100 * MeV,
    **decay_arguments,
):
    """
    Return a Xi_cc+ -> Xi_c0 pi+ decay.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->2body
                        maker
    """
    xic0_to_pkkpi = make_xic0_to_pkkpi(
        pi_pidk_max=xic_pi_pidk_max,
        k_pidk_min=xic_k_pidk_min,
        p_pidp_min=xic_p_pidp_min,
        pi_p_min=xic_pi_p_min,
        pi_pt_min=xic_pi_pt_min,
        k_p_min=xic_k_p_min,
        k_pt_min=xic_k_pt_min,
        p_p_min=xic_p_p_min,
        p_pt_min=xic_p_pt_min,
    )
    pions = basic_builder.make_soft_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min
    )

    return basic_builder.make_twobody(
        particles=[xic0_to_pkkpi, pions],
        descriptor="[Xi_cc+ -> Xi_c0 pi+]cc",
        am_min=am_min,
        am_max=am_max,
        name="B2OCXiccp2Xic0PiCombiner_{hash}",
        **decay_arguments,
    )


@configurable
def make_xiccpp_to_lcpkmpippip(
    am_min=3571 * MeV,
    am_max=3671 * MeV,
    lc_pi_pidk_max=5,
    lc_k_pidk_min=-5,
    lc_p_pidp_min=-5,
    lc_pi_p_min=2 * GeV,
    lc_pi_pt_min=250 * MeV,
    lc_k_p_min=5 * GeV,
    lc_k_pt_min=250 * MeV,
    lc_p_p_min=5 * GeV,
    lc_p_pt_min=250 * MeV,
    pi_pidk_max=10,
    k_pidk_min=-5,
    pi_p_min=2 * GeV,
    pi_pt_min=250 * MeV,
    k_p_min=5 * GeV,
    k_pt_min=250 * MeV,
    **decay_arguments,
):
    """
    Return a Xi_cc++ -> Lambda_c+ K- pi+ pi+ decay.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->4body
                        maker
    """
    lc_to_pkpi = make_lc_to_pkpi(
        pi_pidk_max=lc_pi_pidk_max,
        k_pidk_min=lc_k_pidk_min,
        p_pidp_min=lc_p_pidp_min,
        pi_p_min=lc_pi_p_min,
        pi_pt_min=lc_pi_pt_min,
        k_p_min=lc_k_p_min,
        k_pt_min=lc_k_pt_min,
        p_p_min=lc_p_p_min,
        p_pt_min=lc_p_pt_min,
    )
    kaons = basic_builder.make_soft_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min
    )
    pions = basic_builder.make_soft_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min
    )

    return basic_builder.make_fourbody(
        particles=[lc_to_pkpi, kaons, pions, pions],
        descriptor="[Xi_cc++ -> Lambda_c+ K- pi+ pi+]cc",
        am_min=am_min,
        am_max=am_max,
        name="B2OCXiccpp2LcPKmPipPipCombiner_{hash}",
        **decay_arguments,
    )


@configurable
def make_xiccp_to_lcpkmpip(
    am_min=3571 * MeV,
    am_max=3671 * MeV,
    lc_pi_pidk_max=5,
    lc_k_pidk_min=-5,
    lc_p_pidp_min=-5,
    lc_pi_p_min=2 * GeV,
    lc_pi_pt_min=250 * MeV,
    lc_k_p_min=2 * GeV,
    lc_k_pt_min=250 * MeV,
    lc_p_p_min=5 * GeV,
    lc_p_pt_min=250 * MeV,
    pi_pidk_max=10,
    k_pidk_min=-5,
    pi_p_min=2 * GeV,
    pi_pt_min=250 * MeV,
    k_p_min=5 * GeV,
    k_pt_min=250 * MeV,
    **decay_arguments,
):
    """
    Return a Xi_cc+ -> Lambda_c+ K- pi+ decay.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->4body
                        maker
    """
    lc_to_pkpi = make_lc_to_pkpi(
        pi_pidk_max=lc_pi_pidk_max,
        k_pidk_min=lc_k_pidk_min,
        p_pidp_min=lc_p_pidp_min,
        pi_p_min=lc_pi_p_min,
        pi_pt_min=lc_pi_pt_min,
        k_p_min=lc_k_p_min,
        k_pt_min=lc_k_pt_min,
        p_p_min=lc_p_p_min,
        p_pt_min=lc_p_pt_min,
    )
    kaons = basic_builder.make_soft_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min
    )
    pions = basic_builder.make_soft_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min
    )

    return basic_builder.make_threebody(
        particles=[lc_to_pkpi, kaons, pions],
        descriptor="[Xi_cc+ -> Lambda_c+ K- pi+]cc",
        am_min=am_min,
        am_max=am_max,
        name="B2OCXiccp2LcpKmPIpCombiner_{hash}",
        **decay_arguments,
    )


@configurable
def make_xiccp_to_dppk(
    am_min=3571 * MeV,
    am_max=3671 * MeV,
    d_pi_pidk_max=5,
    d_k_pidk_min=-5,
    d_pi_p_min=2 * GeV,
    d_pi_pt_min=250 * MeV,
    d_k_p_min=2 * GeV,
    d_k_pt_min=250 * MeV,
    k_pidk_min=-10,
    p_pidp_min=-5,
    k_p_min=2 * GeV,
    k_pt_min=100 * MeV,
    p_p_min=2 * GeV,
    p_pt_min=250 * MeV,
    **decay_arguments,
):
    """
    Return a Xi_cc+ -> D+ p+ K- decay.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->4body
                        maker
    """
    dp_to_kmpippip = d_builder.make_dplus_to_kmpippip(
        pi_pidk_max=d_pi_pidk_max,
        k_pidk_min=d_k_pidk_min,
        pi_p_min=d_pi_p_min,
        pi_pt_min=d_pi_pt_min,
        k_p_min=d_k_p_min,
        k_pt_min=d_k_pt_min,
    )
    protons = basic_builder.make_protons(
        p_pidp_min=p_pidp_min, p_min=p_p_min, pt_min=p_pt_min
    )
    kaons = basic_builder.make_soft_kaons(
        k_pidk_min=k_pidk_min, p_min=k_p_min, pt_min=k_pt_min
    )

    return basic_builder.make_threebody(
        particles=[dp_to_kmpippip, protons, kaons],
        descriptor="[Xi_cc+ -> D+ p+ K-]cc",
        am_min=am_min,
        am_max=am_max,
        name="B2OCXiccp2DpPKCombiner_{hash}",
        **decay_arguments,
    )


@configurable
def make_tight_lc_to_pkpi(
    p_pidp_min=-5, k_pidk_min=-5, pi_pidk_max=5, **decay_arguments
):
    """make lc with tighter P, PT, PID"""
    return make_lc_to_pkpi(
        pi_pidk_max=pi_pidk_max,
        k_pidk_min=k_pidk_min,
        p_pidp_min=p_pidp_min,
        pi_p_min=3 * GeV,
        pi_pt_min=300 * MeV,
        k_p_min=5 * GeV,
        k_pt_min=500 * MeV,
        p_p_min=8 * GeV,
        p_pt_min=500 * MeV,
        **decay_arguments,
    )


@configurable
def make_tight_lc_to_pkpi_for_xibc(**decay_arguments):
    """make Lc with tighter K, pi and FD&vtx selections"""
    return make_tight_lc_to_pkpi(
        am_min=2246 * MeV,
        am_max=2326 * MeV,
        p_pidp_min=-2,
        k_pidk_min=-2,
        pi_pidk_max=2,
        vchi2pdof_max=7,
        bpvvdchi2_min=36,
        **decay_arguments,
    )


@configurable
def make_tight_lc_to_pkk(**decay_arguments):
    """make lc with tighter P, PT, PID"""
    return make_lc_to_pkk(
        k_pidk_min=-5,
        p_pidp_min=-5,
        k_p_min=5 * GeV,
        k_pt_min=500 * MeV,
        p_p_min=8 * GeV,
        p_pt_min=500 * MeV,
        **decay_arguments,
    )


@configurable
def make_tight_lc_to_ppipi(**decay_arguments):
    """make lc with tighter P, PT, PID"""
    return make_lc_to_ppipi(
        pi_pidk_max=5,
        p_pidp_min=-5,
        pi_p_min=3 * GeV,
        pi_pt_min=300 * MeV,
        p_p_min=8 * GeV,
        p_pt_min=500 * MeV,
        **decay_arguments,
    )


@configurable
def make_tight_lc_to_phh(
    name="B2OCLc2PHHCombiner_{hash}",
    pi_pidk_max=5,
    k_pidk_min=-5,
    p_pidp_min=-5,
    p_p_min=3 * GeV,
    p_pt_min=500 * MeV,
    pi_p_min=2 * GeV,
    pi_pt_min=250 * MeV,
    k_p_min=2 * GeV,
    k_pt_min=250 * MeV,
    **decay_arguments,
):
    lc_to_pkpi = make_lc_to_pkpi(
        pi_pidk_max=pi_pidk_max,
        k_pidk_min=k_pidk_min,
        p_pidp_min=p_pidp_min,
        p_p_min=p_p_min,
        p_pt_min=p_pt_min,
        pi_p_min=pi_p_min,
        pi_pt_min=pi_pt_min,
        k_p_min=k_p_min,
        k_pt_min=k_pt_min,
        **decay_arguments,
    )
    lc_to_pkk = make_lc_to_pkk(
        k_pidk_min=k_pidk_min,
        p_pidp_min=p_pidp_min,
        p_p_min=p_p_min,
        p_pt_min=p_pt_min,
        k_p_min=k_p_min,
        k_pt_min=k_pt_min,
        **decay_arguments,
    )
    lc_to_ppipi = make_lc_to_ppipi(
        p_pidp_min=p_pidp_min,
        p_p_min=p_p_min,
        p_pt_min=p_pt_min,
        pi_pidk_max=pi_pidk_max,
        pi_p_min=pi_p_min,
        pi_pt_min=pi_pt_min,
        **decay_arguments,
    )
    return ParticleContainersMerger([lc_to_pkpi, lc_to_pkk, lc_to_ppipi], name=name)


@configurable
def make_tight_xicp_to_pkpi(
    pi_pidk_max=5, k_pidk_min=-5, p_pidp_min=-5, **decay_arguments
):
    """make xicp with tighter P, PT, PID"""
    return make_xicp_to_pkpi(
        pi_pidk_max=pi_pidk_max,
        k_pidk_min=k_pidk_min,
        p_pidp_min=p_pidp_min,
        pi_p_min=3 * GeV,
        pi_pt_min=300 * MeV,
        k_p_min=5 * GeV,
        k_pt_min=500 * MeV,
        p_p_min=8 * GeV,
        p_pt_min=500 * MeV,
        **decay_arguments,
    )


@configurable
def make_tight_xic0_to_pkkpi(
    pi_pidk_max=5, k_pidk_min=-5, p_pidp_min=-5, **decay_arguments
):
    """make Xic0 with tighter P, PT, PID"""
    return make_xic0_to_pkkpi(
        pi_pidk_max=pi_pidk_max,
        k_pidk_min=k_pidk_min,
        p_pidp_min=p_pidp_min,
        pi_p_min=2 * GeV,
        pi_pt_min=250 * MeV,
        k_p_min=5 * GeV,
        k_pt_min=500 * MeV,
        p_p_min=8 * GeV,
        p_pt_min=500 * MeV,
        **decay_arguments,
    )


@configurable
def make_tight_xicp_to_pkpi_for_xibc(**decay_arguments):
    """make Xic with tighter K, pi and FD&vtx selections"""
    return make_tight_xicp_to_pkpi(
        am_min=2428 * MeV,
        am_max=2508 * MeV,
        p_pidp_min=-2,
        k_pidk_min=-2,
        pi_pidk_max=2,
        vchi2pdof_max=7,
        bpvvdchi2_min=36,
        **decay_arguments,
    )


@configurable
def make_tight_xic0_to_pkkpi_for_xibc(**decay_arguments):
    """make Xic with tighter K, pi and FD&vtx selections"""
    return make_tight_xic0_to_pkkpi(
        am_min=2431 * MeV,
        am_max=2511 * MeV,
        p_pidp_min=-2,
        k_pidk_min=-2,
        pi_pidk_max=2,
        vchi2pdof_max=7,
        bpvvdchi2_min=36,
        **decay_arguments,
    )


@configurable
def make_tight_omegac0_to_pkkpi(**decay_arguments):
    """make omegac0 with tighter P, PT, PID"""
    return make_omegac0_to_pkkpi(
        pi_pidk_max=5,
        k_pidk_min=-5,
        p_pidp_min=-5,
        pi_p_min=2 * GeV,
        pi_pt_min=250 * MeV,
        k_p_min=5 * GeV,
        k_pt_min=500 * MeV,
        p_p_min=8 * GeV,
        p_pt_min=500 * MeV,
        **decay_arguments,
    )


@configurable
def make_sigmacpp_to_lcppi(
    am_min=2254 * MeV,
    am_max=2654 * MeV,
    lc_pi_pidk_max=5,
    lc_k_pidk_min=-5,
    lc_p_pidp_min=-5,
    lc_pi_p_min=2 * GeV,
    lc_pi_pt_min=250 * MeV,
    lc_k_p_min=2 * GeV,
    lc_k_pt_min=250 * MeV,
    lc_p_p_min=2 * GeV,
    lc_p_pt_min=250 * MeV,
    pi_pidk_max=20,
    pi_p_min=2 * GeV,
    pi_pt_min=100 * MeV,
    **decay_arguments,
):
    """
    Return a Sigma_c++ -> Lambda_c+ pi+ decay.

    Parameters
    ----------
    **decay_arguments : keyword arguments are passed on to the HC->2body
                        maker
    """
    lc_to_pkpi = make_lc_to_pkpi(
        pi_pidk_max=lc_pi_pidk_max,
        k_pidk_min=lc_k_pidk_min,
        p_pidp_min=lc_p_pidp_min,
        pi_p_min=lc_pi_p_min,
        pi_pt_min=lc_pi_pt_min,
        k_p_min=lc_k_p_min,
        k_pt_min=lc_k_pt_min,
        p_p_min=lc_p_p_min,
        p_pt_min=lc_p_pt_min,
    )
    pions = basic_builder.make_soft_pions(
        pi_pidk_max=pi_pidk_max, p_min=pi_p_min, pt_min=pi_pt_min
    )

    return basic_builder.make_twobody(
        particles=[lc_to_pkpi, pions],
        descriptor="[Sigma_c++ -> Lambda_c+ pi+]cc",
        am_min=am_min,
        am_max=am_max,
        name="B2OCSigmacpp2LcpPiCombiner_{hash}",
        **decay_arguments,
    )
