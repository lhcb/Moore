###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV

from Hlt2Conf.lines.b_to_open_charm.builders import (
    b_builder,
    basic_builder,
    cbaryon_builder,
    d_builder,
)
from Hlt2Conf.lines.b_to_open_charm.utils import check_process

###########################################################
# Form the Xi_bc+ -> Lc+ D0 pi+ pi-, Lc+ -> p K pi,
# NB: Here, D0 == K- pi+ AND K+ pi- (provides a WS sample)
##########################################################


@check_process
def make_XibcpToLcpD0PipPim_LcpToPKPi_D0ToKPiOrKPiPiPi(process):
    Lc = cbaryon_builder.make_tight_lc_to_pkpi_for_xibc()
    Dz = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
    pions = basic_builder.make_tightpid_soft_pions()

    ### applying hard cuts on pion if it's not forming D* with D0 or Sigma_c with Lambda_c
    M13 = F.SUBCOMB(Functor=F.MASS, Indices=[1, 3])
    M23 = F.SUBCOMB(Functor=F.MASS, Indices=[2, 3])
    M14 = F.SUBCOMB(Functor=F.MASS, Indices=[1, 4])

    Dst_23 = (M23 - F.CHILD(2, F.MASS)) < 150 * MeV
    Sc_13 = (M13 - F.CHILD(1, F.MASS)) < 300 * MeV
    Sc_14 = (M14 - F.CHILD(1, F.MASS)) < 300 * MeV

    tight3 = (F.CHILD(3, F.PT) > 500 * MeV) & (F.CHILD(3, F.P) > 5 * GeV)
    tight4 = (F.CHILD(4, F.PT) > 500 * MeV) & (F.CHILD(4, F.P) > 5 * GeV)

    comb123_cut_add = Sc_13 | Dst_23 | tight3
    comb_cut_add = Sc_14 | tight4

    line_alg = b_builder.make_xibc2ccx(
        particles=[Lc, Dz, pions, pions],
        descriptors=[
            "Xi_bc+ -> Lambda_c+ D0 pi+ pi-",
            "Xi_bc~- -> Lambda_c~- D0 pi+ pi-",
        ],
        sum_pt_hbach_min=1.25 * GeV,
        comb123_cut_add=comb123_cut_add,
        comb_cut_add=comb_cut_add,
    )
    return line_alg


@check_process
def make_LbToLcpDstmPipKm_LcpToPKPi_DstmToD0Pi(process):
    if process == "spruce":
        Lc = cbaryon_builder.make_lc_to_pkpi()
        Dstar = d_builder.make_dstar_to_dzeropi_cf()
        pions = basic_builder.make_pions()
        kaons = basic_builder.make_kaons()
    elif process == "hlt2":
        Lc = cbaryon_builder.make_tight_lc_to_pkpi()
        Dstar = d_builder.make_dstar_to_dzeropi_cf()
        pions = basic_builder.make_tightpid_tight_pions()
        kaons = basic_builder.make_tightpid_tight_kaons(k_pidk_min=-2)

    line_alg = b_builder.make_lb(
        particles=[Lc, Dstar, pions, kaons],
        descriptors=[
            "Lambda_b0 -> Lambda_c+ D*(2010)- pi+ K-",
            "Lambda_b~0 -> Lambda_c~- D*(2010)+ pi- K+",
        ],
    )
    return line_alg


@check_process
def make_LbToLcpDmPipKm_LcpToPKPi_DmToKPiPi(process):
    if process == "spruce":
        Lc = cbaryon_builder.make_lc_to_pkpi()
        D = d_builder.make_dplus_to_kpkmpip()
        pions = basic_builder.make_pions()
        kaons = basic_builder.make_kaons()
    elif process == "hlt2":
        Lc = cbaryon_builder.make_tight_lc_to_pkpi()
        D = d_builder.make_tight_dplus_to_kpkmpip()
        pions = basic_builder.make_tightpid_tight_pions()
        kaons = basic_builder.make_tightpid_tight_kaons(k_pidk_min=-2)

    line_alg = b_builder.make_lb(
        particles=[Lc, D, pions, kaons],
        descriptors=[
            "Lambda_b0 -> Lambda_c+ D- pi+ K-",
            "Lambda_b~0 -> Lambda_c~- D+ pi- K+",
        ],
    )
    return line_alg
