###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Booking of B2OC sprucing lines, notice PROCESS = 'spruce'

Usage:
add line names in corresponding list/dict,
or register lines separately at the very end.

Non trivial imports:
prefilters and line_alg ("bare" line builders) like b_to_dh.make_BdToDsmK_DsmToHHH

Output:
updated dictionary of sprucing_lines

To be noted:
"bare" line builders, like like b_to_dh.make_BdToDsmK_DsmToHHH, have PROCESS as
argument to allow ad hoc settings

"""

import Functors as F
from GaudiKernel.SystemOfUnits import MeV
from Moore.config import SpruceLine, register_line_builder
from Moore.streams import DETECTORS
from PyConf import configurable
from SelAlgorithms.monitoring import histogram_1d, monitor

from Hlt2Conf.lines.b_to_open_charm import (
    b_to_cbaryon_h,
    b_to_cbaryon_hh,
    b_to_cbaryons,
    b_to_cbaryons_h,
    b_to_dd,
    b_to_ddh,
    b_to_ddh_standalone,
    b_to_ddhh,
    b_to_dh,
    b_to_dhh,
    b_to_dhhh,
    b_to_dll,
    b_to_dmunu,
    b_to_dx_ltu,
    bbaryon_to_cbaryon_d,
    bbaryon_to_cbaryon_dh,
    bbaryon_to_cbaryon_dhh,
    bbaryon_to_cbaryon_h,
    bbaryon_to_cbaryon_hh,
    bbaryon_to_cbaryon_hhh,
    bbaryon_to_cbaryons_h,
    bbaryon_to_lightbaryon_d,
    bbaryon_to_lightbaryon_dd,
    bbaryon_to_lightbaryon_ddh,
    bbaryon_to_lightbaryon_dh,
    prefilters,
)
from Hlt2Conf.lines.b_to_open_charm.persistency import make_iso_particles
from Hlt2Conf.lines.b_to_open_charm.utils import update_makers, validate_config

PROCESS = "spruce"
sprucing_lines = {}

##############################################
# Read and store all line makers in one dict #
##############################################

line_makers = {}
update_makers(line_makers, b_to_dh)
update_makers(line_makers, b_to_dx_ltu)
update_makers(line_makers, b_to_dll)
update_makers(line_makers, b_to_dhh)
update_makers(line_makers, b_to_dmunu)
update_makers(line_makers, b_to_dhhh)
update_makers(line_makers, b_to_dd)
update_makers(line_makers, b_to_ddh)
update_makers(line_makers, b_to_ddh_standalone)
update_makers(line_makers, b_to_ddhh)
update_makers(line_makers, b_to_cbaryons)
update_makers(line_makers, b_to_cbaryons_h)
update_makers(line_makers, b_to_cbaryon_h)
update_makers(line_makers, b_to_cbaryon_hh)
update_makers(line_makers, bbaryon_to_cbaryon_h)
update_makers(line_makers, bbaryon_to_cbaryon_hh)
update_makers(line_makers, bbaryon_to_cbaryon_hhh)
update_makers(line_makers, bbaryon_to_cbaryon_d)
update_makers(line_makers, bbaryon_to_cbaryon_dh)
update_makers(line_makers, bbaryon_to_cbaryon_dhh)
update_makers(line_makers, bbaryon_to_cbaryons_h)
update_makers(line_makers, bbaryon_to_lightbaryon_d)
update_makers(line_makers, bbaryon_to_lightbaryon_dh)
update_makers(line_makers, bbaryon_to_lightbaryon_dd)
update_makers(line_makers, bbaryon_to_lightbaryon_ddh)

###################################
# List of Hlt2 lines to filter on #
###################################
topo_lines = ["Hlt2Topo2BodyDecision", "Hlt2Topo3BodyDecision"]

############################################
# Define functions for line booking        #
# Make it possible to register lines       #
# outside this file (e.g. in test scripts) #
############################################


@configurable
def make_sprucing_lines(
    line_dict=sprucing_lines, line_makers=line_makers, all_lines=None, extra_config={}
):
    if not all_lines:
        return

    custom_prescales = extra_config.get("prescale", {})
    custom_mva = extra_config.get("mva", {})
    flavour_tagging = extra_config.get("flavour_tagging", [])
    pv_unbiasing = extra_config.get("pv_unbiasing", [])
    isolation = extra_config.get("isolation", [])

    for decay in all_lines:
        # default configs
        prescale = 1.0
        MVACut = None  # do not apply MVA by default
        include_ft = False
        include_pv_tracks = False
        include_isolation = False

        # custom configs
        if decay in custom_prescales.keys():
            prescale = custom_prescales[decay]
        if decay in flavour_tagging:
            include_ft = True
            include_pv_tracks = True
        if decay in custom_mva.keys():
            MVACut = custom_mva[decay]
        if decay in pv_unbiasing:
            include_pv_tracks = True
        if decay in isolation:
            include_isolation = True

        @register_line_builder(line_dict)
        def make_sprucing_line(
            name="SpruceB2OC_%s" % decay,
            maker_name="make_%s" % decay,
            prescale=prescale,
            MVACut=MVACut,
            include_ft=include_ft,
            include_pv_tracks=include_pv_tracks,
            include_isolation=include_isolation,
        ):
            extra_outputs = []
            if MVACut is None:  # do not apply any MVA
                line_alg = line_makers[maker_name](process=PROCESS)
            else:  # use custom MVA
                line_alg = line_makers[maker_name](process=PROCESS, MVACut=MVACut)
            if include_isolation:
                extra_outputs += make_iso_particles(line_alg, name="B")
            return SpruceLine(
                name=name,
                prescale=prescale,
                hlt2_filter_code=topo_lines,
                algs=prefilters.b2oc_prefilters() + [line_alg],
                extra_outputs=extra_outputs,
                tagging_particles=include_ft,
                pv_tracks=include_pv_tracks,
            )


#############################################
# Lists and dicts of lines to register      #
# Authors should add decays here            #
# NO `SpruceB2OC_` prefix or `` suffix      #
#############################################

# list of all lines
all_lines = [
    # lines from b_to_dh
    "BdToDmPi_DmToPimPimKp",
    #'BdToDsstmK_DsstmToDsmGamma_DsmToHHH',
    "BdToDsstmPi_DsstmToDsmGamma_DsmToHHH",
    "BuToD0Pi_D0ToHH",
    "BuToD0K_D0ToHH",
    #'BuToD0Pi_D0ToKsLLPi0Resolved',
    #'BuToD0Pi_D0ToKsDDPi0Resolved',
    #'BuToD0Pi_D0ToKsLLPi0Merged',
    #'BuToD0Pi_D0ToKsDDPi0Merged',
    #'BuToD0K_D0ToKsLLPi0Resolved',
    #'BuToD0K_D0ToKsDDPi0Resolved',
    #'BuToD0K_D0ToKsLLPi0Merged',
    #'BuToD0K_D0ToKsDDPi0Merged',
    "BuToD0Pi_PartialD0ToKsLLHH",
    "BuToD0Pi_PartialD0ToKsDDHH",
    "BuToD0K_PartialD0ToKsLLHH",
    "BuToD0K_PartialD0ToKsDDHH",
    "BuToD0Pi_PartialD0ToKsLLHHWS",
    "BuToD0Pi_PartialD0ToKsDDHHWS",
    "BuToD0K_PartialD0ToKsLLHHWS",
    "BuToD0K_PartialD0ToKsDDHHWS",
    "BuToD0Pi_D0ToKsLLHH",
    "BuToD0Pi_D0ToKsDDHH",
    "BuToD0K_D0ToKsLLHH",
    "BuToD0K_D0ToKsDDHH",
    "BuToD0Pi_D0ToHHHH",
    "BuToD0K_D0ToHHHH",
    #'BuToD0Pi_D0ToKsLLHHPi0Resolved',
    #'BuToD0Pi_D0ToKsDDHHPi0Resolved',
    #'BuToD0Pi_D0ToKsLLHHPi0Merged',
    #'BuToD0Pi_D0ToKsDDHHPi0Merged',
    #'BuToD0K_D0ToKsLLHHPi0Resolved',
    #'BuToD0K_D0ToKsDDHHPi0Resolved',
    #'BuToD0K_D0ToKsLLHHPi0Merged',
    #'BuToD0K_D0ToKsDDHHPi0Merged',
    #'BuToD0Pi_D0ToKsLLHHWSPi0Resolved',
    #'BuToD0Pi_D0ToKsDDHHWSPi0Resolved',
    #'BuToD0Pi_D0ToKsLLHHWSPi0Merged',
    #'BuToD0Pi_D0ToKsDDHHWSPi0Merged',
    #'BuToD0K_D0ToKsLLHHWSPi0Resolved',
    #'BuToD0K_D0ToKsDDHHWSPi0Resolved',
    #'BuToD0K_D0ToKsLLHHWSPi0Merged',
    #'BuToD0K_D0ToKsDDHHWSPi0Merged',
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToHH',
    #'BuToDst0K_Dst0ToD0Gamma_D0ToHH',
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHH',
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToHH',
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToHHWS',
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHHWS',
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToHHWS',
    #'BuToDst0K_Dst0ToD0Gamma_D0ToHHWS',
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHHWS',
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToHHWS',
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToKsLLHH',
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToKsDDHH',
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToKsLLHH',
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToKsDDHH',
    "BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsLLHH",
    "BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsDDHH",
    "BuToDst0K_Dst0ToD0Gamma_D0ToKsLLHH",
    "BuToDst0K_Dst0ToD0Gamma_D0ToKsDDHH",
    "BuToDst0K_Dst0ToD0Pi0Resolved_D0ToKsLLHH",
    "BuToDst0K_Dst0ToD0Pi0Resolved_D0ToKsDDHH",
    "BuToDst0K_Dst0ToD0Pi0Merged_D0ToKsLLHH",
    "BuToDst0K_Dst0ToD0Pi0Merged_D0ToKsDDHH",
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToKsLLHHWS',
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToKsDDHHWS',
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToKsLLHHWS',
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToKsDDHHWS',
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsLLHHWS',
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsDDHHWS',
    #'BuToDst0K_Dst0ToD0Gamma_D0ToKsLLHHWS',
    #'BuToDst0K_Dst0ToD0Gamma_D0ToKsDDHHWS',
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToKsLLHHWS',
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToKsDDHHWS',
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToKsLLHHWS',
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToKsDDHHWS',
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToHHHH',
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHHHH',
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToHHHH',
    #'BuToDst0K_Dst0ToD0Gamma_D0ToHHHH',
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHHHH',
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToHHHH',
    #'BuToDst0Pi_Dst0ToD0Gamma_D0ToHHHHWS',
    #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHHHHWS',
    #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToHHHHWS',
    #'BuToDst0K_Dst0ToD0Gamma_D0ToHHHHWS',
    #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHHHHWS',
    #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToHHHHWS',
    "BcToD0Pi_D0ToHH",
    "BcToD0K_D0ToHH",
    "BcToD0Pi_D0ToKsLLHH",
    "BcToD0Pi_D0ToKsDDHH",
    "BcToD0K_D0ToKsLLHH",
    "BcToD0K_D0ToKsDDHH",
    "BcToD0Pi_D0ToHHHH",
    "BcToD0K_D0ToHHHH",
    "BcToDmKst0_DmToHHH",
    "BcToDsmKst0_DsmToHHH",
    "BcToDmKsLL_DmToHHH",
    "BcToDmKsDD_DmToHHH",
    "BcToDsmKsLL_DsmToHHH",
    "BcToDsmKsDD_DsmToHHH",
    "BcToDmF0_DmToHHH",
    "BcToDsmF0_DsmToHHH",
    "BcToD0Pi_D0ToHHWS",
    "BcToD0K_D0ToHHWS",
    "BcToD0Pi_D0ToKsLLHHWS",
    "BcToD0Pi_D0ToKsDDHHWS",
    "BcToD0K_D0ToKsLLHHWS",
    "BcToD0K_D0ToKsDDHHWS",
    "BcToD0Pi_D0ToHHHHWS",
    "BcToD0K_D0ToHHHHWS",
    "BcToDmKst0_DmToHHHWS",
    "BcToDsmKst0_DsmToHHHWS",
    "BcToDmPhi_DmToHHH",
    "BcToDsmPhi_DsmToHHH",
    "BuToDsstpGamma_DsstpToDspGamma_DspToHHH",
    "BuToDspGamma_DspToHHH",
    "BuToDsstpPi0Resolved_DsstpToDspGamma_DspToHHH",
    "BuToDspPi0Resolved_DspToHHH",
    "BuToDsstpPi0Merged_DsstpToDspGamma_DspToHHH",
    "BuToDspPi0Merged_DspToHHH",
    # lines from b_to_dx_ltu
    "BdToDsmK_DsmToKpKmPim_LTU",
    # lines from b_to_dll
    "BcToDsmMupMum_DsmToHHH",
    "BcToDspMumMum_DspToHHH",
    "BcToDsmMumMumWS_DsmToHHH",
    # lines from b_to_dhh
    #'BdToD0PiPi_D0ToHH',
    #'BdToD0KPi_D0ToHH',
    #'BdToD0KK_D0ToHH',
    #'BdToD0PbarP_D0ToHH',
    #'BdToD0PiPiWS_D0ToHH',
    #'BdToD0KPiWS_D0ToHH',
    #'BdToD0KKWS_D0ToHH',
    #'BdToD0PbarPWS_D0ToHH',
    "BdToD0PiPi_D0ToHHHH",
    "BdToD0KPi_D0ToHHHH",
    "BdToD0KK_D0ToHHHH",
    "BdToD0PiPiWS_D0ToHHHH",
    "BdToD0KPiWS_D0ToHHHH",
    "BdToD0KKWS_D0ToHHHH",
    "BdToDstpKsLLPi_DstpToD0Pi_D0ToHH",
    "BdToDstpKsDDPi_DstpToD0Pi_D0ToHH",
    "BdToDstpKsLLK_DstpToD0Pi_D0ToHH",
    "BdToDstpKsDDK_DstpToD0Pi_D0ToHH",
    "BdToDstpKsLLPi_DstpToD0Pi_D0ToHHHH",
    "BdToDstpKsDDPi_DstpToD0Pi_D0ToHHHH",
    "BdToDstpKsLLK_DstpToD0Pi_D0ToHHHH",
    "BdToDstpKsDDK_DstpToD0Pi_D0ToHHHH",
    #'BdToDst0KPi_Dst0ToD0Gamma_D0ToHH',
    #'BdToDst0KPi_Dst0ToD0Pi0Resolved_D0ToHH',
    #'BdToDst0KK_Dst0ToD0Gamma_D0ToHH',
    #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToHH',
    #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToHH',
    #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToHH',
    "BdToDsstmKsLLPi_DsstmToDsmGamma_DsmToHHH",
    "BdToDsstmKsDDPi_DsstmToDsmGamma_DsmToHHH",
    "BuToDpKPi_DpToHHH",
    "BuToD0KsLLPi_D0ToHH",
    "BuToD0KsDDPi_D0ToHH",
    "BuToD0KsLLPi_D0ToKsLLHH",
    "BuToD0KsDDPi_D0ToKsLLHH",
    "BuToD0KsLLPi_D0ToKsDDHH",
    "BuToD0KsDDPi_D0ToKsDDHH",
    "BuToD0KsLLPi_D0ToHHHH",
    "BuToD0KsDDPi_D0ToHHHH",
    "BuToD0KsLLPi_D0ToHHWS",
    "BuToD0KsDDPi_D0ToHHWS",
    "BuToD0KsLLPi_D0ToKsLLHHWS",
    "BuToD0KsDDPi_D0ToKsLLHHWS",
    "BuToD0KsLLPi_D0ToKsDDHHWS",
    "BuToD0KsDDPi_D0ToKsDDHHWS",
    "BuToD0KsLLPi_D0ToHHHHWS",
    "BuToD0KsDDPi_D0ToHHHHWS",
    "BdToDmKsLLPi_DmToHHH",
    "BdToDmKsDDPi_DmToHHH",
    "BdToDsmKsLLPi_DsmToHHH",
    "BdToDsmKsDDPi_DsmToHHH",
    #'BuToD0PiPi0Resolved_D0ToHH',
    #'BuToD0PiPi0Merged_D0ToHH',
    #'BuToD0KPi0Resolved_D0ToHH',
    #'BuToD0KPi0Merged_D0ToHH',
    #'BuToD0PiPi0Resolved_D0ToHHWS',
    #'BuToD0PiPi0Merged_D0ToHHWS',
    #'BuToD0KPi0Resolved_D0ToHHWS',
    #'BuToD0KPi0Merged_D0ToHHWS',
    #'BuToD0PiPi0Resolved_D0ToKsLLHH',
    #'BuToD0PiPi0Resolved_D0ToKsDDHH',
    #'BuToD0PiPi0Merged_D0ToKsLLHH',
    #'BuToD0PiPi0Merged_D0ToKsDDHH',
    #'BuToD0KPi0Resolved_D0ToKsLLHH',
    #'BuToD0KPi0Resolved_D0ToKsDDHH',
    #'BuToD0KPi0Merged_D0ToKsLLHH',
    #'BuToD0KPi0Merged_D0ToKsDDHH',
    #'BuToD0PiPi0Resolved_D0ToKsLLHHWS',
    #'BuToD0PiPi0Resolved_D0ToKsDDHHWS',
    #'BuToD0PiPi0Merged_D0ToKsLLHHWS',
    #'BuToD0PiPi0Merged_D0ToKsDDHHWS',
    #'BuToD0KPi0Resolved_D0ToKsLLHHWS',
    #'BuToD0KPi0Resolved_D0ToKsDDHHWS',
    #'BuToD0KPi0Merged_D0ToKsLLHHWS',
    #'BuToD0KPi0Merged_D0ToKsDDHHWS',
    #'BuToD0PiPi0Resolved_D0ToHHHH',
    #'BuToD0PiPi0Merged_D0ToHHHH',
    #'BuToD0KPi0Resolved_D0ToHHHH',
    #'BuToD0KPi0Merged_D0ToHHHH',
    #'BuToD0PiPi0Resolved_D0ToHHHHWS',
    #'BuToD0PiPi0Merged_D0ToHHHHWS',
    #'BuToD0KPi0Resolved_D0ToHHHHWS',
    #'BuToD0KPi0Merged_D0ToHHHHWS',
    # lines from b_to_dmunu
    #'BdToDstmMuNu_DstmToD0Pi_D0ToKsLLHHPi0Resolved',
    #'BdToDstmMuNu_DstmToD0Pi_D0ToKsDDHHPi0Resolved',
    #'BdToDstmMuNu_DstmToD0Pi_D0ToKsLLHHPi0Merged',
    #'BdToDstmMuNu_DstmToD0Pi_D0ToKsDDHHPi0Merged',
    # lines from b_to_dhhh
    "BdToDsmKKPi_DsmToKmKpPim",
    "BdToDsmKPiPi_DsmToKmKpPim",
    "BdToDsmPiPiPi_DsmToKmKpPim",
    "BdToDmKPiPi_DmToPimPimKp",
    "BdToDmPiPiPi_DmToPimPimKp",
    "BdToDstpPiPiPi_DstpToD0Pi_D0ToHH",
    "BdToDstpKPiPi_DstpToD0Pi_D0ToHH",
    "BdToDstpKKPi_DstpToD0Pi_D0ToHH",
    "BdToDstpKKK_DstpToD0Pi_D0ToHH",
    "BdToDstpPiPiPi_DstpToD0Pi_D0ToHHHH",
    "BdToDstpKPiPi_DstpToD0Pi_D0ToHHHH",
    "BdToDstpKKPi_DstpToD0Pi_D0ToHHHH",
    "BdToDstpKKK_DstpToD0Pi_D0ToHHHH",
    "BuToD0PiPiPi_D0ToHHWS",
    "BuToD0KPiPi_D0ToHHWS",
    "BuToD0PiPiPi_D0ToHHHHWS",
    "BuToD0KPiPi_D0ToHHHHWS",
    "BdToDstpPbarPPi_DstpToD0Pi_D0ToHH",
    "BdToDstpPbarPK_DstpToD0Pi_D0ToHH",
    "BdToDstpPbarPPi_DstpToD0Pi_D0ToHHHH",
    "BdToDstpPbarPK_DstpToD0Pi_D0ToHHHH",
    "BuToD0PiPiPi_D0ToHHHH",
    "BuToD0KPiPi_D0ToHHHH",
    "BuToD0PiPiPi_D0ToKsLLHH",
    "BuToD0PiPiPi_D0ToKsDDHH",
    "BuToD0KPiPi_D0ToKsLLHH",
    "BuToD0KPiPi_D0ToKsDDHH",
    "BdToDsmPbarPPi_DsmToKmKpPim",
    "BdToDmPbarPPi_DmToPimPimKp",
    "BdToDmKKPi_DmToPimPimKp",
    "BuToD0PbarPPi_D0ToKPi",
    "BuToD0KPiPi_D0ToHH",
    "BuToD0KKPi_D0ToKPi",
    "BuToD0PiPiPi_D0ToHH",
    # lines from b_to_dd
    "BdToD0D0_D0ToHH",
    "BdToD0D0_D0ToHH_D0ToHHHH",
    "BdToD0D0_D0ToHHHH",
    "BdToD0D0_D0ToHH_D0ToKsLLHH",
    "BdToD0D0_D0ToHH_D0ToKsDDHH",
    "BdToD0D0_D0ToKsLLHH",
    "BdToD0D0_D0ToKsLLHH_D0ToKsDDHH",
    "BdToD0D0_D0ToKsDDHH",
    "BdToD0D0_D0ToKsLLHH_D0ToHHHH",
    "BdToD0D0_D0ToKsDDHH_D0ToHHHH",
    "BdToDpDm_DpToHHH",
    "BdToDspDm_DspToHHH_DmToHHH",
    "BdToDspDsm_DspToHHH",
    "BdToDstpDm_DstpToD0Pi_D0ToHHHH_DmToHHH",
    "BdToDstpDm_DstpToD0Pi_D0ToHH_DmToHHH",
    "BdToDstpDsm_DstpToD0Pi_D0ToHHHH_DsmToHHH",
    "BdToDstpDsm_DstpToD0Pi_D0ToHH_DsmToHHH",
    "BdToDstpDstm_DstpToD0Pi_D0ToHH",
    "BdToDstpDstm_DstpToD0Pi_D0ToHHHH",
    "BdToDstpDstm_DstpToD0Pi_D0ToHH_D0ToHHHH",
    # lines from b_to_ddh
    "BdToDsstpD0Pi_DsstpToDspGamma_DspToHHH_D0ToKPiOrKPiPiPi",
    "BuToDsstpDmPi_DsstpToDspGamma_DspToHHH_DmToHHH",
    "BuToDst0D0K_Dst0ToD0Gamma_D0ToHH_D0ToHH",
    # "BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToHH_D0ToHH",
    "BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToHH_D0ToHH",
    "BuToDst0D0K_Dst0ToD0Gamma_D0ToKsLLHH_D0ToKsLLHH",
    # "BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToKsLLHH_D0ToKsLLHH",
    "BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToKsLLHH_D0ToKsLLHH",
    "BuToDst0D0K_Dst0ToD0Gamma_D0ToKsDDHH_D0ToKsDDHH",
    # "BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToKsDDHH_D0ToKsDDHH",
    "BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToKsDDHH_D0ToKsDDHH",
    "BuToDst0D0K_Dst0ToD0Gamma_D0ToHHHH_D0ToHHHH",
    # "BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToHHHH_D0ToHHHH",
    "BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToHHHH_D0ToHHHH",
    "BuToDst0D0K_Dst0ToD0Gamma_D0ToHH_D0ToKsLLHH",
    # "BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToHH_D0ToKsLLHH",
    "BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToHH_D0ToKsLLHH",
    "BuToDst0D0K_Dst0ToD0Gamma_D0ToKsLLHH_D0ToHH",
    # "BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToKsLLHH_D0ToHH",
    "BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToKsLLHH_D0ToHH",
    "BuToDst0D0K_Dst0ToD0Gamma_D0ToHH_D0ToKsDDHH",
    # "BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToHH_D0ToKsDDHH",
    "BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToHH_D0ToKsDDHH",
    "BuToDst0D0K_Dst0ToD0Gamma_D0ToKsDDHH_D0ToHH",
    # "BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToKsDDHH_D0ToHH",
    "BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToKsDDHH_D0ToHH",
    "BuToDst0D0K_Dst0ToD0Gamma_D0ToHH_D0ToHHHH",
    # "BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToHH_D0ToHHHH",
    "BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToHH_D0ToHHHH",
    "BuToDst0D0K_Dst0ToD0Gamma_D0ToHHHH_D0ToHH",
    # "BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToHHHH_D0ToHH",
    "BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToHHHH_D0ToHH",
    "BuToDst0D0K_Dst0ToD0Gamma_D0ToKsLLHH_D0ToKsDDHH",
    # "BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToKsLLHH_D0ToKsDDHH",
    "BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToKsLLHH_D0ToKsDDHH",
    "BuToDst0D0K_Dst0ToD0Gamma_D0ToKsDDHH_D0ToKsLLHH",
    # "BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToKsDDHH_D0ToKsLLHH",
    "BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToKsDDHH_D0ToKsLLHH",
    "BuToDst0D0K_Dst0ToD0Gamma_D0ToHHHH_D0ToKsLLHH",
    # "BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToHHHH_D0ToKsLLHH",
    "BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToHHHH_D0ToKsLLHH",
    "BuToDst0D0K_Dst0ToD0Gamma_D0ToKsLLHH_D0ToHHHH",
    # "BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToKsLLHH_D0ToHHHH",
    "BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToKsLLHH_D0ToHHHH",
    "BuToDst0D0K_Dst0ToD0Gamma_D0ToHHHH_D0ToKsDDHH",
    # "BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToHHHH_D0ToKsDDHH",
    "BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToHHHH_D0ToKsDDHH",
    "BuToDst0D0K_Dst0ToD0Gamma_D0ToKsDDHH_D0ToHHHH",
    # "BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToKsDDHH_D0ToHHHH",
    "BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToKsDDHH_D0ToHHHH",
    "BdToDst0DstK_Dst0ToD0Gamma_DstToD0Pi_D0ToHH_D0ToHH",
    "BdToDst0DstK_Dst0ToD0Gamma_DstToD0Pi_D0ToKsLLHH_D0ToKsLLHH",
    "BdToDst0DstK_Dst0ToD0Gamma_DstToD0Pi_D0ToKsDDHH_D0ToKsDDHH",
    "BdToDst0DstK_Dst0ToD0Gamma_DstToD0Pi_D0ToHHHH_D0ToHHHH",
    "BdToDst0DstK_Dst0ToD0Gamma_DstToD0Pi_D0ToHH_D0ToKsLLHH",
    "BdToDst0DstK_Dst0ToD0Gamma_DstToD0Pi_D0ToKsLLHH_D0ToHH",
    "BdToDst0DstK_Dst0ToD0Gamma_DstToD0Pi_D0ToHH_D0ToKsDDHH",
    "BdToDst0DstK_Dst0ToD0Gamma_DstToD0Pi_D0ToKsDDHH_D0ToHH",
    "BdToDst0DstK_Dst0ToD0Gamma_DstToD0Pi_D0ToKsLLHH_D0ToKsDDHH",
    "BdToDst0DstK_Dst0ToD0Gamma_DstToD0Pi_D0ToKsDDHH_D0ToKsLLHH",
    "BdToDst0DstK_Dst0ToD0Gamma_DstToD0Pi_D0ToHH_D0ToHHHH",
    "BdToDst0DstK_Dst0ToD0Gamma_DstToD0Pi_D0ToHHHH_D0ToHH",
    "BdToDst0DstK_Dst0ToD0Gamma_DstToD0Pi_D0ToHHHH_D0ToKsLLHH",
    "BdToDst0DstK_Dst0ToD0Gamma_DstToD0Pi_D0ToKsLLHH_D0ToHHHH",
    "BdToDst0DstK_Dst0ToD0Gamma_DstToD0Pi_D0ToHHHH_D0ToKsDDHH",
    "BdToDst0DstK_Dst0ToD0Gamma_DstToD0Pi_D0ToKsDDHH_D0ToHHHH",
    # "BdToDst0DstK_Dst0ToD0Pi0Merged_DstToD0Pi_D0ToHH_D0ToHH",
    # "BdToDst0DstK_Dst0ToD0Pi0Merged_DstToD0Pi_D0ToKsLLHH_D0ToKsLLHH",
    # "BdToDst0DstK_Dst0ToD0Pi0Merged_DstToD0Pi_D0ToKsDDHH_D0ToKsDDHH",
    # "BdToDst0DstK_Dst0ToD0Pi0Merged_DstToD0Pi_D0ToHHHH_D0ToHHHH",
    # "BdToDst0DstK_Dst0ToD0Pi0Merged_DstToD0Pi_D0ToHH_D0ToKsLLHH",
    # "BdToDst0DstK_Dst0ToD0Pi0Merged_DstToD0Pi_D0ToKsLLHH_D0ToHH",
    # "BdToDst0DstK_Dst0ToD0Pi0Merged_DstToD0Pi_D0ToHH_D0ToKsDDHH",
    # "BdToDst0DstK_Dst0ToD0Pi0Merged_DstToD0Pi_D0ToKsDDHH_D0ToHH",
    # "BdToDst0DstK_Dst0ToD0Pi0Merged_DstToD0Pi_D0ToKsLLHH_D0ToKsDDHH",
    # "BdToDst0DstK_Dst0ToD0Pi0Merged_DstToD0Pi_D0ToKsDDHH_D0ToKsLLHH",
    # "BdToDst0DstK_Dst0ToD0Pi0Merged_DstToD0Pi_D0ToHH_D0ToHHHH",
    # "BdToDst0DstK_Dst0ToD0Pi0Merged_DstToD0Pi_D0ToHHHH_D0ToHH",
    # "BdToDst0DstK_Dst0ToD0Pi0Merged_DstToD0Pi_D0ToHHHH_D0ToKsLLHH",
    # "BdToDst0DstK_Dst0ToD0Pi0Merged_DstToD0Pi_D0ToKsLLHH_D0ToHHHH",
    # "BdToDst0DstK_Dst0ToD0Pi0Merged_DstToD0Pi_D0ToHHHH_D0ToKsDDHH",
    # "BdToDst0DstK_Dst0ToD0Pi0Merged_DstToD0Pi_D0ToKsDDHH_D0ToHHHH",
    "BdToDst0DstK_Dst0ToD0Pi0Resolved_DstToD0Pi_D0ToHH_D0ToHH",
    "BdToDst0DstK_Dst0ToD0Pi0Resolved_DstToD0Pi_D0ToKsLLHH_D0ToKsLLHH",
    "BdToDst0DstK_Dst0ToD0Pi0Resolved_DstToD0Pi_D0ToKsDDHH_D0ToKsDDHH",
    "BdToDst0DstK_Dst0ToD0Pi0Resolved_DstToD0Pi_D0ToHHHH_D0ToHHHH",
    "BdToDst0DstK_Dst0ToD0Pi0Resolved_DstToD0Pi_D0ToHH_D0ToKsLLHH",
    "BdToDst0DstK_Dst0ToD0Pi0Resolved_DstToD0Pi_D0ToKsLLHH_D0ToHH",
    "BdToDst0DstK_Dst0ToD0Pi0Resolved_DstToD0Pi_D0ToHH_D0ToKsDDHH",
    "BdToDst0DstK_Dst0ToD0Pi0Resolved_DstToD0Pi_D0ToKsDDHH_D0ToHH",
    "BdToDst0DstK_Dst0ToD0Pi0Resolved_DstToD0Pi_D0ToKsLLHH_D0ToKsDDHH",
    "BdToDst0DstK_Dst0ToD0Pi0Resolved_DstToD0Pi_D0ToKsDDHH_D0ToKsLLHH",
    "BdToDst0DstK_Dst0ToD0Pi0Resolved_DstToD0Pi_D0ToHH_D0ToHHHH",
    "BdToDst0DstK_Dst0ToD0Pi0Resolved_DstToD0Pi_D0ToHHHH_D0ToHH",
    "BdToDst0DstK_Dst0ToD0Pi0Resolved_DstToD0Pi_D0ToHHHH_D0ToKsLLHH",
    "BdToDst0DstK_Dst0ToD0Pi0Resolved_DstToD0Pi_D0ToKsLLHH_D0ToHHHH",
    "BdToDst0DstK_Dst0ToD0Pi0Resolved_DstToD0Pi_D0ToHHHH_D0ToKsDDHH",
    "BdToDst0DstK_Dst0ToD0Pi0Resolved_DstToD0Pi_D0ToKsDDHH_D0ToHHHH",
    # "BuToDsD0Pi0Merged_DsToKKPi_D0ToKPi",
    # "BuToDsD0Pi0Merged_DsToKKPi_D0ToKPiPiPi",
    "BuToDsD0Pi0Resolved_DsToKKPi_D0ToKPi",
    "BuToDsD0Pi0Resolved_DsToKKPi_D0ToKPiPiPi",
    # "BdToDsDmPi0Merged_DsToKKPi_DToHHH",
    "BdToDsDmPi0Resolved_DsToKKPi_DToHHH",
    "BdToDst0DK_Dst0ToD0Gamma_D0ToHH_DToHHH",
    "BdToDst0DK_Dst0ToD0Gamma_D0ToHHHH_DToHHH",
    "BdToDst0DK_Dst0ToD0Gamma_D0ToKsLLHH_DToHHH",
    "BdToDst0DK_Dst0ToD0Gamma_D0ToKsDDHH_DToHHH",
    # "BdToDst0DK_Dst0ToD0Pi0Merged_D0ToHH_DToHHH",
    # "BdToDst0DK_Dst0ToD0Pi0Merged_D0ToHHHH_DToHHH",
    # "BdToDst0DK_Dst0ToD0Pi0Merged_D0ToKsLLHH_DToHHH",
    # "BdToDst0DK_Dst0ToD0Pi0Merged_D0ToKsDDHH_DToHHH",
    "BdToDst0DK_Dst0ToD0Pi0Resolved_D0ToHH_DToHHH",
    "BdToDst0DK_Dst0ToD0Pi0Resolved_D0ToHHHH_DToHHH",
    "BdToDst0DK_Dst0ToD0Pi0Resolved_D0ToKsLLHH_DToHHH",
    "BdToDst0DK_Dst0ToD0Pi0Resolved_D0ToKsDDHH_DToHHH",
    # "BdToDst0DsPi_Dst0ToD0Pi0Merged_D0ToHH_DsToHHH",
    # "BdToDst0DsPi_Dst0ToD0Pi0Merged_D0ToHHHH_DsToHHH",
    # "BdToDst0DsPi_Dst0ToD0Pi0Merged_D0ToKsLLHH_DsToHHH",
    # "BdToDst0DsPi_Dst0ToD0Pi0Merged_D0ToKsDDHH_DsToHHH",
    "BdToDst0DsPi_Dst0ToD0Pi0Resolved_D0ToHH_DsToHHH",
    "BdToDst0DsPi_Dst0ToD0Pi0Resolved_D0ToHHHH_DsToHHH",
    "BdToDst0DsPi_Dst0ToD0Pi0Resolved_D0ToKsLLHH_DsToHHH",
    "BdToDst0DsPi_Dst0ToD0Pi0Resolved_D0ToKsDDHH_DsToHHH",
    "BdToDst0DsPi_Dst0ToD0Gamma_D0ToHH_DsToHHH",
    "BdToDst0DsPi_Dst0ToD0Gamma_D0ToHHHH_DsToHHH",
    "BdToDst0DsPi_Dst0ToD0Gamma_D0ToKsLLHH_DsToHHH",
    "BdToDst0DsPi_Dst0ToD0Gamma_D0ToKsDDHH_DsToHHH",
    # "BdToDspDsmPi0Merged_DsToKKPi",
    "BdToDspDsmPi0Resolved_DsToKKPi",
    # lines from b_to_ddh_standalone
    # lines from bbaryon_to_cbaryon_h
    #'OmbmToOmc0PiGammaWS_Omc0ToPKKPi',
    #'OmbmToOmc0PiGamma_Omc0ToPKKPi',
    #'OmbmToOmc0PiPi0ResolvedWS_Omc0ToPKKPi',
    #'OmbmToOmc0PiPi0Resolved_Omc0ToPKKPi',
    #'OmbmToXicpKPiGammaWS_XicpToPKPi',
    #'OmbmToXicpKPiGamma_XicpToPKPi',
    # lines from b_to_cbaryon_hh
    "BuToLcmPPi_LcmToPKPi",
    "BuToLcmPK_LcmToPKPi",
    "BdToOmc0PPi_Omc0ToPKKPi",
    "BdToOmc0PK_Omc0ToPKKPi",
    # lines from bbaryon_to_cbaryon_hh
    "XibmToLcpPiPi_LcpToPKPi",
    "XibmToLcpKPi_LcpToPKPi",
    "XibmToLcpKK_LcpToPKPi",
    "XibmToXicpPiPi_XicpToPKPi",
    "XibmToXicpKPi_XicpToPKPi",
    "XibmToXicpKK_XicpToPKPi",
    "Xib0ToXicpPiPiPi_XicpToPKPi",
    "LbToLcpKsLLK_LcpToPKPi",
    "LbToLcpKsDDK_LcpToPKPi",
    "LbToLcpPbarLambdaLL_LcpToPKPi",
    "LbToLcpPbarLambdaDD_LcpToPKPi",
    # lines from bbaryon_to_cbaryon_hhh
    "Xib0ToXicpPbarPPi_XicpToPKPi",
    "Xib0ToXicpKPiPi_XicpToPKPi",
    "Xib0ToXicpKKPi_XicpToPKPi",
    "LbToLcpPiPiPi_LcpToPPiPi",
    "LbToLcpPiPiPi_LcpToPKPi",
    "LbToLcpPbarPPi_LcpToPKPi",
    "LbToLcpKKPi_LcpToPKPi",
    # lines from bbaryon_to_cbaryon_dh
    "Xib0ToXicpD0K_XicpToPKPi_D0ToKPi",
    "Xib0ToXicpD0K_XicpToPKPi_D0ToKPiPiPi",
    "LbToLcpD0Pi_LcpToPKPi_D0ToKPiOrKPiPiPi",
    "LbToLcpD0K_LcpToPKPi_D0ToKPiOrKPiPiPi",
    # lines from bbaryon_to_cbaryon_dhh
    "LbToLcpDstmPipKm_LcpToPKPi_DstmToD0Pi",
    "LbToLcpDmPipKm_LcpToPKPi_DmToKPiPi",
    # lines from bbaryon_to_lightbaryon_d
    "LbToDsmP_DsmToHHH",
    # lines from bbaryon_to_lightbaryon_dh
    "XibmToDmPK_DmToHHH",
    "XibmToDmPPi_DmToHHH",
    "XibmToDsmPK_DsmToHHH",
    "XibmToDsmPPi_DsmToHHH",
    "LbToD0PPi_D0ToKK",
    "LbToD0PPi_D0ToKPi",
    "LbToD0PPi_D0ToPiPi",
    "LbToD0PK_D0ToKK",
    "LbToD0PK_D0ToKPi",
    "LbToD0PK_D0ToPiPi",
    # lines from bbaryon_to_lightbaryon_dd
    "LbToDpDmLambdaLL_DpToHHH",
    "LbToDpDmLambdaDD_DpToHHH",
    "LbToD0D0LambdaLL_D0ToHH",
    "LbToD0D0LambdaLL_D0ToKsLLHH",
    "LbToD0D0LambdaLL_D0ToKsDDHH",
    "LbToD0D0LambdaLL_D0ToHHHH",
    "LbToD0D0LambdaLL_D0ToHH_D0ToKsLLHH",
    "LbToD0D0LambdaLL_D0ToHH_D0ToKsDDHH",
    "LbToD0D0LambdaLL_D0ToHH_D0ToHHHH",
    "LbToD0D0LambdaLL_D0ToKsLLHH_D0ToKsDDHH",
    "LbToD0D0LambdaLL_D0ToKsLLHH_D0ToHHHH",
    "LbToD0D0LambdaLL_D0ToKsDDHH_D0ToHHHH",
    "LbToD0D0LambdaDD_D0ToHH",
    "LbToD0D0LambdaDD_D0ToKsLLHH",
    "LbToD0D0LambdaDD_D0ToKsDDHH",
    "LbToD0D0LambdaDD_D0ToHHHH",
    "LbToD0D0LambdaDD_D0ToHH_D0ToKsLLHH",
    "LbToD0D0LambdaDD_D0ToHH_D0ToKsDDHH",
    "LbToD0D0LambdaDD_D0ToHH_D0ToHHHH",
    "LbToD0D0LambdaDD_D0ToKsLLHH_D0ToKsDDHH",
    "LbToD0D0LambdaDD_D0ToKsLLHH_D0ToHHHH",
    "LbToD0D0LambdaDD_D0ToKsDDHH_D0ToHHHH",
    "LbToDstDLambdaLL_DstToD0Pi_D0ToHH_DToHHH",
    "LbToDstDLambdaDD_DstToD0Pi_D0ToHH_DToHHH",
    "LbToDstDLambdaLL_DstToD0Pi_D0ToKPiPiPi_DToHHH",
    "LbToDstDLambdaDD_DstToD0Pi_D0ToKPiPiPi_DToHHH",
    # lines from b_to_cbaryons
    "BdToLcpLcm_LcpToPHH",
    "BdToXicpXicm_XicpToPKPi",
    # lines from b_to_cbaryons_h
    "BuToLcpLcmK_LcpToPKPi",
]

# default lines will be booked with:
#     prescale = 1,
#     no flavour tagging
# add extra configurations in the following dictionary
extra_config = {
    "prescale": {
        # lines from b_to_dh
        "BuToD0Pi_PartialD0ToKsLLHHWS": 0.1,
        "BuToD0Pi_PartialD0ToKsDDHHWS": 0.1,
        "BuToD0K_PartialD0ToKsLLHHWS": 0.1,
        "BuToD0K_PartialD0ToKsDDHHWS": 0.1,
        #'BuToD0Pi_D0ToKsLLHHWSPi0Resolved': 0.1,
        #'BuToD0Pi_D0ToKsDDHHWSPi0Resolved': 0.1,
        #'BuToD0Pi_D0ToKsLLHHWSPi0Merged': 0.1,
        #'BuToD0Pi_D0ToKsDDHHWSPi0Merged': 0.1,
        #'BuToD0K_D0ToKsLLHHWSPi0Resolved': 0.1,
        #'BuToD0K_D0ToKsDDHHWSPi0Resolved': 0.1,
        #'BuToD0K_D0ToKsLLHHWSPi0Merged': 0.1,
        #'BuToD0K_D0ToKsDDHHWSPi0Merged': 0.1,
        #'BuToDst0Pi_Dst0ToD0Gamma_D0ToHHWS': 0.1,
        #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHHWS': 0.1,
        #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToHHWS': 0.1,
        #'BuToDst0K_Dst0ToD0Gamma_D0ToHHWS': 0.1,
        #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHHWS': 0.1,
        #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToHHWS': 0.1,
        #'BuToDst0Pi_Dst0ToD0Gamma_D0ToKsLLHHWS': 0.1,
        #'BuToDst0Pi_Dst0ToD0Gamma_D0ToKsDDHHWS': 0.1,
        #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToKsLLHHWS': 0.1,
        #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToKsDDHHWS': 0.1,
        #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsLLHHWS': 0.1,
        #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsDDHHWS': 0.1,
        #'BuToDst0K_Dst0ToD0Gamma_D0ToKsLLHHWS': 0.1,
        #'BuToDst0K_Dst0ToD0Gamma_D0ToKsDDHHWS': 0.1,
        #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToKsLLHHWS': 0.1,
        #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToKsDDHHWS': 0.1,
        #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToKsLLHHWS': 0.1,
        #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToKsDDHHWS': 0.1,
        #'BuToDst0Pi_Dst0ToD0Gamma_D0ToHHHHWS': 0.1,
        #'BuToDst0Pi_Dst0ToD0Pi0Resolved_D0ToHHHHWS': 0.1,
        #'BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToHHHHWS': 0.1,
        #'BuToDst0K_Dst0ToD0Gamma_D0ToHHHHWS': 0.1,
        #'BuToDst0K_Dst0ToD0Pi0Resolved_D0ToHHHHWS': 0.1,
        #'BuToDst0K_Dst0ToD0Pi0Merged_D0ToHHHHWS': 0.1,
        "BcToD0Pi_D0ToHHWS": 0.1,
        "BcToD0K_D0ToHHWS": 0.1,
        "BcToD0Pi_D0ToKsLLHHWS": 0.1,
        "BcToD0Pi_D0ToKsDDHHWS": 0.1,
        "BcToD0K_D0ToKsLLHHWS": 0.1,
        "BcToD0K_D0ToKsDDHHWS": 0.1,
        "BcToD0Pi_D0ToHHHHWS": 0.1,
        "BcToD0K_D0ToHHHHWS": 0.1,
        "BcToDmKst0_DmToHHHWS": 0.1,
        "BcToDsmKst0_DsmToHHHWS": 0.1,
        "BcToDmPhi_DmToHHH": 0.2,
        "BcToDsmPhi_DsmToHHH": 0.2,
        # lines from b_to_dx_ltu
        "BdToDsmK_DsmToKpKmPim_LTU": 0.1,
        # lines from b_to_dll
        "BcToDsmMumMumWS_DsmToHHH": 0.1,
        # lines from b_to_dhh
        "BuToD0KsLLPi_D0ToHHWS": 0.1,
        "BuToD0KsDDPi_D0ToHHWS": 0.1,
        "BuToD0KsLLPi_D0ToKsLLHHWS": 0.1,
        "BuToD0KsDDPi_D0ToKsLLHHWS": 0.1,
        "BuToD0KsLLPi_D0ToKsDDHHWS": 0.1,
        "BuToD0KsDDPi_D0ToKsDDHHWS": 0.1,
        "BuToD0KsLLPi_D0ToHHHHWS": 0.1,
        "BuToD0KsDDPi_D0ToHHHHWS": 0.1,
        #'BuToD0PiPi0Resolved_D0ToHH': 0.1,
        #'BuToD0PiPi0Merged_D0ToHH': 0.1,
        #'BuToD0KPi0Resolved_D0ToHH': 0.1,
        #'BuToD0KPi0Merged_D0ToHH': 0.1,
        #'BuToD0PiPi0Resolved_D0ToHHWS': 0.1,
        #'BuToD0PiPi0Merged_D0ToHHWS': 0.1,
        #'BuToD0KPi0Resolved_D0ToHHWS': 0.1,
        #'BuToD0KPi0Merged_D0ToHHWS': 0.1,
        #'BuToD0PiPi0Resolved_D0ToKsLLHH': 0.1,
        #'BuToD0PiPi0Resolved_D0ToKsDDHH': 0.1,
        #'BuToD0PiPi0Merged_D0ToKsLLHH': 0.1,
        #'BuToD0PiPi0Merged_D0ToKsDDHH': 0.1,
        #'BuToD0KPi0Resolved_D0ToKsLLHH': 0.1,
        #'BuToD0KPi0Resolved_D0ToKsDDHH': 0.1,
        #'BuToD0KPi0Merged_D0ToKsLLHH': 0.1,
        #'BuToD0KPi0Merged_D0ToKsDDHH': 0.1,
        #'BuToD0PiPi0Resolved_D0ToKsLLHHWS': 0.1,
        #'BuToD0PiPi0Resolved_D0ToKsDDHHWS': 0.1,
        #'BuToD0PiPi0Merged_D0ToKsLLHHWS': 0.1,
        #'BuToD0PiPi0Merged_D0ToKsDDHHWS': 0.1,
        #'BuToD0KPi0Resolved_D0ToKsLLHHWS': 0.1,
        #'BuToD0KPi0Resolved_D0ToKsDDHHWS': 0.1,
        #'BuToD0KPi0Merged_D0ToKsLLHHWS': 0.1,
        #'BuToD0KPi0Merged_D0ToKsDDHHWS': 0.1,
        #'BuToD0PiPi0Resolved_D0ToHHHH': 0.1,
        #'BuToD0PiPi0Merged_D0ToHHHH': 0.1,
        #'BuToD0KPi0Resolved_D0ToHHHH': 0.1,
        #'BuToD0KPi0Merged_D0ToHHHH': 0.1,
        #'BuToD0PiPi0Resolved_D0ToHHHHWS': 0.1,
        #'BuToD0PiPi0Merged_D0ToHHHHWS': 0.1,
        #'BuToD0KPi0Resolved_D0ToHHHHWS': 0.1,
        #'BuToD0KPi0Merged_D0ToHHHHWS': 0.1,
        # lines from b_to_dhhh
        "BuToD0PiPiPi_D0ToHHWS": 0.1,
        "BuToD0KPiPi_D0ToHHWS": 0.1,
        "BuToD0PiPiPi_D0ToHHHHWS": 0.1,
        "BuToD0KPiPi_D0ToHHHHWS": 0.1,
        # lines of beauty -> charm + charged hadron(s) + neutral decays
        # prescale for now. When photon CL and AM12 get available we will further optimize the selection
        #'OmbmToOmc0PiGammaWS_Omc0ToPKKPi': 0.5,
        #'OmbmToOmc0PiGamma_Omc0ToPKKPi': 0.5,
        #'OmbmToOmc0PiPi0ResolvedWS_Omc0ToPKKPi': 0.1,
        #'OmbmToOmc0PiPi0Resolved_Omc0ToPKKPi': 0.1,
        #'OmbmToXicpKPiGammaWS_XicpToPKPi': 0.5,
        #'OmbmToXicpKPiGamma_XicpToPKPi': 0.5,
    },
    "mva": {},
    "flavour_tagging": [
        # lines from b_to_dh
        #'BdToDsstmPi_DsstmToDsmGamma_DsmToHHH',
        #'BdToDsstmK_DsstmToDsmGamma_DsmToHHH',
        "BdToDmPi_DmToPimPimKp",
        # lines from b_to_dhh
        #'BdToD0KK_D0ToHH',
        "BdToD0KK_D0ToHHHH",
        "BdToD0PiPi_D0ToHHHH",
        #'BdToDst0KK_Dst0ToD0Gamma_D0ToHH',
        #'BdToDst0KK_Dst0ToD0Pi0Resolved_D0ToHH',
        #'BdToDst0PiPi_Dst0ToD0Gamma_D0ToHH',
        #'BdToDst0PiPi_Dst0ToD0Pi0Resolved_D0ToHH',
        "BdToDmKsDDPi_DmToHHH",
        "BdToDsmKsLLPi_DsmToHHH",
        "BdToDsmKsDDPi_DsmToHHH",
        # lines from b_to_dhhh
        "BdToDsmKKPi_DsmToKmKpPim",
        "BdToDsmKPiPi_DsmToKmKpPim",
        "BdToDsmPiPiPi_DsmToKmKpPim",
        "BdToDmKPiPi_DmToPimPimKp",
        "BdToDmPiPiPi_DmToPimPimKp",
        # lines from b_to_dd
        "BdToD0D0_D0ToHH",
        "BdToD0D0_D0ToHH_D0ToHHHH",
        "BdToD0D0_D0ToHHHH",
        "BdToD0D0_D0ToHH_D0ToKsLLHH",
        "BdToD0D0_D0ToHH_D0ToKsDDHH",
        "BdToD0D0_D0ToKsLLHH",
        "BdToD0D0_D0ToKsLLHH_D0ToKsDDHH",
        "BdToD0D0_D0ToKsDDHH",
        "BdToD0D0_D0ToKsLLHH_D0ToHHHH",
        "BdToD0D0_D0ToKsDDHH_D0ToHHHH",
        "BdToDpDm_DpToHHH",
        "BdToDspDm_DspToHHH_DmToHHH",
        "BdToDspDsm_DspToHHH",
        "BdToDstpDm_DstpToD0Pi_D0ToHHHH_DmToHHH",
        "BdToDstpDm_DstpToD0Pi_D0ToHH_DmToHHH",
        "BdToDstpDsm_DstpToD0Pi_D0ToHHHH_DsmToHHH",
        "BdToDstpDsm_DstpToD0Pi_D0ToHH_DsmToHHH",
        "BdToDstpDstm_DstpToD0Pi_D0ToHH",
        "BdToDstpDstm_DstpToD0Pi_D0ToHHHH",
        "BdToDstpDstm_DstpToD0Pi_D0ToHH_D0ToHHHH",
        # lines from b_to_ddh_standalone
        "BdToDsstmPi_DsstmToDsmGamma_DsmToHHH",
        "BdToDmKsLLPi_DmToHHH",
    ],
    "pv_unbiasing": [
        # PV-unbiasing is automatically added for all FT lines,
        # book line here if FT is not required but PV-unbiasing is required.
        "BdToDsmK_DsmToKpKmPim_LTU",
        "BdToDmPiPiPi_DmToPimPimKp",
        "BdToDsmPiPiPi_DsmToKmKpPim",
        "BuToD0PiPiPi_D0ToHH",
        "XibmToLcpKK_LcpToPKPi",
        "XibmToLcpKPi_LcpToPKPi",
        "XibmToLcpPiPi_LcpToPKPi",
        "XibmToXicpKPi_XicpToPKPi",
        "LbToLcpPiPiPi_LcpToPKPi",
        "Xib0ToXicpPiPiPi_XicpToPKPi",
    ],
    "isolation": [
        "BuToD0Pi_D0ToHH",
        "BuToD0K_D0ToHH",
        "BuToD0Pi_D0ToKsLLHH",
        "BuToD0Pi_D0ToKsDDHH",
        "BuToD0K_D0ToKsLLHH",
        "BuToD0K_D0ToKsDDHH",
        "BuToD0Pi_D0ToHHHH",
        "BuToD0K_D0ToHHHH",
        "BuToD0K_PartialD0ToKsDDHH",
        "BuToD0K_PartialD0ToKsDDHHWS",
        "BuToD0K_PartialD0ToKsLLHH",
        "BuToD0K_PartialD0ToKsLLHHWS",
        "BuToD0Pi_PartialD0ToKsDDHH",
        "BuToD0Pi_PartialD0ToKsDDHHWS",
        "BuToD0Pi_PartialD0ToKsLLHH",
        "BuToD0Pi_PartialD0ToKsLLHHWS",
        "BuToDst0K_Dst0ToD0Gamma_D0ToKsDDHH",
        "BuToDst0K_Dst0ToD0Gamma_D0ToKsLLHH",
        "BuToDst0K_Dst0ToD0Pi0Merged_D0ToKsDDHH",
        "BuToDst0K_Dst0ToD0Pi0Merged_D0ToKsLLHH",
        "BuToDst0K_Dst0ToD0Pi0Resolved_D0ToKsDDHH",
        "BuToDst0K_Dst0ToD0Pi0Resolved_D0ToKsLLHH",
        "BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsDDHH",
        "BuToDst0Pi_Dst0ToD0Pi0Merged_D0ToKsLLHH",
        "BuToD0KsDDPi_D0ToHH",
        "BuToD0KsDDPi_D0ToHHHH",
        "BuToD0KsDDPi_D0ToHHHHWS",
        "BuToD0KsDDPi_D0ToHHWS",
        "BuToD0KsDDPi_D0ToKsDDHH",
        "BuToD0KsDDPi_D0ToKsDDHHWS",
        "BuToD0KsDDPi_D0ToKsLLHH",
        "BuToD0KsDDPi_D0ToKsLLHHWS",
        "BuToD0KsLLPi_D0ToHH",
        "BuToD0KsLLPi_D0ToHHHH",
        "BuToD0KsLLPi_D0ToHHHHWS",
        "BuToD0KsLLPi_D0ToHHWS",
        "BuToD0KsLLPi_D0ToKsDDHH",
        "BuToD0KsLLPi_D0ToKsDDHHWS",
        "BuToD0KsLLPi_D0ToKsLLHH",
        "BuToD0KsLLPi_D0ToKsLLHHWS",
        "BuToD0KPiPi_D0ToHHHH",
        "BuToD0KPiPi_D0ToHHHHWS",
        "BuToD0KPiPi_D0ToHHWS",
        "BuToD0PiPiPi_D0ToHHHH",
        "BuToD0PiPiPi_D0ToHHHHWS",
        "BuToD0PiPiPi_D0ToHHWS",
    ],
}

####################################
# Register lines in sprucing_lines #
# with generic functions           #
####################################
validate_config(all_lines, extra_config)

make_sprucing_lines(
    line_dict=sprucing_lines,
    line_makers=line_makers,
    all_lines=all_lines,
    extra_config=extra_config,
)

####################################
# Register lines in sprucing_lines #
# with unique functions            #
# for lines have special purpose   #
####################################

# test spruce line for FEST
# A Monitor for the b mass is added for development of SprucingDQ
# To save the histogram in a root file the line
# histo_file : 'my_histograms.root'
# has to be added to the lbexec yaml file


@register_line_builder(sprucing_lines)
def BdToDsmK_DsmToHHH_sprucing_FEST_line(
    name="SpruceB2OC_BdToDsmK_DsmToHHH_FEST", prescale=1
):
    line_alg = b_to_dh.make_BdToDsmK_DsmToHHH_FEST(process=PROCESS)
    b_mon = monitor(
        name="Test_B_Monitor",
        data=line_alg,
        histograms=[
            histogram_1d("m", "m", F.MASS, 50, (5000 * MeV, 7000 * MeV), "MASS")
        ],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=prefilters.b2oc_prefilters() + [b_mon, line_alg],
    )


@register_line_builder(sprucing_lines)
def BdToDsmPi_DsmToKpKmPim_Calib(name="SpruceCalib_BdToDsmPi_DsmToKpKmPim", prescale=1):
    line_alg = b_to_dh.make_BdToDsmPi_DsmToKpKmPim_Calib(process=PROCESS, MVACut=0.75)
    return SpruceLine(
        name=name,
        hlt2_filter_code=["Hlt2Calib_BdToDsmPi_DsmToKpKmPimDecision"],
        prescale=prescale,
        persistreco=True,
        raw_banks=DETECTORS,
        algs=prefilters.b2oc_prefilters() + [line_alg],
    )


@register_line_builder(sprucing_lines)
def BdToDmPi_DmToPimPimKp_Calib(name="SpruceCalib_BdToDmPi_DmToPimPimKp", prescale=1):
    line_alg = b_to_dh.make_BdToDmPi_DmToPimPimKp_Calib(process=PROCESS, MVACut=0.75)
    return SpruceLine(
        name=name,
        hlt2_filter_code=["Hlt2Calib_BdToDmPi_DmToPimPimKpDecision"],
        prescale=prescale,
        persistreco=True,
        raw_banks=DETECTORS,
        algs=prefilters.b2oc_prefilters() + [line_alg],
    )
