###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of B2OC selections for the Dll topology

Non trivial imports:
basic builders for D mesons and h, prefilters, BDT post filter

Returns:
every function returns a line_alg

"""

from GaudiKernel.SystemOfUnits import MeV

from Hlt2Conf.lines.b_to_open_charm.builders import b_builder, basic_builder, d_builder
from Hlt2Conf.lines.b_to_open_charm.utils import check_process


@check_process
def make_BcToDsmMupMum_DsmToHHH(process):
    muon = basic_builder.make_muons()
    d = d_builder.make_dsplus_to_hhh(
        pi_pidk_max=20, k_pidk_min=-10, am_min=1868.35 * MeV, am_max=2068.35 * MeV
    )
    line_alg = b_builder.make_bc2x(
        particles=[d, muon, muon],
        descriptors=["[B_c- -> D_s- mu+ mu-]cc"],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV,
    )
    return line_alg


@check_process
def make_BcToDspMumMum_DspToHHH(process):
    muon = basic_builder.make_muons()
    d = d_builder.make_dsplus_to_hhh(
        pi_pidk_max=20, k_pidk_min=-10, am_min=1868.35 * MeV, am_max=2068.35 * MeV
    )
    line_alg = b_builder.make_bc2x(
        particles=[d, muon, muon],
        descriptors=["[B_c- -> D_s+ mu- mu-]cc"],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV,
    )
    return line_alg


@check_process
def make_BcToDsmMumMumWS_DsmToHHH(process):
    muon = basic_builder.make_muons()
    d = d_builder.make_dsplus_to_hhh(
        pi_pidk_max=20, k_pidk_min=-10, am_min=1868.35 * MeV, am_max=2068.35 * MeV
    )
    line_alg = b_builder.make_bc2x(
        particles=[d, muon, muon],
        descriptors=["[B_c- -> D_s- mu- mu-]cc"],
        am_min=5800 * MeV,
        am_max=6700 * MeV,
        am_min_vtx=5800 * MeV,
        am_max_vtx=6700 * MeV,
    )
    return line_alg
