###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""

from Hlt2Conf.lines.b_to_open_charm.builders import b_builder, cbaryon_builder
from Hlt2Conf.lines.b_to_open_charm.utils import check_process


##############################################################
# Form the B(s)0 --> Lc+ Lc-, Lc+ --> p h- h+, Lc- -->pbar h+ h-
# contact peilian@cern.ch
##############################################################
@check_process
def make_BdToLcpLcm_LcpToPHH(process):
    lc = cbaryon_builder.make_tight_lc_to_phh()
    line_alg = b_builder.make_b2x(
        particles=[lc, lc], descriptors=["B0 -> Lambda_c+ Lambda_c~-"]
    )
    return line_alg


##############################################################
# Form the B(s)0 --> Xic+ Xic-, Xic+ --> p K- pi+, Xic- -->pbar K+ pi-
# contact peilian@cern.ch
##############################################################
@check_process
def make_BdToXicpXicm_XicpToPKPi(process):
    xicp = cbaryon_builder.make_tight_xicp_to_pkpi()
    line_alg = b_builder.make_b2x(
        particles=[xicp, xicp], descriptors=["B0 -> Xi_c+ Xi_c~-"]
    )
    return line_alg
