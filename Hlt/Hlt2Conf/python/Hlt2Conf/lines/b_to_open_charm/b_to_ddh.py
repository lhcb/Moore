###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC B2DDh lines
"""
# from RecoConf.algorithms_thor import ParticleContainersMerger

from GaudiKernel.SystemOfUnits import GeV, mm

from Hlt2Conf.lines.b_to_open_charm.builders import b_builder, basic_builder, d_builder
from Hlt2Conf.lines.b_to_open_charm.filters import b_sigmanet_filter
from Hlt2Conf.lines.b_to_open_charm.utils import check_process

#############################################################################
# Form the B0 -> D0 D0 K*(892)0, D0 --> {hh, Kshh, hhhh}
##############################################################################

# Split of previous BdToD0D0_D0ToHHOrHHHH line into three lines


@check_process
def make_BdToD0D0Kst_D0ToHH(process, MVACut=0.5):  # 2x2-body D0 decay modes (isolated)
    kst = basic_builder.make_kstar0(am_min=600, am_max=1600, k_pidk_min=-2)
    dzero_hh = d_builder.make_dzero_to_hh(k_pidk_min=-2)
    b = b_builder.make_b2cch(
        particles=[dzero_hh, dzero_hh, kst],
        descriptors=["B0 -> D0 D0 K*(892)0", "B0 -> D0 D0 K*(892)~0"],
    )
    line_alg = b_sigmanet_filter(b, MVACut)

    return line_alg


@check_process
def make_BdToD0D0Kst_D0ToHH_D0ToHHHH(
    process, MVACut=0.5
):  # 2x4-body D0 decay modes (isolated)
    kst = basic_builder.make_kstar0(am_min=600, am_max=1600, k_pidk_min=-2)
    dzero_hh = d_builder.make_dzero_to_hh(k_pidk_min=-2)
    dzero_hhhh = d_builder.make_dzero_to_hhhh(k_pidk_min=-2)
    b = b_builder.make_b2cch(
        particles=[dzero_hh, dzero_hhhh, kst],
        descriptors=["B0 -> D0 D0 K*(892)0", "B0 -> D0 D0 K*(892)~0"],
        AllowDiffInputsForSameIDChildren=True,
    )
    line_alg = b_sigmanet_filter(b, MVACut)

    return line_alg


@check_process
def make_BdToD0D0Kst_D0ToHHHH(
    process, MVACut=0.5
):  # 4x4-body D0 decay modes (isolated)
    kst = basic_builder.make_kstar0(am_min=600, am_max=1600, k_pidk_min=-2)
    dzero_hhhh = d_builder.make_dzero_to_hhhh(k_pidk_min=-2)
    b = b_builder.make_b2cch(
        particles=[dzero_hhhh, dzero_hhhh, kst],
        descriptors=["B0 -> D0 D0 K*(892)0", "B0 -> D0 D0 K*(892)~0"],
    )
    line_alg = b_sigmanet_filter(b, MVACut)

    return line_alg


# Additional D decays modes added by Paras Naik


@check_process
def make_BdToD0D0Kst_D0ToHH_D0ToKsLLHH(
    process, MVACut=0.5
):  # 2x3(LL)-body D0 decay modes
    kst = basic_builder.make_kstar0(am_min=600, am_max=1600, k_pidk_min=-2)
    dzero_hh = d_builder.make_dzero_to_hh(k_pidk_min=-2)
    dzero_KshhLL = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
    )
    b = b_builder.make_b2cch(
        particles=[dzero_hh, dzero_KshhLL, kst],
        descriptors=["B0 -> D0 D0 K*(892)0", "B0 -> D0 D0 K*(892)~0"],
        AllowDiffInputsForSameIDChildren=True,
    )
    line_alg = b_sigmanet_filter(b, MVACut)

    return line_alg


@check_process
def make_BdToD0D0Kst_D0ToHH_D0ToKsDDHH(
    process, MVACut=0.5
):  # 2x3(DD)-body D0 decay modes
    kst = basic_builder.make_kstar0(am_min=600, am_max=1600, k_pidk_min=-2)
    dzero_hh = d_builder.make_dzero_to_hh(k_pidk_min=-2)
    dzero_KshhDD = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
    )
    b = b_builder.make_b2cch(
        particles=[dzero_hh, dzero_KshhDD, kst],
        descriptors=["B0 -> D0 D0 K*(892)0", "B0 -> D0 D0 K*(892)~0"],
        AllowDiffInputsForSameIDChildren=True,
    )
    line_alg = b_sigmanet_filter(b, MVACut)

    return line_alg


@check_process
def make_BdToD0D0Kst_D0ToKsLLHH(process, MVACut=0.5):  # 3(LL)x3(LL)-body D0 decay modes
    kst = basic_builder.make_kstar0(am_min=600, am_max=1600, k_pidk_min=-2)
    dzero_KshhLL = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
    )
    b = b_builder.make_b2cch(
        particles=[dzero_KshhLL, dzero_KshhLL, kst],
        descriptors=["B0 -> D0 D0 K*(892)0", "B0 -> D0 D0 K*(892)~0"],
    )
    line_alg = b_sigmanet_filter(b, MVACut)

    return line_alg


@check_process
def make_BdToD0D0Kst_D0ToKsLLHH_D0ToKsDDHH(
    process, MVACut=0.5
):  # 3(LL)x3(DD)-body D0 decay modes
    kst = basic_builder.make_kstar0(am_min=600, am_max=1600, k_pidk_min=-2)
    dzero_KshhLL = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
    )
    dzero_KshhDD = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
    )
    b = b_builder.make_b2cch(
        particles=[dzero_KshhLL, dzero_KshhDD, kst],
        descriptors=["B0 -> D0 D0 K*(892)0", "B0 -> D0 D0 K*(892)~0"],
        AllowDiffInputsForSameIDChildren=True,
    )
    line_alg = b_sigmanet_filter(b, MVACut)

    return line_alg


@check_process
def make_BdToD0D0Kst_D0ToKsDDHH(process, MVACut=0.5):  # 3(DD)x3(DD)-body D0 decay modes
    kst = basic_builder.make_kstar0(am_min=600, am_max=1600, k_pidk_min=-2)
    dzero_KshhDD = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
    )
    b = b_builder.make_b2cch(
        particles=[dzero_KshhDD, dzero_KshhDD, kst],
        descriptors=["B0 -> D0 D0 K*(892)0", "B0 -> D0 D0 K*(892)~0"],
    )
    line_alg = b_sigmanet_filter(b, MVACut)

    return line_alg


@check_process
def make_BdToD0D0Kst_D0ToKsLLHH_D0ToHHHH(
    process, MVACut=0.5
):  # 3(LL)x4-body D0 decay modes
    kst = basic_builder.make_kstar0(am_min=600, am_max=1600, k_pidk_min=-2)
    dzero_KshhLL = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
    )
    dzero_hhhh = d_builder.make_dzero_to_hhhh(k_pidk_min=-2)
    b = b_builder.make_b2cch(
        particles=[dzero_KshhLL, dzero_hhhh, kst],
        descriptors=["B0 -> D0 D0 K*(892)0", "B0 -> D0 D0 K*(892)~0"],
        AllowDiffInputsForSameIDChildren=True,
    )
    line_alg = b_sigmanet_filter(b, MVACut)

    return line_alg


@check_process
def make_BdToD0D0Kst_D0ToKsDDHH_D0ToHHHH(
    process, MVACut=0.5
):  # 3(DD)x4-body D0 decay modes
    kst = basic_builder.make_kstar0(am_min=600, am_max=1600, k_pidk_min=-2)
    dzero_KshhDD = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
    )
    dzero_hhhh = d_builder.make_dzero_to_hhhh(k_pidk_min=-2)
    b = b_builder.make_b2cch(
        particles=[dzero_KshhDD, dzero_hhhh, kst],
        descriptors=["B0 -> D0 D0 K*(892)0", "B0 -> D0 D0 K*(892)~0"],
        AllowDiffInputsForSameIDChildren=True,
    )
    line_alg = b_sigmanet_filter(b, MVACut)

    return line_alg


#############################################################################
# Form the B0 -> D*(2010)- D0 K+, D*+ --> D0pi+ (D0 --> hh), D0 --> hh
##############################################################################


@check_process
def make_BdToDstD0K_DstToD0Pi_D0ToHH_D0ToHH(process):
    kaon = basic_builder.make_tight_kaons()
    dzero = d_builder.make_dzero_to_hh()
    dst = d_builder.make_dstar_to_dzeropi(dzero)
    line_alg = b_builder.make_b2x(
        particles=[dst, dzero, kaon],
        descriptors=["B0 -> D*(2010)- D0 K+", "B0 -> D*(2010)+ D0 K-"],
    )
    return line_alg


#############################################################################
# Form the Tbc -> D0 D+ pi-, D0 --> Kpi & K3pi, D+ --> Kpipi
##############################################################################


@check_process
def make_TbcToD0DpPim_D0ToKPiOrKPiPiPi(process):
    Dz = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
    Dp = d_builder.make_tight_dplus_to_kmpippip_for_xibc()
    pion = basic_builder.make_tightpid_tight_pions()
    line_alg = b_builder.make_tbc2ccx(
        particles=[Dz, Dp, pion],
        descriptors=["Xi_bc0 -> D0 D+ pi-", "Xi_bc~0 -> D0 D- pi+"],
    )
    return line_alg


######################################################################
######################################################################
@check_process
def make_BuToD0Ds2460p_Ds2460pToDsPiPi_DsToKHH_D0ToKPi(process, MVACut=0.2):
    dz = d_builder.make_dzero_to_kpi()
    ds = d_builder.make_dsplus_to_hhh(
        am_min=1868.35,
        am_max=2068.35,
        k_pidk_min=0,
        DsToKKPi=True,
        DsToKPiPi=True,
        DsToPiPiPi=False,
    )
    ds1 = d_builder.make_ds2460_to_dspipi(ds)

    line_alg = b_builder.make_b2x(
        particles=[dz, ds1],
        descriptors=["B+ -> D0 D_s1(2460)+", "B- -> D0 D_s1(2460)-"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


@check_process
def make_BuToD0Ds2460p_Ds2460pToDsPiPi_DsToKHH_D0ToKPiPiPi(process, MVACut=0.2):
    dz = d_builder.make_dzero_to_kpipipi()
    ds = d_builder.make_dsplus_to_hhh(
        am_min=1868.35,
        am_max=2068.35,
        k_pidk_min=0,
        DsToKKPi=True,
        DsToKPiPi=True,
        DsToPiPiPi=False,
    )
    ds1 = d_builder.make_ds2460_to_dspipi(ds)

    line_alg = b_builder.make_b2x(
        particles=[dz, ds1],
        descriptors=["B+ -> D0 D_s1(2460)+", "B- -> D0 D_s1(2460)-"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


@check_process
def make_BdToDmDs2460p_Ds2460pToDsPiPi_DsToKHH_DmToHHH(process, MVACut=0.2):
    dm = d_builder.make_dplus_to_kpipi_or_kkpi(
        am_min=1769.66,
        am_max=1969.66,
    )
    ds = d_builder.make_dsplus_to_hhh(
        am_min=1868.35,
        am_max=2068.35,
        k_pidk_min=0,
        DsToKKPi=True,
        DsToKPiPi=True,
        DsToPiPiPi=False,
    )
    ds1 = d_builder.make_ds2460_to_dspipi(ds)

    line_alg = b_builder.make_b2x(
        particles=[dm, ds1],
        descriptors=["B0 -> D- D_s1(2460)+", "B0 -> D+ D_s1(2460)-"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


@check_process
def make_BdToDstmDs2460p_DstmToD0Pi_Ds2460pToDsPiPi_DsToKHH(process, MVACut=0.2):
    dstm = d_builder.make_dstar_to_dzeropi_cf(
        am_min=1764.84,
        am_max=1964.84,
    )
    ds = d_builder.make_dsplus_to_hhh(
        am_min=1868.35,
        am_max=2068.35,
        k_pidk_min=0,
        DsToKKPi=True,
        DsToKPiPi=True,
        DsToPiPiPi=False,
    )
    ds1 = d_builder.make_ds2460_to_dspipi(ds)

    line_alg = b_builder.make_b2x(
        particles=[dstm, ds1],
        descriptors=["B0 -> D*(2010)- D_s1(2460)+", "B0 -> D*(2010)+ D_s1(2460)-"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


@check_process
def make_BdToDst0DsPi_Dst0ToD0Pi0Merged_D0ToKPiOrKPiPiPi_DsToHHH(process):
    if process == "spruce":
        dz = d_builder.make_dzero_to_kpi_or_kpipipi(pi_pidk_max=None, k_pidk_min=None)
        ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
    elif process == "hlt2":
        dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        ds = d_builder.make_dsplus_to_hhh()
        pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, ds, pion],
        descriptors=["B0 -> D*(2007)0 D_s+ pi-", "B0 -> D*(2007)0 D_s- pi+"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


#############################################################################
# Moved from b_to_ddh_standalone because these lines do not require make_b2ddh()
##############################################################################

# BuToD0D0K lines


@check_process
def make_BuToD0D0K_D0ToHH(process, MVACut=0.2):  # 2x2-body
    dz_hh = d_builder.make_dzero_to_hh()
    dzb_hh = d_builder.make_dzero_to_hh()
    kaon = basic_builder.make_tight_kaons()

    line_alg = b_builder.make_b2x(
        particles=[dz_hh, dzb_hh, kaon],
        descriptors=["B+ -> D0 D0 K+", "B- -> D0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToD0D0K_D0ToKsLLHH(process, MVACut=0.2):  # 3(LL)x3(LL)-body
    dz_KsLLhh = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
    )
    dzb_KsLLhh = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
    )
    kaon = basic_builder.make_tight_kaons()

    line_alg = b_builder.make_b2x(
        particles=[dz_KsLLhh, dzb_KsLLhh, kaon],
        descriptors=["B+ -> D0 D0 K+", "B- -> D0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToD0D0K_D0ToKsDDHH(process, MVACut=0.2):  # 3(DD)x3(DD)-body
    dz_KsDDhh = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
    )
    dzb_KsDDhh = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
    )
    kaon = basic_builder.make_tight_kaons()

    line_alg = b_builder.make_b2x(
        particles=[dz_KsDDhh, dzb_KsDDhh, kaon],
        descriptors=["B+ -> D0 D0 K+", "B- -> D0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToD0D0K_D0ToHHHH(process, MVACut=0.2):  # 4x4-body
    dz_hhhh = d_builder.make_dzero_to_hhhh()
    dzb_hhhh = d_builder.make_dzero_to_hhhh()
    kaon = basic_builder.make_tight_kaons()

    line_alg = b_builder.make_b2x(
        particles=[dz_hhhh, dzb_hhhh, kaon],
        descriptors=["B+ -> D0 D0 K+", "B- -> D0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToD0D0K_D0ToHH_D0ToKsLLHH(process, MVACut=0.2):  # 2x3(LL)-body
    dz_hh = d_builder.make_dzero_to_hh()
    dzb_KsLLhh = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
    )
    kaon = basic_builder.make_tight_kaons()

    line_alg = b_builder.make_b2x(
        particles=[dz_hh, dzb_KsLLhh, kaon],
        descriptors=["B+ -> D0 D0 K+", "B- -> D0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
        AllowDiffInputsForSameIDChildren=True,
    )
    return line_alg


@check_process
def make_BuToD0D0K_D0ToHH_D0ToKsDDHH(process, MVACut=0.2):  # 2x3(DD)-body
    dz_hh = d_builder.make_dzero_to_hh()
    dzb_KsDDhh = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
    )
    kaon = basic_builder.make_tight_kaons()

    line_alg = b_builder.make_b2x(
        particles=[dz_hh, dzb_KsDDhh, kaon],
        descriptors=["B+ -> D0 D0 K+", "B- -> D0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
        AllowDiffInputsForSameIDChildren=True,
    )
    return line_alg


@check_process
def make_BuToD0D0K_D0ToHH_D0ToHHHH(process, MVACut=0.2):  # 2x4-body
    dz_hh = d_builder.make_dzero_to_hh()
    dzb_hhhh = d_builder.make_dzero_to_hhhh()
    kaon = basic_builder.make_tight_kaons()

    line_alg = b_builder.make_b2x(
        particles=[dz_hh, dzb_hhhh, kaon],
        descriptors=["B+ -> D0 D0 K+", "B- -> D0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
        AllowDiffInputsForSameIDChildren=True,
    )
    return line_alg


@check_process
def make_BuToD0D0K_D0ToKsLLHH_D0ToKsDDHH(process, MVACut=0.2):  # 3x(LL)x3(DD)-body
    dz_KsLLhh = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
    )
    dzb_KsDDhh = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
    )
    kaon = basic_builder.make_tight_kaons()

    line_alg = b_builder.make_b2x(
        particles=[dz_KsLLhh, dzb_KsDDhh, kaon],
        descriptors=["B+ -> D0 D0 K+", "B- -> D0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
        AllowDiffInputsForSameIDChildren=True,
    )
    return line_alg


@check_process
def make_BuToD0D0K_D0ToKsLLHH_D0ToHHHH(process, MVACut=0.2):  # 3x(LL)x4-body
    dz_KsLLhh = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
    )
    dzb_hhhh = d_builder.make_dzero_to_hhhh()

    kaon = basic_builder.make_tight_kaons()

    line_alg = b_builder.make_b2x(
        particles=[dz_KsLLhh, dzb_hhhh, kaon],
        descriptors=["B+ -> D0 D0 K+", "B- -> D0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
        AllowDiffInputsForSameIDChildren=True,
    )
    return line_alg


@check_process
def make_BuToD0D0K_D0ToKsDDHH_D0ToHHHH(process, MVACut=0.2):  # 3x(DD)x4-body
    dz_KsDDhh = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
    )
    dzb_hhhh = d_builder.make_dzero_to_hhhh()

    kaon = basic_builder.make_tight_kaons()

    line_alg = b_builder.make_b2x(
        particles=[dz_KsDDhh, dzb_hhhh, kaon],
        descriptors=["B+ -> D0 D0 K+", "B- -> D0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
        AllowDiffInputsForSameIDChildren=True,
    )
    return line_alg


# BuToD0D0Pi lines


@check_process
def make_BuToD0D0Pi_D0ToHH(process, MVACut=0.2):  # 2x2-body
    dz_hh = d_builder.make_dzero_to_hh()
    dzb_hh = d_builder.make_dzero_to_hh()
    pion = basic_builder.make_tight_pions()

    line_alg = b_builder.make_b2x(
        particles=[dz_hh, dzb_hh, pion],
        descriptors=["B+ -> D0 D0 pi+", "B- -> D0 D0 pi-"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToD0D0Pi_D0ToKsLLHH(process, MVACut=0.2):  # 3(LL)x3(LL)-body
    dz_KsLLhh = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
    )
    dzb_KsLLhh = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
    )
    pion = basic_builder.make_tight_pions()

    line_alg = b_builder.make_b2x(
        particles=[dz_KsLLhh, dzb_KsLLhh, pion],
        descriptors=["B+ -> D0 D0 pi+", "B- -> D0 D0 pi-"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToD0D0Pi_D0ToKsDDHH(process, MVACut=0.2):  # 3(DD)x3(DD)-body
    dz_KsDDhh = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
    )
    dzb_KsDDhh = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
    )
    pion = basic_builder.make_tight_pions()

    line_alg = b_builder.make_b2x(
        particles=[dz_KsDDhh, dzb_KsDDhh, pion],
        descriptors=["B+ -> D0 D0 pi+", "B- -> D0 D0 pi-"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToD0D0Pi_D0ToHHHH(process, MVACut=0.2):  # 4x4-body
    dz_hhhh = d_builder.make_dzero_to_hhhh()
    dzb_hhhh = d_builder.make_dzero_to_hhhh()
    pion = basic_builder.make_tight_pions()

    line_alg = b_builder.make_b2x(
        particles=[dz_hhhh, dzb_hhhh, pion],
        descriptors=["B+ -> D0 D0 pi+", "B- -> D0 D0 pi-"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToD0D0Pi_D0ToHH_D0ToKsLLHH(process, MVACut=0.2):  # 2x3(LL)-body
    dz_hh = d_builder.make_dzero_to_hh()
    dzb_KsLLhh = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
    )
    pion = basic_builder.make_tight_pions()

    line_alg = b_builder.make_b2x(
        particles=[dz_hh, dzb_KsLLhh, pion],
        descriptors=["B+ -> D0 D0 pi+", "B- -> D0 D0 pi-"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
        AllowDiffInputsForSameIDChildren=True,
    )
    return line_alg


@check_process
def make_BuToD0D0Pi_D0ToHH_D0ToKsDDHH(process, MVACut=0.2):  # 2x3(DD)-body
    dz_hh = d_builder.make_dzero_to_hh()
    dzb_KsDDhh = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
    )
    pion = basic_builder.make_tight_pions()

    line_alg = b_builder.make_b2x(
        particles=[dz_hh, dzb_KsDDhh, pion],
        descriptors=["B+ -> D0 D0 pi+", "B- -> D0 D0 pi-"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
        AllowDiffInputsForSameIDChildren=True,
    )
    return line_alg


@check_process
def make_BuToD0D0Pi_D0ToHH_D0ToHHHH(process, MVACut=0.2):  # 2x4-body
    dz_hh = d_builder.make_dzero_to_hh()
    dzb_hhhh = d_builder.make_dzero_to_hhhh()
    pion = basic_builder.make_tight_pions()

    line_alg = b_builder.make_b2x(
        particles=[dz_hh, dzb_hhhh, pion],
        descriptors=["B+ -> D0 D0 pi+", "B- -> D0 D0 pi-"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
        AllowDiffInputsForSameIDChildren=True,
    )
    return line_alg


@check_process
def make_BuToD0D0Pi_D0ToKsLLHH_D0ToKsDDHH(process, MVACut=0.2):  # 3x(LL)x3(DD)-body
    dz_KsLLhh = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
    )
    dzb_KsDDhh = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
    )
    pion = basic_builder.make_tight_pions()

    line_alg = b_builder.make_b2x(
        particles=[dz_KsLLhh, dzb_KsDDhh, pion],
        descriptors=["B+ -> D0 D0 pi+", "B- -> D0 D0 pi-"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
        AllowDiffInputsForSameIDChildren=True,
    )
    return line_alg


@check_process
def make_BuToD0D0Pi_D0ToKsLLHH_D0ToHHHH(process, MVACut=0.2):  # 3x(LL)x4-body
    dz_KsLLhh = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
    )
    dzb_hhhh = d_builder.make_dzero_to_hhhh()

    pion = basic_builder.make_tight_pions()

    line_alg = b_builder.make_b2x(
        particles=[dz_KsLLhh, dzb_hhhh, pion],
        descriptors=["B+ -> D0 D0 pi+", "B- -> D0 D0 pi-"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
        AllowDiffInputsForSameIDChildren=True,
    )
    return line_alg


@check_process
def make_BuToD0D0Pi_D0ToKsDDHH_D0ToHHHH(process, MVACut=0.2):  # 3x(DD)x4-body
    dz_KsDDhh = d_builder.make_dzero_to_kshh(
        k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
    )
    dzb_hhhh = d_builder.make_dzero_to_hhhh()

    pion = basic_builder.make_tight_pions()

    line_alg = b_builder.make_b2x(
        particles=[dz_KsDDhh, dzb_hhhh, pion],
        descriptors=["B+ -> D0 D0 pi+", "B- -> D0 D0 pi-"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
        AllowDiffInputsForSameIDChildren=True,
    )
    return line_alg


# excited dst to d gamma/pi0 lines


@check_process
def make_BdToDst0DspPi_Dst0ToD0Gamma_D0ToKPiOrKPiPiPi_DspToHHH(process, MVACut=0.2):
    if process == "spruce":
        dz = d_builder.make_dzero_to_kpi_or_kpipipi(
            k_pidk_min=0,
        )
        ds = d_builder.make_dsplus_to_hhh(
            k_pidk_min=0,
            am_min=1868.35,
            am_max=2068.35,
        )
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
    elif process == "hlt2":
        dz = d_builder.make_dzero_to_kpi_or_kpipipi(
            k_pidk_min=0,
        )
        ds = d_builder.make_dsplus_to_hhh(
            k_pidk_min=0,
            am_min=1878.35,
            am_max=2058.35,
        )
        pion = basic_builder.make_tight_pions()
    dst = d_builder.make_dzerost_to_dzerogamma(dz)

    line_alg = b_builder.make_b2x(
        particles=[dst, ds, pion],
        descriptors=["B0 -> D*(2007)0 D_s+ pi-", "B0 -> D*(2007)0 D_s- pi+"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BdToDsstpD0Pi_DsstpToDspGamma_DspToHHH_D0ToKPiOrKPiPiPi(process, MVACut=0.2):
    if process == "spruce":
        dz = d_builder.make_dzero_to_kpi_or_kpipipi(
            k_pidk_min=0,
        )
        ds = d_builder.make_dsplus_to_hhh(
            k_pidk_min=0,
            am_min=1868.35,
            am_max=2068.35,
        )
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
    elif process == "hlt2":
        dz = d_builder.make_dzero_to_kpi_or_kpipipi(
            k_pidk_min=0,
        )
        ds = d_builder.make_dsplus_to_hhh(
            k_pidk_min=0,
            am_min=1868.35,
            am_max=2068.35,
        )
        pion = basic_builder.make_tight_pions()
    dsst = d_builder.make_dsst_to_dsplusgamma(ds)

    line_alg = b_builder.make_b2x(
        particles=[dsst, dz, pion],
        descriptors=["B0 -> D*_s+ D0 pi-", "B0 -> D*_s- D0 pi+"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDsstpDmPi_DsstpToDspGamma_DspToHHH_DmToHHH(process, MVACut=0.2):
    if process == "spruce":
        dp = d_builder.make_dplus_to_kpipi_or_kkpi(
            k_pidk_min=0,
            am_min=1769.66,
            am_max=1969.66,
        )
        ds = d_builder.make_dsplus_to_hhh(
            k_pidk_min=0,
            am_min=1878.35,
            am_max=2058.35,
        )
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
    elif process == "hlt2":
        dp = d_builder.make_dplus_to_kpipi_or_kkpi(
            k_pidk_min=0,
            am_min=1779.66,
            am_max=1959.66,
        )
        ds = d_builder.make_dsplus_to_hhh(
            k_pidk_min=0,
            am_min=1878.35,
            am_max=2058.35,
        )
        pion = basic_builder.make_tight_pions()
    dsst = d_builder.make_dsst_to_dsplusgamma(ds)

    line_alg = b_builder.make_b2x(
        particles=[dsst, dp, pion],
        descriptors=["[B+ -> D*_s+ D- pi+]cc"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


# BuToDst0D0K (Dst0ToD0Gamma) lines


@check_process
def make_BuToDst0D0K_Dst0ToD0Gamma_D0ToHH_D0ToHH(
    process, MVACut=0.2
):  # 2x2-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
        )
        kaon = basic_builder.make_tight_kaons()
    dst = d_builder.make_dzerost_to_dzerogamma(dz_hh)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Gamma_D0ToKsLLHH_D0ToKsLLHH(
    process, MVACut=0.2
):  # 3(LL)x3(LL)-body D0 decays
    if process == "spruce":
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons()
    dst = d_builder.make_dzerost_to_dzerogamma(dz_KshhLL)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhLL, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Gamma_D0ToKsDDHH_D0ToKsDDHH(
    process, MVACut=0.2
):  # 3(DD)x3(DD)-body D0 decays
    if process == "spruce":
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons()
    dst = d_builder.make_dzerost_to_dzerogamma(dz_KshhDD)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhDD, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Gamma_D0ToHHHH_D0ToHHHH(
    process, MVACut=0.2
):  # 4x4-body D0 decays
    if process == "spruce":
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
        )
        kaon = basic_builder.make_tight_kaons()
    dst = d_builder.make_dzerost_to_dzerogamma(dz_hhhh)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hhhh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Gamma_D0ToHH_D0ToKsLLHH(
    process, MVACut=0.2
):  # 2_D0fromDstx3_D0(LL)-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons()
    dst = d_builder.make_dzerost_to_dzerogamma(dz_hh)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhLL, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Gamma_D0ToKsLLHH_D0ToHH(
    process, MVACut=0.2
):  # 3_D0fromDst(LL)x2_D0-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons()
    dst = d_builder.make_dzerost_to_dzerogamma(dz_KshhLL)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Gamma_D0ToHH_D0ToKsDDHH(
    process, MVACut=0.2
):  # 2_D0fromDstx3_D0(DD)-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons()
    dst = d_builder.make_dzerost_to_dzerogamma(dz_hh)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhDD, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Gamma_D0ToKsDDHH_D0ToHH(
    process, MVACut=0.2
):  # 3_D0fromDst(DD)x2_D0-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons()
    dst = d_builder.make_dzerost_to_dzerogamma(dz_KshhDD)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Gamma_D0ToKsLLHH_D0ToKsDDHH(
    process, MVACut=0.2
):  # 3_D0fromDst(LL)x3_D0(DD)-body D0 decays
    if process == "spruce":
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons()
    dst = d_builder.make_dzerost_to_dzerogamma(dz_KshhLL)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhDD, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Gamma_D0ToKsDDHH_D0ToKsLLHH(
    process, MVACut=0.2
):  # 3_D0fromDst(DD)x3_D0(LL)-body D0 decays
    if process == "spruce":
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons()
    dst = d_builder.make_dzerost_to_dzerogamma(dz_KshhDD)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhLL, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Gamma_D0ToHH_D0ToHHHH(
    process, MVACut=0.2
):  # 2_D0fromDstx4_D0-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
        )
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
        )
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
        )
        kaon = basic_builder.make_tight_kaons()
    dst = d_builder.make_dzerost_to_dzerogamma(dz_hh)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hhhh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Gamma_D0ToHHHH_D0ToHH(
    process, MVACut=0.2
):  # 4_D0fromDstx2_D0-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
        )
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
        )
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
        )
        kaon = basic_builder.make_tight_kaons()
    dst = d_builder.make_dzerost_to_dzerogamma(dz_hhhh)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Gamma_D0ToHHHH_D0ToKsLLHH(
    process, MVACut=0.2
):  # 4_D0fromDstx3_D0(LL)-body D0 decays
    if process == "spruce":
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons()
    dst = d_builder.make_dzerost_to_dzerogamma(dz_hhhh)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhLL, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Gamma_D0ToKsLLHH_D0ToHHHH(
    process, MVACut=0.2
):  # 3_D0fromDst(LL)x4_D0-body D0 decays
    if process == "spruce":
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons()
    dst = d_builder.make_dzerost_to_dzerogamma(dz_KshhLL)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hhhh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Gamma_D0ToHHHH_D0ToKsDDHH(
    process, MVACut=0.2
):  # 4_D0fromDstx3_D0(DD)-body D0 decays
    if process == "spruce":
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons()
    dst = d_builder.make_dzerost_to_dzerogamma(dz_hhhh)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhDD, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Gamma_D0ToKsDDHH_D0ToHHHH(
    process, MVACut=0.2
):  # 3_D0fromDst(DD)x4_D0-body D0 decays
    if process == "spruce":
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(),
            pi_pidk_max=20,
            k_pidk_min=-10,
        )
        kaon = basic_builder.make_tight_kaons()
    dst = d_builder.make_dzerost_to_dzerogamma(dz_KshhDD)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hhhh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


# BuToDst0D0K (Dst0ToD0Pi0Merged) lines


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToHH_D0ToHH(process):  # 2x2-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh()
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_hh, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToKsLLHH_D0ToKsLLHH(
    process,
):  # 3(LL)x3(LL)-body D0 decays
    if process == "spruce":
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_KshhLL, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhLL, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToKsDDHH_D0ToKsDDHH(
    process,
):  # 3(DD)x3(DD)-body D0 decays
    if process == "spruce":
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_KshhDD, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhDD, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToHHHH_D0ToHHHH(process):  # 4x4-body D0 decays
    if process == "spruce":
        dz_hhhh = d_builder.make_dzero_to_hhhh(pi_pidk_max=None, k_pidk_min=None)
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hhhh = d_builder.make_dzero_to_hhhh()
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_hhhh, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hhhh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToHH_D0ToKsLLHH(
    process,
):  # 2_D0fromDstx3_D0(LL)-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh()
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_hh, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhLL, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToKsLLHH_D0ToHH(
    process,
):  # 3_D0fromDst(LL)x2_D0-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh()
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_KshhLL, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToHH_D0ToKsDDHH(
    process,
):  # 2_D0fromDstx3_D0(DD)-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh()
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_hh, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhDD, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToKsDDHH_D0ToHH(
    process,
):  # 3_D0fromDst(DD)x2_D0-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh()
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_KshhDD, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToKsLLHH_D0ToKsDDHH(
    process,
):  # 3_D0fromDst(LL)x3_D0(DD)-body D0 decays
    if process == "spruce":
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_KshhLL, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhDD, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToKsDDHH_D0ToKsLLHH(
    process,
):  # 3_D0fromDst(DD)x3_D0(LL)-body D0 decays
    if process == "spruce":
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_KshhDD, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhLL, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToHH_D0ToHHHH(
    process,
):  # 2_D0fromDstx4_D0-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        dz_hhhh = d_builder.make_dzero_to_hhhh(pi_pidk_max=None, k_pidk_min=None)
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh()
        dz_hhhh = d_builder.make_dzero_to_hhhh()
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_hh, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hhhh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToHHHH_D0ToHH(
    process,
):  # 4_D0fromDstx2_D0-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(pi_pidk_max=None, k_pidk_min=None)
        dz_hhhh = d_builder.make_dzero_to_hhhh(pi_pidk_max=None, k_pidk_min=None)
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh()
        dz_hhhh = d_builder.make_dzero_to_hhhh()
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_hhhh, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToHHHH_D0ToKsLLHH(
    process,
):  # 4_D0fromDstx3_D0(LL)-body D0 decays
    if process == "spruce":
        dz_hhhh = d_builder.make_dzero_to_hhhh(pi_pidk_max=None, k_pidk_min=None)
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hhhh = d_builder.make_dzero_to_hhhh()
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_hhhh, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhLL, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToKsLLHH_D0ToHHHH(
    process,
):  # 3_D0fromDst(LL)x4_D0-body D0 decays
    if process == "spruce":
        dz_hhhh = d_builder.make_dzero_to_hhhh(pi_pidk_max=None, k_pidk_min=None)
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hhhh = d_builder.make_dzero_to_hhhh()
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_KshhLL, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hhhh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToHHHH_D0ToKsDDHH(
    process,
):  # 4_D0fromDstx3_D0(DD)-body D0 decays
    if process == "spruce":
        dz_hhhh = d_builder.make_dzero_to_hhhh(pi_pidk_max=None, k_pidk_min=None)
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hhhh = d_builder.make_dzero_to_hhhh()
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_hhhh, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhDD, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToKsDDHH_D0ToHHHH(
    process,
):  # 3_D0fromDst(DD)x4_D0-body D0 decays
    if process == "spruce":
        dz_hhhh = d_builder.make_dzero_to_hhhh(pi_pidk_max=None, k_pidk_min=None)
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz_hhhh = d_builder.make_dzero_to_hhhh()
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_KshhDD, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hhhh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
    )
    return line_alg


# BuToDst0D0K (Dst0ToD0Pi0Resolved) lines


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToHH_D0ToHH(
    process, MVACut=0.2
):  # 2x2-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=-5,
            pi_pidk_max=5,
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_hh, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToKsLLHH_D0ToKsLLHH(
    process, MVACut=0.2
):  # 3(LL)x3(LL)-body D0 decays
    if process == "spruce":
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    elif process == "hlt2":
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_KshhLL, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhLL, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToKsDDHH_D0ToKsDDHH(
    process, MVACut=0.2
):  # 3(DD)x3(DD)-body D0 decays
    if process == "spruce":
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    elif process == "hlt2":
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_KshhDD, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhDD, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToHHHH_D0ToHHHH(
    process, MVACut=0.2
):  # 4x4-body D0 decays
    if process == "spruce":
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=-5,
            pi_pidk_max=5,
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    elif process == "hlt2":
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_hhhh, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hhhh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToHH_D0ToKsLLHH(
    process, MVACut=0.2
):  # 2_D0fromDstx3_D0(LL)-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=-5,
            pi_pidk_max=5,
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_hh, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhLL, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToKsLLHH_D0ToHH(
    process, MVACut=0.2
):  # 3_D0fromDst(LL)x2_D0-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=-5,
            pi_pidk_max=5,
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_KshhLL, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToHH_D0ToKsDDHH(
    process, MVACut=0.2
):  # 2_D0fromDstx3_D0(DD)-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=-5,
            pi_pidk_max=5,
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_hh, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhDD, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToKsDDHH_D0ToHH(
    process, MVACut=0.2
):  # 3_D0fromDst(DD)x2_D0-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=-5,
            pi_pidk_max=5,
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_KshhDD, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToKsLLHH_D0ToKsDDHH(
    process, MVACut=0.2
):  # 3_D0fromDst(LL)x3_D0(DD)-body D0 decays
    if process == "spruce":
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    elif process == "hlt2":
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_KshhLL, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhDD, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToKsDDHH_D0ToKsLLHH(
    process, MVACut=0.2
):  # 3_D0fromDst(DD)x3_D0(LL)-body D0 decays
    if process == "spruce":
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    elif process == "hlt2":
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_KshhDD, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhLL, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToHH_D0ToHHHH(
    process, MVACut=0.2
):  # 2_D0fromDstx4_D0-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=-5,
            pi_pidk_max=5,
        )
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=-5,
            pi_pidk_max=5,
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_hh, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hhhh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToHHHH_D0ToHH(
    process, MVACut=0.2
):  # 4_D0fromDstx2_D0-body D0 decays
    if process == "spruce":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=-5,
            pi_pidk_max=5,
        )
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=-5,
            pi_pidk_max=5,
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    elif process == "hlt2":
        dz_hh = d_builder.make_dzero_to_hh(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_hhhh, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToHHHH_D0ToKsLLHH(
    process, MVACut=0.2
):  # 4_D0fromDstx3_D0(LL)-body D0 decays
    if process == "spruce":
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=-5,
            pi_pidk_max=5,
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    elif process == "hlt2":
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_hhhh, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhLL, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToKsLLHH_D0ToHHHH(
    process, MVACut=0.2
):  # 3_D0fromDst(LL)x4_D0-body D0 decays
    if process == "spruce":
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=-5,
            pi_pidk_max=5,
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    elif process == "hlt2":
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        dz_KshhLL = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_LL(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_KshhLL, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hhhh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToHHHH_D0ToKsDDHH(
    process, MVACut=0.2
):  # 4_D0fromDstx3_D0(DD)-body D0 decays
    if process == "spruce":
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=-5,
            pi_pidk_max=5,
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    elif process == "hlt2":
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_hhhh, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_KshhDD, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToKsDDHH_D0ToHHHH(
    process, MVACut=0.2
):  # 3_D0fromDst(DD)x4_D0-body D0 decays
    if process == "spruce":
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=-5,
            pi_pidk_max=5,
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons(
            k_pidk_min=0,
        )
    elif process == "hlt2":
        dz_hhhh = d_builder.make_dzero_to_hhhh(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        dz_KshhDD = d_builder.make_dzero_to_kshh(
            k_shorts=basic_builder.make_ks_DD(), pi_pidk_max=20, k_pidk_min=-10
        )
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz_KshhDD, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dz_hhhh, kaon],
        descriptors=["B+ -> D*(2007)0 D0 K+", "B- -> D*(2007)0 D0 K-"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


# BuToDsD0Pi0 lines


@check_process
def make_BuToDsD0Pi0Merged_DsToKKPi_D0ToKPiOrKPiPiPi(process, MVACut=0.2):
    if process == "spruce":
        dz = d_builder.make_dzero_to_kpi_or_kpipipi(
            k_pidk_min=-5,
            pi_pidk_max=5,
        )
        ds = d_builder.make_dsplus_to_kpkmpip(
            k_pidk_min=-5,
            pi_pidk_max=5,
        )
    else:
        dz = d_builder.make_dzero_to_kpi_or_kpipipi(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        ds = d_builder.make_dsplus_to_kpkmpip(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
    pi0 = basic_builder.make_merged_pi0s()
    line_alg = b_builder.make_b2x(
        particles=[ds, dz, pi0],
        descriptors=["B+ -> D_s+ D0 pi0", "B- -> D_s- D0 pi0"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDsD0Pi0Resolved_DsToKKPi_D0ToKPi(process, MVACut=0.2):
    if process == "spruce":
        dz = d_builder.make_dzero_to_kpi(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        ds = d_builder.make_dsplus_to_kpkmpip(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
    else:
        dz = d_builder.make_dzero_to_kpi(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        ds = d_builder.make_dsplus_to_kpkmpip(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
    pi0 = basic_builder.make_resolved_pi0s()
    line_alg = b_builder.make_b2x(
        particles=[ds, dz, pi0],
        descriptors=["B+ -> D_s+ D0 pi0", "B- -> D_s- D0 pi0"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
    )
    return line_alg


@check_process
def make_BuToDsD0Pi0Resolved_DsToKKPi_D0ToKPiPiPi(process, MVACut=0.2):
    if process == "spruce":
        dz = d_builder.make_dzero_to_kpipipi(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        ds = d_builder.make_dsplus_to_kpkmpip(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
    else:
        dz = d_builder.make_dzero_to_kpipipi(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        ds = d_builder.make_dsplus_to_kpkmpip(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
    pi0 = basic_builder.make_resolved_pi0s()
    line_alg = b_builder.make_b2x(
        particles=[ds, dz, pi0],
        descriptors=["B+ -> D_s+ D0 pi0", "B- -> D_s- D0 pi0"],
        sum_pt_min=6 * GeV,
        b2ddh_sep_min=(0) * mm,
    )
    return line_alg


# BdToDst0DK (Dst0ToD0Gamma) line


@check_process
def make_BdToDst0DK_Dst0ToD0Gamma_D0ToKPiOrKPiPiPi_DToHHH(process, MVACut=0.2):
    if process == "spruce":
        dz = d_builder.make_dzero_to_kpi_or_kpipipi(
            k_pidk_min=0,
        )
        dp = d_builder.make_dplus_to_kpipi_or_kkpi(
            k_pidk_min=0,
            am_min=1769.66,
            am_max=1969.66,
        )
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        dz = d_builder.make_dzero_to_kpi_or_kpipipi(
            k_pidk_min=0,
        )
        dp = d_builder.make_dplus_to_kpipi_or_kkpi(
            k_pidk_min=0,
            am_min=1769.66,
            am_max=1969.66,
        )
        kaon = basic_builder.make_tight_kaons()
    dst = d_builder.make_dzerost_to_dzerogamma(dz)

    line_alg = b_builder.make_b2x(
        particles=[dst, dp, kaon],
        descriptors=["B0 -> D*(2007)0 D+ K-", "B0 -> D*(2007)0 D- K+"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg


# BdToDst0DK (Dst0ToD0Pi0) lines


@check_process
def make_BdToDst0DK_Dst0ToD0Pi0Merged_D0ToKPiOrKPiPiPi_DToHHH(process):
    dz = d_builder.make_dzero_to_kpi_or_kpipipi()
    dp = d_builder.make_dplus_to_kpipi_or_kkpi()
    kaon = basic_builder.make_tightpid_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dp, kaon],
        descriptors=["B0 -> D*(2007)0 D+ K-", "B0 -> D*(2007)0 D- K+"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(-0.5) * mm,
    )
    return line_alg


@check_process
def make_BdToDst0DK_Dst0ToD0Pi0Resolved_D0ToKPiOrKPiPiPi_DToHHH(process):
    dz = d_builder.make_dzero_to_kpi_or_kpipipi()
    dp = d_builder.make_dplus_to_kpipi_or_kkpi()
    kaon = basic_builder.make_tightpid_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, dp, kaon],
        descriptors=["B0 -> D*(2007)0 D+ K-", "B0 -> D*(2007)0 D- K+"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(-0.5) * mm,
    )
    return line_alg


# BdToDst0DsPi (Dst0ToD0Pi0Resolved) line


@check_process
def make_BdToDst0DsPi_Dst0ToD0Pi0Resolved_D0ToKPiOrKPiPiPi_DsToHHH(process, MVACut=0.2):
    if process == "spruce":
        dz = d_builder.make_dzero_to_kpi_or_kpipipi(
            k_pidk_min=-5,
            pi_pidk_max=5,
        )
        ds = d_builder.make_dsplus_to_hhh(
            k_pidk_min=-5,
            pi_pidk_max=5,
        )
        pion = basic_builder.make_tight_pions(
            pi_pidk_max=1,
        )
    elif process == "hlt2":
        dz = d_builder.make_dzero_to_kpi_or_kpipipi(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        ds = d_builder.make_dsplus_to_hhh(
            k_pidk_min=0,
            pi_pidk_max=1,
        )
        pion = basic_builder.make_tight_pions(
            pi_pidk_max=1,
        )
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz, pi0)
    line_alg = b_builder.make_b2x(
        particles=[dst, ds, pion],
        descriptors=["B0 -> D*(2007)0 D_s+ pi-", "B0 -> D*(2007)0 D_s- pi+"],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(0) * mm,
    )
    return line_alg
