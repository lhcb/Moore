###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""

from GaudiKernel.SystemOfUnits import GeV, MeV

from Hlt2Conf.lines.b_to_open_charm.builders import (
    b_builder,
    basic_builder,
    cbaryon_builder,
)
from Hlt2Conf.lines.b_to_open_charm.utils import check_process


##############################################################
# Form the B+ --> Lc+ Xic- pi+, Lc+ --> p K- pi+, Xic+ -->p K- pi+
##############################################################
@check_process
def make_BuToLcpXicmPi_LcpToPKPi_XicmToPKPi(process):
    pion = basic_builder.make_tight_pions()
    lc = cbaryon_builder.make_tight_lc_to_pkpi()
    xicp = cbaryon_builder.make_tight_xicp_to_pkpi()
    line_alg = b_builder.make_b2x(
        particles=[lc, xicp, pion], descriptors=["[B+ -> Lambda_c+ Xi_c~- pi+]cc"]
    )
    return line_alg


##############################################################
# Form the B0 --> Lc+ Sigma_c-- K+, Lc+ --> p K- pi+, Xic+ -->p K- pi+
##############################################################
@check_process
def make_BdToScmmLcpK_ScmmToLcmPi_LcmToPKPi_LcpToPKPi(process):
    kaon = basic_builder.make_tight_kaons()
    lc = cbaryon_builder.make_tight_lc_to_pkpi()
    sigmacpp = cbaryon_builder.make_sigmacpp_to_lcppi()
    line_alg = b_builder.make_b2x(
        particles=[sigmacpp, lc, kaon],
        descriptors=["[B0 -> Sigma_c~-- Lambda_c+ K+]cc"],
    )
    return line_alg


##################################
# B+ -> Lc+ Lcbar- K+
# Author: juan.baptista.leite@cern.ch
##################################
@check_process
def make_BuToLcpLcmK_LcpToPKPi(process):
    if process == "spruce":
        lc = cbaryon_builder.make_lc_to_pkpi(
            pi_pidk_max=10,
            k_pidk_min=-10,
            p_pidp_min=-10,
            am_min=2176 * MeV,
            am_max=2396 * MeV,
        )
        kaon = basic_builder.make_kaons(k_pidk_min=-10)
    elif process == "hlt2":
        lc = cbaryon_builder.make_lc_to_pkpi(
            am_min=2176 * MeV,
            am_max=2396 * MeV,
        )
        kaon = basic_builder.make_kaons(k_pidk_min=-5)
    line_alg = b_builder.make_b2x(
        particles=[lc, lc, kaon],
        descriptors=["[B+ -> Lambda_c+ Lambda_c~- K+]cc"],
        vtx_chi2pdof_max=10.0,
        sum_pt_min=6.0 * GeV,
        bpvipchi2_max=15,
        bpvdira_min=0.9997,
        am_min=5050 * MeV,
        am_max=6000 * MeV,
        am_min_vtx=5050 * MeV,
        am_max_vtx=6000 * MeV,
    )
    return line_alg
