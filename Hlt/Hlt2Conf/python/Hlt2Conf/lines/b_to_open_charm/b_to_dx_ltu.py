###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC BToDX LTUnbiased lines
* do NOT use SigmaNet NN here, these lines are instead prescaled
"""

import Functors as F
from Functors import require_all
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV
from RecoConf.algorithms_thor import ParticleFilter

from Hlt2Conf.lines.b_to_open_charm.builders import b_builder, basic_builder, d_builder
from Hlt2Conf.lines.b_to_open_charm.utils import check_process

"""
see:
https://gitlab.cern.ch/lhcb-datapkg/AnalysisProductions/-/merge_requests/1443/diffs
for mipchi2_min=0 and bpvvdchi2_min=0
https://gitlab.cern.ch/lhcb/Stripping/-/blob/run2-patches/Phys/StrippingSelections/python/StrippingSelections/StrippingB2OC/Beauty2Charm_LTUnbiased.py#L69-L73
"""


@check_process
def make_BdToDsmK_DsmToKpKmPim_LTU(process):
    kaon = basic_builder.make_tight_kaons(k_pidk_min=None, mipchi2_min=0)
    d = d_builder.make_dsplus_to_kpkmpip(bpvvdchi2_min=0)
    code = require_all(
        in_range(1930.0, F.MASS, 2025.0),
    )
    d_narrow = ParticleFilter(d, F.FILTER(code))
    line_alg = b_builder.make_lifetime_unbiased_b2x(
        particles=[d_narrow, kaon], descriptors=["[B0 -> D_s- K+]cc"]
    )
    return line_alg


@check_process
def make_BdToDmPi_DmToPimPimKp_LTU(process):
    pion = basic_builder.make_tight_pions(pi_pidk_max=0, mipchi2_min=0)
    d = d_builder.make_dplus_to_kmpippip(bpvvdchi2_min=0)
    code = require_all(
        in_range(1830.0, F.MASS, 1920.0),
    )
    d_narrow = ParticleFilter(d, F.FILTER(code))
    line_alg = b_builder.make_lifetime_unbiased_b2x(
        particles=[d_narrow, pion],
        descriptors=["[B0 -> D- pi+]cc"],
        am_min=4950 * MeV,
        am_min_vtx=4950 * MeV,
        am_max=6000 * MeV,
        am_max_vtx=6000 * MeV,
    )
    return line_alg


"""
https://gitlab.cern.ch/lhcb/Stripping/-/blob/run2-patches/Phys/StrippingSelections/python/StrippingSelections/StrippingB2OC/Beauty2Charm_LTUnbiased.py#L75-L81
"""


@check_process
def make_BdToDsmKPiPi_DsmToKmKpPim_LTU(process):
    kaon = basic_builder.make_tight_kaons(mipchi2_min=0)
    pion = basic_builder.make_pions(mipchi2_min=0)
    d = d_builder.make_dsplus_to_kpkmpip(bpvvdchi2_min=0)
    line_alg = b_builder.make_lifetime_unbiased_b2x(
        particles=[d, kaon, pion, pion], descriptors=["[B0 -> D_s- K+ pi+ pi-]cc"]
    )
    return line_alg
