# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This tightware is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of Neutron Lines
"""

from GaudiKernel.SystemOfUnits import GeV, MeV

from Hlt2Conf.lines.b_to_open_charm.builders import (
    b_builder,
    basic_builder,
    cbaryon_builder,
)
from Hlt2Conf.lines.b_to_open_charm.utils import check_process


# Author: juan.baptista.leite@cern.ch
# https://lhcbdoc.web.cern.ch/lhcbdoc/stripping/config/stripping34r0p3/bhadroncompleteevent/strippinglb2lclcnlc2pkpibeauty2charmline.html
@check_process
def make_LbToLcpLcmN0(process):
    if process == "spruce":
        lc = cbaryon_builder.make_lc_to_pkpi(  # make_lc_to_pkpi
            pi_pidk_max=10,
            k_pidk_min=-10,
            p_pidp_min=-10,
            am_min=2176 * MeV,
            am_max=2396 * MeV,
        )
    elif process == "hlt2":
        lc = cbaryon_builder.make_lc_to_pkpi(
            am_min=2176 * MeV,
            am_max=2396 * MeV,
        )

    line_alg = b_builder.make_b2x(
        [lc, lc],
        descriptors=["Lambda_b0 -> Lambda_c+ Lambda_c~-"],
        am_min=3900 * MeV,
        am_max=7000 * MeV,
        am_min_vtx=3900 * MeV,
        am_max_vtx=7000 * MeV,
        vtx_chi2pdof_max=10.0,
        sum_pt_min=6.0 * GeV,
        bpvipchi2_max=15,
        bpvdira_min=0.9997,
    )

    return line_alg


# Author: juan.baptista.leite@cern.ch
@check_process
def make_LbToPbarPN0(process):
    line_alg = None
    if process == "spruce":
        proton = basic_builder.make_tight_protons(p_pidkmp_max=20.0, pt_min=750 * MeV)
    elif process == "hlt2":
        proton = basic_builder.make_tight_protons(
            p_pidp_min=-5.0, p_pidkmp_max=10.0, pt_min=750 * MeV
        )

    line_alg = b_builder.make_b2x(
        [proton, proton],
        descriptors=["Lambda_b0 -> p+ p~-"],
        am_min=4000 * MeV,
        am_max=7000 * MeV,
        am_min_vtx=4000 * MeV,
        am_max_vtx=7000 * MeV,
        vtx_chi2pdof_max=10.0,
        sum_pt_min=6.0 * GeV,
        bpvipchi2_max=15,
        bpvdira_min=0.9997,
        bpvfdchi2_min=100,
    )

    return line_alg
