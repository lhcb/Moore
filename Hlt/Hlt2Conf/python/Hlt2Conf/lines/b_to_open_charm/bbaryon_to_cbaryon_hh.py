###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond

from Hlt2Conf.lines.b_to_open_charm.builders import (
    b_builder,
    basic_builder,
    cbaryon_builder,
)
from Hlt2Conf.lines.b_to_open_charm.utils import check_process


###########################################################
# Form the Xi_bc0 -> Xi_c0 Pi- Pi+, Xi_c0 --> p K- K- pi+
##########################################################
@check_process
def make_Xibc0ToXic0PiPi_Xic0ToPKKPi(process):
    if process == "spruce":
        pion = basic_builder.make_tight_pions()
        pion_soft = basic_builder.make_soft_pions()
        cbaryon = cbaryon_builder.make_xic0_to_pkkpi()
    elif process == "hlt2":
        pion = basic_builder.make_tightpid_tight_pions()
        pion_soft = basic_builder.make_tightpid_soft_pions()
        cbaryon = cbaryon_builder.make_tight_xic0_to_pkkpi_for_xibc()

    ### applying hard cuts on pion if it's not forming Xic* with any of Xic
    Xicst_12 = (F.MASS - F.CHILD(1, F.MASS)) < 185 * MeV
    tight2 = (F.CHILD(2, F.PT) > 500 * MeV) & (F.CHILD(2, F.P) > 5 * GeV)

    comb12_cut_add = Xicst_12 | tight2

    line_alg = b_builder.make_xibc2cx(
        particles=[cbaryon, pion_soft, pion],
        descriptors=["[Xi_bc0 -> Xi_c0 pi+ pi-]cc"],
        sum_pt_hbach_min=2.25 * GeV,
        comb12_cut_add=comb12_cut_add,
    )
    return line_alg


###########################################################
# Form the Xi_bc+ -> Lc+ K- Pi+, Lc+ --> p K- pi+
##########################################################
@check_process
def make_XibcpToLcpKmPip_LcpToPKPi(process):
    if process == "spruce":
        pion = basic_builder.make_soft_pions()
        kaon = basic_builder.make_tight_kaons()
        cbaryon = cbaryon_builder.make_lc_to_pkpi()
    elif process == "hlt2":
        pion = basic_builder.make_tightpid_soft_pions()
        kaon = basic_builder.make_tightpid_tight_kaons(k_pidk_min=-2)
        cbaryon = cbaryon_builder.make_tight_lc_to_pkpi_for_xibc()

    ### applying hard cuts on pion if it's not forming Sigmac with Lc
    Sc_12 = (F.MASS - F.CHILD(1, F.MASS)) < 300 * MeV
    tight2 = (F.CHILD(2, F.PT) > 500 * MeV) & (F.CHILD(2, F.P) > 5 * GeV)

    comb12_cut_add = Sc_12 | tight2

    line_alg = b_builder.make_xibc2cx(
        particles=[cbaryon, pion, kaon],
        descriptors=["[Xi_bc+ -> Lambda_c+ pi+ K-]cc"],
        sum_pt_hbach_min=2.25 * GeV,
        comb12_cut_add=comb12_cut_add,
    )
    return line_alg


###########################################################
# Form the Xi_bc+ -> Xic+ Pi- Pi+, Xic+ --> p K- pi+
##########################################################
@check_process
def make_XibcpToXicpPiPi_XicpToPKPi(process):
    if process == "spruce":
        pion = basic_builder.make_tight_pions()
        pion_soft = basic_builder.make_soft_pions()
        cbaryon = cbaryon_builder.make_xicp_to_pkpi()
    elif process == "hlt2":
        pion = basic_builder.make_tightpid_tight_pions()
        pion_soft = basic_builder.make_tightpid_soft_pions()
        cbaryon = cbaryon_builder.make_tight_xicp_to_pkpi_for_xibc()

    ### applying hard cuts on pion if it's not forming Xic* with any of Xic
    Xicst_12 = (F.MASS - F.CHILD(1, F.MASS)) < 185 * MeV
    tight2 = (F.CHILD(2, F.PT) > 500 * MeV) & (F.CHILD(2, F.P) > 5 * GeV)

    comb12_cut_add = Xicst_12 | tight2

    line_alg = b_builder.make_xibc2cx(
        particles=[cbaryon, pion_soft, pion],
        descriptors=["[Xi_bc+ -> Xi_c+ pi- pi+]cc"],
        sum_pt_hbach_min=2.25 * GeV,
        comb12_cut_add=comb12_cut_add,
    )
    return line_alg


###########################################################
# Form the Xi_bc+ -> Lc+ Pi- Pi+, Lc+ --> p K- pi+
##########################################################
@check_process
def make_XibcpToLcpPiPi_LcpToPKPi(process):
    if process == "spruce":
        pion = basic_builder.make_soft_pions()
        cbaryon = cbaryon_builder.make_lc_to_pkpi()
    elif process == "hlt2":
        pion = basic_builder.make_tightpid_soft_pions()
        cbaryon = cbaryon_builder.make_tight_lc_to_pkpi_for_xibc()

    ### applying hard cuts on pion if it's not forming Sigmac with Lc
    # try with both Sigmac(2455) and (2520) for the moment
    M13 = F.SUBCOMB(Functor=F.MASS, Indices=[1, 3])
    Sc_12 = (F.MASS - F.CHILD(1, F.MASS)) < 300 * MeV
    Sc_13 = (M13 - F.CHILD(1, F.MASS)) < 300 * MeV

    tight2 = (F.CHILD(2, F.PT) > 500 * MeV) & (F.CHILD(2, F.P) > 5 * GeV)
    tight3 = (F.CHILD(3, F.PT) > 500 * MeV) & (F.CHILD(3, F.P) > 5 * GeV)

    comb12_cut_add = Sc_12 | tight2
    comb_cut_add = Sc_13 | tight3

    line_alg = b_builder.make_xibc2cx(
        particles=[cbaryon, pion, pion],
        descriptors=["[Xi_bc+ -> Lambda_c+ pi- pi+]cc"],
        sum_pt_hbach_min=2.25 * GeV,
        comb12_cut_add=comb12_cut_add,
        comb_cut_add=comb_cut_add,
    )
    return line_alg


##############################################################
# Form the Omega_bc0 -> Xi_c0 K- Pi+, Xi_c0 --> p K- K- pi+
##############################################################
@check_process
def make_Ombc0ToXic0KmPip_Xic0ToPKKPi(process):
    if process == "spruce":
        pion = basic_builder.make_soft_pions()
        kaon = basic_builder.make_tight_kaons()
        cbaryon = cbaryon_builder.make_xic0_to_pkkpi()
    elif process == "hlt2":
        pion = basic_builder.make_tightpid_soft_pions()
        kaon = basic_builder.make_tightpid_tight_kaons(k_pidk_min=-2)
        cbaryon = cbaryon_builder.make_tight_xic0_to_pkkpi_for_xibc()

    ### applying hard cuts on pion if it's not forming Xic* with any of Xic
    Xicst_12 = (F.MASS - F.CHILD(1, F.MASS)) < 185 * MeV
    tight2 = (F.CHILD(2, F.PT) > 500 * MeV) & (F.CHILD(2, F.P) > 5 * GeV)

    comb12_cut_add = Xicst_12 | tight2

    line_alg = b_builder.make_xibc2cx(
        particles=[cbaryon, pion, kaon],
        descriptors=["[Omega_bc0 -> Xi_c0 pi+ K-]cc"],
        sum_pt_hbach_min=2.25 * GeV,
        comb12_cut_add=comb12_cut_add,
    )
    return line_alg


##############################################################
# Form the Omega_b- -> Xi_c+ K- Pi-, Xi_c+ --> p K- pi+
##############################################################
@check_process
def make_OmbmToXicpKPi_XicpToPKPi(process):
    if process == "spruce":
        pion = basic_builder.make_tightpid_tight_pions(pi_pidk_max=None)
        kaon = basic_builder.make_tightpid_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        pion = basic_builder.make_tightpid_tight_pions(pi_pidk_max=-1)
        kaon = basic_builder.make_tightpid_tight_kaons(k_pidk_min=1)
    cbaryon = cbaryon_builder.make_tight_xicp_to_pkpi()
    line_alg = b_builder.make_omegab(
        particles=[cbaryon, kaon, pion], descriptors=["[Omega_b- -> Xi_c+ K- pi-]cc"]
    )
    return line_alg


##############################################################
# Form the Omega_b- -> Xi_c+ K- Pi+, Xi_c+ --> p K- pi+ (WS)
##############################################################
@check_process
def make_OmbmToXicpKPiWS_XicpToPKPi(process):
    if process == "spruce":
        pion = basic_builder.make_tightpid_tight_pions(pi_pidk_max=None)
        kaon = basic_builder.make_tightpid_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        pion = basic_builder.make_tightpid_tight_pions()
        kaon = basic_builder.make_tightpid_tight_kaons()
    cbaryon = cbaryon_builder.make_tight_xicp_to_pkpi()
    line_alg = b_builder.make_omegab(
        particles=[cbaryon, kaon, pion], descriptors=["[Omega_b- -> Xi_c+ K- pi+]cc"]
    )
    return line_alg


##############################################################
# Form the Omega_b- -> Xi_c+ K- Pi- Gamma, Xi_c+ --> p K- pi+
# 1. Hadronic part to obtain a vertex, and require
# it to have a non-negiligble b-decay lifetime to
# control the rate.
# 2. Add the neutral state to cut on the mass window.
##############################################################
@check_process
def make_OmbmToXicpKPiGamma_XicpToPKPi(process):
    if process == "spruce":
        pion = basic_builder.make_tightpid_tight_pions(pi_pidk_max=None)
        kaon = basic_builder.make_tightpid_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        pion = basic_builder.make_tightpid_tight_pions()
        kaon = basic_builder.make_tightpid_tight_kaons()
    cbaryon = cbaryon_builder.make_tight_xicp_to_pkpi()
    hadronic = b_builder.make_loose_omegab(
        particles=[cbaryon, kaon, pion],
        descriptors=["[Lambda_b0 -> Xi_c+ K- pi-]cc"],
        am_min=0.0 * MeV,
        am_min_vtx=0.0 * MeV,
    )
    gamma = basic_builder.make_photons(et_min=2000 * MeV)
    line_alg = b_builder.make_omegab_with_neutral(
        particles=[hadronic, gamma], descriptor="[Omega_b- -> Lambda_b0 gamma]cc"
    )
    return line_alg


##############################################################
# Form the Omega_b- -> Xi_c+ K- Pi+ Gamma, Xi_c+ --> p K- pi+ (WS)
# 1. Hadronic part to obtain a vertex, and require
# it to have a non-negiligble b-decay lifetime to
# control the rate.
# 2. Add the neutral state to cut on the mass window.
##############################################################
@check_process
def make_OmbmToXicpKPiGammaWS_XicpToPKPi(process):
    if process == "spruce":
        pion = basic_builder.make_tightpid_tight_pions(pi_pidk_max=None)
        kaon = basic_builder.make_tightpid_tight_kaons(k_pidk_min=None)
    elif process == "hlt2":
        pion = basic_builder.make_tightpid_tight_pions()
        kaon = basic_builder.make_tightpid_tight_kaons()
    cbaryon = cbaryon_builder.make_tight_xicp_to_pkpi()
    hadronic = b_builder.make_loose_omegab(
        particles=[cbaryon, kaon, pion],
        descriptors=["[Lambda_b0 -> Xi_c+ K- pi+]cc"],
        am_min=0.0 * MeV,
        am_min_vtx=0.0 * MeV,
    )
    gamma = basic_builder.make_photons(et_min=2000 * MeV)
    line_alg = b_builder.make_omegab_with_neutral(
        particles=[hadronic, gamma], descriptor="[Omega_b- -> Lambda_b0 gamma]cc"
    )
    return line_alg


##############################################
# Form the Omegab- -> Omegac0 Pi- Pi0
# 1. Hadronic part to obtain a vertex, and require
# it to have a non-negiligble b-decay lifetime to
# control the rate.
# 2. Add the neutral state to cut on the mass window.
##############################################
@check_process
def make_OmbmToOmc0PiPi0Resolved_Omc0ToPKKPi(process):
    pion = basic_builder.make_tightpid_tight_pions()
    cbaryon = cbaryon_builder.make_omegac0_to_pkkpi()
    hadronic = b_builder.make_loose_omegab(
        particles=[cbaryon, pion],
        descriptors=["[Lambda_b0 -> Omega_c0 pi-]cc"],
        am_min=0.0 * MeV,
        am_min_vtx=0.0 * MeV,
    )
    pi0 = basic_builder.make_resolved_pi0s(pt_min=1500 * MeV)
    line_alg = b_builder.make_omegab_with_neutral(
        particles=[hadronic, pi0],
        descriptor="[Omega_b- -> Lambda_b0 pi0]cc",
    )
    return line_alg


##############################################
# Form the Omegab- -> Omegac0 Pi+ Pi0 (WS)
# 1. Hadronic part to obtain a vertex, and require
# it to have a non-negiligble b-decay lifetime to
# control the rate.
# 2. Add the neutral state to cut on the mass window.
##############################################
@check_process
def make_OmbmToOmc0PiPi0ResolvedWS_Omc0ToPKKPi(process):
    pion = basic_builder.make_tightpid_tight_pions()
    cbaryon = cbaryon_builder.make_omegac0_to_pkkpi()
    hadronic = b_builder.make_loose_omegab(
        particles=[cbaryon, pion],
        descriptors=["[Lambda_b0 -> Omega_c0 pi+]cc"],
        am_min=0.0 * MeV,
        am_min_vtx=0.0 * MeV,
    )
    pi0 = basic_builder.make_resolved_pi0s(pt_min=1500 * MeV)
    line_alg = b_builder.make_omegab_with_neutral(
        particles=[hadronic, pi0],
        descriptor="[Omega_b- -> Lambda_b0 pi0]cc",
    )
    return line_alg


##############################################
# XibToLchh lines
##############################################


##############################################################
# Form the Xi_b-(Om_b-) --> Lc+ pi- pi-, Lc+ --> p K- pi+
##############################################################
@check_process
def make_XibmToLcpPiPi_LcpToPKPi(process):
    if process == "spruce":
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
        lc = cbaryon_builder.make_lc_to_pkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None
        )
    elif process == "hlt2":
        pion = basic_builder.make_soft_pions()
        lc = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_xib2chh(
        particles=[pion, pion, lc], descriptors=["[Xi_b- -> pi- pi- Lambda_c+]cc"]
    )
    return line_alg


##############################################################
# Form the Xi_b-(Om_b-) --> Lc+ K- pi-, Lc+ --> p K- pi+
##############################################################
@check_process
def make_XibmToLcpKPi_LcpToPKPi(process):
    if process == "spruce":
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
        lc = cbaryon_builder.make_lc_to_pkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None
        )
    elif process == "hlt2":
        pion = basic_builder.make_tightpid_soft_pions()
        kaon = basic_builder.make_tightpid_soft_kaons()
        lc = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_xib2chh(
        particles=[kaon, pion, lc], descriptors=["[Xi_b- -> K- pi- Lambda_c+]cc"]
    )
    return line_alg


##############################################################
# Form the Xi_b-(Om_b-) --> Lc+ K- pi+, Lc+ --> p K- pi+
##############################################################
@check_process
def make_XibmToLcpKPiWS_LcpToPKPi(process):
    if process == "spruce":
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
        lc = cbaryon_builder.make_lc_to_pkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None
        )
    elif process == "hlt2":
        pion = basic_builder.make_soft_pions()
        kaon = basic_builder.make_soft_kaons()
        lc = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_xib2chh(
        particles=[kaon, pion, lc], descriptors=["[Xi_b- -> K- pi+ Lambda_c+]cc"]
    )
    return line_alg


##############################################################
# Form the Xi_b-(Om_b-) --> Lc+ K- K-, Lc+ --> p K- pi+
##############################################################
@check_process
def make_XibmToLcpKK_LcpToPKPi(process):
    if process == "spruce":
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
        lc = cbaryon_builder.make_lc_to_pkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None
        )
    elif process == "hlt2":
        kaon = basic_builder.make_soft_kaons()
        lc = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_xib2chh(
        particles=[kaon, kaon, lc], descriptors=["[Xi_b- -> K- K- Lambda_c+]cc"]
    )
    return line_alg


##############################################################
# Form the Xi_b-(Om_b-) --> Xi_c+ Pi- Pi-, Xi_c+ --> p K- pi+
##############################################################
@check_process
def make_XibmToXicpPiPi_XicpToPKPi(process):
    if process == "spruce":
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
        xic = cbaryon_builder.make_xicp_to_pkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None
        )
    elif process == "hlt2":
        pion = basic_builder.make_tightpid_soft_pions()
        xic = cbaryon_builder.make_xicp_to_pkpi()
    line_alg = b_builder.make_xib2chh(
        particles=[pion, pion, xic], descriptors=["[Xi_b- -> pi- pi- Xi_c+]cc"]
    )
    return line_alg


##############################################################
# Form the Xi_b-(Om_b-) --> Xi_c+ K- Pi-, Xi_c+ --> p K- pi+
##############################################################
@check_process
def make_XibmToXicpKPi_XicpToPKPi(process):
    if process == "spruce":
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
        xic = cbaryon_builder.make_xicp_to_pkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None
        )
    elif process == "hlt2":
        pion = basic_builder.make_tightpid_soft_pions()
        kaon = basic_builder.make_tightpid_soft_kaons()
        xic = cbaryon_builder.make_xicp_to_pkpi()
    line_alg = b_builder.make_xib2chh(
        particles=[kaon, pion, xic],
        descriptors=["[Xi_b- -> K- pi- Xi_c+]cc"],
        sum_pt_min=6 * GeV,
        bpvltime_min=0.3 * picosecond,
    )
    return line_alg


##############################################################
# Form the Xi_b-(Om_b-) --> Xi_c+ K- K-, Xi_c+ --> p K- pi+
##############################################################
@check_process
def make_XibmToXicpKK_XicpToPKPi(process):
    if process == "spruce":
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
        xic = cbaryon_builder.make_xicp_to_pkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None
        )
    elif process == "hlt2":
        kaon = basic_builder.make_soft_kaons()
        xic = cbaryon_builder.make_xicp_to_pkpi()
    line_alg = b_builder.make_xib2chh(
        particles=[kaon, kaon, xic], descriptors=["[Xi_b- -> K- K- Xi_c+]cc"]
    )
    return line_alg


##############################################
# LbToXic0hh and LbToOmegachhlines
##############################################


##############################################################
# Form the Lambda_b (Xi_b0) --> Xi_c0 pi+ pi-, Xi_c0 --> p K- K- pi+
##############################################################
@check_process
def make_LbToXic0PiPi_Xic0ToPKKPi(process):
    if process == "spruce":
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
        xic0 = cbaryon_builder.make_xic0_to_pkkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None
        )
    elif process == "hlt2":
        pion = basic_builder.make_soft_pions()
        xic0 = cbaryon_builder.make_xic0_to_pkkpi()
    line_alg = b_builder.make_lb2chh(
        particles=[pion, pion, xic0], descriptors=["[Lambda_b0 -> pi+ pi- Xi_c0]cc"]
    )
    return line_alg


##############################################################
# Form the Lambda_b (Xi_b0) --> Xi_c0 K+ pi-, Xi_c0 --> p K- K- pi+
##############################################################
@check_process
def make_LbToXic0KPi_Xic0ToPKKPi(process):
    if process == "spruce":
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
        xic0 = cbaryon_builder.make_xic0_to_pkkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None
        )
    elif process == "hlt2":
        pion = basic_builder.make_soft_pions()
        kaon = basic_builder.make_soft_kaons()
        xic0 = cbaryon_builder.make_xic0_to_pkkpi()
    line_alg = b_builder.make_lb2chh(
        particles=[kaon, pion, xic0],
        descriptors=["[Lambda_b0 -> K+ pi- Xi_c0]cc", "[Lambda_b0 -> K- pi+ Xi_c0]cc"],
    )
    return line_alg


##############################################################
# Form the Lambda_b (Xi_b0) --> Xi_c0 K+ K-, Xi_c0 --> p K- K- pi+
##############################################################
@check_process
def make_LbToXic0KK_Xic0ToPKKPi(process):
    if process == "spruce":
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
        xic0 = cbaryon_builder.make_xic0_to_pkkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None
        )
    elif process == "hlt2":
        kaon = basic_builder.make_soft_kaons()
        xic0 = cbaryon_builder.make_xic0_to_pkkpi()
    line_alg = b_builder.make_lb2chh(
        particles=[kaon, kaon, xic0], descriptors=["[Lambda_b0 -> K+ K- Xi_c0]cc"]
    )
    return line_alg


##############################################################
# Form the Lambda_b (Xi_b0) --> Omega_c0 pi+ pi-, Omega_c0 --> p K- K- pi+
##############################################################
@check_process
def make_LbToOmc0PiPi_Omc0ToPKKPi(process):
    if process == "spruce":
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
        omegac0 = cbaryon_builder.make_omegac0_to_pkkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None
        )
    elif process == "hlt2":
        pion = basic_builder.make_soft_pions()
        omegac0 = cbaryon_builder.make_omegac0_to_pkkpi()
    line_alg = b_builder.make_lb2chh(
        particles=[pion, pion, omegac0],
        descriptors=["[Lambda_b0 -> pi+ pi- Omega_c0]cc"],
    )
    return line_alg


##############################################################
# Form the Lambda_b (Xi_b0) --> Omega_c0 K+ pi-, Omega_c0 --> p K- K- pi+
##############################################################
@check_process
def make_LbToOmc0KPi_Omc0ToPKKPi(process):
    if process == "spruce":
        pion = basic_builder.make_soft_pions(pi_pidk_max=None)
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
        omegac0 = cbaryon_builder.make_omegac0_to_pkkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None
        )
    elif process == "hlt2":
        pion = basic_builder.make_soft_pions()
        kaon = basic_builder.make_soft_kaons()
        omegac0 = cbaryon_builder.make_omegac0_to_pkkpi()
    line_alg = b_builder.make_lb2chh(
        particles=[kaon, pion, omegac0],
        descriptors=[
            "[Lambda_b0 -> K+ pi- Omega_c0]cc",
            "[Lambda_b0 -> K- pi+ Omega_c0]cc",
        ],
    )
    return line_alg


##############################################################
# Form the Lambda_b (Xi_b0) --> Omega_c0 K+ K-, Omega_c0 --> p K- K- pi+
##############################################################
@check_process
def make_LbToOmc0KK_Omc0ToPKKPi(process):
    if process == "spruce":
        kaon = basic_builder.make_soft_kaons(k_pidk_min=None)
        omegac0 = cbaryon_builder.make_omegac0_to_pkkpi(
            pi_pidk_max=None, k_pidk_min=None, p_pidp_min=None
        )
    elif process == "hlt2":
        kaon = basic_builder.make_soft_kaons()
        omegac0 = cbaryon_builder.make_omegac0_to_pkkpi()
    line_alg = b_builder.make_lb2chh(
        particles=[kaon, kaon, omegac0], descriptors=["[Lambda_b0 -> K+ K- Omega_c0]cc"]
    )
    return line_alg


##############################################################
# Form the Lb --> Lc+ KS K-, Lc+ --> p K- pi+
##############################################################
@check_process
def make_LbToLcpKsDDK_LcpToPKPi(process):
    ks_dd = basic_builder.make_ks_DD()
    kaon = basic_builder.make_soft_kaons()
    lc = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_b2x(
        particles=[lc, ks_dd, kaon],
        descriptors=[
            "Lambda_b0 -> Lambda_c+ KS0 K-",
            "Lambda_b~0 -> Lambda_c~- KS0 K+",
        ],
    )
    return line_alg


@check_process
def make_LbToLcpKsLLK_LcpToPKPi(process):
    ks_ll = basic_builder.make_ks_LL()
    kaon = basic_builder.make_soft_kaons()
    lc = cbaryon_builder.make_lc_to_pkpi()
    line_alg = b_builder.make_b2x(
        particles=[lc, ks_ll, kaon],
        descriptors=[
            "Lambda_b0 -> Lambda_c+ KS0 K-",
            "Lambda_b~0 -> Lambda_c~- KS0 K+",
        ],
    )
    return line_alg


#######################################################################
# Form the Lambda_b0 -> Lambda_c+ p~ Lambda0, Lambda_c+ --> p K- pi+, and Lambda0 -> p pi-
######################################################################
@check_process
def make_LbToLcpPbarLambdaLL_LcpToPKPi(process):
    if process == "spruce":
        cbaryon = cbaryon_builder.make_lc_to_pkpi()
    elif process == "hlt2":
        cbaryon = cbaryon_builder.make_tight_lc_to_pkpi()
    proton = basic_builder.make_protons()
    lambdas = basic_builder.make_lambda_LL(pt_min=250 * MeV, p_min=6 * GeV)
    line_alg = b_builder.make_lb(
        particles=[cbaryon, lambdas, proton],
        descriptors=["[Lambda_b0 -> Lambda_c+ Lambda0 p~-]cc"],
        sum_pt_min=2.1 * GeV,
        bpvdira_min=0.99,
    )
    return line_alg


#######################################################################
# Form the Lambda_b0 -> Lambda_c+ p~ Lambda0, Lambda_c+ --> p K- pi+, and Lambda0 -> p pi-
######################################################################
@check_process
def make_LbToLcpPbarLambdaDD_LcpToPKPi(process):
    if process == "spruce":
        cbaryon = cbaryon_builder.make_lc_to_pkpi()
    elif process == "hlt2":
        cbaryon = cbaryon_builder.make_tight_lc_to_pkpi()
    proton = basic_builder.make_protons()
    lambdas = basic_builder.make_lambda_DD(pt_min=300 * MeV, p_min=6 * GeV)
    line_alg = b_builder.make_lb(
        particles=[cbaryon, lambdas, proton],
        descriptors=["[Lambda_b0 -> Lambda_c+ Lambda0 p~-]cc"],
        sum_pt_min=2.1 * GeV,
        bpvdira_min=0.99,
    )
    return line_alg
