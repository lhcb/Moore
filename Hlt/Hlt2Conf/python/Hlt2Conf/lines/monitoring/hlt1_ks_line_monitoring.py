###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Monitoring lines to validate and assess the performance (efficiency and S/B
ratio for decays with KS particles) of the new HLT1 KS line, compared to the
[Two]TrackMVA lines.

All lines aim at collecting nearly HLT2-unbiased samples of decays with very
loose kinematical selections.

  1. Hlt2Monitoring_DpToPimPipPip_PR_Hlt1Monitoring
  2. Hlt2Monitoring_DpToKsPip_PR_Hlt1Monitoring
  3. Hlt2Monitoring_InclKsKs_PR_Hlt1Monitoring: Searches for X0 -> KS0 KS0
      candidates, in a large X0 mass window.
  4. Hlt2Monitoring_PassthroughKs_PR_Hlt1Monitoring: To test the
      TwoTrackKs line.
  5. Hlt2Monitoring_PassthroughV0_DD_PR_Hlt1Monitoring: Collects inclusive
      V0 -> pi+ pi- downstream candidates, without applying any selections on
      the 2-track candidate. The events is required to trigger the HLT1
      passthrough line, avoiding any possible bias by HLT1. It is aimed at
      collecting a background sample for studies on downstream candidates.

TODO: all lines are currently missing HLT1 filtering (waiting for ThOr filter).
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import make_down_pions, make_long_pions

from Hlt2Conf.lines.charm.particle_properties import _PION_M
from Hlt2Conf.lines.charm.prefilters import charm_prefilters

#####################################################################
### Shortcuts for filters and builders used throughout the module ###
#####################################################################


def _hlt1validation_pions():
    return ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 400 * MeV,
                F.P > 2 * GeV,
                # trchi2dof_max=2.5, # TODO
                F.MINIPCHI2CUT(IPChi2Cut=15.0, Vertices=make_pvs()),
            ),
        ),
    )


#########################
### Lines definition  ###
#########################

all_lines = {}


@register_line_builder(all_lines)
def dptopimpippip_line(
    name="Hlt2Monitoring_DpToPimPipPip_PR_Hlt1Monitoring",
    prescale=2e-4,
    persistreco=True,
):
    dplus = ParticleCombiner(
        [_hlt1validation_pions(), _hlt1validation_pions(), _hlt1validation_pions()],
        DecayDescriptor="[D+ -> pi- pi+ pi+]cc",
        name="Monitoring_Hlt1KsLine_DpToPimPipPip",
        Combination12Cut=F.MASS < 2000 * MeV - _PION_M,
        CombinationCut=F.require_all(
            F.MASS > 1740 * MeV,
            F.MASS < 2000 * MeV,
            F.SUM(F.PT) > 1500 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS > 1770 * MeV,
            F.MASS < 1970 * MeV,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dplus],
        prescale=prescale,
        persistreco=persistreco,
        # hlt1_filter_code=
        # "Hlt1TrackMVADecision|Hlt1TwoTrackMVADecision|Hlt1TwoTrackKsDecision" # TODO
    )


@register_line_builder(all_lines)
def dptokspip_line(
    name="Hlt2Monitoring_DpToKsPip_PR_Hlt1Monitoring", prescale=2e-3, persistreco=True
):
    ks0 = ParticleCombiner(
        [_hlt1validation_pions(), _hlt1validation_pions()],
        DecayDescriptor="KS0 -> pi+ pi-",
        name="Monitoring_Hlt1KsLine_Ks_for_DpToKsPip",
        CombinationCut=F.require_all(
            F.MASS > 430 * MeV,
            F.MASS < 570 * MeV,
            F.SUM(F.PT) > 200 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS > 450 * MeV,
            F.MASS < 550 * MeV,
            F.CHI2DOF < 10,
        ),
    )

    dplus = ParticleCombiner(
        [ks0, _hlt1validation_pions()],
        DecayDescriptor="[D+ -> KS0 pi+]cc",
        name="Monitoring_Hlt1KsLine_DpToKsPip",
        CombinationCut=F.require_all(
            F.MASS > 1740 * MeV,
            F.MASS < 2000 * MeV,
            F.SUM(F.PT) > 1500 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS > 1770 * MeV,
            F.MASS < 1970 * MeV,
        ),
    )
    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [dplus],
        prescale=prescale,
        persistreco=persistreco,
        # hlt1_filter_code=
        # "Hlt1TrackMVADecision|Hlt1TwoTrackMVADecision|Hlt1TwoTrackKsDecision" # TODO
    )


@register_line_builder(all_lines)
def passthrough_ks_line(
    name="Hlt2Monitoring_PassthroughKs_PR_Hlt1Monitoring",
    prescale=1e-4,
    persistreco=True,
):
    pions = ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 400 * MeV,
                F.P > 2 * GeV,
                # trchi2dof_max=2.5,  # TODO
                F.MINIPCHI2CUT(IPChi2Cut=20.0, Vertices=make_pvs()),
            ),
        ),
    )

    ks0 = ParticleCombiner(
        [pions, pions],
        DecayDescriptor="KS0 -> pi+ pi-",
        name="Monitoring_Hlt1KsLine_Ks_for_Passthrough",
        CombinationCut=F.require_all(
            F.MASS > 430 * MeV,
            F.MASS < 570 * MeV,
            F.SUM(F.PT) > 250 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS > 455 * MeV,
            F.MASS < 545 * MeV,
            F.CHI2DOF < 25,
        ),
    )

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [ks0],
        prescale=prescale,
        persistreco=persistreco,
        # hlt1_filter_code=
        # "Hlt1TrackMVADecision|Hlt1TwoTrackMVADecision|Hlt1TwoTrackKsDecision|Hlt1TwoKsDecision" # TODO
    )


@register_line_builder(all_lines)
def inclksks_line(
    name="Hlt2Monitoring_InclKsKs_PR_Hlt1Monitoring", prescale=1e-4, persistreco=True
):
    ks0 = ParticleCombiner(
        [make_long_pions(), make_long_pions()],
        DecayDescriptor="KS0 -> pi+ pi-",
        name="Monitoring_Hlt1KsLine_Ks_for_InclKsKs",
        CombinationCut=F.require_all(
            F.MASS > 430 * MeV,
            F.MASS < 570 * MeV,
            F.SUM(F.PT) > 200 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS > 455 * MeV,
            F.MASS < 545 * MeV,
            F.CHI2DOF < 10,
        ),
    )

    x0 = ParticleCombiner(
        [ks0, ks0],
        DecayDescriptor="D0 -> KS0 KS0",
        name="Monitoring_Hlt1KsLine_InclKsKs",
        CombinationCut=F.require_all(
            F.MASS > 1350 * MeV,
            F.MASS < 5550 * MeV,
            F.SUM(F.PT) > 500 * MeV,
        ),
        CompositeCut=F.require_all(
            F.MASS > 1400 * MeV,
            F.MASS < 5500 * MeV,
        ),
    )

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [x0],
        prescale=prescale,
        persistreco=persistreco,
        # hlt1_filter_code=
        # "Hlt1TrackMVADecision|Hlt1TwoTrackMVADecision|Hlt1TwoTrackKsDecision|Hlt1TwoKsDecision" # TODO
    )


@register_line_builder(all_lines)
def passthrough_v0_downstream_line(
    name="Hlt2Monitoring_PassthroughV0_DD_PR_Hlt1Monitoring",
    prescale=1e-6,
    persistreco=True,
):
    # TODO: Fix the prescale once Hlt1PassthroughDecision prescale is defined

    pions = ParticleFilter(
        make_down_pions(),
        F.FILTER(
            F.require_all(
                F.PT > 200 * MeV,
                F.P > 1 * GeV,
                F.MINIPCHI2CUT(IPChi2Cut=5.0, Vertices=make_pvs()),
            ),
        ),
    )

    v0 = ParticleCombiner(
        [pions, pions],
        DecayDescriptor="KS0 -> pi+ pi-",
        name="Monitoring_Hlt1KsLine_Ks_DD_for_Passthrough",
        CombinationCut=F.require_all(
            F.MASS > 0 * MeV,  # needed to avoid combiner error
            F.MASS < 1000000 * MeV,
        ),
        CompositeCut=F.require_all(F.MASS > 0 * MeV, F.MASS < 1000000 * MeV),
    )  # needed to avoid combiner error

    return Hlt2Line(
        name=name,
        algs=charm_prefilters() + [v0],
        prescale=prescale,
        persistreco=persistreco,
    )
    # hlt1_filter_code="Hlt1PassthroughDecision")
    # TODO: add filter once Hlt1PassthroughDecision prescale is defined
