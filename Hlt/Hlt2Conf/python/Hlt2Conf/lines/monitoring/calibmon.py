###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Configuration of Hlt2 lines for the CalibMon task
Uses the following postscaled Hlt2 lines:
  - Hlt2CalibMon_Passthrough
  - Hlt2CalibMon_DstToD0Pi
  - Hlt2CalibMon_L0ToPPi_LL
  - Hlt2CalibMon_BpToJpsiKp_JpsiToEmEp_Tagged
  - Hlt2CalibMon_JpsiToMupMum_Detached_Tagged
  - Hlt2CalibMon_DiMuon_VeloMuon
  - Hlt2CalibMon_DiMuon_Downstream
  - Hlt2CalibMon_DiMuon_UToMuon
  - Hlt2CalibMon_DiMuon_SeedMuon
  - Hlt2CalibMon_KshortVeloLongAllHlt1
"""

import Functors as F
from DecayTreeFitter import DecayTreeFitter
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, TeV, mm, ps
from GaudiKernel.SystemOfUnits import micrometer as um
from Moore.config import Hlt2Line, register_line_builder
from PyConf.Algorithms import (
    FlattenDecayTree,  # FunctionalChargedProtoParticleMaker,
    HltRoutingBitsFilter,
    KSLongVeloFilter,
    KSVelo2LongEfficiencyMonitor,
    LHCbIDOverlapRelationTable,
    MuonChamberMonitor,
    MuonProbeToLongMatcher,
    MuonRawInUpgradeToHits,
    OdinTypesFilter,
    ThOrParticleSelection,
    VoidFilter,
)
from PyConf.application import default_raw_banks, make_odin
from PyConf.control_flow import CompositeNode, NodeLogic
from PyConf.reading import get_decreports

# To uncomment, see https://gitlab.cern.ch/lhcb/Moore/-/merge_requests/3729/diffs#note_8270038
# from Allen.config import setup_allen_non_event_data_service
# from AllenConf.enum_types import TrackingType
# from AllenConf.hlt1_reconstruction import hlt1_reconstruction
from RecoConf.algorithms_thor import (
    ParticleCombiner,
    ParticleContainersMerger,
    ParticleFilter,
)
from RecoConf.event_filters import require_pvs
from RecoConf.muonid import make_muon_hits
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    make_ismuon_long_muon,
    # _make_particles, get_all_track_selector,
    make_long_electrons_with_brem,
    make_long_kaons,
    make_long_muons,
    make_long_pions,
    make_long_protons,
)
from SelAlgorithms.monitoring import histogram_1d, histogram_2d, histogram_axis, monitor

from Hlt2Conf.hlt1_tistos import hlt1_tos_on_any_filter
from Hlt2Conf.lines.trackeff import KSVeloLong
from Hlt2Conf.probe_muons import (
    make_downstream_muons,
    make_muonut_muons,
    make_seed_muons,
    make_velomuon_muons,
)

# from RecoConf.hlt1_allen import make_converted_tracks


def _calibmon_filters(pvs):
    rb_filters = []
    try:
        from Moore import options

        if options.input_type == "Online":
            rb_filters = [
                HltRoutingBitsFilter(
                    RawBanks=default_raw_banks("HltRoutingBits"),
                    RequireMask=((1 << 17) | (1 << 14), 0, 0),
                    PassOnError=False,
                )
            ]
    except AttributeError:
        # With LbExec, Moore.options doesn't make sense...
        pass
    odin_bb_filter = OdinTypesFilter(ODIN=make_odin(), BXTypes=["BeamCrossing"])
    return rb_filters + [odin_bb_filter, require_pvs(pvs)]


def _make_hist_list(hist_dict, line_name):
    # rearrange histos so that the the monitoring runs only once per input DataHandle
    histos = {}
    hist_list = []
    for histname, params in hist_dict.items():
        comb_name = params["input"].location.replace("/", "__")
        ax = lambda xy: histogram_axis(
            functor=params[f"variable_{xy}"],
            label=params[f"label_{xy}"],
            bins=params[f"bins_{xy}"],
            range=params[f"range_{xy}"],
        )
        if "variable_x" in params:
            hist = histogram_2d(
                name=f"/{line_name}/{histname}",
                title=line_name,
                xaxis=ax("x"),
                yaxis=ax("y"),
            )
        else:
            hist = histogram_1d(
                functor=params["variable"],
                name=f"/{line_name}/{histname}",
                label=params["label"],
                bins=params["bins"],
                range=params["range"],
                title=line_name,
            )
        if comb_name in histos.keys():
            histos[comb_name]["histograms"].append(hist)
        else:
            histos[comb_name] = dict(histograms=[hist], data_handle=params["input"])
    for params in histos.values():
        hist_list.append(
            monitor(data=params["data_handle"], histograms=params["histograms"])
        )
    return hist_list


# define a few shortcuts to make the code better readable
def _CHILDMASS(i=1):
    return F.CHILD(i, F.MASS)


def _CHILDP(i=1):
    return F.CHILD(i, F.P)


def _DELTAMASS(i=1):
    return F.MASS - F.CHILD(i, F.MASS)


def _CHILDPID_K(i=1):
    return F.CHILD(i, F.PID_K)


def _CHILDPID_P(i=1):
    return F.CHILD(i, F.PID_P)


def _GCHILDPID_K(i=1, j=1):
    return F.CHILD(i, _CHILDPID_K(j))


def _GCHILDPID_P(i=1, j=1):
    return F.CHILD(i, _CHILDPID_P(j))


def _CHILDPID_PK(i=1):
    return F.CHILD(i, F.PID_P) - F.CHILD(i, F.PID_K)


def _GCHILDPID_PK(i=1, j=1):
    return F.CHILD(i, _CHILDPID_P(j)) - F.CHILD(i, _CHILDPID_K(j))


def _GCHILDP(i=1, j=1):
    return F.CHILD(i, F.CHILD(j, F.P))


def _GCHILDETA(i=1, j=1):
    return F.CHILD(i, F.CHILD(j, F.ETA))


def _MIPCHI2_MIN(cut, pvs=make_pvs):
    return F.MINIPCHI2CUT(IPChi2Cut=cut, Vertices=pvs())


def _HLT1_DEC(lines):
    return F.DECREPORTS_FILTER(get_decreports("Hlt1"), lines)


all_lines = {}


@register_line_builder(all_lines)
def calibmon_passthrough(name="Hlt2CalibMon_Passthrough"):
    pvs = make_pvs()

    def _passthrough_global_monitor(postfix):
        _hlt1_dec_hist = lambda list_of_hlt1_lines: [
            histogram_1d(
                f"/{name}/{l}_{postfix}",
                f"Monitor_{name}_{l}_{postfix}",
                _HLT1_DEC([f"{l}Decision"]),
                2,
                (0, 2),
                f"{l}Decision",
            )
            for l in list_of_hlt1_lines
        ]
        return monitor(
            name=f"GlobalMonitor__{name}_{postfix}",
            histograms=[
                histogram_1d(
                    f"/{name}/nPVs_{postfix}",
                    f"Monitor_{name}_n_pvs_{postfix}",
                    F.SIZE(pvs),
                    25,
                    (0, 25),
                    "nPVs",
                ),
                histogram_1d(
                    f"/{name}/BXType_{postfix}",
                    f"Monitor_{name}_BXType_{postfix}",
                    F.CAST_TO_INT @ F.BUNCHCROSSING_TYPE(make_odin()),
                    4,
                    (0, 4),
                    "BXType",
                ),
                histogram_1d(
                    f"/{name}/events_{postfix}",
                    f"Monitor_{name}_events_{postfix}",
                    F.ALL,
                    2,
                    (0, 2),
                    "# events",
                ),
            ]
            + _hlt1_dec_hist(
                [
                    "Hlt1ODINLumi",
                    "Hlt1TrackMuonMVADecision",
                    "Hlt1TrackMVADecision",
                    "Hlt1TwoTrackMVADecision",
                    "Hlt1OneMuonTrackLineDecision",
                    "Hlt1DiMuonHighMassDecision",
                    "Hlt1DiMuonNoIPDecision",
                    "Hlt1PassthroughDecision",
                    "Hlt1GECPassthroughDecision",
                    "Hlt1D2KPiDecision",
                    "Hlt1Dst2D0PiDecision",
                    "Hlt1SingleHighPtMuonNoMuIDDecision",
                    "Hlt1DetJpsiToMuMuPosTagLineDecision",
                    "Hlt1DetJpsiToMuMuNegTagLineDecision",
                    "Hlt1SingleHighPtMuonDecision",
                    "Hlt1DisplacedDiMuonDecision",
                    "Hlt1TrackElectronMVADecision",
                    "Hlt1SingleHighPtElectronDecision",
                    "Hlt1DisplacedDielectronDecision",
                    "Hlt1SingleHighEtDecision",
                ]
            ),
        )

    return Hlt2Line(
        name=name,
        algs=[_passthrough_global_monitor("inclusive")]
        + _calibmon_filters(pvs)
        + [_passthrough_global_monitor("prefiltered")],
        postscale=0,
        persistreco=True,
        monitoring_variables=(),
    )


@register_line_builder(all_lines)
def calibmon_dst_to_d0_pi(name="Hlt2CalibMon_DstToD0Pi"):
    pvs = make_pvs()
    kaons = ParticleFilter(
        make_long_kaons(),
        F.FILTER(
            F.require_all(
                F.P > 2 * GeV,
                F.PT > 250 * MeV,
                F.CHI2DOF < 3,
                _MIPCHI2_MIN(16),
                F.GHOSTPROB < 0.5,
            )
        ),
    )
    pions = ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(
                F.P > 2 * GeV,
                F.PT > 250 * MeV,
                F.CHI2DOF < 3,
                _MIPCHI2_MIN(16),
                F.GHOSTPROB < 0.5,
            )
        ),
    )
    slow_pions = ParticleFilter(
        make_long_pions(),
        F.FILTER(
            F.require_all(
                F.P > 1 * GeV, F.PT > 100 * MeV, F.CHI2DOF < 3, F.GHOSTPROB < 0.5
            )
        ),
    )
    d0s = ParticleCombiner(
        [kaons, pions],
        name="CalibMon_D0ToKPi_Combiner_{hash}",
        DecayDescriptor="[D0 -> K- pi+]cc",
        CombinationCut=F.require_all(
            in_range(1769.84 * MeV, F.MASS, 1959.84 * MeV),
            F.PT > 1500 * MeV,
            F.MAXDOCACUT(80 * um),
        ),
        CompositeCut=F.require_all(
            in_range(1794 * MeV, F.MASS, 1934 * MeV),
            F.CHI2DOF < 10,
            F.BPVFDCHI2(pvs) > 49,
            F.BPVDIRA(pvs) > 0.99997,
            (F.MASSWITHHYPOTHESES(("pi-", "K+")) < 1839.84 * MeV)
            | (F.MASSWITHHYPOTHESES(("pi-", "K+")) > 1889.84 * MeV),
            (F.MASSWITHHYPOTHESES(("pi-", "pi+")) < 1839.84 * MeV)
            | (F.MASSWITHHYPOTHESES(("pi-", "pi+")) > 1889.84 * MeV),
            (F.CHILD(1, F.PT) > 1 * GeV) | (F.CHILD(2, F.PT) > 1 * GeV),
        ),
    )
    dsts = ParticleCombiner(
        [d0s, slow_pions],
        name="CalibMon_DstToD0Pi_Combiner_{hash}",
        DecayDescriptor="[D*(2010)+ -> D0 pi+]cc",
        CombinationCut=in_range(135 * MeV, _DELTAMASS(), 160 * MeV),
        CompositeCut=F.require_all(
            in_range(140 * MeV, _DELTAMASS(), 155 * MeV), F.CHI2DOF < 10
        ),
    )

    # We want to fill a bunch of histograms. Define a partial dictionary for Lambda mass plots, and bin edges for differential PID efficiencies
    dst_mass_part_dict = {
        "input": dsts,
        "bins_x": 60,
        "bins_y": 60,
        "range_x": (1794.0, 1934.0),
        "range_y": (140.0, 155.0),
        "label_x": "m (K pi) [MeV]",
        "label_y": "m (D*) - m (D0) [MeV]",
    }
    p_bins = (3, 30, 60, 100)
    eta_bins = (2, 3.5, 4.9)
    pid_bins = (-1000, -10, -5, 0, 5, 10)
    # This is the dict we will use to configure the monitoring alg. Define some 1D histograms at initialization
    dst_hist_dict = {
        "D_PT": {
            "input": d0s,
            "variable": F.PT,
            "label": "pT (D0) [MeV]",
            "bins": 100,
            "range": (0, 40000),
        },
        "D_BPVIPCHI2": {
            "input": d0s,
            "variable": F.BPVIPCHI2(pvs),
            "label": "FD chi^2 (D0)",
            "bins": 100,
            "range": (0, 5000),
        },
        "K_ETA": {
            "input": kaons,
            "variable": F.ETA,
            "label": "eta (K)",
            "bins": 100,
            "range": (2, 5),
        },
        "K_P": {
            "input": kaons,
            "variable": F.P,
            "label": "p (K) [MeV]",
            "bins": 100,
            "range": (0, 250000),
        },
        "K_PT": {
            "input": kaons,
            "variable": F.PT,
            "label": "pT (K) [MeV]",
            "bins": 100,
            "range": (0, 20000),
        },
        "K_TCHI2DOF": {
            "input": kaons,
            "variable": F.CHI2DOF,
            "label": "Track chi^2/dof (K)",
            "bins": 100,
            "range": (0, 4),
        },
        "K_GHOSTPROB": {
            "input": kaons,
            "variable": F.GHOSTPROB,
            "label": "GhostProb (K)",
            "bins": 100,
            "range": (0, 1),
        },
        "pi_ETA": {
            "input": pions,
            "variable": F.ETA,
            "label": "eta (pi)",
            "bins": 100,
            "range": (2, 5),
        },
        "pi_P": {
            "input": pions,
            "variable": F.P,
            "label": "p (pi) [MeV]",
            "bins": 100,
            "range": (0, 300000),
        },
        "pi_PT": {
            "input": pions,
            "variable": F.PT,
            "label": "pT (pi) [MeV]",
            "bins": 100,
            "range": (0, 25000),
        },
        "pi_TCHI2DOF": {
            "input": pions,
            "variable": F.CHI2DOF,
            "label": "Track chi^2/dof (pi)",
            "bins": 100,
            "range": (0, 4),
        },
        "pi_GHOSTPROB": {
            "input": pions,
            "variable": F.GHOSTPROB,
            "label": "GhostProb (pi)",
            "bins": 100,
            "range": (0, 1),
        },
    }

    for pid_cut in pid_bins:
        dst_hist_dict[f"integrated__D_MvsDst_D0_DeltaM__K_PID_K-{pid_cut}"] = dict(
            dst_mass_part_dict,
            variable_x=_CHILDMASS() * (_GCHILDPID_K() > pid_cut),
            variable_y=_DELTAMASS() * (_GCHILDPID_K() > pid_cut),
        )
        dst_hist_dict[f"integrated__D_MvsDst_D0_DeltaM__K_PID_P-{pid_cut}"] = dict(
            dst_mass_part_dict,
            variable_x=_CHILDMASS()
            * ((_GCHILDPID_P() > pid_cut) & (_GCHILDP() > 10 * GeV)),
            variable_y=_DELTAMASS()
            * ((_GCHILDPID_P() > pid_cut) & (_GCHILDP() > 10 * GeV)),
        )
        dst_hist_dict[f"integrated__D_MvsDst_D0_DeltaM__K_PID_PK-{pid_cut}"] = dict(
            dst_mass_part_dict,
            variable_x=_CHILDMASS()
            * ((_GCHILDPID_PK() > pid_cut) & (_GCHILDP() > 10 * GeV)),
            variable_y=_DELTAMASS()
            * ((_GCHILDPID_PK() > pid_cut) & (_GCHILDP() > 10 * GeV)),
        )
        dst_hist_dict[f"integrated__D_MvsDst_D0_DeltaM__pi_PID_K-{pid_cut}"] = dict(
            dst_mass_part_dict,
            variable_x=_CHILDMASS() * (_GCHILDPID_K(j=2) > pid_cut),
            variable_y=_DELTAMASS() * (_GCHILDPID_K(j=2) > pid_cut),
        )
        dst_hist_dict[f"integrated__D_MvsDst_D0_DeltaM__pi_PID_P-{pid_cut}"] = dict(
            dst_mass_part_dict,
            variable_x=_CHILDMASS() * (_GCHILDPID_P(j=2) > pid_cut),
            variable_y=_DELTAMASS() * (_GCHILDPID_P(j=2) > pid_cut),
        )
        dst_hist_dict[f"integrated__D_MvsDst_D0_DeltaM__pi_PID_PK-{pid_cut}"] = dict(
            dst_mass_part_dict,
            variable_x=_CHILDMASS() * (_GCHILDPID_PK(j=2) > pid_cut),
            variable_y=_DELTAMASS() * (_GCHILDPID_PK(j=2) > pid_cut),
        )
        for p_idx in range(0, len(p_bins) - 1):
            dst_hist_dict[
                f"K_P-{p_bins[p_idx]}-{p_bins[p_idx + 1]}__D_MvsDst_D0_DeltaM__K_PID_K-{pid_cut}"
            ] = dict(
                dst_mass_part_dict,
                variable_x=_CHILDMASS()
                * (
                    in_range(p_bins[p_idx] * GeV, _GCHILDP(), p_bins[p_idx + 1] * GeV)
                    & (_GCHILDPID_K() > pid_cut)
                ),
                variable_y=_DELTAMASS()
                * (
                    in_range(p_bins[p_idx] * GeV, _GCHILDP(), p_bins[p_idx + 1] * GeV)
                    & (_GCHILDPID_K() > pid_cut)
                ),
            )
            dst_hist_dict[
                f"K_P-{p_bins[p_idx]}-{p_bins[p_idx + 1]}__D_MvsDst_D0_DeltaM__K_PID_P-{pid_cut}"
            ] = dict(
                dst_mass_part_dict,
                variable_x=_CHILDMASS()
                * (
                    in_range(p_bins[p_idx] * GeV, _GCHILDP(), p_bins[p_idx + 1] * GeV)
                    & (_GCHILDPID_P() > pid_cut)
                    & (_GCHILDP() > 10 * GeV)
                ),
                variable_y=_DELTAMASS()
                * (
                    in_range(p_bins[p_idx] * GeV, _GCHILDP(), p_bins[p_idx + 1] * GeV)
                    & (_GCHILDPID_P() > pid_cut)
                    & (_GCHILDP() > 10 * GeV)
                ),
            )
            dst_hist_dict[
                f"K_P-{p_bins[p_idx]}-{p_bins[p_idx + 1]}__D_MvsDst_D0_DeltaM__K_PID_PK-{pid_cut}"
            ] = dict(
                dst_mass_part_dict,
                variable_x=_CHILDMASS()
                * (
                    in_range(p_bins[p_idx] * GeV, _GCHILDP(), p_bins[p_idx + 1] * GeV)
                    & (_GCHILDPID_PK() > pid_cut)
                    & (_GCHILDP() > 10 * GeV)
                ),
                variable_y=_DELTAMASS()
                * (
                    in_range(p_bins[p_idx] * GeV, _GCHILDP(), p_bins[p_idx + 1] * GeV)
                    & (_GCHILDPID_PK() > pid_cut)
                    & (_GCHILDP() > 10 * GeV)
                ),
            )
            dst_hist_dict[
                f"pi_P-{p_bins[p_idx]}-{p_bins[p_idx + 1]}__D_MvsDst_D0_DeltaM__pi_PID_K-{pid_cut}"
            ] = dict(
                dst_mass_part_dict,
                variable_x=_CHILDMASS()
                * (
                    in_range(
                        p_bins[p_idx] * GeV, _GCHILDP(j=2), p_bins[p_idx + 1] * GeV
                    )
                    & (_GCHILDPID_K(j=2) > pid_cut)
                ),
                variable_y=_DELTAMASS()
                * (
                    in_range(
                        p_bins[p_idx] * GeV, _GCHILDP(j=2), p_bins[p_idx + 1] * GeV
                    )
                    & (_GCHILDPID_K(j=2) > pid_cut)
                ),
            )
            dst_hist_dict[
                f"pi_P-{p_bins[p_idx]}-{p_bins[p_idx + 1]}__D_MvsDst_D0_DeltaM__pi_PID_P-{pid_cut}"
            ] = dict(
                dst_mass_part_dict,
                variable_x=_CHILDMASS()
                * (
                    in_range(
                        p_bins[p_idx] * GeV, _GCHILDP(j=2), p_bins[p_idx + 1] * GeV
                    )
                    & (_GCHILDPID_P(j=2) > pid_cut)
                ),
                variable_y=_DELTAMASS()
                * (
                    in_range(
                        p_bins[p_idx] * GeV, _GCHILDP(j=2), p_bins[p_idx + 1] * GeV
                    )
                    & (_GCHILDPID_P(j=2) > pid_cut)
                ),
            )
            dst_hist_dict[
                f"pi_P-{p_bins[p_idx]}-{p_bins[p_idx + 1]}__D_MvsDst_D0_DeltaM__pi_PID_PK-{pid_cut}"
            ] = dict(
                dst_mass_part_dict,
                variable_x=_CHILDMASS()
                * (
                    in_range(
                        p_bins[p_idx] * GeV, _GCHILDP(j=2), p_bins[p_idx + 1] * GeV
                    )
                    & (_GCHILDPID_PK(j=2) > pid_cut)
                ),
                variable_y=_DELTAMASS()
                * (
                    in_range(
                        p_bins[p_idx] * GeV, _GCHILDP(j=2), p_bins[p_idx + 1] * GeV
                    )
                    & (_GCHILDPID_PK(j=2) > pid_cut)
                ),
            )
        for eta_idx in range(0, len(eta_bins) - 1):
            dst_hist_dict[
                f"K_ETA-{eta_bins[eta_idx]}-{eta_bins[eta_idx + 1]}__D_MvsDst_D0_DeltaM__K_PID_K-{pid_cut}"
            ] = dict(
                dst_mass_part_dict,
                variable_x=_CHILDMASS()
                * (
                    in_range(p_bins[p_idx] * GeV, _GCHILDETA(), p_bins[p_idx + 1] * GeV)
                    & (_GCHILDPID_K() > pid_cut)
                ),
                variable_y=_DELTAMASS()
                * (
                    in_range(p_bins[p_idx] * GeV, _GCHILDETA(), p_bins[p_idx + 1] * GeV)
                    & (_GCHILDPID_K() > pid_cut)
                ),
            )
            dst_hist_dict[
                f"K_ETA-{eta_bins[eta_idx]}-{eta_bins[eta_idx + 1]}__D_MvsDst_D0_DeltaM__K_PID_P-{pid_cut}"
            ] = dict(
                dst_mass_part_dict,
                variable_x=_CHILDMASS()
                * (
                    in_range(p_bins[p_idx] * GeV, _GCHILDETA(), p_bins[p_idx + 1] * GeV)
                    & (_GCHILDPID_P() > pid_cut)
                    & (_GCHILDP() > 10 * GeV)
                ),
                variable_y=_DELTAMASS()
                * (
                    in_range(p_bins[p_idx] * GeV, _GCHILDETA(), p_bins[p_idx + 1] * GeV)
                    & (_GCHILDPID_P() > pid_cut)
                    & (_GCHILDP() > 10 * GeV)
                ),
            )
            dst_hist_dict[
                f"K_ETA-{eta_bins[eta_idx]}-{eta_bins[eta_idx + 1]}__D_MvsDst_D0_DeltaM__K_PID_PK-{pid_cut}"
            ] = dict(
                dst_mass_part_dict,
                variable_x=_CHILDMASS()
                * (
                    in_range(p_bins[p_idx] * GeV, _GCHILDETA(), p_bins[p_idx + 1] * GeV)
                    & (_GCHILDPID_PK() > pid_cut)
                    & (_GCHILDP() > 10 * GeV)
                ),
                variable_y=_DELTAMASS()
                * (
                    in_range(p_bins[p_idx] * GeV, _GCHILDETA(), p_bins[p_idx + 1] * GeV)
                    & (_GCHILDPID_PK() > pid_cut)
                    & (_GCHILDP() > 10 * GeV)
                ),
            )
            dst_hist_dict[
                f"pi_ETA-{eta_bins[eta_idx]}-{eta_bins[eta_idx + 1]}__D_MvsDst_D0_DeltaM__pi_PID_K-{pid_cut}"
            ] = dict(
                dst_mass_part_dict,
                variable_x=_CHILDMASS()
                * (
                    in_range(
                        p_bins[p_idx] * GeV, _GCHILDETA(j=2), p_bins[p_idx + 1] * GeV
                    )
                    & (_GCHILDPID_K(j=2) > pid_cut)
                ),
                variable_y=_DELTAMASS()
                * (
                    in_range(
                        p_bins[p_idx] * GeV, _GCHILDETA(j=2), p_bins[p_idx + 1] * GeV
                    )
                    & (_GCHILDPID_K(j=2) > pid_cut)
                ),
            )
            dst_hist_dict[
                f"pi_ETA-{eta_bins[eta_idx]}-{eta_bins[eta_idx + 1]}__D_MvsDst_D0_DeltaM__pi_PID_P-{pid_cut}"
            ] = dict(
                dst_mass_part_dict,
                variable_x=_CHILDMASS()
                * (
                    in_range(
                        p_bins[p_idx] * GeV, _GCHILDETA(j=2), p_bins[p_idx + 1] * GeV
                    )
                    & (_GCHILDPID_P(j=2) > pid_cut)
                ),
                variable_y=_DELTAMASS()
                * (
                    in_range(
                        p_bins[p_idx] * GeV, _GCHILDETA(j=2), p_bins[p_idx + 1] * GeV
                    )
                    & (_GCHILDPID_P(j=2) > pid_cut)
                ),
            )
            dst_hist_dict[
                f"pi_ETA-{eta_bins[eta_idx]}-{eta_bins[eta_idx + 1]}__D_MvsDst_D0_DeltaM__pi_PID_PK-{pid_cut}"
            ] = dict(
                dst_mass_part_dict,
                variable_x=_CHILDMASS()
                * (
                    in_range(
                        p_bins[p_idx] * GeV, _GCHILDETA(j=2), p_bins[p_idx + 1] * GeV
                    )
                    & (_GCHILDPID_PK(j=2) > pid_cut)
                ),
                variable_y=_DELTAMASS()
                * (
                    in_range(
                        p_bins[p_idx] * GeV, _GCHILDETA(j=2), p_bins[p_idx + 1] * GeV
                    )
                    & (_GCHILDPID_PK(j=2) > pid_cut)
                ),
            )

    dst_hist_list = _make_hist_list(dst_hist_dict, name)
    global_hists = monitor(
        name=f"GlobalMonitor__{name}",
        histograms=[
            histogram_1d(
                f"/{name}/nPVS",
                f"Monitor_{name}_n_pvs",
                F.SIZE(pvs),
                10,
                (0, 10),
                "nPVs",
            ),
            histogram_1d(
                f"/{name}/n_candidates",
                f"Monitor_{name}_n_candidates",
                F.SIZE(dsts),
                100,
                (0, 100),
                "NCANDIDATES",
            ),
        ],
    )

    return Hlt2Line(
        name=name,
        algs=_calibmon_filters(pvs) + [dsts, global_hists] + dst_hist_list,
        hlt1_filter_code=[
            "Hlt1TrackMVADecision",
            "Hlt1TwoTrackMVADecision",
            "Hlt1Dst2D0PiDecision",
        ],
        postscale=0,
        persistreco=True,
        monitoring_variables=("pt", "eta", "m", "vchi2"),
    )


@register_line_builder(all_lines)
def calibmon_l0_to_p_pi(name="Hlt2CalibMon_L0ToPPi_LL"):
    pvs = make_pvs()
    protons = ParticleFilter(
        make_long_protons(),
        F.FILTER(
            F.require_all(
                F.P > 2 * GeV,
                F.PT < 1 * TeV,
                F.CHI2DOF < 4,
                _MIPCHI2_MIN(36),
                F.GHOSTPROB < 0.5,
            )
        ),
    )
    pions = ParticleFilter(
        make_long_pions(),
        F.FILTER(F.require_all(F.CHI2DOF < 4, _MIPCHI2_MIN(36), F.GHOSTPROB < 0.5)),
    )
    l0lls = ParticleCombiner(
        [protons, pions],
        name="CalibMon_L0ToPPi_LL_Combiner_{hash}",
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        CombinationCut=F.require_all(in_range(1065.683 * MeV, F.MASS, 1165.683 * MeV)),
        CompositeCut=F.require_all(
            in_range(1095.683 * MeV, F.MASS, 1135.683 * MeV),
            F.CHI2DOF < 30,
            F.MINIPCHI2(pvs) < 50,
            (F.MASSWITHHYPOTHESES(("pi+", "pi-")) < 477.611 * MeV)
            | (F.MASSWITHHYPOTHESES(("pi+", "pi-")) > 517.611 * MeV),
            F.BPVLTIME(pvs) > 2 * ps,
        ),
    )

    # We want to fill a bunch of histograms. Define a partial dictionary for Lambda mass plots, and bin edges for differential PID efficiencies
    l_mass_part_dict = {
        "input": l0lls,
        "bins": 100,
        "range": (1095.683, 1135.683),
        "label": "m (p pi) [MeV]",
    }
    pt_bins = (0, 1.5, 3, 6, 1000)
    p_bins = (3, 30, 60, 100)
    eta_bins = (2, 3.5, 4.9)
    pid_bins = (-1000, -10, -5, 0, 5, 10)
    # This is the dict we will use to configure the monitoring alg. Define some 1D histograms at initialization
    l0_hist_dict = {
        "integrated__L0_M__p_PID_P-neg": dict(
            l_mass_part_dict, variable=F.MASS * (_CHILDPID_P() < 0)
        ),
        "integrated__L0_M__p_PID_PK-neg": dict(
            l_mass_part_dict,
            variable=F.MASS * ((_CHILDPID_PK() < 0) & (_CHILDP() > 10 * GeV)),
        ),
        "L0_PT": {
            "input": l0lls,
            "variable": F.PT,
            "label": "pT (Lambda) [MeV]",
            "bins": 100,
            "range": (0, 2200),
        },
        "L0_BPVIPCHI2": {
            "input": l0lls,
            "variable": F.BPVIPCHI2(pvs),
            "label": "IP chi^2 (Lambda)",
            "bins": 100,
            "range": (0, 60),
        },
        "p_ETA": {
            "input": protons,
            "variable": F.ETA,
            "label": "eta (p)",
            "bins": 100,
            "range": (2, 5),
        },
        "p_P": {
            "input": protons,
            "variable": F.P,
            "label": "p (p) [MeV]",
            "bins": 100,
            "range": (5000, 50000),
        },
        "p_PT": {
            "input": protons,
            "variable": F.PT,
            "label": "pT (p) [MeV]",
            "bins": 100,
            "range": (0, 100000),
        },
        "p_TCHI2DOF": {
            "input": protons,
            "variable": F.CHI2DOF,
            "label": "Track chi^2/dof (p)",
            "bins": 100,
            "range": (0, 5),
        },
        "p_GHOSTPROB": {
            "input": protons,
            "variable": F.GHOSTPROB,
            "label": "GhostProb (p)",
            "bins": 100,
            "range": (0, 1),
        },
    }
    # Update the dict in loops
    for pid_cut in pid_bins:
        l0_hist_dict[f"integrated__L0_M__p_PID_P-{pid_cut}"] = dict(
            l_mass_part_dict, variable=F.MASS * (_CHILDPID_P() > pid_cut)
        )
        l0_hist_dict[f"integrated__L0_M__p_PID_PK-{pid_cut}"] = dict(
            l_mass_part_dict,
            variable=F.MASS * ((_CHILDPID_PK() > pid_cut) & (_CHILDP() > 10 * GeV)),
        )
        for pt_idx in range(0, len(pt_bins) - 1):
            for p_idx in range(0, len(p_bins) - 1):
                l0_hist_dict[
                    f"pT_P-{pt_bins[pt_idx]}-{pt_bins[pt_idx + 1]}__p_P-{p_bins[p_idx]}-{p_bins[p_idx + 1]}__L0_M__p_PID_P-{pid_cut}"
                ] = dict(
                    l_mass_part_dict,
                    variable=F.MASS
                    * (
                        in_range(
                            pt_bins[pt_idx] * GeV,
                            F.CHILD(1, F.PT),
                            pt_bins[pt_idx + 1] * GeV,
                        )
                        & in_range(
                            p_bins[p_idx] * GeV,
                            F.CHILD(1, F.P),
                            p_bins[p_idx + 1] * GeV,
                        )
                        & (_CHILDPID_P() > pid_cut)
                    ),
                )
                l0_hist_dict[
                    f"pT_P-{pt_bins[pt_idx]}-{pt_bins[pt_idx + 1]}__p_P-{p_bins[p_idx]}-{p_bins[p_idx + 1]}__L0_M__p_PID_PK-{pid_cut}"
                ] = dict(
                    l_mass_part_dict,
                    variable=F.MASS
                    * (
                        in_range(
                            pt_bins[pt_idx] * GeV,
                            F.CHILD(1, F.PT),
                            pt_bins[pt_idx + 1] * GeV,
                        )
                        & in_range(
                            p_bins[p_idx] * GeV,
                            F.CHILD(1, F.P),
                            p_bins[p_idx + 1] * GeV,
                        )
                        & (_CHILDPID_PK() > pid_cut)
                        & (_CHILDP() > 10 * GeV)
                    ),
                )
            for eta_idx in range(0, len(eta_bins) - 1):
                l0_hist_dict[
                    f"pT_P-{pt_bins[pt_idx]}-{pt_bins[pt_idx + 1]}__p_ETA-{eta_bins[eta_idx]}-{eta_bins[eta_idx + 1]}__L0_M__p_PID_P-{pid_cut}"
                ] = dict(
                    l_mass_part_dict,
                    variable=F.MASS
                    * (
                        in_range(
                            pt_bins[pt_idx] * GeV,
                            F.CHILD(1, F.PT),
                            pt_bins[pt_idx + 1] * GeV,
                        )
                        & in_range(
                            eta_bins[p_idx], F.CHILD(1, F.ETA), eta_bins[eta_idx + 1]
                        )
                        & (_CHILDPID_P() > pid_cut)
                    ),
                )
                l0_hist_dict[
                    f"pT_P-{pt_bins[pt_idx]}-{pt_bins[pt_idx + 1]}__p_ETA-{eta_bins[eta_idx]}-{eta_bins[eta_idx + 1]}__L0_M__p_PID_PK-{pid_cut}"
                ] = dict(
                    l_mass_part_dict,
                    variable=F.MASS
                    * (
                        in_range(
                            pt_bins[pt_idx] * GeV,
                            F.CHILD(1, F.PT),
                            pt_bins[pt_idx + 1] * GeV,
                        )
                        & in_range(
                            eta_bins[p_idx], F.CHILD(1, F.ETA), eta_bins[eta_idx + 1]
                        )
                        & (_CHILDPID_PK() > pid_cut)
                        & (_CHILDP() > 10 * GeV)
                    ),
                )

    l0_hist_list = _make_hist_list(l0_hist_dict, name)
    global_hists = monitor(
        name=f"GlobalMonitor__{name}",
        histograms=[
            histogram_1d(
                f"/{name}/nPVS",
                f"Monitor_{name}_n_pvs",
                F.SIZE(pvs),
                10,
                (0, 10),
                "nPVs",
            ),
            histogram_1d(
                f"/{name}/n_candidates",
                f"Monitor_{name}_n_candidates",
                F.SIZE(l0lls),
                100,
                (0, 100),
                "NCANDIDATES",
            ),
        ],
    )

    return Hlt2Line(
        name=name,
        algs=_calibmon_filters(pvs) + [l0lls, global_hists] + l0_hist_list,
        hlt1_filter_code=["Hlt1TrackMVADecision", "Hlt1TwoTrackMVADecision"],
        postscale=0,
        persistreco=True,
        monitoring_variables=("pt", "eta", "m", "vchi2"),
    )


def _make_bs(jpsis, kaons, pvs):
    return ParticleCombiner(
        [jpsis, kaons],
        name="CalibMon_BToJpsiK_Combiner_{hash}",
        DecayDescriptor="[B+ -> J/psi(1S) K+]cc",
        CombinationCut=in_range(5 * GeV, F.MASS, 5.7 * GeV),
        CompositeCut=F.require_all(
            in_range(5.1 * GeV, F.MASS, 5.6 * GeV),
            F.CHI2 < 9,
            F.BPVFDCHI2(pvs) > 150,
            F.BPVIPCHI2(pvs) < 25,
        ),
    )


@register_line_builder(all_lines)
def calibmon_b_to_jpsi_k_jpsi_to_em_ep_tagged(
    name="Hlt2CalibMon_BpToJpsiKp_JpsiToEmEp_Tagged",
):
    pvs = make_pvs()
    kaons = ParticleFilter(
        make_long_kaons(),
        F.FILTER(
            F.require_all(F.PT > 1 * GeV, F.P > 3 * GeV, _MIPCHI2_MIN(9), F.PID_K > 5)
        ),
    )
    es_tag = ParticleFilter(
        make_long_electrons_with_brem(),
        F.FILTER(
            F.require_all(
                F.PT > 1.5 * GeV,
                F.P > 6 * GeV,
                _MIPCHI2_MIN(9),
                F.PID_E > 5,
                F.GHOSTPROB < 0.65,
            )
        ),
    )
    es_probe = ParticleFilter(
        make_long_electrons_with_brem(),
        F.FILTER(
            F.require_all(
                F.PT > 0.5 * GeV, F.P > 3 * GeV, _MIPCHI2_MIN(9), F.GHOSTPROB < 0.65
            )
        ),
    )
    eps_tag = ParticleFilter(es_tag, Cut=F.FILTER(F.CHARGE > 0))
    eps_probe = ParticleFilter(es_probe, Cut=F.FILTER(F.CHARGE > 0))
    ems_tag = ParticleFilter(es_tag, Cut=F.FILTER(F.CHARGE < 0))
    ems_probe = ParticleFilter(es_probe, Cut=F.FILTER(F.CHARGE < 0))

    jpsi_combination_cut = F.require_all(
        in_range(2.1 * GeV, F.MASS, 3.6 * GeV), F.ALV(1, 2) < 0.999999875
    )  # 0.5 mrad
    jpsi_composite_cut = F.require_all(
        in_range(2.2 * GeV, F.MASS, 3.5 * GeV), F.CHI2DOF < 25
    )
    jpsis_ep_tagged = ParticleCombiner(
        [eps_tag, ems_probe],
        DecayDescriptor="J/psi(1S) -> e+ e-",
        name="CalibMon_JpsiToee_Combiner_{hash}",
        CombinationCut=jpsi_combination_cut,
        CompositeCut=jpsi_composite_cut,
    )
    jpsis_em_tagged = ParticleCombiner(
        [ems_tag, eps_probe],
        DecayDescriptor="J/psi(1S) -> e- e+",
        name="CalibMon_JpsiToee_Combiner_{hash}",
        CombinationCut=jpsi_combination_cut,
        CompositeCut=jpsi_composite_cut,
    )
    b_ep_tagged = _make_bs(jpsis_ep_tagged, kaons, pvs)
    b_em_tagged = _make_bs(jpsis_em_tagged, kaons, pvs)

    bs = ParticleContainersMerger([b_ep_tagged, b_em_tagged])

    def _get_dtf_mass(particles):
        dtf = DecayTreeFitter(
            name="CalibMon_BToJpsiK_DTF_{hash}",
            input_particles=particles,
            input_pvs=pvs,
            mass_constraints=["J/psi(1S)"],
            constrain_to_ownpv=True,
        )
        return dtf(F.VALUE_OR(Value=0) @ F.MASS)

    pid_bins = (0, 5)
    b_hist_dict = {}
    b_mass_part_dict = {"bins": 50, "range": (5100, 5600), "label": "m (J/psi K) [MeV]"}

    b_hist_dict["B_M"] = dict(b_mass_part_dict, input=bs, variable=_get_dtf_mass(bs))

    for pid_cut in pid_bins:
        b_hist_dict[f"B_M_ep_tagged_probe_PIDe_gt_{pid_cut}"] = dict(
            b_mass_part_dict,
            input=b_ep_tagged,
            variable=_get_dtf_mass(b_ep_tagged)
            * (F.CHILD(1, F.CHILD(2, F.PID_E > pid_cut))),
        )
        b_hist_dict[f"B_M_em_tagged_probe_PIDe_gt_{pid_cut}"] = dict(
            b_mass_part_dict,
            input=b_em_tagged,
            variable=_get_dtf_mass(b_em_tagged)
            * (F.CHILD(1, F.CHILD(2, F.PID_E > pid_cut))),
        )
        b_hist_dict[f"B_M_probe_PIDe_gt_{pid_cut}"] = dict(
            b_mass_part_dict,
            input=bs,
            variable=_get_dtf_mass(bs) * (F.CHILD(1, F.CHILD(2, F.PID_E > pid_cut))),
        )

    b_hist_list = _make_hist_list(b_hist_dict, name)

    return Hlt2Line(
        name=name,
        algs=_calibmon_filters(pvs) + [bs] + b_hist_list,
        hlt1_filter_code=["Hlt1TrackMVADecision", "Hlt1TwoTrackMVADecision"],
        persistreco=True,
        postscale=0,
    )


def _make_detached_jpsis(tag, probe, pvs):
    return ParticleCombiner(
        [tag, probe],
        name="CalibMon_Detached_JpsiToMuMu_Combiner_{hash}",
        DecayDescriptor="J/psi(1S) -> mu+ mu-"
        if "CHARGE > 0" in tag.producer.properties["Cut"].code_repr()
        else "J/psi(1S) -> mu- mu+",
        CombinationCut=F.require_all(
            in_range(2890 * MeV, F.MASS, 3300 * MeV), F.SDOCACHI2(1, 2) < 6.0
        ),
        CompositeCut=F.require_all(
            in_range(2915 * MeV, F.MASS, 3275 * MeV),
            F.PT > 1 * GeV,
            F.CHI2 < 15,
            F.BPVFDCHI2(pvs) > 150,
            _MIPCHI2_MIN(5),
            F.BPVDIRA(pvs) > 0.995,
        ),
    )


@register_line_builder(all_lines)
def calibmon_jpsi_to_mup_mum_detached_tagged(
    name="Hlt2CalibMon_JpsiToMupMum_Detached_Tagged",
):
    pvs = make_pvs()
    mus_tag = ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(F.require_all(F.P > 3 * GeV, F.PT > 1.2 * GeV, _MIPCHI2_MIN(9))),
    )
    mus_probe = ParticleFilter(
        make_long_muons(),
        F.FILTER(F.require_all(F.INMUON, F.P > 3 * GeV, _MIPCHI2_MIN(20))),
    )

    mups_tag = ParticleFilter(
        mus_tag,
        Cut=F.FILTER(
            F.require_all(
                F.CHARGE > 0, _HLT1_DEC(["Hlt1DetJpsiToMuMuPosTagLineDecision"])
            )
        ),
    )
    mups_probe = ParticleFilter(mus_probe, Cut=F.FILTER(F.CHARGE > 0))
    mums_tag = ParticleFilter(
        mus_tag,
        Cut=F.FILTER(
            F.require_all(
                F.CHARGE < 0, _HLT1_DEC(["Hlt1DetJpsiToMuMuNegTagLineDecision"])
            )
        ),
    )
    mums_probe = ParticleFilter(mus_probe, Cut=F.FILTER(F.CHARGE < 0))

    detached_jpsis_mum_tagged = _make_detached_jpsis(mums_tag, mups_probe, pvs)
    detached_jpsis_mup_tagged = _make_detached_jpsis(mups_tag, mums_probe, pvs)
    detached_jpsis = ParticleContainersMerger(
        [detached_jpsis_mum_tagged, detached_jpsis_mup_tagged]
    )

    p_bins = (3000, 6000, 10000, float("inf"))
    pt_bins = (0, 800, float("inf"))
    jpsi_hist_dict = {}
    jpsi_mass_part_dict = {
        "bins": 90,
        "range": (2915, 3275),
        "label": "m (mu^+ mu^-) [MeV]",
    }
    for p_idx in range(0, len(p_bins) - 1):
        for pt_idx in range(0, len(pt_bins) - 1):
            jpsi_hist_dict[
                f"Jpsi_M__p-{p_bins[p_idx]}-{p_bins[p_idx + 1]}__pT_{pt_bins[pt_idx]}-{pt_bins[pt_idx + 1]}_mum_tag"
            ] = dict(
                jpsi_mass_part_dict,
                input=detached_jpsis_mum_tagged,
                variable=F.MASS
                * (
                    in_range(
                        pt_bins[pt_idx],
                        F.CHILD(2, F.PT),
                        100 * TeV
                        if pt_bins[pt_idx + 1] == float("inf")
                        else pt_bins[pt_idx + 1],
                    )
                    & in_range(
                        p_bins[p_idx],
                        F.CHILD(2, F.P),
                        100 * TeV
                        if p_bins[p_idx + 1] == float("inf")
                        else p_bins[p_idx + 1],
                    )
                ),
            )
            jpsi_hist_dict[
                f"Jpsi_M__p-{p_bins[p_idx]}-{p_bins[p_idx + 1]}__pT_{pt_bins[pt_idx]}-{pt_bins[pt_idx + 1]}_mup_tag"
            ] = dict(
                jpsi_mass_part_dict,
                input=detached_jpsis_mup_tagged,
                variable=F.MASS
                * (
                    in_range(
                        pt_bins[pt_idx],
                        F.CHILD(2, F.PT),
                        100 * TeV
                        if pt_bins[pt_idx + 1] == float("inf")
                        else pt_bins[pt_idx + 1],
                    )
                    & in_range(
                        p_bins[p_idx],
                        F.CHILD(2, F.P),
                        100 * TeV
                        if p_bins[p_idx + 1] == float("inf")
                        else p_bins[p_idx + 1],
                    )
                ),
            )
            jpsi_hist_dict[
                f"Jpsi_M__p-{p_bins[p_idx]}-{p_bins[p_idx + 1]}__pT_{pt_bins[pt_idx]}-{pt_bins[pt_idx + 1]}_muprobe_ISMUON_mum_tag"
            ] = dict(
                jpsi_mass_part_dict,
                input=detached_jpsis_mum_tagged,
                variable=F.MASS
                * (
                    (F.CHILD(2, F.ISMUON))
                    & in_range(
                        pt_bins[pt_idx],
                        F.CHILD(2, F.PT),
                        100 * TeV
                        if pt_bins[pt_idx + 1] == float("inf")
                        else pt_bins[pt_idx + 1],
                    )
                    & in_range(
                        p_bins[p_idx],
                        F.CHILD(2, F.P),
                        100 * TeV
                        if p_bins[p_idx + 1] == float("inf")
                        else p_bins[p_idx + 1],
                    )
                ),
            )
            jpsi_hist_dict[
                f"Jpsi_M__p-{p_bins[p_idx]}-{p_bins[p_idx + 1]}__pT_{pt_bins[pt_idx]}-{pt_bins[pt_idx + 1]}_muprobe_ISMUON_mup_tag"
            ] = dict(
                jpsi_mass_part_dict,
                input=detached_jpsis_mup_tagged,
                variable=F.MASS
                * (
                    (F.CHILD(2, F.ISMUON))
                    & in_range(
                        pt_bins[pt_idx],
                        F.CHILD(2, F.PT),
                        100 * TeV
                        if pt_bins[pt_idx + 1] == float("inf")
                        else pt_bins[pt_idx + 1],
                    )
                    & in_range(
                        p_bins[p_idx],
                        F.CHILD(2, F.P),
                        100 * TeV
                        if p_bins[p_idx + 1] == float("inf")
                        else p_bins[p_idx + 1],
                    )
                ),
            )
    jpsi_hist_list = _make_hist_list(jpsi_hist_dict, name)

    detached_jpsis_mum_tagged_tag_p_filtered = ParticleFilter(
        detached_jpsis_mum_tagged, Cut=F.FILTER(F.CHILD(2, F.P) > 10 * GeV)
    )

    detached_jpsis_mup_tagged_tag_p_filtered = ParticleFilter(
        detached_jpsis_mup_tagged, Cut=F.FILTER(F.CHILD(2, F.P) > 10 * GeV)
    )

    def get_mu_from_jpsi(jpsi_candidate, mu_id):
        return ThOrParticleSelection(
            InputParticles=jpsi_candidate,
            Functor=(F.FILTER(F.IS_ID(mu_id)) @ F.GET_ALL_DESCENDANTS()),
        ).OutputSelection

    hits = MuonRawInUpgradeToHits(
        OutputLevel=0,
        RawBanks=default_raw_banks("Muon"),
        ErrorRawBanks=default_raw_banks("MuonError"),
        UseErrorBank=False,
        PrintStat=False,
    )

    muon_chamber_mon_mums_tagged = MuonChamberMonitor(
        Probes=get_mu_from_jpsi(detached_jpsis_mum_tagged_tag_p_filtered, "mu+"),
        Tags=get_mu_from_jpsi(detached_jpsis_mum_tagged_tag_p_filtered, "mu-"),
        MuonHits=hits,
        name="MuonChamberMonitor_MumTagged",
    )
    muon_chamber_mon_mups_tagged = MuonChamberMonitor(
        Probes=get_mu_from_jpsi(detached_jpsis_mup_tagged_tag_p_filtered, "mu-"),
        Tags=get_mu_from_jpsi(detached_jpsis_mup_tagged_tag_p_filtered, "mu+"),
        MuonHits=hits,
        name="MuonChamberMonitor_MupTagged",
    )

    comb_chamber_mon = CompositeNode(
        "MuonChamberMonitor_Combined_Node",
        [muon_chamber_mon_mums_tagged, muon_chamber_mon_mups_tagged],
        combine_logic=NodeLogic.NONLAZY_OR,
        force_order=False,
    )

    return Hlt2Line(
        name=name,
        algs=_calibmon_filters(pvs)
        + [detached_jpsis]
        + jpsi_hist_list
        + [comb_chamber_mon],
        persistreco=True,
        postscale=0,
    )


def _fill_hist_lists_for_dimuon_trackeff(
    line_name,
    jpsis,
    jpsis_mum_tagged,
    jpsis_mup_tagged,
    matched_jpsis,
    matched_jpsi_mum_tagged,
    matched_jpsi_mup_tagged,
    pvs,
):
    pt_bins = (1000, 2000, 3000, 4000, 10000)
    eta_bins = (2, 3, 4, 5)
    npv_bins = (1, 2, 4, 6, 20)
    jpsi_hist_dict = {}
    matched_jpsi_hist_dict = {}
    jpsi_mass_part_dict = {
        "bins": 90,
        "range": (2700, 3600),
        "label": "m (mu^+ mu^-) [MeV]",
    }

    def _make_match_and_tag_hists(binname, functor):
        jpsi_hist_dict[f"{binname}"] = dict(
            jpsi_mass_part_dict, input=jpsis, variable=functor
        )
        matched_jpsi_hist_dict[f"{binname}_mup_probe_matched"] = dict(
            jpsi_mass_part_dict, input=matched_jpsis, variable=functor
        )
        jpsi_hist_dict[f"{binname}_mup_probe"] = dict(
            jpsi_mass_part_dict, input=jpsis_mum_tagged, variable=functor
        )
        jpsi_hist_dict[f"{binname}_mum_probe"] = dict(
            jpsi_mass_part_dict, input=jpsis_mup_tagged, variable=functor
        )
        matched_jpsi_hist_dict[f"{binname}_mup_probe_matched"] = dict(
            jpsi_mass_part_dict, input=matched_jpsi_mum_tagged, variable=functor
        )
        matched_jpsi_hist_dict[f"{binname}_mum_probe_matched"] = dict(
            jpsi_mass_part_dict, input=matched_jpsi_mup_tagged, variable=functor
        )

    _make_match_and_tag_hists("Jpsi_M", F.MASS)
    _make_match_and_tag_hists("muprobe_PT", F.CHILD(2, F.PT))
    _make_match_and_tag_hists("muprobe_ETA", F.CHILD(2, F.ETA))
    _make_match_and_tag_hists("muprobe_PHI", F.CHILD(2, F.PHI))

    for pt_idx in range(0, len(pt_bins) - 1):
        # can't use in_range here. JIT compilation fails horribly
        _make_match_and_tag_hists(
            f"Jpsi_M_muprobe_PT-{pt_bins[pt_idx]}-{pt_bins[pt_idx + 1]}",
            F.MASS
            * (
                (pt_bins[pt_idx] < F.CHILD(2, F.PT))
                & (F.CHILD(2, F.PT) > pt_bins[pt_idx + 1])
            ),
        )
        for eta_idx in range(0, len(eta_bins) - 1):
            _make_match_and_tag_hists(
                f"Jpsi_M_muprobe_PT-{pt_bins[pt_idx]}-{pt_bins[pt_idx + 1]}_muprobe_ETA-{eta_bins[eta_idx]}-{eta_bins[eta_idx + 1]}",
                F.MASS
                * (
                    in_range(pt_bins[pt_idx], F.CHILD(2, F.PT), pt_bins[pt_idx + 1])
                    & in_range(
                        eta_bins[eta_idx], F.CHILD(2, F.ETA), eta_bins[eta_idx + 1]
                    )
                ),
            )
    for eta_idx in range(0, len(eta_bins) - 1):
        _make_match_and_tag_hists(
            f"Jpsi_M_muprobe_ETA-{eta_bins[eta_idx]}-{eta_bins[eta_idx + 1]}",
            F.MASS
            * (
                (eta_bins[eta_idx] < F.CHILD(2, F.ETA))
                & (F.CHILD(2, F.ETA) > eta_bins[eta_idx + 1])
            ),
        )
    for npv_idx in range(0, len(npv_bins) - 1):
        _make_match_and_tag_hists(
            f"Jpsi_M_nPVs-{npv_bins[npv_idx]}-{npv_bins[npv_idx + 1]}",
            F.MASS
            * (
                (npv_bins[npv_idx] < F.SIZE(pvs))
                & (F.SIZE(pvs) > npv_bins[npv_idx + 1])
            ),
        )

    return _make_hist_list(jpsi_hist_dict, line_name), _make_hist_list(
        matched_jpsi_hist_dict, line_name
    )


_TAG_HLT1_LINES = ["Hlt1TrackMuonMVADecision", "Hlt1TrackMVADecision"]


def _filter_probe_muons(particles):
    return ParticleFilter(
        particles, F.FILTER(F.require_all(F.CHI2DOF < 5, F.PT > 1 * GeV, F.P > 5 * GeV))
    )


def _make_velomuon_jpsis(tag_and_probe_particles):
    return ParticleCombiner(
        tag_and_probe_particles,
        name="CalibMon_VeloMuon_JpsiToMuMu_Combiner_{hash}",
        DecayDescriptor="J/psi(1S) -> mu+ mu-"
        if "CHARGE > 0"
        in tag_and_probe_particles[0]
        .__dict__["_producer"]
        .__dict__["_properties"]["Cut"]
        .code_repr()
        else "J/psi(1S) -> mu- mu+",
        CombinationCut=F.require_all(
            in_range(2600 * MeV, F.MASS, 3700 * MeV), F.MAXDOCACUT(0.1 * mm)
        ),
        CompositeCut=F.require_all(
            in_range(2700 * MeV, F.MASS, 3600 * MeV), F.PT > 500 * MeV, F.CHI2DOF < 2
        ),
    )


@register_line_builder(all_lines)
def calibmon_dimuon_velomuon(name="Hlt2CalibMon_DiMuon_VeloMuon"):
    pvs = make_pvs()
    mus_tag_all_hlt1 = ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.CHI2DOF < 3,
                F.PT > 500 * MeV,
                F.P > 7 * GeV,
                F.PID_MU > -1,
                F.MINIP(pvs) > 0.2 * mm,
            )
        ),
    )
    mus_tag = hlt1_tos_on_any_filter(
        data=mus_tag_all_hlt1, hlt1_trigger_lines=_TAG_HLT1_LINES
    )
    mus_probe = _filter_probe_muons(make_velomuon_muons())

    mups_tag = ParticleFilter(mus_tag, Cut=F.FILTER(F.CHARGE > 0))
    mups_probe = ParticleFilter(mus_probe, Cut=F.FILTER(F.CHARGE > 0))
    mums_tag = ParticleFilter(mus_tag, Cut=F.FILTER(F.CHARGE < 0))
    mums_probe = ParticleFilter(mus_probe, Cut=F.FILTER(F.CHARGE < 0))

    jpsis_mum_tagged = _make_velomuon_jpsis([mums_tag, mups_probe])
    jpsis_mup_tagged = _make_velomuon_jpsis([mups_tag, mums_probe])
    jpsis = ParticleContainersMerger([jpsis_mum_tagged, jpsis_mup_tagged])

    # for matching
    long_mups = ParticleFilter(make_ismuon_long_muon(), Cut=F.FILTER(F.CHARGE > 0))
    long_mums = ParticleFilter(make_ismuon_long_muon(), Cut=F.FILTER(F.CHARGE < 0))
    matched_jpsi_mum_tagged = MuonProbeToLongMatcher(
        TwoBodyComposites=jpsis_mum_tagged,
        LongTracks=long_mups,
        checkVP=True,
        checkFT=False,
        checkUT=False,
        checkMuon=True,
        MuonHitContainer=make_muon_hits(),
        addNeighbouringMuonHits=True,
    ).MatchedComposites
    matched_jpsi_mup_tagged = MuonProbeToLongMatcher(
        TwoBodyComposites=jpsis_mup_tagged,
        LongTracks=long_mums,
        checkVP=True,
        checkFT=False,
        checkUT=False,
        checkMuon=True,
        MuonHitContainer=make_muon_hits(),
        addNeighbouringMuonHits=True,
    ).MatchedComposites
    matched_jpsis = ParticleContainersMerger(
        [matched_jpsi_mum_tagged, matched_jpsi_mup_tagged]
    )

    jpsi_hist_list, matched_jpsi_hist_list = _fill_hist_lists_for_dimuon_trackeff(
        name,
        jpsis,
        jpsis_mum_tagged,
        jpsis_mup_tagged,
        matched_jpsis,
        matched_jpsi_mum_tagged,
        matched_jpsi_mup_tagged,
        pvs,
    )

    return Hlt2Line(
        name=name,
        algs=_calibmon_filters(pvs)
        + [mus_tag, jpsis]
        + jpsi_hist_list
        + [matched_jpsis]
        + matched_jpsi_hist_list,
        persistreco=True,
        postscale=0,
    )


def _make_downstream_jpsis(tag_and_probe_particles, pvs):
    return ParticleCombiner(
        tag_and_probe_particles,
        name="CalibMon_Downstream_JpsiToMuMu_Combiner_{hash}",
        DecayDescriptor="J/psi(1S) -> mu+ mu-"
        if "CHARGE > 0"
        in tag_and_probe_particles[0].producer.properties["Cut"].code_repr()
        else "J/psi(1S) -> mu- mu+",
        CombinationCut=F.require_all(
            in_range(2700 * MeV, F.MASS, 3500 * MeV), F.MAXDOCACUT(5 * mm)
        ),
        CompositeCut=F.require_all(
            in_range(2800 * MeV, F.MASS, 3400 * MeV),
            F.MINIP(pvs) < 2 * mm,
            F.CHI2DOF < 5.0,
            F.BPVLTIME(pvs) > 0.0,
        ),
    )


@register_line_builder(all_lines)
def calibmon_dimuon_downstream(name="Hlt2CalibMon_DiMuon_Downstream", prescale=0.25):
    pvs = make_pvs()
    mus_tag_all_hlt1 = ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.CHI2DOF < 10,
                F.PT > 700 * MeV,
                F.P > 5 * GeV,
                F.PID_MU > -2,
                F.GHOSTPROB() < 1,
                F.MINIP(pvs) > 0.5 * mm,
            )
        ),
    )
    mus_tag = hlt1_tos_on_any_filter(
        data=mus_tag_all_hlt1, hlt1_trigger_lines=_TAG_HLT1_LINES
    )
    mus_probe = ParticleFilter(
        make_downstream_muons(),
        F.FILTER(
            F.require_all(
                F.CHI2DOF < 10,
                F.PT > 500 * MeV,
                F.P > 5 * GeV,
                F.MINIPCHI2(pvs) > 0,
                F.ISMUON(),
            )
        ),
    )

    mups_tag = ParticleFilter(mus_tag, Cut=F.FILTER(F.CHARGE > 0))
    mups_probe = ParticleFilter(mus_probe, Cut=F.FILTER(F.CHARGE > 0))
    mums_tag = ParticleFilter(mus_tag, Cut=F.FILTER(F.CHARGE < 0))
    mums_probe = ParticleFilter(mus_probe, Cut=F.FILTER(F.CHARGE < 0))

    jpsis_mum_tagged = _make_downstream_jpsis([mums_tag, mups_probe], pvs)
    jpsis_mup_tagged = _make_downstream_jpsis([mups_tag, mums_probe], pvs)
    jpsis = ParticleContainersMerger([jpsis_mum_tagged, jpsis_mup_tagged])

    # for matching
    long_mups = ParticleFilter(make_ismuon_long_muon(), Cut=F.FILTER(F.CHARGE > 0))
    long_mums = ParticleFilter(make_ismuon_long_muon(), Cut=F.FILTER(F.CHARGE < 0))
    matched_jpsi_mum_tagged = MuonProbeToLongMatcher(
        TwoBodyComposites=jpsis_mum_tagged,
        LongTracks=long_mups,
        checkVP=False,
        checkFT=True,
        checkUT=True,
        checkMuon=True,
        MuonHitContainer=make_muon_hits(),
        addNeighbouringMuonHits=True,
    ).MatchedComposites
    matched_jpsi_mup_tagged = MuonProbeToLongMatcher(
        TwoBodyComposites=jpsis_mup_tagged,
        LongTracks=long_mums,
        checkVP=False,
        checkFT=True,
        checkUT=True,
        checkMuon=True,
        MuonHitContainer=make_muon_hits(),
        addNeighbouringMuonHits=True,
    ).MatchedComposites
    matched_jpsis = ParticleContainersMerger(
        [matched_jpsi_mum_tagged, matched_jpsi_mup_tagged]
    )

    jpsi_hist_list, matched_jpsi_hist_list = _fill_hist_lists_for_dimuon_trackeff(
        name,
        jpsis,
        jpsis_mum_tagged,
        jpsis_mup_tagged,
        matched_jpsis,
        matched_jpsi_mum_tagged,
        matched_jpsi_mup_tagged,
        pvs,
    )

    return Hlt2Line(
        name=name,
        algs=_calibmon_filters(pvs)
        + [mus_tag, jpsis]
        + jpsi_hist_list
        + [matched_jpsis]
        + matched_jpsi_hist_list,
        persistreco=True,
        prescale=prescale,
        postscale=0,
    )


def _make_muonut_jpsis(tag_and_probe_particles, pvs):
    return ParticleCombiner(
        tag_and_probe_particles,
        name="CalibMon_MuonUT_JpsiToMuMu_Combiner_{hash}",
        DecayDescriptor="J/psi(1S) -> mu+ mu-"
        if "CHARGE > 0"
        in tag_and_probe_particles[0].producer.properties["Cut"].code_repr()
        else "J/psi(1S) -> mu- mu+",
        CombinationCut=F.require_all(in_range(2500 * MeV, F.MASS, 3700 * MeV)),
        CompositeCut=F.require_all(
            in_range(2600 * MeV, F.MASS, 3600 * MeV),
            F.MINIP(pvs) < 1 * mm,
            F.CHI2DOF < 2.0,
            F.BPVLTIME(pvs) > 0.0,
        ),
    )


@register_line_builder(all_lines)
def calibmon_dimuon_muonut(name="Hlt2CalibMon_DiMuon_MuonUT"):
    pvs = make_pvs()
    mus_tag_all_hlt1 = ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.CHI2DOF < 5,
                F.PT > 1.3 * GeV,
                F.P > 10 * GeV,
                F.PID_MU > 0.0,
                F.GHOSTPROB() < 0.3,
            )
        ),
    )
    mus_tag = hlt1_tos_on_any_filter(
        data=mus_tag_all_hlt1, hlt1_trigger_lines=_TAG_HLT1_LINES
    )
    mus_probe = ParticleFilter(
        make_muonut_muons(),
        F.FILTER(F.require_all(F.PT > 500 * MeV, F.P > 5 * GeV, F.MINIPCHI2(pvs) > 0)),
    )

    mups_tag = ParticleFilter(mus_tag, Cut=F.FILTER(F.CHARGE > 0))
    mups_probe = ParticleFilter(mus_probe, Cut=F.FILTER(F.CHARGE > 0))
    mums_tag = ParticleFilter(mus_tag, Cut=F.FILTER(F.CHARGE < 0))
    mums_probe = ParticleFilter(mus_probe, Cut=F.FILTER(F.CHARGE < 0))

    jpsis_mum_tagged = _make_muonut_jpsis([mums_tag, mups_probe], pvs)
    jpsis_mup_tagged = _make_muonut_jpsis([mups_tag, mums_probe], pvs)
    jpsis = ParticleContainersMerger([jpsis_mum_tagged, jpsis_mup_tagged])

    # for matching
    long_mups = ParticleFilter(make_ismuon_long_muon(), Cut=F.FILTER(F.CHARGE > 0))
    long_mums = ParticleFilter(make_ismuon_long_muon(), Cut=F.FILTER(F.CHARGE < 0))
    matched_jpsi_mum_tagged = MuonProbeToLongMatcher(
        TwoBodyComposites=jpsis_mum_tagged,
        LongTracks=long_mups,
        checkVP=False,
        checkFT=False,
        checkUT=True,
        checkMuon=True,
        MuonHitContainer=make_muon_hits(),
        addNeighbouringMuonHits=True,
    ).MatchedComposites
    matched_jpsi_mup_tagged = MuonProbeToLongMatcher(
        TwoBodyComposites=jpsis_mup_tagged,
        LongTracks=long_mums,
        checkVP=False,
        checkFT=False,
        checkUT=True,
        checkMuon=True,
        MuonHitContainer=make_muon_hits(),
        addNeighbouringMuonHits=True,
    ).MatchedComposites
    matched_jpsis = ParticleContainersMerger(
        [matched_jpsi_mum_tagged, matched_jpsi_mup_tagged]
    )

    jpsi_hist_list, matched_jpsi_hist_list = _fill_hist_lists_for_dimuon_trackeff(
        name,
        jpsis,
        jpsis_mum_tagged,
        jpsis_mup_tagged,
        matched_jpsis,
        matched_jpsi_mum_tagged,
        matched_jpsi_mup_tagged,
        pvs,
    )

    return Hlt2Line(
        name=name,
        algs=_calibmon_filters(pvs)
        + [mus_tag, jpsis]
        + jpsi_hist_list
        + [matched_jpsis]
        + matched_jpsi_hist_list,
        persistreco=True,
        postscale=0,
    )


def _make_seed_jpsis(tag_and_probe_particles):
    return ParticleCombiner(
        tag_and_probe_particles,
        name="CalibMon_SeedMuon_JpsiToMuMu_Combiner_{hash}",
        DecayDescriptor="J/psi(1S) -> mu+ mu-"
        if "CHARGE > 0"
        in tag_and_probe_particles[0].producer.properties["Cut"].code_repr()
        else "J/psi(1S) -> mu- mu+",
        CombinationCut=F.require_all(
            in_range(2600 * MeV, F.MASS, 3700 * MeV), F.MAXDOCACUT(50 * mm)
        ),
        CompositeCut=F.require_all(in_range(2700 * MeV, F.MASS, 3600 * MeV)),
    )


@register_line_builder(all_lines)
def calibmon_dimuon_seedmuon(name="Hlt2CalibMon_DiMuon_SeedMuon"):
    pvs = make_pvs()
    mus_tag_all_hlt1 = ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.CHI2DOF < 3,
                F.PT > 500 * MeV,
                F.P > 10 * GeV,
                F.PID_MU > -2,
                F.MINIP(pvs) > 0.1 * mm,
                _MIPCHI2_MIN(38),
            )
        ),
    )
    mus_tag = hlt1_tos_on_any_filter(
        data=mus_tag_all_hlt1, hlt1_trigger_lines=_TAG_HLT1_LINES
    )
    mus_probe = _filter_probe_muons(make_seed_muons())

    mups_tag = ParticleFilter(mus_tag, Cut=F.FILTER(F.CHARGE > 0))
    mups_probe = ParticleFilter(mus_probe, Cut=F.FILTER(F.CHARGE > 0))
    mums_tag = ParticleFilter(mus_tag, Cut=F.FILTER(F.CHARGE < 0))
    mums_probe = ParticleFilter(mus_probe, Cut=F.FILTER(F.CHARGE < 0))

    jpsis_mum_tagged = _make_seed_jpsis([mums_tag, mups_probe])
    jpsis_mup_tagged = _make_seed_jpsis([mups_tag, mums_probe])
    jpsis = ParticleContainersMerger([jpsis_mum_tagged, jpsis_mup_tagged])

    # for matching
    long_mups = ParticleFilter(make_ismuon_long_muon(), Cut=F.FILTER(F.CHARGE > 0))
    long_mums = ParticleFilter(make_ismuon_long_muon(), Cut=F.FILTER(F.CHARGE < 0))
    matched_jpsi_mum_tagged = MuonProbeToLongMatcher(
        TwoBodyComposites=jpsis_mum_tagged,
        LongTracks=long_mups,
        checkVP=False,
        checkFT=True,
        checkUT=False,
        checkMuon=True,
        MuonHitContainer=make_muon_hits(),
        addNeighbouringMuonHits=True,
    ).MatchedComposites
    matched_jpsi_mup_tagged = MuonProbeToLongMatcher(
        TwoBodyComposites=jpsis_mup_tagged,
        LongTracks=long_mums,
        checkVP=False,
        checkFT=True,
        checkUT=False,
        checkMuon=True,
        MuonHitContainer=make_muon_hits(),
        addNeighbouringMuonHits=True,
    ).MatchedComposites
    matched_jpsis = ParticleContainersMerger(
        [matched_jpsi_mum_tagged, matched_jpsi_mup_tagged]
    )

    jpsi_hist_list, matched_jpsi_hist_list = _fill_hist_lists_for_dimuon_trackeff(
        name,
        jpsis,
        jpsis_mum_tagged,
        jpsis_mup_tagged,
        matched_jpsis,
        matched_jpsi_mum_tagged,
        matched_jpsi_mup_tagged,
        pvs,
    )

    return Hlt2Line(
        name=name,
        algs=_calibmon_filters(pvs)
        + [mus_tag, jpsis]
        + jpsi_hist_list
        + [matched_jpsis]
        + matched_jpsi_hist_list,
        persistreco=True,
        postscale=0,
    )


def _construct_hlt2_monitoring_line(name, prescale, *, charge=None, no_bias=False):
    pvs = make_pvs()
    all_long_pions = make_long_pions()
    pions = KSVeloLong.filter_pions(all_long_pions, pvs, pt_min=0.5 * GeV)
    if not no_bias:
        muonsVelo = KSVeloLong.filter_particles_tis_velo(
            KSVeloLong.make_velo_muons(), pvs
        )
        hlt1_filter_code = None
    else:
        muonsVelo = KSVeloLong.filter_particles_velo(KSVeloLong.make_velo_muons(), pvs)
        hlt1_filter_code = "Hlt1ODINLumiDecision"

    if charge is not None:
        if charge > 0:
            pions = ParticleFilter(pions, F.FILTER(F.PARTICLE_ID < 0))
        else:
            pions = ParticleFilter(pions, F.FILTER(F.PARTICLE_ID > 0))

    kshorts = KSVeloLong.make_kshort_pi_muplus(
        pions, muonsVelo, pvs=pvs, comb_m_max=1500, comb_m_min=0
    )  # probe should be on index 1
    filtered_kshorts = KSLongVeloFilter(
        InputParticle=kshorts,
        InputPVs=pvs,
        PVConstrainedMassMin=350.0,
        PVConstrainedMassMax=625.0,
        PVConstrainedProbePMin=2000.0,
        PVConstrainedProbePtMin=250.0,
        IPperpendicularMax=0.007,
        ApplyLambdaVeto=True,
        IPMax=0.8,
        name="TrackEffFilterMonitoring_{hash}",
    ).OutputParticles
    muon_filter = VoidFilter(Cut=F.SIZE(muonsVelo) > 0)

    flattened_decay_tree = FlattenDecayTree(InputParticles=filtered_kshorts)
    basic_particles = ParticleFilter(
        flattened_decay_tree.OutputParticles,
        F.FILTER(F.ISBASICPARTICLE),
        name="KsEfficiencyFlattenDecayTree_{hash}",
    )

    relation_table_match_to_long = LHCbIDOverlapRelationTable(
        MatchFrom=basic_particles, MatchTo=all_long_pions, MinMatchFraction=0.5
    )
    plot_efficiency = KSVelo2LongEfficiencyMonitor(
        name=f"{name}",
        Particles=filtered_kshorts,
        Table=relation_table_match_to_long,
        MinMatchFraction=0.7,
    )

    algs = _calibmon_filters(pvs) + [muon_filter, filtered_kshorts, plot_efficiency]
    return Hlt2Line(
        name=name,
        prescale=prescale,
        postscale=0,
        algs=algs,
        persistreco=True,
        hlt1_filter_code=hlt1_filter_code,
    )


@register_line_builder(all_lines)
def kshort_velo_long_all_hlt1_monitoring_line(
    name="Hlt2CalibMon_KshortVeloLongAllHlt1", prescale=1.0, no_bias=False
):
    return _construct_hlt2_monitoring_line(
        name=name, prescale=prescale, no_bias=no_bias
    )


@register_line_builder(all_lines)
def kshort_velo_long_all_hlt1_plus_monitoring_line(
    name="Hlt2CalibMon_KshortVeloLongAllHlt1_plus", prescale=1.0, no_bias=False
):
    return _construct_hlt2_monitoring_line(
        name=name, prescale=prescale, no_bias=no_bias, charge=1
    )


@register_line_builder(all_lines)
def kshort_velo_long_all_hlt1_minus_monitoring_line(
    name="Hlt2CalibMon_KshortVeloLongAllHlt1_minus", prescale=1.0, no_bias=False
):
    return _construct_hlt2_monitoring_line(
        name=name, prescale=prescale, no_bias=no_bias, charge=-1
    )


# mstahl: currently (240717) broken; please keep the commented code
# def _construct_hlt1_monitoring_line(name,
#                                     prescale,
#                                     *,
#                                     charge=None,
#                                     no_bias=False):
#     pvs = make_pvs()
#     all_long_pions = make_long_pions()
#     pions = KSVeloLong.filter_pions(all_long_pions, pvs, pt_min=0.5 * GeV)
#     if not no_bias:
#         muonsVelo = KSVeloLong.filter_particles_tis_velo(
#             KSVeloLong.make_velo_muons(), pvs)
#         hlt1_filter_code = None
#     else:
#         muonsVelo = KSVeloLong.filter_particles_velo(
#             KSVeloLong.make_velo_muons(), pvs)
#         hlt1_filter_code = "Hlt1ODINLumiDecision"

#     if charge is not None:
#         if charge > 0:
#             pions = ParticleFilter(pions, F.FILTER(F.PARTICLE_ID < 0))
#         else:
#             pions = ParticleFilter(pions, F.FILTER(F.PARTICLE_ID > 0))

#     kshorts = KSVeloLong.make_kshort_pi_muplus(
#         pions, muonsVelo, pvs=pvs, comb_m_max=1500,
#         comb_m_min=0)  # probe should be on index 1
#     filtered_kshorts = KSLongVeloFilter(
#         InputParticle=kshorts,
#         InputPVs=pvs,
#         PVConstrainedMassMin=350.,
#         PVConstrainedMassMax=625.,
#         PVConstrainedProbePMin=2000.,
#         PVConstrainedProbePtMin=250.,
#         IPperpendicularMax=0.007,
#         ApplyLambdaVeto=True,
#         IPMax=0.8,
#         name="TrackEffFilterMonitoring_{hash}",
#     ).OutputParticles
#     muon_filter = VoidFilter(Cut=F.SIZE(muonsVelo) > 0)

#     flattened_decay_tree = FlattenDecayTree(InputParticles=filtered_kshorts)
#     basic_particles = ParticleFilter(
#         flattened_decay_tree.OutputParticles,
#         F.FILTER(F.ISBASICPARTICLE),
#         name="KsEfficiencyFlattenDecayTree_{hash}")

#     non_event_data_node = setup_allen_non_event_data_service()
#     hlt1_reco = hlt1_reconstruction(
#         tracking_type=TrackingType.MATCHING,
#         with_calo=False,
#         with_muon=False,
#         with_ut=False)
#     allen_long_tracks = make_converted_tracks(
#         hlt1_reco['long_track_particles']
#         ['dev_multi_event_basic_particles'])["v1keyed"]
#     charged_long_protos_callable = lambda: FunctionalChargedProtoParticleMaker(Inputs=[allen_long_tracks], Code=F.require_all(F.ALL)).Output
#     allen_long_pions = _make_particles(
#         species="pion",
#         get_track_selector=get_all_track_selector,
#         make_protoparticles=charged_long_protos_callable)
#     relation_table_match_to_long = LHCbIDOverlapRelationTable(
#         MatchFrom=basic_particles,
#         MatchTo=allen_long_pions,
#         MinMatchFraction=0.5)
#     plot_efficiency = KSVelo2LongEfficiencyMonitor(
#         name=f"{name}",
#         Particles=filtered_kshorts,
#         Table=relation_table_match_to_long,
#         MinMatchFraction=0.7)

#     allen_node = CompositeNode(
#         f'{name}_allen_reconstruction',
#         combine_logic=NodeLogic.NONLAZY_OR,
#         children=[non_event_data_node, plot_efficiency],
#         force_order=True)

#     algs = _calibmon_filters(pvs) + [muon_filter, filtered_kshorts, allen_node]
#     return Hlt2Line(
#         name=name,
#         prescale=prescale,
#         postscale=0,
#         algs=algs,
#         persistreco=True,
#         hlt1_filter_code=hlt1_filter_code)

# @register_line_builder(all_lines)
# def kshort_velo_hlt1long_all_hlt1_monitoring_line(
#         name="Hlt2CalibMon_KshortVeloHlt1LongAllHlt1", prescale=1.0,
#         no_bias=False):
#     return _construct_hlt1_monitoring_line(
#         name=name, prescale=prescale, no_bias=no_bias)

# @register_line_builder(all_lines)
# def kshort_velo_hlt1long_all_hlt1_plus_monitoring_line(
#         name="Hlt2CalibMon_KshortVeloHlt1LongAllHlt1_plus",
#         prescale=1.0,
#         no_bias=False):
#     return _construct_hlt1_monitoring_line(
#         name=name, prescale=prescale, no_bias=no_bias, charge=1)

# @register_line_builder(all_lines)
# def kshort_velo_hlt1long_all_hlt1_minus_monitoring_line(
#         name="Hlt2CalibMon_KshortVeloHlt1LongAllHlt1_minus",
#         prescale=1.0,
#         no_bias=False):
#     return _construct_hlt1_monitoring_line(
#         name=name, prescale=prescale, no_bias=no_bias, charge=-1)
