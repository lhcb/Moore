###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf.Algorithms import TrackSelectionMerger, TrackVertexMonitor
from RecoConf.hlt2_global_reco import make_pvs, make_tracks


def make_vertex_monitoring():
    long_tracks = make_tracks(track_type="Long")
    velo_tracks = make_tracks(track_type="Velo")
    tracks = TrackSelectionMerger(
        InputLocations=[long_tracks, velo_tracks]
    ).OutputLocation

    pvs = make_pvs()
    vertex_moni = TrackVertexMonitor(PVContainer=pvs, TrackContainer=tracks)

    return vertex_moni
