###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Postscaled (zero rate) monitoring lines for Offline DQ.

The module contains the following lines:
- Hlt2DQ_Passthrough (prescaled with plots from RecoMon)
- Hlt2DQ_DiPhoton (pi0 -> gamma gamma)
- Hlt2DQ_KsToPimPip_LL and DD (KS0 -> pi+ pi-)
- Hlt2DQ_L0ToPpPim_LL and DD ([Lambda0 -> p+ pi-]cc)
- Hlt2DQ_XimToL0Pim_LLL + DDL and DDD ([Xi- -> Lambda0 -> pi-]cc)
- Hlt2DQ_OmegamToL0Km_LLL and DDD ([Omega- -> Lambda0 -> K-]cc)
- Hlt2DQ_D0ToKmPip ([D0 -> K- pi+]cc)
- Hlt2DQ_DstpToD0Pip ([D*(2010)+ -> D0 pi+]cc)
- Hlt2DQ_DpToKmPipPip ([D+ -> K- pi+ pi+]cc)
- Hlt2DQ_DspToPhiPip ([D_s+ -> K- K+ pi+]cc)
- Hlt2DQ_LcpToPpKmPip ([Lambda_c+ -> p+ K- pi+]cc)
- Hlt2DQ_JpsiToMumMup (J/psi(1S) -> mu+ mu-)
"""

import Functors as F
from Functors.math import log
from Moore import options
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf.Algorithms import (
    HltRoutingBitsFilter,
    MonitorDetectorCorrelations,
    TrackMonitor,
    TrackPV2HalfMonitor,
    TrackSelectionMerger,
    TrackVertexMonitor,
)
from PyConf.Algorithms import Rich__Future__Rec__Moni__DLLs as DLLs
from PyConf.Algorithms import (
    Rich__Future__Rec__Moni__SIMDPhotonCherenkovAngles as PhotAngles,
)
from PyConf.application import default_raw_banks, make_odin
from PyConf.dataflow import DataHandle
from PyConf.reading import get_decreports
from RecoConf import monitoring_particles
from RecoConf.event_filters import require_pvs

# TODO: support all types of UTHits in MonitorDetectorCorrelations
from RecoConf.legacy_rec_hlt1_tracking import make_PrStoreUTHit_empty_hits
from RecoConf.reconstruction_objects import make_pvs, reconstruction
from SelAlgorithms.monitoring import histogram_1d, monitor


def _dq_filters(unbiased=False):
    rb_filter = HltRoutingBitsFilter(
        RawBanks=default_raw_banks("HltRoutingBits"),
        RequireMask=(1 << 14, 0, 0),
        PassOnError=False,
    )
    rb_filter_lumi = HltRoutingBitsFilter(
        RawBanks=default_raw_banks("HltRoutingBits"),
        RequireMask=(1 << 1, 0, 0),
        PassOnError=False,
    )
    filters = []
    try:
        if options.input_type == "Online":
            if unbiased:
                filters.append(rb_filter_lumi)
            else:
                filters.append(rb_filter)
    except AttributeError:
        # LbExec case
        pass
    return filters


def _HLT1_RE_DEC(line_re: str):
    return F.DECREPORTS_RE_FILTER(get_decreports("Hlt1"), line_re)


def _ASYM(functor, child_idx_1=1, child_idx_2=2):
    return (F.CHILD(child_idx_1, functor) - F.CHILD(child_idx_2, functor)) / (
        F.CHILD(child_idx_1, functor) + F.CHILD(child_idx_2, functor)
    )


def _CHILDPID_K(i=1):
    return F.CHILD(i, F.PID_K)


def _CHILDPID_P(i=1):
    return F.CHILD(i, F.PID_P)


def _CHILDPID_PK(i=1):
    return F.CHILD(i, F.PID_P) - F.CHILD(i, F.PID_K)


def _get_inputs(data_handle: DataHandle):
    return data_handle.producer.inputs


def _get_properties(data_handle: DataHandle):
    return data_handle.producer.properties


all_lines = {}


# don't register this for now, as none of the monitors is threadsafe
def _dq_richmon(name="Hlt2DQ_RICHMon", prescale=1):
    richmon = []

    # we have to traverse the data flow to configure the cherenkov angle plots with the proper reconstruction objects
    long_rich_pids = reconstruction()["LongRichPIDs"]
    long_rich_pids_props = _get_properties(long_rich_pids)
    long_rich_pids_inputs = _get_inputs(long_rich_pids)
    long_rich_pids_recsummary = long_rich_pids_inputs["SummaryTracksLocation"]
    long_rich_pids_recsummary_inputs = _get_inputs(long_rich_pids_recsummary)

    # leave the Cherenkov resoluiton plots out for now as the algorithm is slow(er than e.g. 3xHybridSeeding)
    # we can probably live with the online version of the plot for DQ
    long_rich_pids_photon_signals_inputs = _get_inputs(
        long_rich_pids_recsummary_inputs["PhotonSignalsLocation"]
    )
    richmon.append(
        PhotAngles(
            name="DQ_RiCKResLong",
            Detectors=long_rich_pids_props["Detectors"],
            Radiators=long_rich_pids_props["Radiators"],
            CKResHistoRange=(0.025, 0.0026, 0.002),
            TracksLocation=long_rich_pids_inputs["TracksLocation"],
            TrackSegmentsLocation=long_rich_pids_recsummary_inputs[
                "TrackSegmentsLocation"
            ],
            CherenkovPhotonLocation=long_rich_pids_photon_signals_inputs[
                "CherenkovPhotonLocation"
            ],
            CherenkovAnglesLocation=long_rich_pids_photon_signals_inputs[
                "CherenkovAnglesLocation"
            ],
            SummaryTracksLocation=long_rich_pids_recsummary,
            PhotonToParentsLocation=long_rich_pids_recsummary_inputs[
                "PhotonToParentsLocation"
            ],
        )
    )
    richmon.append(
        DLLs(
            name="DQ_RichDLLsLong",
            Detectors=long_rich_pids_props["Detectors"],
            Radiators=long_rich_pids_props["Radiators"],
            RichPIDsLocation=long_rich_pids,
        )
    )

    return Hlt2Line(
        name=name,
        algs=_dq_filters() + richmon,
        postscale=0,
        persistreco=True,
        monitoring_variables=(),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dq_pv_2half_mon(name="Hlt2DQ_TrackPV2HalfMonitor", prescale=1):
    mon = TrackPV2HalfMonitor(
        name="DQ_TrackPV2HalfMonitor",
        TrackContainer=reconstruction()["AllTrackHandles"]["Velo"]["v1"],
        ODINLocation=make_odin(),
        allow_duplicate_instances_with_distinct_names=True,
    )

    return Hlt2Line(
        name=name,
        algs=_dq_filters() + [mon],
        postscale=0,
        persistreco=True,
        monitoring_variables=(),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def dq_pv_2half_mon_nobias(name="Hlt2DQ_TrackPV2HalfMonitor_NoBias", prescale=1):
    mon = TrackPV2HalfMonitor(
        name="DQ_TrackPV2HalfMonitor_NoBias",
        TrackContainer=reconstruction()["AllTrackHandles"]["Velo"]["v1"],
        ODINLocation=make_odin(),
        allow_duplicate_instances_with_distinct_names=True,
    )

    return Hlt2Line(
        name=name,
        algs=_dq_filters(unbiased=True) + [mon],
        postscale=0,
        persistreco=True,
        monitoring_variables=(),
        hlt1_filter_code=["Hlt1ODINLumiDecision"],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def _dq_tracking(name="Hlt2DQ_TrackMonitors", prescale=0.01):
    pvs = make_pvs()

    all_tracks = reconstruction()["AllTrackHandles"]

    trackmon = []

    calo_objects = reconstruction()["AllCaloHandles"]

    trackmon.append(
        MonitorDetectorCorrelations(
            name="DQ_MonitorDetectorCorrelations",
            VeloHits=all_tracks["Velo"]["Pr"].producer.HitsLocation,
            # TODO: there are now 3 different types of UT hits.
            # MonitorDetectorCorrelations uses the first one of https://gitlab.cern.ch/lhcb/Rec/-/blob/51834236b66bee0ab92b763973a64e85be466f05/Pr/PrAlgorithms/src/PrStoreUTHit.cpp#L231
            # while the reconstruction uses SoA hits
            # UTHits=_get_inputs(all_tracks["Downstream"]["Pr"])["UTHits"],
            UTHits=make_PrStoreUTHit_empty_hits(),
            SciFiHits=_get_inputs(all_tracks["Seed"]["Pr"])["FTHitsLocation"],
            MuonHits=_get_inputs(
                _get_inputs(reconstruction()["LongMuonPIDs"])["InputMuonPIDs"]
            )["InputMuonHits"],
            ECALClusters=calo_objects["ecalClusters"],
            ECALDigits=calo_objects["digitsEcal"],
            HCALDigits=calo_objects["digitsHcal"],
            RichPixels=_get_inputs(
                _get_inputs(
                    _get_inputs(
                        _get_inputs(reconstruction()["LongRichPIDs"])[
                            "SummaryTracksLocation"
                        ]
                    )["RichSIMDPixelSummariesLocation"]
                )["RichPixelClustersLocation"]
            )["DecodedDataLocation"],
            CollisionType="pp",
            allow_duplicate_instances_with_distinct_names=True,
        )
    )

    # merge all best tracks
    all_best_tracks = TrackSelectionMerger(
        InputLocations=[v["v1"] for k, v in all_tracks.items() if k.startswith("Best")]
    ).OutputLocation

    trackmon.append(
        TrackVertexMonitor(
            name="DQ_TrackVertexMonitor",
            PVContainer=pvs,
            TrackContainer=all_best_tracks,
            allow_duplicate_instances_with_distinct_names=True,
        )
    )

    trackmon.append(
        TrackMonitor(
            name="DQ_TrackMonitor",
            TracksInContainer=all_best_tracks,
            allow_duplicate_instances_with_distinct_names=True,
        )
    )

    return Hlt2Line(
        name=name,
        algs=_dq_filters() + [require_pvs(pvs)] + trackmon,
        postscale=0,
        persistreco=True,
        monitoring_variables=(),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def _dq_tracking_nobias(name="Hlt2DQ_TrackMonitors_NoBias", prescale=1):
    pvs = make_pvs()

    all_tracks = reconstruction()["AllTrackHandles"]

    trackmon = []

    calo_objects = reconstruction()["AllCaloHandles"]

    trackmon.append(
        MonitorDetectorCorrelations(
            name="DQ_MonitorDetectorCorrelations_NoBias",
            VeloHits=all_tracks["Velo"]["Pr"].producer.HitsLocation,
            # TODO: there are now 3 different types of UT hits.
            # MonitorDetectorCorrelations uses the first one of https://gitlab.cern.ch/lhcb/Rec/-/blob/51834236b66bee0ab92b763973a64e85be466f05/Pr/PrAlgorithms/src/PrStoreUTHit.cpp#L231
            # while the reconstruction uses SoA hits
            # UTHits=_get_inputs(all_tracks["Downstream"]["Pr"])["UTHits"],
            UTHits=make_PrStoreUTHit_empty_hits(),
            SciFiHits=_get_inputs(all_tracks["Seed"]["Pr"])["FTHitsLocation"],
            MuonHits=_get_inputs(
                _get_inputs(reconstruction()["LongMuonPIDs"])["InputMuonPIDs"]
            )["InputMuonHits"],
            ECALClusters=calo_objects["ecalClusters"],
            ECALDigits=calo_objects["digitsEcal"],
            HCALDigits=calo_objects["digitsHcal"],
            RichPixels=_get_inputs(
                _get_inputs(
                    _get_inputs(
                        _get_inputs(reconstruction()["LongRichPIDs"])[
                            "SummaryTracksLocation"
                        ]
                    )["RichSIMDPixelSummariesLocation"]
                )["RichPixelClustersLocation"]
            )["DecodedDataLocation"],
            CollisionType="pp",
            allow_duplicate_instances_with_distinct_names=True,
        )
    )

    # merge all best tracks
    all_best_tracks = TrackSelectionMerger(
        InputLocations=[v["v1"] for k, v in all_tracks.items() if k.startswith("Best")]
    ).OutputLocation

    trackmon.append(
        TrackVertexMonitor(
            name="DQ_TrackVertexMonitor_NoBias",
            PVContainer=pvs,
            TrackContainer=all_best_tracks,
            allow_duplicate_instances_with_distinct_names=True,
        )
    )

    trackmon.append(
        TrackMonitor(
            name="DQ_TrackMonitor_NoBias",
            TracksInContainer=all_best_tracks,
            allow_duplicate_instances_with_distinct_names=True,
        )
    )

    return Hlt2Line(
        name=name,
        algs=_dq_filters(unbiased=True) + [require_pvs(pvs)] + trackmon,
        postscale=0,
        persistreco=True,
        monitoring_variables=(),
        hlt1_filter_code=["Hlt1ODINLumiDecision"],
        prescale=prescale,
    )


@register_line_builder(all_lines)
def _diphoton_line(name="Hlt2DQ_DiPhoton", prescale=1):
    return Hlt2Line(
        name=name,
        algs=_dq_filters() + [monitoring_particles.diphoton()],
        postscale=0,
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def _kshort_ll_line(name="Hlt2DQ_KsToPimPip_LL", prescale=1):
    ks = monitoring_particles.prompt_ks_ll()

    mon = monitor(
        data=ks,
        histograms=[
            histogram_1d(
                functor=log(F.OWNPVFD),
                name=f"/{name}/Ks_FDlog",
                title="Ks_FDlog",
                label="log(KS FD [mm])",
                bins=100,
                range=(1, 8),
            ),
            histogram_1d(
                functor=F.MASS * _HLT1_RE_DEC(r"Hlt1.*Ks.*Decision"),
                name=f"/{name}/m_Hlt1_Ks_DecTOS",
                title="m_Hlt1_Ks_DecTOS",
                label="m(pi- pi+) [MeV]",
                bins=100,
                range=(450, 550),
            ),
            histogram_1d(
                functor=F.OWNPVDLS,
                name=f"/{name}/Ks_DLS",
                title="Ks_DLS",
                label="KS DLS",
                bins=100,
                range=(-200, 1800),
            ),
        ],
    )

    return Hlt2Line(
        name=name,
        algs=_dq_filters() + [require_pvs(make_pvs())] + [ks, mon],
        postscale=0,
        persistreco=True,
        monitoring_variables=(
            "pt",
            "eta",
            "phi",
            "m",
            "vchi2",
            "ipchi2",
            "n_candidates",
        ),
        prescale=prescale,
    )


@register_line_builder(all_lines)
def _kshort_dd_line(name="Hlt2DQ_KsToPimPip_DD", prescale=1):
    ks = monitoring_particles.prompt_ks_dd()

    mon = monitor(
        data=ks,
        histograms=[
            histogram_1d(
                functor=log(F.OWNPVFD),
                name=f"/{name}/Ks_FDlog",
                title="Ks_FDlog",
                label="log(KS FD [mm])",
                bins=100,
                range=(5, 9),
            )
        ],
    )

    return Hlt2Line(
        name=name,
        algs=_dq_filters() + [require_pvs(make_pvs())] + [ks, mon],
        postscale=0,
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def _kshort_uu_line(name="Hlt2DQ_KsToPimPip_UU", prescale=1):
    ks = monitoring_particles.prompt_ks_uu()

    mon = monitor(
        data=ks,
        histograms=[
            histogram_1d(
                functor=log(F.OWNPVFD),
                name=f"/{name}/Ks_FDlog",
                title="Ks_FDlog",
                label="log(KS FD [mm])",
                bins=100,
                range=(1, 8),
            ),
            histogram_1d(
                functor=F.MASS,
                name=f"/{name}/m_Ks_UU",
                title="m_Ks",
                label="m(pi- pi+) [MeV]",
                bins=100,
                range=(300, 700),
            ),
        ],
    )

    return Hlt2Line(
        name=name,
        algs=_dq_filters() + [require_pvs(make_pvs())] + [ks, mon],
        postscale=0,
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def _lambda_ll_line(name="Hlt2DQ_L0ToPpPim_LL", prescale=1):
    lz = monitoring_particles.prompt_lambda_ll()
    partial_mass_dict = {
        "title": name,
        "label": "m(p #pi^{-}) [MeV]",
        "bins": 120,
        "range": (1080, 1140),
    }

    mon = monitor(
        data=lz,
        histograms=[
            histogram_1d(
                functor=log(F.OWNPVFD),
                name=f"/{name}/Lambda_FDlog",
                title="Lambda_FDlog",
                label="log(Lambda FD [mm])",
                bins=90,
                range=(1, 8),
            ),
            histogram_1d(
                functor=F.CHILD(1, F.GHOSTPROB),
                name=f"/{name}/p_ghostprob",
                title="p_GHOSTPROB",
                label="Ghostprob (p)",
                bins=100,
                range=(0, 1),
            ),
            histogram_1d(
                functor=F.CHILD(2, F.GHOSTPROB),
                name=f"/{name}/pi_ghostprob",
                title="pi_GHOSTPROB",
                label="Ghostprob (pi)",
                bins=100,
                range=(0, 1),
            ),
            histogram_1d(
                functor=F.CHILD(1, F.PROBNN_P),
                name=f"/{name}/p_ProbNNp",
                title="p_ProbNNp",
                label="ProbNNp (p)",
                bins=100,
                range=(0, 1),
            ),
            histogram_1d(
                functor=F.CHILD(2, F.PROBNN_PI),
                name=f"/{name}/pi_ProbNNpi",
                title="pi_ProbNNpi",
                label="ProbNNpi (pi)",
                bins=100,
                range=(0, 1),
            ),
            histogram_1d(functor=F.MASS, name=f"/{name}/m_L", **partial_mass_dict),
            histogram_1d(
                functor=F.MASS * (F.PARTICLE_ID > 0),
                name=f"/{name}/m_Lambda",
                **partial_mass_dict,
            ),
            histogram_1d(
                functor=F.MASS * (F.PARTICLE_ID < 0),
                name=f"/{name}/m_Lambdabar",
                **partial_mass_dict,
            ),
        ],
    )

    return Hlt2Line(
        name=name,
        algs=_dq_filters() + [require_pvs(make_pvs())] + [lz, mon],
        postscale=0,
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def _lambda_dd_line(name="Hlt2DQ_L0ToPpPim_DD", prescale=1):
    lz = monitoring_particles.prompt_lambda_dd()
    partial_mass_dict = {
        "title": name,
        "label": "m(p #pi^{-}) [MeV]",
        "bins": 120,
        "range": (1080, 1140),
    }
    mon = monitor(
        data=lz,
        histograms=[
            histogram_1d(
                functor=log(F.OWNPVFD),
                name=f"/{name}/Lambda_FDlog",
                title="Lambda_FDlog",
                label="log(Lambda FD [mm])",
                bins=90,
                range=(5, 9),
            ),
            histogram_1d(
                functor=F.CHILD(1, F.GHOSTPROB),
                name=f"/{name}/p_ghostprob",
                title="p_GHOSTPROB",
                label="Ghostprob (p)",
                bins=100,
                range=(0, 1),
            ),
            histogram_1d(
                functor=F.CHILD(2, F.GHOSTPROB),
                name=f"/{name}/pi_ghostprob",
                title="pi_GHOSTPROB",
                label="Ghostprob (pi)",
                bins=100,
                range=(0, 1),
            ),
            histogram_1d(functor=F.MASS, name=f"/{name}/m_L", **partial_mass_dict),
            histogram_1d(
                functor=F.MASS * (F.PARTICLE_ID > 0),
                name=f"/{name}/m_Lambda",
                **partial_mass_dict,
            ),
            histogram_1d(
                functor=F.MASS * (F.PARTICLE_ID < 0),
                name=f"/{name}/m_Lambdabar",
                **partial_mass_dict,
            ),
            histogram_1d(
                functor=F.MASS * (_CHILDPID_P() > 0),
                name=f"/{name}/m_p_pidp_gt_0",
                **partial_mass_dict,
            ),
            histogram_1d(
                functor=F.MASS * (_CHILDPID_P() > 5),
                name=f"/{name}/m_p_pidp_gt_5",
                **partial_mass_dict,
            ),
            histogram_1d(
                functor=F.MASS * (_CHILDPID_PK() > 0),
                name=f"/{name}/m_p_pidpk_gt_0",
                **partial_mass_dict,
            ),
            histogram_1d(
                functor=F.MASS * (_CHILDPID_PK() > 5),
                name=f"/{name}/m_p_pidpk_gt_5",
                **partial_mass_dict,
            ),
            histogram_1d(
                functor=F.MASS * (_CHILDPID_K(2) < 0),
                name=f"/{name}/m_pi_pidk_lt_0",
                **partial_mass_dict,
            ),
            histogram_1d(
                functor=F.MASS * (_CHILDPID_K(2) < -5),
                name=f"/{name}/m_pi_pidk_lt_m5",
                **partial_mass_dict,
            ),
        ],
    )
    return Hlt2Line(
        name=name,
        algs=_dq_filters() + [require_pvs(make_pvs())] + [lz, mon],
        postscale=0,
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def _xi_lll_line(name="Hlt2DQ_XimToL0Pim_LLL", prescale=1):
    xi = monitoring_particles.prompt_xi_lll()
    mon = monitor(
        data=xi,
        histograms=[
            histogram_1d(
                functor=log(F.OWNPVFD),
                name=f"/{name}/Xi_FDlog",
                title="Xi_FDlog",
                label="log(#Xi^{-} FD [mm])",
                bins=100,
                range=(1, 8),
            ),
            histogram_1d(
                functor=F.MASS,
                name=f"/{name}/m_Xi",
                title="m_Xi",
                label="m(#Lambda #pi^{-}) [MeV]",
                bins=120,
                range=(1260, 1380),
            ),
            histogram_1d(
                functor=F.MASS * _HLT1_RE_DEC("Hlt1XiOmegaLLLDecision"),
                name=f"/{name}/m_Hlt1_Xi_DecTOS",
                title="m_Hlt1_Xi_DecTOS",
                label="m(#Lambda #pi^{-}) [MeV]",
                bins=120,
                range=(1260, 1380),
            ),
        ],
    )
    return Hlt2Line(
        name=name,
        algs=_dq_filters() + [require_pvs(make_pvs())] + [xi, mon],
        postscale=0,
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def _xi_ddl_line(name="Hlt2DQ_XimToL0Pim_DDL", prescale=1):
    xi = monitoring_particles.prompt_xi_ddl()
    mon = monitor(
        data=xi,
        histograms=[
            histogram_1d(
                functor=log(F.OWNPVFD),
                name=f"/{name}/Xi_FDlog",
                title="Xi_FDlog",
                label="log(#Xi^{-} FD [mm])",
                bins=100,
                range=(1, 8),
            ),
            histogram_1d(
                functor=F.MASS,
                name=f"/{name}/m_Xi",
                title="m_Xi",
                label="m(#Lambda #pi^{-}) [MeV]",
                bins=120,
                range=(1260, 1380),
            ),
        ],
    )
    return Hlt2Line(
        name=name,
        algs=_dq_filters() + [require_pvs(make_pvs())] + [xi, mon],
        postscale=0,
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def _xi_ddd_line(name="Hlt2DQ_XimToL0Pim_DDD", prescale=1):
    xi = monitoring_particles.prompt_xi_ddd()
    mon = monitor(
        data=xi,
        histograms=[
            histogram_1d(
                functor=log(F.OWNPVFD),
                name=f"/{name}/Xi_FDlog",
                title="Xi_FDlog",
                label="log(#Xi^{-} FD [mm])",
                bins=100,
                range=(1, 8),
            ),
            histogram_1d(
                functor=F.MASS,
                name=f"/{name}/m_Xi",
                title="m_Xi",
                label="m(#Lambda #pi^{-}) [MeV]",
                bins=120,
                range=(1260, 1380),
            ),
        ],
    )
    return Hlt2Line(
        name=name,
        algs=_dq_filters() + [require_pvs(make_pvs())] + [xi, mon],
        postscale=0,
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def _omega_lll_line(name="Hlt2DQ_OmegamToL0Km_LLL", prescale=1):
    omega = monitoring_particles.prompt_omega_lll()
    mon = monitor(
        data=omega,
        histograms=[
            histogram_1d(
                functor=log(F.OWNPVFD),
                name=f"/{name}/Omega_FDlog",
                title="Omega_FDlog",
                label="log(#Omega^{-} FD [mm])",
                bins=100,
                range=(1, 8),
            ),
            histogram_1d(
                functor=F.MASS,
                name=f"/{name}/m_Omega",
                title="m_Omega",
                label="m(#Lambda K^{-}) [MeV]",
                bins=65,
                range=(1610, 1740),
            ),
            histogram_1d(
                functor=F.MASS * _HLT1_RE_DEC("Hlt1XiOmegaLLLDecision"),
                name=f"/{name}/m_Hlt1_Omega_DecTOS",
                title="m_Hlt1_Omega_DecTOS",
                label="m(#Lambda K^{-}) [MeV]",
                bins=65,
                range=(1610, 1740),
            ),
        ],
    )
    return Hlt2Line(
        name=name,
        algs=_dq_filters() + [require_pvs(make_pvs())] + [omega, mon],
        postscale=0,
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def _omega_ddd_line(name="Hlt2DQ_OmegamToL0Km_DDD", prescale=1):
    omega = monitoring_particles.prompt_omega_ddd()
    mon = monitor(
        data=omega,
        histograms=[
            histogram_1d(
                functor=log(F.OWNPVFD),
                name=f"/{name}/Omega_FDlog",
                title="Omega_FDlog",
                label="log(#Omega^{-} FD [mm])",
                bins=100,
                range=(1, 8),
            ),
            histogram_1d(
                functor=F.MASS,
                name=f"/{name}/m_Omega",
                title="m_Omega",
                label="m(#Lambda K^{-}) [MeV]",
                bins=65,
                range=(1610, 1740),
            ),
            histogram_1d(
                functor=F.MASS * (_CHILDPID_K(2) > 0),
                name=f"/{name}/m_k_pidk_gt_0",
                title="m_Omega",
                label="m(#Lambda K^{-}) [MeV]",
                bins=65,
                range=(1610, 1740),
            ),
            histogram_1d(
                functor=F.MASS * (_CHILDPID_K(2) > 5),
                name=f"/{name}/m_k_pidk_gt_5",
                title="m_Omega",
                label="m(#Lambda K^{-}) [MeV]",
                bins=65,
                range=(1610, 1740),
            ),
        ],
    )
    return Hlt2Line(
        name=name,
        algs=_dq_filters() + [require_pvs(make_pvs())] + [omega, mon],
        postscale=0,
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def _d0_to_kpi_line(name="Hlt2DQ_D0ToKmPip", prescale=1):
    d0 = monitoring_particles.prompt_d0()

    def _d0_hists(tos: bool):
        return [
            histogram_1d(
                functor=F.MASS * (_HLT1_RE_DEC("Hlt1D2KPiDecision") if tos else 1),
                name=f"/{name}/D0_M{'_Hlt1D2KPiTOS' if tos else ''}",
                bins=66,
                range=(1700, 2030),
                title="",
                label="m(K^{#minus}#pi^{+}) [MeV]",
            ),
            histogram_1d(
                functor=F.PT * (_HLT1_RE_DEC("Hlt1D2KPiDecision") if tos else 1),
                name=f"/{name}/D0_PT{'_Hlt1D2KPiTOS' if tos else ''}",
                bins=75,
                range=(1500, 31500),
                title="",
                label="p_{T} (D^{0})",
            ),
            histogram_1d(
                functor=F.ETA * (_HLT1_RE_DEC("Hlt1D2KPiDecision") if tos else 1),
                name=f"/{name}/D0_ETA{'_Hlt1D2KPiTOS' if tos else ''}",
                bins=100,
                range=(1.5, 5.5),
                title="",
                label="#eta (D^{0})",
            ),
            histogram_1d(
                functor=log(F.OWNPVIPCHI2)
                + (0 if not tos else 9999 * (_HLT1_RE_DEC("Hlt1D2KPiDecision") - 1)),
                name=f"/{name}/D0_logIPCHI2{'_Hlt1D2KPiTOS' if tos else ''}",
                bins=120,
                range=(-6, 12),
                title="",
                label="log(#chi^{2}_{IP}) (D^{0})",
            ),
            histogram_1d(
                functor=_ASYM(F.P)
                + (0 if not tos else 9999 * (_HLT1_RE_DEC("Hlt1D2KPiDecision") - 1)),
                name=f"/{name}/D0_Daughters_Pasy{'_Hlt1D2KPiTOS' if tos else ''}",
                bins=120,
                range=(-1, 1),
                title="",
                label="p asymmetry (K,#pi)_{D^{0}}",
            ),
            histogram_1d(
                functor=_ASYM(F.PT)
                + (0 if not tos else 9999 * (_HLT1_RE_DEC("Hlt1D2KPiDecision") - 1)),
                name=f"/{name}/D0_Daughters_PTasy{'_Hlt1D2KPiTOS' if tos else ''}",
                bins=60,
                range=(-1, 1),
                title="",
                label="p_{T} asymmetry (K,#pi)_{D^{0}}",
            ),
            histogram_1d(
                functor=F.CHILD(1, F.PID_K)
                * (_HLT1_RE_DEC("Hlt1D2KPiDecision") if tos else 1),
                name=f"/{name}/K_PIDk{'_Hlt1D2KPiTOS' if tos else ''}",
                bins=60,
                range=(5, 150),
                title="",
                label="PIDK (K)",
            ),
            histogram_1d(
                functor=F.CHILD(2, F.PID_K)
                + (0 if not tos else 9999 * (_HLT1_RE_DEC("Hlt1D2KPiDecision") - 1)),
                name=f"/{name}/pi_PIDk{'_Hlt1D2KPiTOS' if tos else ''}",
                bins=60,
                range=(-150, 5),
                title="",
                label="PIDK (#pi)",
            ),
            histogram_1d(
                functor=F.CHILD(1, F.PROBNN_K)
                + (0 if not tos else 9999 * (_HLT1_RE_DEC("Hlt1D2KPiDecision") - 1)),
                name=f"/{name}/K_ProbNNk{'_Hlt1D2KPiTOS' if tos else ''}",
                title="K_ProbNNk",
                label="ProbNNk (K)",
                bins=100,
                range=(0, 1),
            ),
            histogram_1d(
                functor=F.CHILD(2, F.PROBNN_PI)
                + (0 if not tos else 9999 * (_HLT1_RE_DEC("Hlt1D2KPiDecision") - 1)),
                name=f"/{name}/pi_ProbNNpi{'_Hlt1D2KPiTOS' if tos else ''}",
                title="pi_ProbNNpi",
                label="ProbNNpi (pi)",
                bins=100,
                range=(0, 1),
            ),
        ]

    mon = monitor(data=d0, histograms=_d0_hists(tos=False) + _d0_hists(tos=True))
    return Hlt2Line(
        name=name,
        algs=_dq_filters() + [require_pvs(make_pvs())] + [d0, mon],
        postscale=0,
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def _dst_line(name="Hlt2DQ_DstpToD0Pip", prescale=1):
    return Hlt2Line(
        name=name,
        algs=_dq_filters()
        + [require_pvs(make_pvs())]
        + [monitoring_particles.prompt_dst()],
        postscale=0,
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def _dp_to_kpipi_line(name="Hlt2DQ_DpToKmPipPip", prescale=1):
    dp = monitoring_particles.prompt_dp()

    def _dp_hists(tos: bool):
        return [
            histogram_1d(
                functor=F.MASS
                * (_HLT1_RE_DEC(r"Hlt1.*TrackMVADecision") if tos else 1),
                name=f"/{name}/d_m{'_Hlt1TrackMVATOS' if tos else ''}",
                title="DMassMon",
                label="m(K^{#minus}#pi^{+}#pi^{+} + c.c.) [MeV]",
                bins=100,
                range=(1670, 2070),
            ),
            histogram_1d(
                functor=F.MASS
                * (
                    (F.PARTICLE_ID > 0)
                    * (_HLT1_RE_DEC(r"Hlt1.*TrackMVADecision") if tos else 1)
                ),
                name=f"/{name}/dplus_m{'_Hlt1TrackMVATOS' if tos else ''}",
                title="DplusMassMon",
                label="m(K^{#minus}#pi^{+}#pi^{+}) [MeV]",
                bins=100,
                range=(1670, 2070),
            ),
            histogram_1d(
                functor=F.MASS
                * (
                    (F.PARTICLE_ID < 0)
                    * (_HLT1_RE_DEC(r"Hlt1.*TrackMVADecision") if tos else 1)
                ),
                name=f"/{name}/dminus_m{'_Hlt1TrackMVATOS' if tos else ''}",
                title="DminusMassMon",
                label="m(K^{+}#pi^{#minus}#pi^{#minus}) [MeV]",
                bins=200,
                range=(1670, 2070),
            ),
            histogram_1d(
                functor=F.PT * (_HLT1_RE_DEC(r"Hlt1.*TrackMVADecision") if tos else 1),
                name=f"/{name}/D_PT{'_Hlt1TrackMVATOS' if tos else ''}",
                bins=150,
                range=(1500, 31500),
                title="",
                label="p_{T} (D^{+})",
            ),
            histogram_1d(
                functor=F.ETA * (_HLT1_RE_DEC(r"Hlt1.*TrackMVADecision") if tos else 1),
                name=f"/{name}/D_ETA{'_Hlt1TrackMVATOS' if tos else ''}",
                bins=100,
                range=(1.5, 5.5),
                title="",
                label="#eta (D^{+})",
            ),
            histogram_1d(
                functor=F.CHI2DOF
                + (
                    0
                    if not tos
                    else 9999 * (_HLT1_RE_DEC(r"Hlt1.*TrackMVADecision") - 1)
                ),
                name=f"/{name}/D_PT{'_Hlt1TrackMVATOS' if tos else ''}",
                bins=120,
                range=(0, 6),
                title="",
                label="#chi^2_{Vertex} (D^{+})",
            ),
            histogram_1d(
                functor=log(F.OWNPVIPCHI2)
                + (
                    0
                    if not tos
                    else 9999 * (_HLT1_RE_DEC(r"Hlt1.*TrackMVADecision") - 1)
                ),
                name=f"/{name}/D_logIPCHI2{'_Hlt1TrackMVATOS' if tos else ''}",
                bins=120,
                range=(-6, 12),
                title="",
                label="log(#chi^{2}_{IP}) (D^{+})",
            ),
        ]

    mon = monitor(data=dp, histograms=_dp_hists(tos=False) + _dp_hists(tos=True))
    return Hlt2Line(
        name=name,
        algs=_dq_filters() + [require_pvs(make_pvs())] + [dp, mon],
        postscale=0,
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def _ds_line(name="Hlt2DQ_DspToPhiPip", prescale=1):
    return Hlt2Line(
        name=name,
        algs=_dq_filters()
        + [require_pvs(make_pvs())]
        + [monitoring_particles.prompt_ds()],
        postscale=0,
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def _lc_line(name="Hlt2DQ_LcpToPpKmPip", prescale=1):
    return Hlt2Line(
        name=name,
        algs=_dq_filters()
        + [require_pvs(make_pvs())]
        + [monitoring_particles.prompt_lc()],
        postscale=0,
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def _jpsi_to_mumu_line(name="Hlt2DQ_JpsiToMumMup", prescale=1):
    return Hlt2Line(
        name=name,
        algs=_dq_filters()
        + [require_pvs(make_pvs())]
        + [monitoring_particles.prompt_jpsi_to_mumu()],
        postscale=0,
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def _jpsi_to_ee_line(name="Hlt2DQ_JpsiToEmEp", prescale=1):
    jpsi = monitoring_particles.prompt_jpsi_to_ee()
    mon = monitor(
        data=jpsi,
        histograms=[
            histogram_1d(
                functor=F.MASS
                * (
                    F.require_any(
                        F.CHILD(1, F.HASBREMADDED) & F.CHILD(2, ~F.HASBREMADDED),
                        F.CHILD(2, F.HASBREMADDED) & F.CHILD(1, ~F.HASBREMADDED),
                    )
                ),
                name=f"/{name}/M_one_brem",
                title="Jpsi_m_one_brem",
                label="m(e^{+}e^{#minus}) [MeV]",
                bins=60,
                range=(2300, 3500),
            ),
            histogram_1d(
                functor=F.MASS
                * (F.CHILD(1, F.HASBREMADDED) & F.CHILD(2, F.HASBREMADDED)),
                name=f"/{name}/M_both_brem",
                title="Jpsi_m_both_brem",
                label="m(e^{+}e^{#minus}) [MeV]",
                bins=60,
                range=(2300, 3500),
            ),
            histogram_1d(
                functor=F.MASS,
                name=f"/{name}/M",
                title="Jpsi_m_any_brem",
                label="m(e^{+}e^{#minus}) [MeV]",
                bins=60,
                range=(2300, 3500),
            ),
        ],
    )

    return Hlt2Line(
        name=name,
        algs=_dq_filters() + [require_pvs(make_pvs())] + [jpsi, mon],
        postscale=0,
        persistreco=True,
        prescale=prescale,
    )
