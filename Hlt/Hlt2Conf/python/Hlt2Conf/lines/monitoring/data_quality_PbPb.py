###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Postscaled (zero rate) monitoring lines for DQ for PbPb data-taking.
The lines are not registered in __init__.py of the monitoring lines as these are special lines for PbPb data-taking.

The module contains the following lines:
- Hlt2DQ_KsToPimPip_LL (KS0 -> pi+ pi-)
- Hlt2DQ_L0ToPpPim_LL ([Lambda0 -> p+ pi-]cc)
- Hlt2DQ_D0ToKmPip ([D0 -> K- pi+]cc)
- Hlt2DQ_DpToKmPipPip ([D+ -> K- pi+ pi+]cc)
- Hlt2DQ_JpsiToMumMup (J/psi(1S) -> mu+ mu-)
- Hlt2DQ_JpsiToEmEp (J/psi(1S) -> e+ e-)
"""

import Functors as F
from Functors import SIZE
from GaudiKernel.SystemOfUnits import GeV, MeV
from GaudiKernel.SystemOfUnits import micrometer as um
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf.Algorithms import VoidFilter
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.event_filters import require_gec, require_pvs
from RecoConf.legacy_rec_hlt1_tracking import make_FTRawBankDecoder_clusters
from RecoConf.monitoring_particles import prompt_ks_ll, prompt_lambda_ll
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import make_ismuon_long_muon

from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters


def _pbpb_prefilters():
    return [require_gec(cut=30_000, skipUT=True), require_pvs(make_pvs())]


def high_fthits_filter(nftClusters=20000):
    ft_clusters = make_FTRawBankDecoder_clusters()
    return [VoidFilter(name="high_fthits_filter", Cut=SIZE(ft_clusters) > nftClusters)]


def prompt_jpsi_to_mumu():
    mu = ParticleFilter(
        make_ismuon_long_muon(),
        F.FILTER(
            F.require_all(
                F.PT > 700 * MeV,
                F.MINIP(make_pvs()) < 80 * um,
                F.MINIPCHI2(make_pvs()) < 6,
                F.PID_MU > 0,
            )
        ),
    )
    return ParticleCombiner(
        [mu, mu],
        name="Monitoring_JpsiToMumMup",
        DecayDescriptor="J/psi(1S) -> mu+ mu-",
        CombinationCut=F.require_all(
            F.math.in_range(2.9 * GeV, F.MASS, 3.3 * GeV),
            F.PT > 0.8 * GeV,
            F.MAXSDOCACUT(80 * um),
        ),
        CompositeCut=F.require_all(
            F.math.in_range(2.9 * GeV, F.MASS, 3.3 * GeV),
            F.PT > 0.8 * GeV,
            F.CHI2DOF < 6.0,
            F.OWNPVIPCHI2 < 9.0,
            F.OWNPVIP < 100 * um,
        ),
    )


all_lines = {}


@register_line_builder(all_lines)
def kshort_ll_smog2_line(name="Hlt2DQ_SMOG2_KsToPimPip_LL"):
    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters() + make_smog2_prefilters() + [prompt_ks_ll()],
        postscale=0.0,
        persistreco=True,
    )


@register_line_builder(all_lines)
def lambda_ll_smog2_line(name="Hlt2DQ_SMOG2_L0ToPpPim_LL"):
    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters() + make_smog2_prefilters() + [prompt_lambda_ll()],
        postscale=0.0,
        persistreco=True,
    )


@register_line_builder(all_lines)
def kshort_ll_pbpb_line(name="Hlt2DQ_PbPb_KsToPimPip_LL"):
    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters() + [prompt_ks_ll()],
        postscale=0.0,
        persistreco=True,
    )


@register_line_builder(all_lines)
def lambda_ll_pbpb_line(name="Hlt2DQ_PbPb_L0ToPpPim_LL"):
    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters() + [prompt_lambda_ll()],
        postscale=0.0,
        persistreco=True,
    )


@register_line_builder(all_lines)
def kshort_ll_pbpb_hm_line(name="Hlt2DQ_PbPb_HighMult_KsToPimPip_LL"):
    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters()
        + high_fthits_filter(nftClusters=20000)
        + [prompt_ks_ll()],
        postscale=0.0,
        persistreco=True,
    )


@register_line_builder(all_lines)
def jpsi_to_mumu_smog2_line(name="Hlt2DQ_SMOG2_JpsiToMumMup", prescale=1):
    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters() + make_smog2_prefilters() + [prompt_jpsi_to_mumu()],
        postscale=0,
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(all_lines)
def jpsi_to_mumu_pbpb_line(name="Hlt2DQ_PbPb_JpsiToMumMup", prescale=1):
    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters() + [prompt_jpsi_to_mumu()],
        postscale=0,
        persistreco=True,
        prescale=prescale,
    )
