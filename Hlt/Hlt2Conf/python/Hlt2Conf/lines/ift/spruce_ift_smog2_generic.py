###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Generic SMOG2 Sprucing high pt lines: high pt single hadron, generic two body, 2-body V0
"""

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, picosecond
from Moore.config import register_line_builder
from Moore.lines import SpruceLine
from PyConf import configurable
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import make_has_rich_long_pions, make_long_pions

from Hlt2Conf.lines.ift.builders.smog2_builders import (
    make_smog2_common_particles,
    make_smog2_prefilters,
)
from Hlt2Conf.lines.ift.builders.smog2_generic_builders import (
    make_smog2_generic_2body,
    make_smog2_generic_3body,
)

PROCESS = "spruce"
sprucing_lines = {}


@register_line_builder(sprucing_lines)
@configurable
def smog2_singletrack_highpt_spruceline(
    name="SpruceIFT_SMOG2SingleTrackHighPT",
    prescale=0.07,
    persistreco=True,
    min_pt=4 * GeV,
):
    """
    Line requiring one single track with pt>2.GeV comming from the SMOG2 region
    """
    pvs = make_pvs

    highp_track = make_smog2_common_particles(
        make_long_pions,
        max_trchi2dof=5,
        min_p=0 * MeV,
        min_pt=min_pt,
        pid_cut=None,
        particle=None,
        pvs=make_pvs,
        min_bpvipchi2=None,
        min_bpvip=None,
        max_bpvipchi2=50,
        max_bpvip=None,
        pvinsmog2=True,
    )

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [highp_track],
        hlt2_filter_code="Hlt2IFTFull_SMOG2SingleTrackHighPTDecision",
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(sprucing_lines)
@configurable
def smog2_singletrack_veryhighpt_spruceline(
    name="SpruceIFT_SMOG2SingleTrackVeryHighPT",
    prescale=0.1,
    persistreco=True,
    min_pt=6 * GeV,
):
    """
    Line requiring one single track with pt>4GeV comming from the SMOG2 region
    """
    pvs = make_pvs

    veryhighp_track = make_smog2_common_particles(
        make_long_pions,
        max_trchi2dof=5,
        min_p=0 * GeV,
        min_pt=min_pt,
        pid_cut=None,
        particle=None,
        pvs=make_pvs,
        min_bpvipchi2=None,
        min_bpvip=None,
        max_bpvipchi2=50,
        max_bpvip=None,
        pvinsmog2=True,
    )

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [veryhighp_track],
        hlt2_filter_code="Hlt2IFTFull_SMOG2SingleTrackVeryHighPTDecision",
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(sprucing_lines)
@configurable
def smog2_generic2bodydetached_spruceline(
    name="SpruceIFT_SMOG2Detached2Body",
    prescale=0.05,
    persistreco=True,
):
    """
    Detached generic 2-body decay coming from the SMOG2 region
    """
    pvs = make_pvs
    generic_particle = make_smog2_common_particles(
        make_has_rich_long_pions,
        max_trchi2dof=3,
        min_p=5000 * MeV,
        min_pt=1 * GeV,
        pvs=make_pvs,
    )

    generic_2body = make_smog2_generic_2body(
        pvs,
        generic_particle,
        generic_particle,
        name="generic_2body_smog2",
        descriptor="B0 -> pi- pi+",
        apt_min=0 * GeV,
        maxsdoca=0.2 * mm,
        vchi2pdof_max=25,
        bpvvdchi2_min=40,
        single_pt_min=2 * GeV,
        mipchi2_min=20,
        sv_insmog=True,
        minmass=3.5 * GeV,
        min_bpvltime=1 * picosecond,
    )

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [generic_2body],
        hlt2_filter_code="Hlt2IFTFull_SMOG2Detached2BodyDecision",
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(sprucing_lines)
@configurable
def smog2_generic3bodydetached_spruceline(
    name="SpruceIFT_SMOG2Detached3Body",
    prescale=0.1,
    persistreco=True,
):
    """
    Detached generic 2-body decay coming from the SMOG2 region
    """
    pvs = make_pvs

    generic_particle = make_smog2_common_particles(
        make_has_rich_long_pions,
        max_trchi2dof=3,
        min_p=5000 * MeV,
        min_pt=800 * MeV,
        pvs=make_pvs,
    )

    generic_3body = make_smog2_generic_3body(
        pvs,
        generic_particle,
        generic_particle,
        generic_particle,
        name="generic_3body_smog2",
        descriptor="B0 -> pi+ pi+ pi-",
        apt_min=0 * GeV,
        maxsdoca=0.2 * mm,
        vchi2pdof_max=25,
        bpvvdchi2_min=50,
        single_pt_min=2 * GeV,
        mipchi2_min=20,
        sv_insmog=True,
        minmass=3.5 * GeV,
        min_bpvltime=1 * picosecond,
    )

    return SpruceLine(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [generic_3body],
        hlt2_filter_code="Hlt2IFTFull_SMOG2Detached3BodyDecision",
        prescale=prescale,
        persistreco=persistreco,
    )
