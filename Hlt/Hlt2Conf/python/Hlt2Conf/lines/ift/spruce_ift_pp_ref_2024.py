###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from . import (
    spruce_ift_isolated_highpt_photon,
    spruce_ift_smog2_chargedPID,
    spruce_ift_smog2_charm,
    spruce_ift_smog2_generic,
    spruce_ift_smog2_muons,
    spruce_ift_smog2_omegas,
    spruce_ift_smog2_pp_ref_2024,
    spruce_ift_smog2_xis,
)

# provide "sprucing_lines" for correct registration by the overall sprucing lines module
sprucing_lines = {}
sprucing_lines.update(spruce_ift_smog2_pp_ref_2024.sprucing_lines)
sprucing_lines.update(spruce_ift_smog2_generic.sprucing_lines)
sprucing_lines.update(spruce_ift_smog2_charm.sprucing_lines)
sprucing_lines.update(spruce_ift_smog2_chargedPID.sprucing_lines)
sprucing_lines.update(spruce_ift_smog2_muons.sprucing_lines)
sprucing_lines.update(spruce_ift_smog2_xis.sprucing_lines)
sprucing_lines.update(spruce_ift_smog2_omegas.sprucing_lines)
sprucing_lines.update(spruce_ift_isolated_highpt_photon.sprucing_lines)
