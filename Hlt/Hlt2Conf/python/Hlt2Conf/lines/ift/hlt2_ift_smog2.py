###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of SMOG2 HLT2 lines
"""

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction

from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters
from Hlt2Conf.lines.ift.builders.smog2_hlt1_filters import hlt1_smog2_global_filter

PROCESS = "hlt2"
all_lines = {}

_hlt1_lumi_SMOG2_lines = ["Hlt1SMOG2BELowMultElectronsDecision"]

_hlt1_lowMult_SMOG2_lines = [
    "Hlt1SMOG2PassThroughLowMult5Decision",
    "Hlt1SMOG2BELowMultElectronsDecision",
]  # For luminosity measurement with p-e scattering

_hlt1_minbias_SMOG2_lines = [
    "Hlt1SMOG2MinimumBiasDecision",
    "Hlt1PassthroughPVinSMOG2Decision",
    "Hlt1SMOG2BENoBiasDecision",
]


@register_line_builder(all_lines)
@configurable
def smog2_passthrough_PV(
    name="Hlt2IFTFull_SMOG2Passthrough_PV_in_SMOG2", prescale=0.0001, persistreco=True
):
    pvs = make_pvs

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs),
        hlt1_filter_code=hlt1_smog2_global_filter,
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_passthrough(
    name="Hlt2IFTFull_SMOG2Passthrough", prescale=0.0001, persistreco=True
):
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction(),
        hlt1_filter_code=hlt1_smog2_global_filter,
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_lumi_passthrough(
    name="Hlt2IFTFull_SMOG2LumiPassthrough", prescale=1, persistreco=True
):
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction(),
        hlt1_filter_code=_hlt1_lumi_SMOG2_lines,
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_cep_passthrough(
    name="Hlt2IFTFull_SMOG2CEPPassthrough", prescale=0.001, persistreco=True
):
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction(),
        hlt1_filter_code=_hlt1_lowMult_SMOG2_lines,
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_minbias_passthrough(
    name="Hlt2IFTFull_SMOG2MBPassthrough", prescale=1, persistreco=True
):
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction(),
        hlt1_filter_code=_hlt1_minbias_SMOG2_lines,
        prescale=prescale,
        persistreco=persistreco,
    )
