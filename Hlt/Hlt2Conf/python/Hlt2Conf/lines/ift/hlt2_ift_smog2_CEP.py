###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
SMOG2 Hlt Lines for CEP events. Seperate line for 1,2,3,4,5 tracks coming SV in SMOG2 Region.
"""

from GaudiKernel.SystemOfUnits import MeV, mm
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.reconstruction_objects import upfront_reconstruction
from RecoConf.standard_particles import make_long_muons, make_long_pions

from Hlt2Conf.lines.ift.builders.smog2_CEP_builders import (
    make_smog2_2_track_cep,
    make_smog2_3_track_cep,
    make_smog2_4_track_cep,
    make_smog2_5_track_cep,
    make_smog2_particle_no_PV,
)

PROCESS = "hlt2"
all_lines = {}
turbo_lines = {}


# =========================
# == Hadronic CEP events ==
# =========================
@configurable
@register_line_builder(all_lines)
@register_line_builder(turbo_lines)
def smog2_1_cep_track_line(
    name="Hlt2IFTTurbo_SMOG2_CEP_1_TRACK",
    prescale=1,
    persistreco=True,
    min_pt=0.001 * MeV,
):
    """
    Line requiring one track coming from Smog2 in low mult events.
    """

    cep_track = make_smog2_particle_no_PV(
        make_long_pions,
        max_trchi2dof=5,
        min_p=0.001 * MeV,
        min_pt=min_pt,
        pid_cut=None,
        particle=None,
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [cep_track],
        hlt1_filter_code=["Hlt1SMOG2PassThroughLowMult5Decision"],
        prescale=prescale,
        persistreco=persistreco,
    )


@configurable
@register_line_builder(all_lines)
@register_line_builder(turbo_lines)
def smog2_2_cep_track_line(
    name="Hlt2IFTTurbo_SMOG2_CEP_2_TRACK",
    prescale=1.0,
    persistreco=True,
    min_pt=0.001 * MeV,
):
    """
    Line requiring 2 long tracks coming from SMOG2 from the same SV
    """
    cep_track = make_smog2_particle_no_PV(
        make_long_pions,
        max_trchi2dof=25,
        min_p=0.001 * MeV,
        min_pt=min_pt,
        pid_cut=None,
        particle=None,
    )

    cep_2track = make_smog2_2_track_cep(
        cep_track,
        cep_track,
        name="cep_2track",
        descriptor="B0 -> pi- pi+",
        apt_min=0.001 * MeV,
        maxsdoca=10 * mm,
        vchi2pdof_max=25,
        single_pt_min=None,
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [cep_2track],
        hlt1_filter_code=["Hlt1SMOG2PassThroughLowMult5Decision"],
        prescale=prescale,
        persistreco=persistreco,
    )


@configurable
@register_line_builder(all_lines)
@register_line_builder(turbo_lines)
def smog2_3_cep_track_line(
    name="Hlt2IFTTurbo_SMOG2_CEP_3_TRACK",
    prescale=1.0,
    persistreco=True,
    min_pt=0.001 * MeV,
):
    """
    Line requiring 3 long tracks coming from SMOG2 from the same SV
    """
    cep_track = make_smog2_particle_no_PV(
        make_long_pions,
        max_trchi2dof=25,
        min_p=0.001 * MeV,
        min_pt=min_pt,
        pid_cut=None,
        particle=None,
    )

    cep_3track = make_smog2_3_track_cep(
        cep_track,
        cep_track,
        cep_track,
        name="cep_3track",
        descriptor="[B- -> pi- pi- pi+]cc",
        apt_min=0.001 * MeV,
        maxsdoca=10 * mm,
        vchi2pdof_max=25,
        single_pt_min=None,
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [cep_3track],
        hlt1_filter_code=["Hlt1SMOG2PassThroughLowMult5Decision"],
        prescale=prescale,
        persistreco=persistreco,
    )


@configurable
@register_line_builder(all_lines)
@register_line_builder(turbo_lines)
def smog2_4_cep_track_line(
    name="Hlt2IFTTurbo_SMOG2_CEP_4_TRACK",
    prescale=1.0,
    persistreco=True,
    min_pt=0.001 * MeV,
):
    """
    Line requiring 4 long tracks coming from SMOG2 from the same SV
    """
    cep_track = make_smog2_particle_no_PV(
        make_long_pions,
        max_trchi2dof=25,
        min_p=0.001 * MeV,
        min_pt=min_pt,
        pid_cut=None,
        particle=None,
    )

    cep_4track = make_smog2_4_track_cep(
        cep_track,
        cep_track,
        cep_track,
        cep_track,
        name="cep_4track",
        descriptor="[B0 -> pi- pi- pi+ pi+]cc",
        apt_min=0.001 * MeV,
        maxsdoca=10 * mm,
        vchi2pdof_max=25,
        single_pt_min=None,
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [cep_4track],
        hlt1_filter_code=["Hlt1SMOG2PassThroughLowMult5Decision"],
        prescale=prescale,
        persistreco=persistreco,
    )


@configurable
@register_line_builder(all_lines)
@register_line_builder(turbo_lines)
def smog2_5_cep_track_line(
    name="Hlt2IFTTurbo_SMOG2_CEP_5_TRACK",
    prescale=1.0,
    persistreco=True,
    min_pt=0.001 * MeV,
):
    """
    Line requiring 5 long tracks coming from SMOG2 from the same SV
    """
    cep_track = make_smog2_particle_no_PV(
        make_long_pions,
        max_trchi2dof=25,
        min_p=0.001 * MeV,
        min_pt=min_pt,
        pid_cut=None,
        particle=None,
    )

    cep_5track = make_smog2_5_track_cep(
        cep_track,
        cep_track,
        cep_track,
        cep_track,
        cep_track,
        name="cep_5track",
        descriptor="[B- -> pi- pi- pi- pi+ pi+]cc",
        apt_min=0.001 * MeV,
        maxsdoca=10 * mm,
        vchi2pdof_max=25,
        single_pt_min=None,
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [cep_5track],
        hlt1_filter_code=["Hlt1SMOG2PassThroughLowMult5Decision"],
        prescale=prescale,
        persistreco=persistreco,
    )


# =======================================
# == Muonic CEP events for Calibration ==
# =======================================
@configurable
@register_line_builder(all_lines)
@register_line_builder(turbo_lines)
def smog2_2_cep_muon_track_line(
    name="Hlt2IFTTurbo_SMOG2_CEP_2_MUON_TRACK",
    prescale=1.0,
    persistreco=True,
    min_pt=0.001 * MeV,
):
    """
    Line requiring 2 long muons coming from SMOG2 from the same SV
    """
    muon_track = make_smog2_particle_no_PV(
        make_long_muons,
        max_trchi2dof=25,
        min_p=0.001 * MeV,
        min_pt=min_pt,
        pid_cut=None,
        particle="Muon",
    )

    cep_2_muon_track = make_smog2_2_track_cep(
        muon_track,
        muon_track,
        name="cep_2_muon_track",
        descriptor="J/psi(1S) -> mu- mu+",
        apt_min=0.001 * MeV,
        maxsdoca=10 * mm,
        vchi2pdof_max=25,
        single_pt_min=None,
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [cep_2_muon_track],
        hlt1_filter_code=["Hlt1SMOG2PassThroughLowMult5Decision"],
        prescale=prescale,
        persistreco=persistreco,
    )


@configurable
@register_line_builder(all_lines)
@register_line_builder(turbo_lines)
def smog2_2_cep_muon_mixed_track_line(
    name="Hlt2IFTTurbo_SMOG2_CEP_2_MUON_MIXED_TRACK",
    prescale=1.0,
    persistreco=True,
    min_pt=0.001 * MeV,
):
    """
    Line requiring 1 long muon, and another long track coming from SMOG2 from the same SV
    """
    muon_track = make_smog2_particle_no_PV(
        make_long_muons,
        max_trchi2dof=25,
        min_p=0.001 * MeV,
        min_pt=min_pt,
        pid_cut=None,
        particle="Muon",
    )

    cep_track = make_smog2_particle_no_PV(
        make_long_pions,
        max_trchi2dof=25,
        min_p=0.001 * MeV,
        min_pt=min_pt,
        pid_cut=None,
        particle=None,
    )

    cep_2_muon_mixed_track = make_smog2_2_track_cep(
        cep_track,
        muon_track,
        name="cep_2_muon_mixed_track",
        descriptor="J/psi(1S) -> mu- pi+",
        apt_min=0.001 * MeV,
        maxsdoca=10 * mm,
        vchi2pdof_max=25,
        single_pt_min=None,
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [cep_2_muon_mixed_track],
        hlt1_filter_code=["Hlt1SMOG2PassThroughLowMult5Decision"],
        prescale=prescale,
        persistreco=persistreco,
    )
