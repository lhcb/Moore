###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines all IFT HLT2 and sprucing lines for the 2024 pp
reference run.
"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, ns
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.algorithms_thor import ParticleFilter
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction
from RecoConf.standard_particles import make_photons

from Hlt2Conf.lines.ift.builders.isolated_highpt_photon_builders import (
    make_isolated_photons,
)
from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters
from Hlt2Conf.lines.ift.builders.smog2_chargedPID_builders import (
    make_smog2_L02ppi_ll_line,
)

from . import (
    hlt2_ift_femtoscopy,
    hlt2_ift_pp_He,
    hlt2_ift_smog2,
    hlt2_ift_smog2_CEP,
    hlt2_ift_smog2_chargedPID,
    hlt2_ift_smog2_charm,
    hlt2_ift_smog2_generic,
    hlt2_ift_smog2_muons,
    hlt2_ift_smog2_omegas,
    hlt2_ift_smog2_xis,
)

# Define lines specific to reference run.
hlt2_ift_ref_run_full_lines = {}
hlt2_ift_ref_run_turbo_lines = {}

_MASSWINDOW_LAMBDA0 = [(1115.683 - 25) * MeV, (1115.683 + 25) * MeV]
hlt1_filter_lambda = ["Hlt1SMOG2L0ToppiDecision"]


@register_line_builder(hlt2_ift_ref_run_full_lines)
def IsolatedGamma5GeVLine(name="Hlt2IFTFull_5GeVIsolatedGamma", minpt=5.0, prescale=1):
    photons = make_photons()

    code = F.require_all(F.PT > minpt * GeV, F.IS_PHOTON > 0.8, F.IS_NOT_H > 0.8)

    highpt_photons = ParticleFilter(Input=photons, Cut=F.FILTER(code))

    isophotons = make_isolated_photons(highpt_photons, max_cone_pt=0.2 * minpt * GeV)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [photons, highpt_photons, isophotons],
        hlt1_filter_code="Hlt1HighPtPhotonDecision",
        calo_clusters=True,
        calo_digits=True,
        prescale=prescale,
        persistreco=True,
        monitoring_variables=("pt", "eta", "n_candidates"),
    )


@register_line_builder(hlt2_ift_ref_run_full_lines)
def IsolatedGamma10GeVLine(
    name="Hlt2IFTFull_10GeVIsolatedGamma", minpt=10.0, prescale=1
):
    photons = make_photons()

    code = F.require_all(F.PT > minpt * GeV, F.IS_PHOTON > 0.8, F.IS_NOT_H > 0.8)

    highpt_photons = ParticleFilter(Input=photons, Cut=F.FILTER(code))

    isophotons = make_isolated_photons(highpt_photons, max_cone_pt=0.2 * minpt * GeV)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [photons, highpt_photons, isophotons],
        hlt1_filter_code="Hlt1HighPtPhotonDecision",
        calo_clusters=True,
        calo_digits=True,
        prescale=prescale,
        persistreco=True,
        monitoring_variables=("pt", "eta", "n_candidates"),
    )


@register_line_builder(hlt2_ift_ref_run_full_lines)
def IsolatedGamma20GeVLine(
    name="Hlt2IFTFull_20GeVIsolatedGamma", minpt=20.0, prescale=1
):
    photons = make_photons()

    code = F.require_all(F.PT > minpt * GeV, F.IS_PHOTON > 0.8, F.IS_NOT_H > 0.8)

    highpt_photons = ParticleFilter(Input=photons, Cut=F.FILTER(code))

    isophotons = make_isolated_photons(highpt_photons, max_cone_pt=0.2 * minpt * GeV)

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [photons, highpt_photons, isophotons],
        hlt1_filter_code="Hlt1HighPtPhotonDecision",
        calo_clusters=True,
        calo_digits=True,
        prescale=prescale,
        persistreco=True,
        monitoring_variables=("pt", "eta", "n_candidates"),
    )


@register_line_builder(hlt2_ift_ref_run_turbo_lines)
def SMOG2LambdaLine(
    name="Hlt2IFTTurbo_SMOG2Lambda02PPiLL",
    prescale=0.5,
    persistreco=False,
    min_p=2 * GeV,
    max_trchi2dof=5,
    min_bpvipchi2=25,
    mmin=_MASSWINDOW_LAMBDA0[0],
    mmax=_MASSWINDOW_LAMBDA0[1],
    apt_min=0 * MeV,
    vchi2pdof_max=25.0,
    end_vz_max=2200 * mm,
    bpvvdchi2_min=0,
    bpvltime_min=0 * ns,
    parent_bpvipchi2_max=100,
    ks_veto_window=20 * MeV,
):
    """
    SMOG2 Lambda -> p pi TURBO line with a looser prescale than the FULL line.
    """

    pvs = make_pvs

    l02ppi = make_smog2_L02ppi_ll_line(
        pvs=pvs,
        min_p=min_p,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin=mmin,
        mmax=mmax,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        end_vz_max=end_vz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvltime_min=bpvltime_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        ks_veto_window=ks_veto_window,
        process="hlt2",
        name="smog2_lambda2ppi_ll",
    )

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [l02ppi],
        hlt1_filter_code=hlt1_filter_lambda,
        prescale=prescale,
        persistreco=persistreco,
        pv_tracks=True,
    )


all_lines = {}
all_lines.update(hlt2_ift_femtoscopy.all_lines)
all_lines.update(hlt2_ift_pp_He.all_lines)
all_lines.update(hlt2_ift_smog2.all_lines)
all_lines.update(hlt2_ift_smog2_generic.all_lines)
all_lines.update(hlt2_ift_smog2_charm.all_lines)
all_lines.update(hlt2_ift_smog2_chargedPID.all_lines)
all_lines.update(hlt2_ift_smog2_muons.all_lines)
all_lines.update(hlt2_ift_smog2_omegas.all_lines)
all_lines.update(hlt2_ift_smog2_xis.all_lines)
all_lines.update(hlt2_ift_ref_run_full_lines)
all_lines.update(hlt2_ift_smog2_CEP.all_lines)

ift_full_lines = {}
ift_full_lines.update(hlt2_ift_smog2.all_lines)
ift_full_lines.update(hlt2_ift_smog2_generic.full_lines)
ift_full_lines.update(hlt2_ift_smog2_charm.full_lines)
ift_full_lines.update(hlt2_ift_smog2_chargedPID.full_lines)
ift_full_lines.update(hlt2_ift_smog2_muons.full_lines)
ift_full_lines.update(hlt2_ift_smog2_omegas.all_lines)
ift_full_lines.update(hlt2_ift_smog2_xis.all_lines)
ift_full_lines.update(hlt2_ift_ref_run_full_lines)

ift_turbo_lines = {}
ift_turbo_lines.update(hlt2_ift_femtoscopy.all_lines)
ift_turbo_lines.update(hlt2_ift_pp_He.all_lines)
ift_turbo_lines.update(hlt2_ift_smog2_generic.turbo_lines)
ift_turbo_lines.update(hlt2_ift_smog2_charm.turbo_lines)
ift_turbo_lines.update(hlt2_ift_smog2_chargedPID.turbo_lines)
ift_turbo_lines.update(hlt2_ift_smog2_muons.turbo_lines)
ift_turbo_lines.update(hlt2_ift_smog2_CEP.all_lines)
ift_turbo_lines.update(hlt2_ift_ref_run_turbo_lines)
