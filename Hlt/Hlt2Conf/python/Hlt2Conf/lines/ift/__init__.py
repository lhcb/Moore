###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines all ift HLT2 and sprucing lines
"""

from . import (
    hlt2_ift_femtoscopy,
    hlt2_ift_isolated_highpt_photon,
    hlt2_ift_pp_He,
    hlt2_ift_smog2,
    hlt2_ift_smog2_CEP,
    hlt2_ift_smog2_chargedPID,
    hlt2_ift_smog2_charm,
    hlt2_ift_smog2_generic,
    hlt2_ift_smog2_muons,
    hlt2_ift_smog2_omegas,
    hlt2_ift_smog2_xis,
)

# provide "all_lines" for correct registration by the overall HLT2 lines module
all_lines = {}
all_lines.update(hlt2_ift_femtoscopy.all_lines)
all_lines.update(hlt2_ift_pp_He.all_lines)
all_lines.update(hlt2_ift_smog2.all_lines)
all_lines.update(hlt2_ift_smog2_generic.all_lines)
all_lines.update(hlt2_ift_smog2_charm.all_lines)
all_lines.update(hlt2_ift_smog2_chargedPID.all_lines)
all_lines.update(hlt2_ift_smog2_muons.all_lines)
all_lines.update(hlt2_ift_smog2_omegas.all_lines)
all_lines.update(hlt2_ift_smog2_xis.all_lines)
all_lines.update(hlt2_ift_isolated_highpt_photon.all_lines)
all_lines.update(hlt2_ift_smog2_CEP.all_lines)

ift_full_lines = {}
ift_full_lines.update(hlt2_ift_smog2.all_lines)
ift_full_lines.update(hlt2_ift_smog2_generic.full_lines)
ift_full_lines.update(hlt2_ift_smog2_charm.full_lines)
ift_full_lines.update(hlt2_ift_smog2_chargedPID.full_lines)
ift_full_lines.update(hlt2_ift_smog2_muons.full_lines)
ift_full_lines.update(hlt2_ift_smog2_omegas.all_lines)
ift_full_lines.update(hlt2_ift_smog2_xis.all_lines)
ift_full_lines.update(hlt2_ift_isolated_highpt_photon.all_lines)

ift_turbo_lines = {}
ift_turbo_lines.update(hlt2_ift_femtoscopy.all_lines)
ift_turbo_lines.update(hlt2_ift_pp_He.all_lines)
ift_turbo_lines.update(hlt2_ift_smog2_generic.turbo_lines)
ift_turbo_lines.update(hlt2_ift_smog2_charm.turbo_lines)
ift_turbo_lines.update(hlt2_ift_smog2_chargedPID.turbo_lines)
ift_turbo_lines.update(hlt2_ift_smog2_muons.turbo_lines)
ift_turbo_lines.update(hlt2_ift_smog2_CEP.all_lines)

##### Sprucing lines

from . import (
    spruce_ift_isolated_highpt_photon,
    spruce_ift_smog2,
    spruce_ift_smog2_chargedPID,
    spruce_ift_smog2_charm,
    spruce_ift_smog2_generic,
    spruce_ift_smog2_muons,
    spruce_ift_smog2_omegas,
    spruce_ift_smog2_xis,
)

# provide "sprucing_lines" for correct registration by the overall sprucing lines module
sprucing_lines = {}
sprucing_lines.update(spruce_ift_smog2.sprucing_lines)
sprucing_lines.update(spruce_ift_smog2_generic.sprucing_lines)
sprucing_lines.update(spruce_ift_smog2_charm.sprucing_lines)
sprucing_lines.update(spruce_ift_smog2_chargedPID.sprucing_lines)
sprucing_lines.update(spruce_ift_smog2_muons.sprucing_lines)
sprucing_lines.update(spruce_ift_smog2_xis.sprucing_lines)
sprucing_lines.update(spruce_ift_smog2_omegas.sprucing_lines)
sprucing_lines.update(spruce_ift_isolated_highpt_photon.sprucing_lines)
