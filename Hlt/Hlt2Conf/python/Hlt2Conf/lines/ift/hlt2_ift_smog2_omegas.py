###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of SMOG2 HLT2 lines for charged PID calibration
"""

from __future__ import absolute_import

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, ns
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters
from Hlt2Conf.lines.ift.builders.smog2_chargedPID_builders import (
    make_smog2_omega2L0K_ddd_line,
    make_smog2_omega2L0K_ddl_line,
    make_smog2_omega2L0K_lll_line,
)
from Hlt2Conf.lines.ift.builders.smog2_hlt1_filters import hlt1_smog2_global_filter

PROCESS = "hlt2"
all_lines = {}

_MASSWINDOW_LAMBDA0 = [(1115.683 - 25) * MeV, (1115.683 + 25) * MeV]
_MASSWINDOW_OMEGA = [(1672 - 25) * MeV, (1672 + 25) * MeV]

hlt1_filter_lambda = ["Hlt1SMOG2L0ToppiDecision"]

#################################################################
#################################################################
###################  CHARGED PID LINES  #########################
#################################################################
#################################################################


@register_line_builder(all_lines)
@configurable
def smog2_omega2lambdak_lll_line(
    name="Hlt2IFTFull_SMOG2Omega2Lambda0K_lll",
    prescale=1,
    persistreco=True,
    min_p=2 * GeV,
    min_pt=100 * MeV,
    max_trchi2dof=5,
    min_bpvipchi2=25,
    mmin_Omega=_MASSWINDOW_OMEGA[0],
    mmax_Omega=_MASSWINDOW_OMEGA[1],
    mmin_L0=_MASSWINDOW_LAMBDA0[0],
    mmax_L0=_MASSWINDOW_LAMBDA0[1],
    end_vz_max=2200 * mm,
    apt_min=0 * MeV,
    vchi2pdof_max=25.0,
    bpvvdchi2_min=5,
    bpvltime_min=0 * ns,
    parent_bpvipchi2_max=1000,
    ks_veto_window=None,
):
    """
    SMOG2 Lambda0 -> p pi HLT2 trigger line for PID calibration (no PID requirements)
    Children reconstructed as long tracks
    """
    pvs = make_pvs

    omega2Lambda0Ks = make_smog2_omega2L0K_lll_line(
        pvs=pvs,
        min_p=min_p,
        min_pt=min_pt,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin_Omega=mmin_Omega,
        mmax_Omega=mmax_Omega,
        mmin_L0=mmin_L0,
        mmax_L0=mmax_L0,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        end_vz_max=end_vz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvltime_min=bpvltime_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        ks_veto_window=ks_veto_window,
    )
    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [omega2Lambda0Ks],
        hlt1_filter_code=hlt1_filter_lambda,
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_omega2lambdak_ddl_line(
    name="Hlt2IFTFull_SMOG2Omega2Lambda0K_ddl",
    prescale=1,
    persistreco=True,
    min_p=2 * GeV,
    min_pt=100 * MeV,
    max_trchi2dof=5,
    min_bpvipchi2=25,
    mmin_Omega=_MASSWINDOW_OMEGA[0],
    mmax_Omega=_MASSWINDOW_OMEGA[1],
    mmin_L0=_MASSWINDOW_LAMBDA0[0],
    mmax_L0=_MASSWINDOW_LAMBDA0[1],
    apt_min=0 * MeV,
    vchi2pdof_max=30.0,
    end_vz_max=2200 * mm,
    bpvvdz_min=400 * mm,
    bpvvdz_max=2800 * mm,
    bpvvdchi2_min=5,
    bpvltime_min=0 * ns,
    parent_bpvipchi2_max=1000,
    ks_veto_window=None,
):
    """
    SMOG2 Lambda0 -> p pi HLT2 trigger line for PID calibration (no PID requirements)
    Children reconstructed as long tracks
    """
    pvs = make_pvs

    omega2Lambda0Ks = make_smog2_omega2L0K_ddl_line(
        pvs=pvs,
        min_p=min_p,
        min_pt=min_pt,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin_Omega=mmin_Omega,
        mmax_Omega=mmax_Omega,
        mmin_L0=mmin_L0,
        mmax_L0=mmax_L0,
        apt_min=apt_min,
        vchi2pdof_max=vchi2pdof_max,
        end_vz_max=end_vz_max,
        bpvvdz_min=bpvvdz_min,
        bpvvdz_max=bpvvdz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        bpvltime_min=bpvltime_min,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        ks_veto_window=ks_veto_window,
    )
    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [omega2Lambda0Ks],
        hlt1_filter_code=hlt1_smog2_global_filter,
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def smog2_omega2lambdak_ddd_line(
    name="Hlt2IFTFull_SMOG2Omega2Lambda0K_ddd",
    prescale=1,
    persistreco=True,
    min_p=2 * GeV,
    min_pt=100 * MeV,
    max_trchi2dof=5,
    min_bpvipchi2=25,
    mmin_Omega=_MASSWINDOW_OMEGA[0],
    mmax_Omega=_MASSWINDOW_OMEGA[1],
    mmin_L0=_MASSWINDOW_LAMBDA0[0],
    mmax_L0=_MASSWINDOW_LAMBDA0[1],
    apt_min=0 * MeV,
    bpvvdz_min=400 * mm,
    bpvvdz_max=2800 * mm,
    bpvvdchi2_min=5,
    vchi2pdof_max=30.0,
    parent_bpvipchi2_max=1000,
    ks_veto_window=None,
):
    """
    SMOG2 Lambda0 -> p pi HLT2 trigger line for PID calibration (no PID requirements)
    Children reconstructed as long tracks
    """
    pvs = make_pvs

    omega2Lambda0Ks = make_smog2_omega2L0K_ddd_line(
        pvs=pvs,
        min_p=min_p,
        min_pt=min_pt,
        max_trchi2dof=max_trchi2dof,
        min_bpvipchi2=min_bpvipchi2,
        mmin_Omega=mmin_Omega,
        mmax_Omega=mmax_Omega,
        mmin_L0=mmin_L0,
        mmax_L0=mmax_L0,
        apt_min=apt_min,
        bpvvdz_min=bpvvdz_min,
        bpvvdz_max=bpvvdz_max,
        bpvvdchi2_min=bpvvdchi2_min,
        vchi2pdof_max=vchi2pdof_max,
        parent_bpvipchi2_max=parent_bpvipchi2_max,
        ks_veto_window=ks_veto_window,
    )
    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [omega2Lambda0Ks],
        hlt1_filter_code=hlt1_smog2_global_filter,
        prescale=prescale,
        persistreco=persistreco,
    )
