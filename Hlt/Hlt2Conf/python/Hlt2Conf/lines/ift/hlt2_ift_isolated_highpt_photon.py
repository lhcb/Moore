###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of High pT isolated gamma.

author: Cesar da Silva
date: 11.05.2024

"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.algorithms_thor import ParticleFilter
from RecoConf.reconstruction_objects import upfront_reconstruction
from RecoConf.standard_particles import make_photons

from Hlt2Conf.lines.ift.builders.isolated_highpt_photon_builders import (
    make_isolated_photons,
)

all_lines = {}

prescales = {5.0: 0.01, 10.0: 0.1, 20.0: 1.0}

for minPt, ps in prescales.items():

    @register_line_builder(all_lines)
    def Hlt2_PtIsolatedGamma(
        name="Hlt2IFTFull_%.0fGeVIsolatedGamma" % minPt, minpt=minPt, prescale=ps
    ):
        """
        Register HighPtIsolatedGamma line
        """
        photons = make_photons()

        code = F.require_all(F.PT > minpt * GeV, F.IS_PHOTON > 0.8, F.IS_NOT_H > 0.8)

        highpt_photons = ParticleFilter(Input=photons, Cut=F.FILTER(code))

        isophotons = make_isolated_photons(
            highpt_photons, max_cone_pt=0.2 * minpt * GeV
        )

        return Hlt2Line(
            name=name,
            algs=upfront_reconstruction() + [photons, highpt_photons, isophotons],
            calo_clusters=True,
            calo_digits=True,
            prescale=prescale,
            persistreco=True,
            monitoring_variables=("pt", "eta", "n_candidates"),
        )
