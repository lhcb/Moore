###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of High pT isolated gamma.

author: Cesar da Silva
date: 21.08.2024

"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV
from Moore.config import SpruceLine, register_line_builder
from RecoConf.algorithms_thor import ParticleFilter
from RecoConf.standard_particles import make_photons

from Hlt2Conf.lines.ift.builders.isolated_highpt_photon_builders import (
    make_isolated_photons,
)

sprucing_lines = {}


@register_line_builder(sprucing_lines)
def Spruce_5GeVIsolatedGamma_line(
    name="SpruceIFT_5GeVIsolatedGamma", minpt=5.0, prescale=1.0
):
    """
    Register HighPtIsolatedGamma line
    """
    photons = make_photons()

    code = F.require_all(F.PT > minpt * GeV, F.IS_PHOTON > 0.8, F.IS_NOT_H > 0.8)

    highpt_photons = ParticleFilter(Input=photons, Cut=F.FILTER(code))

    isophotons = make_isolated_photons(highpt_photons, max_cone_pt=0.2 * minpt * GeV)

    return SpruceLine(
        name=name,
        algs=[photons, highpt_photons, isophotons],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code="Hlt2IFTFull_5GeVIsolatedGammaDecision",
    )


@register_line_builder(sprucing_lines)
def Spruce_10GeVIsolatedGamma_line(
    name="SpruceIFT_10GeVIsolatedGamma", minpt=10.0, prescale=1.0
):
    """
    Register HighPtIsolatedGamma line
    """
    photons = make_photons()

    code = F.require_all(F.PT > minpt * GeV, F.IS_PHOTON > 0.8, F.IS_NOT_H > 0.8)

    highpt_photons = ParticleFilter(Input=photons, Cut=F.FILTER(code))

    isophotons = make_isolated_photons(highpt_photons, max_cone_pt=0.2 * minpt * GeV)

    return SpruceLine(
        name=name,
        algs=[photons, highpt_photons, isophotons],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code="Hlt2IFTFull_10GeVIsolatedGammaDecision",
    )


@register_line_builder(sprucing_lines)
def Spruce_20GeVIsolatedGamma_line(
    name="SpruceIFT_20GeVIsolatedGamma", minpt=20.0, prescale=1.0
):
    """
    Register HighPtIsolatedGamma line
    """
    photons = make_photons()

    code = F.require_all(F.PT > minpt * GeV, F.IS_PHOTON > 0.8, F.IS_NOT_H > 0.8)

    highpt_photons = ParticleFilter(Input=photons, Cut=F.FILTER(code))

    isophotons = make_isolated_photons(highpt_photons, max_cone_pt=0.2 * minpt * GeV)

    return SpruceLine(
        name=name,
        algs=[photons, highpt_photons, isophotons],
        prescale=prescale,
        persistreco=True,
        hlt2_filter_code="Hlt2IFTFull_20GeVIsolatedGammaDecision",
    )
