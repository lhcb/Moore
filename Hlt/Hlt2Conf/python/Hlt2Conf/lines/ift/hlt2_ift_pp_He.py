###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from PyConf.Algorithms import (
    MatchVeloTrackToVertex,
    TrackListRefiner,
    TrackSelectionToContainer,
)
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.hlt2_tracking import (
    get_track_master_extrapolator,
    get_track_master_fitter,
)
from RecoConf.legacy_rec_hlt1_tracking import all_velo_track_types
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    make_down_helium3,
    make_down_pions,
    make_long_helium3,
    make_long_pions,
    make_long_protons,
)

# constants (PDG, Prog. Theor. Exp. Phys. 2020, 083C01 (2020) and 2021 update)
K0_M = 497.611 * MeV  # +/- 0.013
L0_M = 1115.683 * MeV  # +/- 0.006
PI_M = 139.57039 * MeV  # +/- 0.00018
P_M = 938.27208816 * MeV  # +/- 0.00000029

# pp prefilter
# ------------
from RecoConf.event_filters import require_gec, require_pvs
from RecoConf.reconstruction_objects import upfront_reconstruction


@configurable
def make_ift_pp_prefilters(require_GEC=False, pvs=make_pvs):
    """
    Common ift pp prefilters
    """

    filters = list(upfront_reconstruction())  # create a copy
    if require_GEC:
        filters.append(require_gec())
    filters.append(require_pvs(pvs))
    return filters


all_lines = {}


# Filters
# -------
@configurable
def filter_pions(particles, pvs=None, ipchi2=10, track_chi2dof=2.0):
    cut = F.require_all(
        F.ETA < 5.0, F.ETA > 2.0, F.BPVIPCHI2(pvs) >= ipchi2, F.CHI2DOF < track_chi2dof
    )
    return ParticleFilter(particles, F.FILTER(cut))


@configurable
def filter_helium(particles, pvs=None, pt_min=0.6 * GeV, ipchi2=10):
    cut = F.require_all(
        F.P > 5000 * MeV,
        F.PT > pt_min,
        F.ETA < 5.0,
        F.ETA > 2.0,
        F.BPVIPCHI2(pvs) >= ipchi2,
        F.CHI2DOF < 2.0,
        F.NUTHITS @ F.TRACK > 2,
        F.MEDIAN @ F.UTHITADCS @ F.TRACK > 25,
    )
    return ParticleFilter(particles, F.FILTER(cut))


@configurable
def filter_protons(particles, pvs=None, ipchi2=5, track_chi2dof=2.0, pid_p=3):
    cut = F.require_all(
        F.P > 1500 * MeV,
        F.PT > 100 * GeV,
        F.ETA < 5.0,
        F.ETA > 2.0,
        F.BPVIPCHI2(pvs) >= ipchi2,
        F.CHI2DOF < track_chi2dof,
        F.PID_P > pid_p,
    )
    return ParticleFilter(particles, F.FILTER(cut))


# Hypertriton
# -----------
@configurable
def make_hypertriton(
    particles,
    decay_descriptor,
    pvs,
    comb_m_min=0 * MeV,
    comb_m_max=3300 * MeV,
    comb_doca_max=2 * mm,
    comb_docachi2_max=20.0,
    comb_ks_window=20 * MeV,
    comb_l0_window=10 * MeV,
    vtx_chi2dof_max=7.0,
    vtx_dira_min=0.999,
    vtx_fd_chi2_min=55,
    vtx_fd_min=10 * mm,
    vtx_ip_chi2_max=7,
    vtx_m_min=2960 * MeV,
    vtx_m_max=3060 * MeV,
    name="HypertritonCombiner_{hash}",
):
    # V0 mis-ids vetos
    # Can't use MASSWITHHYPOTHESES, as helium charge wouldn't be considert correctly

    # Particle1: He
    P1_PX = F.CHILD(1, F.PX) / 2
    P1_PY = F.CHILD(1, F.PY) / 2
    P1_PZ = F.CHILD(1, F.PZ) / 2
    P1_PP = F.CHILD(1, F.P * F.P) / 4
    P1_E_pi = F.SQRT @ (PI_M * PI_M + P1_PP)
    P1_E_p = F.SQRT @ (P_M * P_M + P1_PP)

    # Particle2: Pi
    P2_PX = F.CHILD(2, F.PX)
    P2_PY = F.CHILD(2, F.PY)
    P2_PZ = F.CHILD(2, F.PZ)
    P2_PP = F.CHILD(2, F.P * F.P)
    P2_E_pi = F.CHILD(2, F.ENERGY)
    P2_E_p = F.SQRT @ (P_M * P_M + P2_PP)

    # V0 combinations
    M_KS = F.SQRT @ (
        (P1_E_pi + P2_E_pi) ** 2
        - (P1_PX + P2_PX) ** 2
        - (P1_PY + P2_PY) ** 2
        - (P1_PZ + P2_PZ) ** 2
    )

    M_L01 = F.SQRT @ (
        (P1_E_p + P2_E_pi) ** 2
        - (P1_PX + P2_PX) ** 2
        - (P1_PY + P2_PY) ** 2
        - (P1_PZ + P2_PZ) ** 2
    )

    M_L02 = F.SQRT @ (
        (P1_E_pi + P2_E_p) ** 2
        - (P1_PX + P2_PX) ** 2
        - (P1_PY + P2_PY) ** 2
        - (P1_PZ + P2_PZ) ** 2
    )

    # combination cut
    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.MAXDOCACUT(comb_doca_max),
        F.MAXDOCACHI2CUT(comb_docachi2_max),
        (M_KS < K0_M - comb_ks_window) | (M_KS > K0_M + comb_ks_window),
        (M_L01 < L0_M - comb_l0_window) | (M_L01 > L0_M + comb_l0_window),
        (M_L02 < L0_M - comb_l0_window) | (M_L02 > L0_M + comb_l0_window),
    )
    vertex_code = F.require_all(
        in_range(vtx_m_min, F.MASS, vtx_m_max),
        F.CHI2DOF < vtx_chi2dof_max,
        F.BPVDIRA(pvs) > vtx_dira_min,
        F.BPVFD(pvs) > vtx_fd_min,
        F.BPVFDCHI2(pvs) > vtx_fd_chi2_min,
        F.BPVIPCHI2(pvs) < vtx_ip_chi2_max,
    )

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def _match_dd_hypertriton_to_velo_track(
    hypertriton, pvs=make_pvs, velo_tracks=all_velo_track_types
):
    filtered_velo_tracks = TrackListRefiner(
        inputLocation=velo_tracks()["v1"],
        Code=F.require_all(
            F.BPVIP(Vertices=pvs) < 2 * mm, F.BPVIPCHI2(Vertices=pvs) < 10
        ),
    ).outputLocation
    filtered_velo_container = TrackSelectionToContainer(
        InputLocation=filtered_velo_tracks
    ).OutputLocation
    return MatchVeloTrackToVertex(
        SeedParticles=hypertriton,
        VeloTracks=filtered_velo_container,
        PrimaryVertices=pvs,
        TrackFitter=get_track_master_fitter(),
        TrackExtrapolator=get_track_master_extrapolator(),
        VeloMatchIP=2 * mm,
        EtaTolerance=0.4,
    ).OutputParticles


@register_line_builder(all_lines)
@configurable
def HypertritonToHePi_LL(name="Hlt2IFTTurbo_HypertritonToHePi_LL", prescale=1):
    pvs = make_pvs()

    pions = filter_pions(make_long_pions(), pvs)
    helium = filter_helium(make_long_helium3(), pvs)

    hypertriton = make_hypertriton(
        particles=[helium, pions],
        decay_descriptor="[hypertriton -> He3 pi-]cc",
        pvs=pvs,
    )

    return Hlt2Line(
        name=name,
        algs=make_ift_pp_prefilters(pvs=pvs) + [hypertriton],
        prescale=prescale,
        persistreco=False,
        raw_banks=["Rich"],
    )


@register_line_builder(all_lines)
@configurable
def HypertritonToHePi_SS_LL(name="Hlt2IFTTurbo_HypertritonToHePi_SS_LL", prescale=0.1):
    pvs = make_pvs()

    pions = filter_pions(make_long_pions(), pvs)
    helium = filter_helium(make_long_helium3(), pvs)

    hypertriton = make_hypertriton(
        particles=[helium, pions],
        decay_descriptor="[hypertriton -> He3 pi+]cc",
        pvs=pvs,
    )

    return Hlt2Line(
        name=name,
        algs=make_ift_pp_prefilters(pvs=pvs) + [hypertriton],
        prescale=prescale,
        persistreco=False,
        raw_banks=["Rich"],
    )


@register_line_builder(all_lines)
@configurable
def HypertritonToHePi_DD_VeloMatch(
    name="Hlt2IFTTurbo_HypertritonToHePi_DD_VeloMatch", prescale=1
):
    pvs = make_pvs()

    pions = filter_pions(make_down_pions(), pvs)
    helium = filter_helium(make_down_helium3(), pvs)

    dd_hypertriton = make_hypertriton(
        particles=[helium, pions],
        decay_descriptor="[hypertriton -> He3 pi-]cc",
        pvs=pvs,
        vtx_fd_min=400 * mm,
    )

    matched_hypertriton = _match_dd_hypertriton_to_velo_track(dd_hypertriton, pvs=pvs)

    return Hlt2Line(
        name=name,
        algs=make_ift_pp_prefilters(pvs=pvs) + [dd_hypertriton, matched_hypertriton],
        persistreco=False,
        raw_banks=["VP", "Rich"],
    )


# Lb -> He p p
# ------------
@configurable
def make_lb_to_he(
    particles,
    decay_descriptor,
    pvs,
    comb_m_min=5000 * MeV,
    comb_m_max=7000 * MeV,
    comb_doca_max=1 * mm,
    comb_docachi2_max=20.0,
    vtx_chi2dof_max=7.0,
    vtx_dira_min=0.9999,
    vtx_fd_chi2_min=20,
    vtx_fd_min=-1 * mm,
    vtx_ip_chi2_max=7,
    vtx_pt_min=1500 * MeV,
    vtx_m_min=5395 * MeV,
    vtx_m_max=6305 * MeV,
    name="LbToHeCombiner_{hash}",
):
    # combination cuts
    combination12_code = F.require_all(
        F.DOCA(1, 2) < comb_doca_max, F.MASS < comb_m_max
    )

    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.MAXDOCACHI2CUT(comb_docachi2_max),
        F.MAXDOCACUT(comb_doca_max),
    )

    vertex_code = F.require_all(
        in_range(vtx_m_min, F.MASS, vtx_m_max),
        F.PT > vtx_pt_min,
        F.CHI2DOF < vtx_chi2dof_max,
        F.BPVDIRA(pvs) > vtx_dira_min,
        F.BPVFD(pvs) > vtx_fd_min,
        F.BPVFDCHI2(pvs) > vtx_fd_chi2_min,
        F.BPVIPCHI2(pvs) < vtx_ip_chi2_max,
    )

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=decay_descriptor,
        Combination12Cut=combination12_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@register_line_builder(all_lines)
@configurable
def Lb0ToHePP(name="Hlt2IFTTurbo_Lb0ToHePmPm", prescale=1):
    pvs = make_pvs()

    protons = filter_protons(make_long_protons(), pvs)
    helium = filter_helium(make_long_helium3(), pvs)

    lambda_b0 = make_lb_to_he(
        particles=[helium, protons, protons],
        decay_descriptor="[Lambda_b0 -> He3 p~- p~-]cc",
        pvs=pvs,
    )

    return Hlt2Line(
        name=name,
        algs=make_ift_pp_prefilters(pvs=pvs) + [lambda_b0],
        persistreco=False,
        raw_banks=["Rich"],
    )
