###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of builders for SMOG2 HLT2 lines for CEP events
"""

import Functors as F
from GaudiKernel.SystemOfUnits import MeV, mm
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter

from Hlt2Conf.lines.ift.builders.smog2_builders import sv_in_smog2


@configurable
def make_smog2_particle_no_PV(
    make_particles,
    max_trchi2dof=5,
    min_p=0.1 * MeV,
    min_pt=0.1 * MeV,
    pid_cut=None,
    particle=None,
):
    """
    Smog2 particles maker for particles coming from SMOG2 without requiring PVs

    Parameters
    ----------
    make_particles: Particle maker function
    max_trchi2dof : Maximum track chi2ndof (5 by default)
    min_p         : Minimum momentum (100 KeV by default)
    min_pt        : Minimum transverse momentum (100 KeV by default)
    pid_cut       : PID cut (for PIDx variables, PIDmu, PIDK, etc.) entered as a float
    particle      : Type of particle (Muon, Kaon, Pion, Proton, Electron) to select which PIDx variable to use
    """

    code = F.require_all(
        (F.P > min_p),
        (F.PT > min_pt),
        (F.CHI2DOF < max_trchi2dof),
        (F.TRACK_POS_CLOSESTTOBEAM_Z >= -541 * mm),
        (F.TRACK_POS_CLOSESTTOBEAM_Z <= -341 * mm),
    )

    if particle == "Muon":
        code &= F.ISMUON

    if pid_cut is not None:
        if particle == "Muon":
            code &= F.PID_MU > pid_cut
        elif particle == "Pion":
            code &= F.PID_K < pid_cut
        elif particle == "Kaon":
            code &= F.PID_K > pid_cut
        elif particle == "Proton":
            code &= F.PID_P > pid_cut

    return ParticleFilter(make_particles(), Cut=F.FILTER(code))


def make_smog2_2_track_cep(
    particle1,
    particle2,
    name,
    descriptor,
    apt_min,
    maxsdoca,
    vchi2pdof_max,
    single_pt_min=None,
):
    """
    Smog2 multi-particle maker for particles coming from SMOG2 without requiring PVs

    Parameters
    ----------
    particle {1,2}: Particle maker functions
    name:
    descriptor: Descriptor for the composite particle 2-body decay we look at
    apt_min: Minimum pt for the composite particle
    maxsdoca: The maximum distance of closest approach
    vchi2pdof_max: The max dof for the vertex algorithm
    single_pt_min: The minimum pt that a given daughter must have
    """

    combination_code = F.require_all(F.PT > apt_min, F.MAXSDOCACUT(maxsdoca))
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max, sv_in_smog2())

    if single_pt_min is not None:
        vertex_code &= (F.MAX_ELEMENT @ F.MAP(F.PT) @ F.GET_ALL_BASICS) > single_pt_min

    return ParticleCombiner(
        Inputs=[particle1, particle2],
        DecayDescriptor=descriptor,
        name=name,
        CombinationCut=combination_code,
        AllowDiffInputsForSameIDChildren=True,
        CompositeCut=vertex_code,
    )


def make_smog2_3_track_cep(
    particle1,
    particle2,
    particle3,
    name,
    descriptor,
    apt_min,
    maxsdoca,
    vchi2pdof_max,
    single_pt_min=None,
):
    """
    Smog2 multi-particle maker for particles coming from SMOG2 without requiring PVs

    Parameters
    ----------
    particle {1,2,3}: Particle maker functions
    name:
    descriptor: Descriptor for the composite particle 3-body decay we look at
    apt_min: Minimum pt for the composite particle
    maxsdoca: The maximum distance of closest approach
    vchi2pdof_max: The max dof for the vertex algorithm
    single_pt_min: The minimum pt that a given daughter must have
    """

    combination_code = F.require_all(F.PT > apt_min, F.MAXSDOCACUT(maxsdoca))
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max, sv_in_smog2())

    if single_pt_min is not None:
        vertex_code &= (F.MAX_ELEMENT @ F.MAP(F.PT) @ F.GET_ALL_BASICS) > single_pt_min

    return ParticleCombiner(
        Inputs=[particle1, particle2, particle3],
        DecayDescriptor=descriptor,
        name=name,
        CombinationCut=combination_code,
        AllowDiffInputsForSameIDChildren=True,
        CompositeCut=vertex_code,
    )


def make_smog2_4_track_cep(
    particle1,
    particle2,
    particle3,
    particle4,
    name,
    descriptor,
    apt_min,
    maxsdoca,
    vchi2pdof_max,
    single_pt_min=None,
):
    """
    Smog2 multi-particle maker for particles coming from SMOG2 without requiring PVs

    Parameters
    ----------
    particle {1,2,3,4}: Particle maker functions
    name:
    descriptor: Descriptor for the composite particle 4-body decay we look at
    apt_min: Minimum pt for the composite particle
    maxsdoca: The maximum distance of closest approach
    vchi2pdof_max: The max dof for the vertex algorithm
    single_pt_min: The minimum pt that a given daughter must have
    """

    combination_code = F.require_all(F.PT > apt_min, F.MAXSDOCACUT(maxsdoca))
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max, sv_in_smog2())

    if single_pt_min is not None:
        vertex_code &= (F.MAX_ELEMENT @ F.MAP(F.PT) @ F.GET_ALL_BASICS) > single_pt_min

    return ParticleCombiner(
        Inputs=[particle1, particle2, particle3, particle4],
        DecayDescriptor=descriptor,
        name=name,
        CombinationCut=combination_code,
        AllowDiffInputsForSameIDChildren=True,
        CompositeCut=vertex_code,
    )


def make_smog2_5_track_cep(
    particle1,
    particle2,
    particle3,
    particle4,
    particle5,
    name,
    descriptor,
    apt_min,
    maxsdoca,
    vchi2pdof_max,
    single_pt_min=None,
):
    """
    Smog2 multi-particle maker for particles coming from SMOG2 without requiring PVs

    Parameters
    ----------
    particle {1,2,3,4,5}: Particle maker functions
    name:
    descriptor: Descriptor for the composite particle 4-body decay we look at
    apt_min: Minimum pt for the composite particle
    maxsdoca: The maximum distance of closest approach
    vchi2pdof_max: The max dof for the vertex algorithm
    single_pt_min: The minimum pt that a given daughter must have
    """

    combination_code = F.require_all(F.PT > apt_min, F.MAXSDOCACUT(maxsdoca))
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max, sv_in_smog2())

    if single_pt_min is not None:
        vertex_code &= (F.MAX_ELEMENT @ F.MAP(F.PT) @ F.GET_ALL_BASICS) > single_pt_min

    return ParticleCombiner(
        Inputs=[particle1, particle2, particle3, particle4, particle5],
        DecayDescriptor=descriptor,
        name=name,
        CombinationCut=combination_code,
        AllowDiffInputsForSameIDChildren=True,
        CompositeCut=vertex_code,
    )
