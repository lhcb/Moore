###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of builders for SMOG2 HLT2 lines for charged PID calibration
"""

import Functors as F
from RecoConf.algorithms_thor import ParticleCombiner

from Hlt2Conf.lines.ift.builders.smog2_builders import (
    bpv_in_smog2,
    sv_in_smog2,
)


def make_smog2_generic_2body(
    pvs,
    particle1,
    particle2,
    name,
    descriptor,
    apt_min,
    maxsdoca,
    vchi2pdof_max,
    single_pt_min=None,
    mipchi2_min=None,
    bpvvdchi2_min=None,
    sv_insmog=True,
    minmass=None,
    min_bpvltime=None,
):
    combination_code = F.require_all(F.PT > apt_min, F.MAXSDOCACUT(maxsdoca))
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        bpv_in_smog2(pvs),
    )
    if single_pt_min is not None:
        vertex_code &= (F.MAX_ELEMENT @ F.MAP(F.PT) @ F.GET_ALL_BASICS) > single_pt_min
    if mipchi2_min is not None:
        vertex_code &= (
            F.MAX_ELEMENT @ F.MAP(F.MINIPCHI2(pvs())) @ F.GET_ALL_BASICS
        ) > mipchi2_min
    if bpvvdchi2_min is not None:
        vertex_code &= F.BPVFDCHI2(pvs()) > bpvvdchi2_min
    if min_bpvltime is not None:
        vertex_code &= F.BPVLTIME(pvs()) > min_bpvltime
    if sv_insmog:
        vertex_code &= sv_in_smog2()
    if minmass is not None:
        vertex_code &= F.MASS > minmass

    return ParticleCombiner(
        Inputs=[particle1, particle2],
        DecayDescriptor=descriptor,
        name=name,
        CombinationCut=combination_code,
        AllowDiffInputsForSameIDChildren=True,
        CompositeCut=vertex_code,
    )


def make_smog2_generic_3body(
    pvs,
    particle1,
    particle2,
    particle3,
    name,
    descriptor,
    apt_min,
    maxsdoca,
    vchi2pdof_max,
    single_pt_min=None,
    mipchi2_min=None,
    bpvvdchi2_min=None,
    sv_insmog=True,
    minmass=None,
    min_bpvltime=None,
):
    combination_code = F.require_all(F.PT > apt_min, F.MAXSDOCACUT(maxsdoca))
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        bpv_in_smog2(pvs),
    )
    if single_pt_min is not None:
        vertex_code &= (F.MAX_ELEMENT @ F.MAP(F.PT) @ F.GET_ALL_BASICS) > single_pt_min
    if mipchi2_min is not None:
        vertex_code &= (
            F.MAX_ELEMENT @ F.MAP(F.MINIPCHI2(pvs())) @ F.GET_ALL_BASICS
        ) > mipchi2_min
    if bpvvdchi2_min is not None:
        vertex_code &= F.BPVFDCHI2(pvs()) > bpvvdchi2_min
    if min_bpvltime is not None:
        vertex_code &= F.BPVLTIME(pvs()) > min_bpvltime
    if sv_insmog:
        vertex_code &= sv_in_smog2()
    if minmass is not None:
        vertex_code &= F.MASS > minmass

    return ParticleCombiner(
        Inputs=[particle1, particle2, particle3],
        DecayDescriptor=descriptor,
        name=name,
        CombinationCut=combination_code,
        AllowDiffInputsForSameIDChildren=True,
        CompositeCut=vertex_code,
    )
