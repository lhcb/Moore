###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of builders for isolated high pt photon.
Author: Cesar da Silva  cldasilv@cern.ch
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV
from PyConf import configurable
from PyConf.Algorithms import WeightedRelTableAlg
from RecoConf.algorithms_thor import ParticleFilter

from Hlt2Conf.standard_jets import make_onlytrack_particleflow


@configurable
def make_isolated_photons(
    photons,
    name="IsolatedPhotonMaker_{hash}",
    max_cone_pt=3.0 * GeV,
    pflow_output=make_onlytrack_particleflow,
):
    ftAlg = WeightedRelTableAlg(
        ReferenceParticles=photons,
        InputCandidates=pflow_output(),
        Cut=in_range(0.0, F.DR2, 0.5**2),
    )

    ftAlg_Rels = ftAlg.OutputRelations
    code = F.require_all(F.SUMCONE(Functor=F.PT, Relations=ftAlg_Rels) < max_cone_pt)

    return ParticleFilter(photons, F.FILTER(code), name=name)
