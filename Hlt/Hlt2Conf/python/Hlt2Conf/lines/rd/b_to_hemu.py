###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of B -> h e mu lines (including same-sign leptons).

Currently includes:

B+ -> K+ e mu
B+ -> pi+ e mu
Bs -> phi(-> K+ K-) e mu
Bd -> K*(892)0(-> K+ pi-) e mu
Lb -> p K e mu
"""

import Functors as F
from GaudiKernel.SystemOfUnits import MeV
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf import rdbuilder_thor
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.rd.builders.rd_isolation import parent_isolation_output

from .builders import b_to_hemu_builders
from .builders.rd_prefilters import (
    _VRD_MONITORING_VARIABLES,
    RD_PERSIST_CALO_CLUSTERS,
    RD_PERSIST_CALO_DIGITS,
    rd_prefilter,
)

all_lines = {}


@register_line_builder(all_lines)
@configurable
def butokemu_line(name="Hlt2RD_BuToKpEMu", prescale=1):
    pvs = make_pvs()
    electrons = rdbuilder_thor.make_rd_detached_electrons(
        mipchi2dvprimary_min=9, pid=(F.PID_E > 2.0)
    )
    muons = rdbuilder_thor.make_rd_detached_muons(mipchi2dvprimary_min=9)
    kaons = rdbuilder_thor.make_rd_has_rich_detached_kaons(mipchi2dvprimary_min=9)
    dileptons = b_to_hemu_builders.make_dilepton(
        muons, electrons, pvs, "[J/psi(1S) -> mu+ e-]cc"
    )
    bu = b_to_hemu_builders.make_btohemu(
        kaons, dileptons, pvs, "[B+ -> J/psi(1S) K+]cc"
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dileptons, bu],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Bu", bu),
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def butopiemu_line(name="Hlt2RD_BuToPipEMu", prescale=1):
    pvs = make_pvs()
    electrons = rdbuilder_thor.make_rd_detached_electrons(
        mipchi2dvprimary_min=9, pid=(F.PID_E > 2.0)
    )
    muons = rdbuilder_thor.make_rd_detached_muons(mipchi2dvprimary_min=9)
    pions = rdbuilder_thor.make_rd_has_rich_detached_pions(mipchi2dvprimary_min=9)
    dileptons = b_to_hemu_builders.make_dilepton(
        muons, electrons, pvs, "[J/psi(1S) -> mu+ e-]cc"
    )
    bu = b_to_hemu_builders.make_btohemu(
        pions, dileptons, pvs, "[B+ -> J/psi(1S) pi+]cc"
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dileptons, bu],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Bu", bu),
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def bstophiemu_line(name="Hlt2RD_BsToPhiEMu_PhiToKK", prescale=1):
    pvs = make_pvs()
    electrons = rdbuilder_thor.make_rd_detached_electrons(
        mipchi2dvprimary_min=9, pid=(F.PID_E > 2.0)
    )
    muons = rdbuilder_thor.make_rd_detached_muons(mipchi2dvprimary_min=9)
    kaons = rdbuilder_thor.make_rd_has_rich_detached_kaons(
        mipchi2dvprimary_min=9, pid=(F.PID_K > 2.0)
    )
    dileptons = b_to_hemu_builders.make_dilepton(
        muons, electrons, pvs, "[J/psi(1S) -> mu+ e-]cc"
    )
    dikaons = b_to_hemu_builders.make_hh(
        kaons,
        kaons,
        pvs,
        "phi(1020) -> K+ K-",
        pt_min=400 * MeV,
        m_min=0 * MeV,
        m_max=1100 * MeV,
    )
    bs = b_to_hemu_builders.make_btohemu(
        dikaons, dileptons, pvs, "[B+ -> J/psi(1S) phi(1020)]cc"
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dikaons, dileptons, bs],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Bs", bs),
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def bdtokstemu_line(name="Hlt2RD_BdToKstEMu_KstToKpPim", prescale=1):
    pvs = make_pvs()
    electrons = rdbuilder_thor.make_rd_detached_electrons(
        mipchi2dvprimary_min=9, pid=(F.PID_E > 2.0)
    )
    muons = rdbuilder_thor.make_rd_detached_muons(mipchi2dvprimary_min=9)
    kaons = rdbuilder_thor.make_rd_has_rich_detached_kaons(
        mipchi2dvprimary_min=9, pid=(F.PID_K > 2.0)
    )
    pions = rdbuilder_thor.make_rd_has_rich_detached_pions(mipchi2dvprimary_min=9)
    dileptons = b_to_hemu_builders.make_dilepton(
        muons, electrons, pvs, "[J/psi(1S) -> mu+ e-]cc"
    )
    kstars = b_to_hemu_builders.make_hh(kaons, pions, pvs, "[K*(892)0 -> K+ pi-]cc")
    bd = b_to_hemu_builders.make_btohemu(
        kstars, dileptons, pvs, "[B+ -> J/psi(1S) K*(892)0]cc"
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kstars, dileptons, bd],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Bd", bd),
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def lbtopkemu_line(name="Hlt2RD_LbToPKEMu", prescale=1):
    pvs = make_pvs()
    electrons = rdbuilder_thor.make_rd_detached_electrons(
        mipchi2dvprimary_min=9, pid=(F.PID_E > 2.0)
    )
    muons = rdbuilder_thor.make_rd_detached_muons(mipchi2dvprimary_min=9)
    dileptons = b_to_hemu_builders.make_dilepton(
        muons, electrons, pvs, "[J/psi(1S) -> mu+ e-]cc"
    )
    kaons = rdbuilder_thor.make_rd_has_rich_detached_kaons(
        mipchi2dvprimary_min=9, pid=(F.PID_K > 2)
    )
    protons = rdbuilder_thor.make_rd_has_rich_detached_protons(
        mipchi2dvprimary_min=9,
        pid=F.require_all(F.PID_P > 4.0, F.PID_P - F.PID_K > 2.0),
    )
    dihadron = b_to_hemu_builders.make_hh(
        protons,
        kaons,
        pvs,
        "[Lambda(1520)0 -> p+ K-]cc",
        pt_min=400 * MeV,
        m_min=0 * MeV,
        m_max=2600 * MeV,
    )
    lb = b_to_hemu_builders.make_btohemu(
        dihadron, dileptons, pvs, "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dihadron, dileptons, lb],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Lb", lb),
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def butokemuSS_line(name="Hlt2RD_BuToKpEMu_SameSign", prescale=1):
    pvs = make_pvs()
    electrons = rdbuilder_thor.make_rd_detached_electrons(
        mipchi2dvprimary_min=9, pid=(F.PID_E > 2.0)
    )
    muons = rdbuilder_thor.make_rd_detached_muons(mipchi2dvprimary_min=9)
    kaons = rdbuilder_thor.make_rd_has_rich_detached_kaons(
        mipchi2dvprimary_min=9, pid=(F.PID_K > 2.0)
    )
    dileptons = b_to_hemu_builders.make_dilepton(
        muons, electrons, pvs, "[J/psi(1S) -> mu+ e+]cc"
    )
    bu = b_to_hemu_builders.make_btohemu(
        kaons, dileptons, pvs, "[B+ -> J/psi(1S) K+]cc"
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dileptons, bu],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Bu", bu),
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def butopiemuSS_line(name="Hlt2RD_BuToPipEMu_SameSign", prescale=1):
    pvs = make_pvs()
    electrons = rdbuilder_thor.make_rd_detached_electrons(
        mipchi2dvprimary_min=9, pid=(F.PID_E > 2.0)
    )
    muons = rdbuilder_thor.make_rd_detached_muons(mipchi2dvprimary_min=9)
    pions = rdbuilder_thor.make_rd_has_rich_detached_pions(mipchi2dvprimary_min=9)
    dileptons = b_to_hemu_builders.make_dilepton(
        muons, electrons, pvs, "[J/psi(1S) -> mu+ e+]cc"
    )
    bu = b_to_hemu_builders.make_btohemu(
        pions, dileptons, pvs, "[B+ -> J/psi(1S) pi+]cc"
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dileptons, bu],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Bu", bu),
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def bstophiemuSS_line(name="Hlt2RD_BsToPhiEMu_SameSign_PhiToKK", prescale=1):
    pvs = make_pvs()
    electrons = rdbuilder_thor.make_rd_detached_electrons(
        mipchi2dvprimary_min=9, pid=(F.PID_E > 2.0)
    )
    muons = rdbuilder_thor.make_rd_detached_muons(mipchi2dvprimary_min=9)
    kaons = rdbuilder_thor.make_rd_has_rich_detached_kaons(
        mipchi2dvprimary_min=9, pid=(F.PID_K > 2.0)
    )
    dileptons = b_to_hemu_builders.make_dilepton(
        muons, electrons, pvs, "[J/psi(1S) -> mu+ e+]cc"
    )
    dikaons = b_to_hemu_builders.make_hh(
        kaons,
        kaons,
        pvs,
        "phi(1020) -> K+ K-",
        pt_min=400 * MeV,
        m_min=0 * MeV,
        m_max=1100 * MeV,
    )
    bs = b_to_hemu_builders.make_btohemu(
        dikaons, dileptons, pvs, "[B+ -> J/psi(1S) phi(1020)]cc"
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dikaons, dileptons, bs],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Bs", bs),
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def bdtokstemuSS_line(name="Hlt2RD_BdToKstEMu_SameSign_KstToKpPim", prescale=1):
    pvs = make_pvs()
    electrons = rdbuilder_thor.make_rd_detached_electrons(
        mipchi2dvprimary_min=9, pid=(F.PID_E > 2.0)
    )
    muons = rdbuilder_thor.make_rd_detached_muons(mipchi2dvprimary_min=9)
    kaons = rdbuilder_thor.make_rd_has_rich_detached_kaons(
        mipchi2dvprimary_min=9, pid=(F.PID_K > 2.0)
    )
    pions = rdbuilder_thor.make_rd_has_rich_detached_pions(mipchi2dvprimary_min=9)
    dileptons = b_to_hemu_builders.make_dilepton(
        muons, electrons, pvs, "[J/psi(1S) -> mu+ e+]cc"
    )
    kstars = b_to_hemu_builders.make_hh(kaons, pions, pvs, "[K*(892)0 -> K+ pi-]cc")
    bd = b_to_hemu_builders.make_btohemu(
        kstars, dileptons, pvs, "[B+ -> J/psi(1S) K*(892)0]cc"
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kstars, dileptons, bd],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Bd", bd),
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def lbtopkemuSS_line(name="Hlt2RD_LbToPKEMu_SameSign", prescale=1):
    pvs = make_pvs()
    electrons = rdbuilder_thor.make_rd_detached_electrons(
        mipchi2dvprimary_min=9, pid=(F.PID_E > 2.0)
    )
    muons = rdbuilder_thor.make_rd_detached_muons(mipchi2dvprimary_min=9)
    dileptons = b_to_hemu_builders.make_dilepton(
        muons, electrons, pvs, "[J/psi(1S) -> mu+ e+]cc"
    )
    kaons = rdbuilder_thor.make_rd_has_rich_detached_kaons(
        mipchi2dvprimary_min=9, pid=(F.PID_K > 2)
    )
    protons = rdbuilder_thor.make_rd_has_rich_detached_protons(
        mipchi2dvprimary_min=9,
        pid=F.require_all(F.PID_P > 4.0, F.PID_P - F.PID_K > 2.0),
    )
    dihadron = b_to_hemu_builders.make_hh(
        protons,
        kaons,
        pvs,
        "[Lambda(1520)0 -> p+ K-]cc",
        pt_min=400 * MeV,
        m_min=0 * MeV,
        m_max=2600 * MeV,
    )
    lb = b_to_hemu_builders.make_btohemu(
        dihadron, dileptons, pvs, "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dihadron, dileptons, lb],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Lb", lb),
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )
