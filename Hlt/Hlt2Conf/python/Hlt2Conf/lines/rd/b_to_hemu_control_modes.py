###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of control modes for B -> h e mu lines.
Currently includes :

B+ -> K+ Jpsi (mumu)                | Hlt2RD_BuToKpJpsi_JpsiToMuMu
B+ -> K+ Jpsi (ee)                  | Hlt2RD_BuToKpJpsi_JpsiToEE
B+ -> pi+ Jpsi(mumu)                | Hlt2RD_BuToPipJpsi_JpsiToMuMu
B+ -> pi+ Jpsi(mumu)                | Hlt2RD_BuToPipJpsi_JpsiToEE
Bs -> phi(-> K+ K-) Jpsi(mumu)      | Hlt2RD_BsToPhiJpsi_PhiToKK_JpsiToEE
Bs -> phi(-> K+ K-) Jpsi(ee)        | Hlt2RD_BsToPhiJpsi_PhiToKK_JpsiToEE
Bd -> K*(892)0(-> K+ pi-) Jpsi(mumu)| Hlt2RD_BdToKstJpsi_KstToKpPim_JpsiToMuMu
Bd -> K*(892)0(-> K+ pi-) Jpsi(ee)  | Hlt2RD_BdToKstJpsi_KstToKpPim_JpsiToEE
Lb -> p K Jpsi(mumu,ee)             | Hlt2RD_LbToPKJpsi_JpsiToEE
Lb -> p K Jpsi(mumu,ee)             | Hlt2RD_LbToPKJpsi_JpsiToEE

NB:
Few of the lines has varied due to bugs in 2024 data Moore Versions the line descriptors
Please double check the variations



pre-TS vs post-TS
- mramos   changed https://gitlab.cern.ch/lhcb/Moore/-/merge_requests/3394/diffs, check history of this file in various releases to see exactly what has been used for data.
    Hlt2RD_BdToKstJpsi_KstToKpPim_JpsiToMuMu,
        before was [B+ -> J/psi(1S) K*(892)0]cc
        after  MR  [B0 -> J/psi(1S) K*(892)0]cc
    Hlt2RD_BsToPhiJpsi_PhiToKK_JpsiToMuMu
        before was [B+ -> J/psi(1S) phi(1020)]cc
        after  MR   B_s0 -> J/psi(1S) phi(1020)
July 2024
- rquaglia changed the following lines
    Hlt2RD_BsToPhiJpsi_PhiToKK_JpsiToEE
        before was "[B+ -> J/psi(1S) phi(1020)]cc"
        after is "B_s0 -> J/psi(1S) phi(1020)"
    Hlt2RD_BdToKstJpsi_KstToKpPim_JpsiToEE
        before was "[B+ -> J/psi(1S) K*(892)0]cc"
        after is   "[B0 -> J/psi(1S) K*(892)0]cc"
"""

import Functors as F
from GaudiKernel.SystemOfUnits import MeV
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf import rdbuilder_thor
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.rd.builders.rd_isolation import parent_isolation_output

from .builders import b_to_hemu_builders
from .builders.rd_prefilters import (
    _RD_MONITORING_VARIABLES,
    RD_PERSIST_CALO_CLUSTERS,
    RD_PERSIST_CALO_DIGITS,
    rd_prefilter,
)

all_lines = {}


@register_line_builder(all_lines)
@configurable
def butokjpsi_mumu_line(name="Hlt2RD_BuToKpJpsi_JpsiToMuMu", prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(
        mipchi2dvprimary_min=9, pid=(F.PID_K > 2)
    )
    dileptons = rdbuilder_thor.make_rd_detached_dimuon(am_max=5500 * MeV)
    bu = b_to_hemu_builders.make_btohemu(
        kaons, dileptons, pvs, "[B+ -> J/psi(1S) K+]cc"
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dileptons, bu],
        prescale=prescale,
        persistreco=True,
        tagging_particles=True,
        extra_outputs=parent_isolation_output("Bu", bu),
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def butopijpsi_mumu_line(name="Hlt2RD_BuToPipJpsi_JpsiToMuMu", prescale=1):
    pvs = make_pvs()
    pions = rdbuilder_thor.make_rd_detached_pions(mipchi2dvprimary_min=9)
    dileptons = rdbuilder_thor.make_rd_detached_dimuon(am_max=5500 * MeV)
    bu = b_to_hemu_builders.make_btohemu(
        pions, dileptons, pvs, "[B+ -> J/psi(1S) pi+]cc"
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dileptons, bu],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Bu", bu),
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def bstophijpsi_mumu_line(name="Hlt2RD_BsToPhiJpsi_PhiToKK_JpsiToMuMu", prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(
        mipchi2dvprimary_min=9, pid=(F.PID_K > 2)
    )
    dileptons = rdbuilder_thor.make_rd_detached_dimuon(am_max=5500 * MeV)
    dikaons = b_to_hemu_builders.make_hh(
        kaons,
        kaons,
        pvs,
        "phi(1020) -> K+ K-",
        pt_min=400 * MeV,
        m_min=0 * MeV,
        m_max=1100 * MeV,
    )
    bs = b_to_hemu_builders.make_btohemu(
        dikaons, dileptons, pvs, "B_s0 -> J/psi(1S) phi(1020)"
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dikaons, dileptons, bs],
        prescale=prescale,
        persistreco=True,
        tagging_particles=True,
        extra_outputs=parent_isolation_output("Bs", bs),
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def bdtokstjpsi_mumu_line(name="Hlt2RD_BdToKstJpsi_KstToKpPim_JpsiToMuMu", prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(
        mipchi2dvprimary_min=9, pid=(F.PID_K > 2)
    )
    pions = rdbuilder_thor.make_rd_detached_pions(mipchi2dvprimary_min=9)
    dileptons = rdbuilder_thor.make_rd_detached_dimuon(am_max=5500 * MeV)
    kstars = b_to_hemu_builders.make_hh(kaons, pions, pvs, "[K*(892)0 -> K+ pi-]cc")
    bd = b_to_hemu_builders.make_btohemu(
        kstars, dileptons, pvs, "[B0 -> J/psi(1S) K*(892)0]cc"
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kstars, dileptons, bd],
        prescale=prescale,
        persistreco=True,
        tagging_particles=True,
        extra_outputs=parent_isolation_output("Bd", bd),
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def lbtopkjpsi_mumu_line(name="Hlt2RD_LbToPKJpsi_JpsiToMuMu", prescale=1):
    pvs = make_pvs()
    dileptons = rdbuilder_thor.make_rd_detached_dimuon(am_max=5500 * MeV)
    kaons = rdbuilder_thor.make_rd_detached_kaons(
        mipchi2dvprimary_min=9, pid=(F.PID_K > 2)
    )
    protons = rdbuilder_thor.make_rd_detached_protons(
        mipchi2dvprimary_min=9,
        pid=F.require_all(F.PID_P > 4.0, F.PID_P - F.PID_K > 0.0),
    )
    dihadron = b_to_hemu_builders.make_hh(
        protons,
        kaons,
        pvs,
        "[Lambda(1520)0 -> p+ K-]cc",
        pt_min=400 * MeV,
        m_min=0 * MeV,
        m_max=2600 * MeV,
    )
    lb = b_to_hemu_builders.make_btohemu(
        dihadron, dileptons, pvs, "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dihadron, dileptons, lb],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Lb", lb),
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def butokjpsi_ee_line(name="Hlt2RD_BuToKpJpsi_JpsiToEE", prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(
        mipchi2dvprimary_min=9, pid=(F.PID_K > 2)
    )
    dileptons = rdbuilder_thor.make_rd_detached_dielectron(
        pid_e_min=2, am_max=5500 * MeV
    )
    bu = b_to_hemu_builders.make_btohemu(
        kaons, dileptons, pvs, "[B+ -> J/psi(1S) K+]cc"
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dileptons, bu],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Bu", bu),
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def butopijpsi_ee_line(name="Hlt2RD_BuToPipJpsi_JpsiToEE", prescale=1):
    pvs = make_pvs()
    pions = rdbuilder_thor.make_rd_detached_pions(mipchi2dvprimary_min=9)
    dileptons = rdbuilder_thor.make_rd_detached_dielectron(
        pid_e_min=2, am_max=5500 * MeV
    )
    bu = b_to_hemu_builders.make_btohemu(
        pions, dileptons, pvs, "[B+ -> J/psi(1S) pi+]cc"
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dileptons, bu],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Bu", bu),
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def bstophijpsi_ee_line(name="Hlt2RD_BsToPhiJpsi_PhiToKK_JpsiToEE", prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(
        mipchi2dvprimary_min=9, pid=(F.PID_K > 2)
    )
    dileptons = rdbuilder_thor.make_rd_detached_dielectron(
        pid_e_min=2, am_max=5500 * MeV
    )
    dikaons = b_to_hemu_builders.make_hh(
        kaons,
        kaons,
        pvs,
        "phi(1020) -> K+ K-",
        pt_min=400 * MeV,
        m_min=0 * MeV,
        m_max=1100 * MeV,
    )
    # -rquaglia on July 2024
    # decay descriptor changed from B+ -> J/psi(1S) phi(1020) to Bs0 head particle and no CC
    # NB : in 2024 this line had different decay descriptors due to bugs,
    # If you read this, you need to be careful with simulation and Moore versions on which decay descriptor to use afterwards in your
    # Analysis production
    bs = b_to_hemu_builders.make_btohemu(
        dikaons, dileptons, pvs, "B_s0 -> J/psi(1S) phi(1020)"
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dikaons, dileptons, bs],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Bs", bs),
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def bdtokstjpsi_ee_line(name="Hlt2RD_BdToKstJpsi_KstToKpPim_JpsiToEE", prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(
        mipchi2dvprimary_min=9, pid=(F.PID_K > 2)
    )
    pions = rdbuilder_thor.make_rd_detached_pions(mipchi2dvprimary_min=9)
    dileptons = rdbuilder_thor.make_rd_detached_dielectron(
        pid_e_min=2, am_max=5500 * MeV
    )
    kstars = b_to_hemu_builders.make_hh(kaons, pions, pvs, "[K*(892)0 -> K+ pi-]cc")

    # -rquaglia on July 2024
    # decay descriptor changed from B+ -> J/psi(1S) K*(892)0 to B0 head particle
    # NB : in 2024 this line had different decay descriptors due to bugs,
    # If you read this, you need to be careful with simulation and Moore versions on which decay descriptor to use afterwards in your
    # Analysis production
    bd = b_to_hemu_builders.make_btohemu(
        kstars, dileptons, pvs, "[B0 -> J/psi(1S) K*(892)0]cc"
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kstars, dileptons, bd],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Bd", bd),
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def lbtopkjpsi_ee_line(name="Hlt2RD_LbToPKJpsi_JpsiToEE", prescale=1):
    pvs = make_pvs()
    dileptons = rdbuilder_thor.make_rd_detached_dielectron(
        pid_e_min=2, am_max=5500 * MeV
    )
    kaons = rdbuilder_thor.make_rd_detached_kaons(
        mipchi2dvprimary_min=9, pid=(F.PID_K > 2)
    )
    protons = rdbuilder_thor.make_rd_detached_protons(
        mipchi2dvprimary_min=9,
        pid=F.require_all(F.PID_P > 4.0, F.PID_P - F.PID_K > 0.0),
    )
    dihadron = b_to_hemu_builders.make_hh(
        protons,
        kaons,
        pvs,
        "[Lambda(1520)0 -> p+ K-]cc",
        pt_min=400 * MeV,
        m_min=0 * MeV,
        m_max=2600 * MeV,
    )
    lb = b_to_hemu_builders.make_btohemu(
        dihadron, dileptons, pvs, "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dihadron, dileptons, lb],
        prescale=prescale,
        extra_outputs=parent_isolation_output("Lb", lb),
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )
