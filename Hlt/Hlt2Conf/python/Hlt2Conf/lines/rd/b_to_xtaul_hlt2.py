###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Author: Tommaso Fulghesu
Contact: tommaso.fulghesu@cern.ch
Date: 07/12/2021
"""

######################################################################################
####                                                                              ####
#### The following exlusive lines Yb -> X tau (-> 3pi nu_tau) l cover the decays: ####
####                                                                              ####
#### B0 -> K* (-> K pi) tau (-> 3pi nu_tau) tau (-> 3pi nu_tau)                   ####
#### B0 -> K* (-> K pi) tau (-> 3pi nu_tau) mu                                    ####
#### B0 -> K* (-> K pi) tau (-> 3pi nu_tau) e                                     ####
#### B0 -> phi (-> K K) tau (-> 3pi nu_tau) tau (-> 3pi nu_tau)                   ####
#### B0 -> phi (-> K K) tau (-> 3pi nu_tau) mu                                    ####
#### B0 -> phi (-> K K) tau (-> 3pi nu_tau) e                                     ####
#### B0 -> rho (-> pi pi) tau (-> 3pi nu_tau) tau (-> 3pi nu_tau)                 ####
#### B0 -> rho (-> pi pi) tau (-> 3pi nu_tau) mu                                  ####
#### B0 -> rho (-> pi pi) tau (-> 3pi nu_tau) e                                   ####
#### B0 -> eta' (-> pi pi gamma) tau (-> 3pi nu_tau) tau (-> 3pi nu_tau)          ####
#### B0 -> eta' (-> pi pi gamma) tau (-> 3pi nu_tau) mu                           ####
#### B0 -> eta' (-> pi pi gamma) tau (-> 3pi nu_tau) e                            ####
#### B0 -> Ks (-> pi pi) tau (-> 3pi nu_tau) tau (-> 3pi nu_tau)                  ####
#### B0 -> Ks (-> pi pi) tau (-> 3pi nu_tau) mu                                   ####
#### B0 -> Ks (-> pi pi) tau (-> 3pi nu_tau) e                                    ####
#### Bu -> K+ tau (-> 3pi nu_tau) tau (-> 3pi nu_tau)                             ####
#### Bu -> K+ tau (-> 3pi nu_tau) mu                                              ####
#### Bu -> K+ tau (-> 3pi nu_tau) e                                               ####
#### Bu -> K1(1270)+ (-> K pi pi) tau (-> 3pi nu_tau) tau (-> 3pi nu_tau)         ####
#### Bu -> K1(1270)+ (-> K pi pi) tau (-> 3pi nu_tau) mu                          ####
#### Bu -> K1(1270)+ (-> K pi pi) tau (-> 3pi nu_tau) e                           ####
#### Lb -> K p tau (-> 3pi nu_tau) tau (-> 3pi nu_tau)                            ####
#### Lb -> K p tau (-> 3pi nu_tau) mu                                             ####
#### Lb -> K p tau (-> 3pi nu_tau) e                                              ####
#### Lb -> Lambda (-> pi p) tau (-> 3pi nu_tau) tau (-> 3pi nu_tau)               ####
#### Lb -> Lambda (-> pi p) tau (-> 3pi nu_tau) mu                                ####
#### Lb -> Lambda (-> pi p) tau (-> 3pi nu_tau) e                                 ####
####                                                                              ####
######################################################################################

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf import rdbuilder_thor as common_builder
from RecoConf.algorithms_thor import ParticleFilter

from Hlt2Conf.lines.rd.builders import b_to_xtaul_rd_builder as builder
from Hlt2Conf.lines.rd.builders import rd_isolation
from Hlt2Conf.lines.rd.builders.rd_prefilters import (
    _RD_MONITORING_VARIABLES,
    RD_PERSIST_CALO_CLUSTERS,
    RD_PERSIST_CALO_DIGITS,
    rd_prefilter,
)

all_lines = {}


def make_kstar0s_with_tighter_mass():
    return ParticleFilter(
        common_builder.make_rd_detached_kstar0s(),
        F.FILTER(F.require_all(F.MASS > 700.0 * MeV, F.MASS < 1100.0 * MeV)),
        name="rd_detached_kstar0s_mass_btoxtaul_{hash}",
    )


def make_rho0s_with_tighter_mass():
    return ParticleFilter(
        common_builder.make_rd_detached_rho0(),
        F.FILTER(F.require_all(F.MASS > 500.0 * MeV, F.MASS < 1100 * MeV)),
        name="rd_detached_rho0s_mass_btoxtaul_{hash}",
    )


## B0 -> K* tau l
@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2KstTauTau_OS_ExclusiveLine(
    name="Hlt2RD_BdToKstTauTau_KstToKPi_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (K*->K+ pi-) (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    """
    kst = make_kstar0s_with_tighter_mass()
    dilepton = builder.make_dilepton_from_tauls(child_id="tau+")
    decay_descriptor = "[B0 -> J/psi(1S) K*(892)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kst], descriptors=decay_descriptor
    )

    kst_child = rd_isolation.find_in_decay(input=b2xtaul, id="K*(892)0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    # Make cone isolation for parent and direct products
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "Kst", "tau"],
        candidates=[b2xtaul, kst_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    # Make vertex isolation for parent particle
    # Note: Add first two decay descriptors for the two-body combinations, then the decay descriptor for the three-body combination

    vtx_iso_comb = rd_isolation.select_combinations_for_vertex_isolation(
        name="B",
        candidate=b2xtaul,
        max_two_body_vtx_cut=9.0,
        max_three_body_vtx_cut=9.0,
        use_fiducial_cut=True,
    )

    iso_parts += vtx_iso_comb

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, kst, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2KstTauTau_SS_ExclusiveLine(
    name="Hlt2RD_BdToKstTauTau_KstToKPi_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (K*->K+ pi-) (tau+->pi+pi+pi-) (tau+->pi-pi-pi+) + CC

          and for Bd -> (K*->K+ pi-) (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    kst = make_kstar0s_with_tighter_mass()
    dilepton = builder.make_dilepton_from_tauls(child_id="tau-")
    decay_descriptor = "[B0 ->  J/psi(1S) K*(892)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kst], descriptors=decay_descriptor
    )

    kst_child = rd_isolation.find_in_decay(input=b2xtaul, id="K*(892)0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    # Make cone isolation for parent and direct products
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "Kst", "tau"],
        candidates=[b2xtaul, kst_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    # Make vertex isolation for parent particle
    # Note: Add first two decay descriptors for the two-body combinations, then the decay descriptor for the three-body combination
    vtx_iso_comb = rd_isolation.select_combinations_for_vertex_isolation(
        name="B",
        candidate=b2xtaul,
        max_two_body_vtx_cut=9.0,
        max_three_body_vtx_cut=9.0,
        use_fiducial_cut=True,
    )

    iso_parts += vtx_iso_comb

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, kst, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2KstTauMu_OS_ExclusiveLine(
    name="Hlt2RD_BdToKstTauMu_KstToKPi_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (K*->K+ pi-) (tau+->pi+pi+pi-) mu- + CC

        and for Bd -> (K*->K+ pi-) (tau-->pi-pi-pi+) mu+ + CC
    """
    kst = make_kstar0s_with_tighter_mass()
    dilepton = builder.make_dilepton_from_tauls(child_id="mu+")
    decay_descriptor = "[B0 -> J/psi(1S) K*(892)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kst], descriptors=decay_descriptor
    )

    kst_child = rd_isolation.find_in_decay(input=b2xtaul, id="K*(892)0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=b2xtaul, id="mu-")

    # Make cone isolation for parent and direct products
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "Kst", "tau"],
        candidates=[b2xtaul, kst_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    # Make vertex isolation for parent particle
    # Note: Add first two decay descriptors for the two-body combinations, then the decay descriptor for the three-body combination
    vtx_iso_comb = rd_isolation.select_combinations_for_vertex_isolation(
        name="B",
        candidate=b2xtaul,
        max_two_body_vtx_cut=9.0,
        max_three_body_vtx_cut=9.0,
        use_fiducial_cut=True,
    )

    iso_parts += vtx_iso_comb

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, kst, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2KstTauMu_SS_ExclusiveLine(
    name="Hlt2RD_BdToKstTauMu_KstToKPi_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (K*->K+ pi-) (tau+->pi+pi+pi-) mu+ + CC

        and for Bd -> (K*->K+ pi-) (tau-->pi-pi-pi+) mu- + CC
    """
    kst = make_kstar0s_with_tighter_mass()
    dilepton = builder.make_dilepton_from_tauls(child_id="mu-")
    decay_descriptor = "[B0 ->  J/psi(1S) K*(892)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kst], descriptors=decay_descriptor
    )

    kst_child = rd_isolation.find_in_decay(input=b2xtaul, id="K*(892)0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=b2xtaul, id="mu-")

    # Make cone isolation for parent and direct products
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "Kst", "tau"],
        candidates=[b2xtaul, kst_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    # Make vertex isolation for parent particle
    # Note: Add first two decay descriptors for the two-body combinations, then the decay descriptor for the three-body combination
    vtx_iso_comb = rd_isolation.select_combinations_for_vertex_isolation(
        name="B",
        candidate=b2xtaul,
        max_two_body_vtx_cut=9.0,
        max_three_body_vtx_cut=9.0,
        use_fiducial_cut=True,
    )

    iso_parts += vtx_iso_comb

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, kst, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2KstTauE_OS_ExclusiveLine(
    name="Hlt2RD_BdToKstTauE_KstToKPi_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (K*->K+ pi-) (tau+->pi+pi+pi-) e- + CC

          and for Bd -> (K*->K+ pi-) (tau-->pi-pi-pi+) e+ + CC
    """
    kst = make_kstar0s_with_tighter_mass()
    dilepton = builder.make_dilepton_from_tauls(child_id="e+")
    decay_descriptor = "[B0 ->  J/psi(1S) K*(892)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kst], descriptors=decay_descriptor
    )

    kst_child = rd_isolation.find_in_decay(input=b2xtaul, id="K*(892)0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=b2xtaul, id="e-")

    # Make cone isolation for parent and direct products
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "Kst", "tau"],
        candidates=[b2xtaul, kst_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    # Make vertex isolation for parent particle
    # Note: Add first two decay descriptors for the two-body combinations, then the decay descriptor for the three-body combination
    vtx_iso_comb = rd_isolation.select_combinations_for_vertex_isolation(
        name="B",
        candidate=b2xtaul,
        max_two_body_vtx_cut=9.0,
        max_three_body_vtx_cut=9.0,
        use_fiducial_cut=True,
    )

    iso_parts += vtx_iso_comb

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, kst, b2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2KstTauE_SS_ExclusiveLine(
    name="Hlt2RD_BdToKstTauE_KstToKPi_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (K*->K+ pi-) (tau+->pi+pi+pi-) e+ + CC

          and for Bd -> (K*->K+ pi-) (tau-->pi-pi-pi+) e- + CC
    """
    kst = make_kstar0s_with_tighter_mass()
    dilepton = builder.make_dilepton_from_tauls(child_id="e-")
    decay_descriptor = "[B0 ->  J/psi(1S) K*(892)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kst], descriptors=decay_descriptor
    )

    kst_child = rd_isolation.find_in_decay(input=b2xtaul, id="K*(892)0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=b2xtaul, id="e-")

    # Make cone isolation for parent and direct products
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "Kst", "tau"],
        candidates=[b2xtaul, kst_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    # Make vertex isolation for parent particle
    # Note: Add first two decay descriptors for the two-body combinations, then the decay descriptor for the three-body combination
    vtx_iso_comb = rd_isolation.select_combinations_for_vertex_isolation(
        name="B",
        candidate=b2xtaul,
        max_two_body_vtx_cut=9.0,
        max_three_body_vtx_cut=9.0,
        use_fiducial_cut=True,
    )

    iso_parts += vtx_iso_comb

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, kst, b2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


## B0 -> phi tau l
@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2PhiTauTau_OS_ExclusiveLine(
    name="Hlt2RD_BdToPhiTauTau_PhiToKK_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (phi->K+ K-) (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    """
    phi = builder.filter_rd_detached_phis()
    dilepton = builder.make_dilepton_from_tauls(child_id="tau+")
    decay_descriptor = "[B0 ->  J/psi(1S) phi(1020)]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, phi], descriptors=decay_descriptor
    )

    phi_child = rd_isolation.find_in_decay(input=b2xtaul, id="phi(1020)")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "phi", "tau"],
        candidates=[b2xtaul, phi_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, phi, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2PhiTauTau_SS_ExclusiveLine(
    name="Hlt2RD_BdToPhiTauTau_PhiToKK_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (phi->K+ K-) (tau+->pi+pi+pi-) (tau+->pi+pi+pi-) + CC

          and for Bd -> (phi->K+ K-) (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    phi = builder.filter_rd_detached_phis()
    dilepton = builder.make_dilepton_from_tauls(child_id="tau-")
    decay_descriptor = "[B0 ->  J/psi(1S) phi(1020)]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, phi], descriptors=decay_descriptor
    )

    phi_child = rd_isolation.find_in_decay(input=b2xtaul, id="phi(1020)")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "phi", "tau"],
        candidates=[b2xtaul, phi_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, phi, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2PhiTauMu_OS_ExclusiveLine(
    name="Hlt2RD_BdToPhiTauMu_PhiToKK_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (phi->K+ K-) (tau+->pi+pi+pi-) mu- + CC

          and for Bd -> (phi->K+ K-) (tau-->pi-pi-pi+) mu+ + CC
    """
    phi = builder.filter_rd_detached_phis()
    dilepton = builder.make_dilepton_from_tauls(child_id="mu+")
    decay_descriptor = "[B0 ->  J/psi(1S) phi(1020)]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, phi], descriptors=decay_descriptor
    )

    phi_child = rd_isolation.find_in_decay(input=b2xtaul, id="phi(1020)")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=b2xtaul, id="mu-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "phi", "tau"],
        candidates=[b2xtaul, phi_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, phi, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2PhiTauMu_SS_ExclusiveLine(
    name="Hlt2RD_BdToPhiTauMu_PhiToKK_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (phi->K+ K-) (tau+->pi+pi+pi-) mu+ + CC

          and for Bd -> (phi->K+ K-) (tau-->pi-pi-pi+) mu- + CC
    """
    phi = builder.filter_rd_detached_phis()
    dilepton = builder.make_dilepton_from_tauls(child_id="mu-")
    decay_descriptor = "[B0 ->  J/psi(1S) phi(1020)]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, phi], descriptors=decay_descriptor
    )

    phi_child = rd_isolation.find_in_decay(input=b2xtaul, id="phi(1020)")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=b2xtaul, id="mu-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "phi", "tau"],
        candidates=[b2xtaul, phi_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, phi, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2PhiTauE_OS_ExclusiveLine(
    name="Hlt2RD_BdToPhiTauE_PhiToKK_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (phi->K+ K-) (tau+->pi+pi+pi-) e- + CC

          and for Bd -> (phi->K+ K-) (tau-->pi-pi-pi+) e+ + CC
    """
    phi = builder.filter_rd_detached_phis()
    dilepton = builder.make_dilepton_from_tauls(child_id="e+")
    decay_descriptor = "[B0 ->  J/psi(1S) phi(1020)]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, phi], descriptors=decay_descriptor
    )

    phi_child = rd_isolation.find_in_decay(input=b2xtaul, id="phi(1020)")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=b2xtaul, id="e-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "phi", "tau"],
        candidates=[b2xtaul, phi_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, phi, b2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2PhiTauE_SS_ExclusiveLine(
    name="Hlt2RD_BdToPhiTauE_PhiToKK_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (phi->K+ K-) (tau+->pi+pi+pi-) e+ + CC

          and for Bd -> (phi->K+ K-) (tau-->pi-pi-pi+) e- + CC
    """
    phi = builder.filter_rd_detached_phis()
    dilepton = builder.make_dilepton_from_tauls(child_id="e-")
    decay_descriptor = "[B0 ->  J/psi(1S) phi(1020)]cc"

    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, phi], descriptors=decay_descriptor
    )

    phi_child = rd_isolation.find_in_decay(input=b2xtaul, id="phi(1020)")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=b2xtaul, id="e-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "phi", "tau"],
        candidates=[b2xtaul, phi_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, phi, b2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


## B0 -> rho tau l
@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2RhoTauTau_OS_ExclusiveLine(
    name="Hlt2RD_BdToRhoTauTau_RhoToPiPi_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (rho->pi+ pi-) (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    """
    rho = make_rho0s_with_tighter_mass()
    dilepton = builder.make_dilepton_from_tauls(child_id="tau+")
    decay_descriptor = "[B0 ->  J/psi(1S) rho(770)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, rho], descriptors=decay_descriptor
    )

    rho_child = rd_isolation.find_in_decay(input=b2xtaul, id="rho(770)0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "rho", "tau"],
        candidates=[b2xtaul, rho_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, rho, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2RhoTauTau_SS_ExclusiveLine(
    name="Hlt2RD_BdToRhoTauTau_RhoToPiPi_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (rho->pi+ pi-) (tau+->pi+pi+pi-) (tau+->pi+pi+pi-) + CC

          and for Bd -> (rho->pi+ pi-) (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    rho = make_rho0s_with_tighter_mass()
    dilepton = builder.make_dilepton_from_tauls(child_id="tau-")
    decay_descriptor = "[B0 ->  J/psi(1S) rho(770)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, rho], descriptors=decay_descriptor
    )

    rho_child = rd_isolation.find_in_decay(input=b2xtaul, id="rho(770)0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "rho", "tau"],
        candidates=[b2xtaul, rho_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, rho, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2RhoTauMu_OS_ExclusiveLine(
    name="Hlt2RD_BdToRhoTauMu_RhoToPiPi_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (rho->pi+ pi-) (tau+->pi+pi+pi-) mu- + CC

          and for Bd -> (rho->pi+ pi-) (tau-->pi-pi-pi+) mu+ + CC
    """
    rho = make_rho0s_with_tighter_mass()
    dilepton = builder.make_dilepton_from_tauls(child_id="mu+")
    decay_descriptor = "[B0 ->  J/psi(1S) rho(770)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, rho], descriptors=decay_descriptor
    )

    rho_child = rd_isolation.find_in_decay(input=b2xtaul, id="rho(770)0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=b2xtaul, id="mu-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "rho", "tau"],
        candidates=[b2xtaul, rho_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, rho, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2RhoTauMu_SS_ExclusiveLine(
    name="Hlt2RD_BdToRhoTauMu_RhoToPiPi_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (rho->pi+ pi-) (tau+->pi+pi+pi-) mu+ + CC

          and for Bd -> (rho->pi+ pi-) (tau-->pi-pi-pi+) mu- + CC
    """
    rho = make_rho0s_with_tighter_mass()
    dilepton = builder.make_dilepton_from_tauls(child_id="mu-")
    decay_descriptor = "[B0 ->  J/psi(1S) rho(770)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, rho], descriptors=decay_descriptor
    )

    rho_child = rd_isolation.find_in_decay(input=b2xtaul, id="rho(770)0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=b2xtaul, id="mu-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "rho", "tau"],
        candidates=[b2xtaul, rho_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, rho, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2RhoTauE_OS_ExclusiveLine(
    name="Hlt2RD_BdToRhoTauE_RhoToPiPi_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (rho->pi+ pi-) (tau+->pi+pi+pi-) e- + CC

          and for Bd -> (rho->pi+ pi-) (tau-->pi-pi-pi+) e+ + CC
    """
    rho = make_rho0s_with_tighter_mass()
    dilepton = builder.make_dilepton_from_tauls(child_id="e+")
    decay_descriptor = "[B0 ->  J/psi(1S) rho(770)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, rho], descriptors=decay_descriptor
    )

    rho_child = rd_isolation.find_in_decay(input=b2xtaul, id="rho(770)0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=b2xtaul, id="e-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "rho", "tau"],
        candidates=[b2xtaul, rho_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, rho, b2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2RhoTauE_SS_ExclusiveLine(
    name="Hlt2RD_BdToRhoTauE_RhoToPiPi_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (rho->pi+ pi-) (tau+->pi+pi+pi-) e+ + CC

          and for Bd -> (rho->pi+ pi-) (tau-->pi-pi-pi+) e- + CC
    """
    rho = make_rho0s_with_tighter_mass()
    dilepton = builder.make_dilepton_from_tauls(child_id="e-")
    decay_descriptor = "[B0 ->  J/psi(1S) rho(770)0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, rho], descriptors=decay_descriptor
    )

    rho_child = rd_isolation.find_in_decay(input=b2xtaul, id="rho(770)0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=b2xtaul, id="e-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "rho", "tau"],
        candidates=[b2xtaul, rho_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, rho, b2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


## B0 -> eta' tau l
@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2EtapTauTau_OS_ExclusiveLine(
    name="Hlt2RD_BdToEtapTauTau_EtapToPiPiGamma_TauTo3Pi_OS",
    prescale=1,
    persistreco=False,
):
    """
    HLT2 line for Bd -> (eta'->pi+ pi- gamma) (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    """
    etap = builder.filter_rd_detached_etaprime()
    dilepton = builder.make_dilepton_from_tauls(child_id="tau+")
    decay_descriptor = "[B0 ->  J/psi(1S) eta_prime]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, etap], descriptors=decay_descriptor
    )

    etap_child = rd_isolation.find_in_decay(input=b2xtaul, id="eta_prime")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "etap", "tau"],
        candidates=[b2xtaul, etap_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, etap, b2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2EtapTauTau_SS_ExclusiveLine(
    name="Hlt2RD_BdToEtapTauTau_EtapToPiPiGamma_TauTo3Pi_SS",
    prescale=1,
    persistreco=False,
):
    """
    HLT2 line for Bd -> (eta'->pi+ pi- gamma) (tau+->pi+pi+pi-) (tau+->pi+pi+pi-) + CC

          and for Bd -> (eta'->pi+ pi- gamma) (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    etap = builder.filter_rd_detached_etaprime()
    dilepton = builder.make_dilepton_from_tauls(child_id="tau-")
    decay_descriptor = "[B0 -> J/psi(1S) eta_prime]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, etap], descriptors=decay_descriptor
    )

    etap_child = rd_isolation.find_in_decay(input=b2xtaul, id="eta_prime")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "etap", "tau"],
        candidates=[b2xtaul, etap_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, etap, b2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2EtapTauMu_OS_ExclusiveLine(
    name="Hlt2RD_BdToEtapTauMu_EtapToPiPiGamma_TauTo3Pi_OS",
    prescale=1,
    persistreco=False,
):
    """
    HLT2 line for Bd -> (eta'->pi+ pi- gamma) (tau+->pi+pi+pi-) mu- + CC

          and for Bd -> (eta'->pi+ pi- gamma) (tau-->pi-pi-pi+) mu+ + CC
    """
    etap = builder.filter_rd_detached_etaprime()
    dilepton = builder.make_dilepton_from_tauls(child_id="mu+")
    decay_descriptor = "[B0 ->  J/psi(1S) eta_prime]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, etap], descriptors=decay_descriptor
    )

    etap_child = rd_isolation.find_in_decay(input=b2xtaul, id="eta_prime")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=b2xtaul, id="mu-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "etap", "tau"],
        candidates=[b2xtaul, etap_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, etap, b2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2EtapTauMu_SS_ExclusiveLine(
    name="Hlt2RD_BdToEtapTauMu_EtapToPiPiGamma_TauTo3Pi_SS",
    prescale=1,
    persistreco=False,
):
    """
    HLT2 line for Bd -> (eta'->pi+ pi- gamma) (tau+->pi+pi+pi-) mu+ + CC

          and for Bd -> (eta'->pi+ pi- gamma) (tau-->pi-pi-pi+) mu- + CC
    """
    etap = builder.filter_rd_detached_etaprime()
    dilepton = builder.make_dilepton_from_tauls(child_id="mu-")
    decay_descriptor = "[B0 -> J/psi(1S) eta_prime]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, etap], descriptors=decay_descriptor
    )

    etap_child = rd_isolation.find_in_decay(input=b2xtaul, id="eta_prime")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=b2xtaul, id="mu-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "etap", "tau"],
        candidates=[b2xtaul, etap_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, etap, b2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2EtapTauE_OS_ExclusiveLine(
    name="Hlt2RD_BdToEtapTauE_EtapToPiPiGamma_TauTo3Pi_OS",
    prescale=1,
    persistreco=False,
):
    """
    HLT2 line for Bd -> (eta'->pi+ pi- gamma) (tau+->pi+pi+pi-) e- + CC

          and for Bd -> (eta'->pi+ pi- gamma) (tau-->pi-pi-pi+) e+ + CC
    """
    etap = builder.filter_rd_detached_etaprime()
    dilepton = builder.make_dilepton_from_tauls(child_id="e+")
    decay_descriptor = "[B0 ->  J/psi(1S) eta_prime]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, etap], descriptors=decay_descriptor
    )

    etap_child = rd_isolation.find_in_decay(input=b2xtaul, id="eta_prime")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=b2xtaul, id="e-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "etap", "tau"],
        candidates=[b2xtaul, etap_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, etap, b2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2EtapTauE_SS_ExclusiveLine(
    name="Hlt2RD_BdToEtapTauE_EtapToPiPiGamma_TauTo3Pi_SS",
    prescale=1,
    persistreco=False,
):
    """
    HLT2 line for Bd -> (eta'->pi+ pi- gamma) (tau+->pi+pi+pi-) e+ + CC

          and for Bd -> (eta'->pi+ pi- gamma) (tau-->pi-pi-pi+) e- + CC
    """
    etap = builder.filter_rd_detached_etaprime()
    dilepton = builder.make_dilepton_from_tauls(child_id="e-")
    decay_descriptor = "[B0 ->  J/psi(1S) eta_prime]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, etap], descriptors=decay_descriptor
    )

    etap_child = rd_isolation.find_in_decay(input=b2xtaul, id="eta_prime")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=b2xtaul, id="e-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "etap", "tau"],
        candidates=[b2xtaul, etap_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, etap, b2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


## B0 -> Ks0 (LL) tau l
@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2KsTauTau_LL_OS_ExclusiveLine(
    name="Hlt2RD_BdToKsTauTau_KsLLToPiPi_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (KS0->pi+ pi-) (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    """
    ks = builder.filter_rd_ks0s()
    dilepton = builder.make_dilepton_from_tauls(child_id="tau+")
    decay_descriptor = "[B0 ->  J/psi(1S) KS0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, ks], descriptors=decay_descriptor
    )

    ks_child = rd_isolation.find_in_decay(input=b2xtaul, id="KS0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "Kshort", "tau"],
        candidates=[b2xtaul, ks_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, ks, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2KsTauTau_LL_SS_ExclusiveLine(
    name="Hlt2RD_BdToKsTauTau_KsLLToPiPi_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (KS0->pi+ pi-) (tau+->pi+pi+pi-) (tau+->pi+pi+pi-) + CC

          and for Bd -> (KS0->pi+ pi-) (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    ks = builder.filter_rd_ks0s()
    dilepton = builder.make_dilepton_from_tauls(child_id="tau-")
    decay_descriptor = "[B0 ->  J/psi(1S) KS0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, ks], descriptors=decay_descriptor
    )

    ks_child = rd_isolation.find_in_decay(input=b2xtaul, id="KS0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "Kshort", "tau"],
        candidates=[b2xtaul, ks_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, ks, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2KsTauMu_LL_OS_ExclusiveLine(
    name="Hlt2RD_BdToKsTauMu_KsLLToPiPi_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (KS0->pi+ pi-) (tau+->pi+pi+pi-) mu- + CC

          and for Bd -> (KS0->pi+ pi-) (tau-->pi-pi-pi+) mu+ + CC
    """
    ks = builder.filter_rd_ks0s()
    dilepton = builder.make_dilepton_from_tauls(child_id="mu+")
    decay_descriptor = "[B0 ->  J/psi(1S) KS0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, ks], descriptors=decay_descriptor
    )

    ks_child = rd_isolation.find_in_decay(input=b2xtaul, id="KS0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=b2xtaul, id="mu-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "Kshort", "tau"],
        candidates=[b2xtaul, ks_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, ks, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2KsTauMu_LL_SS_ExclusiveLine(
    name="Hlt2RD_BdToKsTauMu_KsLLToPiPi_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (KS0->pi+ pi-) (tau+->pi+pi+pi-) mu+ + CC

          and for Bd -> (KS0->pi+ pi-) (tau-->pi-pi-pi+) mu- + CC
    """
    ks = builder.filter_rd_ks0s()
    dilepton = builder.make_dilepton_from_tauls(child_id="mu-")
    decay_descriptor = "[B0 ->  J/psi(1S) KS0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, ks], descriptors=decay_descriptor
    )

    ks_child = rd_isolation.find_in_decay(input=b2xtaul, id="KS0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=b2xtaul, id="mu-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "Kshort", "tau"],
        candidates=[b2xtaul, ks_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, ks, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2KsTauE_LL_OS_ExclusiveLine(
    name="Hlt2RD_BdToKsTauE_KsLLToPiPi_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (KS0->pi+ pi-) (tau+->pi+pi+pi-) e- + CC

          and for Bd -> (KS0->pi+ pi-) (tau-->pi-pi-pi+) e+ + CC
    """
    ks = builder.filter_rd_ks0s()
    dilepton = builder.make_dilepton_from_tauls(child_id="e+")
    decay_descriptor = "[B0 ->  J/psi(1S) KS0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, ks], descriptors=decay_descriptor
    )

    ks_child = rd_isolation.find_in_decay(input=b2xtaul, id="KS0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=b2xtaul, id="e-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "Kshort", "tau"],
        candidates=[b2xtaul, ks_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, ks, b2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2KsTauE_LL_SS_ExclusiveLine(
    name="Hlt2RD_BdToKsTauE_KsLLToPiPi_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (KS0->pi+ pi-) (tau+->pi+pi+pi-) e+ + CC

          and for Bd -> (KS0->pi+ pi-) (tau-->pi-pi-pi+) e- + CC
    """
    ks = builder.filter_rd_ks0s()
    dilepton = builder.make_dilepton_from_tauls(child_id="e-")
    decay_descriptor = "[B0 ->  J/psi(1S) KS0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, ks], descriptors=decay_descriptor
    )

    ks_child = rd_isolation.find_in_decay(input=b2xtaul, id="KS0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=b2xtaul, id="e-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "Kshort", "tau"],
        candidates=[b2xtaul, ks_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, ks, b2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


## B0 -> Ks0 (DD) tau l
@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2KsTauTau_DD_OS_ExclusiveLine(
    name="Hlt2RD_BdToKsTauTau_KsDDToPiPi_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (KS0->pi+ pi-) (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    """
    ks = builder.filter_rd_ks0s(use_down_track=True)
    dilepton = builder.make_dilepton_from_tauls(child_id="tau+")
    decay_descriptor = "[B0 ->  J/psi(1S) KS0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, ks], descriptors=decay_descriptor
    )

    ks_child = rd_isolation.find_in_decay(input=b2xtaul, id="KS0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "Kshort", "tau"],
        candidates=[b2xtaul, ks_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, ks, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2KsTauTau_DD_SS_ExclusiveLine(
    name="Hlt2RD_BdToKsTauTau_KsDDToPiPi_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (KS0->pi+ pi-) (tau+->pi+pi+pi-) (tau+->pi+pi+pi-) + CC

          and for Bd -> (KS0->pi+ pi-) (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    ks = builder.filter_rd_ks0s(use_down_track=True)
    dilepton = builder.make_dilepton_from_tauls(child_id="tau-")
    decay_descriptor = "[B0 ->  J/psi(1S) KS0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, ks], descriptors=decay_descriptor
    )

    ks_child = rd_isolation.find_in_decay(input=b2xtaul, id="KS0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "Kshort", "tau"],
        candidates=[b2xtaul, ks_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, ks, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2KsTauMu_DD_OS_ExclusiveLine(
    name="Hlt2RD_BdToKsTauMu_KsDDToPiPi_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (KS0->pi+ pi-) (tau+->pi+pi+pi-) mu- + CC

          and for Bd -> (KS0->pi+ pi-) (tau-->pi-pi-pi+) mu+ + CC
    """
    ks = builder.filter_rd_ks0s(use_down_track=True)
    dilepton = builder.make_dilepton_from_tauls(child_id="mu+")
    decay_descriptor = "[B0 ->  J/psi(1S) KS0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, ks], descriptors=decay_descriptor
    )

    ks_child = rd_isolation.find_in_decay(input=b2xtaul, id="KS0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=b2xtaul, id="mu-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "Kshort", "tau"],
        candidates=[b2xtaul, ks_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, ks, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2KsTauMu_DD_SS_ExclusiveLine(
    name="Hlt2RD_BdToKsTauMu_KsDDToPiPi_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (KS0->pi+ pi-) (tau+->pi+pi+pi-) mu+ + CC

          and for Bd -> (KS0->pi+ pi-) (tau-->pi-pi-pi+) mu- + CC
    """
    ks = builder.filter_rd_ks0s(use_down_track=True)
    dilepton = builder.make_dilepton_from_tauls(child_id="mu-")
    decay_descriptor = "[B0 ->  J/psi(1S) KS0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, ks], descriptors=decay_descriptor
    )

    ks_child = rd_isolation.find_in_decay(input=b2xtaul, id="KS0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=b2xtaul, id="mu-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "Kshort", "tau"],
        candidates=[b2xtaul, ks_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, ks, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2KsTauE_DD_OS_ExclusiveLine(
    name="Hlt2RD_BdToKsTauE_KsDDToPiPi_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (KS0->pi+ pi-) (tau+->pi+pi+pi-) e- + CC

          and for Bd -> (KS0->pi+ pi-) (tau-->pi-pi-pi+) e+ + CC
    """
    ks = builder.filter_rd_ks0s(use_down_track=True)
    dilepton = builder.make_dilepton_from_tauls(child_id="e+")
    decay_descriptor = "[B0 ->  J/psi(1S) KS0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, ks], descriptors=decay_descriptor
    )

    ks_child = rd_isolation.find_in_decay(input=b2xtaul, id="KS0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=b2xtaul, id="e-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "Kshort", "tau"],
        candidates=[b2xtaul, ks_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, ks, b2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bd2KsTauE_DD_SS_ExclusiveLine(
    name="Hlt2RD_BdToKsTauE_KsDDToPiPi_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for Bd -> (KS0->pi+ pi-) (tau+->pi+pi+pi-) e+ + CC

          and for Bd -> (KS0->pi+ pi-) (tau-->pi-pi-pi+) e- + CC
    """
    ks = builder.filter_rd_ks0s(use_down_track=True)
    dilepton = builder.make_dilepton_from_tauls(child_id="e-")
    decay_descriptor = "[B0 ->  J/psi(1S) KS0]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, ks], descriptors=decay_descriptor
    )

    ks_child = rd_isolation.find_in_decay(input=b2xtaul, id="KS0")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=b2xtaul, id="e-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "Kshort", "tau"],
        candidates=[b2xtaul, ks_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, ks, b2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


## Bu -> K+ tau l
@register_line_builder(all_lines)
@configurable
def Hlt2_Bu2KplusTauTau_OS_ExclusiveLine(
    name="Hlt2RD_BuToKplTauTau_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for B+ -> K+ (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    """
    kaon = common_builder.make_rd_has_rich_detached_kaons(
        pt_min=1 * GeV, p_min=4 * GeV, mipchi2dvprimary_min=9, pid=(F.PID_K > 8)
    )
    dilepton = builder.make_dilepton_from_tauls(child_id="tau+")
    decay_descriptor = "[B+ -> J/psi(1S) K+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kaon], descriptors=decay_descriptor
    )

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "tau"],
        candidates=[b2xtaul, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bu2KplusTauTau_SS_ExclusiveLine(
    name="Hlt2RD_BuToKplTauTau_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for B+ -> K+ (tau+->pi+pi+pi-) (tau+->pi+pi+pi-) + CC

          and for B+ -> K+ (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    kaon = common_builder.make_rd_has_rich_detached_kaons(
        pt_min=1 * GeV, p_min=4 * GeV, mipchi2dvprimary_min=9, pid=(F.PID_K > 8)
    )
    dilepton = builder.make_dilepton_from_tauls(child_id="tau-")
    decay_descriptor = "[B+ -> J/psi(1S) K+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kaon], descriptors=decay_descriptor
    )

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "tau"],
        candidates=[b2xtaul, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bu2KplusTauMu_OS_ExclusiveLine(
    name="Hlt2RD_BuToKplTauMu_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for B+ -> K+ (tau+->pi+pi+pi-) mu- + CC

          and for B+ -> K+ (tau-->pi-pi-pi+) mu+ + CC
    """
    kaon = common_builder.make_rd_has_rich_detached_kaons(
        pt_min=1 * GeV, p_min=4 * GeV, mipchi2dvprimary_min=9, pid=(F.PID_K > 8)
    )
    dilepton = builder.make_dilepton_from_tauls(child_id="mu+")
    decay_descriptor = "[B+ -> J/psi(1S) K+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kaon], descriptors=decay_descriptor
    )

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=b2xtaul, id="mu-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "tau"],
        candidates=[b2xtaul, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bu2KplusTauMu_SS_ExclusiveLine(
    name="Hlt2RD_BuToKplTauMu_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for B+ -> K+ (tau+->pi+pi+pi-) mu+ + CC

          and for B+ -> K+ (tau-->pi-pi-pi+) mu- + CC
    """
    kaon = common_builder.make_rd_has_rich_detached_kaons(
        pt_min=1 * GeV, p_min=4 * GeV, mipchi2dvprimary_min=9, pid=(F.PID_K > 8)
    )
    dilepton = builder.make_dilepton_from_tauls(child_id="mu-")
    decay_descriptor = "[B+ -> J/psi(1S) K+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kaon], descriptors=decay_descriptor
    )

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=b2xtaul, id="mu-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "tau"],
        candidates=[b2xtaul, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bu2KplusTauE_OS_ExclusiveLine(
    name="Hlt2RD_BuToKplTauE_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for B+ -> K+ (tau+->pi+pi+pi-) e- + CC

          and for B+ -> K+ (tau-->pi-pi-pi+) e+ + CC
    """
    kaon = common_builder.make_rd_has_rich_detached_kaons(
        pt_min=1 * GeV, p_min=4 * GeV, mipchi2dvprimary_min=9, pid=(F.PID_K > 8)
    )
    dilepton = builder.make_dilepton_from_tauls(child_id="e+")
    decay_descriptor = "[B+ -> J/psi(1S) K+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kaon], descriptors=decay_descriptor
    )

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=b2xtaul, id="e-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "tau"],
        candidates=[b2xtaul, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, b2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bu2KplusTauE_SS_ExclusiveLine(
    name="Hlt2RD_BuToKplTauE_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for B+ -> K+ (tau+->pi+pi+pi-) e+ + CC

          and for B+ -> K+ (tau-->pi-pi-pi+) e- + CC
    """
    kaon = common_builder.make_rd_has_rich_detached_kaons(
        pt_min=1 * GeV, p_min=4 * GeV, mipchi2dvprimary_min=9, pid=(F.PID_K > 8)
    )
    dilepton = builder.make_dilepton_from_tauls(child_id="e-")
    decay_descriptor = "[B+ -> J/psi(1S) K+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, kaon], descriptors=decay_descriptor
    )

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=b2xtaul, id="e-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "tau"],
        candidates=[b2xtaul, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, b2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


## Bu -> K1(1270)+ tau l
@register_line_builder(all_lines)
@configurable
def Hlt2_Bu2K1plusTauTau_OS_ExclusiveLine(
    name="Hlt2RD_BuToK1plTauTau_K1plTo3Pi_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for B+ -> (K1+->pi+pi+pi-) (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    """
    k1 = builder.filter_rd_detached_k1()
    dilepton = builder.make_dilepton_from_tauls(child_id="tau+")
    decay_descriptor = "[B+ ->  J/psi(1S) K_1(1270)+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, k1], descriptors=decay_descriptor
    )

    k1_child = rd_isolation.find_in_decay(input=b2xtaul, id="K_1(1270)+")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "K1", "tau"],
        candidates=[b2xtaul, k1_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, k1, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bu2K1plusTauTau_SS_ExclusiveLine(
    name="Hlt2RD_BuToK1plTauTau_K1plTo3Pi_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for B+ -> (K1+->pi+pi+pi-) (tau+->pi+pi+pi-) (tau+->pi+pi+pi-) + CC

          and for B+ -> (K1+->pi+pi+pi-) (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    k1 = builder.filter_rd_detached_k1()
    dilepton = builder.make_dilepton_from_tauls(child_id="tau-")
    decay_descriptor = "[B+ ->  J/psi(1S) K_1(1270)+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, k1], descriptors=decay_descriptor
    )

    k1_child = rd_isolation.find_in_decay(input=b2xtaul, id="K_1(1270)+")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "K1", "tau"],
        candidates=[b2xtaul, k1_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, k1, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bu2K1plusTauMu_OS_ExclusiveLine(
    name="Hlt2RD_BuToK1plTauMu_K1plTo3Pi_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for B+ -> (K1+->pi+pi+pi-) (tau+->pi+pi+pi-) mu- + CC

          and for B+ -> (K1+->pi+pi+pi-) (tau-->pi-pi-pi+) mu+ + CC
    """
    k1 = builder.filter_rd_detached_k1()
    dilepton = builder.make_dilepton_from_tauls(child_id="mu+")
    decay_descriptor = "[B+ ->  J/psi(1S) K_1(1270)+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, k1], descriptors=decay_descriptor
    )

    k1_child = rd_isolation.find_in_decay(input=b2xtaul, id="K_1(1270)+")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=b2xtaul, id="mu-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "K1", "tau"],
        candidates=[b2xtaul, k1_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, k1, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bu2K1plusTauMu_SS_ExclusiveLine(
    name="Hlt2RD_BuToK1plTauMu_K1plTo3Pi_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for B+ -> (K1+->pi+pi+pi-) (tau+->pi+pi+pi-) mu+ + CC

          and for B+ -> (K1+->pi+pi+pi-) (tau-->pi-pi-pi+) mu- + CC
    """
    k1 = builder.filter_rd_detached_k1()
    dilepton = builder.make_dilepton_from_tauls(child_id="mu-")
    decay_descriptor = "[B+ ->  J/psi(1S) K_1(1270)+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, k1], descriptors=decay_descriptor
    )

    k1_child = rd_isolation.find_in_decay(input=b2xtaul, id="K_1(1270)+")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=b2xtaul, id="mu-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "K1", "tau"],
        candidates=[b2xtaul, k1_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, k1, b2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bu2K1plusTauE_OS_ExclusiveLine(
    name="Hlt2RD_BuToK1plTauE_K1plTo3Pi_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for B+ -> (K1+->pi+pi+pi-) (tau+->pi+pi+pi-) e- + CC

          and for B+ -> (K1+->pi+pi+pi-) (tau-->pi-pi-pi+) e+ + CC
    """
    k1 = builder.filter_rd_detached_k1()
    dilepton = builder.make_dilepton_from_tauls(child_id="e+")
    decay_descriptor = "[B+ ->  J/psi(1S) K_1(1270)+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, k1], descriptors=decay_descriptor
    )

    k1_child = rd_isolation.find_in_decay(input=b2xtaul, id="K_1(1270)+")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=b2xtaul, id="e-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "K1", "tau"],
        candidates=[b2xtaul, k1_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, k1, b2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Bu2K1plusTauE_SS_ExclusiveLine(
    name="Hlt2RD_BuToK1plTauE_K1plTo3Pi_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for B+ -> (K1+->pi+pi+pi-) (tau+->pi+pi+pi-) e+ + CC

          and for B+ -> (K1+->pi+pi+pi-) (tau-->pi-pi-pi+) e- + CC
    """
    k1 = builder.filter_rd_detached_k1()
    dilepton = builder.make_dilepton_from_tauls(child_id="e-")
    decay_descriptor = "[B+ ->  J/psi(1S) K_1(1270)+]cc"
    b2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, k1], descriptors=decay_descriptor
    )

    k1_child = rd_isolation.find_in_decay(input=b2xtaul, id="K_1(1270)+")

    tau_child = rd_isolation.find_in_decay(input=b2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=b2xtaul, id="e-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "K1", "tau"],
        candidates=[b2xtaul, k1_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, k1, b2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


## Lb -> K p tau l
@register_line_builder(all_lines)
@configurable
def Hlt2_Lambdab2PKTauTau_OS_ExclusiveLine(
    name="Hlt2RD_LbToPKTauTau_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for Lb -> p K- (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    """
    lst = builder.make_dihadron_from_pK()
    dilepton = builder.make_dilepton_from_tauls(child_id="tau+")
    decay_descriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lst], descriptors=decay_descriptor
    )

    tau_child = rd_isolation.find_in_decay(input=lb2xtaul, id="tau+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "tau"],
        candidates=[lb2xtaul, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, lst, lb2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Lambdab2PKTauTau_SS_ExclusiveLine(
    name="Hlt2RD_LbToPKTauTau_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for Lb -> p K- (tau+->pi+pi+pi-) (tau+->pi+pi+pi-) + CC

          and for Lb -> p K- (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    lst = builder.make_dihadron_from_pK()
    dilepton = builder.make_dilepton_from_tauls(child_id="tau-")
    decay_descriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lst], descriptors=decay_descriptor
    )

    tau_child = rd_isolation.find_in_decay(input=lb2xtaul, id="tau+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "tau"],
        candidates=[lb2xtaul, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, lst, lb2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Lambdab2PKTauMu_OS_ExclusiveLine(
    name="Hlt2RD_LbToPKTauMu_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for Lb -> p K- (tau+->pi+pi+pi-) mu- + CC

          and for Lb -> p K- (tau-->pi-pi-pi+) mu+ + CC
    """
    lst = builder.make_dihadron_from_pK()
    dilepton = builder.make_dilepton_from_tauls(child_id="mu+")
    decay_descriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lst], descriptors=decay_descriptor
    )

    tau_child = rd_isolation.find_in_decay(input=lb2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=lb2xtaul, id="mu-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "tau"],
        candidates=[lb2xtaul, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, lst, lb2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Lambdab2PKTauMu_SS_ExclusiveLine(
    name="Hlt2RD_LbToPKTauMu_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for Lb -> p K- (tau+->pi+pi+pi-) mu+ + CC

          and for Lb -> p K- (tau-->pi-pi-pi+) mu- + CC
    """
    lst = builder.make_dihadron_from_pK()
    dilepton = builder.make_dilepton_from_tauls(child_id="mu-")
    decay_descriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lst], descriptors=decay_descriptor
    )

    tau_child = rd_isolation.find_in_decay(input=lb2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=lb2xtaul, id="mu-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "tau"],
        candidates=[lb2xtaul, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, lst, lb2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Lambdab2PKTauE_OS_ExclusiveLine(
    name="Hlt2RD_LbToPKTauE_TauTo3Pi_OS", prescale=1, persistreco=False
):
    """
    HLT2 line for Lb -> p K- (tau+->pi+pi+pi-) e- + CC

          and for Lb -> p K- (tau-->pi-pi-pi+) e+ + CC
    """
    lst = builder.make_dihadron_from_pK()
    dilepton = builder.make_dilepton_from_tauls(child_id="e+")
    decay_descriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lst], descriptors=decay_descriptor
    )

    tau_child = rd_isolation.find_in_decay(input=lb2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=lb2xtaul, id="e-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "tau"],
        candidates=[lb2xtaul, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, lst, lb2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Lambdab2PKTauE_SS_ExclusiveLine(
    name="Hlt2RD_LbToPKTauE_TauTo3Pi_SS", prescale=1, persistreco=False
):
    """
    HLT2 line for Lb -> p K- (tau+->pi+pi+pi-) e+ + CC

          and for Lb -> p K- (tau-->pi-pi-pi+) e- + CC
    """
    lst = builder.make_dihadron_from_pK()
    dilepton = builder.make_dilepton_from_tauls(child_id="e-")
    decay_descriptor = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lst], descriptors=decay_descriptor
    )

    tau_child = rd_isolation.find_in_decay(input=lb2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=lb2xtaul, id="e-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B", "tau"],
        candidates=[lb2xtaul, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, lst, lb2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


## Lambda_b -> Lambda (LL) tau l
@register_line_builder(all_lines)
@configurable
def Hlt2_Lambdab2LambdaTauTau_LL_OS_ExclusiveLine(
    name="Hlt2RD_LbToLambdaTauTau_LambdaLLToPPi_TauTo3Pi_OS",
    prescale=1,
    persistreco=False,
):
    """
    HLT2 line for Lb -> (L0->p pi-) (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    """
    lambda0 = builder.filter_rd_lambdas()
    dilepton = builder.make_dilepton_from_tauls(child_id="tau+")
    decay_descriptor = "[Lambda_b0 ->  J/psi(1S) Lambda0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lambda0], descriptors=decay_descriptor
    )

    lambda0_child = rd_isolation.find_in_decay(input=lb2xtaul, id="Lambda0")

    tau_child = rd_isolation.find_in_decay(input=lb2xtaul, id="tau+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Lb", "L0", "tau"],
        candidates=[lb2xtaul, lambda0_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, lambda0, lb2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Lambdab2LambdaTauTau_LL_SS_ExclusiveLine(
    name="Hlt2RD_LbToLambdaTauTau_LambdaLLToPPi_TauTo3Pi_SS",
    prescale=1,
    persistreco=False,
):
    """
    HLT2 line for Lb -> (L0->p pi-) (tau+->pi+pi+pi-) (tau+->pi+pi+pi-) + CC

          and for Lb -> (L0->p pi-) (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    lambda0 = builder.filter_rd_lambdas()
    dilepton = builder.make_dilepton_from_tauls(child_id="tau-")
    decay_descriptor = "[Lambda_b0 ->  J/psi(1S) Lambda0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lambda0], descriptors=decay_descriptor
    )

    lambda0_child = rd_isolation.find_in_decay(input=lb2xtaul, id="Lambda0")

    tau_child = rd_isolation.find_in_decay(input=lb2xtaul, id="tau+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Lb", "L0", "tau"],
        candidates=[lb2xtaul, lambda0_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, lambda0, lb2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Lambdab2LambdaTauMu_LL_OS_ExclusiveLine(
    name="Hlt2RD_LbToLambdaTauMu_LambdaLLToPPi_TauTo3Pi_OS",
    prescale=1,
    persistreco=False,
):
    """
    HLT2 line for Lb -> (L0->p pi-) (tau+->pi+pi+pi-) mu- + CC

          and for Lb -> (L0->p pi-) (tau-->pi-pi-pi+) mu+ + CC
    """
    lambda0 = builder.filter_rd_lambdas()
    dilepton = builder.make_dilepton_from_tauls(child_id="mu+")
    decay_descriptor = "[Lambda_b0 ->  J/psi(1S) Lambda0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lambda0], descriptors=decay_descriptor
    )

    lambda0_child = rd_isolation.find_in_decay(input=lb2xtaul, id="Lambda0")

    tau_child = rd_isolation.find_in_decay(input=lb2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=lb2xtaul, id="mu-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Lb", "L0", "tau"],
        candidates=[lb2xtaul, lambda0_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, lambda0, lb2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Lambdab2LambdaTauMu_LL_SS_ExclusiveLine(
    name="Hlt2RD_LbToLambdaTauMu_LambdaLLToPPi_TauTo3Pi_SS",
    prescale=1,
    persistreco=False,
):
    """
    HLT2 line for Lb -> (L0->p pi-) (tau+->pi+pi+pi-) mu+ + CC

          and for Lb -> (L0->p pi-) (tau-->pi-pi-pi+) mu- + CC
    """
    lambda0 = builder.filter_rd_lambdas()
    dilepton = builder.make_dilepton_from_tauls(child_id="mu-")
    decay_descriptor = "[Lambda_b0 ->  J/psi(1S) Lambda0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lambda0], descriptors=decay_descriptor
    )

    lambda0_child = rd_isolation.find_in_decay(input=lb2xtaul, id="Lambda0")

    tau_child = rd_isolation.find_in_decay(input=lb2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=lb2xtaul, id="mu-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Lb", "L0", "tau"],
        candidates=[lb2xtaul, lambda0_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, lambda0, lb2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Lambdab2LambdaTauE_LL_OS_ExclusiveLine(
    name="Hlt2RD_LbToLambdaTauE_LambdaLLToPPi_TauTo3Pi_OS",
    prescale=1,
    persistreco=False,
):
    """
    HLT2 line for Lb -> (L0->p pi-) (tau+->pi+pi+pi-) e- + CC

          and for Lb -> (L0->p pi-) (tau-->pi-pi-pi+) e+ + CC
    """
    lambda0 = builder.filter_rd_lambdas()
    dilepton = builder.make_dilepton_from_tauls(child_id="e+")
    decay_descriptor = "[Lambda_b0 -> J/psi(1S) Lambda0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lambda0], descriptors=decay_descriptor
    )

    lambda0_child = rd_isolation.find_in_decay(input=lb2xtaul, id="Lambda0")

    tau_child = rd_isolation.find_in_decay(input=lb2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=lb2xtaul, id="e-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Lb", "L0", "tau"],
        candidates=[lb2xtaul, lambda0_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, lambda0, lb2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Lambdab2LambdaTauE_LL_SS_ExclusiveLine(
    name="Hlt2RD_LbToLambdaTauE_LambdaLLToPPi_TauTo3Pi_SS",
    prescale=1,
    persistreco=False,
):
    """
    HLT2 line for Lb -> (L0->p pi-) (tau+->pi+pi+pi-) e+ + CC

          and for Lb -> (L0->p pi-) (tau-->pi-pi-pi+) e- + CC
    """
    lambda0 = builder.filter_rd_lambdas()
    dilepton = builder.make_dilepton_from_tauls(child_id="e-")
    decay_descriptor = "[Lambda_b0 -> J/psi(1S) Lambda0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lambda0], descriptors=decay_descriptor
    )

    lambda0_child = rd_isolation.find_in_decay(input=lb2xtaul, id="Lambda0")

    tau_child = rd_isolation.find_in_decay(input=lb2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=lb2xtaul, id="e-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Lb", "L0", "tau"],
        candidates=[lb2xtaul, lambda0_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, lambda0, lb2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


## Lambda_b -> Lambda (DD) tau l
@register_line_builder(all_lines)
@configurable
def Hlt2_Lambdab2LambdaTauTau_DD_OS_ExclusiveLine(
    name="Hlt2RD_LbToLambdaTauTau_LambdaDDToPPi_TauTo3Pi_OS",
    prescale=1,
    persistreco=False,
):
    """
    HLT2 line for Lb -> (L0->p pi-) (tau+->pi+pi+pi-) (tau-->pi-pi-pi+) + CC
    """
    lambda0 = builder.filter_rd_lambdas(use_down_tracks=True)
    dilepton = builder.make_dilepton_from_tauls(child_id="tau+")
    decay_descriptor = "[Lambda_b0 ->  J/psi(1S) Lambda0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lambda0], descriptors=decay_descriptor
    )

    lambda0_child = rd_isolation.find_in_decay(input=lb2xtaul, id="Lambda0")

    tau_child = rd_isolation.find_in_decay(input=lb2xtaul, id="tau+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Lb", "L0", "tau"],
        candidates=[lb2xtaul, lambda0_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, lambda0, lb2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Lambdab2LambdaTauTau_DD_SS_ExclusiveLine(
    name="Hlt2RD_LbToLambdaTauTau_LambdaDDToPPi_TauTo3Pi_SS",
    prescale=1,
    persistreco=False,
):
    """
    HLT2 line for Lb -> (L0->p pi-) (tau+->pi+pi+pi-) (tau+->pi+pi+pi-) + CC

          and for Lb -> (L0->p pi-) (tau-->pi-pi-pi+) (tau-->pi-pi-pi+) + CC
    """
    lambda0 = builder.filter_rd_lambdas(use_down_tracks=True)
    dilepton = builder.make_dilepton_from_tauls(child_id="tau-")
    decay_descriptor = "[Lambda_b0 ->  J/psi(1S) Lambda0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lambda0], descriptors=decay_descriptor
    )

    lambda0_child = rd_isolation.find_in_decay(input=lb2xtaul, id="Lambda0")

    tau_child = rd_isolation.find_in_decay(input=lb2xtaul, id="tau+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Lb", "L0", "tau"],
        candidates=[lb2xtaul, lambda0_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, lambda0, lb2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Lambdab2LambdaTauMu_DD_OS_ExclusiveLine(
    name="Hlt2RD_LbToLambdaTauMu_LambdaDDToPPi_TauTo3Pi_OS",
    prescale=1,
    persistreco=False,
):
    """
    HLT2 line for Lb -> (L0->p pi-) (tau+->pi+pi+pi-) mu- + CC

          and for Lb -> (L0->p pi-) (tau-->pi-pi-pi+) mu+ + CC
    """
    lambda0 = builder.filter_rd_lambdas(use_down_tracks=True)
    dilepton = builder.make_dilepton_from_tauls(child_id="mu+")
    decay_descriptor = "[Lambda_b0 ->  J/psi(1S) Lambda0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lambda0], descriptors=decay_descriptor
    )

    lambda0_child = rd_isolation.find_in_decay(input=lb2xtaul, id="Lambda0")

    tau_child = rd_isolation.find_in_decay(input=lb2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=lb2xtaul, id="mu-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Lb", "L0", "tau"],
        candidates=[lb2xtaul, lambda0_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, lambda0, lb2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Lambdab2LambdaTauMu_DD_SS_ExclusiveLine(
    name="Hlt2RD_LbToLambdaTauMu_LambdaDDToPPi_TauTo3Pi_SS",
    prescale=1,
    persistreco=False,
):
    """
    HLT2 line for Lb -> (L0->p pi-) (tau+->pi+pi+pi-) mu+ + CC

          and for Lb -> (L0->p pi-) (tau-->pi-pi-pi+) mu- + CC
    """
    lambda0 = builder.filter_rd_lambdas(use_down_tracks=True)
    dilepton = builder.make_dilepton_from_tauls(child_id="mu-")
    decay_descriptor = "[Lambda_b0 ->  J/psi(1S) Lambda0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lambda0], descriptors=decay_descriptor
    )

    lambda0_child = rd_isolation.find_in_decay(input=lb2xtaul, id="Lambda0")

    tau_child = rd_isolation.find_in_decay(input=lb2xtaul, id="tau+")

    mu_child = rd_isolation.find_in_decay(input=lb2xtaul, id="mu-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Lb", "L0", "tau"],
        candidates=[lb2xtaul, lambda0_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu"],
        candidates=[mu_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, lambda0, lb2xtaul],
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Lambdab2LambdaTauE_DD_OS_ExclusiveLine(
    name="Hlt2RD_LbToLambdaTauE_LambdaDDToPPi_TauTo3Pi_OS",
    prescale=1,
    persistreco=False,
):
    """
    HLT2 line for Lb -> (L0->p pi-) (tau+->pi+pi+pi-) e- + CC

          and for Lb -> (L0->p pi-) (tau-->pi-pi-pi+) e+ + CC
    """
    lambda0 = builder.filter_rd_lambdas(use_down_tracks=True)
    dilepton = builder.make_dilepton_from_tauls(child_id="e+")
    decay_descriptor = "[Lambda_b0 -> J/psi(1S) Lambda0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lambda0], descriptors=decay_descriptor
    )

    lambda0_child = rd_isolation.find_in_decay(input=lb2xtaul, id="Lambda0")

    tau_child = rd_isolation.find_in_decay(input=lb2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=lb2xtaul, id="e-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Lb", "L0", "tau"],
        candidates=[lb2xtaul, lambda0_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, lambda0, lb2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def Hlt2_Lambdab2LambdaTauE_DD_SS_ExclusiveLine(
    name="Hlt2RD_LbToLambdaTauE_LambdaDDToPPi_TauTo3Pi_SS",
    prescale=1,
    persistreco=False,
):
    """
    HLT2 line for Lb -> (L0->p pi-) (tau+->pi+pi+pi-) e+ + CC

          and for Lb -> (L0->p pi-) (tau-->pi-pi-pi+) e- + CC
    """
    lambda0 = builder.filter_rd_lambdas(use_down_tracks=True)
    dilepton = builder.make_dilepton_from_tauls(child_id="e-")
    decay_descriptor = "[Lambda_b0 -> J/psi(1S) Lambda0]cc"
    lb2xtaul = builder.make_beauty2xtaul(
        particles=[dilepton, lambda0], descriptors=decay_descriptor
    )

    lambda0_child = rd_isolation.find_in_decay(input=lb2xtaul, id="Lambda0")

    tau_child = rd_isolation.find_in_decay(input=lb2xtaul, id="tau+")

    e_child = rd_isolation.find_in_decay(input=lb2xtaul, id="e-")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Lb", "L0", "tau"],
        candidates=[lb2xtaul, lambda0_child, tau_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e"],
        candidates=[e_child],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.SHARE_TRACKS()),
    )

    return Hlt2Line(
        name=name,
        prescale=prescale,
        persistreco=persistreco,
        algs=rd_prefilter() + [dilepton, lambda0, lb2xtaul],
        extra_outputs=iso_parts,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )
