###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Registration of exclusive radiative  B -> X gamma Hlt2 lines:
- B_s0 -> ( phi(1020) -> K K ) gamma
- B0 -> (K*(892) -> K pi ) gamma

author: Izaac Sanderswood
date: 25.05.22


Exclusive RAD lines:
- B_s0 -> phi(1020) gamma (broad KK mass)
- B0 -> K*(892)0 gamma  (borad Kpi mass)
- B+ -> K_1(1270)+ gamma
- B+ -> K phi(1020) gamma
- B0 -> Ks pi pi gamma (LL)
- B0 -> Ks pi pi gamma (DD)
- B0 -> Ks K K gamma (LL)
- B0 -> Ks K K gamma (DD)
- B_s0 -> Ks K pi gamma (LL)
- B_s0 -> Ks K pi gamma (DD)
- Lambda_b0 -> Ks p pi gamma (LL)
- Lambda_b0 -> Ks p pi gamma (DD)
- Lambda_b0 -> p K  gamma
- B0 -> pi pi  gamma (broad pipi mass)
- B_c+ -> D*(2010)+ gamma
- B_c+ -> D*_s+ gamma

authors: Debashis Sahoo, Biplab Dey
date: 15.05.2024

"""

from math import cos

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, ps
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.algorithms_thor import ParticleFilter
from RecoConf.rdbuilder_thor import (
    make_rd_detached_down_pions,
    make_rd_detached_k1,
    make_rd_detached_kstar0s,
    make_rd_detached_phis,
    make_rd_detached_pions,
    make_rd_detached_rho0,
    make_rd_has_rich_detached_kaons,
    make_rd_has_rich_detached_pions,
    make_rd_ks0_dds,
    make_rd_ks0_lls,
    make_rd_photons,
)
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.rd.builders import rd_isolation
from Hlt2Conf.lines.rd.builders.b_to_xgamma_exclusive_builders import (
    make_b2xgamma_excl,
    make_b2xgamma_excl_bc,
    make_ds_for_rd_bc,
    make_dsst,
    make_dzeros_for_rd_bc,
    make_PV,
    make_rd_detached_pK,
)
from Hlt2Conf.lines.rd.builders.rd_isolation import parent_isolation_output
from Hlt2Conf.lines.rd.builders.rd_prefilters import (
    _RD_MONITORING_VARIABLES,
    rd_prefilter,
)

all_lines = {}


@register_line_builder(all_lines)
def bs_to_phigamma_line(name="Hlt2RD_BsToPhiGamma", persistreco=False, prescale=1.0):
    pvs = make_pvs()
    # remember to also look at the definition of make_rd_detached_phis as only some cuts are changed here
    phis = make_rd_detached_phis(
        name="rd_BsToPhiGamma_detached_phis",
        k_p_min=2.0 * GeV,
        k_pt_min=500.0 * MeV,
        k_ipchi2_min=11.0,
        phi_pt_min=1500.0 * MeV,
        vchi2pdof_max=15.0,
    )

    photons = make_rd_photons(
        et_min=2.0 * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2
    )

    b_s0 = make_b2xgamma_excl(
        intermediate=phis,
        photons=photons,
        pvs=pvs,
        descriptor="B_s0 -> phi(1020) gamma",
        dira_min=cos(0.1),
        name="rd_BsToPhiGamma_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [phis, b_s0],
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
        extra_outputs=parent_isolation_output("B_s0", b_s0),
    )


@register_line_builder(all_lines)
def bd_to_kstgamma_line(name="Hlt2RD_BdToKstGamma", persistreco=True, prescale=1.0):
    pvs = make_pvs()

    # remember to also look at the definition of make_rd_detached_kstar0s as only some cuts are changed here
    kst = make_rd_detached_kstar0s(
        name="rd_BdToKstGamma_detached_kstar0s_{hash}",
        pi_p_min=2.0 * GeV,
        pi_pt_min=500.0 * MeV,
        k_p_min=3.0 * GeV,
        k_pt_min=500.0 * MeV,
        k_ipchi2_min=11.0,
        pi_ipchi2_min=11.0,
        k_pid=((F.PID_K > 0.0) & ((F.PID_K - F.PID_P) > 2)),
        kstar0_pt_min=1500.0 * MeV,
        vchi2pdof_max=15.0,
        am_min=800.0 * MeV,
        am_max=1000.0 * MeV,
    )

    photons = make_rd_photons(
        et_min=2.0 * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2
    )

    b0 = make_b2xgamma_excl(
        intermediate=kst,
        photons=photons,
        pvs=pvs,
        descriptor="[B0 -> K*(892)0 gamma]cc",
        dira_min=cos(0.045),
        name="rd_BdToKstGamma_Combiner_{hash}",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kst, b0],
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
        extra_outputs=parent_isolation_output("B0", b0),
    )


@register_line_builder(all_lines)
def bs_to_kkgamma_line(name="Hlt2RD_BsToKKGamma", persistreco=False, prescale=1.0):
    pvs = make_pvs()
    # remember to also look at the definition of make_rd_detached_phis as only some cuts are changed here
    phis = make_rd_detached_phis(
        name="rd_BsToKKGamma_detached_phis",
        k_p_min=2.0 * GeV,
        k_pt_min=500.0 * MeV,
        k_ipchi2_min=11.0,
        k_pid=((F.PID_K > 2.0) & ((F.PID_K - F.PID_P) > 0)),
        phi_pt_min=1500.0 * MeV,
        vchi2pdof_max=15.0,
        am_min=950.0 * MeV,
        am_max=2450.0 * MeV,
    )

    photons = make_rd_photons(
        et_min=2.5 * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2
    )

    b_s0 = make_b2xgamma_excl(
        intermediate=phis,
        photons=photons,
        pvs=pvs,
        descriptor="B_s0 -> phi(1020) gamma",
        dira_min=0.9995,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=25.0,
        name="rd_BsToKKGamma_Combiner",
    )

    gamma_child = rd_isolation.find_in_decay(input=b_s0, id="gamma")

    # Make cone isolation for parent and direct products
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B_s0"],
        candidates=[b_s0],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["gamma"],
        candidates=[gamma_child],
        cut=F.require_all(
            in_range(0.0, F.DR2, 0.16),
            ~F.REQUIRE_CLOSE(F.PT() @ F.FORWARDARG0(), F.PT() @ F.FORWARDARG1()),
            F.IS_NOT_H() @ F.FORWARDARG1() > 0.3,
        ),
        LongTrackIso=False,
        TTrackIso=False,
        DownstreamTrackIso=False,
        UpstreamTrackIso=False,
        NeutralIso=True,
        PiZerosIso=True,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [phis, b_s0],
        extra_outputs=iso_parts,
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
        pv_tracks=True,
    )


@register_line_builder(all_lines)
def bd_to_kpigamma_line(name="Hlt2RD_BdToKpiGamma", persistreco=False, prescale=1.0):
    pvs = make_pvs()

    # remember to also look at the definition of make_rd_detached_kstar0s as only some cuts are changed here
    kst = make_rd_detached_kstar0s(
        name="rd_BdToKpiGamma_detached_kstar0s",
        pi_p_min=2.0 * GeV,
        pi_pt_min=500.0 * MeV,
        k_p_min=3.0 * GeV,
        pi_pid=(F.PID_K < -3.0),
        k_pt_min=500.0 * MeV,
        k_ipchi2_min=11.0,
        pi_ipchi2_min=11.0,
        k_pid=((F.PID_K > 3.0) & ((F.PID_K - F.PID_P) > 2)),
        kstar0_pt_min=1500.0 * MeV,
        vchi2pdof_max=15.0,
        am_min=650.0 * MeV,
        am_max=2350.0 * MeV,
    )

    photons = make_rd_photons(
        et_min=2.5 * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2
    )

    b0 = make_b2xgamma_excl(
        intermediate=kst,
        photons=photons,
        pvs=pvs,
        descriptor="[B0 -> K*(892)0 gamma]cc",
        dira_min=0.9995,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=25.0,
        name="rd_BdToKpiGamma_Combiner",
    )

    gamma_child = rd_isolation.find_in_decay(input=b0, id="gamma")

    # Make cone isolation for parent and direct products
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[b0],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["gamma"],
        candidates=[gamma_child],
        cut=F.require_all(
            in_range(0.0, F.DR2, 0.16),
            ~F.REQUIRE_CLOSE(F.PT() @ F.FORWARDARG0(), F.PT() @ F.FORWARDARG1()),
            F.IS_NOT_H() @ F.FORWARDARG1() > 0.3,
        ),
        LongTrackIso=False,
        TTrackIso=False,
        DownstreamTrackIso=False,
        UpstreamTrackIso=False,
        NeutralIso=True,
        PiZerosIso=True,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kst, b0],
        extra_outputs=iso_parts,
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
        pv_tracks=True,
    )


@register_line_builder(all_lines)
def bu_to_kpipigamma_line(
    name="Hlt2RD_BuToKpipiGamma", persistreco=False, prescale=1.0
):
    pvs = make_pvs()

    # remember to also look at the definition of make_rd_detached_k1 as only some cuts are changed here
    k1 = make_rd_detached_k1(
        name="rd_detached_k1_{hash}",
        make_rd_detached_pions=make_rd_has_rich_detached_pions,
        make_rd_detached_kaons=make_rd_has_rich_detached_kaons,
        pi_p_min=1.0 * GeV,
        pi_pt_min=300.0 * MeV,
        pi_ipchi2_min=7.0,
        pi_pid=(F.PID_K < -2),
        k_p_min=1.0 * GeV,
        k_pt_min=300.0 * MeV,
        k_ipchi2_min=7.0,
        k_pid=((F.PID_K > 2.0) & ((F.PID_K - F.PID_P) > 2)),
        pt_min=1500.0 * MeV,
        am_2pi_max=1500 * MeV,
        am_kpi_max=1430 * MeV,
        am_min=1100.0 * MeV,
        am_max=1850.0 * MeV,
        adocachi2_cut=10.0,
        vchi2pdof_max=9,
    )

    photons = make_rd_photons(
        et_min=2.5 * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2
    )

    bu = make_b2xgamma_excl(
        intermediate=k1,
        photons=photons,
        pvs=pvs,
        descriptor="[B+ -> K_1(1270)+ gamma]cc",
        dira_min=0.9995,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=20.0,
        name="rd_BuToKpipiGamma_Combiner",
    )

    gamma_child = rd_isolation.find_in_decay(input=bu, id="gamma")

    # Make cone isolation for parent and direct products
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[bu],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["gamma"],
        candidates=[gamma_child],
        cut=F.require_all(
            in_range(0.0, F.DR2, 0.16),
            ~F.REQUIRE_CLOSE(F.PT() @ F.FORWARDARG0(), F.PT() @ F.FORWARDARG1()),
            F.IS_NOT_H() @ F.FORWARDARG1() > 0.3,
        ),
        LongTrackIso=False,
        TTrackIso=False,
        DownstreamTrackIso=False,
        UpstreamTrackIso=False,
        NeutralIso=True,
        PiZerosIso=True,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [k1, bu],
        extra_outputs=iso_parts,
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bu_to_kphigamma_line(name="Hlt2RD_BuToKPhiGamma", persistreco=True, prescale=1.0):
    pvs = make_pvs()

    phi = make_rd_detached_phis(
        name="rd_BuToKPhiGamma_detached_phis_{hash}",
        make_rd_detached_kaons=make_rd_has_rich_detached_kaons,
        make_pvs=make_pvs,
        am_min=990.0 * MeV,
        am_max=1070.0 * MeV,
        k_p_min=2.0 * GeV,
        k_pt_min=250.0 * MeV,
        k_ipchi2_min=7.0,
        k_pid=((F.PID_K > 2.0) & ((F.PID_K - F.PID_P) > 2)),
        phi_pt_min=500.0 * MeV,
        adocachi2cut=30.0,
        vchi2pdof_max=25.0,
    )

    k1680 = make_PV(
        hadrons=phi,
        kshort=make_rd_has_rich_detached_kaons(),
        decay_descriptor="[K*(1680)+ -> phi(1020) K+]cc",
        name="rd_radexcl_KPhi_{hash}",
        vtxchi2dof_max=16.0,
        m_min=1500 * MeV,
        m_max=2800 * MeV,
        min_pt=1000 * MeV,
        ipchi2_min=9.0,
        fdchi2_min=0.0,
        dira_min=0.9,
    )

    photons = make_rd_photons(
        et_min=2.5 * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2
    )

    bu = make_b2xgamma_excl(
        intermediate=k1680,
        photons=photons,
        pvs=pvs,
        descriptor="[B+ -> K*(1680)+ gamma]cc",
        dira_min=0.9995,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=25.0,
        name="rd_BuToKPhiGamma_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [phi, k1680, bu],
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bd_to_kspipigamma_LL_line(
    name="Hlt2RD_BdToKspipiGamma_LL", persistreco=True, prescale=1.0
):
    pvs = make_pvs()
    ks = ParticleFilter(
        make_rd_ks0_lls(
            name="rd_ks0_lls_{hash}",
            make_pions=make_rd_detached_pions,
            make_pvs=make_pvs,
            mass_window_comb=50.0 * MeV,
            mass_window=35.0 * MeV,
            pi_p_min=2.0 * GeV,
            pi_pt_min=200 * MeV,
            pi_ipchi2_min=9.0,
            pt_min=400 * MeV,
            adocachi2cut=30.0,
            bpvvdchi2_min=50.0,
            bpvltime_min=1.0 * ps,
            vchi2pdof_max=15.0,
        ),
        F.FILTER(F.require_all(F.CHILD(1, F.PID_K) < 0, F.CHILD(2, F.PID_K) < 0)),
    )

    rho0 = make_rd_detached_rho0(
        name="rd_detached_rho0_{hash}",
        make_rd_detached_pions=make_rd_has_rich_detached_pions,
        pi_pt_min=200.0 * MeV,
        pi_p_min=2.0 * GeV,
        pi_ipchi2_min=4.0,
        pi_pid=(F.PID_K < -3.0),
        pt_min=600.0 * MeV,
        am_min=400.0 * MeV,
        am_max=1500.0 * MeV,
        adocachi2cut=10.0,
        vchi2pdof_max=9.0,
    )

    k1 = make_PV(
        hadrons=rho0,
        kshort=ks,
        decay_descriptor="K_1(1270)0 -> rho(770)0 KS0",
        name="rd_radexcl_Kspipi_{hash}",
        vtxchi2dof_max=16.0,
        m_min=1050 * MeV,
        m_max=1900 * MeV,
        min_pt=1000 * MeV,
        ipchi2_min=9.0,
        fdchi2_min=0.0,
        dira_min=0.9,
    )

    photons = make_rd_photons(
        et_min=2.5 * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2
    )

    b0 = make_b2xgamma_excl(
        intermediate=k1,
        photons=photons,
        pvs=pvs,
        descriptor="B0 -> K_1(1270)0 gamma",
        dira_min=0.999,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=25.0,
        name="rd_BdToKspipiGamma_LL_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks, rho0, k1, b0],
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
        pv_tracks=True,
    )


@register_line_builder(all_lines)
def bd_to_kspipigamma_DD_line(
    name="Hlt2RD_BdToKspipiGamma_DD", persistreco=True, prescale=1.0
):
    pvs = make_pvs()

    ks = make_rd_ks0_dds(
        name="rd_ks0_dds_{hash}",
        make_pions=make_rd_detached_down_pions,
        make_pvs=make_pvs,
        mass_window_comb=50.0 * MeV,
        mass_window=35.0 * MeV,
        pi_p_min=2.0 * GeV,
        pi_pt_min=200 * MeV,
        adocachi2cut=30.0,
        bpvvdchi2_min=50.0,
        bpvltime_min=1.0 * ps,
        vchi2pdof_max=15.0,
    )

    rho0 = make_rd_detached_rho0(
        name="rd_detached_rho0_{hash}",
        make_rd_detached_pions=make_rd_has_rich_detached_pions,
        pi_pt_min=200.0 * MeV,
        pi_p_min=2.0 * GeV,
        pi_ipchi2_min=4.0,
        pi_pid=(F.PID_K < -3.0),
        pt_min=600.0 * MeV,
        am_min=400.0 * MeV,
        am_max=1500.0 * MeV,
        adocachi2cut=10.0,
        vchi2pdof_max=9.0,
    )

    k1 = make_PV(
        hadrons=rho0,
        kshort=ks,
        decay_descriptor="K_1(1270)0 -> rho(770)0 KS0",
        name="rd_radexcl_Kspipi_{hash}",
        vtxchi2dof_max=16.0,
        m_min=1050 * MeV,
        m_max=1900 * MeV,
        min_pt=1000 * MeV,
        ipchi2_min=9.0,
        fdchi2_min=0.0,
        dira_min=0.9,
    )

    photons = make_rd_photons(
        et_min=2.5 * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2
    )

    b0 = make_b2xgamma_excl(
        intermediate=k1,
        photons=photons,
        pvs=pvs,
        descriptor="B0 -> K_1(1270)0 gamma",
        dira_min=0.999,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=25.0,
        name="rd_BdToKspipiGamma_DD_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks, rho0, k1, b0],
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
        pv_tracks=True,
    )


@register_line_builder(all_lines)
def bd_to_kskkgamma_LL_line(
    name="Hlt2RD_BdToKsKKGamma_LL", persistreco=True, prescale=1.0
):
    pvs = make_pvs()
    ks = make_rd_ks0_lls(
        name="rd_ks0_lls_{hash}",
        make_pions=make_rd_detached_pions,
        make_pvs=make_pvs,
        mass_window_comb=50.0 * MeV,
        mass_window=35.0 * MeV,
        pi_p_min=2.0 * GeV,
        pi_pt_min=200.0 * MeV,
        pi_ipchi2_min=9.0,
        pt_min=400 * MeV,
        adocachi2cut=30.0,
        bpvvdchi2_min=50.0,
        bpvltime_min=1.0 * ps,
        vchi2pdof_max=15.0,
    )

    phi = make_rd_detached_phis(
        name="rd_BdToKsKKGamma_detached_phis_{hash}",
        make_rd_detached_kaons=make_rd_has_rich_detached_kaons,
        make_pvs=make_pvs,
        am_min=990.0 * MeV,
        am_max=1070.0 * MeV,
        k_p_min=2.0 * GeV,
        k_pt_min=250.0 * MeV,
        k_ipchi2_min=4.0,
        k_pid=((F.PID_K > 0.0) & ((F.PID_K - F.PID_P) > 0)),
        phi_pt_min=500.0 * MeV,
        adocachi2cut=30.0,
        vchi2pdof_max=25.0,
    )

    k2 = make_PV(
        hadrons=phi,
        kshort=ks,
        decay_descriptor="K*_2(1980)0 -> phi(1020) KS0",
        name="rd_radexcl_KsKK_{hash}",
        vtxchi2dof_max=16.0,
        m_min=1500 * MeV,
        m_max=2800 * MeV,
        min_pt=1000 * MeV,
        ipchi2_min=9.0,
        fdchi2_min=0.0,
        dira_min=0.9,
    )

    photons = make_rd_photons(
        et_min=2.5 * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2
    )

    b0 = make_b2xgamma_excl(
        intermediate=k2,
        photons=photons,
        pvs=pvs,
        descriptor="B0 -> K*_2(1980)0 gamma",
        name="rd_BdToKsKKGamma_LL_Combiner",
        dira_min=0.9995,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=25.0,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks, phi, k2, b0],
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
        pv_tracks=True,
    )


def bd_to_kskkgamma_DD_line(
    name="Hlt2RD_BdToKsKKGamma_DD", persistreco=True, prescale=1.0
):
    pvs = make_pvs()

    ks = make_rd_ks0_dds(
        name="rd_ks0_dds_{hash}",
        make_pions=make_rd_detached_down_pions,
        make_pvs=make_pvs,
        mass_window_comb=80.0 * MeV,
        mass_window=64.0 * MeV,
        pi_p_min=2.0 * GeV,
        pi_pt_min=200.0 * MeV,
        adocachi2cut=30.0,
        bpvvdchi2_min=50.0,
        bpvltime_min=1.0 * ps,
        vchi2pdof_max=15.0,
    )

    phi = make_rd_detached_phis(
        name="rd_BdToKsKKGamma_detached_phis_{hash}",
        make_rd_detached_kaons=make_rd_has_rich_detached_kaons,
        make_pvs=make_pvs,
        am_min=990.0 * MeV,
        am_max=1070.0 * MeV,
        k_p_min=2.0 * GeV,
        k_pt_min=250.0 * MeV,
        k_ipchi2_min=4.0,
        k_pid=((F.PID_K > 0.0) & ((F.PID_K - F.PID_P) > 0)),
        phi_pt_min=500.0 * MeV,
        adocachi2cut=30.0,
        vchi2pdof_max=25.0,
    )

    k2 = make_PV(
        hadrons=phi,
        kshort=ks,
        decay_descriptor="K*_2(1980)0 -> phi(1020) KS0",
        name="rd_radexcl_KsKK_{hash}",
        vtxchi2dof_max=16.0,
        m_min=1500 * MeV,
        m_max=2800 * MeV,
        min_pt=1000 * MeV,
        ipchi2_min=9.0,
        fdchi2_min=0.0,
        dira_min=0.9,
    )

    photons = make_rd_photons(
        et_min=2.5 * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2
    )

    b0 = make_b2xgamma_excl(
        intermediate=k2,
        photons=photons,
        pvs=pvs,
        descriptor="B0 -> K*_2(1980)0 gamma",
        dira_min=0.9995,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=25.0,
        name="rd_BdToKsKKGamma_DD_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks, phi, k2, b0],
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
        pv_tracks=True,
    )


@register_line_builder(all_lines)
def bs_to_kskpigamma_LL_line(
    name="Hlt2RD_BsToKsKpiGamma_LL", persistreco=True, prescale=1.0
):
    pvs = make_pvs()
    ks = make_rd_ks0_lls(
        name="rd_ks0_lls_{hash}",
        make_pions=make_rd_detached_pions,
        make_pvs=make_pvs,
        mass_window_comb=50.0 * MeV,
        mass_window=35.0 * MeV,
        pi_p_min=1.0 * GeV,
        pi_pt_min=200.0 * MeV,
        pi_ipchi2_min=9.0,
        pt_min=400 * MeV,
        adocachi2cut=30.0,
        bpvvdchi2_min=50.0,
        bpvltime_min=1.0 * ps,
        vchi2pdof_max=15.0,
    )

    kst = make_rd_detached_kstar0s(
        name="rd_BsToKsKpiGamma_detached_kstar0s",
        pi_p_min=1.0 * GeV,
        pi_pt_min=200.0 * MeV,
        pi_pid=(F.PID_K < -3.0),
        k_p_min=1.0 * GeV,
        k_pt_min=250.0 * MeV,
        k_ipchi2_min=4.0,
        pi_ipchi2_min=4.0,
        k_pid=((F.PID_K > 2.0) & ((F.PID_K - F.PID_P) > 2)),
        kstar0_pt_min=500.0 * MeV,
        vchi2pdof_max=15.0,
        am_min=650.0 * MeV,
        am_max=2500.0 * MeV,
    )

    k1 = make_PV(
        hadrons=kst,
        kshort=ks,
        decay_descriptor="[phi(1680) -> K*(892)0 KS0]cc",
        name="rd_radexcl_KsKpi_{hash}",
        vtxchi2dof_max=16.0,
        m_min=1000 * MeV,
        m_max=3000 * MeV,
        min_pt=1000 * MeV,
        ipchi2_min=9.0,
        fdchi2_min=0.0,
        dira_min=0.9,
    )

    photons = make_rd_photons(
        et_min=2.5 * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2
    )

    bs = make_b2xgamma_excl(
        intermediate=k1,
        photons=photons,
        pvs=pvs,
        descriptor="[B_s0 -> phi(1680) gamma]cc",
        dira_min=0.9995,
        name="rd_BsToKsKpiGamma_LL_Combiner",
        bpv_ipchi2_max=12,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=25.0,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks, kst, k1, bs],
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


def bs_to_kskpigamma_DD_line(
    name="Hlt2RD_BsToKsKpiGamma_DD", persistreco=True, prescale=1.0
):
    pvs = make_pvs()

    ks = make_rd_ks0_dds(
        name="rd_ks0_dds_{hash}",
        make_pions=make_rd_detached_down_pions,
        make_pvs=make_pvs,
        mass_window_comb=50.0 * MeV,
        mass_window=35.0 * MeV,
        pi_p_min=1.0 * GeV,
        pi_pt_min=200.0 * MeV,
        adocachi2cut=30.0,
        bpvvdchi2_min=50.0,
        bpvltime_min=1.0 * ps,
        vchi2pdof_max=15.0,
    )

    kst = make_rd_detached_kstar0s(
        name="rd_BsToKsKpiGamma_detached_kstar0s",
        pi_p_min=1.0 * GeV,
        pi_pt_min=200.0 * MeV,
        pi_pid=(F.PID_K < -3.0),
        k_p_min=1.0 * GeV,
        k_pt_min=250.0 * MeV,
        k_ipchi2_min=4.0,
        pi_ipchi2_min=4.0,
        k_pid=((F.PID_K > 2.0) & ((F.PID_K - F.PID_P) > 2)),
        kstar0_pt_min=1000.0 * MeV,
        vchi2pdof_max=15.0,
        am_min=650.0 * MeV,
        am_max=2500.0 * MeV,
    )

    k1 = make_PV(
        hadrons=kst,
        kshort=ks,
        decay_descriptor="[phi(1680) -> K*(892)0 KS0]cc",
        name="rd_radexcl_KsKpi_{hash}",
        vtxchi2dof_max=16.0,
        m_min=1000 * MeV,
        m_max=3000 * MeV,
        min_pt=1000 * MeV,
        ipchi2_min=9.0,
        fdchi2_min=0.0,
        dira_min=0.9,
    )

    photons = make_rd_photons(
        et_min=2.5 * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2
    )

    bs = make_b2xgamma_excl(
        intermediate=k1,
        photons=photons,
        pvs=pvs,
        descriptor="[B_s0 -> phi(1680) gamma]cc",
        dira_min=0.9995,
        name="rd_BsToKsKpiGamma_DD_Combiner",
        pt_min=2000 * MeV,
        bpv_ipchi2_max=12,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=25.0,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks, kst, k1, bs],
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lb_to_ksppigamma_LL_line(
    name="Hlt2RD_LbToKsppiGamma_LL", persistreco=True, prescale=1.0
):
    pvs = make_pvs()
    ks = make_rd_ks0_lls(
        name="rd_ks0_lls_{hash}",
        make_pions=make_rd_detached_pions,
        make_pvs=make_pvs,
        mass_window_comb=50.0 * MeV,
        mass_window=35.0 * MeV,
        pi_p_min=2.0 * GeV,
        pi_pt_min=200.0 * MeV,
        pi_ipchi2_min=9.0,
        pt_min=400 * MeV,
        adocachi2cut=30.0,
        bpvvdchi2_min=50.0,
        bpvltime_min=1.0 * ps,
        vchi2pdof_max=15.0,
    )

    lst = ParticleFilter(
        make_rd_detached_pK(
            am_min=1300.0 * MeV,
            am_max=2800.0 * MeV,
            p_pid=F.require_all(F.PID_P > 2, (F.PID_P - F.PID_K > 2.0)),
            k_pid=(F.PID_K < -1.0),  # pion
            p_ipchi2_min=9.0,
            k_ipchi2_min=9.0,
            k_p_min=2.0 * GeV,
            p_p_min=2.0 * GeV,
            k_pt_min=250.0 * MeV,
            p_pt_min=400.0 * MeV,
            adocachi2cut=10.0,
            vchi2pdof_max=10.0,
            pK_pt_min=800.0 * MeV,
        ),
        F.FILTER(F.require_all(F.MASS > 1375.0 * MeV, F.MASS < 2625.0 * MeV)),
        name="rd_dt_pk_{hash}",
    )

    l1800 = make_PV(
        hadrons=lst,
        kshort=ks,
        decay_descriptor="[Lambda(1800)0 -> Lambda(1520)0 KS0]cc",
        name="rd_radexcl_Ksppi_{hash}",
        vtxchi2dof_max=16.0,
        m_min=1700 * MeV,
        m_max=3200 * MeV,
        min_pt=1000 * MeV,
        ipchi2_min=9.0,
        fdchi2_min=0.0,
        dira_min=0.9,
    )

    photons = make_rd_photons(
        et_min=2.5 * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2
    )

    lb = make_b2xgamma_excl(
        intermediate=l1800,
        photons=photons,
        pvs=pvs,
        descriptor="[Lambda_b0 -> Lambda(1800)0 gamma]cc",
        name="rd_LbToKsppiGamma_LL_Combiner",
        dira_min=0.9995,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=25.0,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks, lst, l1800, lb],
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lb_to_ksppigamma_DD_line(
    name="Hlt2RD_LbToKsppiGamma_DD", persistreco=True, prescale=1.0
):
    pvs = make_pvs()

    ks = make_rd_ks0_dds(
        name="rd_ks0_dds_{hash}",
        make_pions=make_rd_detached_down_pions,
        make_pvs=make_pvs,
        mass_window_comb=80.0 * MeV,
        mass_window=64.0 * MeV,
        pi_p_min=2.0 * GeV,
        pi_pt_min=200.0 * MeV,
        adocachi2cut=30.0,
        bpvvdchi2_min=50.0,
        bpvltime_min=1.0 * ps,
        vchi2pdof_max=15.0,
    )

    lst = ParticleFilter(
        make_rd_detached_pK(
            am_min=1300.0 * MeV,
            am_max=2800.0 * MeV,
            p_pid=F.require_all(F.PID_P > 2, (F.PID_P - F.PID_K > 2.0)),
            k_pid=(F.PID_K < 0.0),  # pion
            p_ipchi2_min=9.0,
            k_ipchi2_min=9.0,
            k_p_min=2.0 * GeV,
            p_p_min=2.0 * GeV,
            k_pt_min=250.0 * MeV,
            p_pt_min=400.0 * MeV,
            adocachi2cut=10.0,
            vchi2pdof_max=10.0,
            pK_pt_min=800.0 * MeV,
        ),
        F.FILTER(F.require_all(F.MASS > 1375.0 * MeV, F.MASS < 2625.0 * MeV)),
        name="rd_dt_pk_{hash}",
    )

    l1800 = make_PV(
        hadrons=lst,
        kshort=ks,
        decay_descriptor="[Lambda(1800)0 -> Lambda(1520)0 KS0]cc",
        name="rd_radexcl_Ksppi_{hash}",
        vtxchi2dof_max=16.0,
        m_min=1700 * MeV,
        m_max=3200 * MeV,
        min_pt=1000 * MeV,
        ipchi2_min=9.0,
        fdchi2_min=0.0,
        dira_min=0.9,
    )

    photons = make_rd_photons(
        et_min=2.5 * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2
    )

    lb = make_b2xgamma_excl(
        intermediate=l1800,
        photons=photons,
        pvs=pvs,
        descriptor="[Lambda_b0 -> Lambda(1800)0 gamma]cc",
        name="rd_LbToKsppiGamma_DD_Combiner",
        dira_min=0.9995,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=25.0,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks, lst, l1800, lb],
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lb_to_pkgamma_line(name="Hlt2RD_LbTopKGamma", persistreco=False, prescale=1.0):
    pvs = make_pvs()

    lst = ParticleFilter(
        make_rd_detached_pK(
            am_min=1350.0 * MeV,
            am_max=2800.0 * MeV,
            p_pid=F.require_all(F.PID_P > 2, (F.PID_P - F.PID_K > 2.0)),
            k_pid=((F.PID_K > 2.0) & ((F.PID_K - F.PID_P) > 0.0)),
            p_ipchi2_min=9.0,
            k_ipchi2_min=9.0,
            k_p_min=2.0 * GeV,
            p_p_min=2.0 * GeV,
            k_pt_min=400.0 * MeV,
            p_pt_min=500.0 * MeV,
            adocachi2cut=10.0,
            vchi2pdof_max=10.0,
            pK_pt_min=1300.0 * MeV,
        ),
        F.FILTER(F.require_all(F.MASS > 1375.0 * MeV, F.MASS < 2625.0 * MeV)),
        name="rd_dt_pk_{hash}",
    )

    photons = make_rd_photons(
        et_min=2.5 * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2
    )

    lb = make_b2xgamma_excl(
        intermediate=lst,
        photons=photons,
        pvs=pvs,
        descriptor="[Lambda_b0 -> Lambda(1520)0 gamma]cc",
        name="rd_LbTopKGamma_Combiner",
        dira_min=0.9995,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=25.0,
        comb_m_min=4500 * MeV,
        comb_m_max=6500 * MeV,
    )

    gamma_child = rd_isolation.find_in_decay(input=lb, id="gamma")

    # Make cone isolation for parent and direct products
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["Lb"],
        candidates=[lb],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["gamma"],
        candidates=[gamma_child],
        cut=F.require_all(
            in_range(0.0, F.DR2, 0.16),
            ~F.REQUIRE_CLOSE(F.PT() @ F.FORWARDARG0(), F.PT() @ F.FORWARDARG1()),
            F.IS_NOT_H() @ F.FORWARDARG1() > 0.3,
        ),
        LongTrackIso=False,
        TTrackIso=False,
        DownstreamTrackIso=False,
        UpstreamTrackIso=False,
        NeutralIso=True,
        PiZerosIso=True,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [lst, lb],
        extra_outputs=iso_parts,
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def b0_to_pipigamma_line(name="Hlt2RD_BdTopipiGamma", persistreco=False, prescale=1.0):
    pvs = make_pvs()
    rho0 = make_rd_detached_rho0(
        name="rd_detached_rho0_{hash}",
        make_rd_detached_pions=make_rd_has_rich_detached_pions,
        pi_pt_min=500.0 * MeV,
        pi_p_min=2.0 * GeV,
        pi_ipchi2_min=16.0,
        pi_pid=(F.PID_K < -5.0) & (F.PID_P < 0),
        pt_min=1200.0 * MeV,
        am_min=500.0 * MeV,
        am_max=4000.0 * MeV,
        adocachi2cut=10.0,
        vchi2pdof_max=9.0,
    )

    photons = make_rd_photons(
        et_min=2.5 * GeV, IsPhoton_min=0.6, IsNotH_min=0.4, E19_min=0.2
    )

    bd = make_b2xgamma_excl(
        intermediate=rho0,
        photons=photons,
        pvs=pvs,
        descriptor="B0 -> rho(770)0 gamma",
        dira_min=0.9999,
        sum_pt_min=5.0 * GeV,
        bpv_fdchi2_min=25.0,
        name="rd_BtopipiGamma_Combiner",
    )

    gamma_child = rd_isolation.find_in_decay(input=bd, id="gamma")

    # Make cone isolation for parent and direct products
    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"],
        candidates=[bd],
        cut=F.require_all(in_range(0.0, F.DR2, 0.25), ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["gamma"],
        candidates=[gamma_child],
        cut=F.require_all(
            in_range(0.0, F.DR2, 0.16),
            ~F.REQUIRE_CLOSE(F.PT() @ F.FORWARDARG0(), F.PT() @ F.FORWARDARG1()),
            F.IS_NOT_H() @ F.FORWARDARG1() > 0.3,
        ),
        LongTrackIso=False,
        TTrackIso=False,
        DownstreamTrackIso=False,
        UpstreamTrackIso=False,
        NeutralIso=True,
        PiZerosIso=True,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [rho0, bd],
        extra_outputs=iso_parts,
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
        pv_tracks=True,
    )


@register_line_builder(all_lines)
def bc_to_dstgamma_line(name="Hlt2RD_BcToDstGamma", persistreco=True, prescale=1.0):
    pvs = make_pvs()

    pions = make_rd_has_rich_detached_pions()
    kaons = make_rd_has_rich_detached_kaons()
    dzeros = make_dzeros_for_rd_bc(kaons, pions)

    deltaM_min = 135.421 * MeV
    deltaM_max = 160.421 * MeV

    dst = ParticleFilter(
        make_PV(
            hadrons=dzeros,
            kshort=make_rd_detached_pions(p_min=1.5 * GeV, pt_min=150.0 * MeV),
            decay_descriptor="[D*(2010)+ -> D0 pi+]cc",
            name="rd_radexcl_d0pi_{hash}",
            vtxchi2dof_max=10.0,
            m_min=1920.0 * MeV,
            m_max=2100.0 * MeV,
            min_pt=1000.0 * MeV,
            ipchi2_min=9.0,
            fdchi2_min=0.0,
            dira_min=0.9,
        ),
        F.FILTER(
            F.require_all(
                in_range(deltaM_min, F.MASS - F.CHILD(1, F.MASS), deltaM_max)
            ),
        ),
    )

    photons = make_rd_photons(
        et_min=2.5 * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2
    )

    bc = make_b2xgamma_excl(
        intermediate=dst,
        photons=photons,
        pvs=pvs,
        descriptor="[B_c+ -> D*(2010)+ gamma]cc",
        name="rd_BcToDstGamma_Combiner",
        comb_m_min=5200.0 * MeV,
        comb_m_max=7200.0 * MeV,
        dira_min=0.9995,
        sum_pt_min=4.0 * GeV,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dzeros, dst, bc],
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bc_to_dsstgamma_line(name="Hlt2RD_BcToDsstGamma", persistreco=True, prescale=1.0):
    pions = make_rd_has_rich_detached_pions(pid=(F.PID_K < 0.0))
    kaons = make_rd_has_rich_detached_kaons(
        pid=((F.PID_K > 2.0) & ((F.PID_K - F.PID_P) > 2))
    )
    ds = make_ds_for_rd_bc(kaons, kaons, pions)

    photons = make_rd_photons(
        et_min=2.5 * GeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2
    )

    soft_photons = make_rd_photons(
        et_min=500.0 * MeV, IsPhoton_min=0.4, IsNotH_min=0.3, E19_min=0.2
    )

    dsst = make_dsst(
        hadrons=ds,
        kshort=soft_photons,
        decay_descriptor="[D*_s+ -> D_s+ gamma]cc",
        name="rd_radexcl_dsgamma_{hash}",
        m_min=2010.0 * MeV,
        m_max=2210.0 * MeV,
        min_pt=1000.0 * MeV,
    )

    bc = make_b2xgamma_excl_bc(
        intermediate=dsst,
        photons=photons,
        descriptor="[B_c+ -> D*_s+ gamma]cc",
        sum_pt_min=4.0 * GeV,
        name="rd_BcToDsstGamma_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ds, dsst, bc],
        prescale=prescale,
        persistreco=persistreco,
        calo_clusters=True,
        calo_digits=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )
