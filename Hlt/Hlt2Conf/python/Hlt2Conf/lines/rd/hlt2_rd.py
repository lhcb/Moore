# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines HLT2 lines for studies of rare decays.
"""

from . import (
    RpK_lines,
    b_to_hemu,
    b_to_hemu_control_modes,
    b_to_kstarmumu,
    b_to_ll_hlt2,
    b_to_ll_LFV,
    b_to_multilepton_hlt2,
    b_to_tautau_hlt2,
    b_to_v0ll_hlt2,
    b_to_Xdilepton_detached,
    b_to_xgamma_conv_exclusive_hlt2,
    b_to_xgamma_exclusive_hlt2,
    b_to_xll_hlt2,
    b_to_xll_hlt2_mva,
    b_to_xtaul_hlt2,
    baryonic,
    baryonic_radiative,
    bnv_lines_hlt2,
    btosetau_exclusive_hlt2,
    btosetau_tau_to_e_exclusive_hlt2,
    btosmutau_exclusive_hlt2,
    btosmutau_tau_to_e_exclusive_hlt2,
    btostautau_ee_exclusive_hlt2,
    btostautau_exclusive_hlt2,
    btostautau_mue_exclusive_hlt2,
    lfv_lines_hlt2,
    omega_rare_decay_lines,
    prompt_multilepton,
    qqbar_to_ll,
    rad_incl,
    rare_tau_decay_lines,
    strange,
)

# full lines
full_lines = {}
full_lines.update(rad_incl.all_lines)

# turbo lines
turbo_lines = {}
turbo_lines.update(b_to_ll_hlt2.all_lines)
turbo_lines.update(b_to_multilepton_hlt2.all_lines)
turbo_lines.update(b_to_Xdilepton_detached.all_lines)
turbo_lines.update(b_to_xtaul_hlt2.all_lines)
turbo_lines.update(prompt_multilepton.all_lines)
turbo_lines.update(rare_tau_decay_lines.all_lines)
turbo_lines.update(RpK_lines.all_lines)
turbo_lines.update(b_to_hemu.all_lines)
turbo_lines.update(b_to_hemu_control_modes.all_lines)
turbo_lines.update(baryonic.all_lines)
turbo_lines.update(qqbar_to_ll.all_lines)
turbo_lines.update(b_to_ll_LFV.hlt2_lines)
turbo_lines.update(b_to_kstarmumu.all_lines)
turbo_lines.update(btosetau_exclusive_hlt2.all_lines)
turbo_lines.update(btosmutau_tau_to_e_exclusive_hlt2.all_lines)
turbo_lines.update(btosetau_tau_to_e_exclusive_hlt2.all_lines)
turbo_lines.update(btosmutau_exclusive_hlt2.all_lines)
turbo_lines.update(btostautau_exclusive_hlt2.all_lines)
turbo_lines.update(btostautau_mue_exclusive_hlt2.all_lines)
turbo_lines.update(btostautau_ee_exclusive_hlt2.all_lines)
turbo_lines.update(b_to_xgamma_exclusive_hlt2.all_lines)
turbo_lines.update(b_to_xgamma_conv_exclusive_hlt2.all_lines)
turbo_lines.update(b_to_xll_hlt2.all_lines)
turbo_lines.update(b_to_xll_hlt2_mva.all_lines)
turbo_lines.update(strange.all_lines)
turbo_lines.update(b_to_v0ll_hlt2.all_lines)
turbo_lines.update(omega_rare_decay_lines.all_lines)
turbo_lines.update(baryonic_radiative.all_lines)
turbo_lines.update(bnv_lines_hlt2.all_lines)
turbo_lines.update(lfv_lines_hlt2.all_lines)
turbo_lines.update(b_to_tautau_hlt2.hlt2_lines)
