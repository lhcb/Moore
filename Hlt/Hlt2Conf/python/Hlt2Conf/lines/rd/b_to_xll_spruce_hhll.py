###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Sprucing line definitions of B -> Hll and B->HHll lines, with l=e,mu
The idea is to have a set of sprucing lines that are well aligned to the cuts used in the inclusive detached dilepton lines (IDDL).
Next to the items in the cut dictionaries most of the times are reported, in a comment, the values of the cuts before the alignment with the IDDL
(if the alignment happened is signaled with iddlM).
Since the idea is to have the same selection regardless of the hadron considered, no PID cuts are included.

Contact authors : Michele Atzeni (michele.atzeni@cern.ch), Eluned Anne Smith (eluned.anne.smith@cern.ch)

Last update : 20/06/2023

"""

import Functors as F
from GaudiKernel.SystemOfUnits import MeV
from Moore.config import register_line_builder
from Moore.lines import SpruceLine
from RecoConf import rdbuilder_thor
from RecoConf.reconstruction_objects import make_pvs

from .builders import b_to_xll_builders, rd_isolation
from .builders.rd_prefilters import rd_prefilter

sprucing_lines = {}
#######################################
#      B+ -> h+ ll sprucing lines     #
#######################################
BtoHee = {
    "B": {
        "am_min": 4_500.0 * MeV,  # iddl NOT M
        "am_max": 7_000.0 * MeV,  # iddl NOT M
        "B_pt_min": 0.0 * MeV,
        "FDchi2_min": 10.0,  # 100.,#iddlM
        "vchi2pdof_max": 20.0,  # 9., #iddlM
        "bpvipchi2_max": 100_000.0,  # 16., #not in iddl, loosened
        "min_cosine": 0.9995,  # not in iddl
    },
    "dielectrons": {
        "adocachi2cut_max": 36.0,  ### 30 in inclusive
        "ipchi2_e_min": 3.0,  # 9.,#iddlM2body
        "pid_e_min": 1.0,  # 2.,#iddlM
        "pt_diE_min": 0.0 * MeV,  # iddlM
        "pt_e_min": 100.0 * MeV,  # 350. * MeV,#iddlM
        "p_e_min": 0.0 * MeV,  # iddlM in def of rd_detached_dielectron
        "vfaspfchi2ndof_max": 30.0,  # 9.,#iddlM
        "am_min": 0.0 * MeV,  # iddlM
        "am_max": 6_500 * MeV,  # 5_500. * MeV,#iddlM
        "bpvvdchi2_min": 0.0,  # 36., #iddlM3Body
    },
    "hadrons": {
        "pt_min": 100.0 * MeV,  # 400. * MeV,
        "mipchi2dvprimary_min": 6.0,  # 9.,
        "p_min": 2_000 * MeV,  # 0. * MeV,
        "pid": None,
    },
}

BtoHmumu = {
    "B": {
        "am_min": 4_500.0 * MeV,  # iddl NOT M
        "am_max": 7_000.0 * MeV,  # iddl NOT M
        "B_pt_min": 0.0 * MeV,  # iddlM
        "FDchi2_min": 10.0,  # 36.,
        "vchi2pdof_max": 20.0,  # 16.,#iddlM
        "bpvipchi2_max": 100_000.0,  # 5., #no cut in iddl, loosened
        "min_cosine": 0.9995,  # no cut in iddl, NOT M
    },
    "dimuons": {
        "adocachi2cut_max": 36.0,  # larger than iddl (30)
        "ipchi2_muon_min": 3.0,  # 9.,#iddlM2Body
        "pidmu_muon_min": -3.0,  # -4.,#iddlM
        "pt_dimuon_min": 0.0 * MeV,  # iddl matched
        "pt_muon_min": 100.0 * MeV,  # 350. * MeV, #iddlM
        "p_muon_min": 0.0 * MeV,  # iddlM
        "vchi2pdof_max": 30.0,  # 9.,#iddlM
        "bpvvdchi2_min": 0.0,  # 16., #iddlM3Body
        "am_min": 0.0 * MeV,  # iddlM
        "am_max": 6_500 * MeV,  # 5_500. * MeV,#iddlM
        # IsMuon #iddlM in def of rd_detached_dielectron
    },
    "hadrons": {
        "pt_min": 100.0 * MeV,  # 400. * MeV,
        "mipchi2dvprimary_min": 6.0,  # 4.,
        "p_min": 2_000 * MeV,  # 0. * MeV,
        "pid": None,  # (F.PID_K > -4.),
    },
}


@register_line_builder(sprucing_lines)
def Spruce_BuToHpEE_line(name="SpruceRD_BuToHpEE", prescale=1):
    """
    Sprucing line for B+ -> H+ e+ e- (cc)
    """
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoHee["dielectrons"], opposite_sign=True
    )
    hadrons = rdbuilder_thor.make_rd_detached_pions(**BtoHee["hadrons"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        hadrons,
        pvs,
        Descriptor="[B+ -> J/psi(1S) pi+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoHee["B"],
    )
    h_child = rd_isolation.find_in_decay(input=B, id="pi+")
    e_child = rd_isolation.find_in_decay(input=B, id="e+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"], candidates=[B], cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE())
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["Hp", "e"],
        candidates=[h_child, e_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        extra_outputs=iso_parts,
        hlt2_filter_code=[
            "Hlt2_InclDetDiElectronDecision",
            "Hlt2_InclDetDiElectron_3BodyDecision",
        ],
    )


@register_line_builder(sprucing_lines)
def Spruce_BuToHpEE_SameSign_line(name="SpruceRD_BuToHpEE_SameSign", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoHee["dielectrons"], opposite_sign=False
    )
    hadrons = rdbuilder_thor.make_rd_detached_pions(**BtoHee["hadrons"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        hadrons,
        pvs,
        Descriptor="[B+ -> J/psi(1S) pi+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoHee["B"],
    )
    h_child = rd_isolation.find_in_decay(input=B, id="pi+")
    e_child = rd_isolation.find_in_decay(input=B, id="e+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"], candidates=[B], cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE())
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["Hp", "e"],
        candidates=[h_child, e_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        extra_outputs=iso_parts,
        hlt2_filter_code=[
            "Hlt2_InclDetDiElectron_SSDecision",
            "Hlt2_InclDetDiElectron_3Body_SSDecision",
        ],
    )


@register_line_builder(sprucing_lines)
def Spruce_BuToHpMuMu_line(name="SpruceRD_BuToHpMuMu", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoHmumu["dimuons"], same_sign=False
    )
    hadrons = rdbuilder_thor.make_rd_detached_pions(**BtoHmumu["hadrons"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        hadrons,
        pvs,
        Descriptor="[B+ -> J/psi(1S) pi+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoHmumu["B"],
    )
    h_child = rd_isolation.find_in_decay(input=B, id="pi+")
    mu_child = rd_isolation.find_in_decay(input=B, id="mu+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"], candidates=[B], cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE())
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["Hp", "mu"],
        candidates=[h_child, mu_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        extra_outputs=iso_parts,
        hlt2_filter_code=[
            "Hlt2_InclDetDiMuonDecision",
            "Hlt2_InclDetDiMuon_3BodyDecision",
        ],
    )


@register_line_builder(sprucing_lines)
def Spruce_BuToHpMuMu_SameSign_line(name="SpruceRD_BuToHpMuMu_SameSign", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoHmumu["dimuons"], same_sign=True
    )
    hadrons = rdbuilder_thor.make_rd_detached_pions(**BtoHmumu["hadrons"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        hadrons,
        pvs,
        Descriptor="[B+ -> J/psi(1S) pi+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoHmumu["B"],
    )
    h_child = rd_isolation.find_in_decay(input=B, id="pi+")
    mu_child = rd_isolation.find_in_decay(input=B, id="mu+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"], candidates=[B], cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE())
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["Hp", "mu"],
        candidates=[h_child, mu_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        extra_outputs=iso_parts,
        hlt2_filter_code=[
            "Hlt2_InclDetDiMuon_SSDecision",
            "Hlt2_InclDetDiMuon_3Body_SSDecision",
        ],
    )


#############################################
#      B0 -> H+ H- ll sprucing lines        #
#############################################

#### Selections #####
BtoHHee = {
    "B": {
        "am_min": 4_500.0 * MeV,  # iddl NOT M
        "am_max": 7_000.0 * MeV,  # iddl NOT M
        "B_pt_min": 0.0 * MeV,  # iddlM
        "FDchi2_min": 10.0,  # 100.,#iddlM
        "vchi2pdof_max": 20.0,  # 9.,#iddlM
        "bpvipchi2_max": 100_000.0,  # 16.,# not in iddl, loosen cut
        "min_cosine": 0.9995,  # not in iddl, NOT M
    },
    "dielectrons": {
        "adocachi2cut_max": 36.0,  ### larger than iddl
        "ipchi2_e_min": 3.0,  # 9.,#iddlM2Body
        "pid_e_min": 1.0,  # 2.,#iddlM
        "pt_diE_min": 0.0 * MeV,  # iddlM
        "pt_e_min": 100.0 * MeV,  # 350. * MeV,#iddlM
        "p_e_min": 0.0 * MeV,  # iddlM
        "vfaspfchi2ndof_max": 30.0,  # 9.,#iddlM
        "am_min": 0.0 * MeV,  # iddlM
        "am_max": 6_500.0 * MeV,  # 5_500. * MeV,#iddlM
        "bpvvdchi2_min": 0.0,  ##iddlM3Body
    },
    "rhos": {
        "pi_pid": None,
        "pi_ipchi2_min": 6.0,
    },
}

BtoHHmumu = {
    "B": {
        "am_min": 4_500.0 * MeV,  # iddl NOT M
        "am_max": 7_000.0 * MeV,  # iddl NOT M
        "B_pt_min": 0.0 * MeV,  # iddlM
        "FDchi2_min": 10.0,  # 36.,
        "vchi2pdof_max": 20.0,  # 16.,#iddlM
        "bpvipchi2_max": 100_000.0,  # 5., #no cut in iddl, loosened
        "min_cosine": 0.9995,  # no cut in iddl, NOT M
    },
    "dimuons": {
        "adocachi2cut_max": 36.0,  # larger than iddl (30)
        "ipchi2_muon_min": 3.0,  # 9.,#iddlM2Body
        "pidmu_muon_min": -3.0,  # -4.,#iddlM
        "pt_dimuon_min": 0.0 * MeV,  # iddl matched
        "pt_muon_min": 100.0 * MeV,  # 350. * MeV, #iddlM
        "p_muon_min": 0.0 * MeV,  # iddlM
        "vchi2pdof_max": 30.0,  # 9.,#iddlM
        "bpvvdchi2_min": 0.0,  # 16., #iddlM3Body
        "am_min": 0.0 * MeV,  # iddlM
        "am_max": 6_500 * MeV,  # 5_500. * MeV,#iddlM
        # IsMuon #iddlM
    },
    "rhos": {
        "pi_pid": None,
        "pi_ipchi2_min": 6.0,
    },
}


@register_line_builder(sprucing_lines)
def Spruce_B0ToHpHmEE_line(name="SpruceRD_B0ToHpHmEE", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoHHee["dielectrons"], opposite_sign=True
    )
    rhos = rdbuilder_thor.make_rd_detached_rho0(**BtoHHee["rhos"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        rhos,
        pvs,
        Descriptor="B0 -> J/psi(1S) rho(770)0",
        name="make_rd_BToXll_for_" + name,
        **BtoHHee["B"],
    )
    res_child = rd_isolation.find_in_decay(input=B, id="rho(770)0")
    pi_child = rd_isolation.find_in_decay(input=B, id="pi+")
    e_child = rd_isolation.find_in_decay(input=B, id="e+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B0", "Res"],
        candidates=[B, res_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e", "H"],
        candidates=[e_child, pi_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        extra_outputs=iso_parts,
        tagging_particles=True,
        hlt2_filter_code=[
            "Hlt2_InclDetDiElectronDecision",
            "Hlt2_InclDetDiElectron_3BodyDecision",
            "Hlt2_InclDetDiElectron_4BodyDecision",
        ],
    )


@register_line_builder(sprucing_lines)
def Spruce_B0ToHpHmEE_SameSign_line(name="SpruceRD_B0ToHpHmEE_SameSign", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoHHee["dielectrons"], opposite_sign=False
    )
    rhos = rdbuilder_thor.make_rd_detached_rho0(**BtoHHee["rhos"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        rhos,
        pvs,
        Descriptor="B0 -> J/psi(1S) rho(770)0",
        name="make_rd_BToXll_for_" + name,
        **BtoHHee["B"],
    )
    res_child = rd_isolation.find_in_decay(input=B, id="rho(770)0")
    pi_child = rd_isolation.find_in_decay(input=B, id="pi+")
    e_child = rd_isolation.find_in_decay(input=B, id="e+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B0", "Res"],
        candidates=[B, res_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e", "H"],
        candidates=[e_child, pi_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        extra_outputs=iso_parts,
        tagging_particles=True,
        hlt2_filter_code=[
            "Hlt2_InclDetDiElectron_SSDecision",
            "Hlt2_InclDetDiElectron_3Body_SSDecision",
            "Hlt2_InclDetDiElectron_4Body_SSDecision",
        ],
    )


@register_line_builder(sprucing_lines)
def Spruce_B0ToHpHmMuMu_line(name="SpruceRD_B0ToHpHmMuMu", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoHHmumu["dimuons"], same_sign=False
    )
    rhos = rdbuilder_thor.make_rd_detached_rho0(**BtoHHmumu["rhos"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        rhos,
        pvs,
        Descriptor="B0 -> J/psi(1S) rho(770)0",
        name="make_rd_BToXll_for_" + name,
        **BtoHHmumu["B"],
    )
    res_child = rd_isolation.find_in_decay(input=B, id="rho(770)0")
    pi_child = rd_isolation.find_in_decay(input=B, id="pi+")
    mu_child = rd_isolation.find_in_decay(input=B, id="mu+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B0", "Res"],
        candidates=[B, res_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu", "H"],
        candidates=[mu_child, pi_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        extra_outputs=iso_parts,
        tagging_particles=True,
        hlt2_filter_code=[
            "Hlt2_InclDetDiMuonDecision",
            "Hlt2_InclDetDiMuon_3BodyDecision",
            "Hlt2_InclDetDiMuon_4BodyDecision",
        ],
    )


@register_line_builder(sprucing_lines)
def Spruce_B0ToHpHmMuMu_SameSign_line(
    name="SpruceRD_B0ToHpHmMuMu_SameSign", prescale=1
):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoHHmumu["dimuons"], same_sign=True
    )
    rhos = rdbuilder_thor.make_rd_detached_rho0(**BtoHHmumu["rhos"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        rhos,
        pvs,
        Descriptor="B0 -> J/psi(1S) rho(770)0",
        name="make_rd_BToXll_for_" + name,
        **BtoHHmumu["B"],
    )
    res_child = rd_isolation.find_in_decay(input=B, id="rho(770)0")
    pi_child = rd_isolation.find_in_decay(input=B, id="pi+")
    mu_child = rd_isolation.find_in_decay(input=B, id="mu+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B0", "Res"],
        candidates=[B, res_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu", "H"],
        candidates=[mu_child, pi_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        extra_outputs=iso_parts,
        tagging_particles=True,
        hlt2_filter_code=[
            "Hlt2_InclDetDiMuon_SSDecision",
            "Hlt2_InclDetDiMuon_3Body_SSDecision",
            "Hlt2_InclDetDiMuon_4Body_SSDecision",
        ],
    )
