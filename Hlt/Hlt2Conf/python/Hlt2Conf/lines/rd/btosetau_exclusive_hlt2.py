###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of b -> s mu tau HLT2 lines.
Final states built are (all taus decay to muons and neutrinos):
 1. Bs decays to phi
     - Bs -> phi(-> K+ K-) e+ tau- and its charge conjugate
     - Bs -> phi(-> K+ K+) e+ tau- and its charge conjugate
     - Bs -> phi(-> K+ K+) e- tau+ and its charge conjugate
     - Bs -> phi(-> K+ K-) e+ tau+ and its charge conjugate
     - Bs -> phi(-> K+ K-) e+ tau- with a fake kaon and its charge conjugate
     - Bs -> phi(-> K+ K-) e+ tau- with a fake muon from the Bs and its charge conjugate
     - Bs -> phi(-> K+ K-) e+ tau- with a fake muon from the tau and its charge conjugate
 2. Lb decays to pK
     - Lb -> Lambda(-> p+ K-) e+ tau- and its charge conjugate
     - Lb -> Lambda(-> p+ K-) e- tau+ and its charge conjugate
     - Lb -> Lambda(-> p+ K+) e+ tau- and its charge conjugate
     - Lb -> Lambda(-> p+ K+) e- tau+ and its charge conjugate
     - Lb -> Lambda(-> p+ K-) e+ tau+ and its charge conjugate
     - Lb -> Lambda(-> p+ K-) e- tau- and its charge conjugate
     - Lb -> Lambda(-> p+ K-) e+ tau- with a fake kaon and its charge conjugate
     - Lb -> Lambda(-> p+ K-) e- tau+ with a fake kaon and its charge conjugate
     - Lb -> Lambda(-> p+ K-) e+ tau- with a fake proton
     - Lb -> Lambda(-> p+ K-) e- tau+ with a fake proton
     - Lb -> Lambda(-> K+ K-) e+ tau- with a fake muon
     - Lb -> Lambda(-> p+ K-) e- tau+ with a fake muon
 3. Bd decays to K*
     - Bd -> K*(-> K+ pi-) e+ tau- and its charge conjugate
     - Bd -> K*(-> K+ pi-) e- tau+ and its charge conjugate
     - Bd -> K*(-> K+ pi+) e+ tau- and its charge conjugate
     - Bd -> K*(-> K+ pi+) e- tau+ and its charge conjugate
     - Bd -> K*(-> K+ pi-) e+ tau+ and its charge conjugate
     - Bd -> K*(-> K+ pi-) e- tau- and its charge conjugate
     - Bd -> K*(-> K+ pi-) e+ tau- with a fake kaon and its charge conjugate
     - Bd -> K*(-> K+ pi-) e- tau+ with a fake kaon and its charge conjugate
     - Bd -> K*(-> K+ pi-) e+ tau- with a fake pion and its charge conjugate
     - Bd -> K*(-> K+ pi-) e- tau+ with a fake pion and its charge conjugate
     - Bd -> K*(-> K+ pi-) e+ tau- with a fake muon and its charge conjugate
     - Bd -> K*(-> K+ pi-) e- tau+ with a fake muon and its charge conjugate
Note a: fake particles are obtained through reversing the PID requirements
Note b: particles stemming directly from the b-hadron are combined with a ParticleCombiner. This is not intended to represent a given particle (hence the loose mass requirements)
"""

import Functors as F
from GaudiKernel.SystemOfUnits import MeV
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf import rdbuilder_thor
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.rd.builders import btosetau_exclusive, btosmutau_exclusive
from Hlt2Conf.lines.rd.builders.rd_isolation import parent_and_children_isolation
from Hlt2Conf.lines.rd.builders.rd_prefilters import (
    _RD_MONITORING_VARIABLES,
    rd_prefilter,
)

all_lines = {}

kwargs_protons = {
    "p_min": 3000 * MeV,
    "pt_min": 250 * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": F.require_all(F.PID_P > 4, F.PID_P - F.PID_K > 0, ~F.ISMUON),
}
kwargs_protons_reverse_pid = {
    "p_min": 3000 * MeV,
    "pt_min": 250 * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": (F.PID_P < 4),
}

kwargs_kaons = {
    "pt_min": 250 * MeV,
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": F.require_all(F.PID_K > 4, ~F.ISMUON),
}

kwargs_kaons_reverse_pid = {
    "pt_min": 250 * MeV,
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 16,
    "pid": (F.PID_K < 4),
}

kwargs_kaons_for_bu = {
    "pt_min": 750 * MeV,
    "p_min": 5000 * MeV,
    "mipchi2dvprimary_min": 36,
    "pid": F.require_all(F.PID_K > 4, F.PID_K - F.PID_P > 0, ~F.ISMUON),
}

kwargs_kaons_for_bu_reverse_pid = {
    "pt_min": 750 * MeV,
    "p_min": 5000 * MeV,
    "mipchi2dvprimary_min": 36,
    "pid": (F.PID_K < 4),
}

kwargs_kaons_for_kstar = {
    "p_min": 5000 * MeV,
    "pt_min": 500.0 * MeV,
    "mipchi2dvprimary_min": 25,
    "pid": F.require_all(F.PID_K > 6, ~F.ISMUON),
}
kwargs_kaons_for_kstar_reverse_pid = {
    "p_min": 5000 * MeV,
    "pt_min": 500.0 * MeV,
    "mipchi2dvprimary_min": 25,
    "pid": (F.PID_K < 6),
}

kwargs_pions = {
    "p_min": 5000 * MeV,
    "pt_min": 500 * MeV,
    "mipchi2dvprimary_min": 25,
    "pid": F.require_all(F.PID_K < -2, ~F.ISMUON),
}
kwargs_pions_reverse_pid = {
    "p_min": 5000 * MeV,
    "pt_min": 500 * MeV,
    "mipchi2dvprimary_min": 25,
    "pid": (F.PID_K > -2),
}

kwargs_muons_from_tau = {
    "p_min": 3000 * MeV,
    "pt_min": 250 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": F.require_all(F.PID_MU > 1, F.ISMUON),
}
kwargs_muons_from_tau_reverse_pid = {
    "p_min": 3000 * MeV,
    "pt_min": 250 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": (F.PID_MU < 1),
}

kwargs_electrons = {
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": F.require_all(F.PID_E > 6, ~F.ISMUON),
    "pt_min": 250 * MeV,
}

kwargs_electrons_reverse_pid = {
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 9,
    "pid": (F.PID_E < 6),
    "pt_min": 250 * MeV,
}

kwargs_electrons_for_bu = {
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 25,
    "pid": F.require_all(F.PID_E > 6, ~F.ISMUON),
    "pt_min": 750 * MeV,
}

kwargs_electrons_for_bu_reverse_pid = {
    "p_min": 3000 * MeV,
    "mipchi2dvprimary_min": 25,
    "pid": (F.PID_E < 6),
    "pt_min": 750 * MeV,
}


@register_line_builder(all_lines)
def bstophietau_tautomu_line(name="Hlt2RD_BsToKKTauE_TauToMu", prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    phies = btosetau_exclusive.make_phie(kaons, kaons, electrons, pvs)
    bs = btosmutau_exclusive.make_bs(phies, muons_from_tau, pvs)
    algs = rd_prefilter() + [phies, bs]

    iso_parts = parent_and_children_isolation(
        parents={"Bs": bs, "phie": phies}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bstophietau_tautomu_same_sign_kaons_sskmu_line(
    name="Hlt2RD_BsToKKTauE_TauToMu_SSK_SSKmu", prescale=0.001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    phies = btosetau_exclusive.make_phie(
        kaons,
        kaons,
        electrons,
        pvs,
        decay_descriptor="[B0 -> K+ K+ e+]cc",
        name="rd_same_sign_dikaon_electron_for_btosetau_{hash}",
    )
    bs = btosmutau_exclusive.make_bs(
        phies, muons_from_tau, pvs, name="rd_make_bs_to_kktaue_same_sign_kaons_{hash}"
    )
    algs = rd_prefilter() + [phies, bs]

    iso_parts = parent_and_children_isolation(
        parents={"Bs": bs, "phie": phies}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bstophietau_tautomu_same_sign_kaons_oskmu_line(
    name="Hlt2RD_BsToKKTauE_TauToMu_SSK_OSKmu", prescale=0.001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    phies = btosetau_exclusive.make_phie(
        kaons,
        kaons,
        electrons,
        pvs,
        decay_descriptor="[B0 -> K- K- e+]cc",
        name="rd_same_sign_dikaon_electron_for_btosetau_{hash}",
    )
    bs = btosmutau_exclusive.make_bs(
        phies, muons_from_tau, pvs, name="rd_make_bs_to_kktaue_same_sign_kaons_{hash}"
    )
    algs = rd_prefilter() + [phies, bs]

    iso_parts = parent_and_children_isolation(
        parents={"Bs": bs, "phie": phies}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bstophimutau_tautomu_same_sign_muons_line(
    name="Hlt2RD_BsToKKTauE_TauToMu_SSl", prescale=0.001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    phies = btosetau_exclusive.make_phie(
        kaons, kaons, electrons, pvs, decay_descriptor="[B0 -> K+ K- e+]cc"
    )
    bs = btosmutau_exclusive.make_bs(
        phies,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu+]cc",
        name="rd_make_bs_to_kktaue_same_sign_leptons_{hash}",
    )
    algs = rd_prefilter() + [phies, bs]

    iso_parts = parent_and_children_isolation(
        parents={"Bs": bs, "phie": phies}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bstophietau_tautomu_fakemuon_from_tau_line(
    name="Hlt2RD_BsToKKTauE_TauToMu_FakeMuonFromTau", prescale=0.0001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    fake_muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau_reverse_pid
    )
    phies = btosetau_exclusive.make_phie(kaons, kaons, electrons, pvs)
    bs = btosmutau_exclusive.make_bs(
        phies,
        fake_muons_from_tau,
        pvs,
        name="rd_make_bs_to_kktaue_fake_muon_from_tau_{hash}",
    )
    algs = rd_prefilter() + [phies, bs]

    iso_parts = parent_and_children_isolation(
        parents={"Bs": bs, "phie": phies},
        decay_products={"mu_f_tau": fake_muons_from_tau},
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bstophietau_tautomu_fakemuon_from_b_line(
    name="Hlt2RD_BsToKKTauE_TauToMu_FakeMuonFromB", prescale=0.0001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    fake_electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_reverse_pid
    )
    phies_fake_electron = btosetau_exclusive.make_phie(
        kaons,
        kaons,
        fake_electrons,
        pvs,
        name="rd_dikaon_fake_electron_for_btosetau_{hash}",
    )
    bs = btosmutau_exclusive.make_bs(
        phies_fake_electron,
        muons_from_tau,
        pvs,
        name="rd_make_bs_to_kktaue_fake_electron_from_b_{hash}",
    )
    algs = rd_prefilter() + [phies_fake_electron, bs]

    iso_parts = parent_and_children_isolation(
        parents={"Bs": bs, "phie": phies_fake_electron},
        decay_products={"mu_f_tau": muons_from_tau},
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bstophietau_tautomu_fakekaon_line(
    name="Hlt2RD_BsToKKTauE_TauToMu_FakeKaon", prescale=0.001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    fake_kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_reverse_pid)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    phies = btosetau_exclusive.make_phie(
        kaons,
        fake_kaons,
        electrons,
        pvs,
        name="rd_fake_dikaon_electron_for_btosetau_{hash}",
    )
    bs = btosmutau_exclusive.make_bs(
        phies, muons_from_tau, pvs, name="rd_make_bs_to_kktaue_fake_kaons_{hash}"
    )
    algs = rd_prefilter() + [phies, bs]

    iso_parts = parent_and_children_isolation(
        parents={"Bs": bs, "phie": phies}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lbtopketau_tautomu_sspmu_line(name="Hlt2RD_LbToPKTauE_TauToMu_SSpmu", prescale=1):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pkes = btosetau_exclusive.make_pke(
        kaons, protons, electrons, pvs, decay_descriptor="[B0 -> p+ K- e+]cc"
    )
    lb = btosmutau_exclusive.make_lb(
        pkes, muons_from_tau, pvs, decay_descriptor="[Lambda_b0 -> B0 mu-]cc"
    )
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={"Lb": lb, "pKe": pkes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lbtopketau_tautomu_ospmu_line(name="Hlt2RD_LbToPKTauE_TauToMu_OSpmu", prescale=1):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pkes = btosetau_exclusive.make_pke(
        kaons, protons, electrons, pvs, decay_descriptor="[B0 -> p+ K- e-]cc"
    )
    lb = btosmutau_exclusive.make_lb(
        pkes, muons_from_tau, pvs, decay_descriptor="[Lambda_b0 -> B0 mu+]cc"
    )
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={"Lb": lb, "pKe": pkes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lbtopketau_tautomu_same_sign_pk_sspmu_line(
    name="Hlt2RD_LbToPKTauE_TauToMu_SSpK_SSpmu", prescale=0.001
):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pkes = btosetau_exclusive.make_pke(
        kaons,
        protons,
        electrons,
        pvs,
        decay_descriptor="[B0 -> p+ K+ e+]cc",
        name="rd_same_sign_pk_electron_for_btosetau_{hash}",
    )
    lb = btosmutau_exclusive.make_lb(
        pkes,
        muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu-]cc",
        name="rd_make_lb_to_pketau_same_sign_pk_{hash}",
    )
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={"Lb": lb, "pKe": pkes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lbtopketau_tautomu_same_sign_pk_ospmu_line(
    name="Hlt2RD_LbToPKTauE_TauToMu_SSpK_OSpmu", prescale=0.001
):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pkes = btosetau_exclusive.make_pke(
        kaons,
        protons,
        electrons,
        pvs,
        decay_descriptor="[B0 -> p+ K+ e-]cc",
        name="rd_same_sign_pk_electron_for_btosetau_{hash}",
    )
    lb = btosmutau_exclusive.make_lb(
        pkes,
        muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu+]cc",
        name="rd_make_lb_to_pketau_same_sign_pk_{hash}",
    )
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={"Lb": lb, "pKe": pkes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lbtopketau_tautomu_same_sign_muons_sspmu_line(
    name="Hlt2RD_LbToPKTauE_TauToMu_SSl_SSpmu", prescale=0.001
):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pkes = btosetau_exclusive.make_pke(
        kaons, protons, electrons, pvs, decay_descriptor="[B0 -> p+ K- e+]cc"
    )
    lb = btosmutau_exclusive.make_lb(
        pkes,
        muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu+]cc",
        name="rd_make_lb_to_pketau_same_sign_leptons_{hash}",
    )
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={"Lb": lb, "pKe": pkes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lbtopketau_tautomu_same_sign_muons_ospmu_line(
    name="Hlt2RD_LbToPKTauE_TauToMu_SSl_OSpmu", prescale=0.001
):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pkes = btosetau_exclusive.make_pke(
        kaons, protons, electrons, pvs, decay_descriptor="[B0 -> p+ K- e-]cc"
    )
    lb = btosmutau_exclusive.make_lb(
        pkes,
        muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu-]cc",
        name="rd_make_lb_to_pketau_same_sign_leptons_{hash}",
    )
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={"Lb": lb, "pKe": pkes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lbtopketau_tautomu_fakemuon_from_tau_sspmu_line(
    name="Hlt2RD_LbToPKTauE_TauToMu_FakeMuonFromTau_SSpmu", prescale=0.0001
):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    fake_muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau_reverse_pid
    )
    pkes = btosetau_exclusive.make_pke(
        kaons, protons, electrons, pvs, decay_descriptor="[B0 -> p+ K- e+]cc"
    )
    lb = btosmutau_exclusive.make_lb(
        pkes,
        fake_muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu-]cc",
        name="rd_make_lb_to_pketau_fake_muon_from_tau_{hash}",
    )
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={"Lb": lb, "pKe": pkes},
        decay_products={"mu_f_tau": fake_muons_from_tau},
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lbtopketau_tautomu_fakemuon_from_tau_ospmu_line(
    name="Hlt2RD_LbToPKTauE_TauToMu_FakeMuonFromTau_OSpmu", prescale=0.0001
):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    fake_muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau_reverse_pid
    )
    pkes = btosetau_exclusive.make_pke(
        kaons, protons, electrons, pvs, decay_descriptor="[B0 -> p+ K- e-]cc"
    )
    lb = btosmutau_exclusive.make_lb(
        pkes,
        fake_muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu+]cc",
        name="rd_make_lb_to_pketau_fake_muon_from_tau_{hash}",
    )
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={"Lb": lb, "pKe": pkes},
        decay_products={"mu_f_tau": fake_muons_from_tau},
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lbtopketau_tautomu_fakemuon_from_b_sspmu_line(
    name="Hlt2RD_LbToPKTauE_TauToMu_FakeMuonFromB_SSpmu", prescale=0.0001
):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    fake_electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_reverse_pid
    )
    pkes_fake_electron = btosetau_exclusive.make_pke(
        kaons,
        protons,
        fake_electrons,
        pvs,
        decay_descriptor="[B0 -> p+ K- e+]cc",
        name="rd_pk_fake_electron_for_btosetau_{hash}",
    )
    lb = btosmutau_exclusive.make_lb(
        pkes_fake_electron,
        muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu-]cc",
        name="rd_make_lb_to_pketau_fake_electron_from_b_{hash}",
    )
    algs = rd_prefilter() + [pkes_fake_electron, lb]

    iso_parts = parent_and_children_isolation(
        parents={"Lb": lb, "pKe": pkes_fake_electron},
        decay_products={"mu_f_tau": muons_from_tau},
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lbtopketau_tautomu_fakemuon_from_b_ospmu_line(
    name="Hlt2RD_LbToPKTauE_TauToMu_FakeMuonFromB_OSpmu", prescale=0.0001
):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    fake_electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_reverse_pid
    )
    pkes_fake_electron = btosetau_exclusive.make_pke(
        kaons,
        protons,
        fake_electrons,
        pvs,
        decay_descriptor="[B0 -> p+ K- e-]cc",
        name="rd_pk_fake_electron_for_btosetau_{hash}",
    )
    lb = btosmutau_exclusive.make_lb(
        pkes_fake_electron,
        muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu+]cc",
        name="rd_make_lb_to_pketau_fake_electron_from_b_{hash}",
    )
    algs = rd_prefilter() + [pkes_fake_electron, lb]

    iso_parts = parent_and_children_isolation(
        parents={"Lb": lb, "pKe": pkes_fake_electron},
        decay_products={"mu_f_tau": muons_from_tau},
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lbtopketau_tautomu_fakeproton_sspmu_line(
    name="Hlt2RD_LbToPKTauE_TauToMu_FakeProton_SSpmu", prescale=0.001
):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons_reverse_pid)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pkes = btosetau_exclusive.make_pke(
        kaons,
        protons,
        electrons,
        pvs,
        decay_descriptor="[B0 -> p+ K- e+]cc",
        name="rd_pk_fake_proton_electron_for_btosetau_{hash}",
    )
    lb = btosmutau_exclusive.make_lb(
        pkes,
        muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu-]cc",
        name="rd_make_lb_to_pketau_fake_protons_{hash}",
    )
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={"Lb": lb, "pKe": pkes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lbtopketau_tautomu_fakeproton_ospmu_line(
    name="Hlt2RD_LbToPKTauE_TauToMu_FakeProton_OSpmu", prescale=0.001
):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons_reverse_pid)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pkes = btosetau_exclusive.make_pke(
        kaons,
        protons,
        electrons,
        pvs,
        decay_descriptor="[B0 -> p+ K- e-]cc",
        name="rd_pk_fake_proton_electron_for_btosetau_{hash}",
    )
    lb = btosmutau_exclusive.make_lb(
        pkes,
        muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu+]cc",
        name="rd_make_lb_to_pketau_fake_protons_{hash}",
    )
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={"Lb": lb, "pKe": pkes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lbtopketau_tautomu_fakekaon_sspmu_line(
    name="Hlt2RD_LbToPKTauE_TauToMu_FakeKaon_SSpmu", prescale=0.001
):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_reverse_pid)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pkes = btosetau_exclusive.make_pke(
        kaons,
        protons,
        electrons,
        pvs,
        decay_descriptor="[B0 -> p+ K- e+]cc",
        name="rd_pk_fake_kaon_electron_for_btosetau_{hash}",
    )
    lb = btosmutau_exclusive.make_lb(
        pkes,
        muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu-]cc",
        name="rd_make_lb_to_pketau_fake_kaons_{hash}",
    )
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={"Lb": lb, "pKe": pkes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lbtopketau_tautomu_fakekaon_ospmu_line(
    name="Hlt2RD_LbToPKTauE_TauToMu_FakeKaon_OSpmu", prescale=0.001
):
    pvs = make_pvs()
    protons = rdbuilder_thor.make_rd_detached_protons(**kwargs_protons)
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_reverse_pid)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    pkes = btosetau_exclusive.make_pke(
        kaons,
        protons,
        electrons,
        pvs,
        decay_descriptor="[B0 -> p+ K- e-]cc",
        name="rd_pk_fake_kaon_electron_for_btosetau_{hash}",
    )
    lb = btosmutau_exclusive.make_lb(
        pkes,
        muons_from_tau,
        pvs,
        decay_descriptor="[Lambda_b0 -> B0 mu+]cc",
        name="rd_make_lb_to_pketau_fake_kaons_{hash}",
    )
    algs = rd_prefilter() + [pkes, lb]

    iso_parts = parent_and_children_isolation(
        parents={"Lb": lb, "pKe": pkes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bdtokstetau_tautomu_sskmu_line(name="Hlt2RD_BdToKPiTauE_TauToMu_SSKmu", prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    kstes = btosetau_exclusive.make_kste(
        kaons, pions, electrons, pvs, decay_descriptor="[B0 -> K+ pi- e+]cc"
    )
    bd = btosmutau_exclusive.make_bd(
        kstes, muons_from_tau, pvs, decay_descriptor="[B_s0 -> B0 mu-]cc"
    )
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={"Bd": bd, "Kstes": kstes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bdtokstetau_tautomu_oskmu_line(name="Hlt2RD_BdToKPiTauE_TauToMu_OSKmu", prescale=1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    kstes = btosetau_exclusive.make_kste(
        kaons, pions, electrons, pvs, decay_descriptor="[B0 -> K+ pi- e-]cc"
    )
    bd = btosmutau_exclusive.make_bd(
        kstes, muons_from_tau, pvs, decay_descriptor="[B_s0 -> B0 mu+]cc"
    )
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={"Bd": bd, "Kstes": kstes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bdtokstetau_tautomu_same_sign_kpi_sskmu_line(
    name="Hlt2RD_BdToKPiTauE_TauToMu_SSKpi_SSKmu", prescale=0.001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    kstes = btosetau_exclusive.make_kste(
        kaons,
        pions,
        electrons,
        pvs,
        decay_descriptor="[B0 -> K+ pi+ e+]cc",
        name="rd_same_sign_kpi_electron_for_btosetau_{hash}",
    )
    bd = btosmutau_exclusive.make_bd(
        kstes,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu-]cc",
        name="rd_make_bd_to_kpietau_same_sign_kpi_{hash}",
    )
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={"Bd": bd, "Kstes": kstes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bdtokstetau_tautomu_same_sign_kpi_oskmu_line(
    name="Hlt2RD_BdToKPiTauE_TauToMu_SSKpi_OSKmu", prescale=0.001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    kstes = btosetau_exclusive.make_kste(
        kaons,
        pions,
        electrons,
        pvs,
        decay_descriptor="[B0 -> K+ pi+ e-]cc",
        name="rd_same_sign_kpi_electron_for_btosetau_{hash}",
    )
    bd = btosmutau_exclusive.make_bd(
        kstes,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu+]cc",
        name="rd_make_bd_to_kpietau_same_sign_kpi_{hash}",
    )
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={"Bd": bd, "Kstes": kstes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bdtokstetau_tautomu_same_sign_muons_sskmu_line(
    name="Hlt2RD_BdToKPiTauE_TauToMu_SSl_SSKmu", prescale=0.001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    kstes = btosetau_exclusive.make_kste(
        kaons, pions, electrons, pvs, decay_descriptor="[B0 -> K+ pi- e+]cc"
    )
    bd = btosmutau_exclusive.make_bd(
        kstes,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu+]cc",
        name="rd_make_bd_to_kpietau_same_sign_leptons_{hash}",
    )
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={"Bd": bd, "Kstes": kstes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bdtokstetau_tautomu_same_sign_muons_oskmu_line(
    name="Hlt2RD_BdToKPiTauE_TauToMu_SSl_OSKmu", prescale=0.001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    kstes = btosetau_exclusive.make_kste(
        kaons, pions, electrons, pvs, decay_descriptor="[B0 -> K+ pi- e-]cc"
    )
    bd = btosmutau_exclusive.make_bd(
        kstes,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu-]cc",
        name="rd_make_bd_to_kpietau_same_sign_leptons_{hash}",
    )
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={"Bd": bd, "Kstes": kstes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bdtokstetau_tautomu_fakemuon_from_tau_sskmu_line(
    name="Hlt2RD_BdToKPiTauE_TauToMu_FakeMuonFromTau_SSKmu", prescale=0.0001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    fake_muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau_reverse_pid
    )
    kstes = btosetau_exclusive.make_kste(
        kaons, pions, electrons, pvs, decay_descriptor="[B0 -> K+ pi- e+]cc"
    )
    bd = btosmutau_exclusive.make_bd(
        kstes,
        fake_muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu-]cc",
        name="rd_make_bd_to_kpietau_fake_muon_from_tau_{hash}",
    )
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={"Bd": bd, "Kstes": kstes},
        decay_products={"mu_f_tau": fake_muons_from_tau},
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bdtokstetau_tautomu_fakemuon_from_tau_oskmu_line(
    name="Hlt2RD_BdToKPiTauE_TauToMu_FakeMuonFromTau_OSKmu", prescale=0.0001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    fake_muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau_reverse_pid
    )
    kstes = btosetau_exclusive.make_kste(
        kaons, pions, electrons, pvs, decay_descriptor="[B0 -> K+ pi- e-]cc"
    )
    bd = btosmutau_exclusive.make_bd(
        kstes,
        fake_muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu+]cc",
        name="rd_make_bd_to_kpietau_fake_muon_from_tau_{hash}",
    )
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={"Bd": bd, "Kstes": kstes},
        decay_products={"mu_f_tau": fake_muons_from_tau},
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bdtokstetau_tautomu_fakemuon_from_b_sskmu_line(
    name="Hlt2RD_BdToKPiTauE_TauToMu_FakeMuonFromB_SSKmu", prescale=0.0001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    fake_electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_reverse_pid
    )
    kstes_fake_electron = btosetau_exclusive.make_kste(
        kaons,
        pions,
        fake_electrons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- e+]cc",
        name="rd_kpi_fake_electron_for_btosetau_{hash}",
    )
    bd = btosmutau_exclusive.make_bd(
        kstes_fake_electron,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu-]cc",
        name="rd_make_bd_to_kpietau_fake_electron_from_b_{hash}",
    )
    algs = rd_prefilter() + [kstes_fake_electron, bd]

    iso_parts = parent_and_children_isolation(
        parents={"Bd": bd, "Kstes": kstes_fake_electron},
        decay_products={"mu_f_tau": muons_from_tau},
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bdtokstetau_tautomu_fakemuon_from_b_oskmu_line(
    name="Hlt2RD_BdToKPiTauE_TauToMu_FakeMuonFromB_OSKmu", prescale=0.0001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    fake_electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_reverse_pid
    )
    kstes_fake_electron = btosetau_exclusive.make_kste(
        kaons,
        pions,
        fake_electrons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- e-]cc",
        name="rd_kpi_fake_electron_for_btosetau_{hash}",
    )
    bd = btosmutau_exclusive.make_bd(
        kstes_fake_electron,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu+]cc",
        name="rd_make_bd_to_kpietau_fake_electron_from_b_{hash}",
    )
    algs = rd_prefilter() + [kstes_fake_electron, bd]

    iso_parts = parent_and_children_isolation(
        parents={"Bd": bd, "Kstes": kstes_fake_electron},
        decay_products={"mu_f_tau": muons_from_tau},
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bdtokstetau_tautomu_fakekaon_sskmu_line(
    name="Hlt2RD_BdToKPiTauE_TauToMu_FakeKaon_SSKmu", prescale=0.001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar_reverse_pid)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    kstes = btosetau_exclusive.make_kste(
        kaons,
        pions,
        electrons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- e+]cc",
        name="rd_kpi_fake_kaon_electron_for_btosetau_{hash}",
    )
    bd = btosmutau_exclusive.make_bd(
        kstes,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu-]cc",
        name="rd_make_bd_to_kpietau_fake_kaons_{hash}",
    )
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={"Bd": bd, "Kstes": kstes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bdtokstetau_tautomu_fakekaon_oskmu_line(
    name="Hlt2RD_BdToKPiTauE_TauToMu_FakeKaon_OSKmu", prescale=0.001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar_reverse_pid)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    kstes = btosetau_exclusive.make_kste(
        kaons,
        pions,
        electrons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- e-]cc",
        name="rd_kpi_fake_kaon_electron_for_btosetau_{hash}",
    )
    bd = btosmutau_exclusive.make_bd(
        kstes,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu+]cc",
        name="rd_make_bd_to_kpietau_fake_kaons_{hash}",
    )
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={"Bd": bd, "Kstes": kstes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bdtokstetau_tautomu_fakepion_sskmu_line(
    name="Hlt2RD_BdToKPiTauE_TauToMu_FakePion_SSKmu", prescale=0.001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions_reverse_pid)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    kstes = btosetau_exclusive.make_kste(
        kaons,
        pions,
        electrons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- e+]cc",
        name="rd_kpi_fake_pion_electron_for_btosetau_{hash}",
    )
    bd = btosmutau_exclusive.make_bd(
        kstes,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu-]cc",
        name="rd_make_bd_to_kpietau_fake_pions_{hash}",
    )
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={"Bd": bd, "Kstes": kstes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bdtokstetau_tautomu_fakepion_oskmu_line(
    name="Hlt2RD_BdToKPiTauE_TauToMu_FakePion_OSKmu", prescale=0.001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_kstar)
    pions = rdbuilder_thor.make_rd_detached_pions(**kwargs_pions_reverse_pid)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons)
    kstes = btosetau_exclusive.make_kste(
        kaons,
        pions,
        electrons,
        pvs,
        decay_descriptor="[B0 -> K+ pi- e-]cc",
        name="rd_kpi_fake_pion_electron_for_btosetau_{hash}",
    )
    bd = btosmutau_exclusive.make_bd(
        kstes,
        muons_from_tau,
        pvs,
        decay_descriptor="[B_s0 -> B0 mu+]cc",
        name="rd_make_bd_to_kpietau_fake_pions_{hash}",
    )
    algs = rd_prefilter() + [kstes, bd]

    iso_parts = parent_and_children_isolation(
        parents={"Bd": bd, "Kstes": kstes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def butoketau_tautomu_oskmu_line(name="Hlt2RD_BuToKTauE_TauToMu_OSKmu", prescale=0.1):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons_for_bu)
    kes = btosetau_exclusive.make_ke(
        kaons, electrons, pvs, decay_descriptor="[B0 -> e- K+]cc"
    )
    bu = btosmutau_exclusive.make_bu(
        kes, muons_from_tau, pvs, decay_descriptor="[B+ -> B0 mu+]cc"
    )

    iso_parts = parent_and_children_isolation(
        parents={"Bu": bu, "Kes": kes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kes, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def butoketau_tautomu_sskmu_line(name="Hlt2RD_BuToKTauE_TauToMu_SSKmu", prescale=0.3):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons_for_bu)
    kes = btosetau_exclusive.make_ke(
        kaons, electrons, pvs, decay_descriptor="[B0 -> e+ K+]cc"
    )
    bu = btosmutau_exclusive.make_bu(
        kes, muons_from_tau, pvs, decay_descriptor="[B+ -> B0 mu-]cc"
    )

    iso_parts = parent_and_children_isolation(
        parents={"Bu": bu, "Kes": kes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kes, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def butoketau_tautomu_same_sign_muons_oskmu_line(
    name="Hlt2RD_BuToKTauE_TauToMu_SSl_OSKmu", prescale=0.01
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons_for_bu)
    kes = btosetau_exclusive.make_ke(
        kaons, electrons, pvs, decay_descriptor="[B0 -> e- K+]cc"
    )
    bu = btosmutau_exclusive.make_bu(
        kes,
        muons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 mu-]cc",
        name="rd_make_bu_to_ketau_same_sign_leptons_{hash}",
    )

    iso_parts = parent_and_children_isolation(
        parents={"Bu": bu, "Kes": kes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kes, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def butoketau_tautomu_same_sign_muons_sskmu_line(
    name="Hlt2RD_BuToKTauE_TauToMu_SSl_SSKmu", prescale=0.01
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons_for_bu)
    kes = btosetau_exclusive.make_ke(
        kaons, electrons, pvs, decay_descriptor="[B0 -> e- K-]cc"
    )
    bu = btosmutau_exclusive.make_bu(
        kes,
        muons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 mu-]cc",
        name="rd_make_bu_to_ketau_same_sign_leptons_{hash}",
    )

    iso_parts = parent_and_children_isolation(
        parents={"Bu": bu, "Kes": kes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kes, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def butoketau_tautomu_fakemuon_from_tau_oskmu_line(
    name="Hlt2RD_BuToKTauE_TauToMu_FakeMuonFromTau_OSKmu", prescale=0.0001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons_for_bu)
    fake_muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau_reverse_pid
    )
    kes = btosetau_exclusive.make_ke(
        kaons, electrons, pvs, decay_descriptor="[B0 -> e- K+]cc"
    )
    bu = btosmutau_exclusive.make_bu(
        kes,
        fake_muons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 mu+]cc",
        name="rd_make_bu_to_ketau_fake_muon_from_tau_{hash}",
    )
    algs = rd_prefilter() + [kes, bu]

    iso_parts = parent_and_children_isolation(
        parents={"Bu": bu, "Kes": kes}, decay_products={"mu_f_tau": fake_muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def butoketau_tautomu_fakemuon_from_tau_sskmu_line(
    name="Hlt2RD_BuToKTauE_TauToMu_FakeMuonFromTau_SSKmu", prescale=0.0001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons_for_bu)
    fake_muons_from_tau = rdbuilder_thor.make_rd_detached_muons(
        **kwargs_muons_from_tau_reverse_pid
    )
    kes = btosetau_exclusive.make_ke(
        kaons, electrons, pvs, decay_descriptor="[B0 -> e+ K+]cc"
    )
    bu = btosmutau_exclusive.make_bu(
        kes,
        fake_muons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 mu-]cc",
        name="rd_make_bu_to_ketau_fake_muon_from_tau_{hash}",
    )
    algs = rd_prefilter() + [kes, bu]

    iso_parts = parent_and_children_isolation(
        parents={"Bu": bu, "Kes": kes}, decay_products={"mu_f_tau": fake_muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def butoketau_tautomu_fakemuon_from_b_oskmu_line(
    name="Hlt2RD_BuToKTauE_TauToMu_FakeMuonFromB_OSKmu", prescale=0.0001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    fake_electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_for_bu_reverse_pid
    )
    kes_fake_electron = btosetau_exclusive.make_ke(
        kaons,
        fake_electrons,
        pvs,
        decay_descriptor="[B0 -> e- K+]cc",
        name="rd_kaon_fake_electron_for_btosetau_{hash}",
    )
    bu = btosmutau_exclusive.make_bu(
        kes_fake_electron,
        muons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 mu+]cc",
        name="rd_make_bu_to_ketau_fake_electron_from_b_{hash}",
    )
    algs = rd_prefilter() + [kes_fake_electron, bu]

    iso_parts = parent_and_children_isolation(
        parents={"Bu": bu, "Kes": kes_fake_electron},
        decay_products={"mu_f_tau": muons_from_tau},
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def butoketau_tautomu_fakemuon_from_b_sskmu_line(
    name="Hlt2RD_BuToKTauE_TauToMu_FakeMuonFromB_SSKmu", prescale=0.0001
):
    pvs = make_pvs()
    kaons = rdbuilder_thor.make_rd_detached_kaons(**kwargs_kaons_for_bu)
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    fake_electrons = rdbuilder_thor.make_rd_detached_electrons(
        **kwargs_electrons_for_bu_reverse_pid
    )
    kes_fake_electron = btosetau_exclusive.make_ke(
        kaons,
        fake_electrons,
        pvs,
        decay_descriptor="[B0 -> e+ K+]cc",
        name="rd_kaon_fake_electron_for_btosetau_{hash}",
    )
    bu = btosmutau_exclusive.make_bu(
        kes_fake_electron,
        muons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 mu-]cc",
        name="rd_make_bu_to_ketau_fake_electron_from_b_{hash}",
    )
    algs = rd_prefilter() + [kes_fake_electron, bu]

    iso_parts = parent_and_children_isolation(
        parents={"Bu": bu, "Kes": kes_fake_electron},
        decay_products={"mu_f_tau": muons_from_tau},
    )

    return Hlt2Line(
        name=name,
        algs=algs,
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def butoketau_tautomu_fakekaon_oskmu_line(
    name="Hlt2RD_BuToKTauE_TauToMu_FakeKaon_OSKmu", prescale=0.001
):
    pvs = make_pvs()
    fake_kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_for_bu_reverse_pid
    )
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons_for_bu)
    kes = btosetau_exclusive.make_ke(
        fake_kaons,
        electrons,
        pvs,
        decay_descriptor="[B0 -> e- K+]cc",
        name="rd_fake_kaon_electron_for_btosetau_{hash}",
    )
    bu = btosmutau_exclusive.make_bu(
        kes,
        muons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 mu+]cc",
        name="rd_make_bu_to_ketau_fake_kaons_{hash}",
    )

    iso_parts = parent_and_children_isolation(
        parents={"Bu": bu, "Kes": kes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kes, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def butoketau_tautomu_fakekaon_sskmu_line(
    name="Hlt2RD_BuToKTauE_TauToMu_FakeKaon_SSKmu", prescale=0.001
):
    pvs = make_pvs()
    fake_kaons = rdbuilder_thor.make_rd_detached_kaons(
        **kwargs_kaons_for_bu_reverse_pid
    )
    muons_from_tau = rdbuilder_thor.make_rd_detached_muons(**kwargs_muons_from_tau)
    electrons = rdbuilder_thor.make_rd_detached_electrons(**kwargs_electrons_for_bu)
    kes = btosetau_exclusive.make_ke(
        fake_kaons,
        electrons,
        pvs,
        decay_descriptor="[B0 -> e+ K+]cc",
        name="rd_fake_kaon_electron_for_btosetau_{hash}",
    )
    bu = btosmutau_exclusive.make_bu(
        kes,
        muons_from_tau,
        pvs,
        decay_descriptor="[B+ -> B0 mu-]cc",
        name="rd_make_bu_to_ketau_fake_kaons_{hash}",
    )

    iso_parts = parent_and_children_isolation(
        parents={"Bu": bu, "Kes": kes}, decay_products={"mu_f_tau": muons_from_tau}
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kes, bu],
        prescale=prescale,
        extra_outputs=iso_parts,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )
