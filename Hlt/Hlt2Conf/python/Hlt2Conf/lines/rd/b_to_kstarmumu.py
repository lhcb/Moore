###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of B -> K* Mu Mu HLT2 exclusive line"""

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf import rdbuilder_thor
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.rd.builders.rd_isolation import parent_isolation_output

from .builders import b0_builder_kstarmumu
from .builders.rd_prefilters import _VRD_MONITORING_VARIABLES, rd_prefilter

all_lines = {}


@register_line_builder(all_lines)
def BdToKstarMuMu_line(name="Hlt2RD_BdToKstarMuMu", prescale=1):
    pvs = make_pvs()
    kstars = rdbuilder_thor.make_rd_detached_kstar0s(
        k_pt_min=300.0, pi_pt_min=300.0, k_p_min=3.0, pi_p_min=3.0, vchi2pdof_max=20.0
    )
    jpsis = rdbuilder_thor.make_rd_detached_dimuon()
    b = b0_builder_kstarmumu.make_b(kstars, jpsis, pvs)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [b],
        prescale=prescale,
        extra_outputs=parent_isolation_output("B0", b),
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )
