###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""

Definition of some Baryon Number Violation lines:
    1) Bs -> P mu       Hlt2RD_BToPpMu
    2) Lz -> Pi mu      Hlt2RD_LambdaToPipMu
    3) Lz -> K mu       Hlt2RD_LambdaToKpMu
    4) Lb -> pi mu      Hlt2RD_LbToPipMu
    5) Lb -> K mu       Hlt2RD_LbToKpMu
    6) B -> Lc mu       Hlt2RD_BToLcMu_LcToPpKmPip
    7) Lb -> Ds mu      Hlt2RD_LbToDsMu_DsToKpKmPip

Notes:
    * For all channels two lines are defined, one with the right charge, one with wrong charge (SS)

    1) This is the HLT2 porting of the B2PMu Stripping line:
          https://gitlab.cern.ch/lhcb/Stripping/-/blob/2018-patches/Phys/StrippingSelections/python/StrippingSelections/StrippingRD/StrippingLFVLines.py#L2415
    2), 3) HLT2 porting of Lz2KMu Stripping line:
              https://gitlab.cern.ch/lhcb/Stripping/-/blob/2018-patches/Phys/StrippingSelections/python/StrippingSelections/StrippingRD/StrippingBLVLines.py#L234
    4), 5) HLT2 porting of Lb2KMu Stripping line:
              https://gitlab.cern.ch/lhcb/Stripping/-/blob/2018-patches/Phys/StrippingSelections/python/StrippingSelections/StrippingRD/StrippingBLVLines.py#L250


Author: Fabio De Vellis
Contact: fabio.de.vellis@cern.ch

"""

import Functors as F
from GaudiKernel.SystemOfUnits import MeV
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.rdbuilder_thor import (
    make_rd_detached_kaons,
    make_rd_detached_muons,
    make_rd_detached_pions,
    make_rd_detached_protons,
)
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.rd.builders.bnv_builders import (
    make_BToHMu,
    make_BToLcMu,
    make_DsToKKPi,
    make_LbToDsMu,
    make_LbToHMu,
    make_LcToPKPi,
    make_LzToHMu,
)
from Hlt2Conf.lines.rd.builders.rd_prefilters import (
    _VRD_MONITORING_VARIABLES,
    rd_prefilter,
)

all_lines = {}


# 1) Bs -> p mu
@register_line_builder(all_lines)
def BToPMu_line(name="Hlt2RD_BToPpMu", prescale=1 / 2.0):
    descriptor = "[B_s0 -> p+ mu-]cc"

    pvs = make_pvs()
    proton = make_rd_detached_protons(pt_min=500.0 * MeV, pid=(F.PID_P > 3))
    muon = make_rd_detached_muons(
        pt_min=500.0 * MeV, pid=F.require_all(F.ISMUON, F.PID_MU > 3)
    )

    B = make_BToHMu(pvs, proton, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [B],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def BToPMu_SS_line(name="Hlt2RD_BToPpMu_SS", prescale=1):
    descriptor = "[B_s0 -> p+ mu+]cc"

    pvs = make_pvs()
    proton = make_rd_detached_protons(pt_min=500.0 * MeV, pid=(F.PID_P > 3))
    muon = make_rd_detached_muons(
        pt_min=500.0 * MeV, pid=F.require_all(F.ISMUON, F.PID_MU > 3)
    )

    B = make_BToHMu(pvs, proton, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [B],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


# 2) Lz -> pi mu
@register_line_builder(all_lines)
def LambdaToPiMu_line(name="Hlt2RD_LambdaToPipMu", prescale=1):
    descriptor = "[Lambda0 -> pi+ mu-]cc"

    pvs = make_pvs()
    pion = make_rd_detached_pions(pt_min=500.0 * MeV, pid=(F.PID_K < 0.0))
    muon = make_rd_detached_muons(
        pt_min=500.0 * MeV, pid=F.require_all(F.ISMUON, F.PID_MU > 3.0)
    )

    Lz = make_LzToHMu(pvs, pion, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [Lz],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def LambdaToPiMu_SS_line(name="Hlt2RD_LambdaToPipMu_SS", prescale=1):
    descriptor = "[Lambda0 -> pi+ mu+]cc"

    pvs = make_pvs()
    pion = make_rd_detached_pions(pt_min=500.0 * MeV, pid=(F.PID_K < 0.0))
    muon = make_rd_detached_muons(
        pt_min=500.0 * MeV, pid=F.require_all(F.ISMUON, F.PID_MU > 3.0)
    )

    Lz = make_LzToHMu(pvs, pion, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [Lz],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


# 3) Lz -> K mu
@register_line_builder(all_lines)
def LambdaToKMu_line(name="Hlt2RD_LambdaToKpMu", prescale=1):
    descriptor = "[Lambda0 -> K+ mu-]cc"

    pvs = make_pvs()
    kaon = make_rd_detached_kaons(pt_min=500.0 * MeV, pid=(F.PID_K > 3.0))
    muon = make_rd_detached_muons(
        pt_min=500.0 * MeV, pid=F.require_all(F.ISMUON, F.PID_MU > 3.0)
    )

    Lz = make_LzToHMu(pvs, kaon, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [Lz],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def LambdaToKMu_SS_line(name="Hlt2RD_LambdaToKpMu_SS", prescale=1 / 2.0):
    descriptor = "[Lambda0 -> K+ mu+]cc"

    pvs = make_pvs()
    kaon = make_rd_detached_kaons(pt_min=500.0 * MeV, pid=(F.PID_K > 3.0))
    muon = make_rd_detached_muons(
        pt_min=500.0 * MeV, pid=F.require_all(F.ISMUON, F.PID_MU > 3.0)
    )

    Lz = make_LzToHMu(pvs, kaon, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [Lz],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


# 4) Lb -> pi mu
@register_line_builder(all_lines)
def LbToPiMu_line(name="Hlt2RD_LbToPipMu", prescale=1):
    descriptor = "[Lambda_b0 -> pi+ mu-]cc"

    pvs = make_pvs()
    pion = make_rd_detached_pions(pt_min=500.0 * MeV, pid=(F.PID_K < 0.0))
    muon = make_rd_detached_muons(
        pt_min=500.0 * MeV, pid=F.require_all(F.ISMUON, F.PID_MU > 3.0)
    )

    Lb = make_LbToHMu(pvs, pion, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [Lb],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def LbToPiMu_SS_line(name="Hlt2RD_LbToPipMu_SS", prescale=1):
    descriptor = "[Lambda_b0 -> pi+ mu+]cc"

    pvs = make_pvs()
    pion = make_rd_detached_pions(pt_min=500.0 * MeV, pid=(F.PID_K < 0.0))
    muon = make_rd_detached_muons(
        pt_min=500.0 * MeV, pid=F.require_all(F.ISMUON, F.PID_MU > 3.0)
    )

    Lb = make_LbToHMu(pvs, pion, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [Lb],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


# 5) Lb -> K mu
@register_line_builder(all_lines)
def LbToKMu_line(name="Hlt2RD_LbToKpMu", prescale=1):
    descriptor = "[Lambda_b0 -> K+ mu-]cc"

    pvs = make_pvs()
    kaon = make_rd_detached_kaons(pt_min=500.0 * MeV, pid=(F.PID_K > 3.0))
    muon = make_rd_detached_muons(
        pt_min=500.0 * MeV, pid=F.require_all(F.ISMUON, F.PID_MU > 3.0)
    )

    Lb = make_LbToHMu(pvs, kaon, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [Lb],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def LbToKMu_SS_line(name="Hlt2RD_LbToKpMu_SS", prescale=1):
    descriptor = "[Lambda_b0 -> K+ mu+]cc"

    pvs = make_pvs()
    kaon = make_rd_detached_kaons(pt_min=500.0 * MeV, pid=(F.PID_K > 3.0))
    muon = make_rd_detached_muons(
        pt_min=500.0 * MeV, pid=F.require_all(F.ISMUON, F.PID_MU > 3.0)
    )

    Lb = make_LbToHMu(pvs, kaon, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [Lb],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


# 6) B -> Lc mu
@register_line_builder(all_lines)
def BToLcMu_line(name="Hlt2RD_BToLcMu_LcToPpKmPip", prescale=1):
    descriptor = "[B0 -> Lambda_c+ mu-]cc"

    pvs = make_pvs()
    Lambda_c = make_LcToPKPi(
        pvs,
        make_rd_detached_protons(),
        make_rd_detached_kaons(),
        make_rd_detached_pions(),
        "[Lambda_c+ -> p+ K- pi+]cc",
    )
    muon = make_rd_detached_muons()

    B = make_BToLcMu(pvs, Lambda_c, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [Lambda_c, B],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def BToLcMu_SS_line(name="Hlt2RD_BToLcMu_SS_LcToPpKmPip", prescale=1):
    descriptor = "[B0 -> Lambda_c+ mu+]cc"

    pvs = make_pvs()
    Lambda_c = make_LcToPKPi(
        pvs,
        make_rd_detached_protons(),
        make_rd_detached_kaons(),
        make_rd_detached_pions(),
        "[Lambda_c+ -> p+ K- pi+]cc",
    )
    muon = make_rd_detached_muons()

    B = make_BToLcMu(pvs, Lambda_c, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [Lambda_c, B],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


# 7) Lb -> Ds mu
@register_line_builder(all_lines)
def LbToDsMu_line(name="Hlt2RD_LbToDsMu_DsToKpKmPip", prescale=1):
    descriptor = "[Lambda_b0 -> D_s+ mu-]cc"

    pvs = make_pvs()
    Ds = make_DsToKKPi(
        pvs, make_rd_detached_kaons(), make_rd_detached_pions(), "[D_s+ -> K+ K- pi+]cc"
    )
    muon = make_rd_detached_muons()

    Lb = make_LbToDsMu(pvs, Ds, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [Ds, Lb],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def LbToDsMu_SS_line(name="Hlt2RD_LbToDsMu_SS_DsToKpKmPip", prescale=1):
    descriptor = "[Lambda_b0 -> D_s+ mu+]cc"

    pvs = make_pvs()
    Ds = make_DsToKKPi(
        pvs, make_rd_detached_kaons(), make_rd_detached_pions(), "[D_s+ -> K+ K- pi+]cc"
    )
    muon = make_rd_detached_muons()

    Lb = make_LbToDsMu(pvs, Ds, muon, descriptor)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [Ds, Lb],
        prescale=prescale,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )
