###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of radiative inclusive builders"""

import math

import Functors as F
from Functors.math import in_range, log
from GaudiKernel.SystemOfUnits import GeV, MeV
from PyConf import configurable
from RecoConf import rdbuilder_thor
from RecoConf.algorithms_thor import (
    ParticleCombiner,
    ParticleContainersMerger,
    ParticleFilter,
)
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    _make_dielectron_with_brem,
    make_down_electrons_no_brem,
    make_long_electrons_no_brem,
)
from SelAlgorithms.monitoring import histogram_1d, monitor


def make_monitor(name, input, variable, HistogramName=None, Bins=100, Range=(-1, 1)):
    hist_name = HistogramName or name
    return monitor(
        name=name,
        data=input,
        histograms=[
            histogram_1d(
                name=hist_name,
                title=hist_name,
                functor=variable,
                bins=Bins,
                range=Range,
                label=hist_name,
            )
        ],
    )


####################################
# basic builders                   #
####################################
# Definition of inclusive_radiative_b basic particles: hadrons, photons, electrons


@configurable
def filter_electrons(
    particles,
    pvs,
    pt_min=0.1 * GeV,
    p_min=0 * GeV,
    pid_min=-2,
    name="rd_rad_incl_electrons_{hash}",
):
    """Returns electrons for inclusive_radiative_b selection"""
    code = F.require_all(F.PT > pt_min, F.P > p_min)
    if pid_min:
        code &= F.PID_E > pid_min
    return ParticleFilter(particles, F.FILTER(code), name=name)


@configurable
def filter_photons(
    particles,
    pvs,
    pt_min=2.0 * GeV,
    p_min=5.0 * GeV,
    IsPhoton_min=0.5,
    IsNotH_min=0.3,
    name="rd_rad_incl_photons_{hash}",
):
    """Returns photons for inclusive_radiative_b selection"""
    code = F.require_all(
        F.PT > pt_min, F.P > p_min, F.IS_PHOTON > IsPhoton_min, F.IS_NOT_H > IsNotH_min
    )
    return ParticleFilter(particles, F.FILTER(code), name=name)


@configurable
def filter_basic_hadrons(
    particles,
    pvs,
    pt_min=1.0 * GeV,
    pt_max=25.0 * GeV,
    ipchi2_min=7.4,
    trchi2_max=2.5,
    trgp_max=0.2,
    param1=1.0,
    param2=1.0,
    param3=1.1,
    name="rd_rad_incl_charged_hadrons_{hash}",
):
    """Returns basic hadrons with the inclusive_radiative_b selections"""
    difficult_cut = F.require_all(
        in_range(pt_min, F.PT, pt_max),
        log(F.BPVIPCHI2(pvs))
        > (
            param1 / ((F.PT / GeV - param2) ** 2)
            + (param3 / pt_max) * (pt_max - F.PT)
            + math.log(ipchi2_min)
        ),
    )  # mimicking track mva HLT1 cut, IP chi2 over a hyperbola-like function of PT

    code = F.require_all(
        # F.CHI2DOF < trchi2_max, # <- removed for the early data taking in 2022
        # F.GHOSTPROB < trgp_max, # <- removed for the early data taking in 2022
        ((F.PT > pt_max) & (F.BPVIPCHI2(pvs) > ipchi2_min)) | (difficult_cut),
    )

    return ParticleFilter(particles, F.FILTER(code), name=name)


@configurable
def filter_third_hadrons(
    particles,
    pvs,
    pt_min=0.2 * GeV,
    p_min=3.0 * GeV,
    trchi2_max=3.0,
    ipchi2_min=4.0,
    name="rd_rad_incl_third_hadrons_{hash}",
):
    """Returns third hadron used in HHH inclusive_radiative_b selections"""
    code = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        # F.CHI2DOF < trchi2_max, # <- removed for the early data taking in 2022
        F.MINIPCHI2CUT(IPChi2Cut=ipchi2_min, Vertices=pvs),
    )
    return ParticleFilter(particles, F.FILTER(code), name=name)


@configurable
def filter_neutral_hadrons(
    particles,
    pvs,
    pt_min=0.5 * GeV,
    ipchi2_min=0.0,
    name="rd_rad_incl_neutral_hadrons_{hash}_{hash}",
):
    """Returns extra neutral hadrons with the inclusive_radiative_b selections"""
    code = F.require_all(
        F.PT > pt_min,
        F.MINIPCHI2CUT(IPChi2Cut=ipchi2_min, Vertices=pvs),
    )
    return ParticleFilter(particles, F.FILTER(code), name=name)


@configurable
def make_KsLL(
    mass_window=20.0 * MeV,
    pt_min=1.0 * GeV,
    ipchi2_min=9.0,
    vchi2pdof_max=9.0,
    name="rd_rad_incl_ksll_{hash}",
):
    rd_ks0_lls = rdbuilder_thor.make_rd_ks0_lls()
    pvs = make_pvs()
    code = F.require_all(
        F.ABS_DELTA_MASS("KS0") < mass_window,
        F.PT > pt_min,
        F.MINIPCHI2CUT(IPChi2Cut=ipchi2_min, Vertices=pvs),
        F.CHI2DOF < vchi2pdof_max,
    )
    return ParticleFilter(rd_ks0_lls, F.FILTER(code), name=name)


@configurable
def make_LambdaLL(
    mass_window=20.0 * MeV,
    pt_min=1.0 * GeV,
    ipchi2_min=9.0,
    vchi2pdof_max=9.0,
    name="rd_rad_incl_l0ll_{hash}",
):
    rd_lambda_lls = rdbuilder_thor.make_rd_lambda_lls()
    pvs = make_pvs()
    code = F.require_all(
        F.ABS_DELTA_MASS("Lambda0") < mass_window,
        F.PT > pt_min,
        F.MINIPCHI2CUT(IPChi2Cut=ipchi2_min, Vertices=pvs),
        F.CHI2DOF < vchi2pdof_max,
    )
    return ParticleFilter(rd_lambda_lls, F.FILTER(code), name=name)


####################################
# hh builder                       #
####################################
# Definition of HH builder for inclusive_radiative_b selections
@configurable
def make_hh(
    inputs,
    pvs,
    descriptors,
    pt_min=2.0 * GeV,
    doca_chi2_max=9.0,
    m_max=10.0 * GeV,
    vtx_chi2_max=9.0,
    bpvfdchi2_min=16.0,
    eta_min=2.0,
    eta_max=5.0,
    name="rd_rad_incl_hh_{hash}",
):
    """Builds two-hadron particle for inclusive_radiative_b"""
    combination_code = F.require_all(
        F.PT > pt_min,
        F.MAXSDOCACHI2CUT(doca_chi2_max),
        F.MASS < m_max,
    )

    vertex_code = F.require_all(
        F.CHI2 < vtx_chi2_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        in_range(eta_min, F.BPVETA(pvs), eta_max),
    )

    hh = []
    for input, descriptor in zip(
        inputs,
        descriptors,
    ):
        subname = "rd_rad_incl_K*_{hash}"
        if "K+ K-" in descriptor:
            subname = "rd_rad_incl_K*0ToK+K-_{hash}"
        if "K+ KS0" in descriptor:
            subname = "rd_rad_incl_K*+ToK+KS0_{hash}"
        if "K+ Lambda0" in descriptor:
            subname = "rd_rad_incl_K*+ToK+Lambda0_{hash}"
        if "K- Lambda0" in descriptor:
            subname = "rd_rad_incl_K*-ToK-Lambda0_{hash}"
        hh.append(
            ParticleCombiner(
                input,
                DecayDescriptor=descriptor,
                CombinationCut=combination_code,
                CompositeCut=vertex_code,
                name=subname,
            )
        )

    return ParticleContainersMerger(hh, name=name)


####################################
# hhh builder                      #
####################################
# Definition of (HH)H builder for inclusive_radiative_b selections.
@configurable
def make_hhh(
    inputs,
    pvs,
    descriptors,
    pt_min=2.0 * GeV,
    doca_chi2_max=9.0,
    m_max=10.0 * GeV,
    vtx_chi2_max=9.0,
    bpvfdchi2_min=16.0,
    eta_min=2.0,
    eta_max=5.0,
    dira_min=0.0,
    name="rd_rad_incl_hhh_{hash}",
):
    """Builds HHH particle as HH + H for inclusive_radiative_b"""
    combination_code = F.require_all(
        F.PT > pt_min,
        F.MAXSDOCACHI2CUT(doca_chi2_max),
        F.MASS < m_max,
    )

    vertex_code = F.require_all(
        F.CHI2 < vtx_chi2_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        in_range(eta_min, F.BPVETA(pvs), eta_max),
        F.BPVDIRA(pvs) > dira_min,
    )

    hhh = []
    for input, descriptor in zip(inputs, descriptors):
        subname = "rd_rad_incl_D*_{hash}"
        if "K*(892)0 K+" in descriptor:
            subname = "rd_rad_incl_D*ToK*0K+_{hash}"
        if "K*(892)0 K-" in descriptor:
            subname = "rd_rad_incl_D*ToK*0K-_{hash}"
        if "K*(892)0 KS0" in descriptor:
            subname = "rd_rad_incl_D*ToK*0KS0_{hash}"
        if "K*(892)+ KS0" in descriptor:
            subname = "rd_rad_incl_D*ToK*+KS0_{hash}"
        if "K*(892)0 Lambda0" in descriptor:
            subname = "rd_rad_incl_D*ToK*0Lambda0_{hash}"
        if "K*(892)0 Lambda~0" in descriptor:
            subname = "rd_rad_incl_D*ToK*0Lambda~0_{hash}"
        if "K*(892)+ Lambda0" in descriptor:
            subname = "rd_rad_incl_D*ToK*+Lambda0_{hash}"
        if "K*(892)+ Lambda~0" in descriptor:
            subname = "rd_rad_incl_D*ToK*+Lambda~0_{hash}"
        hhh.append(
            ParticleCombiner(
                input,
                DecayDescriptor=descriptor,
                CombinationCut=combination_code,
                CompositeCut=vertex_code,
                name=subname,
            )
        )
    return ParticleContainersMerger(hhh, name=name)


####################################
# Converted photons                #
####################################
# Definition of inclusive_radiative_b gamma->ee builder


@configurable
def make_gamma_eeLL(
    pvs,
    pt_min=1.5 * GeV,
    p_min=0.0 * GeV,
    m_max=50.0 * MeV,
    vchi2pdof_max=10,
    pid_e_min=-2,
    max_pid_e_min=None,
    pt_e_min=100.0 * MeV,
    p_e_min=1.0 * GeV,
    name="rd_rad_incl_converted_photons_LL_{hash}",
):
    code_e = F.require_all(F.PT > pt_e_min, F.P > p_e_min)
    if pid_e_min:
        code_e &= F.PID_E > pid_e_min
    electrons = ParticleFilter(
        make_long_electrons_no_brem(), F.FILTER(code_e), name=name + "_electrons"
    )

    dielectron_with_brem = _make_dielectron_with_brem(
        electrons,
        pt_diE=pt_min,
        m_diE_min=0.0 * MeV,
        m_diE_max=m_max,
        m_diE_ID="gamma",
    )

    code_dielectron = F.require_all(
        F.P > p_min,
        F.CHI2DOF < vchi2pdof_max,
    )
    if max_pid_e_min:
        code_dielectron &= F.require_any(
            F.CHILD(1, F.PID_E) > max_pid_e_min, F.CHILD(2, F.PID_E) > max_pid_e_min
        )

    return ParticleFilter(dielectron_with_brem, F.FILTER(code_dielectron), name=name)


@configurable
def make_gamma_eeDD(
    pvs,
    pt_min=1.5 * GeV,
    p_min=0.0 * GeV,
    m_max=100.0 * MeV,
    vchi2pdof_max=10,
    pid_e_min=-2,
    max_pid_e_min=None,
    pt_e_min=100.0 * MeV,
    p_e_min=1.0 * GeV,
    name="rd_rad_incl_converted_photons_DD_{hash}",
):
    code_e = F.require_all(F.PT > pt_e_min, F.P > p_e_min)
    if pid_e_min:
        code_e &= F.PID_E > pid_e_min
    electrons = ParticleFilter(
        make_down_electrons_no_brem(), F.FILTER(code_e), name=name + "_electrons"
    )

    dielectron_with_brem = _make_dielectron_with_brem(
        electrons,
        pt_diE=pt_min,
        m_diE_min=0.0 * MeV,
        m_diE_max=m_max,
        m_diE_ID="gamma",
    )

    code_dielectron = F.require_all(
        F.P > p_min,
        F.CHI2DOF < vchi2pdof_max,
    )
    if max_pid_e_min:
        code_dielectron &= F.require_any(
            F.CHILD(1, F.PID_E) > max_pid_e_min, F.CHILD(2, F.PID_E) > max_pid_e_min
        )

    return ParticleFilter(dielectron_with_brem, F.FILTER(code_dielectron), name=name)


####################################
# B builder                        #
####################################
# Definition of HH builder for inclusive_radiative_b selections


@configurable
def make_presel_b(
    inputs,
    pvs,
    descriptors,
    pt_min=2.0 * GeV,
    eta_min=2.0,
    eta_max=5.0,
    corrm_min=2.5 * GeV,
    corrm_max=11.0 * GeV,
    dira_min=0.0,
    bpvvdchi2_min=0.0,
    name="rd_rad_incl_presel_B_{hash}",
):
    """Builds B->X gamma for inclusive_radiative_b selection"""
    combination_code = F.require_all(
        F.PT > pt_min,
        F.MASS < corrm_max,
    )

    vertex_code = F.require_all(
        in_range(eta_min, F.BPVETA(pvs), eta_max),
        in_range(corrm_min, F.BPVCORRM(pvs), corrm_max),
        F.BPVDIRA(pvs) > dira_min,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
    )

    presel_b = []
    for descriptor in descriptors:
        subname = "rd_rad_incl_preselB_{hash}"
        if "K*(892)0 gamma" in descriptor:
            subname = "rd_rad_incl_preselB0ToK*0Gamma_{hash}"
        if "K*(892)+ gamma" in descriptor:
            subname = "rd_rad_incl_preselB+ToK*+Gamma_{hash}"
        if "D*(2007)0 gamma" in descriptor:
            subname = "rd_rad_incl_preselB0ToD*0Gamma_{hash}"
        if "D*(2010)+ gamma" in descriptor:
            subname = "rd_rad_incl_preselB+ToD*+Gamma_{hash}"
        presel_b.append(
            ParticleCombiner(
                inputs,
                DecayDescriptor=descriptor,
                CombinationCut=combination_code,
                CompositeCut=vertex_code,
                name=subname,
            )
        )

    return ParticleContainersMerger(presel_b, name=name)
