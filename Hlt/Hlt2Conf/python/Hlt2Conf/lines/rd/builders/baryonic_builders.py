###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from Functors import require_all
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner
from RecoConf.rdbuilder_thor import (
    make_rd_detached_down_kaons,
    make_rd_detached_down_pions,
    make_rd_detached_kaons,
    make_rd_detached_pions,
    make_rd_has_rich_detached_pions,
    make_rd_lambda_dds,
    make_rd_lambda_lls,
)


@configurable
def make_dipion_for_lb(
    pi_pt_min=350.0 * MeV,
    pi_ipchi2_min=4.0,
    pi_pid=(F.PID_K < -1.0),
    pt_min=1000.0 * MeV,
    am_min=240.0 * MeV,
    am_max=3500.0 * MeV,
    adocachi2cut=8.0,
    vchi2pdof_max=9.0,
):
    """
    Build pi+pi- candidates, based on make_rd_detached_rho0
    """
    pions = make_rd_has_rich_detached_pions(
        pt_min=pi_pt_min,
        mipchi2dvprimary_min=pi_ipchi2_min,
        pid=pi_pid,
    )
    DecayDescriptor = "rho(770)0 -> pi+ pi-"
    combination_cut = require_all(
        in_range(am_min, F.MASS, am_max), F.PT > pt_min, F.MAXSDOCACHI2CUT(adocachi2cut)
    )

    composite_cut = require_all(F.CHI2DOF < vchi2pdof_max)
    return ParticleCombiner(
        [pions, pions],
        DecayDescriptor=DecayDescriptor,
        CombinationCut=combination_cut,
        CompositeCut=composite_cut,
    )


@configurable
def make_xim_to_lambda_pi(
    name,
    lambdas,
    pions,
    pvs,
    comb_m_min=1260 * MeV,
    comb_m_max=1390 * MeV,
    m_min=1272 * MeV,
    m_max=1372 * MeV,
    comb_p_min=9.0 * GeV,
    p_min=9.5 * GeV,
    docachi2_max=25.0,
    vchi2pdof_max=20.0,
    bpvvdz_min=1 * mm,
    bpvdira_min=None,
):
    """
    Make Xi- -> Lambda pi-.
    """
    comb_cut = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.P > comb_p_min,
        F.MAXDOCACHI2CUT(docachi2_max),
    )
    vertex_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.P > p_min,
        F.CHI2DOF < vchi2pdof_max,
        F.BPVVDZ(pvs) > bpvvdz_min,
    )
    if bpvdira_min is not None:
        vertex_cut &= F.require_all(F.BPVDIRA(pvs) > bpvdira_min)

    return ParticleCombiner(
        [lambdas, pions],
        name=name,
        DecayDescriptor="[Xi- -> Lambda0 pi-]cc",
        CombinationCut=comb_cut,
        CompositeCut=vertex_cut,
    )


@configurable
def make_omegam_to_lambda_k(
    name,
    lambdas,
    kaons,
    pvs,
    comb_m_min=1612 * MeV,
    comb_m_max=1732 * MeV,
    m_min=1622 * MeV,
    m_max=1722 * MeV,
    comb_p_min=9.0 * GeV,
    p_min=9.5 * GeV,
    docachi2_max=25.0,
    vchi2pdof_max=20.0,
    bpvvdz_min=0.5 * mm,
    bpvdira_min=None,
):
    """
    Make Omega- -> Lambda K-.
    """
    comb_cut = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.P > comb_p_min,
        F.MAXDOCACHI2CUT(docachi2_max),
    )
    vertex_cut = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.P > p_min,
        F.CHI2DOF < vchi2pdof_max,
        F.BPVVDZ(pvs) > bpvvdz_min,
    )
    if bpvdira_min is not None:
        vertex_cut &= F.require_all(F.BPVDIRA(pvs) > bpvdira_min)

    return ParticleCombiner(
        [lambdas, kaons],
        name=name,
        DecayDescriptor="[Omega- -> Lambda0 K-]cc",
        CombinationCut=comb_cut,
        CompositeCut=vertex_cut,
    )


def make_xim_to_lambda_pi_lll(
    name,
    pvs,
    comb_m_min=1260 * MeV,
    comb_m_max=1390 * MeV,
    m_min=1272 * MeV,
    m_max=1372 * MeV,
    comb_p_min=9.0 * GeV,
    p_min=9.5 * GeV,
    docachi2_max=25.0,
    vchi2pdof_max=16.0,
    bpvvdz_min=1 * mm,
    bpvdira_min=None,
    lambda_mass_window=35.0 * MeV,
    lambda_vchi2pdof_max=25.0,
    lambda_bpvvdchi2_min=4.0,
    lambda_pt_min=0.0,
    pion_p_min=0.0,
    pion_pt_min=0.0,
    pion_mipchi2dvprimary_min=9.0,
    pion_PID=None,
):
    lambdas = make_rd_lambda_lls(
        mass_window=lambda_mass_window,
        vchi2pdof_max=lambda_vchi2pdof_max,
        bpvvdchi2_min=lambda_bpvvdchi2_min,
        lambda_pt_min=lambda_pt_min,
    )
    pions = make_rd_detached_pions(
        p_min=pion_p_min,
        pt_min=pion_pt_min,
        mipchi2dvprimary_min=pion_mipchi2dvprimary_min,
        pid=pion_PID,
    )
    return make_xim_to_lambda_pi(
        name,
        pvs=pvs,
        lambdas=lambdas,
        pions=pions,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        m_min=m_min,
        m_max=m_max,
        comb_p_min=comb_p_min,
        p_min=p_min,
        docachi2_max=docachi2_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bpvdira_min=bpvdira_min,
    )


def make_xim_to_lambda_pi_ddl(
    name,
    pvs,
    comb_m_min=1260 * MeV,
    comb_m_max=1390 * MeV,
    m_min=1272 * MeV,
    m_max=1372 * MeV,
    comb_p_min=9.0 * GeV,
    p_min=9.5 * GeV,
    docachi2_max=25.0,
    vchi2pdof_max=20.0,
    bpvvdz_min=1 * mm,
    bpvdira_min=None,
    lambda_mass_window=35.0 * MeV,
    lambda_vchi2pdof_max=18.0,
    lambda_bpvvdchi2_min=10.0,
    lambda_pt_min=0.0,
    pion_p_min=0.0,
    pion_pt_min=0.0,
    pion_mipchi2dvprimary_min=9.0,
    pion_PID=None,
):
    lambdas = make_rd_lambda_dds(
        mass_window=lambda_mass_window,
        vchi2pdof_max=lambda_vchi2pdof_max,
        bpvvdchi2_min=lambda_bpvvdchi2_min,
        lambda_pt_min=lambda_pt_min,
    )
    pions = make_rd_detached_pions(
        p_min=pion_p_min,
        pt_min=pion_pt_min,
        mipchi2dvprimary_min=pion_mipchi2dvprimary_min,
        pid=pion_PID,
    )
    return make_xim_to_lambda_pi(
        name,
        pvs=pvs,
        lambdas=lambdas,
        pions=pions,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        m_min=m_min,
        m_max=m_max,
        comb_p_min=comb_p_min,
        p_min=p_min,
        docachi2_max=docachi2_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bpvdira_min=bpvdira_min,
    )


def make_xim_to_lambda_pi_ddd(
    name,
    pvs,
    comb_m_min=1260 * MeV,
    comb_m_max=1390 * MeV,
    m_min=1272 * MeV,
    m_max=1372 * MeV,
    comb_p_min=9.0 * GeV,
    p_min=9.5 * GeV,
    docachi2_max=25.0,
    vchi2pdof_max=20.0,
    bpvvdz_min=20 * mm,
    bpvdira_min=None,
    lambda_mass_window=35.0 * MeV,
    lambda_vchi2pdof_max=18.0,
    lambda_bpvvdchi2_min=4.0,
    lambda_pt_min=0.0,
    pion_p_min=0.0,
    pion_pt_min=0.0,
    pion_PID=None,
):
    lambdas = make_rd_lambda_dds(
        mass_window=lambda_mass_window,
        vchi2pdof_max=lambda_vchi2pdof_max,
        bpvvdchi2_min=lambda_bpvvdchi2_min,
        lambda_pt_min=lambda_pt_min,
    )
    pions = make_rd_detached_down_pions(
        p_min=pion_p_min, pt_min=pion_pt_min, pid=pion_PID
    )
    return make_xim_to_lambda_pi(
        name,
        pvs=pvs,
        lambdas=lambdas,
        pions=pions,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        m_min=m_min,
        m_max=m_max,
        comb_p_min=comb_p_min,
        p_min=p_min,
        docachi2_max=docachi2_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bpvdira_min=bpvdira_min,
    )


def make_omegam_to_lambda_k_lll(
    name,
    pvs,
    comb_m_min=1612 * MeV,
    comb_m_max=1732 * MeV,
    m_min=1622 * MeV,
    m_max=1722 * MeV,
    comb_p_min=9.0 * GeV,
    p_min=9.5 * GeV,
    docachi2_max=25.0,
    vchi2pdof_max=16.0,
    bpvvdz_min=0.5 * mm,
    bpvdira_min=None,
    lambda_mass_window=35.0 * MeV,
    lambda_vchi2pdof_max=18.0,
    lambda_bpvvdchi2_min=4.0,
    lambda_pt_min=0.0,
    kaon_p_min=0.0,
    kaon_pt_min=0.0,
    kaon_mipchi2dvprimary_min=9.0,
    kaon_PID=(F.PID_K > -2.0),
):
    lambdas = make_rd_lambda_lls(
        mass_window=lambda_mass_window,
        vchi2pdof_max=lambda_vchi2pdof_max,
        bpvvdchi2_min=lambda_bpvvdchi2_min,
        lambda_pt_min=lambda_pt_min,
    )
    kaons = make_rd_detached_kaons(
        p_min=kaon_p_min,
        pt_min=kaon_pt_min,
        mipchi2dvprimary_min=kaon_mipchi2dvprimary_min,
        pid=kaon_PID,
    )
    return make_omegam_to_lambda_k(
        name,
        pvs=pvs,
        lambdas=lambdas,
        kaons=kaons,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        m_min=m_min,
        m_max=m_max,
        comb_p_min=comb_p_min,
        p_min=p_min,
        docachi2_max=docachi2_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bpvdira_min=bpvdira_min,
    )


def make_omegam_to_lambda_k_ddl(
    name,
    pvs,
    comb_m_min=1612 * MeV,
    comb_m_max=1732 * MeV,
    m_min=1622 * MeV,
    m_max=1722 * MeV,
    comb_p_min=9.0 * GeV,
    p_min=9.5 * GeV,
    docachi2_max=25.0,
    vchi2pdof_max=20.0,
    bpvvdz_min=0.5 * mm,
    bpvdira_min=None,
    lambda_mass_window=35.0 * MeV,
    lambda_vchi2pdof_max=18.0,
    lambda_bpvvdchi2_min=10.0,
    lambda_pt_min=0.0,
    kaon_p_min=0.0,
    kaon_pt_min=0.0,
    kaon_mipchi2dvprimary_min=9.0,
    kaon_PID=(F.PID_K > -2.0),
):
    lambdas = make_rd_lambda_dds(
        mass_window=lambda_mass_window,
        vchi2pdof_max=lambda_vchi2pdof_max,
        bpvvdchi2_min=lambda_bpvvdchi2_min,
        lambda_pt_min=lambda_pt_min,
    )
    kaons = make_rd_detached_kaons(
        p_min=kaon_p_min,
        pt_min=kaon_pt_min,
        mipchi2dvprimary_min=kaon_mipchi2dvprimary_min,
        pid=kaon_PID,
    )
    return make_omegam_to_lambda_k(
        name,
        pvs=pvs,
        lambdas=lambdas,
        kaons=kaons,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        m_min=m_min,
        m_max=m_max,
        comb_p_min=comb_p_min,
        p_min=p_min,
        docachi2_max=docachi2_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bpvdira_min=bpvdira_min,
    )


def make_omegam_to_lambda_k_ddd(
    name,
    pvs,
    comb_m_min=1612 * MeV,
    comb_m_max=1732 * MeV,
    m_min=1622 * MeV,
    m_max=1722 * MeV,
    comb_p_min=9.0 * GeV,
    p_min=9.5 * GeV,
    docachi2_max=25.0,
    vchi2pdof_max=20.0,
    bpvvdz_min=20 * mm,
    bpvdira_min=None,
    lambda_mass_window=35.0 * MeV,
    lambda_vchi2pdof_max=18.0,
    lambda_bpvvdchi2_min=10.0,
    lambda_pt_min=0.0,
    kaon_p_min=0.0,
    kaon_pt_min=0.0,
    kaon_PID=(F.PID_K > -2.0),
):
    lambdas = make_rd_lambda_dds(
        mass_window=lambda_mass_window,
        vchi2pdof_max=lambda_vchi2pdof_max,
        bpvvdchi2_min=lambda_bpvvdchi2_min,
        lambda_pt_min=lambda_pt_min,
    )
    kaons = make_rd_detached_down_kaons(
        p_min=kaon_p_min, pt_min=kaon_pt_min, pid=kaon_PID
    )
    return make_omegam_to_lambda_k(
        name,
        pvs=pvs,
        lambdas=lambdas,
        kaons=kaons,
        comb_m_min=comb_m_min,
        comb_m_max=comb_m_max,
        m_min=m_min,
        m_max=m_max,
        comb_p_min=comb_p_min,
        p_min=p_min,
        docachi2_max=docachi2_max,
        vchi2pdof_max=vchi2pdof_max,
        bpvvdz_min=bpvvdz_min,
        bpvdira_min=bpvdira_min,
    )


@configurable
def make_bhadron(
    pvs,
    dilepton,
    hadron,
    descriptor,
    min_mass=4800 * MeV,
    max_mass=6900 * MeV,
    pt_min=2500 * MeV,
    docachi2_max=25.0,
    vchi2pdof_max=16.0,
    ipchi2_max=25.0,
    bpvvdchi2_min=40.0,
    dira_min=0.995,
    vtx_sep_min=None,
    daughter_index=2,
):
    """Builder for b-baryon decaying to light baryon and dilepton."""
    name = "RD_bbaryon_{hash}"
    combination_cut = F.require_all(
        in_range(min_mass - 100 * MeV, F.MASS, max_mass + 100 * MeV),
        F.PT > 0.9 * pt_min,
        F.MAXSDOCACHI2CUT(docachi2_max),
    )

    composite_cut = F.require_all(
        in_range(min_mass, F.MASS, max_mass),
        F.PT > pt_min,
        F.CHI2 < vchi2pdof_max,
        F.BPVDIRA(pvs) > dira_min,
        F.BPVIPCHI2(pvs) < ipchi2_max,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
    )

    # To be used only for lines with long-lived composite daughter (KS0, L0, Xi-, Omega-)
    if vtx_sep_min is not None:
        composite_cut &= F.CHILD(daughter_index, F.END_VZ) - F.END_VZ > vtx_sep_min

    return ParticleCombiner(
        [dilepton, hadron],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_cut,
        CompositeCut=composite_cut,
    )


@configurable
def make_bhadron_3body(
    pvs,
    dilepton,
    hadron1,
    hadron2,
    descriptor,
    min_mass=4800 * MeV,
    max_mass=6900 * MeV,
    pt_min=2500 * MeV,
    docachi2_max=25.0,
    vchi2pdof_max=16.0,
    ipchi2_max=25.0,
    bpvvdchi2_min=40.0,
    dira_min=0.995,
    vtx_sep_min=None,
    daughter_index=2,
):
    """Builder for b-baryon decaying to light baryon, one or two pions (dipion) and dilepton."""
    name = "RD_bbaryon_3body_{hash}"

    combination_cut = require_all(
        in_range(min_mass - 100 * MeV, F.MASS, max_mass + 100 * MeV),
        F.PT > 0.9 * pt_min,
        F.MAXSDOCACHI2CUT(docachi2_max),
    )

    composite_cut = require_all(
        in_range(min_mass, F.MASS, max_mass),
        F.PT > pt_min,
        F.CHI2 < vchi2pdof_max,
        F.BPVDIRA(pvs) > dira_min,
        F.BPVIPCHI2(pvs) < ipchi2_max,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
    )

    # To be used only for lines with long-lived composite daughter (KS0, L0, Xi-, Omega-)
    if vtx_sep_min is not None:
        composite_cut &= F.CHILD(daughter_index, F.END_VZ) - F.END_VZ > vtx_sep_min

    return ParticleCombiner(
        [dilepton, hadron1, hadron2],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_cut,
        CompositeCut=composite_cut,
    )


@configurable
def make_bhadron_4body(
    pvs,
    dilepton,
    hadron1,
    hadron2,
    hadron3,
    descriptor,
    min_mass=4800 * MeV,
    max_mass=6900 * MeV,
    pt_min=2500 * MeV,
    docachi2_max=25.0,
    vchi2pdof_max=16.0,
    ipchi2_max=25.0,
    bpvvdchi2_min=40.0,
    dira_min=0.995,
    vtx_sep_min=None,
    daughter_index=2,
):
    """Builder for b-hadron decaying to two hadrons, pion or two pions (dipion) and dilepton."""
    name = "RD_bbaryon_4body_{hash}"

    combination_cut = require_all(
        in_range(min_mass - 100 * MeV, F.MASS, max_mass + 100 * MeV),
        F.PT > 0.9 * pt_min,
        F.MAXSDOCACHI2CUT(docachi2_max),
    )

    composite_cut = require_all(
        in_range(min_mass, F.MASS, max_mass),
        F.PT > pt_min,
        F.CHI2 < vchi2pdof_max,
        F.BPVDIRA(pvs) > dira_min,
        F.BPVIPCHI2(pvs) < ipchi2_max,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
    )

    # To be used only for lines with long-lived composite daughter (KS0, L0, Xi-, Omega-)
    if vtx_sep_min is not None:
        composite_cut &= F.CHILD(daughter_index, F.END_VZ) - F.END_VZ > vtx_sep_min

    return ParticleCombiner(
        [dilepton, hadron1, hadron2, hadron3],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_cut,
        CompositeCut=composite_cut,
    )
