###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from Functors.math import in_range
from GaudiKernel.PhysicalConstants import c_light
from GaudiKernel.SystemOfUnits import MeV, micrometer
from PyConf import ConfigurationError, configurable
from RecoConf.algorithms_thor import ParticleCombiner
from RecoConf.reconstruction_objects import make_pvs


@configurable
def _make_tau_to_3body(
    particle1,
    particle2,
    particle3,
    descriptor,
    name="HLT2RD_TauTo3Body_Builder",
    make_pvs=make_pvs,
    pid_cut=(F.PID_MU > 0),
    min_particle_with_pid=0,
    min_mass12=0 * MeV,
    max_mass12=1978 * MeV,
    min_mass=1578 * MeV,
    max_mass=1978 * MeV,
    max_vtxchi2dof=9,
    min_flightdist=100 * micrometer,
    max_ipchi2=16,
):
    """
    Builder for tau decaying to three particles.
    """
    pvs = make_pvs()

    combination12_code = in_range(min_mass12, F.MASS, max_mass12)

    combination_code = F.require_all(
        in_range(min_mass, F.MASS, max_mass), F.SUM(pid_cut) >= min_particle_with_pid
    )

    vertex_code = F.require_all(
        F.CHI2DOF < max_vtxchi2dof,
        F.BPVIPCHI2(pvs) < max_ipchi2,
        F.BPVLTIME(pvs) * c_light > min_flightdist,
    )

    return ParticleCombiner(
        [particle1, particle2, particle3],
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def _make_tau_to_2body(
    particle1,
    particle2,
    descriptor,
    name="HLT2RD_TauTo2Body_Builder",
    make_pvs=make_pvs,
    pid_cut=(F.PID_MU > 0),
    min_particle_with_pid=0,
    min_mass=1578 * MeV,
    max_mass=1978 * MeV,
    max_vtxchi2dof=9,
    min_flightdist=100 * micrometer,
    max_ipchi2=16,
    min_pt=0.0 * MeV,
):
    """
    Builder for tau decaying to two particles.
    """
    pvs = make_pvs()

    combination_code = F.require_all(
        in_range(min_mass, F.MASS, max_mass), F.SUM(pid_cut) >= min_particle_with_pid
    )

    vertex_code = F.require_all(
        F.CHI2DOF < max_vtxchi2dof,
        F.BPVIPCHI2(pvs) < max_ipchi2,
        F.BPVLTIME(pvs) * c_light > min_flightdist,
        F.PT > min_pt,
    )

    return ParticleCombiner(
        [particle1, particle2],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def _make_tau_to_nmu(
    muons,
    muons_loose,
    descriptor,
    name="HLT2RD_TauToNMu_Builder",
    make_pvs=make_pvs,
    n_muons=5,
    min_mass=1378 * MeV,
    max_mass=2178 * MeV,
    max_vtxchi2dof=25,
    max_doca12=500 * micrometer,
    min_flightdist=100 * micrometer,
    max_ipchi2=16,
):
    """
    Builder for tau decaying to five, seven or nine muons with at least three
    muons fulfilling PID requirements
    """
    pvs = make_pvs()

    vertex_code = F.require_all(
        F.CHI2DOF < max_vtxchi2dof,
        F.BPVIPCHI2(pvs) < max_ipchi2,
        F.BPVLTIME(pvs) * c_light > min_flightdist,
    )
    combination_code = in_range(min_mass, F.MASS, max_mass)
    combination12_code = F.MAXDOCACUT(max_doca12)

    if n_muons == 5:
        return ParticleCombiner(
            [muons, muons, muons, muons_loose, muons_loose],
            name=name,
            DecayDescriptor=descriptor,
            Combination12Cut=combination12_code,
            CombinationCut=combination_code,
            CompositeCut=vertex_code,
        )
    elif n_muons == 7:
        return ParticleCombiner(
            [muons, muons, muons, muons, muons_loose, muons_loose, muons_loose],
            name=name,
            DecayDescriptor=descriptor,
            Combination12Cut=combination12_code,
            CombinationCut=combination_code,
            CompositeCut=vertex_code,
        )
    else:
        raise ConfigurationError("Parameter n_muons must be 5 or 7")


@configurable
def _make_tau_MVA_functor(line):
    """
    Return the MVA decision of a tau made of a mu with a gamma(->ee).

    Args:
        pvs (DataHandle): Input pvs.
        line (string): Name of the desired tau line.
    """

    pvs = make_pvs()

    tau_vars_thor = {
        "tau_PT": F.PT,
        "tau_BPVIPCHI2": F.BPVIPCHI2(pvs),
        "tau_CHI2DOF": F.CHI2DOF,
        "tau_BPVLTIME": F.BPVLTIME(pvs) * c_light,
    }

    mu_vars_thor = {
        "mu_MINIPCHI2": F.CHILD(1, F.MINIPCHI2(pvs)),
        "mu_PT": F.CHILD(1, F.PT),
        "mu_P": F.CHILD(1, F.P),
        "mu_PID_MU": F.CHILD(1, F.PID_MU),
    }

    gamma_vars_thor = {
        "gamma_M": F.CHILD(2, F.MASS),
        "gamma_MAXDOCACHI2": F.CHILD(2, F.MAXSDOCACHI2),
        "gamma_BPVFDCHI2": F.CHILD(2, F.BPVFDCHI2(pvs)),
    }

    ee_vars_thor = {
        "eplus_P": F.CHILD(2, F.CHILD(1, F.P)),
        "eplus_PT": F.CHILD(2, F.CHILD(1, F.PT)),
        "eplus_MINIPCHI2": F.CHILD(2, F.CHILD(1, F.MINIPCHI2(pvs))),
        "eminus_P": F.CHILD(2, F.CHILD(2, F.P)),
        "eminus_PT": F.CHILD(2, F.CHILD(2, F.PT)),
        "eminus_MINIPCHI2": F.CHILD(2, F.CHILD(2, F.MINIPCHI2(pvs))),
    }

    bdt_vars_thor = {
        "Hlt2RD_TauToMuGamma_EE_MVA": {
            **tau_vars_thor,
            **mu_vars_thor,
            **gamma_vars_thor,
            **ee_vars_thor,
        },
    }

    return F.MVA(
        MVAType="TMVA",
        Config={
            "XMLFile": "paramfile://data/%s.xml" % line,
            "Name": "BDT",
        },
        Inputs=bdt_vars_thor[line],
    )


@configurable
def _make_phi_to_2mu(
    muons,
    name="rd_PhiToMuMu_Builder_{hash}",
    parent_id="phi(1020)",
    pt_dimuon_min=0.0 * MeV,
    adocachi2cut_max=30.0,
    bpvvdchi2_min=30.0,
    vchi2pdof_max=30.0,
    am_min=950 * MeV,
    am_max=1100.0 * MeV,
):
    """
    Make the phi(1020) from the given muons.
    """
    DecayDescriptor = f"{parent_id} -> mu+ mu-"
    combination_code = F.require_all(
        in_range(am_min * 0.9, F.MASS, am_max * 1.1),
        F.PT > pt_dimuon_min,
        F.MAXDOCACHI2CUT(adocachi2cut_max),
    )
    pvs = make_pvs()
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        in_range(am_min, F.MASS, am_max),
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
    )

    return ParticleCombiner(
        [muons, muons],
        name=name,
        DecayDescriptor=DecayDescriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
