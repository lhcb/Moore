###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Hlt2 particle filters/combiners for the following:
exclusive B -> hh gamma builder

author: Izaac Sanderswood
date: 25.05.22

###
Exclusive builders for Bc
authors: Debashis Sahoo, Biplab Dey
date: 15.05.2024

"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner
from RecoConf.rdbuilder_thor import (
    make_rd_has_rich_detached_kaons,
    make_rd_has_rich_detached_protons,
)
from RecoConf.reconstruction_objects import make_pvs


@configurable
def make_b2xgamma_excl(
    intermediate,
    photons,
    pvs,
    descriptor,
    dira_min,
    name,
    comb_m_min=4200 * MeV,
    comb_m_max=6400 * MeV,
    pt_min=2000 * MeV,
    sum_pt_min=0 * MeV,
    bpv_ipchi2_max=12,
    bpv_fdchi2_min=0,
    vchi2pdof_max=None,
    eta_min=1.9,
    eta_max=5.1,
):
    combination_code = F.require_all(
        F.PT > pt_min,
        F.CHILD(1, F.SUM(F.PT)) + F.CHILD(2, F.PT) > sum_pt_min,
        in_range(comb_m_min, F.MASS, comb_m_max),
    )
    vertex_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.PT > pt_min,
        F.BPVDIRA(pvs) > dira_min,
        F.BPVFDCHI2(pvs) > bpv_fdchi2_min,
        F.BPVIPCHI2(pvs) < bpv_ipchi2_max,
        in_range(eta_min, F.BPVETA(pvs), eta_max),
    )
    if vchi2pdof_max:
        vertex_code &= F.CHI2DOF < vchi2pdof_max
    # return particle combiner
    return ParticleCombiner(
        name=name,
        Inputs=[intermediate, photons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_rd_detached_pK(
    name="rd_detached_pK_{hash}",
    make_rd_detached_protons=make_rd_has_rich_detached_protons,
    make_rd_detached_kaons=make_rd_has_rich_detached_kaons,
    make_pvs=make_pvs,
    am_min=1400 * MeV,
    am_max=2600.0 * MeV,
    p_p_min=2.0 * GeV,
    p_pt_min=500.0 * MeV,
    p_ipchi2_min=4.0,
    p_pid=(F.PID_P - F.PID_K > 5.0),
    k_p_min=2.0 * GeV,
    k_pt_min=500.0 * MeV,
    k_ipchi2_min=4.0,
    k_pid=(F.PID_K > -2.0),
    pK_pt_min=400.0 * MeV,
    adocachi2cut=30.0,
    vchi2pdof_max=25.0,
    bpvipchi2_min=None,
):
    protons = make_rd_detached_protons(
        p_min=p_p_min, pt_min=p_pt_min, mipchi2dvprimary_min=p_ipchi2_min, pid=p_pid
    )
    kaons = make_rd_detached_kaons(
        p_min=k_p_min, pt_min=k_pt_min, mipchi2dvprimary_min=k_ipchi2_min, pid=k_pid
    )
    descriptor = "[Lambda(1520)0 -> p+ K-]cc"
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.MAXSDOCACHI2CUT(adocachi2cut),
        F.PT > pK_pt_min,
    )
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max)

    if bpvipchi2_min is not None:
        pvs = make_pvs()
        vertex_code &= F.BPVIPCHI2(pvs) > bpvipchi2_min

    return ParticleCombiner(
        [protons, kaons],
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


@configurable
def make_PV(
    hadrons,
    kshort,
    decay_descriptor,
    name="rd_radexcl_PV_{hash}",
    vtxchi2dof_max=16.0,
    m_min=1000 * MeV,
    m_max=2000 * MeV,
    min_pt=1000 * MeV,
    ipchi2_min=9.0,
    fdchi2_min=0.0,
    dira_min=0.9,
):
    combination_code = F.require_all(in_range(m_min, F.MASS, m_max), F.PT > min_pt)
    pvs = make_pvs()
    vertex_code = F.require_all(
        F.CHI2DOF < vtxchi2dof_max,
        F.BPVIPCHI2(pvs) > ipchi2_min,
        F.BPVFDCHI2(pvs) > fdchi2_min,
        F.BPVDIRA(pvs) > dira_min,
    )

    return ParticleCombiner(
        name=name,
        Inputs=[hadrons, kshort],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def make_dzeros_for_rd_bc(particle1, particle2):
    descriptor = "[D0 -> K- pi+]cc"
    name = "rd_D0ToKmPip_{hash}"
    return ParticleCombiner(
        [particle1, particle2],
        DecayDescriptor=descriptor,
        name=name,
        CombinationCut=F.require_all(
            in_range(1700 * MeV, F.MASS, 2000 * MeV),
            F.MAX(F.PT) > 1200 * MeV,
            F.PT > 1 * GeV,
        ),
        CompositeCut=F.require_all(
            in_range(1764 * MeV, F.MASS, 1964 * MeV),
            F.CHI2DOF < 16.0,
            F.BPVFDCHI2(make_pvs()) > 25.0,
        ),
    )


def make_ds_for_rd_bc(particle1, particle2, particle3):
    descriptor = "[D_s+ -> K+ K- pi+]cc"
    name = "rd_DsToKpKmPip_{hash}"

    pvs = make_pvs()

    return ParticleCombiner(
        [particle1, particle2, particle3],
        DecayDescriptor=descriptor,
        name=name,
        CombinationCut=F.require_all(
            in_range(1910 * MeV, F.MASS, 2030 * MeV),
            F.MAX(F.PT) > 1000 * MeV,
            F.PT > 1 * GeV,
        ),
        CompositeCut=F.require_all(
            in_range(1920 * MeV, F.MASS, 2020 * MeV),
            F.CHI2DOF < 16.0,
            F.BPVDIRA(pvs) > 0.999,
        ),
    )


@configurable
def make_dsst(
    hadrons,
    kshort,
    decay_descriptor,
    name="rd_radexcl_PV_{hash}",
    m_min=1000 * MeV,
    m_max=2500 * MeV,
    min_pt=1000 * MeV,
):
    combination_code = F.require_all(in_range(m_min, F.MASS, m_max), F.PT > min_pt)

    return ParticleCombiner(
        ParticleCombiner="ParticleAdder",
        name=name,
        Inputs=[hadrons, kshort],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
    )


@configurable
def make_b2xgamma_excl_bc(
    intermediate,
    photons,
    descriptor,
    name,
    comb_m_min=5000 * MeV,
    comb_m_max=7400 * MeV,
    pt_min=2000 * MeV,
    sum_pt_min=0 * MeV,
):
    combination_code = F.require_all(
        F.PT > pt_min,
        F.CHILD(1, F.SUM(F.PT)) + F.CHILD(2, F.PT) > sum_pt_min,
        in_range(comb_m_min, F.MASS, comb_m_max),
    )

    return ParticleCombiner(
        ParticleCombiner="ParticleAdder",
        name=name,
        Inputs=[intermediate, photons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
    )
