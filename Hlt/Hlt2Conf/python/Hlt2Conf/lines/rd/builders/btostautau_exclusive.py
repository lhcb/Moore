###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of builders for the b -> s tau tau exclusive lines

Current builders:
 - make_phi: make combination of two kaons (which can be OS, SS, or with one kaon with reverse PID requirements)
 - make_kst: make combination of one kaon and one pion (which can be OS, SS, or with one hadron with reverse PID requirements)
 - make_lst: make combination of one proton and one kaon (which can be OS, SS, or with one hadron with reverse PID requirements)
 - make_dimuon: make combination of two muons (which can be OS, SS, or with one muon with reverse PID requirements)
 - make_bs: combine a dikaon and a dimuon
 - make_bu: combine a kaon and a dimuon
 - make_lb: combine a pK and a dimuon
 - make_bd: combine a Kpi and a dimuon
 - make_bs_to_kstkst: combine two kstars and one dimuon
Note: the dimuon here represents a combination of two muons, each coming from a tau

Author: H. Tilquin
Contact: hanae.tilquin@cern.ch
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV, mm
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner


@configurable
def make_phi(
    kaons1,
    kaons2,
    pvs,
    comb_m_min=950 * MeV,
    comb_m_max=1850 * MeV,
    comb_pt_min=1000 * MeV,
    vchi2pdof_max=3,
    bpvfdchi2_min=25,
    dira_min=0.9995,
    docachi2_max=2.5,
    doca_max=0.07 * mm,
    decay_descriptor="[phi(1020) -> K+ K-]cc",
    name="rd_dikaons_for_btostautau_{hash}",
):
    """Builder for X -> K K decays"""
    combination_code = in_range(comb_m_min, F.MASS, comb_m_max)
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.PT > comb_pt_min,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVDIRA(pvs) > dira_min,
        F.MAXSDOCACHI2CUT(docachi2_max),
        F.MAXSDOCACUT(doca_max),
    )
    return ParticleCombiner(
        [kaons1, kaons2],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        name=name,
    )


@configurable
def make_kst(
    kaons,
    pions,
    pvs,
    comb_m_min=750 * MeV,
    comb_m_max=1750 * MeV,
    comb_pt_min=600 * MeV,
    vchi2pdof_max=3,
    bpvfdchi2_min=25,
    dira_min=0.995,
    docachi2_max=2.5,
    doca_max=0.08 * mm,
    decay_descriptor="[K*(892)0 -> K+ pi-]cc",
    name="rd_kpi_for_btostautau_{hash}",
):
    """Builder for X -> K pi decays"""
    combination_code = in_range(comb_m_min, F.MASS, comb_m_max)
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.PT > comb_pt_min,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVDIRA(pvs) > dira_min,
        F.MAXSDOCACHI2CUT(docachi2_max),
        F.MAXSDOCACUT(doca_max),
    )
    return ParticleCombiner(
        [kaons, pions],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        name=name,
    )


@configurable
def make_lst(
    protons,
    kaons,
    pvs,
    comb_m_min=1400 * MeV,
    comb_m_max=2200 * MeV,
    comb_pt_min=1500 * MeV,
    vchi2pdof_max=2,
    bpvfdchi2_min=40,
    dira_min=0.9995,
    docachi2_max=2.5,
    doca_max=0.07 * mm,
    decay_descriptor="[Lambda(1520)0 -> p+ K-]cc",
    name="rd_pk_for_btostautau_{hash}",
):
    """Builder for X -> p K decays"""
    combination_code = in_range(comb_m_min, F.MASS, comb_m_max)
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.PT > comb_pt_min,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVDIRA(pvs) > dira_min,
        F.MAXSDOCACHI2CUT(docachi2_max),
        F.MAXSDOCACUT(doca_max),
    )
    return ParticleCombiner(
        [protons, kaons],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        name=name,
    )


def make_dimuon(
    muons1,
    muons2,
    pvs,
    comb_m_max=4500 * MeV,
    vchi2pdof_max=25,
    dira_min=0,
    docachi2_max=16.0,
    doca_max=0.18 * mm,
    decay_descriptor="[D0 -> mu+ mu-]cc",
    name="rd_dimuons_for_btostautau_{hash}",
):
    """Builder for detached dimuons"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(
        F.CHI2DOF < vchi2pdof_max,
        F.BPVDIRA(pvs) > dira_min,
        F.MAXSDOCACHI2CUT(docachi2_max),
        F.MAXSDOCACUT(doca_max),
    )
    return ParticleCombiner(
        [muons1, muons2],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        name=name,
    )


def make_dielectron(
    electrons1,
    electrons2,
    pvs,
    comb_m_max=4500 * MeV,
    vchi2pdof_max=16,
    dira_min=0,
    decay_descriptor="[D0 -> e+ e-]cc",
    name="rd_dielectrons_for_btostautau_{hash}",
):
    """Builder for detached dielectrons"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max, F.BPVDIRA(pvs) > dira_min)
    return ParticleCombiner(
        [electrons1, electrons2],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        name=name,
    )


def make_mue(
    muons,
    electrons,
    pvs,
    comb_m_max=4000 * MeV,
    vchi2pdof_max=20,
    dira_min=0,
    decay_descriptor="[D0 -> mu+ e-]cc",
    name="rd_mue_for_btostautau_{hash}",
):
    """Builder for detached mue"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max, F.BPVDIRA(pvs) > dira_min)
    return ParticleCombiner(
        [muons, electrons],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        name=name,
    )


def make_bs(
    phis,
    dileptons,
    pvs,
    comb_m_max=7500 * MeV,
    vchi2pdof_max=150,
    dira_min=0.999,
    name="rd_make_bs_to_kktautau_{hash}",
):
    """Builder for Bs -> K K tau (-> l nu nu) tau (-> l nu nu) decays"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max, F.BPVDIRA(pvs) > dira_min)
    return ParticleCombiner(
        [dileptons, phis],
        DecayDescriptor="[B_s0 -> D0 phi(1020)]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        name=name,
    )


@configurable
def make_bu(
    kaons,
    dileptons,
    pvs,
    comb_m_max=7250 * MeV,
    vchi2pdof_max=100,
    dira_min=0.999,
    name="rd_make_bu_to_ktautau_{hash}",
):
    """Builder for Bu -> K tau (-> l nu nu) tau (-> l nu nu) decays"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max, F.BPVDIRA(pvs) > dira_min)
    return ParticleCombiner(
        [dileptons, kaons],
        DecayDescriptor="[B+ -> D0 K+]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        name=name,
    )


def make_lb(
    pks,
    dileptons,
    pvs,
    comb_m_max=7750 * MeV,
    vchi2pdof_max=150,
    dira_min=0.999,
    name="rd_make_lb_to_pktautau_{hash}",
):
    """Builder for Lb -> p K tau (-> l nu nu) tau (-> l nu nu) decays"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max, F.BPVDIRA(pvs) > dira_min)
    return ParticleCombiner(
        [dileptons, pks],
        DecayDescriptor="[Lambda_b0 -> D0 Lambda(1520)0]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        name=name,
    )


def make_bd(
    ksts,
    dileptons,
    pvs,
    comb_m_max=7250 * MeV,
    vchi2pdof_max=100,
    dira_min=0.999,
    name="rd_make_bd_to_kpitautau_{hash}",
):
    """Builder for Bd -> K pi tau (-> l nu nu) tau (-> l nu nu) decays"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max, F.BPVDIRA(pvs) > dira_min)
    return ParticleCombiner(
        [dileptons, ksts],
        DecayDescriptor="[B0 -> D0 K*(892)0]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        name=name,
    )


def make_bs_to_kstkst(
    ksts,
    dileptons,
    pvs,
    comb_m_max=7000 * MeV,
    kstkst_m_max=3500 * MeV,
    vchi2pdof_max=150,
    dira_min=0.995,
    docachi2_max=25.0,
    decay_descriptor="[B_s0 -> K*(892)0 K*(892)~0 D0]cc",
    name="rd_make_bs_to_kstksttautau_{hash}",
):
    """Builder for Bs -> Kst Kst tau (-> l nu nu) tau (-> l nu nu) decays"""
    combination_code = F.MASS < comb_m_max
    two_body_combination_code = F.require_all(
        F.MASS < kstkst_m_max, F.MAXSDOCACHI2CUT(docachi2_max)
    )
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max, F.BPVDIRA(pvs) > dira_min)
    return ParticleCombiner(
        [ksts, ksts, dileptons],
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        Combination12Cut=two_body_combination_code,
        CompositeCut=vertex_code,
        name=name,
    )
