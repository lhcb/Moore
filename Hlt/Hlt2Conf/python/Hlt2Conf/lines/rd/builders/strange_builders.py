###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Builders for strange decays. In this file we define constructors for particles
originating from strange decays. We provide two sets of requirements for basic
particles, normal and tight, which are chosen for rare and more abundant
decay signatures, respectively. For combinations, we distinguish between
those in which the parent particle comes from a primary vertex, and those
in which the combination of the decay products do not point to any primary
vertex (tipically semileptonic decays). The mass bounds are centralized in
dedicated configurable functions in order to consistently modify lines where
the mass bounds are complementary.

Contact: Miguel Ramos Pernas (miguel.ramos.pernas@cern.ch)
"""

import collections
import functools

import Functors as F
import numpy
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, ns
from PyConf import configurable
from RecoConf import standard_particles
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import FunctionalDiElectronMaker

from Hlt2Conf.lines.rd.builders import rd_isolation

# Monitoring variables in different scenarios
MONITORING_VARIABLES_CONTROL_NO_VERTEX = ("m", "pt", "eta", "n_candidates")
MONITORING_VARIABLES_CONTROL = ("m", "pt", "eta", "vchi2", "ipchi2", "n_candidates")
MONITORING_VARIABLES_SEARCH = ("pt", "eta", "vchi2", "ipchi2", "n_candidates")
MONITORING_VARIABLES_SEARCH_NO_VERTEX = ("pt", "eta", "n_candidates")


def optional_functor(function):
    """Wrap a function so it returns None if the first argument is None"""

    def wrapper(value, *args):
        return None if value is None else function(value, *args)

    return wrapper


# empty requirements generator
NO_REQUIREMENTS = lambda: {}

# functors used for filtering
MIN_P = optional_functor(lambda v: F.P >= v)
MIN_PT = optional_functor(lambda v: F.PT >= v)
MAX_CHI2DOF = optional_functor(lambda v: F.CHI2DOF < v)
MAX_GHOSTPROB = optional_functor(lambda v: F.GHOSTPROB < v)
MAX_MINIP = optional_functor(lambda v, pvs: F.MINIP(pvs) < v)
MIN_MINIP = optional_functor(lambda v, pvs: F.MINIP(pvs) >= v)
MAX_MINIPCHI2 = optional_functor(lambda v, pvs: F.MINIPCHI2(pvs) < v)
MIN_MINIPCHI2 = optional_functor(lambda v, pvs: F.MINIPCHI2(pvs) >= v)
MIN_PID_MU = optional_functor(lambda v: F.PID_MU >= v)
MIN_PID_P = optional_functor(lambda v: F.PID_P >= v)
MIN_PID_P_OVER_K = optional_functor(lambda v: (F.PID_P - F.PID_K) >= v)
MIN_PID_K = optional_functor(lambda v: F.PID_K >= v)
MIN_PID_K_OVER_P = optional_functor(lambda v: (F.PID_K - F.PID_P) >= v)
MIN_PID_E = optional_functor(lambda v: F.PID_E >= v)
MAX_DOCA = optional_functor(lambda v: F.MAXSDOCACUT(v))
MAX_DOCACHI2 = optional_functor(lambda v: F.MAXSDOCACHI2CUT(v))
MIN_BPVVDZ = optional_functor(lambda v, pvs: F.BPVVDZ(pvs) >= v)
MAX_IPBPVVDZ_RATIO = optional_functor(lambda v, pvs: F.MINIP(pvs) / F.BPVVDZ(pvs) < v)
MIN_BPVVDRHO = optional_functor(lambda v, pvs: F.BPVVDRHO(pvs) >= v)
MIN_RHO = optional_functor(lambda v: F.END_VX**2 + F.END_VY**2 >= v**2)
MIN_BPVLTIME = optional_functor(lambda v, pvs: F.BPVLTIME(pvs) >= v)
MIN_DIRA = optional_functor(lambda v, pvs: F.BPVDIRA(pvs) >= v)
IS_MUON = optional_functor(lambda v: F.ISMUON if v else not F.ISMUON)
MIN_IS_NOT_H = optional_functor(lambda v: F.IS_NOT_H >= v)
MIN_IS_PHOTON = optional_functor(lambda v: F.IS_PHOTON >= v)
MAX_IS_PHOTON = optional_functor(lambda v: F.IS_PHOTON < v)
# needed due to https://gitlab.cern.ch/lhcb/MooreAnalysis/-/issues/36
# TODO: remove once the issue is solved
CHECK_PVS = lambda pvs: F.SIZE(pvs) > 0

child_requirement = collections.namedtuple("child_requirement", ["index", "value"])
children_comparison_requirement = collections.namedtuple(
    "children_comparison_requirement", ["first", "second", "value"]
)


def require_all_with_zip(functor, *args):
    """
    Utility function to apply a functor to a series of arguments processing
    both lists and scalars in one dimension. If both arguments are lists
    this is equivalent to applying the functor to the zip of those. If both
    are scalar then it evaluates to the functor applied to those arguments.
    If one is scalar and the other one is a list, the former is repeated
    to match the length of the latter. An error is raised if at any point
    the lengths of any of the lists mismatch.
    """
    # conver to a list so we can assign elements
    args = list(args)

    if all(map(lambda a: numpy.ndim(a) > 1, args)):
        # all the input values are scalars, so we don't need
        # to do anything special
        return functor(*args)

    # compute the size instead of the length to avoid problems
    # if some arguments are scalars
    sizes = numpy.fromiter(map(numpy.size, args), int)
    max_size = numpy.max(sizes)
    for i, a in enumerate(args):
        if numpy.size(a) != max_size:
            if numpy.ndim(a) > 0:
                raise ValueError(
                    "Mismatch of lengths when applying requirements on children"
                )
            else:
                # replace it with an array of the maximum size, using a list
                # because functors can only handle basic types and not NumPy types
                args[i] = numpy.full(max_size, a).tolist()
        else:
            pass

    return F.require_all(*(functor(*a) for a in zip(*args)))


_MIN_DISTANCE_TO_PARENT = (
    lambda index, value: (
        F.MAGNITUDE @ (F.CHILD(index, F.ENDVERTEX_POS) - F.ENDVERTEX_POS)
    )
    >= value
)
_MIN_DISTANCE_BETWEEN_CHILDREN = (
    lambda first, second, value: (
        F.MAGNITUDE
        @ (F.CHILD(second, F.ENDVERTEX_POS) - F.CHILD(first, F.ENDVERTEX_POS))
    )
    >= value
)

MIN_DISTANCE_TO_PARENT = optional_functor(
    lambda cfg: require_all_with_zip(_MIN_DISTANCE_TO_PARENT, *cfg)
)
MIN_DISTANCE_BETWEEN_CHILDREN = optional_functor(
    lambda cfg: require_all_with_zip(_MIN_DISTANCE_BETWEEN_CHILDREN, *cfg)
)


def used_functors(*args):
    """Filter optional functors"""
    return list(filter(lambda f: f is not None, args))


@configurable
def filter_decay_products(
    particles,
    *,
    p_min: float = None,
    pt_min: float = None,
    track_chi2dof_max: float = None,
    ghostprob_max: float = None,
    ip_max: float = None,
    ip_min: float = None,
    ipchi2_max: float = None,
    ipchi2_min: float = None,
    pid_mu_min: float = None,
    pid_p_min: float = None,
    pid_p_over_k_min: float = None,
    pid_k_min: float = None,
    pid_k_over_p_min: float = None,
    pid_e_min: float = None,
    is_not_h_min: float = None,
    is_photon_min: float = None,
    is_photon_max: float = None,
    is_muon: float = None,
    name: str,
):
    """Filter the products of strange decays"""
    pvs = make_pvs()
    selection = used_functors(
        CHECK_PVS(pvs),  # TODO: remove once MooreAnalysis#36 is fixed
        MIN_P(p_min),
        MIN_PT(pt_min),
        MAX_CHI2DOF(track_chi2dof_max),
        MAX_GHOSTPROB(ghostprob_max),
        MAX_MINIP(ip_max, pvs),
        MIN_MINIP(ip_min, pvs),
        MAX_MINIPCHI2(ipchi2_max, pvs),
        MIN_MINIPCHI2(ipchi2_min, pvs),
        IS_MUON(is_muon),
        MIN_PID_MU(pid_mu_min),
        MIN_PID_P(pid_p_min),
        MIN_PID_P_OVER_K(pid_p_over_k_min),
        MIN_PID_K(pid_k_min),
        MIN_PID_K_OVER_P(pid_k_over_p_min),
        MIN_PID_E(pid_e_min),
        MIN_IS_NOT_H(is_not_h_min),
        MIN_IS_PHOTON(is_photon_min),
        MAX_IS_PHOTON(is_photon_max),
    )
    return ParticleFilter(
        particles,
        F.FILTER(F.ALL if not selection else F.require_all(*selection)),
        name=name,
    )


@configurable
def combination_requirements(
    *,
    # combination
    mass_range: tuple[float, float],
    doca_max: float = None,
    docachi2_max: float = None,
    pt_min: float = None,
    # vertex selection
    vchi2pdof_max: float = None,
    bpvvdz_min: float = None,
    ip_bpvvdz_ratio_max: float = None,
    bpvvdrho_min: float = None,
    rho_min: float = None,
    ip_max: float = None,
    ip_min: float = None,
    ipchi2_max: float = None,
    ipchi2_min: float = None,
    dira_min: float = None,
    lifetime_min: float = None,
    # operations on decay products
    child_endvtx_distance_to_parent_min: child_requirement = None,
    children_endvtx_separation_min: children_comparison_requirement = None,
):
    """Build the combination and vertex requirements for a trigger line"""
    comb_mass_min, comb_mass_max = mass_range

    # account for up to a 10% difference in the invariant mass
    # measurement before and after the vertex fit
    combination_selection = [
        in_range(0.9 * comb_mass_min, F.MASS, 1.1 * comb_mass_max)
    ] + used_functors(
        MAX_DOCA(doca_max),
        MAX_DOCACHI2(docachi2_max),
        MIN_PT(pt_min),
    )

    pvs = make_pvs()

    vertex_selection = used_functors(
        CHECK_PVS(pvs),  # TODO: remove once MooreAnalysis#36 is fixed
        in_range(comb_mass_min, F.MASS, comb_mass_max),
        MAX_CHI2DOF(vchi2pdof_max),
        MIN_BPVVDZ(bpvvdz_min, pvs),
        MAX_IPBPVVDZ_RATIO(ip_bpvvdz_ratio_max, pvs),
        MIN_BPVVDRHO(bpvvdrho_min, pvs),
        MIN_RHO(rho_min),
        MAX_MINIP(ip_max, pvs),
        MIN_MINIP(ip_min, pvs),
        MAX_MINIPCHI2(ipchi2_max, pvs),
        MIN_MINIPCHI2(ipchi2_min, pvs),
        MIN_DIRA(dira_min, pvs),
        MIN_BPVLTIME(lifetime_min, pvs),
        MIN_DISTANCE_TO_PARENT(child_endvtx_distance_to_parent_min),
        MIN_DISTANCE_BETWEEN_CHILDREN(children_endvtx_separation_min),
    )

    return F.require_all(*combination_selection), (
        F.ALL if not vertex_selection else F.require_all(*vertex_selection)
    )


@configurable
def make_combination(
    decay_descriptor,
    inputs,
    *,
    build_requirements=None,
    mass_range,
    can_reco_vertex=True,
    name,
):
    """General combiner"""
    combination_selection, vertex_selection = combination_requirements(
        mass_range=mass_range,
        **({} if build_requirements is None else build_requirements()),
    )
    return ParticleCombiner(
        inputs,
        ParticleCombiner=(
            "ParticleVertexFitter" if can_reco_vertex else "ParticleAdder"
        ),
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_selection,
        CompositeCut=vertex_selection,
        name=name,
    )


@configurable
def filter_combination(combination, *, build_requirements=None, mass_range, name):
    """Filter a combination candidate"""
    combination_selection, vertex_selection = combination_requirements(
        mass_range=mass_range,
        **({} if build_requirements is None else build_requirements()),
    )
    return ParticleFilter(
        combination,
        F.FILTER(F.require_all(combination_selection, vertex_selection)),
        name=name,
    )


def requirements_builder(function):
    """
    Wrap a function to update the configuration returned by it with the
    provided keyword arguments. This allows to keep having a configurable
    function defining a set of requirements but in such a way that we do
    not need to propagate by hand the different keyword arguments.
    """

    @configurable
    @functools.wraps(function)
    def wrapper(**kwargs):
        requirements = function()
        requirements.update(kwargs)
        return requirements

    return wrapper


#
# Requirements on the decay products
#
@requirements_builder
def loose_requirements_for_decay_products():
    return dict(
        p_min=3 * GeV,
        pt_min=80 * MeV,
        track_chi2dof_max=16.0,
        ip_min=0.5 * mm,
        ipchi2_min=16.0,
    )


@requirements_builder
def requirements_for_decay_products():
    return loose_requirements_for_decay_products(
        p_min=3 * GeV,
        pt_min=80 * MeV,
        track_chi2dof_max=9.0,
        ip_min=1.0 * mm,
        ipchi2_min=25.0,
    )


@requirements_builder
def tight_requirements_for_decay_products():
    return requirements_for_decay_products(
        p_min=3 * GeV,
        pt_min=80 * MeV,
        track_chi2dof_max=4.0,
        ip_min=5.0 * mm,
        ipchi2_min=144.0,
    )


@requirements_builder
def loose_requirements_for_protons_from_sigma():
    return dict(
        p_min=3 * GeV,
        pt_min=80 * MeV,
        track_chi2dof_max=9.0,
        ip_min=0.1 * mm,
        ipchi2_min=9.0,
    )


@requirements_builder
def requirements_for_protons_from_sigma():
    return loose_requirements_for_protons_from_sigma(
        p_min=3 * GeV,
        pt_min=1000 * MeV,
        track_chi2dof_max=9.0,
        ip_min=0.1 * mm,
        ipchi2_min=9.0,
    )


@requirements_builder
def tight_requirements_for_protons_from_sigma():
    return requirements_for_protons_from_sigma(
        p_min=3 * GeV,
        pt_min=1000 * MeV,
        track_chi2dof_max=9.0,
        ip_min=2.0 * mm,
        ipchi2_min=25.0,
    )


@requirements_builder
def requirements_for_muons_from_sigma():
    return dict(
        p_min=3 * GeV,
        pt_min=80 * MeV,
        track_chi2dof_max=9.0,
        ip_min=0.5 * mm,
        ipchi2_min=9.0,
    )


#
# Requirements on the vertex for any combination involving a neutral particle. These
# selections can not contain vertexing requirements unless the combination involves
# at least two particles with information on the direction of motion.
#
@requirements_builder
def loose_requirements_for_neutral_combination_no_vertex():
    return dict(
        pt_min=500 * MeV,
    )


@requirements_builder
def requirements_for_neutral_combination_no_vertex():
    return loose_requirements_for_neutral_combination_no_vertex()


@requirements_builder
def tight_requirements_for_neutral_combination_no_vertex():
    return requirements_for_neutral_combination_no_vertex()


@requirements_builder
def requirements_for_sigma_decay_neutral_combination_no_vertex():
    return dict(
        pt_min=1000 * MeV,
    )


#
# Requirements on the vertex for any loose combination
#
@requirements_builder
def loose_requirements_for_combination():
    return dict(
        bpvvdz_min=0.0 * mm,
        docachi2_max=25.0,
        doca_max=0.3 * mm,
        vchi2pdof_max=16.0,
        ip_bpvvdz_ratio_max=1.0 / 60,
    )


@requirements_builder
def requirements_for_combination():
    return loose_requirements_for_combination(
        docachi2_max=16.0,
        doca_max=0.2 * mm,
        vchi2pdof_max=9.0,
    )


@requirements_builder
def requirements_for_sigma_combination():
    return loose_requirements_for_combination(
        docachi2_max=100.0,
        doca_max=1.0 * mm,
        vchi2pdof_max=25.0,
        dira_min=0.99,
        lifetime_min=0.006 * ns,
        pt_min=500 * MeV,
    )


@requirements_builder
def tight_requirements_for_combination():
    return requirements_for_combination(
        docachi2_max=9.0,
        doca_max=0.1 * mm,
        vchi2pdof_max=4.0,
    )


#
# Selection for a combination that is assumed to come from a
# primary vertex. We add requirements on the maximum distance
# and the minimum displacement in the perpendicular plane with
# respect to any primary vertex.
#
default_additional_requirements_for_prompt_combination = dict(
    ip_max=0.4 * mm, bpvvdrho_min=3.0 * mm, dira_min=0.999, ipchi2_max=25.0
)

default_additional_requirements_for_prompt_sigma_combination = dict(
    ip_max=0.4 * mm, bpvvdrho_min=3.0 * mm, ipchi2_max=25.0
)


@requirements_builder
def loose_requirements_for_prompt_combination():
    return loose_requirements_for_combination(
        **default_additional_requirements_for_prompt_combination
    )


@requirements_builder
def requirements_for_prompt_combination():
    return requirements_for_combination(
        **default_additional_requirements_for_prompt_combination
    )


@requirements_builder
def requirements_for_prompt_sigma_combination():
    return requirements_for_sigma_combination(
        **default_additional_requirements_for_prompt_sigma_combination
    )


@requirements_builder
def tight_requirements_for_prompt_combination():
    return tight_requirements_for_combination(
        **default_additional_requirements_for_prompt_combination
    )


#
# Requirements for a combination that does not come from
# a primary vertex (predominantly semileptonic decays). We
# simply add a requirement on the vertex position with respect
# to the Z axis. The lack of a requirement on the impact
# parameter allows to use this requirement also on the
# normalization modes.
#
default_additional_requirements_for_detached_combination_no_ip = dict(rho_min=3.0 * mm)


@requirements_builder
def loose_requirements_for_detached_combination_no_ip():
    return loose_requirements_for_combination(
        **default_additional_requirements_for_detached_combination_no_ip
    )


@requirements_builder
def requirements_for_detached_combination_no_ip():
    return requirements_for_combination(
        **default_additional_requirements_for_detached_combination_no_ip
    )


@requirements_builder
def tight_requirements_for_detached_combination_no_ip():
    return tight_requirements_for_combination(
        **default_additional_requirements_for_detached_combination_no_ip
    )


#
# Requirements for a combination that does not come from
# a primary vertex (predominantly semileptonic decays).
#
@requirements_builder
def loose_requirements_for_detached_combination():
    return loose_requirements_for_detached_combination_no_ip(ip_min=0.5 * mm)


@requirements_builder
def requirements_for_detached_combination():
    return requirements_for_detached_combination_no_ip(ip_min=1.0 * mm)


@requirements_builder
def tight_requirements_for_detached_combination():
    return tight_requirements_for_detached_combination_no_ip(
        ip_min=5.0 * mm, ipchi2_min=100.0, ipchi2_max=900.0
    )


#
# Requirements for a prompt KS0 selection
#
default_additional_requirements_for_prompt_ks0 = dict(
    lifetime_min=0.0045 * ns  # eff. ~ 95 %
)


@requirements_builder
def loose_requirements_for_prompt_ks0():
    return loose_requirements_for_prompt_combination(
        **default_additional_requirements_for_prompt_ks0
    )


@requirements_builder
def requirements_for_prompt_ks0():
    return requirements_for_prompt_combination(
        **default_additional_requirements_for_prompt_ks0
    )


def loose_requirements_for_prompt_ks0_with_intermediate_vertex_separation(*indices):
    @requirements_builder
    def wrapper():
        return loose_requirements_for_prompt_ks0(
            children_endvtx_separation_min=children_comparison_requirement(
                indices, indices, 0.5 * mm
            ),
        )

    return wrapper


#
# Basic particle makers (decay products)
#
@configurable
def make_loose_electrons(*, pid_e_min=0, add_brem=True, **kwargs):
    return filter_decay_products(
        standard_particles.make_long_electrons_with_brem()
        if add_brem
        else standard_particles.make_long_electrons_no_brem(),
        pid_e_min=pid_e_min,
        name="LooseElectronsForStrangeDecays_{hash}",
        **loose_requirements_for_decay_products(**kwargs),
    )


@configurable
def make_electrons(*, pid_e_min=2, add_brem=True, **kwargs):
    return filter_decay_products(
        make_loose_electrons(add_brem=add_brem),
        pid_e_min=pid_e_min,
        name="ElectronsForStrangeDecays_{hash}",
        **requirements_for_decay_products(**kwargs),
    )


@configurable
def make_tight_electrons(*, pid_e_min=5, add_brem=True, **kwargs):
    return filter_decay_products(
        make_electrons(add_brem=add_brem),
        pid_e_min=pid_e_min,
        name="TightElectronsForStrangeDecays_{hash}",
        **tight_requirements_for_decay_products(**kwargs),
    )


@configurable
def make_loose_downstream_electrons(*, pid_e_min=0, add_brem=True, **kwargs):
    return filter_decay_products(
        standard_particles.make_down_electrons_with_brem()
        if add_brem
        else standard_particles.make_down_electrons_no_brem(),
        pid_e_min=pid_e_min,
        name="LooseDownstreamElectronsForStrangeDecays_{hash}",
        **loose_requirements_for_decay_products(**kwargs),
    )


@configurable
def make_downstream_electrons(*, pid_e_min=2, add_brem=True, **kwargs):
    return filter_decay_products(
        make_loose_downstream_electrons(add_brem=add_brem),
        pid_e_min=pid_e_min,
        name="DownstreamElectronsForStrangeDecays_{hash}",
        **requirements_for_decay_products(**kwargs),
    )


@configurable
def make_tight_downstream_electrons(*, pid_e_min=5, add_brem=True, **kwargs):
    return filter_decay_products(
        make_downstream_electrons(add_brem=add_brem),
        pid_e_min=pid_e_min,
        name="TightDownstreamElectronsForStrangeDecays_{hash}",
        **tight_requirements_for_decay_products(**kwargs),
    )


@configurable
def make_loose_muons(**kwargs):
    return filter_decay_products(
        standard_particles.make_long_muons(),
        name="LooseMuonsForStrangeDecays_{hash}",
        **loose_requirements_for_decay_products(**kwargs),
    )


@configurable
def make_muons(*, pid_mu_min=-3, **kwargs):
    return filter_decay_products(
        make_loose_muons(),
        pid_mu_min=pid_mu_min,
        name="MuonsForStrangeDecays_{hash}",
        is_muon=True,
        **requirements_for_decay_products(**kwargs),
    )


@configurable
def make_tight_muons(pid_mu_min=5.0, **kwargs):
    return filter_decay_products(
        make_muons(),
        pid_mu_min=pid_mu_min,
        name="TightMuonsForStrangeDecays_{hash}",
        **tight_requirements_for_decay_products(**kwargs),
    )


@configurable
def make_muons_for_sigma(*, pid_mu_min=-3, **kwargs):
    return filter_decay_products(
        make_loose_muons(),
        pid_mu_min=pid_mu_min,
        name="MuonsForSigmaDecays_{hash}",
        is_muon=True,
        **requirements_for_muons_from_sigma(**kwargs),
    )


@configurable
def make_loose_downstream_muons(**kwargs):
    return filter_decay_products(
        standard_particles.make_down_muons(),
        name="LooseDownstreamMuonsForStrangeDecays_{hash}",
        **loose_requirements_for_decay_products(**kwargs),
    )


@configurable
def make_downstream_muons(*, pid_mu_min=-3, **kwargs):
    return filter_decay_products(
        make_loose_downstream_muons(),
        pid_mu_min=pid_mu_min,
        name="DownstreamMuonsForStrangeDecays_{hash}",
        is_muon=True,
        **requirements_for_decay_products(**kwargs),
    )


@configurable
def make_tight_downstream_muons(pid_mu_min=5.0, **kwargs):
    return filter_decay_products(
        make_downstream_muons(),
        pid_mu_min=pid_mu_min,
        name="TightDownstreamMuonsForStrangeDecays_{hash}",
        **tight_requirements_for_decay_products(**kwargs),
    )


@configurable
def make_downstream_muons_for_sigma(*, pid_mu_min=-3, **kwargs):
    return filter_decay_products(
        make_loose_downstream_muons(),
        pid_mu_min=pid_mu_min,
        name="DownstreamMuonsForSigmaDecays_{hash}",
        is_muon=True,
        **requirements_for_muons_from_sigma(**kwargs),
    )


@configurable
def make_loose_pions(**kwargs):
    return filter_decay_products(
        standard_particles.make_long_pions(),
        name="LoosePionsForStrangeDecays_{hash}",
        **loose_requirements_for_decay_products(**kwargs),
    )


@configurable
def make_pions(**kwargs):
    return filter_decay_products(
        make_loose_pions(),
        name="PionsForStrangeDecays_{hash}",
        **requirements_for_decay_products(**kwargs),
    )


@configurable
def make_tight_pions(**kwargs):
    return filter_decay_products(
        make_pions(),
        name="TightPionsForStrangeDecays_{hash}",
        **tight_requirements_for_decay_products(**kwargs),
    )


@configurable
def make_loose_downstream_pions(**kwargs):
    return filter_decay_products(
        standard_particles.make_down_pions(),
        name="LooseDownstreamPionsForStrangeDecays_{hash}",
        **loose_requirements_for_decay_products(**kwargs),
    )


@configurable
def make_downstream_pions(**kwargs):
    return filter_decay_products(
        make_loose_downstream_pions(),
        name="DownstreamPionsForStrangeDecays_{hash}",
        **requirements_for_decay_products(**kwargs),
    )


@configurable
def make_tight_downstream_pions(**kwargs):
    return filter_decay_products(
        make_downstream_pions(),
        name="TightDownstreamPionsForStrangeDecays_{hash}",
        **tight_requirements_for_decay_products(**kwargs),
    )


@configurable
def make_kaons(pid_k_min=3, pid_k_over_p_min=3, **kwargs):
    return filter_decay_products(
        standard_particles.make_long_kaons(),
        name="KaonsForStrangeDecays_{hash}",
        pid_k_min=pid_k_min,
        pid_k_over_p_min=pid_k_over_p_min,
        **requirements_for_decay_products(**kwargs),
    )


@configurable
def make_tight_kaons(**kwargs):
    return filter_decay_products(
        make_kaons(),
        name="TightKaonsForStrangeDecays_{hash}",
        **tight_requirements_for_decay_products(**kwargs),
    )


@configurable
def make_downstream_kaons(pid_k_min=3, pid_k_over_p_min=3, **kwargs):
    return filter_decay_products(
        standard_particles.make_down_kaons(),
        name="DownstreamKaonsForStrangeDecays_{hash}",
        pid_k_min=pid_k_min,
        pid_k_over_p_min=pid_k_over_p_min,
        **requirements_for_decay_products(**kwargs),
    )


@configurable
def make_photons(pt_min=200 * MeV, is_not_h_min=0.5, is_photon_min=0.5):
    particles = standard_particles.make_photons(PtCut=pt_min)
    return filter_decay_products(
        particles,
        is_not_h_min=is_not_h_min,
        is_photon_min=is_photon_min,
        name="PhotonsForStrangeDecays_{hash}",
    )


@configurable
def make_tight_photons(is_not_h_min=0.7, is_photon_min=0.7):
    return filter_decay_products(
        make_photons(),
        is_not_h_min=is_not_h_min,
        is_photon_min=is_photon_min,
        name="TightPhotonsForStrangeDecays_{hash}",
    )


@configurable
def make_resolved_neutral_pions(*, mass_window=30.0 * MeV, pt_min=500.0 * MeV):
    return standard_particles.make_resolved_pi0s(
        particles=make_photons, mass_window=mass_window, PtCut=pt_min
    )


@configurable
def make_tight_resolved_neutral_pions(**kwargs):
    return make_resolved_neutral_pions(**kwargs)


@configurable
def make_merged_neutral_pions(
    *,
    mass_window=60 * MeV,
    pt_min=2 * GeV,
    is_not_h_min=0.5,
    is_photon_max=0.5,
):
    return filter_decay_products(
        standard_particles.make_merged_pi0s(mass_window=mass_window, PtCut=pt_min),
        is_not_h_min=is_not_h_min,
        is_photon_max=is_photon_max,
        name="MergedPi0sForStrangeDecays_{hash}",
    )


@configurable
def make_tight_merged_neutral_pions(**kwargs):
    return make_merged_neutral_pions(**kwargs)


@configurable
def make_protons(*, pid_p_min=0, pid_p_over_k_min=0, **kwargs):
    return filter_decay_products(
        standard_particles.make_long_protons(),
        pid_p_min=pid_p_min,
        pid_p_over_k_min=pid_p_over_k_min,
        name="ProtonsForStrangeDecays_{hash}",
        **requirements_for_decay_products(**kwargs),
    )


@configurable
def make_tight_protons(
    *, pt_min=500 * MeV, pid_p_min=+10, pid_p_over_k_min=+10, **kwargs
):
    return filter_decay_products(
        make_protons(),
        pid_p_min=pid_p_min,
        pid_p_over_k_min=pid_p_over_k_min,
        name="TightProtonsForStrangeDecays_{hash}",
        **tight_requirements_for_decay_products(pt_min=pt_min, **kwargs),
    )


@configurable
def make_loose_protons_for_sigma(*, pid_p_min=0, pid_p_over_k_min=0, **kwargs):
    return filter_decay_products(
        standard_particles.make_long_protons(),
        pid_p_min=pid_p_min,
        pid_p_over_k_min=pid_p_over_k_min,
        name="LooseProtonsForSigmaDecays_{hash}",
        **loose_requirements_for_protons_from_sigma(**kwargs),
    )


@configurable
def make_protons_for_sigma(*, pid_p_min=5, pid_p_over_k_min=0, **kwargs):
    return filter_decay_products(
        make_loose_protons_for_sigma(),
        pid_p_min=pid_p_min,
        pid_p_over_k_min=pid_p_over_k_min,
        name="ProtonsForSigmaDecays_{hash}",
        **requirements_for_protons_from_sigma(**kwargs),
    )


@configurable
def make_tight_protons_for_sigma(*, pid_p_min=10, pid_p_over_k_min=0, **kwargs):
    return filter_decay_products(
        make_protons_for_sigma(),
        pid_p_min=pid_p_min,
        pid_p_over_k_min=pid_p_over_k_min,
        name="TightProtonsForSigmaDecays_{hash}",
        **tight_requirements_for_protons_from_sigma(**kwargs),
    )


@configurable
def make_downstream_protons(*, pid_p_min=0, pid_p_over_k_min=0, **kwargs):
    return filter_decay_products(
        standard_particles.make_down_protons(),
        pid_p_min=pid_p_min,
        pid_p_over_k_min=pid_p_over_k_min,
        name="DownstreamProtonsForStrangeDecays_{hash}",
        **requirements_for_decay_products(**kwargs),
    )


@configurable
def make_tight_downstream_protons(*, pid_p_min=+5, pid_p_over_k_min=+3, **kwargs):
    return filter_decay_products(
        make_downstream_protons(),
        pid_p_min=pid_p_min,
        pid_p_over_k_min=pid_p_over_k_min,
        name="TightDownstreamProtonsForStrangeDecays_{hash}",
        **kwargs,
        **tight_requirements_for_decay_products(**kwargs),
    )


@configurable
def make_loose_downstream_protons_for_sigma(
    *, pid_p_min=0, pid_p_over_k_min=0, **kwargs
):
    return filter_decay_products(
        standard_particles.make_down_protons(),
        pid_p_min=pid_p_min,
        pid_p_over_k_min=pid_p_over_k_min,
        name="LooseDownstreamProtonsForSigmaDecays_{hash}",
        **loose_requirements_for_protons_from_sigma(**kwargs),
    )


@configurable
def make_downstream_protons_for_sigma(*, pid_p_min=5, pid_p_over_k_min=0, **kwargs):
    return filter_decay_products(
        make_loose_downstream_protons_for_sigma(),
        pid_p_min=pid_p_min,
        pid_p_over_k_min=pid_p_over_k_min,
        name="DownstreamProtonsForSigmaDecays_{hash}",
        **requirements_for_protons_from_sigma(**kwargs),
    )


@configurable
def make_tight_downstream_protons_for_sigma(
    *, pid_p_min=10, pid_p_over_k_min=0, **kwargs
):
    return filter_decay_products(
        make_downstream_protons_for_sigma(),
        pid_p_min=pid_p_min,
        pid_p_over_k_min=pid_p_over_k_min,
        name="TightDownstreamProtonsForSigmaDecays_{hash}",
        **tight_requirements_for_protons_from_sigma(**kwargs),
    )


#
# Definitions of invariant mass bounds
#


@configurable
def ks0_mass_bounds(mass_min=400 * MeV, mass_max=600 * MeV):
    """Allow to define the same mass bounds for many lines that select KS0 decays"""
    return mass_min, mass_max


@configurable
def ks0_tight_mass_bounds(mass_min=470 * MeV, mass_max=600 * MeV):
    """
    Allow to define the same mass bounds for many lines that select KS0 decays

    The lower mass bound is chosen to avoid the doubly misidentified KS0 -> pi+ pi-
    decays.
    """
    return mass_min, mass_max


@configurable
def ks0_semileptonic_mass_bounds(mass_min=0.0 * MeV, mass_max=360 * MeV):
    """
    Allow to define the same mass bounds for many lines that select semileptonic
    KS0 decays
    """
    return mass_min, mass_max


@configurable
def kplus_mass_bounds(mass_min=460 * MeV, mass_max=660 * MeV):
    """
    Allow to define the same mass bounds for many lines that select K+ decays

    The lower mass bound is chosen to avoid doubly-misidentified decays in
    leptonic modes.
    """
    return mass_min, mass_max


@configurable
def kplus_semileptonic_mass_bounds(mass_min=0.0 * MeV, mass_max=660 * MeV):
    """
    Allow to define the same mass bounds for lines that select semileptonic
    K+ decays
    """
    return mass_min, mass_max


@configurable
def lambda0_mass_bounds(mass_min=1000 * MeV, mass_max=1200 * MeV):
    """Allow to define the same mass bounds for many lines that select Lambda0 decays"""
    return mass_min, mass_max


@configurable
def sigma_mass_bounds(mass_min=1000 * MeV, mass_max=1300 * MeV):
    """Allow to define the same mass bounds for many lines that select Sigma+ decays"""
    return mass_min, mass_max


@configurable
def xi_mass_bounds(mass_min=1200 * MeV, mass_max=1400 * MeV):
    """Allow to define the same mass bounds for many lines that select Xi-/0 decays"""
    return mass_min, mass_max


@configurable
def xi_tight_mass_bounds(mass_min=1275 * MeV, mass_max=1375 * MeV):
    """Allow to define the same mass bounds for many lines that select Xi-/0 decays"""
    return mass_min, mass_max


@configurable
def omega_mass_bounds(mass_min=1550 * MeV, mass_max=1750 * MeV):
    """Allow to define the same mass bounds for many lines that select Omega- decays"""
    return mass_min, mass_max


#
# Intermediate particle makers
#
@configurable
def make_dielectron(
    particle_id,
    *,
    mass_range,
    dielectron_pt_min=0.0 * MeV,
    brem_adder="SelectiveBremAdder",
    opposite_sign=True,
    electron_builder=make_electrons,
    combiner="dielectron-maker",
):
    """
    Create a dielectron candidate where we avoid using the
    same radiation photon more than once
    """
    dielectron_mass_min, dielectron_mass_max = mass_range

    if combiner == "dielectron-maker":
        return FunctionalDiElectronMaker(
            InputParticles=electron_builder(add_brem=False),
            MinDiElecPT=dielectron_pt_min,
            MinDiElecMass=dielectron_mass_min,
            MaxDiElecMass=dielectron_mass_max,
            DiElecID=particle_id,
            OppositeSign=opposite_sign,
            BremAdder=brem_adder,
            ParticleCombiner="ParticleVertexFitter",
            name="DiElectronForStrangeDecays_DiElectronMaker_{hash}",
        ).Particles
    elif combiner == "combiner":
        electrons = electron_builder(add_brem=False)
        return make_combination(
            f"{particle_id} -> e+ e-"
            if opposite_sign
            else f"[{particle_id} -> e+ e+]cc",
            [electrons, electrons],
            mass_range=(dielectron_mass_min, dielectron_mass_max),
            build_requirements=lambda: dict(pt_min=dielectron_pt_min),
            name="DiElectronForStrangeDecays_Combiner_{hash}",
        )
    else:
        raise ValueError(
            f'Unknown combiner type "{combiner}"; choose between "dielectron-maker" or "combiner"'
        )


def build_lambda0():
    """Build Lambda0 baryons"""
    proton = make_protons()
    pion = make_pions()
    return make_combination(
        "[Lambda0 -> p+ pi-]cc",
        [proton, pion],
        build_requirements=requirements_for_detached_combination_no_ip,
        mass_range=lambda0_mass_bounds(),
        name="Lambda0ForStrangeDecays_Combiner_{hash}",
    )


def build_tight_lambda0():
    """Build Lambda0 baryons"""
    proton = make_tight_protons()
    pion = make_tight_pions()
    return make_combination(
        "[Lambda0 -> p+ pi-]cc",
        [proton, pion],
        build_requirements=tight_requirements_for_detached_combination_no_ip,
        mass_range=lambda0_mass_bounds(),
        name="TightLambda0ForStrangeDecays_Combiner_{hash}",
    )


def build_downstream_lambda0():
    """Build downstream Lambda0 baryons"""
    proton = make_downstream_protons()
    pion = make_downstream_pions()
    return make_combination(
        "[Lambda0 -> p+ pi-]cc",
        [proton, pion],
        build_requirements=requirements_for_detached_combination_no_ip,
        mass_range=lambda0_mass_bounds(),
        name="DownstreamLambda0ForStrangeDecays_Combiner_{hash}",
    )


def build_tight_downstream_lambda0():
    """Build downstream Lambda0 baryons"""
    proton = make_tight_downstream_protons()
    pion = make_tight_downstream_pions()
    return make_combination(
        "[Lambda0 -> p+ pi-]cc",
        [proton, pion],
        build_requirements=tight_requirements_for_detached_combination_no_ip,
        mass_range=lambda0_mass_bounds(),
        name="TightDownstreamLambda0ForStrangeDecays_Combiner_{hash}",
    )


def build_ximinus(*, return_all_containers=False):
    """Build Xi- baryons"""
    l0 = build_lambda0()
    pions = make_pions()
    xi = make_combination(
        "[Xi- -> Lambda0 pi-]cc",
        [l0, pions],
        build_requirements=requirements_for_detached_combination_no_ip,
        mass_range=xi_mass_bounds(),
        name="XiMinusForStrangeDecays_Combiner_{hash}",
    )
    if return_all_containers:
        return xi, l0, pions
    else:
        return xi


def build_tight_ximinus(*, return_all_containers=False):
    """Build Xi- baryons"""
    l0 = build_tight_lambda0()
    pions = make_tight_pions()
    xi = make_combination(
        "[Xi- -> Lambda0 pi-]cc",
        [l0, pions],
        build_requirements=tight_requirements_for_detached_combination_no_ip,
        mass_range=xi_mass_bounds(),
        name="TightXiMinusForStrangeDecays_Combiner_{hash}",
    )
    if return_all_containers:
        return xi, l0, pions
    else:
        return xi


def build_loose_dimuon_intermediate_neutral(particle_name, *, mass_max):
    """Build a neutral particle from two muons with loose requirements"""
    muons = make_loose_muons()
    return make_combination(
        f"{particle_name} -> mu+ mu-",
        [muons, muons],
        build_requirements=loose_requirements_for_detached_combination,
        mass_range=(dimuon_min_mass(), mass_max),
        name="DiMuon_IntermediateNeutral_Combiner_{hash}",
    )


def build_loose_dielectron_intermediate_neutral(particle_name, *, mass_max):
    """Build a neutral particle from two electrons with loose requirements"""
    mass_range = (dielectron_min_mass(), mass_max)
    return filter_combination(
        make_dielectron(
            particle_name, mass_range=mass_range, electron_builder=make_loose_electrons
        ),
        build_requirements=loose_requirements_for_detached_combination,
        mass_range=mass_range,
        name="DiElectron_IntermediateNeutral_Filter_{hash}",
    )


def build_loose_downstream_dielectron_intermediate_neutral(particle_name, *, mass_max):
    """Build a neutral particle from two (downstream) electrons with loose requirements"""
    mass_range = (dielectron_min_mass(), mass_max)
    return filter_combination(
        make_dielectron(
            particle_name,
            mass_range=mass_range,
            electron_builder=make_loose_downstream_electrons,
        ),
        build_requirements=loose_requirements_for_detached_combination,
        mass_range=mass_range,
        name="LooseDownstreamDiElectron_IntermediateNeutral_Filter_{hash}",
    )


#
# Basic diparticle minimum invariant mass values
#
@configurable
def dielectron_min_mass(mass_min=30.0 * MeV):
    """Requirement to avoid a peak at low mass in the dielectron spectrum"""
    return mass_min


@configurable
def dimuon_min_mass(mass_min=220.0 * MeV):
    """Requirement to avoid a peak at low mass in the dimuon spectrum"""
    return mass_min


@configurable
def pion_muon_min_mass(mass_min=250.0 * MeV):
    """Requirement to avoid a peak at low mass in the pion-muon spectrum"""
    return mass_min


@configurable
def pion_electron_min_mass(mass_min=160.0 * MeV):
    """Requirement to avoid a peak at low mass in the pion-electron spectrum"""
    return mass_min


@configurable
def muon_electron_min_mass(mass_min=105.0 * MeV):
    """Requirement for [mu+, e-]cc combinations"""
    return mass_min


#
# Definition of the isolation
#
@configurable
def define_isolation(
    names_and_candidates,
    *,
    cut,
    tracks_with_velo_segment=False,
    downstream_tracks=False,
):
    """
    Define the storage of additional particles based on requirements
    imposed with respect to the head of a decay.
    """
    names, candidates = list(zip(*names_and_candidates.items()))
    return rd_isolation.select_parts_for_isolation(
        names=names,
        candidates=candidates,
        cut=cut,
        LongTrackIso=tracks_with_velo_segment,
        DownstreamTrackIso=downstream_tracks,
        # we also persist upstream tracks when we work with long tracks
        UpstreamTrackIso=tracks_with_velo_segment,
        # we ensure the other isolation objects are disabled
        TTrackIso=False,
        NeutralIso=False,
        PiZerosIso=False,
    )


@configurable
def define_impact_parameter_chi2_based_isolation(
    names_and_candidates, *, ipchi2_max=25, **kwargs
):
    """
    Define the isolation based on a requirement on the impact parameter chi2
    between the additional particles and the vertex of the composite objects.
    """
    # the first argument to the algorithm making the weighted related-info table
    # is the reference particles and the second is the candidates to persist
    functor = (
        F.IPCHI2.bind(F.FORWARDARG0, F.FORWARDARG1) < ipchi2_max
    ) & ~F.FIND_IN_TREE()
    return define_isolation(names_and_candidates, cut=functor, **kwargs)


@configurable
def define_cone_based_isolation(
    names_and_candidates, *, delta_r_requirement=0.25, **kwargs
):
    """
    Define the isolation based on a requirement in the cone distance based on
    the azimuthal and pseudorapidity space.
    """
    functor = (F.DR2 < delta_r_requirement * delta_r_requirement) & ~F.FIND_IN_TREE()
    return define_isolation(names_and_candidates, cut=functor, **kwargs)
