###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Definition of MVA tools for XLL lines"""

import Functors as F
from Functors.math import log
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleFilter


# ------------------------------------------------------------------
def get_bullk(pvs):
    d_bullk = {
        "min_ll_pt": F.MINTREE(((F.IS_ID("e+")) | (F.IS_ID("e-"))), F.PT),
        "max_ll_pt": F.MAXTREE(((F.IS_ID("e+")) | (F.IS_ID("e-"))), F.PT),
        "min_log_ll_ipchi2": F.MINTREE(
            ((F.IS_ID("e+")) | (F.IS_ID("e-"))), log(F.MINIPCHI2(pvs))
        ),
        "max_log_ll_ipchi2": F.MAXTREE(
            ((F.IS_ID("e+")) | (F.IS_ID("e-"))), log(F.MINIPCHI2(pvs))
        ),
        "dira": F.BPVDIRA(pvs),
        "log_b_pt": log(F.PT),
        "log_b_vtx_chi2": log(F.CHI2DOF),
        "log_b_fdchi2": log(F.BPVFDCHI2(pvs)),
        "log_b_ipchi2": log(F.BPVIPCHI2(pvs)),
        "log_j_pt": log(F.CHILD(1, F.PT)),
        "log_j_vtx_chi2": log(F.CHILD(1, F.CHI2DOF)),
        "log_j_fdchi2": log(F.CHILD(1, F.BPVFDCHI2(pvs))),
        "log_j_ipchi2": log(F.CHILD(1, F.BPVIPCHI2(pvs))),
        "log_h_pt": log(F.CHILD(2, F.PT)),
        "log_h_ipchi2": log(F.CHILD(2, F.BPVIPCHI2(pvs))),
    }

    return d_bullk


# ------------------------------------------------------------------
def get_bdllkst(pvs):
    d_bdllkst = {
        "min_ll_pt": F.MINTREE(((F.IS_ID("e+")) | (F.IS_ID("e-"))), F.PT),
        "max_ll_pt": F.MAXTREE(((F.IS_ID("e+")) | (F.IS_ID("e-"))), F.PT),
        "min_log_ll_ipchi2": F.MINTREE(
            ((F.IS_ID("e+")) | (F.IS_ID("e-"))), log(F.MINIPCHI2(pvs))
        ),
        "max_log_ll_ipchi2": F.MAXTREE(
            ((F.IS_ID("e+")) | (F.IS_ID("e-"))), log(F.MINIPCHI2(pvs))
        ),
        "dira": F.BPVDIRA(pvs),
        "log_b_pt": log(F.PT),
        "log_b_vtx_chi2": log(F.CHI2DOF),
        "log_b_fdchi2": log(F.BPVFDCHI2(pvs)),
        "log_b_ipchi2": log(F.BPVIPCHI2(pvs)),
        "log_j_pt": log(F.CHILD(1, F.PT)),
        # There is only one kaon and one pion in the final state
        # Used to identify safely the particle
        "log_k_pt": log(F.MINTREE(((F.IS_ID("K+")) | (F.IS_ID("K-"))), F.PT)),
        "log_pi_pt": log(F.MINTREE(((F.IS_ID("pi+")) | (F.IS_ID("pi-"))), F.PT)),
    }

    return d_bdllkst


# ------------------------------------------------------------------
def get_bsllphi(pvs):
    d_bsllphi = {
        "min_ll_pt": F.MINTREE(((F.IS_ID("e+")) | (F.IS_ID("e-"))), F.PT),
        "max_ll_pt": F.MAXTREE(((F.IS_ID("e+")) | (F.IS_ID("e-"))), F.PT),
        "min_log_ll_ipchi2": F.MINTREE(
            ((F.IS_ID("e+")) | (F.IS_ID("e-"))), log(F.MINIPCHI2(pvs))
        ),
        "max_log_ll_ipchi2": F.MAXTREE(
            ((F.IS_ID("e+")) | (F.IS_ID("e-"))), log(F.MINIPCHI2(pvs))
        ),
        # ----
        "min_hh_pt": F.MINTREE(((F.IS_ID("K+")) | (F.IS_ID("K-"))), F.PT),
        "max_hh_pt": F.MAXTREE(((F.IS_ID("K+")) | (F.IS_ID("K-"))), F.PT),
        "min_log_hh_ipchi2": F.MINTREE(
            ((F.IS_ID("K+")) | (F.IS_ID("K-"))), log(F.MINIPCHI2(pvs))
        ),
        "max_log_hh_ipchi2": F.MAXTREE(
            ((F.IS_ID("K+")) | (F.IS_ID("K-"))), log(F.MINIPCHI2(pvs))
        ),
        # ----
        "dira": F.BPVDIRA(pvs),
        "log_b_pt": log(F.PT),
        "log_b_vtx_chi2": log(F.CHI2DOF),
        "log_b_fdchi2": log(F.BPVFDCHI2(pvs)),
        "log_b_ipchi2": log(F.BPVIPCHI2(pvs)),
        # ----
        "log_j_pt": log(F.CHILD(1, F.PT)),
        "log_j_ipchi2": log(F.CHILD(1, F.BPVIPCHI2(pvs))),
        # ----
        "log_p_pt": log(F.CHILD(2, F.PT)),
        "log_p_ipchi2": log(F.CHILD(2, F.BPVIPCHI2(pvs))),
    }

    return d_bsllphi


# ------------------------------------------------------------------
def get_lbllpk(pvs):
    d_lbllpk = {
        "min_ll_pt": F.MINTREE(((F.IS_ID("e+")) | (F.IS_ID("e-"))), F.PT),
        "max_ll_pt": F.MAXTREE(((F.IS_ID("e+")) | (F.IS_ID("e-"))), F.PT),
        "min_log_ll_ipchi2": F.MINTREE(
            ((F.IS_ID("e+")) | (F.IS_ID("e-"))), log(F.MINIPCHI2(pvs))
        ),
        "max_log_ll_ipchi2": F.MAXTREE(
            ((F.IS_ID("e+")) | (F.IS_ID("e-"))), log(F.MINIPCHI2(pvs))
        ),
        # ----
        "log_b_pt": log(F.PT),
        "log_j_pt": log(F.CHILD(1, F.PT)),
        "log_h_pt": log(F.MINTREE(((F.IS_ID("K+")) | (F.IS_ID("K-"))), F.PT)),
        "dira": F.BPVDIRA(pvs),
        "log_b_vtx_chi2": log(F.CHI2DOF),
        "log_b_fdchi2": log(F.BPVFDCHI2(pvs)),
        "log_b_ipchi2": log(F.BPVIPCHI2(pvs)),
        "log_j_ipchi2": log(F.CHILD(1, F.BPVIPCHI2(pvs))),
    }

    return d_lbllpk


# ------------------------------------------------------------------
@configurable
def xll_BDT_functor(pvs, line):
    d_bdt_vars = {
        "bullk": get_bullk(pvs),
        "bdllkst": get_bdllkst(pvs),
        "bsllphi": get_bsllphi(pvs),
        "lbllpk": get_lbllpk(pvs),
    }

    d_xml_files = {
        "bullk": "paramfile://data/Hlt2_RD_llk_v1.xml",
        "bdllkst": "paramfile://data/Hlt2_RD_llkst_v1.xml",
        "bsllphi": "paramfile://data/Hlt2_RD_llphi_v1.xml",
        "lbllpk": "paramfile://data/Hlt2_RD_llpk_v1.xml",
    }

    xml_file = d_xml_files[line]
    bdt_vars = d_bdt_vars[line]

    fun = F.MVA(
        MVAType="TMVA",
        Config={
            "XMLFile": xml_file,
            "Name": "BDT",
        },
        Inputs=bdt_vars,
    )

    return fun


# ------------------------------------------------------------------
@configurable
def make_b2(presel_b, pvs, line, bdt_cut, filter_name="rd_xll_mva_B_{hash}"):
    code = xll_BDT_functor(pvs, line) > bdt_cut
    return ParticleFilter(presel_b, F.FILTER(code), name=filter_name)
