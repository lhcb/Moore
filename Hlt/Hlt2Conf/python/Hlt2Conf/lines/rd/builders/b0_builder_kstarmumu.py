###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""B0 -> K* J/psi builder for B0 -> K* Mu Mu exclusive line"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import MeV
from RecoConf.algorithms_thor import ParticleCombiner


def make_b(
    kstars,
    jpsis,
    pvs,
    name="make_b_to_kstarmumu",
    max_mass=6000.0 * MeV,
    min_mass=4900.0 * MeV,
    min_fdchi2=64.0,
    min_dira=0.999,
    max_vertex_chi2=25.0,
    max_ipchi2=16.0,
):
    code = in_range(min_mass, F.MASS, max_mass)
    composite_code = F.require_all(
        F.BPVFDCHI2(pvs) > min_fdchi2,
        F.BPVDIRA(pvs) > min_dira,
        F.CHI2 < max_vertex_chi2,
        F.BPVIPCHI2(pvs) < max_ipchi2,
    )

    return ParticleCombiner(
        Inputs=[kstars, jpsis],
        DecayDescriptor="B0 -> K*(892)0 J/psi(1S)",
        CombinationCut=code,
        CompositeCut=composite_code,
    )
