###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# RD isolation algorithm
import math

from PyConf import configurable
from PyConf.Algorithms import ThOrParticleSelection
from RecoConf.algorithms_thor import ParticleCombiner, ParticleContainersMerger
from RecoConf.rdbuilder_thor import (
    make_rd_detached_pions,
    make_rd_tauons_hadronic_decay,
)
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    make_down_pions,
    make_long_pions,
    make_merged_pi0s,
    make_photons,
    make_ttrack_pions,
    make_up_pions,
)

from Hlt2Conf.isolation import extra_outputs_for_isolation

LOG10_E = math.log10(math.e)

import Functors as F
import Functors.math as fmath


@configurable
def find_in_decay(input=make_rd_tauons_hadronic_decay, id="pi+"):
    """
    Retrieve the selection of particles matching 'id' argument from 'input' decay tree
    Args:
        input: Composite particle
        id: String representation of the ParticleProperty to get
    """
    code = F.FILTER(F.IS_ABS_ID(id)) @ F.GET_ALL_DESCENDANTS()

    return ThOrParticleSelection(InputParticles=input, Functor=code).OutputSelection


@configurable
def make_two_body_combination(candidate, tes_name, use_fiducial_cut: bool = True):
    """
    Make two body combination of B0 and extra particles (long tracks with pion id).

    Args:
    candidate: Containers of reference particles
    tes_name: String for the TES location
    use_fiducial_cut(bool): Use preselection requirement for the combination (default value True)

    Returns: TES location of two body combination of candidate and extra_particles
    """

    extra_particles = make_rd_detached_pions(pid=None)
    v2_pvs = make_pvs()
    ## Set default values
    pv_dist_min = -3.0  # mm
    cone_min = 0.25
    log_ipchi2_wrtSV_max = 5

    fiducial_cut_pv_dist = F.ALL
    fiducial_cut_cone = F.ALL
    fiducial_cut_sv_ipchi2 = F.ALL

    if use_fiducial_cut:
        fiducial_cut_pv_dist = (
            F.MAGNITUDE @ (F.ENDVERTEX_POS - F.BPV_POS(v2_pvs))
        ) * fmath.sign(F.END_VZ - F.BPVZ(v2_pvs)) > pv_dist_min

        DETA = F.CHILD(1, F.ETA) - F.CHILD(2, F.ETA)
        DPHI = F.ADJUST_ANGLE @ (F.CHILD(1, F.PHI) - F.CHILD(2, F.PHI))
        fiducial_cut_cone = (DETA * DETA + DPHI * DPHI) > cone_min

        fiducial_cut_sv_ipchi2 = (
            fmath.log(F.IPCHI2.bind(F.CHILD(1, F.ENDVERTEX), F.CHILD(2, F.FORWARDARGS)))
            * LOG10_E
            < log_ipchi2_wrtSV_max
        )

    # first combiner: X pi-
    comb_1 = ParticleCombiner(
        Inputs=[candidate, extra_particles],
        name="rd_vtxiso_combiner_addtrack_1_{hash}",
        DecayDescriptor="[B*+ -> B*0 pi-]cc",
        CombinationCut=F.require_all(fiducial_cut_cone, fiducial_cut_sv_ipchi2),
        CompositeCut=fiducial_cut_pv_dist,
    )
    # second combiner: X pi+
    comb_2 = ParticleCombiner(
        Inputs=[candidate, extra_particles],
        name="rd_vtxiso_combiner_addtrack_2_{hash}",
        DecayDescriptor="[B*- -> B*0 pi+]cc",
        CombinationCut=F.require_all(fiducial_cut_cone, fiducial_cut_sv_ipchi2),
        CompositeCut=fiducial_cut_pv_dist,
    )
    # merge two combiners
    comb = ParticleContainersMerger([comb_1, comb_2], name=tes_name + "_{hash}")

    return comb


@configurable
def select_parts_for_isolation(
    names=[],
    candidates=[],
    cut=(F.DR2 < 0.25),
    LongTrackIso=True,
    TTrackIso=False,
    DownstreamTrackIso=False,
    UpstreamTrackIso=False,
    NeutralIso=True,
    PiZerosIso=False,
):
    """
    Add to the extra_outputs different kind of isolations by properly setting the given flag.
    Returns selection of extra particles for isolation.

    Args:
        names: List of strings with prefix of the TES location for extra selection
        candidates: List of containers of reference particles to relate extra particles
        cut: Predicate to select extra information to persist. By default: cone geometry with max dr2=1
        LongTrackIso: Boolean flag to make isolation with long tracks. By default True.
        TTrackIso: Boolean flag to make isolation with tt tracks. By default False.
        DownstreamTrackIso: Boolean flag to make isolation with downstream tracks. By default False.
        UpstreamTrackIso: Boolean flag to make isolation with upstream tracks. By default False.
        NeutralIso: Boolean flag to make isolation with neutral particles. By default True.
    """
    extra_outputs = []
    assert len(names) == len(candidates), (
        "Different number of names and candidate containers for particle isolation!"
    )

    for name, cand in zip(names, candidates):
        if LongTrackIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_LongTrackIsolation",
                extra_particles=make_long_pions(),
                ref_particles=cand,
                selection=cut,
            )
        if TTrackIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_TTrackIsolation",
                extra_particles=make_ttrack_pions(),
                ref_particles=cand,
                selection=cut,
            )
        if DownstreamTrackIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_DownstreamTrackIsolation",
                extra_particles=make_down_pions(),
                ref_particles=cand,
                selection=cut,
            )
        if UpstreamTrackIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_UpstreamTrackIsolation",
                extra_particles=make_up_pions(),
                ref_particles=cand,
                selection=cut,
            )
        if NeutralIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_NeutralIsolation",
                extra_particles=make_photons(),
                ref_particles=cand,
                selection=cut,
            )
        if PiZerosIso:
            extra_outputs += extra_outputs_for_isolation(
                name=name + "_PiZerosIsolation",
                extra_particles=make_merged_pi0s(),
                ref_particles=cand,
                selection=cut,
            )
    return extra_outputs


def select_combinations_for_vertex_isolation(
    name,
    candidate,
    max_two_body_vtx_cut: float = 9.0,
    max_three_body_vtx_cut: float = 15.0,
    use_fiducial_cut: bool = True,
):
    """
    Add to the extra_outputs vertex isolations when adding one track or two tracks to the vertex fit.
    By default only two(three)-body combinations with vertex fit chi2 smaller than 9.(15.) are selected.
    Returns TES location with two and three-body combinations for vertex isolation

    Args:
        name: String with prefix of the TES location for extra selection
        candidate: Container of reference particles to relate extra particles
        max_two_body_vtx_cut(float): Set the maximum value of the 2-body vertex fit chi2 (default value 9.)
        max_three_body_vtx_cut(float): Set the maximum value of the 3-body vertex fit chi2 (default value 15.)
        use_fiducial_cut(bool): Use preselection requirements in combinations (default value True)
    """

    two_body_comb = make_two_body_combination(candidate, "rd_vtxiso_combiner_onetrack")
    three_body_comb = make_two_body_combination(
        two_body_comb, "rd_vtxiso_combiner_twotracks"
    )

    extra_outputs = extra_outputs_for_isolation(
        name=name + "_OneTrackCombination_VertexIsolation",
        extra_particles=two_body_comb,
        ref_particles=candidate,
        selection=(
            F.ABS @ (F.CHI2() @ F.FORWARDARG1() - F.CHI2() @ F.FORWARDARG0())
            < max_two_body_vtx_cut
        ),
    )

    extra_outputs += extra_outputs_for_isolation(
        name=name + "_TwoTracksCombination_VertexIsolation",
        extra_particles=three_body_comb,
        ref_particles=candidate,
        selection=(
            F.ABS @ (F.CHI2() @ F.FORWARDARG1() - F.CHI2() @ F.FORWARDARG0())
            < max_three_body_vtx_cut
        ),
    )

    return extra_outputs


def parent_isolation_output(name, candidate):
    """
    Add default isolation for parent particle
    Args:
        name: String with prefix of the TES location for extra selection
        candidate: Containers of reference particles to relate extra particles
    """

    return select_parts_for_isolation(
        names=[name], candidates=[candidate], cut=((F.DR2 < 0.25) & ~F.FIND_IN_TREE())
    )


def parent_and_children_isolation(
    parents, decay_products, parents_DR2_size=0.25, decay_products_DR2_size=0.25
):
    """

    Add isolation information around the parent and children tracks

    Args:
        parents: Dictionary of TES location names (key) and container (value) of reference particles relate extra particles on parents
        decay_products: Dictionary of TES location names  (key) and container (value)  of reference particles to relate extra particles on children tracks
        DR2_size: a tuple of size two containing the size of the isolation cone to store extra info, see Functor.DR2
    """
    (p_names, p_candidates) = zip(*parents.items())
    (d_names, d_candidates) = zip(*decay_products.items())

    iso_parts = select_parts_for_isolation(
        names=p_names,
        candidates=p_candidates,
        cut=F.require_all(
            F.DR2 < parents_DR2_size,
            ~F.FIND_IN_TREE(),
        ),
    )

    iso_parts += select_parts_for_isolation(
        names=d_names,
        candidates=d_candidates,
        cut=F.require_all(
            F.DR2 < decay_products_DR2_size,
            ~F.SHARE_TRACKS(),
        ),
    )
    return iso_parts
