###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Author: Raja Nandakumar, Fergus Wilson
Contact: raja.nandakumar@cern.ch
Date: 29/03/2022

Extra builders :
make_prompt_tautau : For Upsilon(1S) -> tau+ tau-
    - Invariant mass : [7500, 13000] MeV
    - Cos(opening angle) > 0.95
    - PT > 1.5 GeV
    - Logarithm of vertex separation chi2: ~CHI2>-1.5
    - Tau cuts :
      - Invariant Mass : [900, 1700] MeV
      - PID_K <= 0.0
      - Invariant Mass of neutral two-pion combinations: [200, 1500] MeV
      - P > 3 GeV

make_prompt_etau : For Upsilon(1S) -> e+ tau-
    - Invariant mass : [7500, 12000] MeV
    - Cos(opening angle) > 0.95
    - PT > 2.5 GeV
    - Tau cuts :
      - Invariant Mass : [900, 1700] MeV
      - Invariant Mass of neutral two-pion combinations: [200, 1500] MeV
      - PID_K <= 0.0
      - P > 3 GeV
    - electron cuts:
      - PID_E > 3
      - PT > 1.5 GeV
      - P > 3 GeV

make_prompt_mutau: For Upsilon(1S) -> mu+ tau-
    - Invariant mass : [7500, 13000] MeV
    - PT > 1 GeV
    - Tau cuts :
      - Invariant Mass : [900, 1700] MeV
      - PID_K <= 0.0
      - Invariant Mass of neutral two-pion combinations: [200, 1500] MeV
      - P > 3 GeV
    - muon cuts:
      - PID_MU > 2.0, ISMUON
      - P > 3 GeV

make_upsilons_to_upsilons : For Upsilon(2S) -> Upsilon(1S) pi+ pi-
    - Invariant mass : [8000, 13000] MeV
    - PT > 1 GeV
    - Pion cuts :
      - PID_K <= 0.0
      - PT > 250 MeV
"""

from __future__ import absolute_import, division, print_function

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV
from PyConf import configurable
from PyConf.Tools import ParticleVertexFitter
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.rdbuilder_thor import (
    make_rd_detached_pions,
    make_rd_prompt_electrons,
    make_rd_prompt_muons,
    make_rd_tauons_hadronic_decay,
)
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.rd.builders.b_to_xtaul_rd_builder import get_taus_log_vtxsepchi2


@configurable
def make_prompt_tautau(
    *,  # we do not allow positional arguments
    parent_id: str,  # this is a keyword argument that must always be provided
    name="prompt_tautau_builder_{hash}",
    min_dilepton_mass=7500.0 * MeV,
    max_dilepton_mass=13000.0 * MeV,
    min_dilepton_pt=1.5 * GeV,
    max_ipchi2_tautau=9,
    max_adocachi2=25.0,
    min_bpvvdchi2=0.0,  # must be 0 for a prompt builder
    cos_tautauangle=0.95,
    taus_log_vtxsepchi2_min=-1.0,
    min_p_tau=6.0 * GeV,
):
    pvs = make_pvs()
    descriptor = "{} -> tau+ tau-".format(parent_id)
    taus = ParticleFilter(
        make_rd_tauons_hadronic_decay(
            pi_pt_min=300 * MeV,
            pi_p_min=6000 * MeV,
            pi_ipchi2_min=3.0,
            best_pi_pt_min=800 * MeV,
            best_pi_ipchi2_min=5.0,
            pi_pid=F.PID_K <= 0.0,
            am_min=900 * MeV,
            am_max=1700 * MeV,
            am_2pi_min=200 * MeV,
            am_2pi_max=1500 * MeV,
            vchi2pdof_max=6.0,
        ),
        F.FILTER(F.P > min_p_tau),
        name="prompt_tau_builder_{hash}",
    )

    combination_code = F.require_all(
        F.MAXSDOCACHI2CUT(max_adocachi2),
        F.ALV(1, 2) > cos_tautauangle,
        get_taus_log_vtxsepchi2() > taus_log_vtxsepchi2_min,
    )
    vertex_code = F.require_all(
        F.BPVFDCHI2(pvs) > min_bpvvdchi2,
        in_range(min_dilepton_mass, F.MASS, max_dilepton_mass),
        F.PT > min_dilepton_pt,
        F.BPVIPCHI2(pvs) < max_ipchi2_tautau,
    )
    tautau = ParticleCombiner(
        name=name,
        Inputs=[taus, taus],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
    return tautau


#####


@configurable
def make_prompt_etau(
    *,  # we do not allow positional arguments
    parent_id: str,  # this is a keyword argument that must always be provided
    name="prompt_etau_builder",
    min_dilepton_mass=8000.0 * MeV,
    max_dilepton_mass=12000.0 * MeV,
    min_dilepton_pt=2.5 * GeV,
    max_ipchi2_etau=9,
    min_pid_e=3.0,
    min_pt_e=1.5 * GeV,
    min_p_e=6 * GeV,
    max_adocachi2=20.0,
    min_bpvvdchi2=0.0,  # must be 0 for a prompt builder
    cos_etauangle=0.95,
    min_p_tau=6 * GeV,
):
    """
    Make the detached electron-tau pair, opposite-sign for now.
    """
    pvs = make_pvs()
    descriptor = "[{} -> tau- e+]cc".format(parent_id)
    # taus = make_upsilon_taus()
    taus = ParticleFilter(
        make_rd_tauons_hadronic_decay(
            pi_pt_min=350 * MeV,
            pi_p_min=6000 * MeV,
            pi_ipchi2_min=3.0,
            best_pi_pt_min=800 * MeV,
            best_pi_ipchi2_min=5.0,
            pi_pid=F.PID_K <= 0.0,
            am_min=900 * MeV,
            am_max=1700 * MeV,
            am_2pi_min=200 * MeV,
            am_2pi_max=1500 * MeV,
            vchi2pdof_max=6.0,
        ),
        F.FILTER(F.P > min_p_tau),
        name="prompt_tau_builder_{hash}",
    )
    electrons = make_rd_prompt_electrons(
        pt_min=min_pt_e, p_min=min_p_e, pid=(F.PID_E > min_pid_e)
    )
    combination_code = F.require_all(
        F.MAXSDOCACHI2CUT(max_adocachi2),
        F.ALV(1, 2) > cos_etauangle,
    )
    vertex_code = F.require_all(
        F.BPVFDCHI2(pvs) > min_bpvvdchi2,
        in_range(min_dilepton_mass, F.MASS, max_dilepton_mass),
        F.PT > min_dilepton_pt,
        F.BPVIPCHI2(pvs) < max_ipchi2_etau,
    )
    etau = ParticleCombiner(
        name=name,
        Inputs=[taus, electrons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
    return etau


#####


@configurable
def make_prompt_mutau(
    *,  # we do not allow positional arguments
    parent_id: str,  # this is a keyword argument that must always be provided
    name="prompt_mutau_builder",
    min_dilepton_mass=7000.0 * MeV,
    max_dilepton_mass=13000.0 * MeV,
    min_dilepton_pt=1.0 * GeV,
    max_ipchi2_mutau=50,
    min_PIDmu=2.0,
    IsMuon=True,
    min_pt_mu=0.0 * GeV,
    min_p_mu=5.0 * GeV,
    max_adocachi2=30.0,
    min_bpvvdchi2=0.0,  # must be 0 for a prompt builder
    min_p_tau=5.0 * GeV,
):
    """
    Make the detached muon-tau pair, opposite-sign for now.
    """
    pvs = make_pvs()
    descriptor = "[{} -> mu+ tau-]cc".format(parent_id)
    taus = ParticleFilter(
        make_rd_tauons_hadronic_decay(
            pi_pt_min=350 * MeV,  # possible tighter
            pi_p_min=6000 * MeV,
            pi_ipchi2_min=6.0,  # possible tighter
            best_pi_pt_min=800 * MeV,  # possible tighter
            best_pi_ipchi2_min=9.0,
            pi_pid=F.PID_K <= 0.0,
            am_min=900 * MeV,  # raja suggestion
            am_max=1700 * MeV,
            am_2pi_min=200 * MeV,
            am_2pi_max=1500 * MeV,
            vchi2pdof_max=6.0,
        ),
        F.FILTER(F.P > min_p_tau),
        name="prompt_tau_builder_{hash}",
    )

    if IsMuon:
        muons = make_rd_prompt_muons(
            pt_min=min_pt_mu,
            p_min=min_p_mu,
            pid=F.require_all(F.PID_MU > min_PIDmu, F.ISMUON),
        )
    else:
        muons = make_rd_prompt_muons(
            pt_min=min_pt_mu, p_min=min_p_mu, pid=(F.PID_MU > min_PIDmu)
        )
    combination_code = F.MAXSDOCACHI2CUT(max_adocachi2)
    vertex_code = F.require_all(
        F.BPVFDCHI2(pvs) > min_bpvvdchi2,
        in_range(min_dilepton_mass, F.MASS, max_dilepton_mass),
        F.PT > min_dilepton_pt,
        F.BPVIPCHI2(pvs) < max_ipchi2_mutau,
    )
    mutau = ParticleCombiner(
        name=name,
        Inputs=[muons, taus],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
    return mutau


#####
@configurable
def make_upsilons_to_upsilons(
    upsilon1s,
    name="upsilon_ll_builder_{hash}",
    min_dilepton_mass=7000.0 * MeV,
    max_dilepton_mass=13000.0 * MeV,
    min_dilepton_pt=1.0 * GeV,
    min_bpvvdchi2=0.0,  # must be 0 for a prompt builder
    max_adocachi2=25.0,
    minipchi2=0,  # must be 0 for a prompt builder
    # pion cut
    pi_pt_min=500 * MeV,
    pi_ipchi2_min=0.0,  # check if you want to filter on MINIPCHI2(pvs) > 0. or MINIPCHI2(pvs) > 1.
    pi_pid=F.PID_K <= 0,
    max_vchi2ndof=9.0,
    useDTFMassConstraint=False,
):
    # The `make_rd_detached_pions` function is used to create a filter for selecting detached pions
    # based on certain criteria. Here is an explanation of the parameters used in this function:
    pion = make_rd_detached_pions(
        pt_min=pi_pt_min, mipchi2dvprimary_min=pi_ipchi2_min, pid=pi_pid
    )

    pvs = make_pvs()
    combination_code = F.require_all(
        in_range(min_dilepton_mass, F.MASS, max_dilepton_mass),
        F.MAXSDOCACHI2CUT(max_adocachi2),
    )

    vertex_code = F.require_all(
        in_range(min_dilepton_mass, F.MASS, max_dilepton_mass),
        F.PT > min_dilepton_pt,
        F.BPVFDCHI2(pvs) > min_bpvvdchi2,
        F.CHI2DOF < max_vchi2ndof,
    )
    if useDTFMassConstraint:
        # Might be useful expecially for rate reductions
        upsilon2s = ParticleCombiner(
            [upsilon1s, pion, pion],
            name=name,
            DecayDescriptor="Upsilon(2S) -> Upsilon(1S) pi+ pi-",
            CombinationCut=combination_code,
            CompositeCut=vertex_code,
            ParticleCombiner=ParticleVertexFitter(MassConstraints=["Upsilon(1S)"]),
        )
    else:
        upsilon2s = ParticleCombiner(
            [upsilon1s, pion, pion],
            name=name,
            DecayDescriptor="Upsilon(2S) -> Upsilon(1S) pi+ pi-",
            CombinationCut=combination_code,
            CompositeCut=vertex_code,
        )
    return upsilon2s
