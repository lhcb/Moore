###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of rare tau decay lines for the RD working group

The following modes are included:

    - Tau -> Mu Mu Mu
    - Tau -> Mu Mu E (OS+SS)
    - Tau -> Mu E E (OS+SS)
    - Tau -> Mu (Gamma -> E E)
    - Tau -> E E E
    - Tau -> P Mu Mu (OS+SS)
    - Tau -> (Phi(1020) -> K K) Mu
    - Tau -> 5 Mu
    - Tau -> 7 Mu

The following control modes are included:

    - Ds -> (Phi -> Mu Mu) Pi
    - Ds -> (Phi -> E E) Pi

"""

import Functors as F
from GaudiKernel.SystemOfUnits import MeV
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleContainersMerger, ParticleFilter
from RecoConf.rdbuilder_thor import (
    make_rd_detached_dielectron,
    make_rd_detached_electrons,
    make_rd_detached_muons,
    make_rd_detached_phis,
    make_rd_detached_pions,
    make_rd_has_rich_detached_protons,
)

from Hlt2Conf.lines.rd.builders.rd_isolation import (
    parent_and_children_isolation,
    parent_isolation_output,
)

from .builders.rare_tau_decay_builders import (
    _make_phi_to_2mu,
    _make_tau_MVA_functor,
    _make_tau_to_2body,
    _make_tau_to_3body,
    _make_tau_to_nmu,
)
from .builders.rd_prefilters import (
    _RD_MONITORING_VARIABLES,
    _VRD_MONITORING_VARIABLES,
    RD_PERSIST_CALO_CLUSTERS,
    RD_PERSIST_CALO_DIGITS,
    rd_prefilter,
)

all_lines = {}

track_cuts = {"mipchi2dvprimary_min": 9, "pt_min": 300 * MeV}


@register_line_builder(all_lines)
@configurable
def tau_to_mumumu_line(name="Hlt2RD_TauToMuMuMu", prescale=1, persistreco=False):
    descriptor = "[tau+ -> mu+ mu+ mu-]cc"
    muons = make_rd_detached_muons(**track_cuts, pid=(F.PID_MU > 2))
    tau = _make_tau_to_3body(
        muons,
        muons,
        muons,
        descriptor,
        pid_cut=F.ISMUON,
        min_particle_with_pid=2,
        name="Hlt2RD_TauToMuMuMu_Builder",
        max_ipchi2=225,
    )
    iso_parts = parent_and_children_isolation(
        parents={"Tau": tau}, decay_products={"muons": muons}
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [tau],
        extra_outputs=iso_parts,
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def tau_to_mumue_line(name="Hlt2RD_TauToMuMuE", prescale=0.1, persistreco=False):
    descriptor_os = "[tau+ -> mu+ mu- e+]cc"
    descriptor_ss = "[tau+ -> mu+ mu+ e-]cc"
    muons = make_rd_detached_muons(
        **track_cuts, pid=F.require_all(F.ISMUON, (F.PID_MU > 2))
    )
    electrons = make_rd_detached_electrons(**track_cuts, pid=(F.PID_E > 4))
    tau_os = _make_tau_to_3body(
        muons, muons, electrons, descriptor_os, name="Hlt2RD_TauToMuMuE_OS_Builder"
    )
    tau_ss = _make_tau_to_3body(
        muons, muons, electrons, descriptor_ss, name="Hlt2RD_TauToMuMuE_SS_Builder"
    )
    tau = ParticleContainersMerger([tau_os, tau_ss])
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [tau],
        extra_outputs=parent_isolation_output("Tau", tau),
        prescale=prescale,
        persistreco=persistreco,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def tau_to_muee_line(name="Hlt2RD_TauToMuEE", prescale=1, persistreco=False):
    descriptor_os = "[tau+ -> mu+ e+ e-]cc"
    descriptor_ss = "[tau+ -> mu- e+ e+]cc"
    muons = make_rd_detached_muons(
        **track_cuts, pid=F.require_all(F.ISMUON, (F.PID_MU > 2))
    )
    electrons = make_rd_detached_electrons(**track_cuts, pid=(F.PID_E > 4))
    tau_os = _make_tau_to_3body(
        muons, electrons, electrons, descriptor_os, name="Hlt2RD_TauToMuEE_OS_Builder"
    )
    tau_ss = _make_tau_to_3body(
        muons, electrons, electrons, descriptor_ss, name="Hlt2RD_TauToMuEE_SS_Builder"
    )
    tau = ParticleContainersMerger([tau_os, tau_ss])
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [tau],
        extra_outputs=parent_isolation_output("Tau", tau),
        prescale=prescale,
        persistreco=persistreco,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def tau_to_mugamma_ee_line(
    name="Hlt2RD_TauToMuGamma_EE", prescale=1, persistreco=False
):
    descriptor = "[tau+ -> mu+ gamma]cc"
    muons = make_rd_detached_muons(
        **track_cuts, pid=F.require_all(F.ISMUON, (F.PID_MU > 2))
    )
    gammas = make_rd_detached_dielectron(
        parent_id="gamma",
        am_min=0 * MeV,
        am_max=100 * MeV,
        pt_e_min=300 * MeV,
        pid_e_min=4,
        ipchi2_e_min=9,
        with_brem=False,
        combination_policy="same_type_or_at_least_one_long",
        track_types="all",
    )
    tau = _make_tau_to_2body(
        muons,
        gammas,
        descriptor,
        min_mass=1378 * MeV,
        max_mass=2078 * MeV,
        name="Hlt2RD_TauToMuGamma_Builder",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [tau],
        extra_outputs=parent_isolation_output("Tau", tau),
        prescale=prescale,
        persistreco=persistreco,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def tau_to_mugamma_ee_mva_line(
    name="Hlt2RD_TauToMuGamma_EE_MVA", prescale=1, persistreco=False
):
    descriptor = "[tau+ -> mu+ gamma]cc"
    track_cuts_bdt = {"mipchi2dvprimary_min": 4, "pt_min": 250 * MeV}
    muons = make_rd_detached_muons(
        **track_cuts_bdt, pid=F.require_all(F.ISMUON, (F.PID_MU > 0))
    )
    gammas = make_rd_detached_dielectron(
        parent_id="gamma",
        am_min=0 * MeV,
        am_max=100 * MeV,
        pt_e_min=250 * MeV,
        pid_e_min=0,
        ipchi2_e_min=4,
        with_brem=False,
        combination_policy="same_type_or_at_least_one_long",
        track_types="all",
    )
    tau = _make_tau_to_2body(
        muons,
        gammas,
        descriptor,
        min_mass=1378 * MeV,
        max_mass=2078 * MeV,
        name="Hlt2RD_TauToMuGamma_MVA_Builder",
    )

    mva_filter = _make_tau_MVA_functor(line=name) > 0.5
    tau_mva = ParticleFilter(tau, F.FILTER(mva_filter), name=name + "_selection")

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [tau_mva],
        extra_outputs=parent_isolation_output("Tau", tau_mva),
        prescale=prescale,
        persistreco=persistreco,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def tau_to_eee_line(name="Hlt2RD_TauToEEE", prescale=1, persistreco=False):
    descriptor = "[tau+ -> e+ e+ e-]cc"
    electrons = make_rd_detached_electrons(**track_cuts, pid=(F.PID_E > 4))
    tau = _make_tau_to_3body(
        electrons, electrons, electrons, descriptor, name="Hlt2RD_TauToEEE_Builder"
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [tau],
        extra_outputs=parent_isolation_output("Tau", tau),
        prescale=prescale,
        persistreco=persistreco,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def tau_to_pmumu_line(name="Hlt2RD_TauToPMuMu", prescale=0.1, persistreco=False):
    descriptor_os = "[tau+ -> p+ mu+ mu-]cc"
    descriptor_ss = "[tau+ -> p~- mu+ mu+]cc"
    muons = make_rd_detached_muons(
        **track_cuts, pid=F.require_all(F.ISMUON, (F.PID_MU > 2))
    )
    protons = make_rd_has_rich_detached_protons(**track_cuts)
    tau_os = _make_tau_to_3body(
        protons, muons, muons, descriptor_os, name="Hlt2RD_TauToPMuMu_OS_Builder"
    )
    tau_ss = _make_tau_to_3body(
        protons, muons, muons, descriptor_ss, name="Hlt2RD_TauToPMuMu_SS_Builder"
    )
    tau = ParticleContainersMerger([tau_os, tau_ss])
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [tau],
        extra_outputs=parent_isolation_output("Tau", tau),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def tau_to_phimu_line(name="Hlt2RD_TauToPhiMu", prescale=1, persistreco=False):
    """
    Modified by Domenico Riccardi
    Contact: domenico.riccardi@cern.ch
    """
    descriptor = "[tau+ -> phi(1020) mu+]cc"
    muons = make_rd_detached_muons(
        **track_cuts, pid=F.require_all(F.ISMUON, (F.PID_MU > -5))
    )
    phis = make_rd_detached_phis(
        am_min=1000 * MeV,
        am_max=1040 * MeV,
        k_pt_min=300 * MeV,
        k_ipchi2_min=4,
        k_pid=(F.PID_K > 4),
        vchi2pdof_max=4,
    )
    tau = _make_tau_to_2body(
        phis,
        muons,
        descriptor,
        name="Hlt2RD_TauToPhiMu_Builder",
        min_mass=1670 * MeV,
        max_mass=2040 * MeV,
        max_ipchi2=25,
        max_vtxchi2dof=4,
        min_pt=3000.0 * MeV,
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [tau],
        extra_outputs=parent_isolation_output("Tau", tau),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def tau_to_5mu_line(name="Hlt2RD_TauTo5Mu", prescale=1, persistreco=False):
    descriptor = "[tau+ -> mu+ mu+ mu+ mu- mu-]cc"
    muons = make_rd_detached_muons(
        **track_cuts, pid=F.require_all(F.ISMUON, (F.PID_MU > 2))
    )
    muons_loose = make_rd_detached_muons(**track_cuts, pid=F.ISMUON)
    tau = _make_tau_to_nmu(
        muons, muons_loose, descriptor, name="Hlt2RD_TauTo5Mu_Builder", n_muons=5
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [tau],
        extra_outputs=parent_isolation_output("Tau", tau),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def tau_to_7mu_line(name="Hlt2RD_TauTo7Mu", prescale=1, persistreco=False):
    descriptor = "[tau+ -> mu+ mu+ mu+ mu+ mu- mu- mu-]cc"
    muons = make_rd_detached_muons(
        **track_cuts, pid=F.require_all(F.ISMUON, (F.PID_MU > 2))
    )
    muons_loose = make_rd_detached_muons(**track_cuts, pid=F.ISMUON)
    tau = _make_tau_to_nmu(
        muons, muons_loose, descriptor, name="Hlt2RD_TauTo7Mu_Builder", n_muons=7
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [tau],
        extra_outputs=parent_isolation_output("Tau", tau),
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def ds_to_phipi_mumu_line(
    name="Hlt2RD_DsToPhiPi_PhiToMuMu", prescale=1, persistreco=False
):
    descriptor = "[D_s+ -> phi(1020) pi+]cc"
    pions = make_rd_detached_pions(**track_cuts)
    muons = make_rd_detached_muons(
        name="rd_detached_muons_{hash}",
        **track_cuts,
        p_min=0.0 * MeV,
        pid=F.require_all(F.ISMUON, F.PID_MU > 2),
    )
    phis = _make_phi_to_2mu(
        muons,
        name="rd_PhiToMuMu_Builder_{hash}",
        parent_id="phi(1020)",
        pt_dimuon_min=0.0 * MeV,
        adocachi2cut_max=30.0,
        bpvvdchi2_min=30.0,
        vchi2pdof_max=30.0,
        am_min=950 * MeV,
        am_max=1100.0 * MeV,
    )
    ds = _make_tau_to_2body(
        phis,
        pions,
        descriptor,
        name="Hlt2RD_DsToPhiPi_PhiToMuMu_Builder",
        min_mass=1718 * MeV,
        max_mass=2218 * MeV,
    )
    iso_parts = parent_and_children_isolation(
        parents={"Ds": ds},
        decay_products={
            "muons": muons,
            "pions": pions,
        },
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ds],
        extra_outputs=iso_parts,
        prescale=prescale,
        persistreco=persistreco,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
@configurable
def ds_to_phipi_ee_line(name="Hlt2RD_DsToPhiPi_PhiToEE", prescale=1, persistreco=False):
    descriptor = "[D_s+ -> phi(1020) pi+]cc"
    pions = make_rd_detached_pions(**track_cuts)
    phis = make_rd_detached_dielectron(
        parent_id="phi(1020)",
        am_min=950 * MeV,
        am_max=1100 * MeV,
        pt_e_min=300 * MeV,
        pid_e_min=2,
    )
    ds = _make_tau_to_2body(
        phis,
        pions,
        descriptor,
        name="Hlt2RD_DsToPhiPi_PhiToEE_Builder",
        min_mass=1718 * MeV,
        max_mass=2218 * MeV,
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ds],
        prescale=prescale,
        persistreco=persistreco,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_VRD_MONITORING_VARIABLES,
    )
