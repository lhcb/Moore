###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Lines to study strange decays:

* KS0 -> l+ l-
* KS0 -> (X0) mu+ mu-
* KS0 -> l+(pi+) l-(pi-) l'+ l'-
* [KS0 -> pi+ l- nu_l~]cc
* [K+ -> pi+ l+ l-]cc
* [K+ -> pi- l+ l+]cc
* [K+ -> pi+ mu± e±]cc
* [K+ -> pi+ [mu+ e-]cc]cc
* [Lambda0 -> p l- nu_l~]cc
* [Lambda0 -> p pi e e]cc
* [Lambda0 -> p pi]cc
* [Lambda0 -> p pi gamma]cc
* [Sigma+ -> p l+ l-]cc , [Sigma+ -> p~- mu+ mu+]cc
* [Sigma+ -> p+ pi0]cc  , [Sigma+ -> p gamma]cc
* [Sigma+ -> p+ [mu+ e-]cc]cc
* Sigma+ -> p+ e+ e- e+ e- , Sigma+ -> p+ mu+ mu- e+ e-
* [Xi0 -> p pi-]cc
* [Xi- -> p pi- pi-]cc
* [Xi- -> Lambda0 l- nu_l~]cc
* [Omega- -> Lambda0 pi-]cc

Contacts:

- Miguel Ramos Pernas (miguel.ramos.pernas@cern.ch)
- Sergio Arguedas Cuendis (sergio.arguedas.cuendis@cern.ch)
- Frank Xiangyu Liu (frank.liu@monash.edu)
- Francesca Dordei (francesca.dordei@cern.ch)
- Francesco Dettori (francesco.dettori@cern.ch)
- Daniele Provenzano (daniele.provenzano@cern.ch)

"""

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable

from Hlt2Conf.lines.rd.builders import strange_builders
from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter

all_lines = {}  # all the lines, updated at the end of the python module
signal_lines = {}  # lines targeting signal decay modes
control_lines = {}  # lines for calibration and normalization

#
# These two lines define particles to be used as proxies for exotic signatures. The
# vertex fitter works differently for resonances and long-lived particles, so lines
# must be fed with these strings with caution. e.g. in the decay
# KS0 -> J/psi(1S) J/psi(1S), the vertex of the KS0 might end-up being built with
# the descendants of the J/psi(1S) particles rather than from the extrapolation of
# the J/psi(1S) trajectories, whereas for KS0 -> J/psi(1S) KS0 it will be built
# using the J/psi(1S) descendants and the KS0 trajectory. Despite we must explicitly
# specify containers for each part of a decay descriptor, we can not provide
# two different containers with the same PID (e.g. combining two J/psi(1S) where
# the first is built from muons and the second with electrons). Therefore we provide
# an alternative.
#
SHORT_LIVED = "phasespa"  # tau=0ns, self-cc
LONG_LIVED = "geantino"  # stable, self-cc
LONG_LIVED_ALT = "opticalphoton"  # stable, self-cc

###################################
# CONTROL AND NORMALIZATION MODES #
###################################


@register_line_builder(control_lines)
@configurable
def ks02pipi_line(name="Hlt2RD_KS0ToPiPi", prescale=1e-3):
    """
    Normalization mode for KS0 decays with two particles in the
    final state
    """
    pions = strange_builders.make_pions()
    ks0 = strange_builders.make_combination(
        "KS0 -> pi+ pi-",
        [pions, pions],
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon"],
    )


@register_line_builder(control_lines)
@configurable
def ks02pipi_detached_line(name="Hlt2RD_KS0ToPiPi_Detached", prescale=1e-3):
    """Normalization mode for KS0 decays that do not come from a PV"""
    pions = strange_builders.make_pions()
    ks0 = strange_builders.make_combination(
        "KS0 -> pi+ pi-",
        [pions, pions],
        build_requirements=strange_builders.requirements_for_detached_combination,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
    )


@register_line_builder(control_lines)
@configurable
def ks02pipi_loose_line(name="Hlt2RD_KS0ToPiPi_Loose", prescale=1e-4):
    """Control mode for analyses using KS0 decays from a PV with loose requirements"""
    pions = strange_builders.make_loose_pions()
    ks0 = strange_builders.make_combination(
        "KS0 -> pi+ pi-",
        [pions, pions],
        build_requirements=strange_builders.loose_requirements_for_prompt_ks0,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
    )


@register_line_builder(control_lines)
@configurable
def ks02pipi_detached_tight_line(name="Hlt2RD_KS0ToPiPi_Detached_Tight", prescale=1):
    """Normalization mode for KS0 decays that do not come from a PV"""
    pions = strange_builders.make_tight_pions()
    ks0 = strange_builders.make_combination(
        "KS0 -> pi+ pi-",
        [pions, pions],
        build_requirements=strange_builders.tight_requirements_for_detached_combination,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
    )


@register_line_builder(control_lines)
@configurable
def ks02mumu_sb_line(name="Hlt2RD_KS0ToMuMu_SB", prescale=1):
    """
    Select doubly misidentified KS0 -> pi+ pi- candidates for the
    Hlt2RD_KS0ToMuMu trigger line
    """
    muons = strange_builders.make_muons()

    # must ensure that the upper mass bound coincides with the lower mass bound
    # of the signal lines

    comb_mass_min = strange_builders.dimuon_min_mass()

    comb_mass_max, _ = strange_builders.ks0_tight_mass_bounds()

    ks0 = strange_builders.make_combination(
        "KS0 -> mu+ mu-",
        [muons, muons],
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=(comb_mass_min, comb_mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon"],
    )


@register_line_builder(control_lines)
@configurable
def ks02mumu_ss_line(name="Hlt2RD_KS0ToMuMu_SS", prescale=1):
    """
    Select same-sign candidates for the Hlt2RD_KS0ToMuMu trigger line
    """
    muons = strange_builders.make_muons()
    ks0 = strange_builders.make_combination(
        "[KS0 -> mu+ mu+]cc",
        [muons, muons],
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon"],
    )


@register_line_builder(control_lines)
@configurable
def ks02emu_ss_line(name="Hlt2RD_KS0ToEMu_SS", prescale=1):
    """
    Select same-sign candidates for the Hlt2RD_KS0ToEMu trigger line
    """
    electrons = strange_builders.make_electrons()
    muons = strange_builders.make_muons()
    ks0 = strange_builders.make_combination(
        "[KS0 -> mu+ e+]cc",
        [muons, electrons],
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(control_lines)
@configurable
def ks02ee_ss_line(name="Hlt2RD_KS0ToEE_SS", prescale=1):
    """
    Select same-sign candidates for the Hlt2RD_KS0ToEE trigger line
    """
    mass_range = strange_builders.ks0_mass_bounds()

    ks0 = strange_builders.filter_combination(
        strange_builders.make_dielectron(
            "KS0",
            mass_range=mass_range,
            opposite_sign=False,
            electron_builder=strange_builders.make_electrons,
        ),
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=mass_range,
        name=f"{name}_DiElectronFilter",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
    )


@register_line_builder(control_lines)
@configurable
def ks02pimu_line(name="Hlt2RD_KS0ToPiMu", prescale=1e-2):
    """
    Select [KS0 -> pi+ mu- (nu_mu~)]cc candidates
    """
    pions = strange_builders.make_pions()
    muons = strange_builders.make_muons()
    ks0 = strange_builders.make_combination(
        "[KS0 -> pi+ mu-]cc",
        [pions, muons],
        build_requirements=strange_builders.requirements_for_detached_combination,
        mass_range=strange_builders.ks0_semileptonic_mass_bounds(
            mass_min=strange_builders.pion_muon_min_mass()
        ),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon"],
    )


@register_line_builder(control_lines)
@configurable
def ks02pimu_loose_line(name="Hlt2RD_KS0ToPiMu_Loose", prescale=1e-3):
    """
    Select [KS0 -> pi+ mu- (nu_mu~)]cc candidates
    """
    pions = strange_builders.make_loose_pions()
    muons = strange_builders.make_loose_muons()
    ks0 = strange_builders.make_combination(
        "[KS0 -> pi+ mu-]cc",
        [pions, muons],
        build_requirements=strange_builders.loose_requirements_for_detached_combination,
        mass_range=strange_builders.ks0_semileptonic_mass_bounds(
            mass_min=strange_builders.pion_muon_min_mass()
        ),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon"],
    )


@register_line_builder(control_lines)
@configurable
def ks02pie_line(name="Hlt2RD_KS0ToPiE", prescale=1e-2):
    """
    Select [KS0 -> pi+ e- (nu_e~)]cc candidates
    """
    pions = strange_builders.make_pions()
    electrons = strange_builders.make_electrons()
    _, mass_max = strange_builders.ks0_mass_bounds()
    ks0 = strange_builders.make_combination(
        "[KS0 -> pi+ e-]cc",
        [pions, electrons],
        build_requirements=strange_builders.requirements_for_detached_combination,
        mass_range=(strange_builders.pion_electron_min_mass(), mass_max),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
    )


@register_line_builder(control_lines)
@configurable
def ks02pie_loose_line(name="Hlt2RD_KS0ToPiE_Loose", prescale=1e-3):
    """
    Select [KS0 -> pi+ e- (nu_e~)]cc candidates
    """
    pions = strange_builders.make_loose_pions()
    electrons = strange_builders.make_loose_electrons()
    _, mass_max = strange_builders.ks0_mass_bounds()
    ks0 = strange_builders.make_combination(
        "[KS0 -> pi+ e-]cc",
        [pions, electrons],
        build_requirements=strange_builders.loose_requirements_for_detached_combination,
        mass_range=(strange_builders.pion_electron_min_mass(), mass_max),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
    )


@register_line_builder(control_lines)
@configurable
def ks02x0mumu_ss_line(name="Hlt2RD_KS0ToX0MuMu_SS_Inclusive", prescale=1):
    """
    Select same-sign candidates for the Hlt2RD_KS0ToX0MuMu_Inclusive
    trigger line
    """
    muons = strange_builders.make_muons()
    _, mass_max = strange_builders.ks0_mass_bounds()
    ks0 = strange_builders.make_combination(
        "[KS0 -> mu+ mu+]cc",
        [muons, muons],
        build_requirements=strange_builders.requirements_for_detached_combination,
        mass_range=(strange_builders.dimuon_min_mass(), mass_max),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon"],
    )


@register_line_builder(control_lines)
@configurable
def kplus2pipipi_line(name="Hlt2RD_KpToPiPiPi", prescale=1e-1):
    """
    Control mode for decays with three charged particles in the
    final state
    """
    pions = strange_builders.make_pions()
    kplus = strange_builders.make_combination(
        "[K+ -> pi+ pi+ pi-]cc",
        [pions, pions, pions],
        build_requirements=strange_builders.requirements_for_prompt_combination,
        mass_range=strange_builders.kplus_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Kplus": kplus}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP"],
    )


@register_line_builder(control_lines)
@configurable
def kplus2pipipi_DS_line(name="Hlt2RD_KpToPiPiPi_Downstream", prescale=1):
    """
    Control mode for decays with three charged particles in the
    final state
    """
    pions = strange_builders.make_downstream_pions()
    kplus = strange_builders.make_combination(
        "[K+ -> pi+ pi+ pi-]cc",
        [pions, pions, pions],
        build_requirements=strange_builders.requirements_for_prompt_combination,
        mass_range=strange_builders.kplus_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Kplus": kplus}, downstream_tracks=True
        ),
        raw_banks=["VP"],
    )


@register_line_builder(control_lines)
@configurable
def ks02pipiee_line(name="Hlt2RD_KS0ToPiPiEE_Loose", prescale=1):
    """Normalization mode for four-body KS0 decays"""
    pions = strange_builders.make_loose_pions()

    mass_min, mass_max = strange_builders.ks0_mass_bounds()

    dielectron = strange_builders.make_dielectron(
        SHORT_LIVED,
        mass_range=(strange_builders.dielectron_min_mass(), mass_max),
        electron_builder=strange_builders.make_loose_electrons,
    )

    ks0 = strange_builders.make_combination(
        f"KS0 -> {SHORT_LIVED} pi+ pi-",
        [dielectron, pions, pions],
        build_requirements=strange_builders.loose_requirements_for_prompt_ks0,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
    )


@register_line_builder(control_lines)
@configurable
def lambda02ppi_line(name="Hlt2RD_Lambda0ToPPi", prescale=1e-3):
    """Control mode for analyses using Lambda0 decays"""
    l0 = strange_builders.build_lambda0()
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [l0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Lambda0": l0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon"],
    )


@register_line_builder(control_lines)
@configurable
def lambda02ppi_DS_line(name="Hlt2RD_Lambda0ToPPi_Downstream", prescale=1e-3):
    """Control mode for analyses using downstream Lambda0 decays"""
    l0 = strange_builders.build_downstream_lambda0()
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [l0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Lambda0": l0}, downstream_tracks=True
        ),
    )


@register_line_builder(control_lines)
@configurable
def lambda02ppi_tight_line(name="Hlt2RD_Lambda0ToPPi_Tight", prescale=1e-1):
    """Control mode for analyses using Lambda0 decays"""
    l0 = strange_builders.build_tight_lambda0()
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [l0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Lambda0": l0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon"],
    )


@register_line_builder(control_lines)
@configurable
def lambda02ppi_tight_DS_line(
    name="Hlt2RD_Lambda0ToPPi_Tight_Downstream", prescale=1e-2
):
    """Control mode for analyses using downstream Lambda0 decays"""
    l0 = strange_builders.build_tight_downstream_lambda0()
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [l0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Lambda0": l0}, downstream_tracks=True
        ),
    )


@register_line_builder(control_lines)
@configurable
def xi02l0pi0_resolved_line(name="Hlt2RD_Xi0ToLambdaPi0Resolved_Tight", prescale=1):
    """Normalization mode for Xi0 decays"""
    l0 = strange_builders.build_tight_lambda0()
    pi0 = strange_builders.make_tight_resolved_neutral_pions()
    xi = strange_builders.make_combination(
        "[Xi0 -> Lambda0 pi0]cc",
        [l0, pi0],
        mass_range=strange_builders.xi_mass_bounds(),
        build_requirements=strange_builders.tight_requirements_for_neutral_combination_no_vertex,
        can_reco_vertex=False,
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL_NO_VERTEX,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {
                # omit the parent particle due to the lack of a vertex
                "Lambda0": l0
            },
            tracks_with_velo_segment=True,
        ),
        raw_banks=["Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(control_lines)
@configurable
def xi02l0pi0_merged_line(name="Hlt2RD_Xi0ToLambdaPi0Merged_Tight", prescale=1):
    """Normalization mode for Xi0 decays"""
    l0 = strange_builders.build_tight_lambda0()
    pi0 = strange_builders.make_tight_merged_neutral_pions()
    xi = strange_builders.make_combination(
        "[Xi0 -> Lambda0 pi0]cc",
        [l0, pi0],
        mass_range=strange_builders.xi_mass_bounds(),
        build_requirements=strange_builders.tight_requirements_for_neutral_combination_no_vertex,
        can_reco_vertex=False,
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL_NO_VERTEX,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {
                # omit the parent particle due to the lack of a vertex
                "Lambda0": l0
            },
            tracks_with_velo_segment=True,
        ),
        raw_banks=["Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(control_lines)
@configurable
def ximinus2l0pi_line(name="Hlt2RD_XiMinusToLambdaPi_Tight", prescale=1):
    """Normalization mode for Xi- decays"""
    xi, l0, _ = strange_builders.build_tight_ximinus(return_all_containers=True)
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Ximinus": xi, "Lambda0": l0}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP"],
    )


@register_line_builder(control_lines)
@configurable
def ximinus2l0pi_LD_line(name="Hlt2RD_XiMinusToLambdaPi_Tight_Long_Down", prescale=1):
    """Normalization mode for Xi- decays
    with a long pion and downstream lambda"""
    l0 = strange_builders.build_tight_downstream_lambda0()
    pions = strange_builders.make_tight_pions()

    xi = strange_builders.make_combination(
        "[Xi- -> Lambda0 pi-]cc",
        [l0, pions],
        build_requirements=strange_builders.requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.xi_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Ximinus": xi, "Lambda0": l0},
            tracks_with_velo_segment=True,
            downstream_tracks=True,
        ),
        raw_banks=["VP"],
    )


@register_line_builder(control_lines)
@configurable
def ximinus2antil0pi_LD_line(
    name="Hlt2RD_XiMinusToAntiLambdaPi_Tight_Long_Down", prescale=1
):
    """Select [Xi- -> Lambda~0 (-> p+ pi-) pi-]cc  (long/downstream) candidates"""

    l0 = strange_builders.build_tight_downstream_lambda0()
    pions = strange_builders.make_tight_pions()
    xi = strange_builders.make_combination(
        "[Xi- -> Lambda~0 pi-]cc",
        [l0, pions],
        build_requirements=strange_builders.requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.xi_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Ximinus": xi, "Lambda0": l0},
            tracks_with_velo_segment=True,
            downstream_tracks=True,
        ),
        raw_banks=["VP"],
    )


@register_line_builder(control_lines)
@configurable
def ximinus2l0pi_DS_line(name="Hlt2RD_XiMinusToLambdaPi_Tight_Downstream", prescale=1):
    """Normalization mode for Xi- downstream decays"""
    l0 = strange_builders.build_tight_downstream_lambda0()
    pions = strange_builders.make_tight_downstream_pions()

    xi = strange_builders.make_combination(
        "[Xi- -> Lambda0 pi-]cc",
        [l0, pions],
        build_requirements=strange_builders.requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.xi_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Ximinus": xi, "Lambda0": l0}, downstream_tracks=True
        ),
        raw_banks=["VP"],
    )


@register_line_builder(control_lines)
@configurable
def ximinus2antil0pi_line(name="Hlt2RD_XiMinusToAntiLambdaPi_Tight", prescale=1):
    """Select [Xi- -> Lambda~0 (-> p+ pi-) pi-]cc  (long) candidates"""

    l0 = strange_builders.build_tight_lambda0()
    pions = strange_builders.make_tight_pions()
    xi = strange_builders.make_combination(
        "[Xi- -> Lambda~0 pi-]cc",
        [l0, pions],
        build_requirements=strange_builders.requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.xi_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Ximinus": xi, "Lambda0": l0}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP"],
    )


@register_line_builder(control_lines)
@configurable
def ximinus2antil0pi_DS_line(
    name="Hlt2RD_XiMinusToAntiLambdaPi_Tight_Downstream", prescale=1
):
    """Select [Xi- -> Lambda~0 (-> p+ pi-) pi-]cc  (downstream) candidates"""

    l0 = strange_builders.build_tight_downstream_lambda0()
    pions = strange_builders.make_tight_downstream_pions()
    xi = strange_builders.make_combination(
        "[Xi- -> Lambda~0 pi-]cc",
        [l0, pions],
        build_requirements=strange_builders.requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.xi_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Ximinus": xi, "Lambda0": l0}, downstream_tracks=True
        ),
        raw_banks=["VP"],
    )


@register_line_builder(control_lines)
@configurable
def omegaminus2l0k_line(name="Hlt2RD_OmegaMinusToLambdaK", prescale=1):
    """Normalization mode for Omega- decays"""
    l0 = strange_builders.build_lambda0()
    kaons = strange_builders.make_kaons()
    omega = strange_builders.make_combination(
        "[Omega- -> Lambda0 K-]cc",
        [l0, kaons],
        build_requirements=strange_builders.requirements_for_prompt_combination,
        mass_range=strange_builders.omega_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [omega],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Omegaminus": omega, "Lambda0": l0}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP"],
    )


@register_line_builder(control_lines)
@configurable
def omegaminus2l0k_tight_line(name="Hlt2RD_OmegaMinusToLambdaK_Tight", prescale=1):
    """Normalization mode for Omega- decays"""
    l0 = strange_builders.build_tight_lambda0()
    kaons = strange_builders.make_tight_kaons()
    omega = strange_builders.make_combination(
        "[Omega- -> Lambda0 K-]cc",
        [l0, kaons],
        build_requirements=strange_builders.tight_requirements_for_prompt_combination,
        mass_range=strange_builders.omega_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [omega],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Omegaminus": omega, "Lambda0": l0}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP"],
    )


@register_line_builder(control_lines)
@configurable
def omegaminus2l0k_downstream_line(
    name="Hlt2RD_OmegaMinusToLambdaK_Downstream", prescale=1
):
    """Normalization mode for Omega- decays"""
    l0 = strange_builders.build_downstream_lambda0()
    kaons = strange_builders.make_downstream_kaons()
    omega = strange_builders.make_combination(
        "[Omega- -> Lambda0 K-]cc",
        [l0, kaons],
        build_requirements=strange_builders.requirements_for_prompt_combination,
        mass_range=strange_builders.omega_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [omega],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Omegaminus": omega, "Lambda0": l0}, downstream_tracks=True
        ),
        raw_banks=["VP"],
    )


@register_line_builder(control_lines)
@configurable
def omegaminus2l0k_long_down_line(
    name="Hlt2RD_OmegaMinusToLambdaK_Long_Down", prescale=1
):
    """Normalization mode for Omega- decays"""
    l0 = strange_builders.build_downstream_lambda0()
    kaons = strange_builders.make_kaons()
    omega = strange_builders.make_combination(
        "[Omega- -> Lambda0 K-]cc",
        [l0, kaons],
        build_requirements=strange_builders.requirements_for_prompt_combination,
        mass_range=strange_builders.omega_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [omega],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Omegaminus": omega, "Lambda0": l0},
            tracks_with_velo_segment=True,
            downstream_tracks=True,
        ),
        raw_banks=["VP"],
    )


#####################
# KS0 TRIGGER LINES #
#####################


@register_line_builder(signal_lines)
@configurable
def ks02mumu_line(name="Hlt2RD_KS0ToMuMu"):
    """
    Select KS0 -> mu+ mu- candidates.

    Control trigger lines:

    - Hlt2RD_KS0ToPiPi => normalization
    - Hlt2RD_KS0ToPiMu => control
    - Hlt2RD_KS0ToMuMu_SB => background
    - Hlt2RD_KS0ToMuMu_SS => background
    """
    muons = strange_builders.make_muons()
    ks0 = strange_builders.make_combination(
        "KS0 -> mu+ mu-",
        [muons, muons],
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=strange_builders.ks0_tight_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def ks02emu_line(name="Hlt2RD_KS0ToEMu"):
    """
    Select [KS0 -> e+ mu-]cc candidates

    Control trigger lines:

    - Hlt2RD_KS0ToPiPi => normalization
    - Hlt2RD_KS0ToPiMu => control
    - Hlt2RD_KS0ToPiE => control
    - Hlt2RD_KS0ToEMu_SS => background
    """
    electrons = strange_builders.make_electrons()
    muons = strange_builders.make_muons()
    ks0 = strange_builders.make_combination(
        "[KS0 -> mu+ e-]cc",
        [muons, electrons],
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def ks02ee_line(name="Hlt2RD_KS0ToEE"):
    """
    Select KS0 -> e+ e- candidates

    Control trigger lines:

    - Hlt2RD_KS0ToPiPi => normalization
    - Hlt2RD_KS0ToPiE => control
    - Hlt2RD_KS0ToEE_SS => background
    """
    mass_range = strange_builders.ks0_mass_bounds()

    ks0 = strange_builders.filter_combination(
        strange_builders.make_dielectron(
            "KS0",
            mass_range=mass_range,
            electron_builder=strange_builders.make_electrons,
        ),
        build_requirements=strange_builders.requirements_for_prompt_ks0,
        mass_range=mass_range,
        name=f"{name}_DiElectronFilter",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def ks02x0mumu_line(name="Hlt2RD_KS0ToX0MuMu_Inclusive"):
    """
    Select KS0 -> X0 mu+ mu- candidates, where the neutral particle
    is not reconstructed. This line is focused on the decays where
    X0 is either a photon or a pi0.

    Control trigger lines:

    - Hlt2RD_KS0ToPiPi_Detached => normalization
    - Hlt2RD_KS0ToPiMu => control
    - Hlt2RD_KS0ToX0MuMu_SS_Inclusive => background
    """
    muons = strange_builders.make_muons()
    _, mass_max = strange_builders.ks0_mass_bounds()
    ks0 = strange_builders.make_combination(
        "KS0 -> mu+ mu-",
        [muons, muons],
        build_requirements=strange_builders.requirements_for_detached_combination,
        mass_range=(strange_builders.dimuon_min_mass(), mass_max),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def ks02pimu_tight_line(name="Hlt2RD_KS0ToPiMu_Tight"):
    """
    Select [KS0 -> pi+ mu- (nu_mu~)]cc candidates

    Control trigger lines:

    - Hlt2RD_KS0ToPiPi_Detached_Tight => normalization
    """
    pions = strange_builders.make_tight_pions()
    muons = strange_builders.make_tight_muons()
    ks0 = strange_builders.make_combination(
        "[KS0 -> pi+ mu-]cc",
        [pions, muons],
        build_requirements=strange_builders.tight_requirements_for_detached_combination,
        mass_range=strange_builders.ks0_semileptonic_mass_bounds(
            mass_min=strange_builders.pion_muon_min_mass()
        ),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def ks02pie_tight_line(name="Hlt2RD_KS0ToPiE_Tight"):
    """
    Select [KS0 -> pi+ e- (nu_e~)]cc candidates

    Control trigger lines:

    - Hlt2RD_KS0ToPiPi_Detached_Tight => normalization
    """
    pions = strange_builders.make_tight_pions()
    electrons = strange_builders.make_tight_electrons()
    ks0 = strange_builders.make_combination(
        "[KS0 -> pi+ e-]cc",
        [pions, electrons],
        build_requirements=strange_builders.tight_requirements_for_detached_combination,
        mass_range=strange_builders.ks0_semileptonic_mass_bounds(
            mass_min=strange_builders.pion_electron_min_mass()
        ),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
    )


@register_line_builder(signal_lines)
@configurable
def ks02mumumumu_line(name="Hlt2RD_KS0ToMuMuMuMu_Loose"):
    """
    Select KS0 -> mu+ mu- mu+ mu- candidates

    Control trigger lines:

    - Hlt2RD_KS0ToPiPiEE_Loose => normalization
    - Hlt2RD_KS0ToPiPi_Loose => normalization
    - Hlt2RD_KS0ToPiMu_Loose => control
    """
    muons = strange_builders.make_loose_muons()
    ks0 = strange_builders.make_combination(
        "KS0 -> mu+ mu+ mu- mu-",
        [muons, muons, muons, muons],
        build_requirements=strange_builders.loose_requirements_for_prompt_ks0,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def ks02pipimumu_line(name="Hlt2RD_KS0ToPiPiMuMu_Loose"):
    """
    Select KS0 -> pi+ pi- mu+ mu- candidates

    Control trigger lines:

    - Hlt2RD_KS0ToPiPiEE_Loose => normalization
    - Hlt2RD_KS0ToPiPi_Loose => normalization
    - Hlt2RD_KS0ToPiMu_Loose => control
    """
    pions = strange_builders.make_loose_pions()
    muons = strange_builders.make_loose_muons()
    ks0 = strange_builders.make_combination(
        "KS0 -> mu+ mu- pi+ pi-",
        [muons, muons, pions, pions],
        build_requirements=strange_builders.loose_requirements_for_prompt_ks0,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def ks02mumuee_line(name="Hlt2RD_KS0ToMuMuEE_Loose"):
    """
    Select KS0 -> mu+ mu- e+ e- candidates

    Control trigger lines:

    - Hlt2RD_KS0ToPiPiEE_Loose => normalization
    - Hlt2RD_KS0ToPiPi_Loose => normalization
    - Hlt2RD_KS0ToPiMu_Loose => control
    - Hlt2RD_KS0ToPiE_Loose => control
    """
    muons = strange_builders.make_loose_muons()

    mass_min, mass_max = strange_builders.ks0_mass_bounds()

    dielectron = strange_builders.make_dielectron(
        SHORT_LIVED,
        mass_range=(strange_builders.dielectron_min_mass(), mass_max),
        electron_builder=strange_builders.make_loose_electrons,
    )

    ks0 = strange_builders.make_combination(
        f"KS0 -> mu+ mu- {SHORT_LIVED}",
        [muons, muons, dielectron],
        build_requirements=strange_builders.loose_requirements_for_prompt_ks0,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def ks02eeee_line(name="Hlt2RD_KS0ToEEEE_Loose"):
    """
    Select KS0 -> e+ e- e+ e- candidates

    Control trigger lines:

    - Hlt2RD_KS0ToPiPiEE_Loose => normalization
    - Hlt2RD_KS0ToPiPi_Loose => normalization
    - Hlt2RD_KS0ToPiE_Loose => control
    """
    electrons = strange_builders.make_loose_electrons()
    # We do not check for overlap of bremsstrahlung photons due to the
    # lack of a multielectron combiner. Using a pair of dielectron makers
    # will produce multiple candidates due to the possibility of
    # swapping electrons/positrons satisfying the selection
    # requirements in the combination.
    ks0 = strange_builders.make_combination(
        "KS0 -> e+ e+ e- e-",
        [electrons, electrons, electrons, electrons],
        build_requirements=strange_builders.loose_requirements_for_prompt_ks0,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def ks02mumuemu_line(name="Hlt2RD_KS0ToMuMuEMu_Loose"):
    """
    Select [KS0 -> mu+ mu- e+ mu-]cc candidates

    Control trigger lines:

    - Hlt2RD_KS0ToPiPiEE_Loose => normalization
    - Hlt2RD_KS0ToPiPi_Loose => normalization
    - Hlt2RD_KS0ToPiMu_Loose => control
    - Hlt2RD_KS0ToPiE_Loose => control
    """
    muons = strange_builders.make_loose_muons()
    electrons = strange_builders.make_loose_electrons()
    ks0 = strange_builders.make_combination(
        "[KS0 -> mu+ mu+ mu- e-]cc",
        [muons, muons, muons, electrons],
        build_requirements=strange_builders.loose_requirements_for_prompt_ks0,
        mass_range=strange_builders.ks0_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def ks02mumumumu_intermediate_neutral_line(
    name="Hlt2RD_KS0ToMuMuMuMu_IntermediateNeutral_Loose",
):
    """
    Select KS0 -> mu+ mu- mu+ mu- candidates where muons can come from
    an intermediate neutral particle.

    Control trigger lines:

    - Hlt2RD_KS0ToPiPiEE_Loose => normalization
    - Hlt2RD_KS0ToPiPi_Loose => normalization
    - Hlt2RD_KS0ToPiMu_Loose => control
    """
    mass_min, mass_max = strange_builders.ks0_mass_bounds()
    x0 = strange_builders.build_loose_dimuon_intermediate_neutral(
        LONG_LIVED, mass_max=mass_max
    )
    ks0 = strange_builders.make_combination(
        f"KS0 -> {LONG_LIVED} {LONG_LIVED}",
        [x0, x0],
        build_requirements=strange_builders.loose_requirements_for_prompt_ks0_with_intermediate_vertex_separation(
            1, 2
        ),
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0, "X0": x0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def ks02mumuee_intermediate_neutral_line(
    name="Hlt2RD_KS0ToMuMuEE_IntermediateNeutral_Loose",
):
    """
    Select KS0 -> mu+ mu- e+ e- candidates where electrons can come from
    an intermediate neutral particle.

    Control trigger lines:

    - Hlt2RD_KS0ToPiPiEE_Loose => normalization
    - Hlt2RD_KS0ToPiPi_Loose => normalization
    - Hlt2RD_KS0ToPiMu_Loose => control
    - Hlt2RD_KS0ToPiE_Loose => control
    """
    mass_min, mass_max = strange_builders.ks0_mass_bounds()

    x01 = strange_builders.build_loose_dimuon_intermediate_neutral(
        LONG_LIVED, mass_max=mass_max
    )
    x02 = strange_builders.build_loose_dielectron_intermediate_neutral(
        LONG_LIVED_ALT, mass_max=mass_max
    )

    ks0 = strange_builders.make_combination(
        f"KS0 -> {LONG_LIVED} {LONG_LIVED_ALT}",
        [x01, x02],
        build_requirements=strange_builders.loose_requirements_for_prompt_ks0_with_intermediate_vertex_separation(
            1, 2
        ),
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0, "X01": x01, "X02": x02}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def ks02eeee_intermediate_neutral_line(
    name="Hlt2RD_KS0ToEEEE_IntermediateNeutral_Loose",
):
    """
    Select KS0 -> e+ e- e+ e- candidates where electrons can come from
    an intermediate neutral particle.

    Control trigger lines:

    - Hlt2RD_KS0ToPiPiEE_Loose => normalization
    - Hlt2RD_KS0ToPiPi_Loose => normalization
    - Hlt2RD_KS0ToPiE_Loose => control
    """
    mass_min, mass_max = strange_builders.ks0_mass_bounds()

    x0 = strange_builders.build_loose_dielectron_intermediate_neutral(
        LONG_LIVED, mass_max=mass_max
    )

    # Note that despite we ensure that within a pair of electrons there are
    # not two particles adding the same bremsstrahlung photon, we can not
    # do the same when we combine the four
    ks0 = strange_builders.make_combination(
        f"KS0 -> {LONG_LIVED} {LONG_LIVED}",
        [x0, x0],
        build_requirements=strange_builders.loose_requirements_for_prompt_ks0_with_intermediate_vertex_separation(
            1, 2
        ),
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0, "X0": x0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def ks02mumuemu_intermediate_neutral_line(
    name="Hlt2RD_KS0ToMuMuEMu_IntermediateNeutral_Loose",
):
    """
    Select [KS0 -> mu+ mu- e+ mu-]cc candidates

    Control trigger lines:

    - Hlt2RD_KS0ToPiPiEE_Loose => normalization
    - Hlt2RD_KS0ToPiPi_Loose => normalization
    - Hlt2RD_KS0ToPiMu_Loose => control
    - Hlt2RD_KS0ToPiE_Loose => control
    """
    muons = strange_builders.make_loose_muons()
    electrons = strange_builders.make_loose_electrons()

    mass_min, mass_max = strange_builders.ks0_mass_bounds()

    x01 = strange_builders.make_combination(
        f"[{LONG_LIVED} -> mu+ e-]cc",
        [muons, electrons],
        build_requirements=strange_builders.loose_requirements_for_detached_combination,
        mass_range=(strange_builders.muon_electron_min_mass(), mass_max),
        name=f"{name}_IntermediateNeutral_Combiner",
    )

    x02 = strange_builders.build_loose_dimuon_intermediate_neutral(
        LONG_LIVED_ALT, mass_max=mass_max
    )

    ks0 = strange_builders.make_combination(
        f"[KS0 -> {LONG_LIVED} {LONG_LIVED_ALT}]cc",
        [x01, x02],
        build_requirements=strange_builders.loose_requirements_for_prompt_ks0_with_intermediate_vertex_separation(
            1, 2
        ),
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [ks0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"KS0": ks0, "X01": x01, "X02": x02}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


####################
# K+ TRIGGER LINES #
####################


@register_line_builder(signal_lines)
@configurable
def kplus2pimumu_line(name="Hlt2RD_KpToPiMuMu"):
    """
    Select [K+ -> pi+ mu+ mu-]cc candidates

    Control trigger lines:

    - Hlt2RD_KpToPiPiPi => normalization
    """
    pions = strange_builders.make_pions()
    muons = strange_builders.make_muons()
    kplus = strange_builders.make_combination(
        "[K+ -> mu+ mu- pi+]cc",
        [muons, muons, pions],
        build_requirements=strange_builders.requirements_for_prompt_combination,
        mass_range=strange_builders.kplus_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Kplus": kplus}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP", "Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def kplus2piminusmumu_line(name="Hlt2RD_KpToPiMinusMuMu"):
    """
    Select [K+ -> pi- mu+ mu+]cc candidates

    """
    pions = strange_builders.make_pions()
    muons = strange_builders.make_muons()
    kplus = strange_builders.make_combination(
        "[K+ -> mu+ mu+ pi-]cc",
        [muons, muons, pions],
        build_requirements=strange_builders.requirements_for_prompt_combination,
        mass_range=strange_builders.kplus_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Kplus": kplus}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP", "Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def kplus2piee_line(name="Hlt2RD_KpToPiEE"):
    """
    Select [K+ -> pi+ e+ e-]cc candidates

    Control trigger lines:

    - Hlt2RD_KpToPiPiPi_Loose => normalization
    - Hlt2RD_KS0ToPiE_Loose => control
    """
    pions = strange_builders.make_pions()
    mass_min, mass_max = strange_builders.kplus_mass_bounds()
    dielectron = strange_builders.make_dielectron(
        SHORT_LIVED,
        mass_range=(strange_builders.dielectron_min_mass(), mass_max),
        electron_builder=strange_builders.make_electrons,
    )
    kplus = strange_builders.make_combination(
        f"[K+ -> {SHORT_LIVED} pi+]cc",
        [dielectron, pions],
        build_requirements=strange_builders.loose_requirements_for_prompt_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Kplus": kplus}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def kplus2piminusee_line(name="Hlt2RD_KpToPiMinusEE"):
    """
    Select [K+ -> pi- e+ e+]cc candidates
    """
    pions = strange_builders.make_pions()
    mass_min, mass_max = strange_builders.kplus_mass_bounds()
    dielectron = strange_builders.make_dielectron(
        SHORT_LIVED,
        mass_range=(strange_builders.dielectron_min_mass(), mass_max),
        opposite_sign=False,
        electron_builder=strange_builders.make_electrons,
    )
    kplus = strange_builders.make_combination(
        f"[K+ -> {SHORT_LIVED} pi-]cc",
        [dielectron, pions],
        build_requirements=strange_builders.loose_requirements_for_prompt_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Kplus": kplus}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def kplus2pimumuee_line(name="Hlt2RD_KpToPiMuMuEE"):
    """
    Select [K+ -> pi+ mu+ mu- e+ e-]cc candidates

    """
    pions = strange_builders.make_pions()
    muons = strange_builders.make_muons()
    mass_min, mass_max = strange_builders.kplus_mass_bounds()
    dielectron = strange_builders.make_dielectron(
        SHORT_LIVED,
        mass_range=(strange_builders.dielectron_min_mass(), mass_max),
        electron_builder=strange_builders.make_electrons,
    )
    kplus = strange_builders.make_combination(
        f"[K+ -> {SHORT_LIVED} mu+ mu- pi+]cc",
        [dielectron, muons, muons, pions],
        build_requirements=strange_builders.requirements_for_prompt_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Kplus": kplus}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP", "Muon", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def Kplus2piemu_line(name="Hlt2RD_KpToPiEMu"):
    """
    Select [K+ -> pi+ [mu+ e-]cc]cc candidates

    """
    pions = strange_builders.make_pions()
    muons = strange_builders.make_muons()
    electrons = strange_builders.make_electrons(add_brem=True)
    mass_min, mass_max = strange_builders.kplus_mass_bounds()
    x0 = strange_builders.make_combination(
        f"[{SHORT_LIVED} -> mu+ e-]cc",
        [muons, electrons],
        build_requirements=strange_builders.NO_REQUIREMENTS,
        mass_range=(0.0, mass_max),
        name=f"{name}_IntermediateNeutral_Combiner",
    )
    kplus = strange_builders.make_combination(
        f"[K+ -> pi+ {SHORT_LIVED}]cc",
        [pions, x0],
        build_requirements=strange_builders.requirements_for_prompt_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Kplus": kplus}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP", "Muon", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def kplus2pipimunu_line(name="Hlt2RD_KpToPiPiMuNu"):
    """
    Select [K+ -> pi+ pi- mu+ nu_mu]cc candidates

    """
    pions = strange_builders.make_pions()
    muons = strange_builders.make_muons(pid_mu_min=10.0)
    kplus = strange_builders.make_combination(
        "[K+ -> mu+ pi+ pi-]cc",
        [muons, pions, pions],
        build_requirements=strange_builders.requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.kplus_semileptonic_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Kplus": kplus}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP", "Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def kplus2pipienu_line(name="Hlt2RD_KpToPiPiENu", prescale=1e-1):
    """
    Select [K+ -> pi+ pi- e+ nu_e]cc candidates

    """
    pions = strange_builders.make_pions()
    electrons = strange_builders.make_electrons(pid_e_min=10.0)
    kplus = strange_builders.make_combination(
        "[K+ -> e+ pi+ pi-]cc",
        [electrons, pions, pions],
        build_requirements=strange_builders.requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.kplus_semileptonic_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Kplus": kplus}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


#########################
# LAMBDA0 TRIGGER LINES #
#########################


@register_line_builder(signal_lines)
@configurable
def lambda02pmu_line(name="Hlt2RD_Lambda0ToPMu_Tight"):
    """
    Select [Lambda0 -> p+ mu- nu_mu~]cc candidates

    Control trigger lines:

    - Hlt2RD_Lambda0ToPPi_Tight => normalization
    """
    protons = strange_builders.make_tight_protons()
    muons = strange_builders.make_tight_muons()
    l0 = strange_builders.make_combination(
        "[Lambda0 -> p+ mu-]cc",
        [protons, muons],
        build_requirements=strange_builders.tight_requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.lambda0_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [l0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Lambda0": l0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def lambda02pe_line(name="Hlt2RD_Lambda0ToPE_Tight"):
    """
    Select [Lambda0 -> p+ e- nu_e~]cc candidates

    Control trigger lines:

    - Hlt2RD_Lambda0ToPPi_Tight => normalization
    """
    protons = strange_builders.make_tight_protons()
    electrons = strange_builders.make_tight_electrons()
    l0 = strange_builders.make_combination(
        "[Lambda0 -> p+ e-]cc",
        [protons, electrons],
        build_requirements=strange_builders.tight_requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.lambda0_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [l0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Lambda0": l0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def lambda02ppiee_line(name="Hlt2RD_Lambda0ToPPiEE_Tight"):
    """
    Select [Lambda0 -> p+ pi- e+ e-]cc candidates

    Control trigger lines:

    - Hlt2RD_Lambda0ToPPi_Tight => normalization
    """
    protons = strange_builders.make_tight_protons()
    pions = strange_builders.make_tight_pions()

    mass_min, mass_max = strange_builders.lambda0_mass_bounds()

    dielectron = strange_builders.make_dielectron(
        SHORT_LIVED,
        # the small phase-space available forces us to start at zero
        mass_range=(0.0, mass_max),
        electron_builder=strange_builders.make_tight_electrons,
    )

    l0 = strange_builders.make_combination(
        f"[Lambda0 -> p+ pi- {SHORT_LIVED}]cc",
        [protons, pions, dielectron],
        build_requirements=strange_builders.tight_requirements_for_detached_combination_no_ip,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [l0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Lambda0": l0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def lambda02ppigamma_line(name="Hlt2RD_Lambda0ToPPiGamma", prescale=1e-1):
    """
    Select [Lambda0 -> p+ pi- gamma]cc candidates

    """
    protons = strange_builders.make_protons()
    pions = strange_builders.make_pions()
    mass_min, mass_max = strange_builders.lambda0_mass_bounds()
    photons = strange_builders.make_tight_photons()
    l0 = strange_builders.make_combination(
        "[Lambda0 -> p+ pi- gamma]cc",
        [protons, pions, photons],
        build_requirements=strange_builders.requirements_for_detached_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [l0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Lambda0": l0}, tracks_with_velo_segment=True
        ),
        raw_banks=["Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


########################
# SIGMA+ TRIGGER LINES #
########################


@register_line_builder(signal_lines)
@configurable
def sigmaplus2pmumu_line(name="Hlt2RD_SigmaPlusToPMuMu"):
    """
    Select [Sigma+ -> p+ mu+ mu-]cc candidates

    Control trigger lines:

    - Hlt2RD_SigmaPlusToPPi0Resolved => normalization
    - Hlt2RD_SigmaPlusToPPi0Merged => normalization
    - Hlt2RD_KpToPiPiPi => control
    """
    protons = strange_builders.make_loose_protons_for_sigma()
    muons = strange_builders.make_muons_for_sigma()
    sigma = strange_builders.make_combination(
        "[Sigma+ -> p+ mu+ mu-]cc",
        [protons, muons, muons],
        build_requirements=strange_builders.requirements_for_prompt_sigma_combination,
        mass_range=strange_builders.sigma_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Sigmaplus": sigma}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP", "Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def sigmaplus2antipmumu_line(name="Hlt2RD_SigmaPlusToAntiPMuMu"):
    """
    Select [Sigma+ -> p~- mu+ mu+]cc candidates

    """
    protons = strange_builders.make_loose_protons_for_sigma()
    muons = strange_builders.make_muons_for_sigma()
    sigma = strange_builders.make_combination(
        "[Sigma+ -> p~- mu+ mu+]cc",
        [protons, muons, muons],
        build_requirements=strange_builders.requirements_for_prompt_sigma_combination,
        mass_range=strange_builders.sigma_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Sigmaplus": sigma}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP", "Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def sigmaplus2pee_line(name="Hlt2RD_SigmaPlusToPEE"):
    """
    Select [Sigma+ -> p+ e+ e-]cc candidates

    Control trigger lines:

    - Hlt2RD_SigmaPlusToPPi0Resolved => normalization
    - Hlt2RD_SigmaPlusToPPi0Merged => normalization
    - Hlt2RD_KpToPiPiPi => control
    """
    protons = strange_builders.make_loose_protons_for_sigma()
    mass_min, mass_max = strange_builders.sigma_mass_bounds()
    dielectron = strange_builders.make_dielectron(
        SHORT_LIVED,
        mass_range=(strange_builders.dielectron_min_mass(), mass_max),
        electron_builder=strange_builders.make_electrons,
    )

    sigma = strange_builders.make_combination(
        f"[Sigma+ -> p+ {SHORT_LIVED}]cc",
        [protons, dielectron],
        build_requirements=strange_builders.requirements_for_prompt_sigma_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Sigmaplus": sigma}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def sigmaplus2pee_detached_line(name="Hlt2RD_SigmaPlusToPEE_Detached"):
    """
    Select [Sigma+ -> p+ (gamma -> e+ e-)]cc candidates

    """
    protons = strange_builders.make_protons_for_sigma()
    mass_min, mass_max = strange_builders.sigma_mass_bounds()
    gamma = strange_builders.build_loose_dielectron_intermediate_neutral(
        LONG_LIVED_ALT, mass_max=mass_max
    )

    sigma = strange_builders.make_combination(
        f"[Sigma+ -> p+ {LONG_LIVED_ALT}]cc",
        [protons, gamma],
        build_requirements=strange_builders.requirements_for_prompt_sigma_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Sigmaplus": sigma, "Gamma": gamma}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def sigmaplus2pgamma_line(name="Hlt2RD_SigmaPlusToPGamma", prescale=1e-3):
    """
    Select [Sigma+ -> p+ gamma]cc candidates
    Normalization mode for Sigma+ decays
    """
    protons = strange_builders.make_tight_protons_for_sigma()
    photons = strange_builders.make_tight_photons()
    sigma = strange_builders.make_combination(
        "[Sigma+ -> p+ gamma]cc",
        [protons, photons],
        mass_range=strange_builders.sigma_mass_bounds(),
        build_requirements=strange_builders.requirements_for_sigma_decay_neutral_combination_no_vertex,
        can_reco_vertex=False,
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH_NO_VERTEX,
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def sigmaplus2pemu_line(name="Hlt2RD_SigmaPlusToPEMu"):
    """
    Select [Sigma+ -> p+ [mu+ e-]cc]cc candidates

    Control trigger lines:

    - Hlt2RD_SigmaPlusToPPi0Resolved => normalization
    - Hlt2RD_SigmaPlusToPPi0Merged => normalization
    - Hlt2RD_KpToPiPiPi => control
    """
    protons = strange_builders.make_loose_protons_for_sigma()
    muons = strange_builders.make_muons_for_sigma()
    electrons = strange_builders.make_electrons(add_brem=True)
    mass_min, mass_max = strange_builders.sigma_mass_bounds()
    x0 = strange_builders.make_combination(
        f"[{SHORT_LIVED} -> mu+ e-]cc",
        [muons, electrons],
        build_requirements=strange_builders.NO_REQUIREMENTS,
        mass_range=(0.0, mass_max),
        name=f"{name}_IntermediateNeutral_Combiner",
    )
    sigma = strange_builders.make_combination(
        f"[Sigma+ -> p+ {SHORT_LIVED}]cc",
        [protons, x0],
        build_requirements=strange_builders.requirements_for_prompt_sigma_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Sigmaplus": sigma}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP", "Muon", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def sigmaplus2peeee_line(name="Hlt2RD_SigmaPlusToPEEEE"):
    """
    Select [Sigma+ -> p+ e+ e- e+ e-]cc candidates
    """
    protons = strange_builders.make_loose_protons_for_sigma()
    mass_min, mass_max = strange_builders.sigma_mass_bounds()
    electrons = strange_builders.make_electrons()
    sigma = strange_builders.make_combination(
        "[Sigma+ -> p+ e+ e+ e- e-]cc",
        [protons, electrons, electrons, electrons, electrons],
        build_requirements=strange_builders.requirements_for_prompt_sigma_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Sigmaplus": sigma}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def sigmaplus2pmumuee_line(name="Hlt2RD_SigmaPlusToPMuMuEE"):
    """
    Select [Sigma+ -> p+ mu+ mu- e+ e-]cc candidates
    """
    protons = strange_builders.make_loose_protons_for_sigma()
    mass_min, mass_max = strange_builders.sigma_mass_bounds()
    muons = strange_builders.make_muons_for_sigma()
    electrons = strange_builders.make_electrons()
    sigma = strange_builders.make_combination(
        "[Sigma+ -> p+ mu+ mu- e+ e-]cc",
        [protons, muons, muons, electrons, electrons],
        build_requirements=strange_builders.requirements_for_prompt_sigma_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Sigmaplus": sigma}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP", "Muon", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(control_lines)
@configurable
def sigmaplus2ppi0_resolved_line(name="Hlt2RD_SigmaPlusToPPi0Resolved", prescale=1e-3):
    """Normalization mode for Sigma+ decays"""
    protons = strange_builders.make_tight_protons_for_sigma()
    pi0 = strange_builders.make_resolved_neutral_pions()
    sigma = strange_builders.make_combination(
        "[Sigma+ -> p+ pi0]cc",
        [protons, pi0],
        mass_range=strange_builders.sigma_mass_bounds(),
        build_requirements=strange_builders.requirements_for_sigma_decay_neutral_combination_no_vertex,
        can_reco_vertex=False,
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL_NO_VERTEX,
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(control_lines)
@configurable
def sigmaplus2ppi0_merged_line(name="Hlt2RD_SigmaPlusToPPi0Merged"):
    """Normalization mode for Sigma+ decays"""
    protons = strange_builders.make_loose_protons_for_sigma()
    pi0 = strange_builders.make_merged_neutral_pions()
    sigma = strange_builders.make_combination(
        "[Sigma+ -> p+ pi0]cc",
        [protons, pi0],
        mass_range=strange_builders.sigma_mass_bounds(),
        build_requirements=strange_builders.requirements_for_sigma_decay_neutral_combination_no_vertex,
        can_reco_vertex=False,
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL_NO_VERTEX,
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


#####################
# XI0 TRIGGER LINES #
#####################


@register_line_builder(signal_lines)
@configurable
def xi02ppi_line(name="Hlt2RD_Xi0ToPPi_Tight"):
    """
    Select [Xi0 -> p+ pi-]cc candidates

    Control trigger lines:

    - Hlt2RD_Xi0ToLambdaPi0Resolved_Tight => normalization
    - Hlt2RD_Xi0ToLambdaPi0Merged_Tight => normalization
    - Hlt2RD_Lambda0ToPPi_Tight => control
    """
    protons = strange_builders.make_tight_protons()
    pions = strange_builders.make_tight_pions()
    xi = strange_builders.make_combination(
        "[Xi0 -> p+ pi-]cc",
        [protons, pions],
        build_requirements=strange_builders.tight_requirements_for_prompt_combination,
        mass_range=strange_builders.xi_tight_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Xi0": xi}, tracks_with_velo_segment=True
        ),
    )


@register_line_builder(signal_lines)
@configurable
def xi02ppi_DS_line(name="Hlt2RD_Xi0ToPPi_Tight_Downstream", prescale=1e-1):
    """
    Select [Xi0 -> p+ pi-]cc downstream candidates

    Control trigger lines:

    - Hlt2RD_Xi0ToLambdaPi0Resolved_Tight => normalization
    - Hlt2RD_Xi0ToLambdaPi0Merged_Tight => normalization
    - Hlt2RD_Lambda0ToPPi_Tight => control
    """
    protons = strange_builders.make_tight_downstream_protons()
    pions = strange_builders.make_tight_downstream_pions()
    xi = strange_builders.make_combination(
        "[Xi0 -> p+ pi-]cc",
        [protons, pions],
        build_requirements=strange_builders.requirements_for_prompt_combination,
        mass_range=strange_builders.xi_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Xi0": xi}, downstream_tracks=True
        ),
    )


#####################
# XI- TRIGGER LINES #
#####################


@register_line_builder(signal_lines)
@configurable
def ximinus2ppipi_line(name="Hlt2RD_XiMinusToPPiPi_Tight"):
    """
    Select [Xi- -> p+ pi- pi-]cc candidates

    Control trigger lines:

    - Hlt2RD_XiMinusToLambdaPi_Tight => normalization
    """
    protons = strange_builders.make_tight_protons()
    pions = strange_builders.make_tight_pions()
    xi = strange_builders.make_combination(
        "[Xi- -> p+ pi- pi-]cc",
        [protons, pions, pions],
        build_requirements=strange_builders.tight_requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.xi_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Ximinus": xi}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP"],
    )


@register_line_builder(signal_lines)
@configurable
def ximinus2ppipi_DS_line(name="Hlt2RD_XiMinusToPPiPi_Tight_Downstream"):
    """
    Select [Xi- -> p+ pi- pi-]cc downstream candidates

    Control trigger lines:

    - Hlt2RD_XiMinusToLambdaPi_Tight => normalization
    """
    protons = strange_builders.make_tight_downstream_protons()
    pions = strange_builders.make_tight_downstream_pions()
    xi = strange_builders.make_combination(
        "[Xi- -> p+ pi- pi-]cc",
        [protons, pions, pions],
        build_requirements=strange_builders.requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.xi_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Ximinus": xi}, downstream_tracks=True
        ),
        raw_banks=["VP"],
    )


@register_line_builder(signal_lines)
@configurable
def ximinus2l0munu_line(name="Hlt2RD_XiMinusToLambdaMu_Tight"):
    """
    Select [Xi- -> Lambda0 (-> p+ pi-) mu- nu_mu~]cc candidates

    Control trigger lines:

    - Hlt2RD_XiMinusToLambdaPi_Tight => normalization
    - Hlt2RD_Lambda0ToPPi_Tight => control
    """
    l0 = strange_builders.build_tight_lambda0()
    muons = strange_builders.make_tight_muons()
    xi = strange_builders.make_combination(
        "[Xi- -> Lambda0 mu-]cc",
        [l0, muons],
        build_requirements=strange_builders.tight_requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.xi_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Xi0": xi, "Lambda0": l0}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP", "Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def ximinus2l0e_line(name="Hlt2RD_XiMinusToLambdaE_Tight"):
    """
    Select [Xi- -> Lambda0 (-> p+ pi-) e+ nu_e]cc candidates

    Control trigger lines:

    - Hlt2RD_XiMinusToLambdaPi_Tight => normalization
    - Hlt2RD_Lambda0ToPPi_Tight => control
    """
    l0 = strange_builders.build_tight_lambda0()
    electrons = strange_builders.make_tight_electrons()
    xi = strange_builders.make_combination(
        "[Xi- -> Lambda0 e-]cc",
        [l0, electrons],
        build_requirements=strange_builders.tight_requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.xi_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [xi],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Ximinus": xi}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


########################
# OMEGA- TRIGGER LINES #
########################


@register_line_builder(signal_lines)
@configurable
def omegaminus2l0pi_line(name="Hlt2RD_OmegaMinusToLambdaPi"):
    """
    Select [Omega- -> Lambda0 (-> p+ pi-) pi-]cc candidates

    Control trigger lines:

    - Hlt2RD_OmegaMinusToLambdaK => normalization
    - Hlt2RD_Lambda0ToPPi => control
    """
    l0 = strange_builders.build_lambda0()
    pions = strange_builders.make_pions()
    omega = strange_builders.make_combination(
        "[Omega- -> Lambda0 pi-]cc",
        [l0, pions],
        build_requirements=strange_builders.requirements_for_prompt_combination,
        mass_range=strange_builders.omega_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [omega],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Omegaminus": omega, "Lambda0": l0}, tracks_with_velo_segment=True
        ),
        raw_banks=["VP"],
    )


@register_line_builder(signal_lines)
@configurable
def omegaminus2l0pi_LD_line(name="Hlt2RD_OmegaMinusToLambdaPi_Long_Down"):
    """
    Select [Omega- -> Lambda0 (-> p+ pi-) pi-]cc candidates
    with a long pion and downstream lambda

    Control trigger lines:

    - Hlt2RD_OmegaMinusToLambdaK => normalization
    - Hlt2RD_OmegaMinusToLambdaK_Long_Down => control
    - Hlt2RD_Lambda0ToPPi_Downstream => control
    """
    l0 = strange_builders.build_downstream_lambda0()
    pions = strange_builders.make_pions()
    omega = strange_builders.make_combination(
        "[Omega- -> Lambda0 pi-]cc",
        [l0, pions],
        build_requirements=strange_builders.requirements_for_prompt_combination,
        mass_range=strange_builders.omega_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [omega],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Omegaminus": omega, "Lambda0": l0},
            tracks_with_velo_segment=True,
            downstream_tracks=True,
        ),
        raw_banks=["VP"],
    )


@register_line_builder(signal_lines)
@configurable
def omegaminus2l0pi_DS_line(name="Hlt2RD_OmegaMinusToLambdaPi_Downstream"):
    """
    Select [Omega- -> Lambda0 (-> p+ pi-) pi-]cc downstream candidates

    Control trigger lines:

    - Hlt2RD_OmegaMinusToLambdaK => normalization
    - Hlt2RD_OmegaMinusToLambdaK_Downstream => normalization
    - Hlt2RD_Lambda0ToPPi_Downstream => control
    """
    l0 = strange_builders.build_downstream_lambda0()
    pions = strange_builders.make_downstream_pions()
    omega = strange_builders.make_combination(
        "[Omega- -> Lambda0 pi-]cc",
        [l0, pions],
        build_requirements=strange_builders.requirements_for_prompt_combination,
        mass_range=strange_builders.omega_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [omega],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Omegaminus": omega, "Lambda0": l0}, downstream_tracks=True
        ),
        raw_banks=["VP"],
    )


@register_line_builder(signal_lines)
@configurable
def omegaminus2ximumu_line(name="Hlt2RD_OmegaMinusToXiMinusMuMu"):
    """
    Select [Omega- -> Xi- (-> Lambda0 (-> p+ pi-) pi-) mu+ mu-]cc candidates

    Control trigger lines:

    - Hlt2RD_OmegaMinusToLambdaK => normalization
    """
    xi, l0, _ = strange_builders.build_ximinus(return_all_containers=True)
    muons = strange_builders.make_muons()
    omega = strange_builders.make_combination(
        "[Omega- -> Xi- mu+ mu-]cc",
        [xi, muons, muons],
        build_requirements=strange_builders.requirements_for_prompt_combination,
        mass_range=strange_builders.omega_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [omega],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {
                "Omegaminus": omega,
                "Ximinus": xi,
                "Lambda0": l0,
            },
            tracks_with_velo_segment=True,
        ),
        raw_banks=["VP"],
    )


@register_line_builder(signal_lines)
@configurable
def omegaminus2xiee_line(name="Hlt2RD_OmegaMinusToXiMinusEE"):
    """
    Select [Omega- -> Xi- (-> Lambda0 (-> p+ pi-) pi-) e+ e-]cc candidates

    Control trigger lines:

    - Hlt2RD_OmegaMinusToLambdaK => normalization
    """
    mass_min, mass_max = strange_builders.omega_mass_bounds()
    xi, l0, _ = strange_builders.build_ximinus(return_all_containers=True)
    dielectron = strange_builders.make_dielectron(
        SHORT_LIVED,
        mass_range=(strange_builders.dielectron_min_mass(), mass_max),
        electron_builder=strange_builders.make_electrons,
    )
    omega = strange_builders.make_combination(
        f"[Omega- -> Xi- {SHORT_LIVED}]cc",
        [xi, dielectron],
        build_requirements=strange_builders.requirements_for_prompt_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [omega],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {
                "Omegaminus": omega,
                "Ximinus": xi,
                "Lambda0": l0,
            },
            tracks_with_velo_segment=True,
        ),
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def omegaminus2xipipi_line(name="Hlt2RD_OmegaMinusToXiMinusPiPi"):
    """
    Select [Omega- -> Xi- (-> Lambda0 (-> p+ pi-) pi-) pi+ pi-]cc candidates

    Control trigger lines:

    - Hlt2RD_OmegaMinusToLambdaK => normalization
    """
    xi, l0, pions = strange_builders.build_ximinus(return_all_containers=True)
    omega = strange_builders.make_combination(
        "[Omega- -> Xi- pi+ pi-]cc",
        [xi, pions, pions],
        build_requirements=strange_builders.requirements_for_prompt_combination,
        mass_range=strange_builders.omega_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [omega],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {
                "Omegaminus": omega,
                "Ximinus": xi,
                "Lambda0": l0,
            },
            tracks_with_velo_segment=True,
        ),
        raw_banks=["VP"],
    )


@register_line_builder(signal_lines)
@configurable
def omegaminus2xigamma_tight_line(name="Hlt2RD_OmegaMinusToXiMinusGamma_Tight"):
    """
    Select [Omega- -> Xi- (-> Lambda0 (-> p+ pi-) pi-) gamma]cc candidates

    Control trigger lines:

    - Hlt2RD_OmegaMinusToLambdaK_Tight => normalization
    - Hlt2RD_XiMinusToLambdaPi_Tight => control
    """
    xi, l0, _ = strange_builders.build_tight_ximinus(return_all_containers=True)
    photons = strange_builders.make_tight_photons()
    omega = strange_builders.make_combination(
        "[Omega- -> Xi- gamma]cc",
        [xi, photons],
        build_requirements=strange_builders.requirements_for_neutral_combination_no_vertex,
        mass_range=strange_builders.omega_mass_bounds(),
        can_reco_vertex=False,
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [omega],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH_NO_VERTEX,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {
                "Ximinus": xi,
                "Lambda0": l0,
            },
            tracks_with_velo_segment=True,
        ),
        raw_banks=["VP"],
    )


#################################
# K+ DOWNSTREAM TRIGGER LINES #
#################################


@register_line_builder(signal_lines)
@configurable
def kplus2pimumu_DS_line(name="Hlt2RD_KpToPiMuMu_Downstream"):
    """
    Select [K+ -> pi+ mu+ mu-]cc candidates

    Control trigger lines:

    - Hlt2RD_KpToPiPiPi => normalization
    """
    pions = strange_builders.make_downstream_pions()
    muons = strange_builders.make_downstream_muons()
    kplus = strange_builders.make_combination(
        "[K+ -> mu+ mu- pi+]cc",
        [muons, muons, pions],
        build_requirements=strange_builders.requirements_for_prompt_combination,
        mass_range=strange_builders.kplus_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Kplus": kplus}, downstream_tracks=True
        ),
        raw_banks=["VP", "Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def kplus2piminusmumu_DS_line(name="Hlt2RD_KpToPiMinusMuMu_Downstream"):
    """
    Select [K+ -> pi- mu+ mu+]cc candidates

    """
    pions = strange_builders.make_downstream_pions()
    muons = strange_builders.make_downstream_muons()
    kplus = strange_builders.make_combination(
        "[K+ -> mu+ mu+ pi-]cc",
        [muons, muons, pions],
        build_requirements=strange_builders.requirements_for_prompt_combination,
        mass_range=strange_builders.kplus_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Kplus": kplus}, downstream_tracks=True
        ),
        raw_banks=["VP", "Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def kplus2piee_DS_line(name="Hlt2RD_KpToPiEE_Downstream"):
    """
    Select [K+ -> pi+ e+ e-]cc candidates
    """
    pions = strange_builders.make_downstream_pions()
    mass_min, mass_max = strange_builders.kplus_mass_bounds()
    dielectron = strange_builders.make_dielectron(
        SHORT_LIVED,
        mass_range=(strange_builders.dielectron_min_mass(), mass_max),
        electron_builder=strange_builders.make_downstream_electrons,
    )
    kplus = strange_builders.make_combination(
        f"[K+ -> {SHORT_LIVED} pi+]cc",
        [dielectron, pions],
        build_requirements=strange_builders.loose_requirements_for_prompt_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Kplus": kplus}, downstream_tracks=True
        ),
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def kplus2piminusee_DS_line(name="Hlt2RD_KpToPiMinusEE_Downstream"):
    """
    Select [K+ -> pi- e+ e+]cc candidates
    """
    pions = strange_builders.make_downstream_pions()
    mass_min, mass_max = strange_builders.kplus_mass_bounds()
    dielectron = strange_builders.make_dielectron(
        SHORT_LIVED,
        mass_range=(strange_builders.dielectron_min_mass(), mass_max),
        opposite_sign=False,
        electron_builder=strange_builders.make_downstream_electrons,
    )
    kplus = strange_builders.make_combination(
        f"[K+ -> {SHORT_LIVED} pi-]cc",
        [dielectron, pions],
        build_requirements=strange_builders.loose_requirements_for_prompt_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Kplus": kplus}, downstream_tracks=True
        ),
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def kplus2pimumuee_DS_line(name="Hlt2RD_KpToPiMuMuEE_Downstream"):
    """
    Select [K+ -> pi+ mu+ mu- e+ e-]cc candidates

    """
    pions = strange_builders.make_downstream_pions()
    muons = strange_builders.make_downstream_muons()
    mass_min, mass_max = strange_builders.kplus_mass_bounds()
    dielectron = strange_builders.make_dielectron(
        SHORT_LIVED,
        mass_range=(strange_builders.dielectron_min_mass(), mass_max),
        electron_builder=strange_builders.make_downstream_electrons,
    )
    kplus = strange_builders.make_combination(
        f"[K+ -> {SHORT_LIVED} mu+ mu- pi+]cc",
        [dielectron, muons, muons, pions],
        build_requirements=strange_builders.requirements_for_prompt_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Kplus": kplus}, downstream_tracks=True
        ),
        raw_banks=["VP", "Muon", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def Kplus2piemu_DS_line(name="Hlt2RD_KpToPiEMu_Downstream"):
    """
    Select [K+ -> pi+ [mu+ e-]cc]cc candidates

    """
    pions = strange_builders.make_downstream_pions()
    muons = strange_builders.make_downstream_muons()
    electrons = strange_builders.make_downstream_electrons(add_brem=True)
    mass_min, mass_max = strange_builders.kplus_mass_bounds()
    x0 = strange_builders.make_combination(
        f"[{SHORT_LIVED} -> mu+ e-]cc",
        [muons, electrons],
        build_requirements=strange_builders.NO_REQUIREMENTS,
        mass_range=(0.0, mass_max),
        name=f"{name}_IntermediateNeutral_Combiner",
    )
    kplus = strange_builders.make_combination(
        f"[K+ -> pi+ {SHORT_LIVED}]cc",
        [pions, x0],
        build_requirements=strange_builders.requirements_for_prompt_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Kplus": kplus}, downstream_tracks=True
        ),
        raw_banks=["VP", "Muon", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def kplus2pipimunu_DS_line(name="Hlt2RD_KpToPiPiMuNu_Downstream"):
    """
    Select [K+ -> pi+ pi- mu+ nu_mu]cc candidates

    """
    pions = strange_builders.make_downstream_pions()
    muons = strange_builders.make_downstream_muons(pid_mu_min=10.0)
    kplus = strange_builders.make_combination(
        "[K+ -> mu+ pi+ pi-]cc",
        [muons, pions, pions],
        build_requirements=strange_builders.requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.kplus_semileptonic_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Kplus": kplus}, downstream_tracks=True
        ),
        raw_banks=["VP", "Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def kplus2pipienu_DS_line(name="Hlt2RD_KpToPiPiENu_Downstream", prescale=1):
    """
    Select [K+ -> pi+ pi- e+ nu_e]cc candidates

    """
    pions = strange_builders.make_downstream_pions()
    electrons = strange_builders.make_downstream_electrons(pid_e_min=10.0)
    kplus = strange_builders.make_combination(
        "[K+ -> e+ pi+ pi-]cc",
        [electrons, pions, pions],
        build_requirements=strange_builders.requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.kplus_semileptonic_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kplus],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Kplus": kplus}, downstream_tracks=True
        ),
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


#################################
# LAMBDA DOWNSTREAM TRIGGER LINES #
#################################


@register_line_builder(signal_lines)
@configurable
def lambda02pmu_DS_line(name="Hlt2RD_Lambda0ToPMu_Tight_Downstream"):
    """
    Select [Lambda0 -> p+ mu- nu_mu~]cc candidates

    Control trigger lines:

    - Hlt2RD_Lambda0ToPPi_Tight => normalization
    """
    protons = strange_builders.make_tight_downstream_protons()
    muons = strange_builders.make_tight_downstream_muons()
    l0 = strange_builders.make_combination(
        "[Lambda0 -> p+ mu-]cc",
        [protons, muons],
        build_requirements=strange_builders.tight_requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.lambda0_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [l0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Lambda0": l0}, downstream_tracks=True
        ),
        raw_banks=["Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def lambda02pe_DS_line(name="Hlt2RD_Lambda0ToPE_Tight_Downstream"):
    """
    Select [Lambda0 -> p+ e- nu_e~]cc candidates

    Control trigger lines:

    - Hlt2RD_Lambda0ToPPi_Tight => normalization
    """
    protons = strange_builders.make_tight_downstream_protons()
    electrons = strange_builders.make_tight_downstream_electrons()
    l0 = strange_builders.make_combination(
        "[Lambda0 -> p+ e-]cc",
        [protons, electrons],
        build_requirements=strange_builders.tight_requirements_for_detached_combination_no_ip,
        mass_range=strange_builders.lambda0_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [l0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Lambda0": l0}, downstream_tracks=True
        ),
        raw_banks=["Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def lambda02ppiee_DS_line(name="Hlt2RD_Lambda0ToPPiEE_Tight_Downstream"):
    """
    Select [Lambda0 -> p+ pi- e+ e-]cc candidates

    """
    protons = strange_builders.make_tight_downstream_protons()
    pions = strange_builders.make_tight_downstream_pions()
    mass_min, mass_max = strange_builders.lambda0_mass_bounds()
    dielectron = strange_builders.make_dielectron(
        SHORT_LIVED,
        # the small phase-space available forces us to start at zero
        mass_range=(0.0, mass_max),
        electron_builder=strange_builders.make_tight_downstream_electrons,
    )

    l0 = strange_builders.make_combination(
        f"[Lambda0 -> p+ pi- {SHORT_LIVED}]cc",
        [protons, pions, dielectron],
        build_requirements=strange_builders.requirements_for_detached_combination_no_ip,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [l0],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Lambda0": l0}, downstream_tracks=True
        ),
        raw_banks=["Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def lambda02ppigamma_DS_line(name="Hlt2RD_Lambda0ToPPiGamma_Downstream", prescale=1):
    """
    Select [Lambda0 -> p+ pi- gamma]cc candidates
    """
    protons = strange_builders.make_downstream_protons()
    pions = strange_builders.make_downstream_pions()
    mass_min, mass_max = strange_builders.lambda0_mass_bounds()
    photons = strange_builders.make_tight_photons()
    l0 = strange_builders.make_combination(
        "[Lambda0 -> p+ pi- gamma]cc",
        [protons, pions, photons],
        build_requirements=strange_builders.requirements_for_detached_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [l0],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Lambda0": l0}, downstream_tracks=True
        ),
        raw_banks=["Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


#################################
# SIGMA+ DOWNSTREAM TRIGGER LINES #
#################################


@register_line_builder(signal_lines)
@configurable
def sigmaplus2pmumu_DS_line(name="Hlt2RD_SigmaPlusToPMuMu_Downstream"):
    """
    Select [Sigma+ -> p+ mu+ mu-]cc candidates
    """
    protons = strange_builders.make_loose_downstream_protons_for_sigma()
    muons = strange_builders.make_downstream_muons_for_sigma()
    sigma = strange_builders.make_combination(
        "[Sigma+ -> p+ mu+ mu-]cc",
        [protons, muons, muons],
        build_requirements=strange_builders.requirements_for_prompt_sigma_combination,
        mass_range=strange_builders.sigma_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Sigmaplus": sigma}, downstream_tracks=True
        ),
        raw_banks=["VP", "Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def sigmaplus2pmumu_LD_line(name="Hlt2RD_SigmaPlusToPMuMu_Long_Down"):
    """
    Select [Sigma+ -> p+ mu+ mu-]cc candidates
    """
    protons = strange_builders.make_loose_protons_for_sigma()
    muons = strange_builders.make_downstream_muons_for_sigma()
    sigma = strange_builders.make_combination(
        "[Sigma+ -> p+ mu+ mu-]cc",
        [protons, muons, muons],
        build_requirements=strange_builders.requirements_for_prompt_sigma_combination,
        mass_range=strange_builders.sigma_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Sigmaplus": sigma}, tracks_with_velo_segment=True, downstream_tracks=True
        ),
        raw_banks=["VP", "Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def sigmaplus2antipmumu_DS_line(name="Hlt2RD_SigmaPlusToAntiPMuMu_Downstream"):
    """
    Select [Sigma+ -> p~- mu+ mu+]cc candidates

    """
    protons = strange_builders.make_loose_downstream_protons_for_sigma()
    muons = strange_builders.make_downstream_muons_for_sigma()
    sigma = strange_builders.make_combination(
        "[Sigma+ -> p~- mu+ mu+]cc",
        [protons, muons, muons],
        build_requirements=strange_builders.requirements_for_prompt_sigma_combination,
        mass_range=strange_builders.sigma_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Sigmaplus": sigma}, downstream_tracks=True
        ),
        raw_banks=["VP", "Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def sigmaplus2antipmumu_LD_line(name="Hlt2RD_SigmaPlusToAntiPMuMu_Long_Down"):
    """
    Select [Sigma+ -> p~- mu+ mu+]cc candidates

    """
    protons = strange_builders.make_loose_protons_for_sigma()
    muons = strange_builders.make_downstream_muons_for_sigma()
    sigma = strange_builders.make_combination(
        "[Sigma+ -> p~- mu+ mu+]cc",
        [protons, muons, muons],
        build_requirements=strange_builders.requirements_for_prompt_sigma_combination,
        mass_range=strange_builders.sigma_mass_bounds(),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Sigmaplus": sigma}, tracks_with_velo_segment=True, downstream_tracks=True
        ),
        raw_banks=["VP", "Muon"],
    )


@register_line_builder(signal_lines)
@configurable
def sigmaplus2pee_DS_line(name="Hlt2RD_SigmaPlusToPEE_Downstream"):
    """
    Select [Sigma+ -> p+ e+ e-]cc candidates
    """
    protons = strange_builders.make_loose_downstream_protons_for_sigma()
    mass_min, mass_max = strange_builders.sigma_mass_bounds()
    dielectron = strange_builders.make_dielectron(
        SHORT_LIVED,
        mass_range=(strange_builders.dielectron_min_mass(), mass_max),
        electron_builder=strange_builders.make_downstream_electrons,
    )

    sigma = strange_builders.make_combination(
        f"[Sigma+ -> p+ {SHORT_LIVED}]cc",
        [protons, dielectron],
        build_requirements=strange_builders.requirements_for_prompt_sigma_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Sigmaplus": sigma}, downstream_tracks=True
        ),
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def sigmaplus2pee_LD_line(name="Hlt2RD_SigmaPlusToPEE_Long_Down"):
    """
    Select [Sigma+ -> p+ e+ e-]cc candidates
    """
    protons = strange_builders.make_loose_protons_for_sigma()
    mass_min, mass_max = strange_builders.sigma_mass_bounds()
    dielectron = strange_builders.make_dielectron(
        SHORT_LIVED,
        mass_range=(strange_builders.dielectron_min_mass(), mass_max),
        electron_builder=strange_builders.make_downstream_electrons,
    )

    sigma = strange_builders.make_combination(
        f"[Sigma+ -> p+ {SHORT_LIVED}]cc",
        [protons, dielectron],
        build_requirements=strange_builders.requirements_for_prompt_sigma_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Sigmaplus": sigma}, tracks_with_velo_segment=True, downstream_tracks=True
        ),
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def sigmaplus2pee_detached_DS_line(name="Hlt2RD_SigmaPlusToPEE_Detached_Downstream"):
    """
    Select [Sigma+ -> p+ (gamma -> e+ e-)]cc candidates
    """
    protons = strange_builders.make_downstream_protons_for_sigma()
    mass_min, mass_max = strange_builders.sigma_mass_bounds()
    gamma = strange_builders.build_loose_downstream_dielectron_intermediate_neutral(
        LONG_LIVED_ALT, mass_max=mass_max
    )

    sigma = strange_builders.make_combination(
        f"[Sigma+ -> p+ {LONG_LIVED_ALT}]cc",
        [protons, gamma],
        build_requirements=strange_builders.requirements_for_prompt_sigma_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Sigmaplus": sigma, "Gamma": gamma}, downstream_tracks=True
        ),
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def sigmaplus2pgamma_DS_line(name="Hlt2RD_SigmaPlusToPGamma_Downstream", prescale=1e-4):
    """
    Select [Sigma+ -> p+ gamma]cc candidates
    Normalization mode for Sigma+ decays
    """
    protons = strange_builders.make_tight_downstream_protons_for_sigma()
    photons = strange_builders.make_tight_photons()
    sigma = strange_builders.make_combination(
        "[Sigma+ -> p+ gamma]cc",
        [protons, photons],
        mass_range=strange_builders.sigma_mass_bounds(),
        build_requirements=strange_builders.requirements_for_sigma_decay_neutral_combination_no_vertex,
        can_reco_vertex=False,
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH_NO_VERTEX,
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def sigmaplus2pemu_DS_line(name="Hlt2RD_SigmaPlusToPEMu_Downstream"):
    """
    Select [Sigma+ -> p+ [mu+ e-]cc]cc candidates
    """
    protons = strange_builders.make_loose_downstream_protons_for_sigma()
    muons = strange_builders.make_downstream_muons_for_sigma()
    electrons = strange_builders.make_downstream_electrons(add_brem=True)
    mass_min, mass_max = strange_builders.sigma_mass_bounds()
    x0 = strange_builders.make_combination(
        f"[{SHORT_LIVED} -> mu+ e-]cc",
        [muons, electrons],
        build_requirements=strange_builders.NO_REQUIREMENTS,
        mass_range=(0.0, mass_max),
        name="Hlt2RD_SigmaPlusToPEMu_LDorDS_IntermediateNeutral_Combiner",
    )
    sigma = strange_builders.make_combination(
        f"[Sigma+ -> p+ {SHORT_LIVED}]cc",
        [protons, x0],
        build_requirements=strange_builders.requirements_for_prompt_sigma_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Sigmaplus": sigma}, downstream_tracks=True
        ),
        raw_banks=["VP", "Muon", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def sigmaplus2pemu_LD_line(name="Hlt2RD_SigmaPlusToPEMu_Long_Down"):
    """
    Select [Sigma+ -> p+ [mu+ e-]cc]cc candidates
    """
    protons = strange_builders.make_loose_protons_for_sigma()
    muons = strange_builders.make_downstream_muons_for_sigma()
    electrons = strange_builders.make_downstream_electrons(add_brem=True)
    mass_min, mass_max = strange_builders.sigma_mass_bounds()
    x0 = strange_builders.make_combination(
        f"[{SHORT_LIVED} -> mu+ e-]cc",
        [muons, electrons],
        build_requirements=strange_builders.NO_REQUIREMENTS,
        mass_range=(0.0, mass_max),
        name="Hlt2RD_SigmaPlusToPEMu_LDorDS_IntermediateNeutral_Combiner",
    )
    sigma = strange_builders.make_combination(
        f"[Sigma+ -> p+ {SHORT_LIVED}]cc",
        [protons, x0],
        build_requirements=strange_builders.requirements_for_prompt_sigma_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Sigmaplus": sigma}, tracks_with_velo_segment=True, downstream_tracks=True
        ),
        raw_banks=["VP", "Muon", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def sigmaplus2peeee_DS_line(name="Hlt2RD_SigmaPlusToPEEEE_Downstream"):
    """
    Select [Sigma+ -> p+ e+ e- e+ e-]cc candidates
    """
    protons = strange_builders.make_loose_downstream_protons_for_sigma()
    mass_min, mass_max = strange_builders.sigma_mass_bounds()
    electrons = strange_builders.make_downstream_electrons()
    sigma = strange_builders.make_combination(
        "[Sigma+ -> p+ e+ e+ e- e-]cc",
        [protons, electrons, electrons, electrons, electrons],
        build_requirements=strange_builders.requirements_for_prompt_sigma_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Sigmaplus": sigma}, downstream_tracks=True
        ),
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def sigmaplus2peeee_LD_line(name="Hlt2RD_SigmaPlusToPEEEE_Long_Down"):
    """
    Select [Sigma+ -> p+ e+ e- e+ e-]cc candidates
    """
    protons = strange_builders.make_loose_protons_for_sigma()
    mass_min, mass_max = strange_builders.sigma_mass_bounds()
    electrons = strange_builders.make_downstream_electrons()
    sigma = strange_builders.make_combination(
        "[Sigma+ -> p+ e+ e+ e- e-]cc",
        [protons, electrons, electrons, electrons, electrons],
        build_requirements=strange_builders.requirements_for_prompt_sigma_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Sigmaplus": sigma}, tracks_with_velo_segment=True, downstream_tracks=True
        ),
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def sigmaplus2pmumuee_DS_line(name="Hlt2RD_SigmaPlusToPMuMuEE_Downstream"):
    """
    Select [Sigma+ -> p+ mu+ mu- e+ e-]cc candidates
    """
    protons = strange_builders.make_loose_downstream_protons_for_sigma()
    mass_min, mass_max = strange_builders.sigma_mass_bounds()
    muons = strange_builders.make_downstream_muons_for_sigma()
    electrons = strange_builders.make_downstream_electrons()
    sigma = strange_builders.make_combination(
        "[Sigma+ -> p+ mu+ mu- e+ e-]cc",
        [protons, muons, muons, electrons, electrons],
        build_requirements=strange_builders.requirements_for_prompt_sigma_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Sigmaplus": sigma}, downstream_tracks=True
        ),
        raw_banks=["VP", "Muon", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(signal_lines)
@configurable
def sigmaplus2pmumuee_LD_line(name="Hlt2RD_SigmaPlusToPMuMuEE_Long_Down"):
    """
    Select [Sigma+ -> p+ mu+ mu- e+ e-]cc candidates
    """
    protons = strange_builders.make_loose_protons_for_sigma()
    mass_min, mass_max = strange_builders.sigma_mass_bounds()
    muons = strange_builders.make_downstream_muons_for_sigma()
    electrons = strange_builders.make_downstream_electrons()
    sigma = strange_builders.make_combination(
        "[Sigma+ -> p+ mu+ mu- e+ e-]cc",
        [protons, muons, muons, electrons, electrons],
        build_requirements=strange_builders.requirements_for_prompt_sigma_combination,
        mass_range=(mass_min, mass_max),
        name=f"{name}_Combiner",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_SEARCH,
        extra_outputs=strange_builders.define_impact_parameter_chi2_based_isolation(
            {"Sigmaplus": sigma}, tracks_with_velo_segment=True, downstream_tracks=True
        ),
        raw_banks=["VP", "Muon", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(control_lines)
@configurable
def sigmaplus2ppi0_resolved_DS_line(
    name="Hlt2RD_SigmaPlusToPPi0Resolved_Downstream", prescale=1e-3
):
    """Normalization mode for Sigma+ decays"""

    protons = strange_builders.make_tight_downstream_protons_for_sigma()
    pi0 = strange_builders.make_resolved_neutral_pions()
    sigma = strange_builders.make_combination(
        "[Sigma+ -> p+ pi0]cc",
        [protons, pi0],
        mass_range=strange_builders.sigma_mass_bounds(),
        build_requirements=strange_builders.requirements_for_sigma_decay_neutral_combination_no_vertex,
        can_reco_vertex=False,
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        prescale=prescale,
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL_NO_VERTEX,
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


@register_line_builder(control_lines)
@configurable
def sigmaplus2ppi0_merged_DS_line(name="Hlt2RD_SigmaPlusToPPi0Merged_Downstream"):
    """Normalization mode for Sigma+ decays"""

    protons = strange_builders.make_loose_downstream_protons_for_sigma()
    pi0 = strange_builders.make_merged_neutral_pions()
    sigma = strange_builders.make_combination(
        "[Sigma+ -> p+ pi0]cc",
        [protons, pi0],
        mass_range=strange_builders.sigma_mass_bounds(),
        build_requirements=strange_builders.requirements_for_sigma_decay_neutral_combination_no_vertex,
        can_reco_vertex=False,
        name=f"{name}_Combiner",
    )
    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [sigma],
        monitoring_variables=strange_builders.MONITORING_VARIABLES_CONTROL_NO_VERTEX,
        raw_banks=["VP", "Calo"],
        calo_digits=True,
        calo_clusters=True,
    )


# Update the dictionary containing all the lines
all_lines.update(control_lines)
all_lines.update(signal_lines)
