###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Sprucing line definitions of B -> Xll lines, especially
 - B+ -> K+ e+ e- (CC)
 - B+ -> K+ mu+ mu- (CC)
 - B+ -> K+ mu+ e- (CC)
 - B0 -> K*0 (-> K+ pi-) e+ e- (CC)
 - B0 -> K*0 (-> K+ pi-) mu+ mu- (CC)
 - B0 -> K*0 (-> K+ pi-) mu+ e- (CC)

Lepton same sign lines :
 - B+ -> K+ e- e- (CC)
 - B+ -> K+ mu- mu- (CC)
 - B+ -> K+ mu- e- (CC)
 - B0 -> K*0 (-> K+ pi-) e- e- (CC)
 - B0 -> K*0 (-> K+ pi-) mu- mu- (CC)
 - B0 -> K*0 (-> K+ pi-) mu- e- (CC)

Hadron same sign lines :
 - B0 -> K*++ (-> K+ pi+) e+ e- (CC)
 - B0 -> K*++ (-> K+ pi+) mu+ mu- (CC)
 - B0 -> K*++ (-> K+ pi+) e- e- (CC)
 - B0 -> K*++ (-> K+ pi+) mu- mu- (CC)

Protonic final state lines :
- B(s)0 -> (J/psi(1S) -> mu+ mu-) p+ p~- (cc)
- B(s)0 -> (J/psi(1S) -> mu+ mu+) p+ p~- (cc)
- B+ -> (J/psi(1S) -> mu+ mu-) Lambda0 (-> p+ pi-) p+ (cc)
- B+ -> (J/psi(1S) -> mu+ mu+) Lambda0 (-> p+ pi-) p+ (cc)

Contact authors : Richard Morgan Williams (richard.morgan.williams@cern.ch), Felicia Carolin Volle (felicia.carolin.volle@cern.ch)

Last update : 13/05/2022

"""

import Functors as F
from GaudiKernel.SystemOfUnits import MeV
from Moore.config import register_line_builder
from Moore.lines import SpruceLine
from PyConf import configurable
from RecoConf import rdbuilder_thor
from RecoConf.reconstruction_objects import make_pvs

from .builders import b_to_xll_builders, rd_isolation
from .builders.rd_prefilters import rd_prefilter

sprucing_lines = {}


def parse_hlt2_filter_code(name: str):
    assert any(["EE" in name, "MuE" in name, "MuMu" in name]), (
        f"Unknown dilepton configuration for sprucing line ``{name}'', hlt2_filter_code could not be generated."
    )
    leptons_dict = {
        "EE": {"mva": "DiElectron", "cut": "Dielectron"},
        "MuE": {"mva": "DiMuE", "cut": "ElectronMuon"},
        "MuMu": {"mva": "DiMuon", "cut": "Dimuon"},
    }
    leptons = None
    for lepton in ("EE", "MuE", "MuMu"):
        if lepton in name:
            assert not leptons, (
                f"More than one lepton pair in sprucing line ``{name}''."
            )
            leptons = leptons_dict[lepton]
    assert leptons, f"Could not identify dileptons from line ``{name}''"

    bodies = 2
    for hadron in ("Kp", "Km", "Pip", "Pim", "Pp", "Pm"):
        bodies += name.count(hadron)
    for hadron in ("Pi0", "L0", "Kst"):
        bodies += name.count(hadron) * 2

    assert bodies > 2, (
        "Could not configure number of bodies from line name, hlt2_filter_code could not be generated."
    )
    if "SS" in name or "SameSign" in name:
        sign = {"mva": "_SS", "cut": "SS"}
    else:
        sign = {"mva": "", "cut": ""}

    hlt2_filter_code = [
        f"Hlt2_InclDet{leptons['mva']}{sign['mva']}Decision",
        f"Hlt2_InclDet{leptons['mva']}_3Body{sign['mva']}Decision",
        f"Hlt2CutBasedIncl{leptons['cut']}{sign['cut']}Decision",
        f"Hlt2CutBasedIncl{leptons['cut']}PlusTrack{sign['cut']}Decision",
        "Hlt2Topo2BodyDecision",
        "Hlt2Topo3BodyDecision",
    ]
    if "Mu" in name:
        hlt2_filter_code += ["Hlt2TopoMu2BodyDecision"]

    if bodies > 3:
        hlt2_filter_code += [
            f"Hlt2_InclDet{leptons['mva']}_4Body{sign['mva']}Decision",
            f"Hlt2_InclDet{leptons['mva']}_neutral{sign['mva']}Decision",
            f"Hlt2CutBasedIncl{leptons['cut']}PlusTwoTrack{sign['cut']}Decision",
        ]

    return hlt2_filter_code


########################################
#      B+ -> K+ ll sprucing lines      #
########################################

from Hlt2Conf.lines.rd.b_to_xll_hlt2 import BtoKee

BtoKmumu = {
    "B": {
        "am_min": 4_500.0 * MeV,
        "am_max": 7_000.0 * MeV,
        "B_pt_min": 0.0 * MeV,
        "FDchi2_min": 36.0,
        "vchi2pdof_max": 16.0,
        "bpvipchi2_max": 25.0,
        "min_cosine": 0.9995,
    },
    "dimuons": {
        "adocachi2cut_max": 36.0,
        "ipchi2_muon_min": 9.0,
        "pidmu_muon_min": -4.0,
        "pt_dimuon_min": 0.0 * MeV,
        "pt_muon_min": 350.0 * MeV,
        "p_muon_min": 0.0 * MeV,
        "vchi2pdof_max": 9.0,
        "bpvvdchi2_min": 16.0,
        "am_min": 0.0 * MeV,
        "am_max": 5_500.0 * MeV,
    },
    "kaons": {
        "pt_min": 400.0 * MeV,
        "mipchi2dvprimary_min": 4.0,
        "p_min": 0.0 * MeV,
        "pid": (F.PID_K > -4.0),
    },
}


@register_line_builder(sprucing_lines)
def Spruce_BuToKpEE_line(name="SpruceRD_BuToKpEE", prescale=1):
    """
    Sprucing line for B+ -> K+ e+ e- (cc)
    """
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKee["dielectrons"], pid_e_min=1.0, opposite_sign=True
    )
    kaons = rdbuilder_thor.make_rd_detached_kaons(**BtoKee["kaons"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        kaons,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKee["B"],
    )
    k_child = rd_isolation.find_in_decay(input=B, id="K+")
    e_child = rd_isolation.find_in_decay(input=B, id="e+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"], candidates=[B], cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE())
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["K", "e"],
        candidates=[k_child, e_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        extra_outputs=iso_parts,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_BuToKpEE_SameSign_line(name="SpruceRD_BuToKpEE_SameSign", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKee["dielectrons"], pid_e_min=1.0, opposite_sign=False
    )
    kaons = rdbuilder_thor.make_rd_detached_kaons(**BtoKee["kaons"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        kaons,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKee["B"],
    )
    k_child = rd_isolation.find_in_decay(input=B, id="K+")
    e_child = rd_isolation.find_in_decay(input=B, id="e+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"], candidates=[B], cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE())
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["K", "e"],
        candidates=[k_child, e_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        extra_outputs=iso_parts,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_BuToKpMuMu_line(name="SpruceRD_BuToKpMuMu", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKmumu["dimuons"], same_sign=False
    )
    kaons = rdbuilder_thor.make_rd_detached_kaons(**BtoKmumu["kaons"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        kaons,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKmumu["B"],
    )
    k_child = rd_isolation.find_in_decay(input=B, id="K+")
    mu_child = rd_isolation.find_in_decay(input=B, id="mu+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"], candidates=[B], cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE())
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["K", "mu"],
        candidates=[k_child, mu_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        extra_outputs=iso_parts,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_BuToKpMuMu_SameSign_line(name="SpruceRD_BuToKpMuMu_SameSign", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKmumu["dimuons"], same_sign=True
    )
    kaons = rdbuilder_thor.make_rd_detached_kaons(**BtoKmumu["kaons"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        kaons,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKmumu["B"],
    )
    k_child = rd_isolation.find_in_decay(input=B, id="K+")
    mu_child = rd_isolation.find_in_decay(input=B, id="mu+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"], candidates=[B], cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE())
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["K", "mu"],
        candidates=[k_child, mu_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        extra_outputs=iso_parts,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


#########################################
#      B+ -> pi+ ll sprucing lines      #
#########################################

from Hlt2Conf.lines.rd.b_to_xll_hlt2 import Btopiee

Btopimumu = {
    "B": {
        "am_min": 4_500.0 * MeV,
        "am_max": 7_000.0 * MeV,
        "B_pt_min": 0.0 * MeV,
        "FDchi2_min": 36.0,
        "vchi2pdof_max": 16.0,
        "bpvipchi2_max": 25.0,
        "min_cosine": 0.9995,
    },
    "dimuons": {
        "adocachi2cut_max": 36.0,
        "ipchi2_muon_min": 9.0,
        "pidmu_muon_min": -4.0,
        "pt_dimuon_min": 0.0 * MeV,
        "pt_muon_min": 350.0 * MeV,
        "p_muon_min": 0.0 * MeV,
        "vchi2pdof_max": 9.0,
        "bpvvdchi2_min": 16.0,
        "am_min": 0.0 * MeV,
        "am_max": 5_500.0 * MeV,
    },
    "pions": {
        "pt_min": 400.0 * MeV,
        "mipchi2dvprimary_min": 4.0,
        "p_min": 0.0 * MeV,
        "pid": (F.PID_K <= 4.0),
    },
}


@register_line_builder(sprucing_lines)
def Spruce_BuToPipEE_line(name="SpruceRD_BuToPipEE", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **Btopiee["dielectrons"], pid_e_min=1.0, opposite_sign=True
    )
    pions = rdbuilder_thor.make_rd_detached_pions(**Btopiee["pions"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        pions,
        pvs,
        Descriptor="[B+ -> J/psi(1S) pi+]cc",
        name="make_rd_BToXll_for_" + name,
        **Btopiee["B"],
    )
    pi_child = rd_isolation.find_in_decay(input=B, id="pi+")
    e_child = rd_isolation.find_in_decay(input=B, id="e+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"], candidates=[B], cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE())
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["Pi", "e"],
        candidates=[pi_child, e_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        extra_outputs=iso_parts,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_BuToPipEE_SameSign_line(name="SpruceRD_BuToPipEE_SameSign", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **Btopiee["dielectrons"], pid_e_min=1.0, opposite_sign=False
    )
    pions = rdbuilder_thor.make_rd_detached_pions(**Btopiee["pions"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        pions,
        pvs,
        Descriptor="[B+ -> J/psi(1S) pi+]cc",
        name="make_rd_BToXll_for_" + name,
        **Btopiee["B"],
    )
    pi_child = rd_isolation.find_in_decay(input=B, id="pi+")
    e_child = rd_isolation.find_in_decay(input=B, id="e+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"], candidates=[B], cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE())
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["Pi", "e"],
        candidates=[pi_child, e_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        extra_outputs=iso_parts,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_BuToPipMuMu_line(name="SpruceRD_BuToPipMuMu", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **Btopimumu["dimuons"], same_sign=False
    )
    pions = rdbuilder_thor.make_rd_detached_pions(**Btopimumu["pions"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        pions,
        pvs,
        Descriptor="[B+ -> J/psi(1S) pi+]cc",
        name="make_rd_BToXll_for_" + name,
        **Btopimumu["B"],
    )
    pi_child = rd_isolation.find_in_decay(input=B, id="pi+")
    mu_child = rd_isolation.find_in_decay(input=B, id="mu+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"], candidates=[B], cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE())
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["Pi", "mu"],
        candidates=[pi_child, mu_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        extra_outputs=iso_parts,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_BuToPipMuMu_SameSign_line(name="SpruceRD_BuToPipMuMu_SameSign", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **Btopimumu["dimuons"], same_sign=True
    )
    pions = rdbuilder_thor.make_rd_detached_pions(**Btopimumu["pions"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        pions,
        pvs,
        Descriptor="[B+ -> J/psi(1S) pi+]cc",
        name="make_rd_BToXll_for_" + name,
        **Btopimumu["B"],
    )
    pi_child = rd_isolation.find_in_decay(input=B, id="pi+")
    mu_child = rd_isolation.find_in_decay(input=B, id="mu+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B"], candidates=[B], cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE())
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["Pi", "mu"],
        candidates=[pi_child, mu_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        extra_outputs=iso_parts,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


############################################
#      B+ -> K+ pi0 ll sprucing lines      #
############################################

from Hlt2Conf.lines.rd.b_to_xll_hlt2 import BtoKpResolvedPi0ee, BtoKpResolvedPi0mumu


@register_line_builder(sprucing_lines)
def Spruce_BuToKpResolvedPi0EE_line(name="SpruceRD_BuToKpResolvedPi0EE", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKpResolvedPi0ee["dielectrons"], pid_e_min=2.0, opposite_sign=True
    )
    Kstps = rdbuilder_thor.make_Kstps_with_pi0s(**BtoKpResolvedPi0ee["Kstps"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        Kstps,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K*(892)+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKpResolvedPi0ee["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_BuToKpResolvedPi0EE_SameSign_line(
    name="SpruceRD_BuToKpResolvedPi0EE_SameSign", prescale=1
):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKpResolvedPi0ee["dielectrons"], pid_e_min=2.0, opposite_sign=False
    )
    Kstps = rdbuilder_thor.make_Kstps_with_pi0s(**BtoKpResolvedPi0ee["Kstps"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        Kstps,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K*(892)+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKpResolvedPi0ee["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_BuToKpResolvedPi0MuMu_line(
    name="SpruceRD_BuToKpResolvedPi0MuMu", prescale=1
):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKpResolvedPi0mumu["dimuons"], same_sign=False
    )
    Kstps = rdbuilder_thor.make_Kstps_with_pi0s(**BtoKpResolvedPi0mumu["Kstps"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        Kstps,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K*(892)+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKpResolvedPi0mumu["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_BuToKpResolvedPi0MuMu_SameSign_line(
    name="SpruceRD_BuToKpResolvedPi0MuMu_SameSign", prescale=1
):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKpResolvedPi0mumu["dimuons"], same_sign=True
    )
    Kstps = rdbuilder_thor.make_Kstps_with_pi0s(**BtoKpResolvedPi0mumu["Kstps"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        Kstps,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K*(892)+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKpResolvedPi0mumu["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


#############################################
#      B0 -> pi+ pi- ll sprucing lines      #
#############################################

from Hlt2Conf.lines.rd.b_to_xll_hlt2 import Btopipiee, Btopipimumu


@register_line_builder(sprucing_lines)
def Spruce_B0ToPipPimEE_line(name="SpruceRD_B0ToPipPimEE", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **Btopipiee["dielectrons"], pid_e_min=1.0, opposite_sign=True
    )
    rhos = rdbuilder_thor.make_rd_detached_rho0()

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        rhos,
        pvs,
        Descriptor="B0 -> J/psi(1S) rho(770)0",
        name="make_rd_BToXll_for_" + name,
        **Btopipiee["B"],
    )
    rho_child = rd_isolation.find_in_decay(input=B, id="rho(770)0")
    pi_child = rd_isolation.find_in_decay(input=B, id="pi+")
    e_child = rd_isolation.find_in_decay(input=B, id="e+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B0", "Rho"],
        candidates=[B, rho_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e", "Pi"],
        candidates=[e_child, pi_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        extra_outputs=iso_parts,
        tagging_particles=True,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_B0ToPipPimEE_SameSign_line(
    name="SpruceRD_B0ToPipPimEE_SameSign", prescale=1
):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **Btopipiee["dielectrons"], pid_e_min=1.0, opposite_sign=False
    )
    rhos = rdbuilder_thor.make_rd_detached_rho0()

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        rhos,
        pvs,
        Descriptor="B0 -> J/psi(1S) rho(770)0",
        name="make_rd_BToXll_for_" + name,
        **Btopipiee["B"],
    )
    rho_child = rd_isolation.find_in_decay(input=B, id="rho(770)0")
    pi_child = rd_isolation.find_in_decay(input=B, id="pi+")
    e_child = rd_isolation.find_in_decay(input=B, id="e+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B0", "Rho"],
        candidates=[B, rho_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e", "Pi"],
        candidates=[e_child, pi_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        extra_outputs=iso_parts,
        tagging_particles=True,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_B0ToPipPimMuMu_line(name="SpruceRD_B0ToPipPimMuMu", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **Btopipimumu["dimuons"], same_sign=False
    )
    rhos = rdbuilder_thor.make_rd_detached_rho0()

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        rhos,
        pvs,
        Descriptor="B0 -> J/psi(1S) rho(770)0",
        name="make_rd_BToXll_for_" + name,
        **Btopipimumu["B"],
    )
    rho_child = rd_isolation.find_in_decay(input=B, id="rho(770)0")
    pi_child = rd_isolation.find_in_decay(input=B, id="pi+")
    mu_child = rd_isolation.find_in_decay(input=B, id="mu+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B0", "Rho"],
        candidates=[B, rho_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu", "Pi"],
        candidates=[mu_child, pi_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        extra_outputs=iso_parts,
        tagging_particles=True,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_B0ToPipPimMuMu_SameSign_line(
    name="SpruceRD_B0ToPipPimMuMu_SameSign", prescale=1
):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **Btopipimumu["dimuons"], same_sign=True
    )
    rhos = rdbuilder_thor.make_rd_detached_rho0()

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        rhos,
        pvs,
        Descriptor="B0 -> J/psi(1S) rho(770)0",
        name="make_rd_BToXll_for_" + name,
        **Btopipimumu["B"],
    )
    rho_child = rd_isolation.find_in_decay(input=B, id="rho(770)0")
    pi_child = rd_isolation.find_in_decay(input=B, id="pi+")
    mu_child = rd_isolation.find_in_decay(input=B, id="mu+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B0", "Rho"],
        candidates=[B, rho_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu", "Pi"],
        candidates=[mu_child, pi_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        extra_outputs=iso_parts,
        tagging_particles=True,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


###########################################
#      B0 -> K+ K- ll sprucing lines      #
###########################################

from Hlt2Conf.lines.rd.b_to_xll_hlt2 import BtoKKee, BtoKKmumu


@register_line_builder(sprucing_lines)
def Spruce_B0ToKpKmEE_line(name="SpruceRD_B0ToKpKmEE", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKKee["dielectrons"], pid_e_min=1.0, opposite_sign=True
    )
    phis = rdbuilder_thor.make_rd_detached_phis(**BtoKKee["phis"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        phis,
        pvs,
        Descriptor="B0 -> J/psi(1S) phi(1020)",
        name="make_rd_BToXll_for_" + name,
        **BtoKKee["B"],
    )
    phi_child = rd_isolation.find_in_decay(input=B, id="phi(1020)")
    k_child = rd_isolation.find_in_decay(input=B, id="K+")
    e_child = rd_isolation.find_in_decay(input=B, id="e+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B0", "Phi"],
        candidates=[B, phi_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e", "K"],
        candidates=[e_child, k_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        extra_outputs=iso_parts,
        tagging_particles=True,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_B0ToKpKmEE_SameSign_line(name="SpruceRD_B0ToKpKmEE_SameSign", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKKee["dielectrons"], pid_e_min=1.0, opposite_sign=False
    )
    phis = rdbuilder_thor.make_rd_detached_phis(**BtoKKee["phis"])

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        phis,
        pvs,
        Descriptor="B0 -> J/psi(1S) phi(1020)",
        name="make_rd_BToXll_for_" + name,
        **BtoKKee["B"],
    )
    phi_child = rd_isolation.find_in_decay(input=B, id="phi(1020)")
    k_child = rd_isolation.find_in_decay(input=B, id="K+")
    e_child = rd_isolation.find_in_decay(input=B, id="e+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B0", "Phi"],
        candidates=[B, phi_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e", "K"],
        candidates=[e_child, k_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        extra_outputs=iso_parts,
        tagging_particles=True,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_B0ToKpKmMuMu_line(name="SpruceRD_B0ToKpKmMuMu", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKKmumu["dimuons"], same_sign=False
    )
    phis = rdbuilder_thor.make_rd_detached_phis(**BtoKKmumu["phis"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        phis,
        pvs,
        Descriptor="B0 -> J/psi(1S) phi(1020)",
        name="make_rd_BToXll_for_" + name,
        **BtoKKmumu["B"],
    )
    phi_child = rd_isolation.find_in_decay(input=B, id="phi(1020)")
    k_child = rd_isolation.find_in_decay(input=B, id="K+")
    mu_child = rd_isolation.find_in_decay(input=B, id="mu+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B0", "Phi"],
        candidates=[B, phi_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu", "K"],
        candidates=[mu_child, k_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        extra_outputs=iso_parts,
        tagging_particles=True,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_B0ToKpKmMuMu_SameSign_line(
    name="SpruceRD_B0ToKpKmMuMu_SameSign", prescale=1
):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKKmumu["dimuons"], same_sign=True
    )
    phis = rdbuilder_thor.make_rd_detached_phis(**BtoKKmumu["phis"])

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        phis,
        pvs,
        Descriptor="B0 -> J/psi(1S) phi(1020)",
        name="make_rd_BToXll_for_" + name,
        **BtoKKmumu["B"],
    )
    phi_child = rd_isolation.find_in_decay(input=B, id="phi(1020)")
    k_child = rd_isolation.find_in_decay(input=B, id="K+")
    mu_child = rd_isolation.find_in_decay(input=B, id="mu+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B0", "Phi"],
        candidates=[B, phi_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu", "K"],
        candidates=[mu_child, k_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        extra_outputs=iso_parts,
        tagging_particles=True,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


############################################
#      B0 -> K+ pi- ll sprucing lines      #
############################################

from Hlt2Conf.lines.rd.b_to_xll_hlt2 import BtoKPiee, BtoKPimumu


@register_line_builder(sprucing_lines)
def Spruce_B0ToKpPimEE_line(name="SpruceRD_B0ToKpPimEE", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKPiee["dielectrons"], pid_e_min=1.0, opposite_sign=True
    )
    Kst0s = rdbuilder_thor.make_rd_detached_kstar0s()

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        Kst0s,
        pvs,
        Descriptor="[B0 -> J/psi(1S) K*(892)0]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPiee["B"],
    )

    kst_child = rd_isolation.find_in_decay(input=B, id="K*(892)0")
    k_child = rd_isolation.find_in_decay(input=B, id="K+")
    pi_child = rd_isolation.find_in_decay(input=B, id="pi+")
    e_child = rd_isolation.find_in_decay(input=B, id="e+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B0", "Kst"],
        candidates=[B, kst_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e", "K", "Pi"],
        candidates=[e_child, k_child, pi_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        extra_outputs=iso_parts,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_B0ToKpPimEE_SameSign_line(name="SpruceRD_B0ToKpPimEE_SameSign", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKPiee["dielectrons"], pid_e_min=1.0, opposite_sign=False
    )
    Kst0s = rdbuilder_thor.make_rd_detached_kstar0s()

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        Kst0s,
        pvs,
        Descriptor="[B0 -> J/psi(1S) K*(892)0]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPiee["B"],
    )

    kst_child = rd_isolation.find_in_decay(input=B, id="K*(892)0")
    k_child = rd_isolation.find_in_decay(input=B, id="K+")
    pi_child = rd_isolation.find_in_decay(input=B, id="pi+")
    e_child = rd_isolation.find_in_decay(input=B, id="e+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B0", "Kst"],
        candidates=[B, kst_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["e", "K", "Pi"],
        candidates=[e_child, k_child, pi_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        extra_outputs=iso_parts,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_B0ToKpPimMuMu_line(name="SpruceRD_B0ToKpPimMuMu", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKPimumu["dimuons"], same_sign=False
    )
    Kst0s = rdbuilder_thor.make_rd_detached_kstar0s()

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        Kst0s,
        pvs,
        Descriptor="[B0 -> J/psi(1S) K*(892)0]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPimumu["B"],
    )

    kst_child = rd_isolation.find_in_decay(input=B, id="K*(892)0")
    k_child = rd_isolation.find_in_decay(input=B, id="K+")
    pi_child = rd_isolation.find_in_decay(input=B, id="pi+")
    mu_child = rd_isolation.find_in_decay(input=B, id="mu+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B0", "Kst"],
        candidates=[B, kst_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu", "K", "Pi"],
        candidates=[mu_child, k_child, pi_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        extra_outputs=iso_parts,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def B0ToKpPimMuMu_SameSign_line(name="SpruceRD_B0ToKpPimMuMu_SameSign", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKPimumu["dimuons"], same_sign=True
    )
    Kst0s = rdbuilder_thor.make_rd_detached_kstar0s()

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        Kst0s,
        pvs,
        Descriptor="[B0 -> J/psi(1S) K*(892)0]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPimumu["B"],
    )

    kst_child = rd_isolation.find_in_decay(input=B, id="K*(892)0")
    k_child = rd_isolation.find_in_decay(input=B, id="K+")
    pi_child = rd_isolation.find_in_decay(input=B, id="pi+")
    mu_child = rd_isolation.find_in_decay(input=B, id="mu+")

    iso_parts = rd_isolation.select_parts_for_isolation(
        names=["B0", "Kst"],
        candidates=[B, kst_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.FIND_IN_TREE()),
    )

    iso_parts += rd_isolation.select_parts_for_isolation(
        names=["mu", "K", "Pi"],
        candidates=[mu_child, k_child, pi_child],
        cut=F.require_all(F.DR2 < 0.25, ~F.SHARE_TRACKS()),
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        extra_outputs=iso_parts,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


################################################
#      B+ -> K+ pi+ pi- ll sprucing lines      #
################################################

from Hlt2Conf.lines.rd.b_to_xll_hlt2 import BtoKPiPiee, BtoKPiPimumu


@register_line_builder(sprucing_lines)
def Spruce_BuToKpPipPimEE_line(name="SpruceRD_BuToKpPipPimEE", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKPiPiee["dielectrons"], pid_e_min=1.0, opposite_sign=True
    )
    K1s = rdbuilder_thor.make_rd_detached_K1()

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        K1s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_1(1270)+ ]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPiPiee["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_BuToKpPipPimEE_SameSign_line(
    name="SpruceRD_BuToKpPipPimEE_SameSign", prescale=1
):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **BtoKPiPiee["dielectrons"], pid_e_min=1.0, opposite_sign=False
    )
    K1s = rdbuilder_thor.make_rd_detached_K1()

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        K1s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_1(1270)+ ]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPiPiee["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_BuToKpPipPimMuMu_line(name="SpruceRD_BuToKpPipPimMuMu", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKPiPimumu["dimuons"], same_sign=False
    )
    K1s = rdbuilder_thor.make_rd_detached_K1()

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        K1s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_1(1270)+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPiPimumu["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_BuToKpPipPimMuMu_SameSign_line(
    name="SpruceRD_BuToKpPipPimMuMu_SameSign", prescale=1
):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **BtoKPiPimumu["dimuons"], same_sign=True
    )
    K1s = rdbuilder_thor.make_rd_detached_K1()

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        K1s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_1(1270)+]cc",
        name="make_rd_BToXll_for_" + name,
        **BtoKPiPimumu["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


##############################################
#      B+ -> K+ K+ K- ll sprucing lines      #
##############################################

from Hlt2Conf.lines.rd.b_to_xll_hlt2 import Bto3Kee, Bto3Kmumu


@register_line_builder(sprucing_lines)
def Spruce_BuToKpKpKmEE_line(name="SpruceRD_BuToKpKpKmEE", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **Bto3Kee["dielectrons"], pid_e_min=1.0, opposite_sign=True
    )
    K2s = rdbuilder_thor.make_rd_detached_K2()

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        K2s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_2(1770)+]cc",
        name="make_rd_BToXll_for_" + name,
        **Bto3Kee["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_BuToKpKpKmEE_SameSign_line(
    name="SpruceRD_BuToKpKpKmEE_SameSign", prescale=1
):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        **Bto3Kee["dielectrons"], pid_e_min=1.0, opposite_sign=False
    )
    K2s = rdbuilder_thor.make_rd_detached_K2()

    B = b_to_xll_builders.make_rd_BToXll(
        dielectrons,
        K2s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_2(1770)+]cc",
        name="make_rd_BToXll_for_" + name,
        **Bto3Kee["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dielectrons, B],
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_BuToKpKpKmMuMu_line(name="SpruceRD_BuToKpKpKmMuMu", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **Bto3Kmumu["dimuons"], same_sign=False
    )
    K2s = rdbuilder_thor.make_rd_detached_K2()

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        K2s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_2(1770)+]cc",
        name="make_rd_BToXll_for_" + name,
        **Bto3Kmumu["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def Spruce_BuToKpKpKmMuMu_SameSign_line(
    name="SpruceRD_BuToKpKpKmMuMu_SameSign", prescale=1
):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(
        **Bto3Kmumu["dimuons"], same_sign=True
    )
    K2s = rdbuilder_thor.make_rd_detached_K2()

    B = b_to_xll_builders.make_rd_BToXll(
        dimuons,
        K2s,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K_2(1770)+]cc",
        name="make_rd_BToXll_for_" + name,
        **Bto3Kmumu["B"],
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [dimuons, B],
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


##################
### MuE lines ####
##################


@register_line_builder(sprucing_lines)
@configurable
def Spruce_BuToKpMuE_line(name="SpruceRD_BuToKpMuE", prescale=1):
    """
    Sprucing line for B+ -> K+ mu+ e- (cc)
    """
    pvs = make_pvs()
    electronmuon = rdbuilder_thor.make_rd_detached_mue(
        same_sign=False, max_vchi2ndof=9.0
    )
    kaons = rdbuilder_thor.make_rd_detached_kaons(
        mipchi2dvprimary_min=9.0, pid=(F.PID_K > 0.0), pt_min=400.0 * MeV
    )

    Bu = b_to_xll_builders.make_rd_BToXll(
        electronmuon,
        kaons,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K+]cc",
        name="make_rd_BToXll_for_" + name,
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [electronmuon, Bu],
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
@configurable
def Spruce_B0ToKpPimMuE_line(name="SpruceRD_B0ToKpPimMuE", prescale=1):
    """
    Sprucing line for B0 -> (K*0 -> K+ pi-) mu+ e-
    """
    pvs = make_pvs()
    electronmuon = rdbuilder_thor.make_rd_detached_mue(
        same_sign=False, max_vchi2ndof=9.0
    )
    hadron = rdbuilder_thor.make_rd_detached_kstar0s(
        am_min=500 * MeV,
        am_max=2600 * MeV,
        pi_pid=(F.PID_K < -0.0),  ##new
        k_pid=(F.PID_K > 0.0),
        pi_ipchi2_min=9.0,
        k_ipchi2_min=9.0,
        pi_pt_min=250 * MeV,
        k_pt_min=250.0 * MeV,
        adocachi2cut=30.0,
        vchi2pdof_max=9.0,
    )

    B0 = b_to_xll_builders.make_rd_BToXll(
        electronmuon,
        hadron,
        pvs,
        Descriptor="[B0 -> J/psi(1S) K*(892)0 ]cc",
        name="make_rd_BToXll_for_" + name,
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [electronmuon, hadron, B0],
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
@configurable
def Spruce_BuToKpMuE_SameSignMuE_line(
    name="SpruceRD_BuToKpMuE_SameSignMuE", prescale=1
):
    """
    Sprucing line for B+ -> K+ mu- e- (cc)
    """
    pvs = make_pvs()
    electronmuon = rdbuilder_thor.make_rd_detached_mue(
        same_sign=True, max_vchi2ndof=9.0
    )
    kaons = rdbuilder_thor.make_rd_detached_kaons(
        mipchi2dvprimary_min=9.0, pid=(F.PID_K > 0.0), pt_min=400.0 * MeV
    )

    Bu = b_to_xll_builders.make_rd_BToXll(
        electronmuon,
        kaons,
        pvs,
        Descriptor="[B+ -> J/psi(1S) K+]cc",
        name="make_rd_BToXll_for_" + name,
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [electronmuon, Bu],
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
@configurable
def Spruce_B0ToKpPimMuE_SameSignMuE_line(
    name="SpruceRD_B0ToKpPimMuE_SameSignMuE", prescale=1
):
    """
    Sprucing line for B0 -> (K*0 -> K+ pi-) mu- e-
    """
    pvs = make_pvs()
    electronmuon = rdbuilder_thor.make_rd_detached_mue(
        same_sign=True, max_vchi2ndof=9.0
    )
    hadron = rdbuilder_thor.make_rd_detached_kstar0s(
        am_min=500 * MeV,
        am_max=2600 * MeV,
        pi_pid=(F.PID_K < -0.0),  ##new
        k_pid=(F.PID_K > 0.0),
        pi_ipchi2_min=9.0,
        k_ipchi2_min=9.0,
        pi_pt_min=250 * MeV,
        k_pt_min=250.0 * MeV,
        adocachi2cut=30.0,
        vchi2pdof_max=9.0,
    )

    B0 = b_to_xll_builders.make_rd_BToXll(
        electronmuon,
        hadron,
        pvs,
        Descriptor="[B0 -> J/psi(1S) K*(892)0 ]cc",
        name="make_rd_BToXll_for_" + name,
    )

    return SpruceLine(
        name=name,
        prescale=prescale,
        algs=rd_prefilter() + [electronmuon, hadron, B0],
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


###########################
# BToXpbarll Spruce lines #
###########################


@register_line_builder(sprucing_lines)
def BToPpPmEE_line(name="SpruceRD_BToPpPmEE", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(vfaspfchi2ndof_max=9.0)
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000.0 * MeV,
        pt_min=300.0 * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.0),
    )

    Bs = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dielectrons,
        protons,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="B_s0 -> J/psi(1S) p+ p~-",
    )
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dielectrons, protons, Bs],
        prescale=prescale,
        tagging_particles=True,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def BToPpPmEE_SameSign_line(name="SpruceRD_BToPpPmEESS", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        vfaspfchi2ndof_max=9.0, opposite_sign=False
    )
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000.0 * MeV,
        pt_min=300.0 * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.0),
    )

    Bs = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dielectrons,
        protons,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="B_s0 -> J/psi(1S) p+ p~-",
    )
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dielectrons, protons, Bs],
        prescale=prescale,
        tagging_particles=True,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def BuToL0PmEE_LL_line(name="SpruceRD_BuToL0PmEE_LL", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(vfaspfchi2ndof_max=9.0)
    lambdas = rdbuilder_thor.make_rd_lambda_lls()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000.0 * MeV,
        pt_min=300.0 * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.0),
    )

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dielectrons,
        lambdas,
        protons,
        pvs,
        am_Xpbar_min=2000.0 * MeV,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc",
    )
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dielectrons, lambdas, protons, Bu],
        prescale=prescale,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def BuToL0PmEE_LL_SameSign_line(name="SpruceRD_BuToL0PmEESS_LL", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        vfaspfchi2ndof_max=9.0, opposite_sign=False
    )
    lambdas = rdbuilder_thor.make_rd_lambda_lls()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000.0 * MeV,
        pt_min=300.0 * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.0),
    )

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dielectrons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc",
    )
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dielectrons, lambdas, protons, Bu],
        prescale=prescale,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


# @register_line_builder(sprucing_lines)
def BuToL0PmEE_DD_line(name="SpruceRD_BuToL0PmEE_DD", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(vfaspfchi2ndof_max=9.0)
    lambdas = rdbuilder_thor.make_rd_lambda_dds()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000.0 * MeV,
        pt_min=300.0 * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.0),
    )

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dielectrons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc",
    )
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dielectrons, lambdas, protons, Bu],
        prescale=prescale,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


# @register_line_builder(sprucing_lines)
def BuToL0PmEE_DD_SameSign_line(name="SpruceRD_BuToL0PmEESS_DD", prescale=1):
    pvs = make_pvs()
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(
        vfaspfchi2ndof_max=9.0, opposite_sign=False
    )
    lambdas = rdbuilder_thor.make_rd_lambda_dds()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000.0 * MeV,
        pt_min=300.0 * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.0),
    )

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dielectrons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc",
    )
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dielectrons, lambdas, protons, Bu],
        prescale=prescale,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def BToPpPmMuMu_line(name="SpruceRD_BToPpPmMuMu", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(vchi2pdof_max=9.0)
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000.0 * MeV,
        pt_min=300.0 * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.0),
    )

    Bs = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dimuons,
        protons,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="B_s0 -> J/psi(1S) p+ p~-",
    )
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dimuons, protons, Bs],
        prescale=prescale,
        tagging_particles=True,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def BToPpPmMuMu_SameSign_line(name="SpruceRD_BToPpPmMuMuSS", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(same_sign=True, vchi2pdof_max=9.0)
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000.0 * MeV,
        pt_min=300.0 * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.0),
    )

    Bs = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dimuons,
        protons,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="B_s0 -> J/psi(1S) p+ p~-",
    )
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dimuons, protons, Bs],
        prescale=prescale,
        tagging_particles=True,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def BuToL0PmMuMu_LL_line(name="SpruceRD_BuToL0PmMuMu_LL", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(vchi2pdof_max=9.0)
    lambdas = rdbuilder_thor.make_rd_lambda_lls()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000.0 * MeV,
        pt_min=300.0 * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.0),
    )

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dimuons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc",
    )
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dimuons, lambdas, protons, Bu],
        prescale=prescale,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


@register_line_builder(sprucing_lines)
def BuToL0PmMuMu_LL_SameSign_line(name="SpruceRD_BuToL0PmMuMuSS_LL", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(same_sign=True, vchi2pdof_max=9.0)
    lambdas = rdbuilder_thor.make_rd_lambda_lls()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000.0 * MeV,
        pt_min=300.0 * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.0),
    )

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dimuons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc",
    )
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dimuons, lambdas, protons, Bu],
        prescale=prescale,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


# @register_line_builder(sprucing_lines)
def BuToL0PmMuMu_DD_line(name="SpruceRD_BuToL0PmMuMu_DD", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(vchi2pdof_max=9.0)
    lambdas = rdbuilder_thor.make_rd_lambda_dds()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000.0 * MeV,
        pt_min=300.0 * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.0),
    )

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dimuons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc",
    )
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dimuons, lambdas, protons, Bu],
        prescale=prescale,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )


# @register_line_builder(sprucing_lines)
def BuToL0PmMuMu_DD_SameSign_line(name="SpruceRD_BuToL0PmMuMuSS_DD", prescale=1):
    pvs = make_pvs()
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(same_sign=True, vchi2pdof_max=9.0)
    lambdas = rdbuilder_thor.make_rd_lambda_dds()
    protons = rdbuilder_thor.make_rd_detached_protons(
        p_min=5000.0 * MeV,
        pt_min=300.0 * MeV,
        mipchi2dvprimary_min=9,
        pid=(F.PID_P > 0.0),
    )

    Bu = b_to_xll_builders.make_b_to_Xpbar_dilepton(
        dimuons,
        lambdas,
        protons,
        pvs,
        name="make_rd_BToXpbarll_for_" + name,
        descriptor="[B- -> J/psi(1S) Lambda0 p~-]cc",
    )
    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [dimuons, lambdas, protons, Bu],
        prescale=prescale,
        hlt2_filter_code=parse_hlt2_filter_code(name),
    )
