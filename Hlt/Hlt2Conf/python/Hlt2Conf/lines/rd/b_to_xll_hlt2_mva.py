###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf import rdbuilder_thor
from RecoConf.algorithms_thor import ParticleFilter
from RecoConf.reconstruction_objects import make_pvs

from .builders import (
    b_to_xll_builders,
    b_to_xll_tmva_builder,
    rad_incl_builder,
    rd_isolation,
)
from .builders.rd_prefilters import (
    _RD_MONITORING_VARIABLES,
    RD_PERSIST_CALO_CLUSTERS,
    RD_PERSIST_CALO_DIGITS,
    rd_prefilter,
)
from .builders.RpK_builders import make_dihadron_from_pK, make_lambdab_to_pkll


# ----------------------------------------
class llx_mva_options:
    """
    Numbers taken from:
    Bs: http://lhcbdoc.web.cern.ch/lhcbdoc/stripping/config/stripping34r0p1/leptonic/strippingb2llxbdt_bs2eephiline.html
    Lb: http://lhcbdoc.web.cern.ch/lhcbdoc/stripping/config/stripping34r0p1/leptonic/strippingb2llxbdt_lb2eepkline.html
    Bd: http://lhcbdoc.web.cern.ch/lhcbdoc/stripping/config/stripping34r0p1/leptonic/strippingb2llxbdt_bd2eekstarline.html
    Bp: http://lhcbdoc.web.cern.ch/lhcbdoc/stripping/config/stripping34r0p1/leptonic/strippingb2llxbdt_bu2eek
    """

    @classmethod
    def __class_getitem__(cls, key):
        low_mass = key.endswith("_low_mass")
        key = key.replace("_low_mass", "")

        if key == "dielectron_tight":
            d_opt = cls.get_dielectron()
            d_opt["pid_e_min"] = +1.0
        elif key == "dielectron_loose":
            d_opt = cls.get_dielectron()
            d_opt["pid_e_min"] = -1.0
        elif key == "dielectron_nopid":
            d_opt = cls.get_dielectron()
            d_opt["pid_e_min"] = -999.0
        elif key == "dielectron_nopid_jpsi":
            d_opt = cls.get_dielectron()
            d_opt["pid_e_min"] = -999.0
            d_opt["am_min"] = 2_500.0 * MeV
            d_opt["am_max"] = 3_500.0 * MeV
        # ----------------------------
        elif key == "dimuon":
            d_opt = cls.get_dimuon()
        # ----------------------------
        elif key == "kaon":
            d_opt = cls.get_kaon()
        elif key == "Kst0s":
            d_opt = cls.get_kstar0s()
        elif key == "phi":
            d_opt = cls.get_phi()
        elif key == "pk":
            d_opt = cls.get_lambda()
        # ----------------------------
        elif key == "lb":
            d_opt = cls.get_lb(low_mass)
        elif key == "bs":
            d_opt = cls.get_bs(low_mass)
        elif key == "bp":
            d_opt = cls.get_bp(low_mass)
        elif key == "bd":
            d_opt = cls.get_bd(low_mass)
        # ----------------------------
        else:
            raise ValueError

        return d_opt

    # ----------------------------
    @classmethod
    def get_dielectron(cls):
        d_opt = dict()
        d_opt["adocachi2cut_max"] = 10.0
        d_opt["ipchi2_e_min"] = 1.0
        d_opt["pt_diE_min"] = 0.0 * MeV
        d_opt["pt_e_min"] = 200.0 * MeV
        d_opt["p_e_min"] = 0.0 * GeV
        d_opt["vfaspfchi2ndof_max"] = 16.0
        d_opt["am_min"] = 0.0 * MeV
        d_opt["am_max"] = 5_500.0 * MeV
        d_opt["bpvvdchi2_min"] = 0.0
        d_opt["parent_id"] = "J/psi(1S)"
        d_opt["opposite_sign"] = True
        d_opt["with_brem"] = True

        return d_opt

    # ----------------------------
    @classmethod
    def get_dimuon(cls):
        d_opt = dict()
        d_opt["am_min"] = 0.0 * MeV
        d_opt["am_max"] = 6000.0 * MeV
        d_opt["pidmu_muon_min"] = 0
        d_opt["ipchi2_muon_min"] = 3.0
        d_opt["pt_muon_min"] = 0.250 * GeV
        d_opt["parent_id"] = "J/psi(1S)"
        d_opt["pt_dimuon_min"] = 0.0 * MeV
        d_opt["bpvvdchi2_min"] = 0.0
        d_opt["adocachi2cut_max"] = 30.0
        d_opt["vchi2pdof_max"] = 16.0
        d_opt["p_muon_min"] = 2_000 * MeV
        d_opt["same_sign"] = False

        return d_opt

    # ----------------------------
    @classmethod
    def get_kaon(cls):
        d_opt = dict()
        d_opt["name"] = "rd_detached_kaons_{hash}"
        d_opt["pt_min"] = 250.0 * MeV
        d_opt["mipchi2dvprimary_min"] = 4.0  # TBC
        d_opt["p_min"] = 0.0 * GeV
        d_opt["pid"] = F.PID_K > -2.0

        return d_opt

    # ----------------------------
    @classmethod
    def get_kstar0s(cls):
        d_opt = dict()
        d_opt["vchi2pdof_max"] = 16.0
        d_opt["am_min"] = 592.0 * MeV
        d_opt["am_max"] = 1192.0 * MeV
        d_opt["pi_p_min"] = 2.0 * GeV
        d_opt["pi_pt_min"] = 250.0 * MeV
        d_opt["pi_ipchi2_min"] = 4.0
        d_opt["pi_pid"] = F.PID_K < 1e-5
        d_opt["k_p_min"] = 2.0 * GeV
        d_opt["k_pt_min"] = 250.0 * MeV
        d_opt["k_ipchi2_min"] = 4.0
        d_opt["k_pid"] = F.PID_K > 0.0
        d_opt["kstar0_pt_min"] = 0.0 * MeV
        d_opt["adocachi2cut"] = 30.0
        d_opt["vchi2pdof_max"] = 16.0
        d_opt["bpvipchi2_min"] = None
        d_opt["same_sign"] = False

        return d_opt

    # ----------------------------
    @classmethod
    def get_lambda(cls):
        d_opt = dict()
        d_opt["max_mass"] = 5600 * MeV
        d_opt["min_pt"] = 0 * MeV
        d_opt["max_vtxchi2dof"] = 25
        d_opt["max_docachi2"] = 30.0
        d_opt["same_sign"] = False

        return d_opt

    # ----------------------------
    @classmethod
    def get_phi(cls):
        d_opt = dict()
        d_opt["vchi2pdof_max"] = 25.0
        d_opt["am_min"] = 950.0 * MeV
        d_opt["am_max"] = 1100.0 * MeV
        d_opt["phi_pt_min"] = 0.0 * MeV
        d_opt["adocachi2cut"] = 30.0

        d_opt["k_p_min"] = 0.0 * GeV
        d_opt["k_pt_min"] = 200.0 * MeV
        d_opt["k_ipchi2_min"] = 2.0
        d_opt["k_pid"] = F.PID_K > 0.0

        return d_opt

    # ----------------------------
    @classmethod
    def get_bs(cls, low_mass):
        d_opt = cls.get_bx(low_mass)
        d_opt["name"] = "bsphill_mva_maker_{hash}"
        d_opt["Descriptor"] = "[B_s0 -> J/psi(1S) phi(1020)]cc"

        return d_opt

    # ----------------------------
    @classmethod
    def get_bp(cls, low_mass):
        d_opt = cls.get_bx(low_mass)
        d_opt["name"] = "bpkll_mva_maker_{hash}"
        d_opt["Descriptor"] = "[B+ -> J/psi(1S) K+]cc"

        return d_opt

    # ----------------------------
    @classmethod
    def get_bd(cls, low_mass):
        d_opt = cls.get_bx(low_mass)
        d_opt["name"] = "bdkstll_mva_maker_{hash}"
        d_opt["Descriptor"] = "[B0 -> J/psi(1S) K*(892)0]cc"

        return d_opt

    # ----------------------------
    @classmethod
    def get_lb(cls, low_mass):
        d_opt = dict()
        d_opt["name"] = "lbpkll_mva_maker_{hash}"
        d_opt["descriptor"] = "[Lambda_b0 -> J/psi(1S) Lambda(1520)0]cc"

        if low_mass:
            d_opt["min_mass"] = 3_500 * MeV
            d_opt["max_mass"] = 7100 * MeV
        else:
            d_opt["min_mass"] = 3700 * MeV
            d_opt["max_mass"] = 7100 * MeV

        d_opt["min_dira"] = 0.999
        d_opt["min_flightdist"] = 0
        d_opt["max_vtxchi2dof"] = 25
        d_opt["max_ipchi2"] = 400

        return d_opt

    # ----------------------------
    @classmethod
    def get_bx(cls, low_mass):
        d_opt = dict()
        if low_mass:
            d_opt["am_min"] = 3_500.0 * MeV
            d_opt["am_max"] = 7_000.0 * MeV
        else:
            d_opt["am_min"] = 4_500.0 * MeV
            d_opt["am_max"] = 7_000.0 * MeV

        d_opt["B_pt_min"] = 0 * MeV
        d_opt["FDchi2_min"] = 50.0
        d_opt["vchi2pdof_max"] = 9.0
        d_opt["bpvipchi2_max"] = 25.0
        d_opt["min_cosine"] = 0.9995
        d_opt["low_factor"] = 0.5
        d_opt["high_factor"] = 1.5

        return d_opt


# ----------------------------
def get_isolations(d_part):
    l_iso = []
    for name, (part, dr) in d_part.items():
        iso_parts = rd_isolation.select_parts_for_isolation(
            names=[name],
            candidates=[part],
            cut=F.require_all(F.DR2 < dr, ~F.FIND_IN_TREE()),
        )

        l_iso += iso_parts

    return l_iso


# ----------------------------
def add_mva(bmeson, pvs, mva_name, line_name, mva_wp):
    b = b_to_xll_tmva_builder.make_b2(
        bmeson, pvs, mva_name, mva_wp, filter_name=f"{mva_name}_{line_name}_{{hash}}"
    )
    bdt_moni = rad_incl_builder.make_monitor(
        line_name,
        bmeson,
        b_to_xll_tmva_builder.xll_BDT_functor(pvs, mva_name),
        HistogramName="BDT",
    )

    return b, bdt_moni


# ----------------------------
def filter_tracks(tracks, line_name, ghst_prob=0.5, trchi2dof_max=3.0):
    code = F.require_all(F.GHOSTPROB < ghst_prob, F.CHI2DOF < trchi2dof_max)
    tracks = ParticleFilter(tracks, Cut=F.FILTER(code))

    return tracks


# ----------------------------
def filter_dileptons(dilepton, line_name, ghst_prob=0.5, trchi2dof_max=3.0):
    code_1 = (F.CHILD(1, F.GHOSTPROB) < ghst_prob) & (
        F.CHILD(2, F.GHOSTPROB) < ghst_prob
    )
    code_2 = (F.CHILD(1, F.CHI2DOF) < trchi2dof_max) & (
        F.CHILD(2, F.CHI2DOF) < trchi2dof_max
    )
    code = code_1 & code_2

    flt = ParticleFilter(dilepton, Cut=F.FILTER(code))

    return flt


# ----------------------------
def tag_dielectron_pid(dielectrons, name, pid_tag=4.0):
    code = (F.CHILD(1, F.PID_E) > pid_tag) | (F.CHILD(2, F.PID_E) > pid_tag)

    flt = ParticleFilter(dielectrons, Cut=F.FILTER(code))

    return flt


# ----------------------------
def filter_anti_pid_e(dielectrons, name, pid_low=-1.0, pid_hig=+1.0):
    code_1 = (F.CHILD(1, F.PID_E) > pid_low) & (F.CHILD(2, F.PID_E) > pid_low)
    code_2 = (F.CHILD(1, F.PID_E) < pid_hig) | (F.CHILD(2, F.PID_E) < pid_hig)
    code = code_1 & code_2

    flt = ParticleFilter(dielectrons, Cut=F.FILTER(code))

    return flt


# ----------------------------
all_lines = {}


########################################
#      Bp -> K+ ll HLT2 lines          #
########################################
@register_line_builder(all_lines)
@configurable
def bp2kpee_mva_line(name="Hlt2RD_BuToKpEE_MVA", prescale=1, mva_wp=0.15):
    d_opt = llx_mva_options["dielectron_tight"]
    detached_dielectrons = rdbuilder_thor.make_rd_detached_dielectron(**d_opt)
    detached_dielectrons = filter_dileptons(detached_dielectrons, name)
    pvs = make_pvs()

    d_opt = llx_mva_options["kaon"]
    kaons = rdbuilder_thor.make_rd_detached_kaons(**d_opt)
    kaons = filter_tracks(kaons, name)

    d_opt = llx_mva_options["bp"]
    d_opt["dileptons"] = detached_dielectrons
    d_opt["hadrons"] = kaons
    d_opt["pvs"] = pvs
    bp = b_to_xll_builders.make_rd_BToXll(**d_opt)
    b, bdt_moni = add_mva(bp, pvs, "bullk", name, mva_wp)

    d_iso = {"bp": (bp, 0.25)}
    l_iso = get_isolations(d_iso)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [detached_dielectrons, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


# ----------------------------
@register_line_builder(all_lines)
@configurable
def bp2kpmumu_mva_line(name="Hlt2RD_BuToKpMuMu_MVA", prescale=1, mva_wp=0.15):
    d_opt = llx_mva_options["dimuon"]
    detached_dimuons = rdbuilder_thor.make_rd_detached_dimuon(**d_opt)
    detached_dimuons = filter_dileptons(detached_dimuons, name)
    pvs = make_pvs()

    d_opt = llx_mva_options["kaon"]
    kaons = rdbuilder_thor.make_rd_detached_kaons(**d_opt)
    kaons = filter_tracks(kaons, name)

    d_opt = llx_mva_options["bp_low_mass"]
    d_opt["dileptons"] = detached_dimuons
    d_opt["hadrons"] = kaons
    d_opt["pvs"] = make_pvs()
    bp = b_to_xll_builders.make_rd_BToXll(**d_opt)
    b, bdt_moni = add_mva(bp, pvs, "bullk", name, mva_wp)

    d_iso = {"bp": (bp, 0.25)}
    l_iso = get_isolations(d_iso)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [detached_dimuons, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


# ----------------------------
@register_line_builder(all_lines)
@configurable
def bp2kpee_ss_mva_line(name="Hlt2RD_BuToKpEE_SameSign_MVA", prescale=1, mva_wp=0.15):
    d_opt = llx_mva_options["dielectron_tight"]
    d_opt["opposite_sign"] = False
    detached_dielectrons = rdbuilder_thor.make_rd_detached_dielectron(**d_opt)
    detached_dielectrons = filter_dileptons(detached_dielectrons, name)
    pvs = make_pvs()

    d_opt = llx_mva_options["kaon"]
    kaons = rdbuilder_thor.make_rd_detached_kaons(**d_opt)
    kaons = filter_tracks(kaons, name)

    d_opt = llx_mva_options["bp"]
    d_opt["dileptons"] = detached_dielectrons
    d_opt["hadrons"] = kaons
    d_opt["pvs"] = pvs
    bp = b_to_xll_builders.make_rd_BToXll(**d_opt)
    b, bdt_moni = add_mva(bp, pvs, "bullk", name, mva_wp)

    d_iso = {"bp": (bp, 0.25)}
    l_iso = get_isolations(d_iso)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [detached_dielectrons, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


# ----------------------------
@register_line_builder(all_lines)
@configurable
def bp2kpmumu_ss_mva_line(
    name="Hlt2RD_BuToKpMuMu_SameSign_MVA", prescale=1, mva_wp=-1.00
):
    d_opt = llx_mva_options["dimuon"]
    d_opt["same_sign"] = True
    detached_dimuons = rdbuilder_thor.make_rd_detached_dimuon(**d_opt)
    detached_dimuons = filter_dileptons(detached_dimuons, name)
    pvs = make_pvs()

    d_opt = llx_mva_options["kaon"]
    kaons = rdbuilder_thor.make_rd_detached_kaons(**d_opt)
    kaons = filter_tracks(kaons, name)

    d_opt = llx_mva_options["bp"]
    d_opt["dileptons"] = detached_dimuons
    d_opt["hadrons"] = kaons
    d_opt["pvs"] = make_pvs()
    bp = b_to_xll_builders.make_rd_BToXll(**d_opt)
    b, bdt_moni = add_mva(bp, pvs, "bullk", name, mva_wp)

    d_iso = {"bp": (bp, 0.25)}
    l_iso = get_isolations(d_iso)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [detached_dimuons, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


# ----------------------------
@register_line_builder(all_lines)
@configurable
def bp2kpee_mva_line_misid(name="Hlt2RD_BuToKpEE_MVA_misid", prescale=0.1, mva_wp=0.15):
    d_opt = llx_mva_options["dielectron_nopid"]
    detached_dielectrons = rdbuilder_thor.make_rd_detached_dielectron(**d_opt)
    detached_dielectrons = filter_dileptons(detached_dielectrons, name)
    detached_dielectrons = filter_anti_pid_e(detached_dielectrons, name)

    pvs = make_pvs()

    d_opt = llx_mva_options["kaon"]
    kaons = rdbuilder_thor.make_rd_detached_kaons(**d_opt)
    kaons = filter_tracks(kaons, name)

    d_opt = llx_mva_options["bp"]
    d_opt["dileptons"] = detached_dielectrons
    d_opt["hadrons"] = kaons
    d_opt["pvs"] = pvs
    bp = b_to_xll_builders.make_rd_BToXll(**d_opt)
    b, bdt_moni = add_mva(bp, pvs, "bullk", name, mva_wp)

    d_iso = {"bp": (bp, 0.25)}
    l_iso = get_isolations(d_iso)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [detached_dielectrons, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


# ----------------------------
@register_line_builder(all_lines)
@configurable
def bp2kpee_mva_line_cal(name="Hlt2RD_BuToKpEE_MVA_cal", prescale=1, mva_wp=0.30):
    d_opt = llx_mva_options["dielectron_nopid_jpsi"]
    detached_dielectrons = rdbuilder_thor.make_rd_detached_dielectron(**d_opt)
    detached_dielectrons = filter_dileptons(detached_dielectrons, name)
    detached_dielectrons = tag_dielectron_pid(detached_dielectrons, name)

    pvs = make_pvs()

    d_opt = llx_mva_options["kaon"]
    kaons = rdbuilder_thor.make_rd_detached_kaons(**d_opt)
    kaons = filter_tracks(kaons, name)

    d_opt = llx_mva_options["bp"]
    d_opt["dileptons"] = detached_dielectrons
    d_opt["hadrons"] = kaons
    d_opt["pvs"] = pvs
    bp = b_to_xll_builders.make_rd_BToXll(**d_opt)
    b, bdt_moni = add_mva(bp, pvs, "bullk", name, mva_wp)

    d_iso = {"bp": (bp, 0.25)}
    l_iso = get_isolations(d_iso)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [detached_dielectrons, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


########################################
#      B0 -> K+ pi- ll HLT2 lines      #
########################################
@register_line_builder(all_lines)
@configurable
def bd2kpiee_mva_line(name="Hlt2RD_B0ToKpPimEE_MVA", prescale=1, mva_wp=0.30):
    pvs = make_pvs()

    d_opt = llx_mva_options["dielectron_tight"]
    detached_dielectrons = rdbuilder_thor.make_rd_detached_dielectron(**d_opt)
    detached_dielectrons = filter_dileptons(detached_dielectrons, name)

    d_opt = llx_mva_options["Kst0s"]
    Kst0s = rdbuilder_thor.make_rd_detached_kstar0s(**d_opt)
    Kst0s = filter_dileptons(Kst0s, name)

    d_opt = llx_mva_options["bd"]
    d_opt["dileptons"] = detached_dielectrons
    d_opt["hadrons"] = Kst0s
    d_opt["pvs"] = pvs
    bd = b_to_xll_builders.make_rd_BToXll(**d_opt)
    b, bdt_moni = add_mva(bd, pvs, "bdllkst", name, mva_wp)

    d_iso = {"bd": (bd, 0.25)}
    l_iso = get_isolations(d_iso)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [detached_dielectrons, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


# ----------------------------
@register_line_builder(all_lines)
@configurable
def bd2kpimm_mva_line(name="Hlt2RD_B0ToKpPimMuMu_MVA", prescale=1, mva_wp=0.30):
    pvs = make_pvs()

    d_opt = llx_mva_options["dimuon"]
    detached_dimuons = rdbuilder_thor.make_rd_detached_dimuon(**d_opt)
    detached_dimuons = filter_dileptons(detached_dimuons, name)

    d_opt = llx_mva_options["Kst0s"]
    Kst0s = rdbuilder_thor.make_rd_detached_kstar0s(**d_opt)
    Kst0s = filter_dileptons(Kst0s, name)

    d_opt = llx_mva_options["bd_low_mass"]
    d_opt["dileptons"] = detached_dimuons
    d_opt["hadrons"] = Kst0s
    d_opt["pvs"] = pvs
    bd = b_to_xll_builders.make_rd_BToXll(**d_opt)
    b, bdt_moni = add_mva(bd, pvs, "bdllkst", name, mva_wp)

    d_iso = {"bd": (bd, 0.25)}
    l_iso = get_isolations(d_iso)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [detached_dimuons, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


# ----------------------------
@register_line_builder(all_lines)
@configurable
def bd2kpiee_ss_mva_line(
    name="Hlt2RD_B0ToKpPimEE_SameSign_MVA", prescale=0.7, mva_wp=0.30
):
    pvs = make_pvs()

    d_opt = llx_mva_options["dielectron_tight"]
    d_opt["opposite_sign"] = False
    detached_dielectrons = rdbuilder_thor.make_rd_detached_dielectron(**d_opt)
    detached_dielectrons = filter_dileptons(detached_dielectrons, name)

    d_opt = llx_mva_options["Kst0s"]
    Kst0s = rdbuilder_thor.make_rd_detached_kstar0s(**d_opt)
    Kst0s = filter_dileptons(Kst0s, name)

    d_opt = llx_mva_options["bd"]
    d_opt["dileptons"] = detached_dielectrons
    d_opt["hadrons"] = Kst0s
    d_opt["pvs"] = pvs
    bd = b_to_xll_builders.make_rd_BToXll(**d_opt)
    b, bdt_moni = add_mva(bd, pvs, "bdllkst", name, mva_wp)

    d_iso = {"bd": (bd, 0.25)}
    l_iso = get_isolations(d_iso)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [detached_dielectrons, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


# ----------------------------
@register_line_builder(all_lines)
@configurable
def bd2kpimm_ss_mva_line(
    name="Hlt2RD_B0ToKpPimMuMu_SameSign_MVA", prescale=1, mva_wp=0.20
):
    pvs = make_pvs()

    d_opt = llx_mva_options["dimuon"]
    d_opt["same_sign"] = True
    detached_dimuons = rdbuilder_thor.make_rd_detached_dimuon(**d_opt)
    detached_dimuons = filter_dileptons(detached_dimuons, name)

    d_opt = llx_mva_options["Kst0s"]
    Kst0s = rdbuilder_thor.make_rd_detached_kstar0s(**d_opt)
    Kst0s = filter_dileptons(Kst0s, name)

    d_opt = llx_mva_options["bd"]
    d_opt["dileptons"] = detached_dimuons
    d_opt["hadrons"] = Kst0s
    d_opt["pvs"] = pvs
    bd = b_to_xll_builders.make_rd_BToXll(**d_opt)
    b, bdt_moni = add_mva(bd, pvs, "bdllkst", name, mva_wp)

    d_iso = {"bd": (bd, 0.25)}
    l_iso = get_isolations(d_iso)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [detached_dimuons, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


# ----------------------------
@register_line_builder(all_lines)
@configurable
def bd2kpiee_mva_line_misid(
    name="Hlt2RD_B0ToKpPimEE_MVA_misid", prescale=0.1, mva_wp=0.30
):
    pvs = make_pvs()

    d_opt = llx_mva_options["dielectron_nopid"]
    detached_dielectrons = rdbuilder_thor.make_rd_detached_dielectron(**d_opt)
    detached_dielectrons = filter_dileptons(detached_dielectrons, name)
    detached_dielectrons = filter_anti_pid_e(detached_dielectrons, name)

    d_opt = llx_mva_options["Kst0s"]
    Kst0s = rdbuilder_thor.make_rd_detached_kstar0s(**d_opt)
    Kst0s = filter_dileptons(Kst0s, name)

    d_opt = llx_mva_options["bd"]
    d_opt["dileptons"] = detached_dielectrons
    d_opt["hadrons"] = Kst0s
    d_opt["pvs"] = pvs
    bd = b_to_xll_builders.make_rd_BToXll(**d_opt)
    b, bdt_moni = add_mva(bd, pvs, "bdllkst", name, mva_wp)

    d_iso = {"bd": (bd, 0.25)}
    l_iso = get_isolations(d_iso)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [detached_dielectrons, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


# ----------------------------
@register_line_builder(all_lines)
@configurable
def bd2kpiee_mva_line_cal(name="Hlt2RD_B0ToKpPimEE_MVA_cal", prescale=1, mva_wp=0.50):
    d_opt = llx_mva_options["dielectron_nopid_jpsi"]
    detached_dielectrons = rdbuilder_thor.make_rd_detached_dielectron(**d_opt)
    detached_dielectrons = filter_dileptons(detached_dielectrons, name)
    detached_dielectrons = tag_dielectron_pid(detached_dielectrons, name)

    pvs = make_pvs()

    d_opt = llx_mva_options["Kst0s"]
    Kst0s = rdbuilder_thor.make_rd_detached_kstar0s(**d_opt)
    Kst0s = filter_dileptons(Kst0s, name)

    d_opt = llx_mva_options["bd"]
    d_opt["dileptons"] = detached_dielectrons
    d_opt["hadrons"] = Kst0s
    d_opt["pvs"] = pvs
    bd = b_to_xll_builders.make_rd_BToXll(**d_opt)
    b, bdt_moni = add_mva(bd, pvs, "bdllkst", name, mva_wp)

    d_iso = {"bd": (bd, 0.25)}
    l_iso = get_isolations(d_iso)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [detached_dielectrons, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


########################################
#   Lb -> Lambda pK ll
########################################
@register_line_builder(all_lines)
@configurable
def lb2pkee_mva_line(name="Hlt2RD_LbTopKEE_MVA", prescale=1, mva_wp=0.30):
    pvs = make_pvs()

    d_opt = llx_mva_options["dielectron_tight"]
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(**d_opt)
    dielectrons = filter_dileptons(dielectrons, name)

    d_opt = llx_mva_options["pk"]
    dihadrons = make_dihadron_from_pK()
    dihadrons = filter_dileptons(dihadrons, name)

    d_opt = llx_mva_options["lb"]
    d_opt["dileptons"] = dielectrons
    d_opt["dihadrons"] = dihadrons
    lb = make_lambdab_to_pkll(**d_opt)

    d_iso = {"Lb": (lb, 0.25)}
    l_iso = get_isolations(d_iso)
    b, bdt_moni = add_mva(lb, pvs, "lbllpk", name, mva_wp)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, dihadrons, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


# ----------------------------
@register_line_builder(all_lines)
@configurable
def lb2pkmm_mva_line(name="Hlt2RD_LbTopKMuMu_MVA", prescale=1, mva_wp=0.30):
    pvs = make_pvs()

    d_opt = llx_mva_options["dimuon"]
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(**d_opt)
    dimuons = filter_dileptons(dimuons, name)

    d_opt = llx_mva_options["pk"]
    dihadrons = make_dihadron_from_pK()
    dihadrons = filter_dileptons(dihadrons, name)

    d_opt = llx_mva_options["lb_low_mass"]
    d_opt["dileptons"] = dimuons
    d_opt["dihadrons"] = dihadrons
    lb = make_lambdab_to_pkll(**d_opt)

    d_iso = {"Lb": (lb, 0.25)}
    l_iso = get_isolations(d_iso)
    b, bdt_moni = add_mva(lb, pvs, "lbllpk", name, mva_wp)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, dihadrons, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


# ----------------------------
@register_line_builder(all_lines)
@configurable
def lb2pkee_ss_mva_line(name="Hlt2RD_LbTopKEE_SameSign_MVA", prescale=0.5, mva_wp=0.30):
    pvs = make_pvs()

    d_opt = llx_mva_options["dielectron_tight"]
    d_opt["opposite_sign"] = False
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(**d_opt)
    dielectrons = filter_dileptons(dielectrons, name)

    d_opt = llx_mva_options["pk"]
    dihadrons = make_dihadron_from_pK()
    dihadrons = filter_dileptons(dihadrons, name)

    d_opt = llx_mva_options["lb"]
    d_opt["dileptons"] = dielectrons
    d_opt["dihadrons"] = dihadrons
    lb = make_lambdab_to_pkll(**d_opt)

    d_iso = {"Lb": (lb, 0.25)}
    l_iso = get_isolations(d_iso)
    b, bdt_moni = add_mva(lb, pvs, "lbllpk", name, mva_wp)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, dihadrons, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


# ----------------------------
@register_line_builder(all_lines)
@configurable
def lb2pkmm_ss_mva_line(name="Hlt2RD_LbTopKMuMu_SameSign_MVA", prescale=1, mva_wp=-1):
    pvs = make_pvs()

    d_opt = llx_mva_options["dimuon"]
    d_opt["same_sign"] = True
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(**d_opt)
    dimuons = filter_dileptons(dimuons, name)

    d_opt = llx_mva_options["pk"]
    dihadrons = make_dihadron_from_pK()
    dihadrons = filter_dileptons(dihadrons, name)

    d_opt = llx_mva_options["lb"]
    d_opt["dileptons"] = dimuons
    d_opt["dihadrons"] = dihadrons
    lb = make_lambdab_to_pkll(**d_opt)

    d_iso = {"Lb": (lb, 0.25)}
    l_iso = get_isolations(d_iso)
    b, bdt_moni = add_mva(lb, pvs, "lbllpk", name, mva_wp)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, dihadrons, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


# ----------------------------
@register_line_builder(all_lines)
@configurable
def lb2pkee_mva_line_misid(name="Hlt2RD_LbTopKEE_MVA_misid", prescale=0.5, mva_wp=0.30):
    pvs = make_pvs()

    d_opt = llx_mva_options["dielectron_nopid"]
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(**d_opt)
    dielectrons = filter_dileptons(dielectrons, name)
    dielectrons = filter_anti_pid_e(dielectrons, name)

    d_opt = llx_mva_options["pk"]
    dihadrons = make_dihadron_from_pK()
    dihadrons = filter_dileptons(dihadrons, name)

    d_opt = llx_mva_options["lb"]
    d_opt["dileptons"] = dielectrons
    d_opt["dihadrons"] = dihadrons
    lb = make_lambdab_to_pkll(**d_opt)

    d_iso = {"Lb": (lb, 0.25)}
    l_iso = get_isolations(d_iso)
    b, bdt_moni = add_mva(lb, pvs, "lbllpk", name, mva_wp)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, dihadrons, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


########################################
# Bs -> phi ll
########################################
@register_line_builder(all_lines)
@configurable
def BsToPhiEE_MVA(name="Hlt2RD_BsToPhiEE_MVA", prescale=1, mva_wp=0.00):
    pvs = make_pvs()

    d_opt = llx_mva_options["dielectron_tight"]
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(**d_opt)
    dielectrons = filter_dileptons(dielectrons, name)

    d_opt = llx_mva_options["phi"]
    phis = rdbuilder_thor.make_rd_detached_phis(**d_opt)
    phis = filter_dileptons(phis, name)

    d_opt = llx_mva_options["bs"]
    d_opt["dileptons"] = dielectrons
    d_opt["hadrons"] = phis
    d_opt["pvs"] = pvs
    bs = b_to_xll_builders.make_rd_BToXll(**d_opt)
    b, bdt_moni = add_mva(bs, pvs, "bsllphi", name, mva_wp)

    d_iso = {"B": (bs, 0.25)}
    l_iso = get_isolations(d_iso)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, phis, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


# ----------------------------
@register_line_builder(all_lines)
@configurable
def BsToPhiMuMu_MVA(name="Hlt2RD_BsToPhiMuMu_MVA", prescale=1, mva_wp=0.00):
    pvs = make_pvs()

    d_opt = llx_mva_options["dimuon"]
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(**d_opt)
    dimuons = filter_dileptons(dimuons, name)

    d_opt = llx_mva_options["phi"]
    phis = rdbuilder_thor.make_rd_detached_phis(**d_opt)
    phis = filter_dileptons(phis, name)

    d_opt = llx_mva_options["bs_low_mass"]
    d_opt["dileptons"] = dimuons
    d_opt["hadrons"] = phis
    d_opt["pvs"] = pvs
    bs = b_to_xll_builders.make_rd_BToXll(**d_opt)
    b, bdt_moni = add_mva(bs, pvs, "bsllphi", name, mva_wp)

    d_iso = {"B": (bs, 0.25)}
    l_iso = get_isolations(d_iso)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, phis, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


# ----------------------------
@register_line_builder(all_lines)
@configurable
def BsToPhiEE_ss_MVA(name="Hlt2RD_BsToPhiEE_SameSign_MVA", prescale=0.1, mva_wp=0.00):
    pvs = make_pvs()

    d_opt = llx_mva_options["dielectron_tight"]
    d_opt["opposite_sign"] = False
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(**d_opt)
    dielectrons = filter_dileptons(dielectrons, name)

    d_opt = llx_mva_options["phi"]
    phis = rdbuilder_thor.make_rd_detached_phis(**d_opt)
    phis = filter_dileptons(phis, name)

    d_opt = llx_mva_options["bs"]
    d_opt["dileptons"] = dielectrons
    d_opt["hadrons"] = phis
    d_opt["pvs"] = pvs
    bs = b_to_xll_builders.make_rd_BToXll(**d_opt)
    b, bdt_moni = add_mva(bs, pvs, "bsllphi", name, mva_wp)

    d_iso = {"B": (bs, 0.25)}
    l_iso = get_isolations(d_iso)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, phis, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


# ----------------------------
@register_line_builder(all_lines)
@configurable
def BsToPhiMuMu_ss_MVA(name="Hlt2RD_BsToPhiMuMu_SameSign_MVA", prescale=1, mva_wp=-1):
    pvs = make_pvs()

    d_opt = llx_mva_options["dimuon"]
    d_opt["same_sign"] = True
    dimuons = rdbuilder_thor.make_rd_detached_dimuon(**d_opt)
    dimuons = filter_dileptons(dimuons, name)

    d_opt = llx_mva_options["phi"]
    phis = rdbuilder_thor.make_rd_detached_phis(**d_opt)
    phis = filter_dileptons(phis, name)

    d_opt = llx_mva_options["bs"]
    d_opt["dileptons"] = dimuons
    d_opt["hadrons"] = phis
    d_opt["pvs"] = pvs
    bs = b_to_xll_builders.make_rd_BToXll(**d_opt)
    b, bdt_moni = add_mva(bs, pvs, "bsllphi", name, mva_wp)

    d_iso = {"B": (bs, 0.25)}
    l_iso = get_isolations(d_iso)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dimuons, phis, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


# ----------------------------
@register_line_builder(all_lines)
@configurable
def BsToPhiEE_MVA_misid(name="Hlt2RD_BsToPhiEE_MVA_misid", prescale=0.05, mva_wp=0.00):
    pvs = make_pvs()

    d_opt = llx_mva_options["dielectron_nopid"]
    dielectrons = rdbuilder_thor.make_rd_detached_dielectron(**d_opt)
    dielectrons = filter_dileptons(dielectrons, name)
    dielectrons = filter_anti_pid_e(dielectrons, name)

    d_opt = llx_mva_options["phi"]
    phis = rdbuilder_thor.make_rd_detached_phis(**d_opt)
    phis = filter_dileptons(phis, name)

    d_opt = llx_mva_options["bs"]
    d_opt["dileptons"] = dielectrons
    d_opt["hadrons"] = phis
    d_opt["pvs"] = pvs
    bs = b_to_xll_builders.make_rd_BToXll(**d_opt)
    b, bdt_moni = add_mva(bs, pvs, "bsllphi", name, mva_wp)

    d_iso = {"B": (bs, 0.25)}
    l_iso = get_isolations(d_iso)

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [dielectrons, phis, bdt_moni, b],
        extra_outputs=l_iso,
        prescale=prescale,
        persistreco=True,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


# ----------------------------
