###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
PID-exclusive B -> XX gamma Hlt2 lines with converted gamma (LL or DD)
- B0 -> K pi gamma
- B_s0 -> K K gamma
- B0 -> pi pi gamma
- L_b0 -> p K gamma

author: Fionn Bishop
date: 05.02.24

"""

import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.algorithms_thor import ParticleFilter
from RecoConf.rdbuilder_thor import (
    make_rd_detached_kstar0s,
    make_rd_detached_phis,
    make_rd_detached_rho0,
)
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.rd.builders.b_to_xgamma_exclusive_builders import (
    make_b2xgamma_excl,
    make_rd_detached_pK,
)
from Hlt2Conf.lines.rd.builders.rad_incl_builder import make_gamma_eeDD, make_gamma_eeLL
from Hlt2Conf.lines.rd.builders.rd_prefilters import (
    _RD_MONITORING_VARIABLES,
    RD_PERSIST_CALO_CLUSTERS,
    RD_PERSIST_CALO_DIGITS,
    rd_prefilter,
)

all_lines = {}


def make_kstar0s_with_tighter_mass():
    return ParticleFilter(
        make_rd_detached_kstar0s(),
        F.FILTER(F.require_all(F.MASS > 767.0 * MeV, F.MASS < 1825.0 * MeV)),
        name="rd_detached_kstar0s_mass_btoxgamma_{hash}",
    )


def make_rho0s_with_tighter_mass():
    return ParticleFilter(
        make_rd_detached_rho0(),
        F.FILTER(F.require_all(F.MASS > 500.0 * MeV, F.MASS < 1850 * MeV)),
        name="rd_detached_rho0s_mass_btoxgamma_{hash}",
    )


@register_line_builder(all_lines)
def bd_to_kpigamma_gammatoeeLL_line(
    name="Hlt2RD_BdToKpPimGamma_GammaToEELL", persistreco=False, prescale=1.0
):
    pvs = make_pvs()
    kpi = make_kstar0s_with_tighter_mass()

    photons = make_gamma_eeLL(pvs, max_pid_e_min=2)

    b = make_b2xgamma_excl(
        intermediate=kpi,
        photons=photons,
        pvs=pvs,
        descriptor="[B0 -> K*(892)0 gamma]cc",
        dira_min=0.9995,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=25.0,
        vchi2pdof_max=10.0,
        name="rd_BdToKpiGamma_GammaToEELL_Combiner_{hash}",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kpi, b],
        prescale=prescale,
        persistreco=persistreco,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bs_to_kkgamma_gammatoeeLL_line(
    name="Hlt2RD_BsToKpKmGamma_GammaToEELL", persistreco=False, prescale=1.0
):
    pvs = make_pvs()
    kk = ParticleFilter(
        make_rd_detached_phis(
            name="rd_detached_phis_{hash}",
            am_max=2000.0 * MeV,
            k_pid=(F.PID_K > 2.0),
            k_ipchi2_min=9.0,
            k_p_min=2.0 * GeV,
            k_pt_min=250.0 * MeV,
            adocachi2cut=10.0,
            vchi2pdof_max=10.0,
            phi_pt_min=800.0 * MeV,
        ),
        F.FILTER(F.require_all(F.MASS < 1850.0 * MeV)),
        name="rd_detached_phis_tightermass_{hash}",
    )

    photons = make_gamma_eeLL(pvs, max_pid_e_min=2)

    b = make_b2xgamma_excl(
        intermediate=kk,
        photons=photons,
        pvs=pvs,
        descriptor="[B_s0 -> phi(1020) gamma]cc",
        dira_min=0.9995,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=25.0,
        vchi2pdof_max=10.0,
        name="rd_BsToKKGamma_GammaToEELL_Combiner_{hash}",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kk, b],
        prescale=prescale,
        persistreco=persistreco,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bd_to_pipigamma_gammatoeeLL_line(
    name="Hlt2RD_BdToPipPimGamma_GammaToEELL", persistreco=False, prescale=1.0
):
    pvs = make_pvs()
    pipi = make_rho0s_with_tighter_mass()
    photons = make_gamma_eeLL(pvs, max_pid_e_min=2)

    b = make_b2xgamma_excl(
        intermediate=pipi,
        photons=photons,
        pvs=pvs,
        descriptor="[B0 -> rho(770)0 gamma]cc",
        dira_min=0.9995,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=25.0,
        vchi2pdof_max=10.0,
        name="rd_BdTopipiGamma_GammaToEELL_Combiner_{hash}",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [pipi, b],
        prescale=prescale,
        persistreco=persistreco,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lb_to_pkgamma_gammatoeeLL_line(
    name="Hlt2RD_LbToPpKmGamma_GammaToEELL", persistreco=False, prescale=1.0
):
    pvs = make_pvs()
    pk = ParticleFilter(
        make_rd_detached_pK(
            am_min=1300.0 * MeV,
            am_max=2800.0 * MeV,
            p_pid=F.require_all(F.PID_P > 2, (F.PID_P - F.PID_K > 2.0)),
            k_pid=(F.PID_K > 2.0),
            p_ipchi2_min=9.0,
            k_ipchi2_min=9.0,
            k_p_min=2.0 * GeV,
            p_p_min=2.0 * GeV,
            k_pt_min=250.0 * MeV,
            p_pt_min=250.0 * MeV,
            adocachi2cut=10.0,
            vchi2pdof_max=10.0,
            pK_pt_min=800.0 * MeV,
        ),
        F.FILTER(F.require_all(F.MASS > 1375.0 * MeV, F.MASS < 2625.0 * MeV)),
        name="rd_detached_pK_tightermass_{hash}",
    )

    photons = make_gamma_eeLL(pvs, max_pid_e_min=2)

    b = make_b2xgamma_excl(
        intermediate=pk,
        photons=photons,
        pvs=pvs,
        descriptor="[Lambda_b0 -> Lambda(1520)0 gamma]cc",
        dira_min=0.9995,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=25.0,
        vchi2pdof_max=10.0,
        name="rd_LbTopKGamma_GammaToEELL_Combiner_{hash}",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [pk, b],
        prescale=prescale,
        persistreco=persistreco,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
    )


@register_line_builder(all_lines)
def bd_to_kpigamma_gammatoeeDD_line(
    name="Hlt2RD_BdToKpPimGamma_GammaToEEDD", persistreco=False, prescale=1.0
):
    pvs = make_pvs()
    kpi = make_kstar0s_with_tighter_mass()

    photons = make_gamma_eeDD(pvs, max_pid_e_min=2)

    b = make_b2xgamma_excl(
        intermediate=kpi,
        photons=photons,
        pvs=pvs,
        descriptor="[B0 -> K*(892)0 gamma]cc",
        dira_min=0.9995,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=25.0,
        vchi2pdof_max=10.0,
        name="rd_BdToKpiGamma_GammaToEEDD_Combiner_{hash}",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kpi, b],
        prescale=prescale,
        persistreco=persistreco,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bs_to_kkgamma_gammatoeeDD_line(
    name="Hlt2RD_BsToKpKmGamma_GammaToEEDD", persistreco=False, prescale=1.0
):
    pvs = make_pvs()
    kk = ParticleFilter(
        make_rd_detached_phis(
            name="rd_detached_phis_{hash}",
            am_max=2000.0 * MeV,
            k_pid=(F.PID_K > 2.0),
            k_ipchi2_min=9.0,
            k_p_min=2.0 * GeV,
            k_pt_min=250.0 * MeV,
            adocachi2cut=10.0,
            vchi2pdof_max=10.0,
            phi_pt_min=800.0 * MeV,
        ),
        F.FILTER(F.require_all(F.MASS < 1850.0 * MeV)),
        name="rd_detached_phis_tightermass_{hash}",
    )

    photons = make_gamma_eeDD(pvs, max_pid_e_min=2)

    b = make_b2xgamma_excl(
        intermediate=kk,
        photons=photons,
        pvs=pvs,
        descriptor="[B_s0 -> phi(1020) gamma]cc",
        dira_min=0.9995,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=25.0,
        vchi2pdof_max=10.0,
        name="rd_BsToKKGamma_GammaToEEDD_Combiner_{hash}",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [kk, b],
        prescale=prescale,
        persistreco=persistreco,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def bd_to_pipigamma_gammatoeeDD_line(
    name="Hlt2RD_BdToPipPimGamma_GammaToEEDD", persistreco=False, prescale=1.0
):
    pvs = make_pvs()
    pipi = make_rho0s_with_tighter_mass()

    photons = make_gamma_eeDD(pvs, max_pid_e_min=2)

    b = make_b2xgamma_excl(
        intermediate=pipi,
        photons=photons,
        pvs=pvs,
        descriptor="[B0 -> rho(770)0 gamma]cc",
        dira_min=0.9995,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=25.0,
        vchi2pdof_max=10.0,
        name="rd_BdTopipiGamma_GammaToEEDD_Combiner_{hash}",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [pipi, b],
        prescale=prescale,
        persistreco=persistreco,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
    )


@register_line_builder(all_lines)
def lb_to_pkgamma_gammatoeeDD_line(
    name="Hlt2RD_LbToPpKmGamma_GammaToEEDD", persistreco=False, prescale=1.0
):
    pvs = make_pvs()
    pk = ParticleFilter(
        make_rd_detached_pK(
            am_min=1300.0 * MeV,
            am_max=2800.0 * MeV,
            p_pid=F.require_all(F.PID_P > 2, (F.PID_P - F.PID_K > 2.0)),
            k_pid=(F.PID_K > 2.0),
            p_ipchi2_min=9.0,
            k_ipchi2_min=9.0,
            k_p_min=2.0 * GeV,
            p_p_min=2.0 * GeV,
            k_pt_min=250.0 * MeV,
            p_pt_min=250.0 * MeV,
            adocachi2cut=10.0,
            vchi2pdof_max=10.0,
            pK_pt_min=800.0 * MeV,
        ),
        F.FILTER(F.require_all(F.MASS > 1375.0 * MeV, F.MASS < 2625.0 * MeV)),
        name="rd_detached_pK_tightermass_{hash}",
    )

    photons = make_gamma_eeDD(pvs, max_pid_e_min=2)

    b = make_b2xgamma_excl(
        intermediate=pk,
        photons=photons,
        pvs=pvs,
        descriptor="[Lambda_b0 -> Lambda(1520)0 gamma]cc",
        dira_min=0.9995,
        sum_pt_min=4.0 * GeV,
        bpv_fdchi2_min=25.0,
        vchi2pdof_max=10.0,
        name="rd_LbTopKGamma_GammaToEEDD_Combiner_{hash}",
    )

    return Hlt2Line(
        name=name,
        algs=rd_prefilter() + [pk, b],
        prescale=prescale,
        persistreco=persistreco,
        calo_digits=RD_PERSIST_CALO_DIGITS,
        calo_clusters=RD_PERSIST_CALO_CLUSTERS,
        monitoring_variables=_RD_MONITORING_VARIABLES,
        tagging_particles=True,
    )
