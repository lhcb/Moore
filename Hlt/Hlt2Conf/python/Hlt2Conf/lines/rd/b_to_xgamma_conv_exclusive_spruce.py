###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""

PID-exclusive B -> XX gamma Hlt2 lines with converted gamma
- B0 -> K pi gamma
- B_s0 -> K K gamma
- B0 -> pi pi gamma
- L_b0 -> p K gamma

author: Fionn Bishop
date: 05.02.24

- L_b0 -> L gamma
- B+ -> K*+ gamma

author: Fionn Bishop
date: 17.10.24

"""

import Functors as F
from GaudiKernel.SystemOfUnits import MeV
from Moore.config import register_line_builder
from Moore.lines import SpruceLine
from RecoConf.algorithms_thor import ParticleFilter
from RecoConf.rdbuilder_thor import (
    make_rd_detached_kstar0s,
    make_rd_detached_phis,
    make_rd_detached_rho0,
    make_rd_kstarp_DD,
    make_rd_kstarp_LL,
)
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.rd.builders.b_to_xgamma_exclusive_builders import (
    make_b2xgamma_excl,
    make_rd_detached_pK,
)
from Hlt2Conf.lines.rd.builders.baryonic_radiative_builders import (
    filter_lambdasDD_for_lb_to_lg,
    filter_lambdasLL_for_lb_to_lg,
    make_rd_lb_to_lgamma_DD,
    make_rd_lb_to_lgamma_LL,
)
from Hlt2Conf.lines.rd.builders.rad_incl_builder import make_gamma_eeDD, make_gamma_eeLL
from Hlt2Conf.lines.rd.builders.rd_prefilters import rd_prefilter

sprucing_lines = {}


def make_kstar0s_with_tighter_mass():
    return ParticleFilter(
        make_rd_detached_kstar0s(),
        F.FILTER(F.require_all(F.MASS > 767.0 * MeV, F.MASS < 1825.0 * MeV)),
        name="rd_detached_kstar0s_mass_btoxgamma_{hash}",
    )


def make_rho0s_with_tighter_mass():
    return ParticleFilter(
        make_rd_detached_rho0(),
        F.FILTER(F.require_all(F.MASS > 500.0 * MeV, F.MASS < 1850 * MeV)),
        name="rd_detached_rho0s_mass_btoxgamma_{hash}",
    )


def make_b2xgamma_conv_excl_spruce(
    intermediate,
    photons,
    pvs,
    descriptor,
    name,
):
    return make_b2xgamma_excl(
        intermediate=intermediate,
        photons=photons,
        pvs=pvs,
        descriptor=descriptor,
        dira_min=0.0,
        name=name,
        bpv_fdchi2_min=16.0,
        vchi2pdof_max=25.0,
    )


@register_line_builder(sprucing_lines)
def spruce_bd_to_kpigamma_gammatoeeLL_line(
    name="SpruceRD_BdToKpPimGamma_GammaToEELL", persistreco=False, prescale=1.0
):
    pvs = make_pvs()
    kpi = make_kstar0s_with_tighter_mass()

    photons = make_gamma_eeLL(pvs)

    b = make_b2xgamma_conv_excl_spruce(
        intermediate=kpi,
        photons=photons,
        pvs=pvs,
        descriptor="[B0 -> K*(892)0 gamma]cc",
        name="rd_BdToKpiGamma_GammaToEELL_Combiner_{hash}",
    )

    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [kpi, b],
        prescale=prescale,
        persistreco=persistreco,
        hlt2_filter_code=["Hlt2RD_BToHHGamma_GammaToEELL_Incl_FullDecision"],
    )


@register_line_builder(sprucing_lines)
def spruce_bs_to_kkgamma_gammatoeeLL_line(
    name="SpruceRD_BsToKpKmGamma_GammaToEELL", persistreco=False, prescale=1.0
):
    pvs = make_pvs()
    kk = ParticleFilter(
        make_rd_detached_phis(
            name="rd_detached_phis_{hash}",
            am_max=2000.0 * MeV,
            vchi2pdof_max=25.0,
            k_pid=(F.PID_K > -10.0),
        ),
        F.FILTER(F.require_all(F.MASS < 1850.0 * MeV)),
        name="rd_detached_phis_tightermass_{hash}",
    )

    photons = make_gamma_eeLL(pvs)

    b = make_b2xgamma_conv_excl_spruce(
        intermediate=kk,
        photons=photons,
        pvs=pvs,
        descriptor="[B_s0 -> phi(1020) gamma]cc",
        name="rd_BsToKKGamma_GammaToEELL_Combiner_{hash}",
    )

    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [kk, b],
        prescale=prescale,
        persistreco=persistreco,
        hlt2_filter_code=["Hlt2RD_BToHHGamma_GammaToEELL_Incl_FullDecision"],
    )


@register_line_builder(sprucing_lines)
def spruce_bd_to_pipigamma_gammatoeeLL_line(
    name="SpruceRD_BdToPipPimGamma_GammaToEELL", persistreco=False, prescale=1.0
):
    pvs = make_pvs()
    pipi = make_rho0s_with_tighter_mass()

    photons = make_gamma_eeLL(pvs)

    b = make_b2xgamma_conv_excl_spruce(
        intermediate=pipi,
        photons=photons,
        pvs=pvs,
        descriptor="[B0 -> rho(770)0 gamma]cc",
        name="rd_BdTopipiGamma_GammaToEELL_Combiner_{hash}",
    )

    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [pipi, b],
        prescale=prescale,
        persistreco=persistreco,
        hlt2_filter_code=["Hlt2RD_BToHHGamma_GammaToEELL_Incl_FullDecision"],
    )


@register_line_builder(sprucing_lines)
def spruce_lb_to_pkgamma_gammatoeeLL_line(
    name="SpruceRD_LbToPpKmGamma_GammaToEELL", persistreco=False, prescale=1.0
):
    pvs = make_pvs()
    pk = ParticleFilter(
        make_rd_detached_pK(
            am_min=1300.0 * MeV,
            am_max=2800.0 * MeV,
            p_pid=(F.PID_P - F.PID_K > -10.0),
            k_pid=(F.PID_K > -10.0),
            vchi2pdof_max=25.0,
        ),
        F.FILTER(F.require_all(F.MASS > 1375.0 * MeV, F.MASS < 2625.0 * MeV)),
        name="rd_detached_pK_tightermass_{hash}",
    )

    photons = make_gamma_eeLL(pvs)

    b = make_b2xgamma_conv_excl_spruce(
        intermediate=pk,
        photons=photons,
        pvs=pvs,
        descriptor="[Lambda_b0 -> Lambda(1520)0 gamma]cc",
        name="rd_LbTopKGamma_GammaToEELL_Combiner_{hash}",
    )

    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [pk, b],
        prescale=prescale,
        persistreco=persistreco,
        hlt2_filter_code=["Hlt2RD_BToHHGamma_GammaToEELL_Incl_FullDecision"],
    )


@register_line_builder(sprucing_lines)
def spruce_bd_to_kpigamma_gammatoeeDD_line(
    name="SpruceRD_BdToKpPimGamma_GammaToEEDD", persistreco=False, prescale=1.0
):
    pvs = make_pvs()
    kpi = make_kstar0s_with_tighter_mass()

    photons = make_gamma_eeDD(pvs)

    b = make_b2xgamma_conv_excl_spruce(
        intermediate=kpi,
        photons=photons,
        pvs=pvs,
        descriptor="[B0 -> K*(892)0 gamma]cc",
        name="rd_BdToKpiGamma_GammaToEEDD_Combiner_{hash}",
    )

    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [kpi, b],
        prescale=prescale,
        persistreco=persistreco,
        hlt2_filter_code=["Hlt2RD_BToHHGamma_GammaToEEDD_Incl_FullDecision"],
    )


@register_line_builder(sprucing_lines)
def spruce_bs_to_kkgamma_gammatoeeDD_line(
    name="SpruceRD_BsToKpKmGamma_GammaToEEDD", persistreco=False, prescale=1.0
):
    pvs = make_pvs()
    kk = ParticleFilter(
        make_rd_detached_phis(
            name="rd_detached_phis_{hash}",
            am_max=2000.0 * MeV,
            vchi2pdof_max=25.0,
            k_pid=(F.PID_K > -10.0),
        ),
        F.FILTER(F.require_all(F.MASS < 1850.0 * MeV)),
        name="rd_detached_phis_tightermass_{hash}",
    )

    photons = make_gamma_eeDD(pvs)

    b = make_b2xgamma_conv_excl_spruce(
        intermediate=kk,
        photons=photons,
        pvs=pvs,
        descriptor="[B_s0 -> phi(1020) gamma]cc",
        name="rd_BsToKKGamma_GammaToEEDD_Combiner_{hash}",
    )

    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [kk, b],
        prescale=prescale,
        persistreco=persistreco,
        hlt2_filter_code=["Hlt2RD_BToHHGamma_GammaToEEDD_Incl_FullDecision"],
    )


@register_line_builder(sprucing_lines)
def spruce_bd_to_pipigamma_gammatoeeDD_line(
    name="SpruceRD_BdToPipPimGamma_GammaToEEDD", persistreco=False, prescale=1.0
):
    pvs = make_pvs()
    pipi = make_rho0s_with_tighter_mass()

    photons = make_gamma_eeDD(pvs)

    b = make_b2xgamma_conv_excl_spruce(
        intermediate=pipi,
        photons=photons,
        pvs=pvs,
        descriptor="[B0 -> rho(770)0 gamma]cc",
        name="rd_BdTopipiGamma_GammaToEEDD_Combiner_{hash}",
    )

    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [pipi, b],
        prescale=prescale,
        persistreco=persistreco,
        hlt2_filter_code=["Hlt2RD_BToHHGamma_GammaToEEDD_Incl_FullDecision"],
    )


@register_line_builder(sprucing_lines)
def spruce_lb_to_pkgamma_gammatoeeDD_line(
    name="SpruceRD_LbToPpKmGamma_GammaToEEDD", persistreco=False, prescale=1.0
):
    pvs = make_pvs()
    pk = ParticleFilter(
        make_rd_detached_pK(
            am_min=1300.0 * MeV,
            am_max=2800.0 * MeV,
            p_pid=(F.PID_P - F.PID_K > -10.0),
            k_pid=(F.PID_K > -10.0),
            vchi2pdof_max=25.0,
        ),
        F.FILTER(F.require_all(F.MASS > 1375.0 * MeV, F.MASS < 2625.0 * MeV)),
        name="rd_detached_pK_tightermass_{hash}",
    )

    photons = make_gamma_eeDD(pvs)

    b = make_b2xgamma_conv_excl_spruce(
        intermediate=pk,
        photons=photons,
        pvs=pvs,
        descriptor="[Lambda_b0 -> Lambda(1520)0 gamma]cc",
        name="rd_LbTopKGamma_GammaToEEDD_Combiner_{hash}",
    )

    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [pk, b],
        prescale=prescale,
        persistreco=persistreco,
        hlt2_filter_code=["Hlt2RD_BToHHGamma_GammaToEEDD_Incl_FullDecision"],
    )


@register_line_builder(sprucing_lines)
def lb_to_lgamma_ll_gammatoeedd_line(
    name="SpruceRD_LbToLambdaGamma_LL_GammaToEEDD",
    prescale=1.0,
    persistreco=False,
    prefilter=rd_prefilter,
):
    pvs = make_pvs()

    lambda_ll = filter_lambdasLL_for_lb_to_lg(pvs=pvs)
    photons = make_gamma_eeDD(pvs)

    lb_ll = make_rd_lb_to_lgamma_LL(Lambda=lambda_ll, gamma=photons, pvs=pvs)

    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [lb_ll],
        prescale=prescale,
        persistreco=persistreco,
        hlt2_filter_code=[
            "Hlt2RD_BToHHGamma_GammaToEEDD_Incl_FullDecision",
            "Hlt2RD_BToHHHGamma_GammaToEEDD_Incl_FullDecision",
            "Hlt2Topo2BodyDecision",
            "Hlt2Topo3BodyDecision",
            "Hlt2_InclDetDiElectronDecision",
            "Hlt2_InclDetDiElectron_3BodyDecision",
            "Hlt2_InclDetDiElectron_4BodyDecision",
            "Hlt2_InclDetDiElectron_4Body_PionSSDecision",
            "Hlt2_InclDetDiElectron_neutralDecision",
        ],
    )


@register_line_builder(sprucing_lines)
def lb_to_lgamma_ll_gammatoeell_line(
    name="SpruceRD_LbToLambdaGamma_LL_GammaToEELL",
    prescale=1.0,
    persistreco=False,
    prefilter=rd_prefilter,
):
    pvs = make_pvs()

    lambda_ll = filter_lambdasLL_for_lb_to_lg(pvs=pvs)
    photons = make_gamma_eeLL(pvs)

    lb_ll = make_rd_lb_to_lgamma_LL(Lambda=lambda_ll, gamma=photons, pvs=pvs)

    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [lb_ll],
        prescale=prescale,
        persistreco=persistreco,
        hlt2_filter_code=[
            "Hlt2RD_BToHHGamma_GammaToEELL_Incl_FullDecision",
            "Hlt2RD_BToHHHGamma_GammaToEELL_Incl_FullDecision",
            "Hlt2Topo2BodyDecision",
            "Hlt2Topo3BodyDecision",
            "Hlt2_InclDetDiElectronDecision",
            "Hlt2_InclDetDiElectron_3BodyDecision",
            "Hlt2_InclDetDiElectron_4BodyDecision",
            "Hlt2_InclDetDiElectron_4Body_PionSSDecision",
            "Hlt2_InclDetDiElectron_neutralDecision",
        ],
    )


@register_line_builder(sprucing_lines)
def lb_to_lgamma_dd_gammatoeedd_line(
    name="SpruceRD_LbToLambdaGamma_DD_GammaToEEDD",
    prescale=1.0,
    persistreco=False,
    prefilter=rd_prefilter,
):
    pvs = make_pvs()

    lambda_dd = filter_lambdasDD_for_lb_to_lg(pvs=pvs)
    photons = make_gamma_eeDD(pvs)

    lb_dd = make_rd_lb_to_lgamma_DD(Lambda=lambda_dd, gamma=photons, pvs=pvs)

    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [lb_dd],
        prescale=prescale,
        persistreco=persistreco,
        hlt2_filter_code=[
            "Hlt2RD_BToHHGamma_GammaToEEDD_Incl_FullDecision",
            "Hlt2RD_BToHHHGamma_GammaToEEDD_Incl_FullDecision",
            "Hlt2Topo2BodyDecision",
            "Hlt2Topo3BodyDecision",
            "Hlt2_InclDetDiElectronDecision",
            "Hlt2_InclDetDiElectron_3BodyDecision",
            "Hlt2_InclDetDiElectron_4BodyDecision",
            "Hlt2_InclDetDiElectron_4Body_PionSSDecision",
            "Hlt2_InclDetDiElectron_neutralDecision",
        ],
    )


@register_line_builder(sprucing_lines)
def lb_to_lgamma_dd_gammatoeell_line(
    name="SpruceRD_LbToLambdaGamma_DD_GammaToEELL",
    prescale=1.0,
    persistreco=False,
    prefilter=rd_prefilter,
):
    pvs = make_pvs()

    lambda_dd = filter_lambdasDD_for_lb_to_lg(pvs=pvs)
    photons = make_gamma_eeLL(pvs)

    lb_dd = make_rd_lb_to_lgamma_DD(Lambda=lambda_dd, gamma=photons, pvs=pvs)

    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [lb_dd],
        prescale=prescale,
        persistreco=persistreco,
        hlt2_filter_code=[
            "Hlt2RD_BToHHGamma_GammaToEELL_Incl_FullDecision",
            "Hlt2RD_BToHHHGamma_GammaToEELL_Incl_FullDecision",
            "Hlt2Topo2BodyDecision",
            "Hlt2Topo3BodyDecision",
            "Hlt2_InclDetDiElectronDecision",
            "Hlt2_InclDetDiElectron_3BodyDecision",
            "Hlt2_InclDetDiElectron_4BodyDecision",
            "Hlt2_InclDetDiElectron_4Body_PionSSDecision",
            "Hlt2_InclDetDiElectron_neutralDecision",
        ],
    )


@register_line_builder(sprucing_lines)
def spruce_bu_to_kstpgamma_kstptoksLLpi_gammatoeeLL_line(
    name="SpruceRD_BuToKstpGamma_KstpToKSLLPi_GammaToEELL",
    persistreco=False,
    prescale=1.0,
):
    pvs = make_pvs()
    kstp = make_rd_kstarp_LL()

    photons = make_gamma_eeLL(pvs)

    b = make_b2xgamma_conv_excl_spruce(
        intermediate=kstp,
        photons=photons,
        pvs=pvs,
        descriptor="[B+ -> K*(892)+ gamma]cc",
        name="rd_BuToKstpGamma_KstpToKSLLPi_GammaToEELL_Combiner_{hash}",
    )

    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [kstp, b],
        prescale=prescale,
        persistreco=persistreco,
        hlt2_filter_code=[
            "Hlt2RD_BToHHGamma_GammaToEELL_Incl_FullDecision",
            "Hlt2RD_BToHHHGamma_GammaToEELL_Incl_FullDecision",
            "Hlt2Topo2BodyDecision",
            "Hlt2Topo3BodyDecision",
            "Hlt2_InclDetDiElectronDecision",
            "Hlt2_InclDetDiElectron_3BodyDecision",
            "Hlt2_InclDetDiElectron_4BodyDecision",
            "Hlt2_InclDetDiElectron_4Body_PionSSDecision",
            "Hlt2_InclDetDiElectron_neutralDecision",
        ],
    )


@register_line_builder(sprucing_lines)
def spruce_bu_to_kstpgamma_kstptoksLLpi_gammatoeeDD_line(
    name="SpruceRD_BuToKstpGamma_KstpToKSLLPi_GammaToEEDD",
    persistreco=False,
    prescale=1.0,
):
    pvs = make_pvs()
    kstp = make_rd_kstarp_LL()

    photons = make_gamma_eeDD(pvs)

    b = make_b2xgamma_conv_excl_spruce(
        intermediate=kstp,
        photons=photons,
        pvs=pvs,
        descriptor="[B+ -> K*(892)+ gamma]cc",
        name="rd_BuToKstpGamma_KstpToKSLLPi_GammaToEEDD_Combiner_{hash}",
    )

    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [kstp, b],
        prescale=prescale,
        persistreco=persistreco,
        hlt2_filter_code=[
            "Hlt2RD_BToHHGamma_GammaToEEDD_Incl_FullDecision",
            "Hlt2RD_BToHHHGamma_GammaToEEDD_Incl_FullDecision",
            "Hlt2Topo2BodyDecision",
            "Hlt2Topo3BodyDecision",
            "Hlt2_InclDetDiElectronDecision",
            "Hlt2_InclDetDiElectron_3BodyDecision",
            "Hlt2_InclDetDiElectron_4BodyDecision",
            "Hlt2_InclDetDiElectron_4Body_PionSSDecision",
            "Hlt2_InclDetDiElectron_neutralDecision",
        ],
    )


@register_line_builder(sprucing_lines)
def spruce_bu_to_kstpgamma_kstptoksDDpi_gammatoeeLL_line(
    name="SpruceRD_BuToKstpGamma_KstpToKSDDPi_GammaToEELL",
    persistreco=False,
    prescale=1.0,
):
    pvs = make_pvs()
    kstp = make_rd_kstarp_DD()

    photons = make_gamma_eeLL(pvs)

    b = make_b2xgamma_conv_excl_spruce(
        intermediate=kstp,
        photons=photons,
        pvs=pvs,
        descriptor="[B+ -> K*(892)+ gamma]cc",
        name="rd_BuToKstpGamma_KstpToKSDDPi_GammaToEELL_Combiner_{hash}",
    )

    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [kstp, b],
        prescale=prescale,
        persistreco=persistreco,
        hlt2_filter_code=[
            "Hlt2RD_BToHHGamma_GammaToEELL_Incl_FullDecision",
            "Hlt2RD_BToHHHGamma_GammaToEELL_Incl_FullDecision",
            "Hlt2Topo2BodyDecision",
            "Hlt2Topo3BodyDecision",
            "Hlt2_InclDetDiElectronDecision",
            "Hlt2_InclDetDiElectron_3BodyDecision",
            "Hlt2_InclDetDiElectron_4BodyDecision",
            "Hlt2_InclDetDiElectron_4Body_PionSSDecision",
            "Hlt2_InclDetDiElectron_neutralDecision",
        ],
    )


@register_line_builder(sprucing_lines)
def spruce_bu_to_kstpgamma_kstptoksDDpi_gammatoeeDD_line(
    name="SpruceRD_BuToKstpGamma_KstpToKSDDPi_GammaToEEDD",
    persistreco=False,
    prescale=1.0,
):
    pvs = make_pvs()
    kstp = make_rd_kstarp_DD()

    photons = make_gamma_eeDD(pvs)

    b = make_b2xgamma_conv_excl_spruce(
        intermediate=kstp,
        photons=photons,
        pvs=pvs,
        descriptor="[B+ -> K*(892)+ gamma]cc",
        name="rd_BuToKstpGamma_KstpToKSDDPi_GammaToEEDD_Combiner_{hash}",
    )

    return SpruceLine(
        name=name,
        algs=rd_prefilter() + [kstp, b],
        prescale=prescale,
        persistreco=persistreco,
        hlt2_filter_code=[
            "Hlt2RD_BToHHGamma_GammaToEEDD_Incl_FullDecision",
            "Hlt2RD_BToHHHGamma_GammaToEEDD_Incl_FullDecision",
            "Hlt2Topo2BodyDecision",
            "Hlt2Topo3BodyDecision",
            "Hlt2_InclDetDiElectronDecision",
            "Hlt2_InclDetDiElectron_3BodyDecision",
            "Hlt2_InclDetDiElectron_4BodyDecision",
            "Hlt2_InclDetDiElectron_4Body_PionSSDecision",
            "Hlt2_InclDetDiElectron_neutralDecision",
        ],
    )
