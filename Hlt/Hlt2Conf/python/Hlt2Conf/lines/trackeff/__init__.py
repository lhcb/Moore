# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines all the Tracking efficiency (and related) HLT2 lines
"""

from . import (
    DiMuonTrackEfficiency,
    KSVeloLong,
    SMOG2_Jpsi_trackeff,
    SMOG2_Ks_trackeff,
    Velo2Long_B2JpsiK_ElectronProbe,
    Velo2Long_B2JpsiK_MuonProbe,
    Velo2Long_Ds2PhiPi_KaonProbe,
    ZTrackEfficiency,
    ZUpsilonFullEvent,
)

turcal_lines = {}
turcal_lines.update(DiMuonTrackEfficiency.all_lines)
turcal_lines.update(Velo2Long_B2JpsiK_MuonProbe.all_lines)
turcal_lines.update(Velo2Long_B2JpsiK_ElectronProbe.all_lines)
turcal_lines.update(KSVeloLong.turcal_lines)
turcal_lines.update(ZTrackEfficiency.all_lines)
turcal_lines.update(SMOG2_Jpsi_trackeff.all_lines)
turcal_lines.update(SMOG2_Ks_trackeff.all_lines)

turbo_lines = {}
turbo_lines.update(KSVeloLong.turbo_lines)
turbo_lines.update(Velo2Long_Ds2PhiPi_KaonProbe.all_lines)
turbo_lines.update(ZUpsilonFullEvent.turbo_lines)

# provide "all_lines" for correct registration by the overall HLT2 lines module
all_lines = {}
all_lines.update(turcal_lines)
all_lines.update(turbo_lines)
