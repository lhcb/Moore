##############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
HLT2 line using Ds -> Phi Pi (phi -> KK) decays, where one of the kaons
is a VELO track (the probe).

Contact: Laurent Dufour <laurent.dufour@cern.ch>
"""

import Functors as F
from Functors.math import abs as functor_abs
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf.Algorithms import (
    FlattenDecayTree,
    FunctionalChargedProtoParticleMaker,
    LHCbIDOverlapRelationTable,
    SelectionFromWeightedRelationTable,
    Velo2Long_Ds2PhiPi_TrackEffFilter,
)
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.event_filters import require_pvs
from RecoConf.hlt2_tracking import make_hlt2_tracks_without_UT
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction
from RecoConf.standard_particles import (
    _make_particles,
    get_all_track_selector,
    make_long_kaons,
    make_long_pions,
    make_up_kaons,
)

from Hlt2Conf.hlt1_tistos import hlt1_tos_on_any_filter

# a TOS on the TAG combination is required
_HLT1_TOS_LINES = ["Hlt1TrackMVADecision", "Hlt1TwoTrackMVADecision"]

all_lines = {}


def filter_tag_kaons(
    particles,
    pvs,
    min_p=6.5 * GeV,
    min_pt=0.5 * GeV,
    min_ipchi2=12.0,
    min_pid=6.0,
    min_eta=2.0,
    max_eta=4.9,
):
    code = F.require_all(
        F.PT > min_pt,
        F.MINIPCHI2(pvs) > min_ipchi2,
        F.PID_K > min_pid,
        F.ETA > min_eta,
        F.ETA < max_eta,
        F.P > min_p,
    )

    return ParticleFilter(
        particles,
        F.FILTER(code),
    )


def filter_tag_pions(
    particles,
    pvs,
    min_p=6.5 * GeV,
    min_pt=0.45 * GeV,
    min_ipchi2=12.0,
    max_pid_k=-1.0,
    min_eta=2.0,
    max_eta=4.9,
):
    code = F.require_all(
        F.PT > min_pt,
        F.MINIPCHI2(pvs) > min_ipchi2,
        F.PID_K < max_pid_k,
        F.ETA > min_eta,
        F.ETA < max_eta,
        F.P > min_p,
    )

    return ParticleFilter(
        particles,
        F.FILTER(code),
    )


def make_tag_combination(
    particles,
    pvs,
    min_Tag_mass=700.0 * MeV,
    max_Tag_mass=1650.0 * MeV,
    min_maxpt=1500.0 * MeV,
    min_maxipchi2=15,
    doca_max=0.13 * mm,
    min_fd_z=2.5 * mm,
    max_bpv_rho=5.0 * mm,
    max_corrected_mass=2.2 * GeV,
    max_vchi2dof=9,
):
    combination_code = F.require_all(
        F.MAXSDOCACUT(doca_max),
        in_range(min_Tag_mass, F.MASS, max_Tag_mass),
        ((F.CHILD(1, F.PT) > min_maxpt) | (F.CHILD(2, F.PT) > min_maxpt)),
        F.MAX(F.MINIPCHI2(pvs)) > min_maxipchi2,
    )

    vertex_code = F.require_all(
        F.MASS > min_Tag_mass,
        F.MASS < max_Tag_mass,
        F.CHI2DOF < max_vchi2dof,
        F.BPVCORRM(pvs) < max_corrected_mass,
        F.BPVVDRHO(pvs) < max_bpv_rho,
        F.BPVVDZ(pvs) > min_fd_z,
    )

    return ParticleCombiner(
        particles,
        DecayDescriptor="[phi(1020) -> K+ pi+]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def filter_hlt1(tag_particles):
    return hlt1_tos_on_any_filter(
        data=tag_particles, hlt1_trigger_lines=_HLT1_TOS_LINES
    )


def make_tag_and_probe_combination(
    particles,
    pvs,
    mass_max=2500 * MeV,
    doca_max=0.13 * mm,
    min_fd_z=3.0 * mm,
    min_end_vz=-300 * mm,  # kill SMOG events
    max_vchi2=10,
    vertex_compatibility=1 * mm,
):
    combination_code = F.require_all(F.MASS < mass_max, F.MAXSDOCACUT(doca_max))

    # some additional cuts
    vertex_code = F.require_all(
        F.CHI2DOF < max_vchi2,
        F.BPVVDZ(pvs) > min_fd_z,
        F.END_VZ > min_end_vz,
        functor_abs(F.END_VZ - F.CHILD(1, F.END_VZ)) < vertex_compatibility,
    )

    return ParticleCombiner(
        particles,
        DecayDescriptor="[D_s+ -> phi(1020) K+]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def make_velo_track_kaon_protos():
    my_velo_tracks = make_hlt2_tracks_without_UT(light_reco=True, use_pr_kf=True)[
        "Velo"
    ]["v1"]
    charged_protos = FunctionalChargedProtoParticleMaker(
        Inputs=[my_velo_tracks], Code=F.require_all(~F.TRACKISVELOBACKWARD)
    )

    return charged_protos.Output


def make_velo_track_kaon_particles():
    return _make_particles(
        species="kaon",
        make_protoparticles=make_velo_track_kaon_protos,
        get_track_selector=get_all_track_selector,
    )


def filter_probe_kaons(
    particles,
    pvs,
    min_ip=0.1 * mm,
    min_eta=2.0,
    max_eta=5.0,
):
    code = F.require_all(
        F.ETA > min_eta,
        F.ETA < max_eta,
        F.MINIP(pvs) > min_ip,
    )

    return ParticleFilter(
        particles, F.FILTER(code), name="VELOProbeFilter_for_kaons_{hash}"
    )


@register_line_builder(all_lines)
def trackeff_velo2long_ds2phipi(
    name="Hlt2TurboVelo2Long_Ds2PhiPi_KaonProbe", prescale=0.5
):
    pvs = make_pvs()

    tag_kaons = filter_tag_kaons(particles=make_long_kaons(), pvs=pvs)
    tag_pions = filter_tag_pions(particles=make_long_pions(), pvs=pvs)

    my_tag_combinations = make_tag_combination(
        particles=[tag_kaons, tag_pions], pvs=pvs
    )
    hlt1_filtered_tag_combination = filter_hlt1(my_tag_combinations)

    probe_kaons = filter_probe_kaons(particles=make_long_kaons(), pvs=pvs)

    ds = make_tag_and_probe_combination(
        particles=[my_tag_combinations, probe_kaons], pvs=pvs
    )

    TrackEffFilter = Velo2Long_Ds2PhiPi_TrackEffFilter(
        InputParticle=ds,
        InputPVs=pvs,
        PVConstrainedProbePMin=5000.0 * MeV,
        PVConstrainedProbePtMin=300.0 * MeV,
        PVConstrainedProbePtMax=15000.0 * MeV,
        PVConstrainedPhiMassMin=920.0 * MeV,
        PVConstrainedPhiMassMax=1140.0 * MeV,
        PVConstrainedDMassMin=1700.0 * MeV,
        PVConstrainedDMassMax=2150.0 * MeV,
        PhiConstrainedDMassMin=1800.0 * MeV,
        PhiConstrainedDMassMax=2200.0 * MeV,
        PVConstrainedDeltaMassMin=800.0 * MeV,
        PVConstrainedDeltaMassMax=1200.0 * MeV,
        ConstrainedIPMax=0.1 * mm,
        name="VELO2Long_Ds2PhiPi_Filter_{hash}",
    ).OutputParticles
    """ Selective persistence"""
    flat_tree = FlattenDecayTree(
        InputParticles=TrackEffFilter, name=f"FlattenDs_{name}"
    )
    Ds_flat_daughters = ParticleFilter(
        flat_tree.OutputParticles,
        F.FILTER(F.require_all(F.ISBASICPARTICLE, (F.TRACKISVELO @ F.TRACK))),
    )

    relation_table_match_by_lhcbid_long = LHCbIDOverlapRelationTable(
        MatchFrom=Ds_flat_daughters,
        MatchTo=make_long_kaons(),
        IncludeVP=True,
        IncludeFT=False,
        IncludeUT=False,
        MinMatchFraction=0.2,
    ).OutputRelations

    relation_table_match_by_lhcbid_upstream = LHCbIDOverlapRelationTable(
        MatchFrom=Ds_flat_daughters,
        MatchTo=make_up_kaons(),
        IncludeVP=True,
        IncludeFT=False,
        IncludeUT=False,
        MinMatchFraction=0.2,
    ).OutputRelations

    matched_long_pions = SelectionFromWeightedRelationTable(
        InputRelations=relation_table_match_by_lhcbid_long
    ).OutputLocation
    matched_upstream_pions = SelectionFromWeightedRelationTable(
        InputRelations=relation_table_match_by_lhcbid_upstream
    ).OutputLocation

    extra_outputs = [
        ("MatchedLongPions", matched_long_pions),
        ("MatchedUpstreamPions", matched_upstream_pions),
    ]

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction()
        + [require_pvs(pvs), hlt1_filtered_tag_combination, TrackEffFilter],
        prescale=prescale,
        extra_outputs=extra_outputs,
        persistreco=False,
        hlt1_filter_code="Hlt1.*Track.*MVADecision",
    )
