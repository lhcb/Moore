###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf.Algorithms import (
    FlattenDecayTree,
    KSLongVeloFilter,
    LHCbIDOverlapRelationTable,
    SelectionFromWeightedRelationTable,
)
from PyConf.tonic import configurable
from RecoConf.algorithms_thor import (
    ParticleCombiner,
    ParticleContainersMerger,
    ParticleFilter,
)
from RecoConf.event_filters import require_pvs
from RecoConf.reconstruction_objects import (
    make_pvs,
    upfront_reconstruction,
)
from RecoConf.standard_particles import make_long_pions, make_up_pions

from Hlt2Conf.hlt1_tistos import hlt1_tis_on_any_filter
from Hlt2Conf.probe_muons import make_velo_muons

turbo_lines = {}
turcal_lines = {}
monitoring_lines = {}

hlt1_tis_lines = [
    "Hlt1TrackMuonMVADecision",
    "Hlt1TrackMVADecision",
    "Hlt1TwoTrackMVADecision",
    "Hlt1OneMuonTrackLineDecision",
    "Hlt1DiMuonHighMassDecision",
    "Hlt1DiMuonNoIPDecision",
    "Hlt1PassthroughDecision",
    "Hlt1GECPassthroughDecision",
    "Hlt1D2KPiDecision",
    "Hlt1Dst2D0PiDecision",
    "Hlt1SingleHighPtMuonNoMuIDDecision",
    "Hlt1DetJpsiToMuMuPosTagLineDecision",
    "Hlt1DetJpsiToMuMuNegTagLineDecision",
    "Hlt1SingleHighPtMuonDecision",
    "Hlt1DisplacedDiMuonDecision",
    "Hlt1TrackElectronMVADecision",
    "Hlt1SingleHighPtElectronDecision",
    "Hlt1DisplacedDielectronDecision",
    "Hlt1SingleHighEtDecision",
]


@configurable
def filter_pions(particles, pvs=None, pt_min=0.4 * GeV):
    if pvs is not None:
        cut = F.require_all(
            F.P > 5000,
            F.PT > pt_min,
            F.ETA < 5.0,
            F.ETA > 1.5,
            F.PID_P < 5,
            F.MINIP(pvs) > 0.4 * mm,
        )
    else:
        cut = F.require_all(
            F.P > 5000, F.PT > pt_min, F.ETA < 5.0, F.ETA > 1.5, F.PID_P < 5
        )

    return ParticleFilter(particles, F.FILTER(cut))


@configurable
def filter_particles_velo(particles, pvs):
    cut = F.require_all(
        F.ETA < 4.7,
        F.ETA > 1.5,
        F.MINIP(pvs) > 0.5 * mm,
    )
    return ParticleFilter(particles, F.FILTER(cut))


@configurable
def filter_particles_tis_velo(particles, pvs):
    pre_filtered_particles = filter_particles_velo(particles, pvs)

    return hlt1_tis_on_any_filter(
        hlt1_trigger_lines=hlt1_tis_lines,
        data=pre_filtered_particles,
        name="Hlt1TISFilter_Velo2Long_{hash}",
        advanced_tistos_arguments={
            "TOSFracFT": 0.0,
            "TISFracUT": 0.0,
            "TOSFracVP": 1.0,
        },
    )


@configurable
def make_kshort_pi_muplus_specific_charge(
    pions,
    muons,
    decay_descriptor,
    pvs,
    comb_m_min=0 * MeV,
    comb_m_max=2650 * MeV,
    comb_maxdoca=0.15 * mm,
    name="KShortMuPiCombiner_{hash}",
):
    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.MAXDOCACUT(comb_maxdoca),
    )
    vertex_code = F.require_all(
        F.END_VRHO < 100 * mm,
        F.END_VZ > 18 * mm,
        F.END_VZ < 600 * mm,
        F.BPVFD(pvs) > 5 * mm,
        F.BPVVDRHO(pvs) > 3 * mm,
    )
    return ParticleCombiner(
        [pions, muons],
        name=name,
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


def make_kshort_pi_muplus(pions, muons, **kwargs):
    ks_particle_combinations = [
        make_kshort_pi_muplus_specific_charge(pions, muons, "KS0 -> pi+ mu+", **kwargs),
        make_kshort_pi_muplus_specific_charge(pions, muons, "KS0 -> pi+ mu-", **kwargs),
        make_kshort_pi_muplus_specific_charge(pions, muons, "KS0 -> pi- mu-", **kwargs),
        make_kshort_pi_muplus_specific_charge(pions, muons, "KS0 -> pi- mu+", **kwargs),
    ]

    return ParticleContainersMerger(
        ks_particle_combinations, name="KS_combinations_{hash}"
    )


def construct_hlt2_line(
    *,
    name,
    prescale,
    pt_min,
    pt_max,
    p_min=5000.0,
    filter_tis=False,
    apply_lambda_veto=False,
):
    pvs = make_pvs()
    pions = filter_pions(make_long_pions(), pvs, pt_min=0.5 * GeV)
    turbo_line = name.find("Hlt2Turbo") != -1

    if filter_tis:
        muonsVelo = filter_particles_tis_velo(make_velo_muons(), pvs)
    else:
        muonsVelo = filter_particles_velo(make_velo_muons(), pvs)

    kshorts = make_kshort_pi_muplus(
        pions, muonsVelo, pvs=pvs, comb_m_max=1500, comb_m_min=0
    )  # probe should be on index 1

    filtered_kshorts = KSLongVeloFilter(
        InputParticle=kshorts,
        InputPVs=pvs,
        PVConstrainedMassMin=365.0,
        PVConstrainedMassMax=625.0,
        PVConstrainedProbePtMin=pt_min,
        PVConstrainedProbePtMax=pt_max,
        PVConstrainedProbePMin=p_min,
        IPperpendicularMax=0.007,
        IPMax=0.4,
        ApplyLambdaVeto=apply_lambda_veto,
        name=f"TrackEffFilter_{name}",
    ).OutputParticles
    if turbo_line:
        """ Selective persistence"""
        kshort_flat_tree = FlattenDecayTree(
            InputParticles=filtered_kshorts, name=f"FlattenKS_{name}"
        )
        kshort_flat_daughters = ParticleFilter(
            kshort_flat_tree.OutputParticles,
            F.FILTER(F.require_all(F.ISBASICPARTICLE, (F.TRACKISVELO @ F.TRACK))),
        )

        relation_table_match_by_lhcbid_long = LHCbIDOverlapRelationTable(
            MatchFrom=kshort_flat_daughters,
            MatchTo=make_long_pions(),
            IncludeVP=True,
            IncludeFT=False,
            IncludeUT=False,
            MinMatchFraction=0.2,
        ).OutputRelations

        relation_table_match_by_lhcbid_upstream = LHCbIDOverlapRelationTable(
            MatchFrom=kshort_flat_daughters,
            MatchTo=make_up_pions(),
            IncludeVP=True,
            IncludeFT=False,
            IncludeUT=False,
            MinMatchFraction=0.2,
        ).OutputRelations

        matched_long_pions = SelectionFromWeightedRelationTable(
            InputRelations=relation_table_match_by_lhcbid_long
        ).OutputLocation
        matched_upstream_pions = SelectionFromWeightedRelationTable(
            InputRelations=relation_table_match_by_lhcbid_upstream
        ).OutputLocation

        particles_to_match = [
            ("LongPions", make_long_pions()),
            ("UpPions", make_up_pions()),
            ("MatchedLongPions", matched_long_pions),
            ("MatchedUpstreamPions", matched_upstream_pions),
        ]
    else:
        particles_to_match = None
    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), filtered_kshorts],
        prescale=prescale,
        monitoring_variables=["m"],
        extra_outputs=particles_to_match,
        persistreco=not turbo_line,
    )


@register_line_builder(turcal_lines)
@configurable
def kshort_velo_long_line(
    name="Hlt2TrackEff_TurCalVelo2Long_KshortVSoft", prescale=0.0005
):
    return construct_hlt2_line(
        name=name, prescale=prescale, pt_min=250.0, pt_max=800.0, p_min=3000.0
    )


@register_line_builder(turcal_lines)
@configurable
def kshort_velo_long_line_15(
    name="Hlt2TrackEff_TurCalVelo2Long_KshortSoft", prescale=0.0025
):
    return construct_hlt2_line(
        name=name, prescale=prescale, pt_min=800.0, pt_max=1500.0
    )


@register_line_builder(turcal_lines)
@configurable
def kshort_velo_long_line_20(
    name="Hlt2TrackEff_TurCalVelo2Long_Kshort", prescale=0.0035
):
    return construct_hlt2_line(
        name=name, prescale=prescale, pt_min=1500.0, pt_max=2000.0
    )


@register_line_builder(turcal_lines)
@configurable
def kshort_velo_long_line_high(
    name="Hlt2TrackEff_TurCalVelo2Long_KshortHard", prescale=0.005
):
    return construct_hlt2_line(
        name=name, prescale=prescale, pt_min=2000.0, pt_max=3500.0
    )


@register_line_builder(turcal_lines)
@configurable
def kshort_velo_long_line_vhigh(
    name="Hlt2TrackEff_TurCalVelo2Long_KshortVHard", prescale=0.035
):
    return construct_hlt2_line(
        name=name, prescale=prescale, pt_min=3500.0, pt_max=10000.0
    )


@register_line_builder(turbo_lines)
@configurable
def turbo_kshort_velo_long_line_vhard(
    name="Hlt2TurboVelo2Long_KshortVHard", prescale=0.5
):
    return construct_hlt2_line(
        name=name,
        prescale=prescale,
        pt_min=3500.0,
        pt_max=10000.0,
        filter_tis=True,
        apply_lambda_veto=True,
    )


@register_line_builder(turbo_lines)
@configurable
def turbo_kshort_velo_long_line_hard(
    name="Hlt2TurboVelo2Long_KshortHard", prescale=0.14
):
    return construct_hlt2_line(
        name=name,
        prescale=prescale,
        pt_min=2000.0,
        pt_max=3500.0,
        filter_tis=True,
        apply_lambda_veto=True,
    )


@register_line_builder(turbo_lines)
@configurable
def turbo_kshort_velo_long_line(name="Hlt2TurboVelo2Long_Kshort", prescale=0.05):
    return construct_hlt2_line(
        name=name,
        prescale=prescale,
        pt_min=1500.0,
        pt_max=2000.0,
        filter_tis=True,
        apply_lambda_veto=True,
    )


@register_line_builder(turbo_lines)
@configurable
def turbo_kshort_velo_long_line_soft(
    name="Hlt2TurboVelo2Long_KshortSoft", prescale=0.013
):
    return construct_hlt2_line(
        name=name, prescale=prescale, pt_min=800.0, pt_max=1500.0, filter_tis=True
    )


@register_line_builder(turbo_lines)
@configurable
def turbo_kshort_velo_long_line_vsoft(
    name="Hlt2TurboVelo2Long_KshortVSoft", prescale=0.0015
):
    return construct_hlt2_line(
        name=name,
        prescale=prescale,
        pt_min=250.0,
        pt_max=800.0,
        p_min=3000.0,
        filter_tis=True,
    )
