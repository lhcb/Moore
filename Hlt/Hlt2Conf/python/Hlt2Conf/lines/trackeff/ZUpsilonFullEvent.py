###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""These lines define high-mass dimuon selections used for general calibration
purposes (mostly related to tracking detectors). They overlap mostly
with the lines from the QEE WG, but are not in that stream for
administration - when a stream optimisation is possible, suggest
that this is put together with the other high-pT muon lines.

Cuts applied are inspired by the Run-3 Drell-Yan HLT2 lines (in QEE).

If there are default function arguments, the cut values applied
are set/configured there. If they are absent, they are intended
to be set downstream.

Contact: Laurent Dufour <laurent.dufour@cern.ch>
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, mm
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from Moore.streams import DETECTORS
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.event_filters import require_pvs
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction
from RecoConf.standard_particles import make_ismuon_long_muon

turbo_lines = {}


def filter_muons(
    particles,
    pvs,
    max_ip,
    min_pt=1.7 * GeV,
    min_eta=1.9,
    max_eta=5.1,
    min_p=17.5 * GeV,
):
    muon_requirements = [
        in_range(min_eta, F.ETA, max_eta),
        F.PT >= min_pt,
        F.P >= min_p,
    ]

    if max_ip is not None:
        muon_requirements += [F.MINIP(pvs) < max_ip]

    muon_requirements = F.require_all(*muon_requirements)

    return ParticleFilter(particles, F.FILTER(muon_requirements))


def make_dimuon_candidates(
    name,
    pvs,
    min_mass,
    max_mass,
    max_ip,
    min_maxpidmu=2,
    max_doca=0.15 * mm,
    vchi2pdof_max=25,
):
    all_muons = make_ismuon_long_muon()
    decay_descriptor = "Z0 -> mu+ mu-"

    filtered_muons = filter_muons(all_muons, pvs, max_ip=max_ip)

    combination_cuts = F.require_all(
        in_range(min_mass, F.MASS, max_mass),
        F.MAX(F.PID_MU) > min_maxpidmu,
        F.SDOCA(1, 2) < max_doca,
    )
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max)

    return ParticleCombiner(
        [filtered_muons, filtered_muons],
        name=name,
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_cuts,
        CompositeCut=vertex_code,
    )


@register_line_builder(turbo_lines)
def hlt2_dimuon_upsilon_calibration_line(
    name="Hlt2TrackEff_Upsilon_FullEvent", prescale=1.0
):
    pvs = make_pvs()
    dimuons = make_dimuon_candidates(
        name + "_combiner",
        pvs=pvs,
        max_ip=0.25 * mm,
        min_mass=8.2 * GeV,
        max_mass=12.5 * GeV,
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), dimuons],
        persistreco=False,
        raw_banks=DETECTORS,
        prescale=prescale,
    )


@register_line_builder(turbo_lines)
def hlt2_dimuon_Z_calibration_line(name="Hlt2TrackEff_Z0_FullEvent", prescale=1.0):
    pvs = make_pvs()
    dimuons = make_dimuon_candidates(
        name + "_combiner",
        pvs=pvs,
        max_ip=None,
        min_mass=60.0 * GeV,
        max_mass=125.0 * GeV,
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [dimuons],
        persistreco=False,
        raw_banks=DETECTORS,
        prescale=prescale,
    )
