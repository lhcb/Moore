###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definition of SMOG2 muon HLT2 lines
"""

from GaudiKernel.SystemOfUnits import MeV
from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters
from Hlt2Conf.lines.ift.builders.smog2_muons_builders import make_Jpsi2mumutagged
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.event_filters import require_gec
from RecoConf.reconstruction_objects import make_pvs

PROCESS = "hlt2"
all_lines = {}
hlt1_filter = ["Hlt1HeavyIonPbSMOGHadronicDecision"]


@configurable
def ion_prefilters(with_gec=True):
    return [require_gec(cut=30_000, skipUT=True)] if with_gec else []


_MASSWINDOW_PIDJPSI = [2900.0 * MeV, 3300.0 * MeV]


# JpsiToMuMumTagged
@register_line_builder(all_lines)
@configurable
def smog2_Jpsi2mumumtagged_line(
    name="Hlt2PID_SMOG2PIDJpsi2MuMumTagged",
    prescale=1,
    persistreco=True,
    massWind_Jpsi=_MASSWINDOW_PIDJPSI,
    tagcharge=-1,
):
    """
    SMOG2 J/psi(1S) -> mu mu and Psi(2S) -> mu mu HLT2 trigger line
    """
    pvs = make_pvs
    jpsis = make_Jpsi2mumutagged(
        process="hlt2",
        pvs=pvs,
        massWind_Jpsi=massWind_Jpsi,
        tagcharge=tagcharge,
    )

    return Hlt2Line(
        name=name,
        hlt1_filter_code=hlt1_filter,
        algs=ion_prefilters() + make_smog2_prefilters(pvs=pvs) + [jpsis],
        prescale=prescale,
        persistreco=persistreco,
    )


# JpsiToMuMupTagged
@register_line_builder(all_lines)
@configurable
def smog2_Jpsi2mumuptagged_line(
    name="Hlt2PID_SMOG2PIDJpsi2MuMupTagged",
    prescale=1,
    persistreco=True,
    massWind_Jpsi=_MASSWINDOW_PIDJPSI,
    tagcharge=+1,
):
    """
    SMOG2 J/psi(1S) -> mu mu and Psi(2S) -> mu mu HLT2 trigger line
    """
    pvs = make_pvs
    jpsis = make_Jpsi2mumutagged(
        process="hlt2",
        pvs=pvs,
        tagcharge=tagcharge,
        massWind_Jpsi=massWind_Jpsi,
    )

    return Hlt2Line(
        name=name,
        hlt1_filter_code=hlt1_filter,
        algs=ion_prefilters() + make_smog2_prefilters(pvs=pvs) + [jpsis],
        prescale=prescale,
        persistreco=persistreco,
    )
