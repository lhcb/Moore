###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.event_filters import require_gec

ionraw_lines = {}
ion_lines = {}
ion_lines_neon = {}


def _pbpb_prefilters():
    return [require_gec(cut=30_000, skipUT=True)]


@register_line_builder(ionraw_lines)
@configurable
def hlt2_PbPb_line(name="Hlt2PbPb", prescale=1):
    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters(),
        hlt1_filter_code=r"^Hlt1HeavyIonPbPb(?!UPC).*Decision",
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(ion_lines_neon)
@register_line_builder(ion_lines)
@configurable
def hlt2_PbPb_upc_line(name="Hlt2PbPbUPC", prescale=1.0):
    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters(),
        hlt1_filter_code=r"^Hlt1HeavyIonPbPbUPC.*Decision",
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(ion_lines)
@configurable
def hlt2_PbSMOG_hadronic_line(name="Hlt2PbSMOG_Hadronic", prescale=0.02):
    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters() + make_smog2_prefilters(),
        hlt1_filter_code="Hlt1HeavyIonPbSMOGHadronicDecision",
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(ion_lines_neon)
@configurable
def hlt2_PbNe_hadronic_line(name="Hlt2PbSMOG_Hadronic", prescale=1):
    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters() + make_smog2_prefilters(),
        hlt1_filter_code="Hlt1HeavyIonPbSMOGHadronicDecision",
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(ion_lines_neon)
@register_line_builder(ion_lines)
@configurable
def hlt2_PbSMOG_minbias_line(name="Hlt2PbSMOG_MinBiasPassthrough", prescale=0.1):
    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters(),
        hlt1_filter_code=[
            "Hlt1HeavyIonPbSMOGMicroBiasDecision",
            "Hlt1HeavyIonPbSMOGMBOneTrackDecision",
        ],
        persistreco=True,
        prescale=prescale,
    )


@register_line_builder(ionraw_lines)
@configurable
def hlt2_PbPb_inclusive_line(name="Hlt2PbPb_Inclusive", prescale=0.001):
    """
    A line that would require "everything but Hlt2PbPb_Hadronic" would remove only
    pathological events, since in almost all cases another Hlt1 line fires as well.
    Hence this line is fully inclusive.
    """
    return Hlt2Line(
        name=name,
        algs=_pbpb_prefilters(),
        hlt1_filter_code=r"^Hlt1(?!ODINLumi).*Decision",
        persistreco=True,
        prescale=prescale,
    )
