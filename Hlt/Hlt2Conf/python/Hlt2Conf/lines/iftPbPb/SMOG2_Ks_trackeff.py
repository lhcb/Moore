###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Hlt2Conf.lines.ift.builders.smog2_builders import (
    bpv_in_smog2,
    make_smog2_prefilters,
)
from Hlt2Conf.probe_muons import make_velo_muons
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleCombiner, ParticleFilter
from RecoConf.event_filters import require_gec
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import make_long_pions

all_lines = {}

hlt1_filter = ["Hlt1HeavyIonPbSMOGHadronicDecision"]


@configurable
def ion_prefilters(with_gec=True):
    return [require_gec(cut=30_000, skipUT=True)] if with_gec else []


@configurable
def filter_pions(particles, pvs=None, pt_min=0.3 * GeV):
    cut = F.require_all(
        F.P > 2000,
        F.PT > pt_min,
        F.ETA < 5.0,
        F.ETA > 1.5,
        F.BPVIPCHI2(pvs()) >= 25,
    )
    return ParticleFilter(particles, F.FILTER(cut))


@configurable
def filter_particles_velo(particles, pvs):
    cut = F.require_all(
        F.ETA < 4.5,
        F.ETA > 1.5,
        F.BPVIPCHI2(pvs()) >= 20,
    )
    return ParticleFilter(particles, F.FILTER(cut))


@configurable
def make_kshort_pi_muplus_specific_charge(
    pions,
    muons,
    pvs,
    decay_descriptor,
    comb_m_min=0 * MeV,
    comb_m_max=2650 * MeV,
    comb_maxdoca=0.15 * mm,
    name="KShortMuPiCombiner_{hash}",
):
    combination_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.MAXDOCACUT(comb_maxdoca),
    )
    vertex_code = F.require_all(
        F.END_VRHO < 100 * mm,
        F.END_VZ < 2200 * mm,
        bpv_in_smog2(pvs),
    )
    return ParticleCombiner(
        [pions, muons],
        name=name,
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )


from RecoConf.algorithms_thor import ParticleContainersMerger


def make_kshort_pi_muplus(pions, muons, pvs, **kwargs):
    ks_particle_combinations = [
        make_kshort_pi_muplus_specific_charge(
            pions,
            muons,
            pvs,
            "KS0 -> pi+ mu+",
            name="KShortMuPiCombiner_{hash}",
            **kwargs,
        ),
        make_kshort_pi_muplus_specific_charge(
            pions,
            muons,
            pvs,
            "KS0 -> pi+ mu-",
            name="KShortMuPiCombiner_{hash}",
            **kwargs,
        ),
        make_kshort_pi_muplus_specific_charge(
            pions,
            muons,
            pvs,
            "KS0 -> pi- mu-",
            name="KShortMuPiCombiner_{hash}",
            **kwargs,
        ),
        make_kshort_pi_muplus_specific_charge(
            pions,
            muons,
            pvs,
            "KS0 -> pi- mu+",
            name="KShortMuPiCombiner_{hash}",
            **kwargs,
        ),
    ]

    return ParticleContainersMerger(
        ks_particle_combinations, name="KS_combinations_{hash}"
    )


from PyConf.Algorithms import KSLongVeloFilter


@register_line_builder(all_lines)
@configurable
def kshort_velo_long_line_high(
    name="Hlt2TrackEff_SMOG2KshortVeloLong_HighPT", prescale=0.2, persistreco=True
):
    pions = filter_pions(make_long_pions(), make_pvs, pt_min=0.5 * GeV)
    muonsVelo = filter_particles_velo(make_velo_muons(), make_pvs)
    kshorts = make_kshort_pi_muplus(
        pions, muonsVelo, make_pvs, comb_m_max=1500, comb_m_min=0
    )  # probe should be on index 1

    filtered_kshorts = KSLongVeloFilter(
        InputParticle=kshorts,
        InputPVs=make_pvs(),
        PVConstrainedMassMin=350.0,
        PVConstrainedMassMax=625.0,
        PVConstrainedProbePtMin=1500.0,
        PVConstrainedProbePtMax=4000.0,
        IPperpendicularMax=0.007,
        IPMax=0.8,
        name="TrackEffFilter_{hash}",
    ).OutputParticles

    return Hlt2Line(
        name=name,
        hlt1_filter_code=hlt1_filter,
        algs=ion_prefilters()
        + make_smog2_prefilters(pvs=make_pvs)
        + [filtered_kshorts],
        prescale=prescale,
        raw_banks=["VP", "UT", "FT", "Muon"],
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def kshort_velo_long_line_veryhigh(
    name="Hlt2TrackEff_SMOG2KshortVeloLong_VeryHighPT", prescale=1, persistreco=True
):
    pions = filter_pions(make_long_pions(), make_pvs, pt_min=0.5 * GeV)
    muonsVelo = filter_particles_velo(make_velo_muons(), make_pvs)
    kshorts = make_kshort_pi_muplus(
        pions, muonsVelo, make_pvs, comb_m_max=1500, comb_m_min=0
    )  # probe should be on index 1

    filtered_kshorts = KSLongVeloFilter(
        InputParticle=kshorts,
        InputPVs=make_pvs(),
        PVConstrainedMassMin=350.0,
        PVConstrainedMassMax=625.0,
        PVConstrainedProbePtMin=4000.0,
        IPperpendicularMax=0.007,
        IPMax=0.8,
        name="TrackEffFilter_{hash}",
    ).OutputParticles

    return Hlt2Line(
        name=name,
        hlt1_filter_code=hlt1_filter,
        algs=ion_prefilters()
        + make_smog2_prefilters(pvs=make_pvs)
        + [filtered_kshorts],
        prescale=prescale,
        raw_banks=["VP", "UT", "FT", "Muon"],
        persistreco=persistreco,
    )
