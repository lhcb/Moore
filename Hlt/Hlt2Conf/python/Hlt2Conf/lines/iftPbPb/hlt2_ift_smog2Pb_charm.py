###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, picosecond
from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters
from Hlt2Conf.lines.ift.builders.smog2_charm_builders import (
    make_charm2hadrons,
    make_charm3hadrons,
    make_charm4hadrons,
    make_comb_criteria,
    make_particle_criteria,
)
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleFilter
from RecoConf.event_filters import require_gec
from RecoConf.reconstruction_objects import make_pvs


@configurable
def ion_prefilters(with_gec=True):
    return [require_gec(cut=30_000, skipUT=True)] if with_gec else []


all_lines = {}
PROCESS = "hlt2"

# ===================
#    PDG masses
# ===================

__MASS_D0 = 1864.83 * MeV
__MASS_DSTAR = 2010.26 * MeV
__MASS_DP = 1869.62 * MeV
__MASS_DS = 1968.49 * MeV
__MASS_Lc = 2286.46 * MeV
__MASS_Xicp = 2467.95 * MeV
__MASS_Xic0 = 2470.99 * MeV
__MASS_Omegac0 = 2695.2 * MeV
__MASS_ETAC = 3000 * MeV
__MASS_JPSI = 3096.9 * MeV
__MASS_CHIC0 = 3414.7 * MeV
__MASS_CHIC2 = 3556.17 * MeV
__MASS_PSI2S = 3686.1 * MeV

__MASSWin_D0 = 80 * MeV
__MASSWin_DSTAR = 75 * MeV
__MASSWin_ETAC = 200 * MeV
__MASSWin_DP = 80 * MeV
__MASSWin_DS = 80 * MeV
__MASSWin_Lc = 80 * MeV
__MASSWin_Xicp = 80 * MeV
__MASSWin_Xic0 = 80 * MeV
__MASSWin_Omegac0 = 100 * MeV

hlt1_filter = ["Hlt1HeavyIonPbSMOGHadronicDecision"]

# ========================================
#            2-body decays
# ========================================


@register_line_builder(all_lines)
@configurable
def D02kpiline(name="Hlt2IFTWithReco_SMOG2D02KPi", prescale=1, persistreco=True):
    # Define bpvipchi2, minPIDk, maxPIDK, minPIDp, minpT, minp, maxtrchi2dof
    # if the line retention rate is low : no PID cut
    # otherwise :
    ## for kaon : PIDK > 0
    ## for pion : PIDK < 5
    ## for proton : PIDp > 0

    # non prompt particle : bpipchi2 > 4
    # prompt particle : bpipchi2 > -1000

    pvs = make_pvs

    CriteriaKaon = make_particle_criteria(
        bpvipchi2=5,
        minPIDK=5,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=600 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )
    CriteriaPion = make_particle_criteria(
        bpvipchi2=5,
        minPIDK=None,
        maxPIDK=0,
        minPIDp=None,
        minPIDpK=None,
        minpT=600 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )
    Particles = ["kaon", "pion"]
    CriteriaParticles = [CriteriaKaon, CriteriaPion]

    CriteriaCombinations = make_comb_criteria(
        mass=__MASS_D0,
        masswin=__MASSWin_D0,
        min_child_pt=800 * MeV,
        max_sdoca=0.5 * mm,
        vchi2pdof_max=15,
        min_bpvltime=0.3 * picosecond,
    )

    D02kpi = make_charm2hadrons(
        name="D02kpi",
        process="hlt2",
        pvs=pvs,
        decay="[D0 -> K- pi+]cc",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        CriteriaCombination=CriteriaCombinations,
    )

    D02kpi_filtered = ParticleFilter(D02kpi, Cut=F.FILTER(F.OWNPVDIRA > 0.999))

    return Hlt2Line(
        name=name,
        algs=ion_prefilters() + make_smog2_prefilters(pvs=pvs) + [D02kpi_filtered],
        hlt1_filter_code=hlt1_filter,
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def D02kkline(name="Hlt2IFTNoReco_SMOG2D02KK", prescale=0.05, persistreco=False):
    pvs = make_pvs

    CriteriaKaon = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=5,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=600 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5,
    )
    Particles = ["kaon", "kaon"]
    CriteriaParticles = [CriteriaKaon, CriteriaKaon]

    CriteriaCombinations = make_comb_criteria(
        mass=__MASS_D0,
        masswin=__MASSWin_D0,
        min_child_pt=800 * MeV,
        max_sdoca=0.5 * mm,
        vchi2pdof_max=25,
        min_bpvltime=0.1 * picosecond,
    )

    D02kk = make_charm2hadrons(
        name="D02kk",
        process="hlt2",
        pvs=pvs,
        decay="D0 -> K- K+",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        CriteriaCombination=CriteriaCombinations,
    )

    D02kk_filtered = ParticleFilter(D02kk, Cut=F.FILTER(F.OWNPVDIRA > 0.999))

    return Hlt2Line(
        name=name,
        algs=ion_prefilters() + make_smog2_prefilters(pvs=pvs) + [D02kk_filtered],
        hlt1_filter_code=hlt1_filter,
        prescale=prescale,
        persistreco=persistreco,
        pv_tracks=True,
    )


@register_line_builder(all_lines)
@configurable
def D02pipiline(name="Hlt2IFTNoReco_SMOG2D02pipi", prescale=0.005, persistreco=False):
    pvs = make_pvs

    CriteriaPion = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=None,
        maxPIDK=0,
        minPIDp=None,
        minPIDpK=None,
        minpT=600 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5,
    )
    Particles = ["pion", "pion"]
    CriteriaParticles = [CriteriaPion, CriteriaPion]

    CriteriaCombinations = make_comb_criteria(
        mass=__MASS_D0,
        masswin=__MASSWin_D0,
        min_child_pt=800 * MeV,
        max_sdoca=0.5 * mm,
        vchi2pdof_max=25,
        min_bpvltime=0.1 * picosecond,
    )

    D02pipi = make_charm2hadrons(
        name="D02pipi",
        process="hlt2",
        pvs=pvs,
        decay="D0 -> pi- pi+",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        CriteriaCombination=CriteriaCombinations,
    )

    D02pipi_filtered = ParticleFilter(D02pipi, Cut=F.FILTER(F.OWNPVDIRA > 0.999))

    return Hlt2Line(
        name=name,
        algs=ion_prefilters() + make_smog2_prefilters(pvs=pvs) + [D02pipi_filtered],
        hlt1_filter_code=hlt1_filter,
        prescale=prescale,
        persistreco=persistreco,
        pv_tracks=True,
    )


@register_line_builder(all_lines)
@configurable
def Etac2ppbarline(name="Hlt2IFTNoReco_SMOG2Etac2ppbar", prescale=1, persistreco=False):
    pvs = make_pvs

    CriteriaProton = make_particle_criteria(
        bpvipchi2=None,
        minPIDK=None,
        maxPIDK=None,
        minPIDp=15,
        minPIDpK=7,
        minpT=800 * MeV,
        minp=15 * GeV,
        maxtrchi2dof=3,
    )
    Particles = ["proton", "proton"]
    CriteriaParticles = [CriteriaProton, CriteriaProton]

    CriteriaCombinations = make_comb_criteria(
        mass=__MASS_ETAC,
        masswin=__MASSWin_ETAC,
        min_child_pt=0.9 * GeV,
        max_sdoca=0.5 * mm,
        vchi2pdof_max=15,
    )

    etac2ppbar = make_charm2hadrons(
        name="etac2ppbar",
        process="hlt2",
        pvs=pvs,
        decay="eta_c(1S) -> p+ p~-",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        CriteriaCombination=CriteriaCombinations,
    )

    etac2ppbar_filtered = ParticleFilter(etac2ppbar, Cut=F.FILTER(F.OWNPVDIRA > 0.999))

    return Hlt2Line(
        name=name,
        algs=ion_prefilters() + make_smog2_prefilters(pvs=pvs) + [etac2ppbar_filtered],
        hlt1_filter_code=hlt1_filter,
        prescale=prescale,
        persistreco=persistreco,
        pv_tracks=True,
    )


# ========================================
#            3-body decays
# ========================================


@register_line_builder(all_lines)
@configurable
def Dpm2kpipiline(name="Hlt2IFTWithReco_SMOG2Dpm2kpipi", prescale=1, persistreco=True):
    pvs = make_pvs

    CriteriaKaon = make_particle_criteria(
        bpvipchi2=5,
        minPIDK=5,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=400 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )
    CriteriaPion = make_particle_criteria(
        bpvipchi2=5,
        minPIDK=None,
        maxPIDK=1,
        minPIDp=None,
        minPIDpK=None,
        minpT=400 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )

    Particles = ["kaon", "pion", "pion"]
    CriteriaParticles = [CriteriaKaon, CriteriaPion, CriteriaPion]

    CriteriaCombinations = make_comb_criteria(
        mass=__MASS_DP,
        masswin=__MASSWin_DP,
        min_child_pt=800 * MeV,
        max_sdoca=0.5 * mm,
        vchi2pdof_max=10,
        min_bpvltime=0.5 * picosecond,
    )

    Dpm2kpipi = make_charm3hadrons(
        name="Dpm2kpipi",
        process="hlt2",
        pvs=pvs,
        decay="[D+ -> K- pi+ pi+]cc",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        CriteriaCombination=CriteriaCombinations,
    )

    Dpm2kpipi_filtered = ParticleFilter(Dpm2kpipi, Cut=F.FILTER(F.OWNPVDIRA > 0.999))

    return Hlt2Line(
        name=name,
        algs=ion_prefilters() + make_smog2_prefilters(pvs=pvs) + [Dpm2kpipi_filtered],
        hlt1_filter_code=hlt1_filter,
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def Ds2kkpiline(name="Hlt2IFTWithReco_SMOG2DsToKKPi", prescale=1, persistreco=True):
    pvs = make_pvs

    CriteriaKaon = make_particle_criteria(
        bpvipchi2=6,
        minPIDK=5,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=400 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )
    CriteriaPion = make_particle_criteria(
        bpvipchi2=6,
        minPIDK=None,
        maxPIDK=0,
        minPIDp=None,
        minPIDpK=None,
        minpT=400 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )
    Particles = ["kaon", "kaon", "pion"]
    CriteriaParticles = [CriteriaKaon, CriteriaKaon, CriteriaPion]

    CriteriaCombinations = make_comb_criteria(
        mass=__MASS_DS,
        masswin=__MASSWin_DS,
        min_child_pt=800 * MeV,
        max_sdoca=0.5 * mm,
        vchi2pdof_max=10,
        min_bpvltime=0.3 * picosecond,
    )

    Ds2kkpi = make_charm3hadrons(
        name="Ds2kkpi",
        process="hlt2",
        pvs=pvs,
        decay="[D_s+ -> K- K+ pi+]cc",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        CriteriaCombination=CriteriaCombinations,
    )

    Ds2kkpi_filtered = ParticleFilter(Ds2kkpi, Cut=F.FILTER(F.OWNPVDIRA > 0.999))

    return Hlt2Line(
        name=name,
        algs=ion_prefilters() + make_smog2_prefilters(pvs=pvs) + [Ds2kkpi_filtered],
        hlt1_filter_code=hlt1_filter,
        prescale=prescale,
        persistreco=persistreco,
    )


@register_line_builder(all_lines)
@configurable
def Lc2pkpiline(name="Hlt2IFTWithReco_SMOG2LcTopKPi", prescale=1, persistreco=True):
    pvs = make_pvs

    CriteriaProton = make_particle_criteria(
        bpvipchi2=6,
        minPIDK=None,
        maxPIDK=None,
        minPIDp=5,
        minPIDpK=3,
        minpT=300 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )
    CriteriaKaon = make_particle_criteria(
        bpvipchi2=6,
        minPIDK=5,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=300 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )
    CriteriaPion = make_particle_criteria(
        bpvipchi2=6,
        minPIDK=None,
        maxPIDK=0,
        minPIDp=None,
        minPIDpK=None,
        minpT=300 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )
    Particles = ["proton", "kaon", "pion"]
    CriteriaParticles = [CriteriaProton, CriteriaKaon, CriteriaPion]

    CriteriaCombinations = make_comb_criteria(
        mass=__MASS_Lc,
        masswin=__MASSWin_Lc,
        min_child_pt=800 * MeV,
        max_sdoca=0.5 * mm,
        vchi2pdof_max=10,
        min_bpvltime=0.1 * picosecond,
    )

    Lc2pkpi = make_charm3hadrons(
        name="Lc2pkpi",
        process="hlt2",
        pvs=pvs,
        decay="[Lambda_c+ -> p+ K- pi+]cc",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        CriteriaCombination=CriteriaCombinations,
    )

    Lc2pkpi_filtered = ParticleFilter(Lc2pkpi, Cut=F.FILTER(F.OWNPVDIRA > 0.999))

    return Hlt2Line(
        name=name,
        algs=ion_prefilters() + make_smog2_prefilters(pvs=pvs) + [Lc2pkpi_filtered],
        hlt1_filter_code=hlt1_filter,
        prescale=prescale,
        persistreco=persistreco,
        pv_tracks=True,
    )


@register_line_builder(all_lines)
@configurable
def Xicp2pkpiline(name="Hlt2IFTNoReco_SMOG2XicpTopKPi", prescale=1, persistreco=False):
    pvs = make_pvs

    CriteriaProton = make_particle_criteria(
        bpvipchi2=5,
        minPIDK=None,
        maxPIDK=None,
        minPIDp=5,
        minPIDpK=3,
        minpT=300 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )
    CriteriaKaon = make_particle_criteria(
        bpvipchi2=5,
        minPIDK=5,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=300 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )
    CriteriaPion = make_particle_criteria(
        bpvipchi2=5,
        minPIDK=None,
        maxPIDK=0,
        minPIDp=None,
        minPIDpK=None,
        minpT=300 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )
    Particles = ["proton", "kaon", "pion"]
    CriteriaParticles = [CriteriaProton, CriteriaKaon, CriteriaPion]

    CriteriaCombinations = make_comb_criteria(
        mass=__MASS_Xicp,
        masswin=__MASSWin_Xicp,
        min_child_pt=800 * MeV,
        max_sdoca=0.5 * mm,
        vchi2pdof_max=10,
        min_bpvltime=0.1 * picosecond,
    )

    Xicp2pkpi = make_charm3hadrons(
        name="Xicp2pkpi",
        process="hlt2",
        pvs=pvs,
        decay="[Xi_c+ -> p+ K- pi+]cc",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        CriteriaCombination=CriteriaCombinations,
    )

    Xicp2pkpi_filtered = ParticleFilter(Xicp2pkpi, Cut=F.FILTER(F.OWNPVDIRA > 0.999))

    return Hlt2Line(
        name=name,
        algs=ion_prefilters() + make_smog2_prefilters(pvs=pvs) + [Xicp2pkpi_filtered],
        hlt1_filter_code=hlt1_filter,
        prescale=prescale,
        persistreco=persistreco,
        pv_tracks=True,
    )


# ========================================
#            4-body decays
# ========================================


@register_line_builder(all_lines)
@configurable
def Xic02pkkpiline(
    name="Hlt2IFTNoReco_SMOG2Xic0TopKKPi", prescale=1, persistreco=False
):
    pvs = make_pvs

    CriteriaProton = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=None,
        maxPIDK=None,
        minPIDp=5,
        minPIDpK=3,
        minpT=300 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )
    CriteriaKaon = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=0,
        maxPIDK=5,
        minPIDp=None,
        minPIDpK=None,
        minpT=300 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )
    CriteriaPion = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=None,
        maxPIDK=0,
        minPIDp=None,
        minPIDpK=None,
        minpT=300 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )
    Particles = ["proton", "kaon", "kaon", "pion"]
    CriteriaParticles = [CriteriaProton, CriteriaKaon, CriteriaKaon, CriteriaPion]

    Xic02pkkpi = make_charm4hadrons(
        name="Xic02pkkpi",
        process="hlt2",
        pvs=pvs,
        decay="[Xi_c0 -> p+ K- K- pi+]cc",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        mass=__MASS_Xic0,
        masswindow=__MASSWin_Xic0,
        max_sdoca=0.2 * mm,
        max_vchi2pdof=15,
        allchild_ptcut=0 * MeV,
        twochild_ptcut=400 * MeV,
        onechild_ptcut=800 * MeV,
    )

    Xic02pkkpi_filtered = ParticleFilter(Xic02pkkpi, Cut=F.FILTER(F.OWNPVDIRA > 0.999))

    return Hlt2Line(
        name=name,
        algs=ion_prefilters() + make_smog2_prefilters(pvs=pvs) + [Xic02pkkpi_filtered],
        hlt1_filter_code=hlt1_filter,
        prescale=prescale,
        persistreco=persistreco,
        pv_tracks=True,
    )


@register_line_builder(all_lines)
@configurable
def Omegac02pkkpiline(
    name="Hlt2IFTNoReco_SMOG2Omegac0TopKKPi", prescale=1, persistreco=False
):
    pvs = make_pvs

    CriteriaProton = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=None,
        maxPIDK=None,
        minPIDp=5,
        minPIDpK=3,
        minpT=300 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )
    CriteriaKaon = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=5,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=300 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )
    CriteriaPion = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=None,
        maxPIDK=0,
        minPIDp=None,
        minPIDpK=None,
        minpT=300 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )
    Particles = ["proton", "kaon", "kaon", "pion"]
    CriteriaParticles = [CriteriaProton, CriteriaKaon, CriteriaKaon, CriteriaPion]

    Omegac02pkkpi = make_charm4hadrons(
        name="Omegac02pkkpi",
        process="hlt2",
        pvs=pvs,
        decay="[Omega_c0 -> p+ K- K- pi+]cc",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        mass=__MASS_Omegac0,
        masswindow=__MASSWin_Omegac0,
        max_sdoca=0.2 * mm,
        max_vchi2pdof=15,
        allchild_ptcut=0 * MeV,
        twochild_ptcut=400 * MeV,
        onechild_ptcut=800 * MeV,
    )

    Omegac02pkkpi_filtered = ParticleFilter(
        Omegac02pkkpi, Cut=F.FILTER(F.OWNPVDIRA > 0.999)
    )

    return Hlt2Line(
        name=name,
        algs=ion_prefilters()
        + make_smog2_prefilters(pvs=pvs)
        + [Omegac02pkkpi_filtered],
        hlt1_filter_code=hlt1_filter,
        prescale=prescale,
        persistreco=persistreco,
        pv_tracks=True,
    )


@register_line_builder(all_lines)
@configurable
def Hiddencharm4piline(
    name="Hlt2IFTNoReco_SMOG2ccTo4Pi", prescale=0.001, persistreco=False
):
    pvs = make_pvs

    CriteriaPion = make_particle_criteria(
        bpvipchi2=None,
        minPIDK=None,
        maxPIDK=0,
        minPIDp=None,
        minPIDpK=None,
        minpT=500 * MeV,  # 250
        minp=3 * GeV,
        maxtrchi2dof=3,  # to 3
    )
    Particles = ["pion", "pion", "pion", "pion"]
    CriteriaParticles = [CriteriaPion, CriteriaPion, CriteriaPion, CriteriaPion]

    cc2pipipipi = make_charm4hadrons(
        name="cc2pipipipi",
        process="hlt2",
        pvs=pvs,
        decay="eta_c(1S) -> pi- pi- pi+ pi+",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        max_sdoca=0.5 * mm,  # Tighten
        max_vchi2pdof=25.0,
        mass=3300 * MeV,
        masswindow=500 * MeV,
        allchild_ptcut=0.5 * GeV,
        twochild_ptcut=1.0 * GeV,
        onechild_ptcut=1.2 * GeV,
    )

    return Hlt2Line(
        name=name,
        algs=ion_prefilters() + make_smog2_prefilters(pvs=pvs) + [cc2pipipipi],
        hlt1_filter_code=hlt1_filter,
        prescale=prescale,
        persistreco=persistreco,
        pv_tracks=True,
    )


@register_line_builder(all_lines)
@configurable
def Hiddencharm2pi2k_line(
    name="Hlt2IFTNoReco_SMOG2ccTo2Pi2K", prescale=0.001, persistreco=False
):
    pvs = make_pvs

    CriteriaPion = make_particle_criteria(
        bpvipchi2=None,
        minPIDK=None,
        maxPIDK=0,
        minPIDp=None,
        minPIDpK=None,
        minpT=400 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )
    CriteriaKaon = make_particle_criteria(
        bpvipchi2=None,
        minPIDK=5,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=400 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )
    Particles = ["kaon", "kaon", "pion", "pion"]
    CriteriaParticles = [CriteriaKaon, CriteriaKaon, CriteriaPion, CriteriaPion]

    cc2kkpipi = make_charm4hadrons(
        name="cc2kkpipi",
        process="hlt2",
        pvs=pvs,
        decay="chi_c1(1P) -> K- K+ pi- pi+",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        max_sdoca=0.5 * mm,
        max_vchi2pdof=25.0,
        mass=__MASS_CHIC0,
        masswindow=600 * MeV,
        allchild_ptcut=0.4 * GeV,
        twochild_ptcut=1.0 * GeV,
        onechild_ptcut=1.2 * GeV,
    )

    return Hlt2Line(
        name=name,
        algs=ion_prefilters() + make_smog2_prefilters(pvs=pvs) + [cc2kkpipi],
        hlt1_filter_code=hlt1_filter,
        prescale=prescale,
        persistreco=persistreco,
        pv_tracks=True,
    )


@register_line_builder(all_lines)
@configurable
def Hiddencharm4k_line(
    name="Hlt2IFTNoReco_SMOG2ccTo4K", prescale=0.001, persistreco=False
):
    pvs = make_pvs

    CriteriaKaon = make_particle_criteria(
        bpvipchi2=None,
        minPIDK=5,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=400 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=3,
    )
    Particles = ["kaon", "kaon", "kaon", "kaon"]
    CriteriaParticles = [CriteriaKaon, CriteriaKaon, CriteriaKaon, CriteriaKaon]

    cc2kkkk = make_charm4hadrons(
        name="cc2kkkk",
        process="hlt2",
        pvs=pvs,
        decay="chi_c1(1P) -> K- K- K+ K+",
        particle=Particles,
        CriteriaParticle=CriteriaParticles,
        max_sdoca=0.5 * mm,
        max_vchi2pdof=25,
        mass=__MASS_CHIC0,
        masswindow=600 * MeV,
        allchild_ptcut=0.4 * GeV,
        twochild_ptcut=0.6 * GeV,
        onechild_ptcut=0.8 * GeV,
    )

    return Hlt2Line(
        name=name,
        algs=ion_prefilters() + make_smog2_prefilters(pvs=pvs) + [cc2kkkk],
        hlt1_filter_code=hlt1_filter,
        prescale=prescale,
        persistreco=persistreco,
        pv_tracks=True,
    )
