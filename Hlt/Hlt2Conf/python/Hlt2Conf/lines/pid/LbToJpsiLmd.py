###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
HLT2 PID line for Lambda_b0 -> J/psi Lmd, Lmd for LL and DD.
Focus on the Lmd downtrack study.
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, ns
from Moore.config import Hlt2Line, register_line_builder
from RecoConf.algorithms_thor import ParticleCombiner
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    make_down_pions,
    make_down_protons,
    make_ismuon_long_muon,
    make_long_pions,
    make_long_protons,
)

from Hlt2Conf.lines.pid.utils import constants
from Hlt2Conf.lines.pid.utils import filters as flt
from Hlt2Conf.lines.pid.utils.charmonium import make_jpsis

all_lines = {}


def make_l0lls(
    protons,
    pions,
    pvs,
    mm_min=constants.LAMBDA_M - 50 * MeV,
    mm_max=constants.LAMBDA_M + 50 * MeV,
    m_min=constants.LAMBDA_M - 20 * MeV,
    m_max=constants.LAMBDA_M + 20 * MeV,
    bpvltime_min=0.002 * ns,
    ks_veto_window=20 * MeV,
    vchi2_max=30,
    pvipchi2_max=50,
):
    combination_code = F.require_all(in_range(mm_min, F.MASS, mm_max))

    composite_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < vchi2_max,
        F.OWNPVIPCHI2 < pvipchi2_max,
        (F.MASSWITHHYPOTHESES(("pi+", "pi-")) < constants.K0_M - ks_veto_window)
        | (F.MASSWITHHYPOTHESES(("pi+", "pi-")) > constants.K0_M + ks_veto_window),
        F.OWNPVLTIME > bpvltime_min,
    )

    return ParticleCombiner(
        [protons, pions],
        name="PID_L0ToPPi_LL_Combiner_{hash}",
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


def make_l0dds(
    protons,
    pions,
    pvs,
    mm_min=constants.LAMBDA_M - 80 * MeV,
    mm_max=constants.LAMBDA_M + 80 * MeV,
    m_min=constants.LAMBDA_M - 21 * MeV,
    m_max=constants.LAMBDA_M + 24 * MeV,
    bpvvdz_min=40 * mm,
    ks_veto_window=20 * MeV,
    vchi2_max=30,
):
    combination_code = F.require_all(in_range(mm_min, F.MASS, mm_max))

    composite_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.OWNPVVDZ > bpvvdz_min,
        F.CHI2DOF < vchi2_max,
        (F.MASSWITHHYPOTHESES(("pi+", "pi-")) < constants.K0_M - ks_veto_window)
        | (F.MASSWITHHYPOTHESES(("pi+", "pi-")) > constants.K0_M - ks_veto_window),
    )

    return ParticleCombiner(
        [protons, pions],
        name="PID_L0ToPPi_DD_Combiner_{hash}",
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


def make_lambdabs(
    lambdas,
    jpsis,
    pvs,
    mm_min=constants.LAMBDAB_M - 350 * MeV,
    mm_max=constants.LAMBDAB_M + 350 * MeV,
    m_min=constants.LAMBDAB_M - 300 * MeV,
    m_max=constants.LAMBDAB_M + 300 * MeV,
    comb_pt_min=1000 * MeV,
    vchi2_max=25,
    pvfdchi2_min=2,
    bpvdira_min=0.9999,
    pvipchi2_max=15,
):
    combination_code = F.require_all(
        in_range(mm_min, F.MASS, mm_max), F.SUM(F.PT) > comb_pt_min
    )

    composite_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2 < vchi2_max,
        F.OWNPVFDCHI2 > pvfdchi2_min,
        F.OWNPVIPCHI2 < pvipchi2_max,
        F.OWNPVDIRA > bpvdira_min,
    )

    return ParticleCombiner(
        [lambdas, jpsis],
        name="PID_Lambdab0ToL0JPsi_Combiner_{hash}",
        DecayDescriptor="[Lambda_b0 -> Lambda0 J/psi(1S)]cc",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


@register_line_builder(all_lines)
def LbToLmdJpsi_DD(name="Hlt2PID_LbToLmdJpsi_DD"):
    pvs = make_pvs()

    protons = flt.filter_down_protons(make_down_protons(), pvs)

    pions = flt.filter_down_particles(
        make_down_pions(),
        p_min=0.5 * GeV,
        pt_min=0.0 * GeV,
    )

    l0dds = make_l0dds(protons, pions, pvs)

    muons = make_ismuon_long_muon()
    jpsis = make_jpsis(mu_neg=muons, mu_pos=muons)

    lambdabs = make_lambdabs(l0dds, jpsis, pvs)

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [l0dds, jpsis, lambdabs],
        persistreco=True,
    )


@register_line_builder(all_lines)
def LbToLmdJpsi_LL(name="Hlt2PID_LbToLmdJpsi_LL"):
    pvs = make_pvs()

    protons = flt.filter_long_protons(make_long_protons(), pvs)
    pions = flt.filter_long_particles(make_long_pions(), pvs)

    l0lls = make_l0lls(protons, pions, pvs)

    muons = make_ismuon_long_muon()
    jpsis = make_jpsis(mu_neg=muons, mu_pos=muons)

    lambdabs = make_lambdabs(l0lls, jpsis, pvs)

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [l0lls, jpsis, lambdabs],
        persistreco=True,
    )
