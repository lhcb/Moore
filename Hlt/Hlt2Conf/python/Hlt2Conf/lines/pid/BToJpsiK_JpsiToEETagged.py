###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define HLT2 PID line for B+ -> [J/psi -> e+ e-] K+.
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.algorithms_thor import ParticleCombiner
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    make_long_kaons,
)
from SelAlgorithms.monitoring import histogram_1d, monitor

from Hlt2Conf.lines.pid.utils import constants
from Hlt2Conf.lines.pid.utils import filters as flt
from Hlt2Conf.lines.pid.utils.charmonium import (
    make_jpsi_to_ee,
    make_probe_electrons,
    make_probe_electrons_noPIDe,
    make_tag_electrons,
    make_tag_electrons_noPIDe,
)

all_lines = {}


def make_bs(
    jpsis,
    kaons,
    pvs,
    am_min=constants.BP_M - 1179.34 * MeV,  # 4.1GeV
    am_max=constants.BP_M + 820.66 * MeV,  # 6.1GeV
    m_min=constants.BP_M - 1079.34 * MeV,  # 4.2GeV
    m_max=constants.BP_M + 720.66 * MeV,  # 6.0GeV
    vchi2_max=9,
    bpvvdchi2_min=150,
    bpvipchi2_max=30,
    bpvdira_min=0.8,
):
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
    )

    composite_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2 < vchi2_max,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.BPVDIRA(pvs) > bpvdira_min,
    )

    return ParticleCombiner(
        [jpsis, kaons],
        name="PID_BToJpsiK_Combiner_{hash}",
        DecayDescriptor="[B+ -> J/psi(1S) K+]cc",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


def make_bs_noPIDe(
    jpsis,
    kaons,
    pvs,
    am_min=constants.BP_M - 1179.34 * MeV,  # 4.1GeV
    am_max=constants.BP_M + 820.66 * MeV,  # 6.1GeV
    m_min=constants.BP_M - 1079.34 * MeV,  # 4.2GeV
    m_max=constants.BP_M + 720.66 * MeV,  # 6.0GeV
    vchi2_max=9,
    mipchi2_max=100,
    bpvdira_min=0.9999,
    bpvvdchi2_min=450,
    bpvipchi2_max=30,
):
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
    )

    composite_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.MINIPCHI2(pvs) < mipchi2_max,
        F.BPVDIRA(pvs) > bpvdira_min,
        F.CHI2 < vchi2_max,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
    )

    return ParticleCombiner(
        [jpsis, kaons],
        name="PID_BToJpsiK_Combiner_{hash}",
        DecayDescriptor="[B+ -> J/psi(1S) K+]cc",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


@register_line_builder(all_lines)
def BToJpsiK_JpsiToEmbremEpbremTagged_noPIDe(
    name="Hlt2PID_BToJpsiK_JpsiToEmbremEpbremTagged_noPIDe", prescale=1
):
    pvs = make_pvs()
    kaons = flt.filter_particles_dll(
        flt.filter_particles(
            make_long_kaons(),
            pvs,
            pt_min=1 * GeV,
            p_min=3 * GeV,
            mipchi2_min=25,
        ),
        dll_lim=5,
        dll_type="k",
        min=True,
    )
    e_tags = make_tag_electrons_noPIDe(charge=+1, brem=True)
    e_probes = make_probe_electrons_noPIDe(charge=-1, brem=True)
    jpsis = make_jpsi_to_ee(e_neg=e_probes, e_pos=e_tags)
    bs = make_bs_noPIDe(jpsis, kaons, pvs)

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [jpsis, bs],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(all_lines)
def BToJpsiK_JpsiToEpbremEmbremTagged_noPIDe(
    name="Hlt2PID_BToJpsiK_JpsiToEpbremEmbremTagged_noPIDe", prescale=1
):
    pvs = make_pvs()
    kaons = flt.filter_particles_dll(
        flt.filter_particles(
            make_long_kaons(),
            pvs,
            pt_min=1 * GeV,
            p_min=3 * GeV,
            mipchi2_min=25,
        ),
        dll_lim=5,
        dll_type="k",
        min=True,
    )
    e_tags = make_tag_electrons_noPIDe(charge=-1, brem=True)
    e_probes = make_probe_electrons_noPIDe(charge=+1, brem=True)
    jpsis = make_jpsi_to_ee(e_pos=e_probes, e_neg=e_tags)
    bs = make_bs_noPIDe(jpsis, kaons, pvs)

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [jpsis, bs],
        prescale=prescale,
        persistreco=True,
    )


###################################### Lines with PIDe ######################################


@register_line_builder(all_lines)
def BToJpsiK_JpsiToEmbremEpbremTagged(
    name="Hlt2PID_BToJpsiK_JpsiToEmbremEpbremTagged", prescale=1
):
    pvs = make_pvs()
    kaons = flt.filter_particles_dll(
        flt.filter_particles(
            make_long_kaons(),
            pvs,
            pt_min=1 * GeV,
            p_min=3 * GeV,
            mipchi2_min=9,
        ),
        dll_lim=5,
        dll_type="k",
        min=True,
    )
    e_tags = make_tag_electrons(charge=+1, brem=True)
    e_probes = make_probe_electrons(charge=-1, brem=True)
    jpsis = make_jpsi_to_ee(e_neg=e_probes, e_pos=e_tags)
    bs = make_bs(jpsis, kaons, pvs)

    probe_hasbrem_mon = histogram_1d(
        f"/{name}/probe_hasBrem",
        "probe_hasBrem",
        F.HASBREM,
        2,
        (-0.1, 1.1),
        "probe_hasBrem",
    )

    probe_ecalpide_mon = histogram_1d(
        f"/{name}/probe_ECALPIDe",
        "probe_ECALPIDe",
        F.VALUE_OR(F.NaN) @ F.ECALPIDE,
        60,
        (-10, 10),
        "probe_ECALPIDe",
    )

    probe_brempide_mon = histogram_1d(
        f"/{name}/probe_BREMPIDe",
        "probe_BREMPIDe",
        F.VALUE_OR(F.NaN) @ F.BREMPIDE,
        60,
        (-10, 10),
        "probe_BREMPIDe",
    )

    probe_monitor = monitor(
        data=e_probes,
        histograms=[probe_hasbrem_mon, probe_ecalpide_mon, probe_brempide_mon],
    )

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [jpsis, bs, probe_monitor],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(all_lines)
def BToJpsiK_JpsiToEpbremEmbremTagged(
    name="Hlt2PID_BToJpsiK_JpsiToEpbremEmbremTagged", prescale=1
):
    pvs = make_pvs()
    kaons = flt.filter_particles_dll(
        flt.filter_particles(
            make_long_kaons(),
            pvs,
            pt_min=1 * GeV,
            p_min=3 * GeV,
            mipchi2_min=9,
        ),
        dll_lim=5,
        dll_type="k",
        min=True,
    )
    e_tags = make_tag_electrons(charge=-1, brem=True)
    e_probes = make_probe_electrons(charge=+1, brem=True)
    jpsis = make_jpsi_to_ee(e_pos=e_probes, e_neg=e_tags)
    bs = make_bs(jpsis, kaons, pvs)

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [jpsis, bs],
        prescale=prescale,
        persistreco=True,
    )
