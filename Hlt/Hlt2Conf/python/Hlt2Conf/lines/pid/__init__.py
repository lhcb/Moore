###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines all the PID calibration HLT2 lines
"""

from . import (
    Bd2KstG_Bs2PhiG,
    BToJpsiK_JpsiToEETagged,
    BToJpsiK_JpsiToMuMuTagged,
    BToJpsiK_JpsiToPPTagged,
    D2EtapPi,
    Dsst2DsG_KKpi,
    DsToPhiPi_PhiToMuMuTagged,
    DstToD0Pi_D0ToKPi,
    DstToD0Pi_D0ToKPiPi0,
    DstToD0Pi_D0ToKPiPiPi,
    EtaMuMuG,
    JpsiToMuMuTagged_Detached,
    KsToPiPi,
    L0ToPPi,
    LbToJpsiLmd,
    LbToLcMuNu_LcToPKPi,
    LbToLcPi_LcToPKPi,
    LcToLmdpi,
    LcToPKPi,
    OmegaToL0K_L0ToPPi,
    PhiToKK_Detached,
    SMOG2_ChargedPID,
    SMOG2_DstToD0Pi,
    SMOG2_JpsiToMuMuTagged,
)

# provide "all_lines" for correct registration by the HLT2 lines module
all_lines = {}
all_lines.update(BToJpsiK_JpsiToEETagged.all_lines)
all_lines.update(BToJpsiK_JpsiToMuMuTagged.all_lines)
all_lines.update(BToJpsiK_JpsiToPPTagged.all_lines)
all_lines.update(DsToPhiPi_PhiToMuMuTagged.all_lines)
all_lines.update(DstToD0Pi_D0ToKPi.all_lines)
all_lines.update(DstToD0Pi_D0ToKPiPiPi.all_lines)
all_lines.update(JpsiToMuMuTagged_Detached.all_lines)
all_lines.update(KsToPiPi.all_lines)
all_lines.update(L0ToPPi.all_lines)
all_lines.update(LbToLcMuNu_LcToPKPi.all_lines)
all_lines.update(LbToLcPi_LcToPKPi.all_lines)
all_lines.update(LcToPKPi.all_lines)
all_lines.update(OmegaToL0K_L0ToPPi.all_lines)
all_lines.update(PhiToKK_Detached.all_lines)
all_lines.update(Bd2KstG_Bs2PhiG.all_lines)
all_lines.update(Dsst2DsG_KKpi.all_lines)
all_lines.update(EtaMuMuG.all_lines)
all_lines.update(DstToD0Pi_D0ToKPiPi0.all_lines)
all_lines.update(D2EtapPi.all_lines)
all_lines.update(SMOG2_JpsiToMuMuTagged.all_lines)
all_lines.update(SMOG2_DstToD0Pi.all_lines)
all_lines.update(SMOG2_ChargedPID.all_lines)
all_lines.update(LcToLmdpi.all_lines)
all_lines.update(LbToJpsiLmd.all_lines)
