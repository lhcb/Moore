###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Define HLT2 PID line for J/psi -> mu+ mu-.
"""

import Functors as F
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    make_ismuon_long_muon,
    make_long_muons,
)
from SelAlgorithms.monitoring import histogram_1d, monitor

from Hlt2Conf.lines.pid.utils.charmonium import (
    make_detached_jpsis,
    make_jpsis,
    make_probe_muons,
    make_tag_muons,
)
from Hlt2Conf.lines.pid.utils.filters import pid_prefilters

all_lines = {}


@register_line_builder(all_lines)
def JpsiToMuMumTagged_Detached(name="Hlt2PID_JpsiToMuMumTagged_Detached", prescale=1):
    pvs = make_pvs()
    tag_muons = make_tag_muons(make_ismuon_long_muon(), pvs, charge=-1)
    probe_muons = make_probe_muons(make_long_muons(), pvs, charge=+1)
    jpsis = make_jpsis(mu_neg=tag_muons, mu_pos=probe_muons)
    detached_jpsis = make_detached_jpsis(jpsis, pvs)

    probe_dll_mon = monitor(
        data=probe_muons,
        histograms=[
            histogram_1d(
                functor=F.PID_MU,
                name=f"/{name}/probe_PIDmu",
                title="probe_PIDmu",
                label="probe_PIDmu",
                bins=60,
                range=(-20, 20),
            )
        ],
    )

    probe_ismuon_mon = histogram_1d(
        functor=F.ISMUON,
        name=f"/{name}/probe_isMuon",
        title="probe_isMuon",
        label="probe_isMuon",
        bins=2,
        range=(-0.1, 1.1),
    )

    probe_p_mon = histogram_1d(
        functor=F.ISMUON * F.P,
        name=f"/{name}/probe_pass_P",
        title="probe_pass_P",
        label="probe_pass_P",
        bins=75,
        range=(3e3, 153e3),
    )

    muon_monitor = monitor(data=probe_muons, histograms=[probe_ismuon_mon, probe_p_mon])

    return Hlt2Line(
        name=name,
        algs=pid_prefilters() + [detached_jpsis, muon_monitor, probe_dll_mon],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(all_lines)
def JpsiToMuMupTagged_Detached(name="Hlt2PID_JpsiToMuMupTagged_Detached", prescale=1):
    pvs = make_pvs()
    tag_muons = make_tag_muons(make_ismuon_long_muon(), pvs, charge=+1)
    probe_muons = make_probe_muons(make_long_muons(), pvs, charge=-1)
    jpsis = make_jpsis(mu_pos=tag_muons, mu_neg=probe_muons)
    detached_jpsis = make_detached_jpsis(jpsis, pvs)
    return Hlt2Line(
        name=name,
        algs=pid_prefilters() + [detached_jpsis],
        prescale=prescale,
        persistreco=True,
    )
