###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
HLT2 PID line for Lambda_c-> Lambda pi, and Lambda_0 -> p pi- LL and DD.
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, ns, ps
from Moore.config import Hlt2Line, register_line_builder
from RecoConf.algorithms_thor import ParticleCombiner
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    make_down_pions,
    make_down_protons,
    make_has_rich_long_pions,
    make_long_pions,
    make_long_protons,
)

from Hlt2Conf.lines.pid.utils import constants
from Hlt2Conf.lines.pid.utils import filters as flt

all_lines = {}


def make_l0lls(
    protons,
    pions,
    pvs,
    mm_min=constants.LAMBDA_M - 30 * MeV,
    mm_max=constants.LAMBDA_M + 30 * MeV,
    m_min=constants.LAMBDA_M - 20 * MeV,
    m_max=constants.LAMBDA_M + 20 * MeV,
    bpvltime_min=1.0 * ps,
    ks_veto_window=20 * MeV,
    vchi2_max=30,
    pvfdchi2_min=50.0,
    pvipchi2_max=50,
    bpvvdz_min=8 * mm,
    adocachi2cut=20.0,
):
    combination_code = F.require_all(in_range(mm_min, F.MASS, mm_max))

    composite_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < vchi2_max,
        F.OWNPVIPCHI2 < pvipchi2_max,
        F.OWNPVFDCHI2 > pvfdchi2_min,
        F.OWNPVVDZ > bpvvdz_min,
        (F.MASSWITHHYPOTHESES(("pi+", "pi-")) < constants.K0_M - ks_veto_window)
        | (F.MASSWITHHYPOTHESES(("pi+", "pi-")) > constants.K0_M + ks_veto_window),
        F.OWNPVLTIME > bpvltime_min,
        F.MAXDOCACHI2CUT(adocachi2cut),
    )

    return ParticleCombiner(
        [protons, pions],
        name="PID_L0ToPPi_LL_Combiner_{hash}",
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


def make_l0dds(
    protons,
    pions,
    pvs,
    mm_min=constants.LAMBDA_M - 31 * MeV,
    mm_max=constants.LAMBDA_M + 34 * MeV,
    m_min=constants.LAMBDA_M - 21 * MeV,
    m_max=constants.LAMBDA_M + 24 * MeV,
    bpvvdz_min=200 * mm,
    ks_veto_window=20 * MeV,
    vchi2_max=40,
    vt_pvfdchi2_min=50.0,
    endvz_min=250 * mm,
    endvz_max=2485 * mm,
):
    combination_code = F.require_all(in_range(mm_min, F.MASS, mm_max))

    composite_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.OWNPVVDZ > bpvvdz_min,
        F.CHI2DOF < vchi2_max,
        F.OWNPVFDCHI2 > vt_pvfdchi2_min,
        F.math.in_range(endvz_min, F.END_VZ, endvz_max),
        (F.MASSWITHHYPOTHESES(("pi+", "pi-")) < constants.K0_M - ks_veto_window)
        | (F.MASSWITHHYPOTHESES(("pi+", "pi-")) > constants.K0_M - ks_veto_window),
    )

    return ParticleCombiner(
        [protons, pions],
        name="PID_L0ToPPi_DD_Combiner_{hash}",
        DecayDescriptor="[Lambda0 -> p+ pi-]cc",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


def make_lcs(
    lambdas,
    pions,
    pvs,
    m_min=constants.LAMBDAC_M - 75 * MeV,
    m_max=constants.LAMBDAC_M + 75 * MeV,
    mm_min=constants.LAMBDAC_M - 85 * MeV,
    mm_max=constants.LAMBDAC_M + 85 * MeV,
    comb_p_min=2000 * MeV,
    comb_pt_min=500 * MeV,
    vchi2pdof_max=30,
    pvfdchi2_min=50,
    bpvltime_min=0.00015 * ns,
    bcvtx_sep_min=10 * mm,
    pvipchi2_max=10,
):
    combination_code = F.require_all(
        F.SUM(F.P) > comb_p_min,
        F.SUM(F.PT) > comb_pt_min,
        in_range(mm_min, F.MASS, mm_max),
    )

    composite_code = F.require_all(
        in_range(m_min, F.MASS, m_max),
        F.CHI2DOF < vchi2pdof_max,
        F.OWNPVIPCHI2 < pvipchi2_max,
        F.OWNPVFDCHI2 > pvfdchi2_min,
        F.OWNPVLTIME > bpvltime_min,
    )

    if bcvtx_sep_min is not None:
        composite_code &= F.CHILD(1, F.END_VZ) - F.END_VZ > bcvtx_sep_min

    return ParticleCombiner(
        [lambdas, pions],
        name="PID_LcToLmdPi_Combiner_{hash}",
        DecayDescriptor="[Lambda_c+ -> Lambda0 pi+]cc",
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


@register_line_builder(all_lines)
def LcToLmdPi_DD(name="Hlt2PID_LcToLmdPi_DD", prescale=1):
    pvs = make_pvs()

    protons = flt.filter_down_protons(make_down_protons(), pvs)

    pions = flt.filter_down_particles(
        make_down_pions(),
        p_min=1.0 * GeV,
        pt_min=0.0 * GeV,
    )

    l0dds = make_l0dds(protons, pions, pvs)

    pions_lc = flt.filter_particles_ghostprob_max(
        flt.filter_particles_dll(
            flt.filter_particles(
                make_has_rich_long_pions(),
                pvs=pvs,
                pt_min=0.4 * GeV,
                p_min=1.0 * GeV,
                mipchi2_min=9,
            ),
            dll_type="k",
            dll_lim=-2,
            min=False,
        ),
        ghostprob_max=1e9,
    )

    lambdacs = make_lcs(l0dds, pions_lc, pvs, bcvtx_sep_min=None)

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [l0dds, lambdacs],
        prescale=prescale,
        persistreco=True,
    )


@register_line_builder(all_lines)
def LcToLmdPi_LL(name="Hlt2PID_LcToLmdPi_LL", prescale=0.1):
    pvs = make_pvs()

    protons = flt.filter_long_protons(make_long_protons(), pvs)
    pions = flt.filter_long_particles(make_long_pions(), pvs)

    l0lls = make_l0lls(protons, pions, pvs)

    pions_lc = flt.filter_particles_ghostprob_max(
        flt.filter_particles_dll(
            flt.filter_particles(
                make_has_rich_long_pions(),
                pvs=pvs,
                pt_min=0.4 * GeV,
                p_min=1.0 * GeV,
                mipchi2_min=9,
            ),
            dll_type="k",
            dll_lim=-2,
            min=False,
        ),
        ghostprob_max=1e9,
    )

    lambdacs = make_lcs(l0lls, pions_lc, pvs)

    return Hlt2Line(
        name=name,
        algs=flt.pid_prefilters() + [l0lls, lambdacs],
        prescale=prescale,
        persistreco=True,
    )
