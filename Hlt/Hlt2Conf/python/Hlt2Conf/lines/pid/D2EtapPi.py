###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Registration of low-PT rad line
D(+) -> eta' (-> rho0  gamma) pi+
Run2 TurCal ref: https://cds.cern.ch/record/2764341/files/LHCb-INT-2021-002.pdf?
Table 9
author: Biplab Dey, Debashis Sahoo
date: 14.05.2024
"""

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.event_filters import require_pvs
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction

from Hlt2Conf.lines.pid.utils.neutral_pid import make_d2etappi

all_lines = {}


@register_line_builder(all_lines)
def D2EtapPi_line(name="Hlt2PID_D2EtapPi", prescale=1.0):
    pvs = make_pvs()
    d = make_d2etappi()

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), d],
        prescale=prescale,
        persistreco=True,
        calo_clusters=True,
        calo_digits=True,
    )
