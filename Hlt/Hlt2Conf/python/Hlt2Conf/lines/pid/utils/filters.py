###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV
from PyConf import configurable
from RecoConf.algorithms_thor import ParticleFilter
from RecoConf.event_filters import require_gec, require_pvs
from RecoConf.reconstruction_objects import (
    make_pvs,
    upfront_reconstruction,
)


def pid_prefilters(require_GEC=False):
    gec = [require_gec()] if require_GEC else []
    return (
        upfront_reconstruction()
        + gec
        + [
            require_pvs(make_pvs()),
        ]
    )


@configurable
def filter_particles(
    particles,
    pvs,
    pt_min=0.4 * GeV,
    p_min=1.0 * GeV,
    trchi2_max=99999,
    mipchi2_min=9,
):
    code = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.CHI2DOF < trchi2_max,
        F.OWNPVIPCHI2 > mipchi2_min,
    )
    return ParticleFilter(particles, F.FILTER(code))


@configurable
def filter_particles_ghostprob_max(particles, ghostprob_max):
    code = F.require_all(F.GHOSTPROB < ghostprob_max)
    return ParticleFilter(particles, F.FILTER(code))


@configurable
def filter_particles_dll(particles, dll_lim, dll_type, min):
    assert dll_type in ("mu", "e", "k", "pi", "p")
    code = None

    if min:
        if dll_type == "mu":
            code = F.require_all(F.PID_MU > dll_lim)
        if dll_type == "e":
            code = F.require_all(F.PID_E > dll_lim)
        if dll_type == "k":
            code = F.require_all(F.PID_K > dll_lim)
        if dll_type == "pi":
            code = F.require_all(F.PID_PI > dll_lim)
        if dll_type == "p":
            code = F.require_all(F.PID_P > dll_lim)
    else:
        if dll_type == "mu":
            code = F.require_all(F.PID_MU < dll_lim)
        if dll_type == "e":
            code = F.require_all(F.PID_E < dll_lim)
        if dll_type == "k":
            code = F.require_all(F.PID_K < dll_lim)
        if dll_type == "pi":
            code = F.require_all(F.PID_PI < dll_lim)
        if dll_type == "p":
            code = F.require_all(F.PID_P < dll_lim)

    return ParticleFilter(particles, F.FILTER(code))


@configurable
def filter_long_particles(particles, pvs, trchi2_max=99999, mipchi2_min=36):
    code = F.require_all(F.CHI2DOF < trchi2_max, F.OWNPVIPCHI2 > mipchi2_min)
    return ParticleFilter(particles, F.FILTER(code))


def filter_long_protons(
    particles, pvs, p_min=2.0 * GeV, pt_min=0 * GeV, pt_max=6 * GeV, pvipchi2_min=15
):
    code = F.require_all(
        F.P > p_min, in_range(pt_min, F.PT, pt_max), F.OWNPVIPCHI2 > pvipchi2_min
    )
    return ParticleFilter(particles, F.FILTER(code))


@configurable
def filter_down_particles(
    particles, p_min=3.0 * GeV, pt_min=0.175 * GeV, trchi2_max=99999
):
    code = F.require_all(F.P > p_min, F.PT > pt_min, F.CHI2DOF < trchi2_max)
    return ParticleFilter(particles, F.FILTER(code))


def filter_down_protons(
    particles,
    pvs,
    p_min=2.0 * GeV,
    pt_min=0 * GeV,
    pt_max=10 * GeV,
    pvipchi2_min=4.0,
):
    code = F.require_all(
        F.P > p_min, in_range(pt_min, F.PT, pt_max), F.OWNPVIPCHI2 > pvipchi2_min
    )
    return ParticleFilter(particles, F.FILTER(code))
