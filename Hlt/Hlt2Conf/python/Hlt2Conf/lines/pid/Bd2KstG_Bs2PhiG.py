###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Registration of high-PT photon radiative lines
Bs -> phi(-> KK) gamma
Bd -> Kst(-> pi K) gamma
author: Biplab Dey, Debashis Sahoo
date: 14.05.24
"""

import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from RecoConf.algorithms_thor import ParticleFilter
from RecoConf.event_filters import require_pvs
from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction

from Hlt2Conf.lines.pid.utils.neutral_pid import (
    make_b2xgamma_excl,
    make_calib_detached_kstar0s,
    make_calib_detached_phis,
    make_exclusive_highpt_photons,
)

all_lines = {}


@register_line_builder(all_lines)
def bs_to_phig_line(name="Hlt2PID_Bs2PhiG", prescale=1.0):
    pvs = make_pvs()

    phis = make_calib_detached_phis(
        name="PID_BsToPhiG_Phi_{hash}",
        k_p_min=2.0 * GeV,
        k_pt_min=500.0 * MeV,
        k_ipchi2_min=55.0,
        k_pid=(F.PID_K > 0.0),
        phi_pt_min=1500.0 * MeV,
    )

    # +/-20 MeV mass window
    phi_filt_code = F.require_all(in_range(1000.0 * MeV, F.MASS, 1040.0 * MeV))
    phi_filt = ParticleFilter(
        phis, F.FILTER(phi_filt_code), name="filt_PID_BsToPhiG_Phi_{hash}"
    )

    photons = make_exclusive_highpt_photons()

    b_s0 = make_b2xgamma_excl(
        intermediate=phi_filt,
        photons=photons,
        pvs=pvs,
        descriptor="B_s0 -> phi(1020) gamma",
        comb_m_min=4100 * MeV,
        comb_m_max=6500 * MeV,
        pt_min=2000 * MeV,
        dira_min=0.9975,
        bpv_ipchi2_max=12,
        name="PID_BsToPhiGamma_Combiner_{hash}",
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), phi_filt, b_s0],
        prescale=prescale,
        persistreco=True,
        calo_clusters=True,
        calo_digits=True,
    )


@register_line_builder(all_lines)
def bd_to_kstg_line(name="Hlt2PID_BdToKstG", prescale=1.0):
    pvs = make_pvs()

    kst = make_calib_detached_kstar0s(
        name="PID_BdToKstG_Kst_{hash}",
        am_min=695.0 * MeV,
        am_max=1095.0 * MeV,
        pi_p_min=2.0 * GeV,
        pi_pt_min=500.0 * MeV,
        pi_ipchi2_min=55.0,
        pi_pid=(F.PID_K < -2.0),
        k_p_min=2.0 * GeV,
        k_pt_min=500.0 * MeV,
        k_ipchi2_min=55.0,
        k_pid=((F.PID_K > 1.0) & ((F.PID_K - F.PID_P) > -5.0)),
        kstar0_pt_min=1500.0 * MeV,
        adocachi2cut=30.0,
        vchi2pdof_max=15.0,
    )

    # +/-100 MeV mass window and K<->pi mis-ID cut
    kst_filt_code = F.require_all(
        in_range(795.0 * MeV, F.MASS, 995.0 * MeV),
        (F.CHILD(1, F.PID_K) - F.CHILD(2, F.PID_K)) > -5.0,
    )
    kst_filt = ParticleFilter(
        kst, F.FILTER(kst_filt_code), name="filt_PID_BdToKstG_Kst_{hash}"
    )

    photons = make_exclusive_highpt_photons()

    b0 = make_b2xgamma_excl(
        intermediate=kst_filt,
        photons=photons,
        pvs=pvs,
        descriptor="[B0 -> K*(892)0 gamma]cc",
        comb_m_min=4100 * MeV,
        comb_m_max=6500 * MeV,
        pt_min=3000 * MeV,
        dira_min=0.9995,
        bpv_ipchi2_max=9,
        name="PID_BdToKstGamma_Combiner_{hash}",
    )

    return Hlt2Line(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), kst_filt, b0],
        prescale=prescale,
        persistreco=True,
        calo_clusters=True,
        calo_digits=True,
    )
