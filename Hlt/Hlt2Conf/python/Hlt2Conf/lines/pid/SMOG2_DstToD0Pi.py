###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, picosecond
from Moore.config import register_line_builder
from Moore.lines import Hlt2Line
from PyConf import configurable
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.ift.builders.smog2_builders import make_smog2_prefilters
from Hlt2Conf.lines.ift.builders.smog2_charm_builders import (
    make_comb_criteria,
    make_Dst,
    make_particle_criteria,
)

turbo_lines, full_lines = {}, {}
all_lines = {}
PROCESS = "hlt2"

# ===================
#    PDG masses
# ===================
__MASS_D0 = 1864.83 * MeV
__MASS_DSTAR = 2010.26 * MeV

__MASSWin_D0 = 80 * MeV
__MASSWin_DSTAR = 75 * MeV


@register_line_builder(all_lines)
@configurable
def Dst2D0piline(name="Hlt2PID_SMOG2Dst2KPi", prescale=1, persistreco=True):
    pvs = make_pvs

    CriteriaKaon = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=None,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5,
    )
    CriteriaPion = make_particle_criteria(
        bpvipchi2=4,
        minPIDK=None,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=250 * MeV,
        minp=3 * GeV,
        maxtrchi2dof=5,
    )

    CriteriaSlowPion = make_particle_criteria(
        bpvipchi2=0,
        minPIDK=None,
        maxPIDK=None,
        minPIDp=None,
        minPIDpK=None,
        minpT=100 * MeV,
        minp=1.0 * GeV,
        maxtrchi2dof=5,
    )

    CriteriaParticlesD0 = [CriteriaKaon, CriteriaPion]

    CriteriaCombinationD0 = make_comb_criteria(
        mass=__MASS_D0,
        masswin=__MASSWin_D0,
        min_child_pt=800 * MeV,
        max_sdoca=0.2 * mm,
        vchi2pdof_max=16,
        min_bpvltime=0.5 * picosecond,
        min_mother_pt=400 * MeV,
    )

    CriteriaCombinationDstar = make_comb_criteria(
        mass=__MASS_DSTAR,
        masswin=__MASSWin_DSTAR,
        min_child_pt=0 * MeV,
        max_sdoca=0.5 * mm,
        vchi2pdof_max=16,
    )

    Dst2D0pi = make_Dst(
        name="smog2_PID_DstToD0Pi_Combiner_{hash}",
        process="hlt2",
        pvs=pvs,
        CriteriaParticleD0=CriteriaParticlesD0,
        CriteriaCombinationD0=CriteriaCombinationD0,
        CriteriaSlowPion=CriteriaSlowPion,
        CriteriaCombinationDstar=CriteriaCombinationDstar,
    )

    return Hlt2Line(
        name=name,
        algs=make_smog2_prefilters(pvs=pvs) + [Dst2D0pi],
        prescale=prescale,
        persistreco=persistreco,
    )
