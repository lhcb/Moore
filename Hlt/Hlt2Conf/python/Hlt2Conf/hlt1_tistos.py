###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from PyConf.Algorithms import Hlt1TisTosAlg
from PyConf.reading import get_decreports, get_hlt1_selreports
from PyConf.Tools import ParticleTisTos
from RecoConf.algorithms_thor import ParticleFilter


def get_hlt1_tistos_relations(*, trigger_lines, data, advanced_tistos_arguments={}):
    # Get the selection and decision reports
    hlt1_sel_report = get_hlt1_selreports()
    hlt1_dec = get_decreports("Hlt1")

    particletistos = ParticleTisTos(**advanced_tistos_arguments)

    if len([l for l in trigger_lines if l[0:4] != "Hlt1"]):
        print("Not all of your trigger lines start with 'Hlt1'! (possible typo?)")

    return Hlt1TisTosAlg(
        name="Hlt1TisTosAlg_{hash}",
        InputParticles=data,
        SelReports=hlt1_sel_report,
        DecReports=hlt1_dec,
        TriggerLines=trigger_lines,
        ParticleTisTosTool=particletistos,
    ).P2TisTosTable


def hlt1_tos_on_any_filter(
    *, hlt1_trigger_lines, data, name=None, advanced_tistos_arguments={}
) -> ParticleFilter:
    tis_tos_table = get_hlt1_tistos_relations(
        trigger_lines=hlt1_trigger_lines,
        data=data,
        advanced_tistos_arguments=advanced_tistos_arguments,
    )

    tos_functors = [(F.IS_TOS(line, tis_tos_table) == 1) for line in hlt1_trigger_lines]
    tos_combination = F.require_any(*tos_functors)

    if not name:
        name = "Hlt1TOSFilter_{hash}"

    return ParticleFilter(data, F.FILTER(tos_combination), name=name)


def hlt1_tis_on_any_filter(
    *, hlt1_trigger_lines, data, name=None, advanced_tistos_arguments={}
) -> ParticleFilter:
    tis_tos_table = get_hlt1_tistos_relations(
        trigger_lines=hlt1_trigger_lines,
        data=data,
        advanced_tistos_arguments=advanced_tistos_arguments,
    )

    tis_functors = [(F.IS_TIS(line, tis_tos_table) == 1) for line in hlt1_trigger_lines]
    tis_combination = F.require_any(*tis_functors)

    if not name:
        name = "Hlt1TISFilter_{hash}"

    return ParticleFilter(data, F.FILTER(tis_combination), name=name)


def hlt1_tis_or_tos_on_any_filter(
    *, hlt1_trigger_lines, data, name=None, advanced_tistos_arguments={}
) -> ParticleFilter:
    tis_tos_table = get_hlt1_tistos_relations(
        trigger_lines=hlt1_trigger_lines,
        data=data,
        advanced_tistos_arguments=advanced_tistos_arguments,
    )

    hlt1_functors = [
        (F.IS_TIS(line, tis_tos_table) == 1) for line in hlt1_trigger_lines
    ]
    hlt1_functors += [
        (F.IS_TOS(line, tis_tos_table) == 1) for line in hlt1_trigger_lines
    ]
    hlt1_combination = F.require_any(*hlt1_functors)

    if not name:
        name = "Hlt1TISORTOSFilter_{hash}"

    return ParticleFilter(data, F.FILTER(hlt1_combination), name=name)
