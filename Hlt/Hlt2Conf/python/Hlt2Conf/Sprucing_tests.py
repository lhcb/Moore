###############################################################################
# (c) Copyright 2022-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import Options, run_moore
from Moore.config import filter_lines as remove_lines
from Moore.lines import PassLine, SpruceLine
from Moore.persistence import persist_line_outputs
from Moore.persistence.hlt2_tistos import list_of_full_stream_lines
from Moore.production import turbo_spruce
from Moore.streams import Stream, Streams
from PyConf.application import create_or_reuse_rootIOAlg, metainfo_repos
from RecoConf.global_tools import stateProvider_with_simplified_geom

from Hlt2Conf.lines import sprucing_lines
from Hlt2Conf.lines.test import spruce_test

public_tools = [stateProvider_with_simplified_geom()]

# Remove SMOG2 dimuon tracking efficiency lines which do not currently pass sprucing tests due to incompatible HLT2-filtered test file
to_remove = [
    "SpruceIFT_SMOG2TrackEff_DiMuon_SeedMuon_mum_Match",
    "SpruceIFT_SMOG2TrackEff_DiMuon_SeedMuon_mum_Tag",
    "SpruceIFT_SMOG2TrackEff_DiMuon_SeedMuon_mup_Match",
    "SpruceIFT_SMOG2TrackEff_DiMuon_SeedMuon_mup_Tag",
    "SpruceIFT_SMOG2TrackEff_DiMuon_VeloMuon_mum_Match",
    "SpruceIFT_SMOG2TrackEff_DiMuon_VeloMuon_mum_Tag",
    "SpruceIFT_SMOG2TrackEff_DiMuon_VeloMuon_mup_Match",
    "SpruceIFT_SMOG2TrackEff_DiMuon_VeloMuon_mup_Tag",
]


def spruce_streaming(options: Options):
    """
    Test streaming of sprucing lines. Produces two spruce_streaming.dst files prepended by stream name. RawBankSP functionality is tested via stream B.
    """

    def make_streams():
        stream_A = Stream(
            name="test_stream_A",
            lines=[spruce_test.Test_sprucing_line(name="Spruce_test_stream_A_line")],
            detectors=["VP", "UT", "FT", "Rich", "Muon", "Calo"],
        )

        stream_B = Stream(
            name="test_stream_B",
            lines=[
                spruce_test.Test_sprucing_rawbank_sp_line(
                    name="Spruce_test_stream_B_line"
                )
            ],
            detectors=["VP", "UT", "Muon", "Calo"],
        )

        return Streams(streams=[stream_A, stream_B])

    config = run_moore(options, make_streams, public_tools)
    return config


def spruce_all_lines_realtime(options: Options):
    """Test running all Sprucing lines

    Run over HLT1 filtered Min bias sample that has been processed by HLT2 lines.
    and then in the readback application  configure the GitANNSvc to use the branch key-ac5f2a28
    """

    metainfo_repos.global_bind(extra_central_tags=["key-adb09877"])

    trunc_lines = sprucing_lines

    for remove in to_remove:
        trunc_lines = remove_lines(trunc_lines, remove)

    print("Removed lines: ", sprucing_lines.keys() - trunc_lines.keys())

    def make_lines():
        return [builder() for builder in trunc_lines.values()]

    config = run_moore(options, make_lines, public_tools)
    return config


def spruce_example_realtime(options: Options):
    """
    Test running Sprucing line on output of topo{2,3} persistreco hlt2 lines (original reco real time). Produces spruce_example_realtimereco.dst
    """

    def make_streams():
        return Streams(
            streams=[
                Stream(
                    name="test",
                    lines=[spruce_test.Test_sprucing_line(name="Spruce_Test_line")],
                    detectors=["Rich"],
                )
            ]
        )

    config = run_moore(options, make_streams, public_tools)
    return config


def spruce_example_realtime_neutral(options: Options):
    """
    Test running a neutral Sprucing line on output of topo{2,3} persistreco hlt2 lines (original reco real time). Produces spruce_example_realtimereco_neutral.dst
    """

    def make_streams():
        return Streams(
            streams=[
                Stream(
                    name="test",
                    lines=[
                        spruce_test.Test_sprucing_BdToKstGamma(
                            name="SpruceTest_BdToKstGamma"
                        )
                    ],
                    detectors=["Rich"],
                )
            ]
        )

    config = run_moore(options, make_streams, public_tools)
    return config


def spruce_sp_passthrough(options: Options):
    # As unpackers are expecting the I/O to be setup and nobody does it,
    # we need to precreate RootIOAlg here
    create_or_reuse_rootIOAlg(options)

    def make_streams():
        return Streams(
            streams=[
                Stream(
                    name="test",
                    lines=[
                        PassLine(
                            "Passthrough", hlt2_filter_code="Hlt2Topo2BodyDecision"
                        )
                    ],
                    detectors=["Muon"],
                )
            ]
        )

    config = run_moore(options, make_streams, public_tools)
    return config


def spruce_passthrough(options: Options):
    def pass_through_line(name="Passthrough"):
        """Return a Sprucing line that performs no selection"""
        return PassLine(name=name)

    def make_lines():
        return [pass_through_line()]

    config = run_moore(options, make_lines, public_tools)
    return config


def spruce_hlt2filter(options: Options):
    """Test HLT2 filters on Sprucing lines. Runs on `hlt2_2or3bodytopo_realtime.mdf` from `hlt2_2or3bodytopo_realtime.py`"""

    def Sprucing_line_1(name="Spruce_filteronTopo2"):
        kst, b0 = spruce_test.b2kstgamma_line()

        return SpruceLine(
            name=name,
            algs=spruce_test.SpruceTest_prefilter() + [kst, b0],
            # hlt1_filter_code="HLT_PASS_RE('Hlt1GECPassthroughDecision')",
            hlt2_filter_code="Hlt2Topo2BodyDecision",
        )

    def Sprucing_line_2(name="Spruce_filteronTopo3"):
        kst, b0 = spruce_test.b2kstgamma_line()

        return SpruceLine(
            name=name,
            algs=spruce_test.SpruceTest_prefilter() + [kst, b0],
            # hlt1_filter_code="HLT_PASS_RE('Hlt1PassthroughDecision')",
            hlt2_filter_code="Hlt2Topo3BodyDecision",
        )

    def make_lines():
        return [Sprucing_line_1(), Sprucing_line_2()]

    config = run_moore(options, make_lines, public_tools)
    return config


def spruce_example_realtime_extraoutputs(options: Options):
    """
    Test Sprucing line with `extra_outputs` on output of topo{2,3} persistreco hlt2 lines (original reco real time). Produces spruce_realtimereco_extraoutputs.dst
    """

    def make_lines():
        return [
            spruce_test.Test_extraoutputs_sprucing_line(
                name="Spruce_Test_line_extraoutputs"
            ),
        ]

    config = run_moore(options, make_lines, public_tools)
    return config


def spruce_example_realtime_persistreco(options: Options):
    """Test Sprucing line with `persistreco` on output of topo{2,3} persistreco hlt2 lines (original reco real time). Produces spruce_realtimereco_persistreco.dst"""

    def make_lines():
        return [
            spruce_test.Test_persistreco_sprucing_line(
                name="Spruce_Test_line_persistreco"
            )
        ]

    config = run_moore(options, make_lines, public_tools)
    return config


def spruce_all_lines_realtime_test_old_json(options: Options):
    """Test running all Sprucing lines on topo{2,3} persistreco hlt2 output (use real time reco). Produces spruce_all_lines_realtimereco_newPacking.dst"""

    trunc_lines = sprucing_lines

    for remove in to_remove:
        trunc_lines = remove_lines(trunc_lines, remove)

    print("Removed lines: ", sprucing_lines.keys() - trunc_lines.keys())

    def make_lines():
        return [builder() for builder in trunc_lines.values()]

    ##Old file has the "Line" suffix on lines
    with (
        persist_line_outputs.bind(allow_missing_containers=True),
        list_of_full_stream_lines.bind(
            lines=["Hlt2Topo2BodyLine", "Hlt2Topo3BodyLine"]
        ),
    ):
        return run_moore(options, make_lines, public_tools)


def spruce_test_lines(options: Options):
    """
    Used for integration test

    Runs only the lines:
     - Test_sprucing_line
     - Test_extraoutputs_sprucing_line
     - Test_persistreco_sprucing_line
    """

    def make_lines():
        return [
            spruce_test.Test_sprucing_line(name="SpruceTest_SpruceTest"),
            spruce_test.Test_extraoutputs_sprucing_line(name="SpruceTest_ExtraOutputs"),
            spruce_test.Test_persistreco_sprucing_line(
                name="Spruce_Test_line_persistreco"
            ),
        ]

    config = run_moore(options, make_lines, public_tools)
    return config


def turbo_overlap(options: Options):
    """
    Test for the technical overlap of passthrough streams created in the sprucing-streaming step

    By technical overlap we mean where
    ...if an event fires `line_charm` and `line_rd` then both the charm stream and rd stream end up with `/Event/line_charm/particles` and `/Event/line_rd/particles`

    This is separate to the physics overlap caused by lines selecting similar physics

    This test shows the overlap using PassLine on a data file in dpa eos space
    """

    def make_streams():
        stream_A = Stream(
            name="topo2",
            lines=[PassLine("Pass_topo2", hlt2_filter_code="Hlt2Topo2BodyDecision")],
        )

        stream_B = Stream(
            name="topo3",
            lines=[PassLine("Pass_topo3", hlt2_filter_code="Hlt2Topo3BodyDecision")],
        )

        return Streams(streams=[stream_A, stream_B])

    config = run_moore(options, make_streams, public_tools)
    return config


def spruce_overlap(options: Options):
    import json

    """
    Test for the technical overlap of passthrough streams created in the sprucing-streaming step

    By technical overlap we mean where
    ...if an event fires `line_charm` and `line_rd` then both the charm stream and rd stream end up with `/Event/line_charm/particles` and `/Event/line_rd/particles`

    This is separate to the physics overlap caused by lines selecting similar physics

    This test shows how to remove the overlap using SpruceLine based on the output of hlt2_foroverlapcheck.py
    """

    from pathlib import Path

    turbolinedict = Path(__file__).parent / "test_streaming_config.json"

    stream = "default"
    input_line_config = options.input_streams_attributes_file
    stream_config = turbolinedict

    # Open and merge the line_config and the streaming_config
    with open(stream_config, "r") as json_file:
        streaming_config = json.load(json_file)
    with open(input_line_config, "r") as json_file:
        line_config = json.load(json_file)[stream]

    for line, l_config in line_config.items():
        for stream, s_config in streaming_config.items():
            if line in s_config:
                l_config["stream"] = stream
                break
        else:
            raise Exception(f"Line {line} not found in any stream")

    assert sum(map(len, streaming_config.values())) == len(line_config.keys()), (
        f"There are {len(line_config.keys())} lines to be run but only {sum(map(len, streaming_config.values()))} lines in the streams config"
    )

    print(f"line_config: {line_config}")
    custom_prescales = {"Hlt2Linetwo": 0.5}

    def make_streams():
        return turbo_spruce(
            line_config, custom_prescales=custom_prescales, use_regex=False
        )

    with list_of_full_stream_lines.bind(lines=[]):
        config = run_moore(options, make_streams, public_tools=[])
        return config
