###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Construct topo 2-body combinations as tag in put to HltJetBuilder
"""

import Functors as F
import Functors.math as Fmath
from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond
from PyConf import configurable
from RecoConf.algorithms_thor import (
    ParticleCombiner,
    ParticleContainersMerger,
    ParticleFilter,
)
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    make_has_rich_long_kaons,
    make_KsDD,
    make_KsLL,
    make_LambdaDD,
    make_LambdaLL,
)


@configurable
def make_topo_kaons(
    particles,
    pvs,
    pt_min=200 * MeV,
    p_min=3000 * MeV,
    trchi2dof_max=3.0,
    ipchi2_min=4.0,
):
    """
    Filter kaons for input to Topo 2-body combinations.
    """
    code = F.require_all(
        F.PT > pt_min,
        F.P > p_min,
        F.CHI2DOF < trchi2dof_max,
        F.MINIPCHI2CUT(pvs, ipchi2_min),
    )
    return ParticleFilter(particles, F.FILTER(code))


@configurable
def make_topo_v0s(particles, pvs, bpvltime_min=0.0 * picosecond):
    """Filter V0s for input to Topo 2-body combinations.
    This is an unnecessary pass-through for any standard V0 with a
    displacement cut.
    """
    ## TODO:  Eliminate this step if it remains redundant
    code = F.require_all(F.BPVLTIME(pvs) > bpvltime_min)
    return ParticleFilter(particles, F.FILTER(code))


def make_topo_ksll():
    return make_topo_v0s(make_KsLL(), make_pvs())


def make_topo_ksdd():
    return make_topo_v0s(make_KsDD(), make_pvs())


def make_topo_lambdall():
    return make_topo_v0s(make_LambdaLL(), make_pvs())


def make_topo_lambdadd():
    return make_topo_v0s(make_LambdaDD(), make_pvs())


@configurable
def prepare_topo_particles(pvs):
    """
    Prepare inputs and 2-body combinations for making Topo candidates
    """
    kaons = make_topo_kaons(make_has_rich_long_kaons(), pvs)
    lambdall = make_topo_lambdall()
    lambdadd = make_topo_lambdadd()

    particles = {
        "K+": kaons,
        "K-": kaons,
        "KS0": make_topo_ksll(),
        "Lambda0": lambdall,
        "Lambda~0": lambdall,
        "KS0dd": make_topo_ksdd(),
        "Lambda0dd": lambdadd,
        "Lambda~0dd": lambdadd,
    }
    inputs = {}
    for pid in particles:
        descriptor = f"K*(892)0 -> K+ {pid}"
        inputs[descriptor] = [kaons, particles[pid]]
    for pid in particles.keys() - {"K+"}:
        descriptor = f"K*(892)0 -> K- {pid}"
        inputs[descriptor] = [kaons, particles[pid]]
    return inputs


@configurable
def make_topo_2body(
    apt_min=2000.0 * MeV,
    adocachi2_max=1000.0,
    am_max=10.0 * GeV,
    ipchi2_max_an=16.0,
    an_ipchi2_max_an=2,
    bpvvdchi2_min=16.0,
    bpveta_min=2.0,
    bpveta_max=5.0,
):
    """
    Make generic Topo 2-body candidates used as tagging input to jet builder.
    """
    pvs = make_pvs()
    combination_code = F.require_all(
        F.PT > apt_min,
        F.MASS() < am_max,
        F.MAXSDOCACHI2CUT(adocachi2_max),
        F.SUM(F.IS_ABS_ID("K+") & (F.BPVIPCHI2(pvs) < ipchi2_max_an))
        < an_ipchi2_max_an,
    )
    vertex_code = F.require_all(
        F.CHI2DOF() < adocachi2_max,
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
        Fmath.in_range(bpveta_min, F.BPVETA(pvs), bpveta_max),
    )

    inputs = prepare_topo_particles(pvs)
    outputs = []
    for descriptor, particles in inputs.items():
        decaydescriptor = descriptor.strip("dd")
        output = ParticleCombiner(
            Inputs=particles,
            DecayDescriptor=decaydescriptor,
            CombinationCut=combination_code,
            CompositeCut=vertex_code,
            name=descriptor + "_topo_2body",
        )
        outputs.append(output)

    outputs_merged = ParticleContainersMerger(outputs, name="topo_2body")
    return outputs_merged


@configurable
def make_topo_2body_with_svtag(
    apt_min=2000.0 * MeV,
    adocachi2_max=10.0,
    prod_ghostprob_max=0.2,
    prod_pt_min=500.0 * MeV,
    prod_ipchi2_min=16.0,
    am_max=10.0 * GeV,
    ipchi2_max_an=16.0,
    an_ipchi2_max_an=2,
    bpvvdchi2_min=25.0,
    bpveta_min=2.0,
    bpveta_max=5.0,
):
    """
    Make generic Topo 2-body candidates used as tagging input to jet builder,
    including cuts to do SV tag
    """
    pvs = make_pvs()
    combination_code = F.require_all(
        F.PT > apt_min,
        F.MASS() < am_max,
        F.MAXSDOCACHI2CUT(adocachi2_max),
        F.SUM(F.IS_ABS_ID("K+") & (F.BPVIPCHI2(pvs) < ipchi2_max_an))
        < an_ipchi2_max_an,
    )
    vertex_code = F.require_all(
        F.MINTREE(F.IS_ABS_ID("K+"), F.GHOSTPROB()) < prod_ghostprob_max,
        F.MINTREE(F.IS_ABS_ID("K+"), F.MINIPCHI2(pvs)) > prod_ipchi2_min,
        F.MINTREE(F.ALL, F.PT) > prod_pt_min,
        F.CHI2DOF() < adocachi2_max,
        Fmath.in_range(bpveta_min, F.BPVETA(pvs), bpveta_max),
        F.BPVFDCHI2(pvs) > bpvvdchi2_min,
    )

    inputs = prepare_topo_particles(pvs)
    outputs = []
    for descriptor, particles in inputs.items():
        decaydescriptor = descriptor.strip("dd")
        output = ParticleCombiner(
            Inputs=particles,
            DecayDescriptor=decaydescriptor,
            CombinationCut=combination_code,
            CompositeCut=vertex_code,
            name=descriptor + "_topo_2body_SVtag",
        )
        outputs.append(output)

    outputs_merged = ParticleContainersMerger(outputs, name="topo_2body_with_svtag")
    return outputs_merged


"""Maker functions for jet definitions common across HLT2.
"""
from math import pi as M_PI

from PyConf import configurable
from PyConf.Algorithms import (
    FastJetBuilder,
    JetTag,
    ParticleFlowFilter,
    ParticleFlowMaker,
    ParticleMakerForParticleFlow,
)
from RecoConf.reconstruction_objects import (
    make_charged_protoparticles as _make_charged_protoparticles,
)
from RecoConf.standard_particles import (
    get_all_track_selector,
    make_merged_pi0s,
    make_photons,
    standard_protoparticle_filter,
)


@configurable
def make_charged_particles(
    make_protoparticles=_make_charged_protoparticles,
    track_type="Long",
    get_track_selector=get_all_track_selector,
    make_protoparticle_filter=standard_protoparticle_filter,
    c_over_e_cut=0,
):
    chargedProtos = make_protoparticles(track_type=track_type)
    return ParticleMakerForParticleFlow(
        InputProtoParticles=chargedProtos,
        TrackPredicate=get_track_selector(),
        c_over_e_cut=c_over_e_cut,
        ProtoParticlePredicate=make_protoparticle_filter(),
        PrimaryVertices=make_pvs(),
    ).Output


@configurable
def jet_tag_maker(tag_key):
    return {
        "TOPO": make_topo_2body,
        "SV": make_topo_2body_with_svtag,
    }[tag_key]


@configurable
def tag_jets(jets, tags, useflightdirection=False, name="TagJets_{hash}"):
    tag = jet_tag_maker(tags)()
    return JetTag(
        Jets=jets,
        Tags=tag,
        UseFlightDirection=useflightdirection,
        PVLocation=make_pvs(),
        name=name,
    ).Output


@configurable
def build_jets(
    pflow, JetsByVtx=False, MCJets=False, applyJEC=False, name="JetBuilder_{hash}"
):
    jetEcFilePath = ""  # Null jetEcFilePath: Don't apply JEC
    if applyJEC:
        jetEcFilePath = "paramfile://data/JetEnergyCorrections_R05_hlt_Run2.root"
    return FastJetBuilder(
        Input=pflow,
        PVLocation=make_pvs(),
        PtMin=5000,  #  FastJet: min pT
        RParameter=0.5,  #  FastJet: cone size
        Sort="by_pt",  #  FastJet: sort by pt
        Strategy="Best",  #  FastJet: strategy:
        Type="antikt_algorithm",  #  FastJet: JetAlgortihm
        Recombination="E_scheme",  #  FastJet: RecombinationScheme
        JetID=98,  # LHCb: Jet PID number
        JetsByVtx=JetsByVtx,
        MCJets=MCJets,  # Reconstruct jets from particles from MCParticles
        jetEcFilePath=jetEcFilePath,
        jecLimNPvs=[0, 1],
        jecLimEta=[2.0, 2.2, 2.3, 2.4, 2.6, 2.8, 3.0, 3.2, 3.6, 4.2, 4.5],
        jecLimCpf=[0.06, 0.3, 0.4, 0.5, 0.6, 0.8, 1.0001],
        jecLimPhi=[0, 1.0 / 6.0 * M_PI, 1.0 / 3.0 * M_PI, 0.5 * M_PI],
        jecLimPt=[5, 298],
        jetEcShift=0.0,
        name=name,
    ).Output


@configurable
def make_trackjets(
    name="TrackJets",
    pt_min=10 * GeV,
    JetsByVtx=True,
    tags=None,
    useflightdirectionfortag=False,
):
    pflowtracks = make_onlytrack_particleflow()
    jets = build_jets(pflowtracks, JetsByVtx, name="JetBuilder" + name)

    if tags is not None:
        taggedjets = tag_jets(
            jets,
            tags,
            useflightdirection=useflightdirectionfortag,
            name="TagJet" + name,
        )
        jets = taggedjets

    code = F.require_all(F.IS_ABS_ID("CELLjet"), F.PT > pt_min)
    return ParticleFilter(jets, F.FILTER(code), name=name)


@configurable
def make_jets(name="SimpleJets_{hash}", pt_min=10 * GeV, JetsByVtx=True, tags=None):
    pflow = make_particleflow()
    jets = build_jets(pflow, JetsByVtx, name="JetBuilder" + name)

    if tags is not None:
        taggedjets = tag_jets(jets, tags, useflightdirection=True, name="Tags" + name)
        jets = taggedjets

    code = F.require_all(
        F.IS_ABS_ID("CELLjet"), F.PT > pt_min, F.NINGENERATION(F.CHARGE != 0, 1) > 0
    )
    return ParticleFilter(jets, F.FILTER(code), name=name)


@configurable
def make_dijets(
    tagpair=(None, None), min_dijet_mass=0 * GeV, prod_pt_min=10 * GeV, min_dphi=0.0
):
    """Make two-jet combinations"""
    jet1 = make_jets(pt_min=prod_pt_min, tags=tagpair[0])
    jet2 = make_jets(pt_min=prod_pt_min, tags=tagpair[1])

    if min_dphi > 0.0:
        delta = F.ADJUST_ANGLE @ (F.CHILD(1, F.PHI) - F.CHILD(2, F.PHI))
        combination_code = F.require_all(
            F.MASS > min_dijet_mass,
            F.ABS @ delta > min_dphi,
            F.CHILD(1, F.OWNPV) == F.CHILD(2, F.OWNPV),
        )

    else:
        combination_code = F.require_all(
            F.MASS > min_dijet_mass, F.CHILD(1, F.OWNPV) == F.CHILD(2, F.OWNPV)
        )

    return ParticleCombiner(
        Inputs=[jet1, jet2],
        DecayDescriptor="CLUSjet -> CELLjet CELLjet",
        CombinationCut=combination_code,
        CompositeCut=F.ALL,
        AllowDiffInputsForSameIDChildren=True,
        ParticleCombiner="ParticleAdder",
    )


@configurable
def make_Trijets(tags=(None, None, None), prod_pt_min=10 * GeV):
    """Make three-jet combinations"""
    jets0 = make_jets(pt_min=prod_pt_min, tags=tags[0])
    jets1 = make_jets(pt_min=prod_pt_min, tags=tags[1])
    jets2 = make_jets(pt_min=prod_pt_min, tags=tags[2])
    tagfilt0 = F.ALL
    tagfilt1 = F.ALL
    tagfilt2 = F.ALL

    combination_code = F.require_all(tagfilt0, tagfilt1, tagfilt2)

    return ParticleCombiner(
        Inputs=[jets0, jets1, jets2],
        DecayDescriptor="CLUSjet -> CELLjet CELLjet CELLjet",
        CombinationCut=combination_code,
        CompositeCut=F.ALL,
        AllowDiffInputsForSameIDChildren=True,
        ParticleCombiner="ParticleAdder",
    )


@configurable
def particleflow_filter(pflow, toban, name="PFFilter"):
    return ParticleFlowFilter(Inputs=pflow, ParticlesToBan=toban, name=name).Output


@configurable
def make_onlytrack_particleflow(name="PFTracksOnly_{hash}"):
    return ParticleFlowMaker(
        Inputs=[
            make_charged_particles(track_type="Long"),
            make_charged_particles(track_type="Downstream"),
        ],
        name=name,
    ).Output


@configurable
def make_particleflow(name="PF_{hash}"):
    return ParticleFlowMaker(
        Inputs=[
            make_charged_particles(track_type="Long"),
            make_charged_particles(track_type="Downstream"),
            make_photons(),
            make_merged_pi0s(),
        ],
        name=name,
    ).Output
