###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import Functors as F
from GaudiKernel.SystemOfUnits import GeV
from PyConf.Algorithms import (
    AdvancedCloneKiller,
    FlavourTagsMerger,
    Run2OSElectronTagger,
    Run2OSKaonTagger,
    Run2OSMuonTagger,
    Run2OSVertexChargeTagger,
    Run2SSKaonTagger,
    Run2SSPionTagger,
    Run2SSProtonTagger,
)
from PyConf.reading import get_particles
from RecoConf.algorithms_thor import ParticleFilter
from RecoConf.reconstruction_objects import make_pvs
from RecoConf.standard_particles import (
    make_has_rich_long_kaons,
    make_has_rich_long_pions,
    make_has_rich_long_protons,
    make_has_rich_up_kaons,
    make_has_rich_up_pions,
    make_has_rich_up_protons,
    make_ismuon_long_muon,
    make_long_electrons_with_brem,
)

""" cuts from BTaggingTool applied to every particles type:
  Gaudi::Property<double> m_cutTagPart_MinTheta{this, "CutTagPart_MinTheta", 0.012,
                                                "Tagging particle requirement: Minimum theta angle"};
    change into eta cause eta functor exists
    eta = - ln ( tan (theta/2 ) ) ==> maxEta = 5.12

  Gaudi::Property<double> m_cutTagPart_MaxGhostProb{this, "CutTagPart_MaxGhostProb", 0.5,
                                                    "Tagging particle requirement: Maximum ghost probability"};

  Gaudi::Property<double> m_cutTagPart_MinP{this, "CutTagPart_MinP", 2,
                                            "Tagging particle requirement: Minimum P (in GeV)"};

  Gaudi::Property<double> m_cutTagPart_MaxP{this, "CutTagPart_MaxP", 200,
                                            "Tagging particle requirement: Maximum P (in GeV)"};

  Gaudi::Property<double> m_cutTagPart_MaxPT{this, "CutTagPart_MaxPT", 10,
                                             "Tagging particle requirement: Maximum PT (in GeV)"};

  doesn't currently exist
  Gaudi::Property<double> m_cutTagPart_MinCloneDist{this, "CutTagPart_MinCloneDist", 5000,
                                                    "Tagging particle requirement: Minimum Track::CloneDist"};
"""


def _make_particles_with_selection(particle_container, selection_code):
    if isinstance(particle_container, str):
        particle_container = get_particles(particle_container)
        return ParticleFilter(Input=particle_container, Cut=F.FILTER(selection_code))
    else:
        return ParticleFilter(Input=particle_container(), Cut=F.FILTER(selection_code))


def make_run2_sameside_tagging_pions(
    tagging_particle_containers=[make_has_rich_long_pions, make_has_rich_up_pions],
    pvs=make_pvs,
    p_min=2 * GeV,
    p_max=200 * GeV,
    pt_min=0.4 * GeV,
    pt_max=10 * GeV,
    eta_max=5.12,
    pidp_max=5,
    pidk_max=5,
    ghostprob_max=0.5,
):
    selection_code = F.require_all(
        F.P > p_min,
        F.P < p_max,
        F.PT > pt_min,
        F.PT < pt_max,
        F.PID_P < pidp_max,
        F.PID_K < pidk_max,
        F.GHOSTPROB < ghostprob_max,
        F.ETA < eta_max,
    )
    selected_tagging_particle_containers = []
    for tagging_particle_container in tagging_particle_containers:
        selected_tagging_particle_containers.append(
            _make_particles_with_selection(tagging_particle_container, selection_code)
        )
    return AdvancedCloneKiller(InputParticles=selected_tagging_particle_containers)


def make_run2_sameside_tagging_kaons(
    tagging_particle_containers=[make_has_rich_long_kaons, make_has_rich_up_kaons],
    pvs=make_pvs,
    p_min=2 * GeV,
    p_max=200 * GeV,
    pt_max=10 * GeV,
    eta_max=5.12,
    ghostprob_max=0.5,
):
    selection_code = F.require_all(
        F.P > p_min,
        F.P < p_max,
        F.PT < pt_max,
        F.GHOSTPROB < ghostprob_max,
        F.ETA < eta_max,
    )
    selected_tagging_particle_containers = []
    for tagging_particle_container in tagging_particle_containers:
        selected_tagging_particle_containers.append(
            _make_particles_with_selection(tagging_particle_container, selection_code)
        )
    return AdvancedCloneKiller(InputParticles=selected_tagging_particle_containers)


def make_run2_sameside_tagging_protons(
    tagging_particle_containers=[make_has_rich_long_protons, make_has_rich_up_protons],
    pvs=make_pvs,
    p_min=2 * GeV,
    p_max=200 * GeV,
    pt_min=0.4 * GeV,
    pt_max=10 * GeV,
    eta_max=5.12,
    pidp_min=5,
    ghostprob_max=0.5,
):
    selection_code = F.require_all(
        F.P > p_min,
        F.P < p_max,
        F.PT > pt_min,
        F.PT < pt_max,
        F.PID_P > pidp_min,
        F.GHOSTPROB < ghostprob_max,
        F.ETA < eta_max,
    )
    selected_tagging_particle_containers = []
    for tagging_particle_container in tagging_particle_containers:
        selected_tagging_particle_containers.append(
            _make_particles_with_selection(tagging_particle_container, selection_code)
        )
    return AdvancedCloneKiller(InputParticles=selected_tagging_particle_containers)


def make_run2_oppositeside_tagging_kaons(
    tagging_particle_containers=[make_has_rich_long_kaons, make_has_rich_up_kaons],
    pvs=make_pvs,
    p_min=5 * GeV,
    p_max=200 * GeV,
    pt_max=10 * GeV,
    eta_max=5.12,
    ghostprob_max=0.52,
    trchi2dof_max=3.0,
):
    selection_code = F.require_all(
        F.P > p_min,
        F.P < p_max,
        F.PT < pt_max,
        F.GHOSTPROB < ghostprob_max,
        F.ETA < eta_max,
        F.CHI2DOF < trchi2dof_max,
    )
    selected_tagging_particle_containers = []
    for tagging_particle_container in tagging_particle_containers:
        selected_tagging_particle_containers.append(
            _make_particles_with_selection(tagging_particle_container, selection_code)
        )
    return AdvancedCloneKiller(InputParticles=selected_tagging_particle_containers)


def make_run2_oppositeside_tagging_electrons(
    tagging_particle_containers=[make_long_electrons_with_brem],
    pvs=make_pvs,
    p_min=5.035169513449979 * GeV,
    p_max=200 * GeV,
    pt_min=1.4033298418685027 * GeV,
    pt_max=10 * GeV,
    eta_max=5.12,
    ghostprob_max=0.5,
    trchi2dof_max=3.0,
    PIDe_min=4.33257736223062,
):
    selection_code = F.require_all(
        F.P > p_min,
        F.P < p_max,
        F.PT > pt_min,
        F.PT < pt_max,
        F.GHOSTPROB < ghostprob_max,
        F.ETA < eta_max,
        F.CHI2DOF < trchi2dof_max,
        F.PID_E > PIDe_min,
    )
    selected_tagging_particle_containers = []
    for tagging_particle_container in tagging_particle_containers:
        selected_tagging_particle_containers.append(
            _make_particles_with_selection(tagging_particle_container, selection_code)
        )
    return AdvancedCloneKiller(InputParticles=selected_tagging_particle_containers)


def make_run2_oppositeside_tagging_muons(
    tagging_particle_containers=[make_ismuon_long_muon],
    pvs=make_pvs,
    p_min=2.54 * GeV,
    p_max=200 * GeV,
    pt_min=0.951 * GeV,
    pt_max=10 * GeV,
    eta_max=5.12,
    ghostprob_max=0.5,
    trchi2dof_max=3.0,
):
    selection_code = F.require_all(
        F.P > p_min,
        F.P < p_max,
        F.PT > pt_min,
        F.PT < pt_max,
        F.GHOSTPROB < ghostprob_max,
        F.ETA < eta_max,
        F.CHI2DOF < trchi2dof_max,
    )
    selected_tagging_particle_containers = []
    for tagging_particle_container in tagging_particle_containers:
        selected_tagging_particle_containers.append(
            _make_particles_with_selection(tagging_particle_container, selection_code)
        )
    return AdvancedCloneKiller(InputParticles=selected_tagging_particle_containers)


def make_run2_oppositeside_vertex_charge_tagging_particles(
    tagging_particle_containers=[make_has_rich_long_pions, make_has_rich_up_pions],
    pvs=make_pvs,
    p_min=2 * GeV,
    p_max=200 * GeV,
    pt_max=10 * GeV,
    eta_max=5.12,
    ghostprob_max=0.5,
):
    selection_code = F.require_all(
        F.P > p_min,
        F.P < p_max,
        F.PT < pt_max,
        F.GHOSTPROB < ghostprob_max,
        F.ETA < eta_max,
    )
    selected_tagging_particle_containers = []
    for tagging_particle_container in tagging_particle_containers:
        selected_tagging_particle_containers.append(
            _make_particles_with_selection(tagging_particle_container, selection_code)
        )
    return AdvancedCloneKiller(InputParticles=selected_tagging_particle_containers)


def run2_sspion_tagger(b_candidates, tagging_particles_locations=[]):
    tagging_particle_containers = []

    if tagging_particles_locations:
        for tagging_particles_location in tagging_particles_locations:
            if tagging_particles_location == "":
                tagging_particle_containers.append(
                    get_particles(tagging_particles_location)
                )
            else:
                tagging_particle_containers.append(tagging_particles_location)
    else:
        tagging_particle_containers.append(make_has_rich_long_pions)
        tagging_particle_containers.append(make_has_rich_up_pions)

    ss_tagging_pions = make_run2_sameside_tagging_pions(
        tagging_particle_containers=tagging_particle_containers
    )
    v2_pvs = make_pvs()
    sspion_tagger = Run2SSPionTagger(
        BCandidates=b_candidates, TaggingPions=ss_tagging_pions, PrimaryVertices=v2_pvs
    )
    return sspion_tagger


def run2_sskaon_tagger(b_candidates, tagging_particles_locations=[]):
    tagging_particle_containers = []

    if tagging_particles_locations:
        for tagging_particles_location in tagging_particles_locations:
            if tagging_particles_location == "":
                tagging_particle_containers.append(
                    get_particles(tagging_particles_location)
                )
            else:
                tagging_particle_containers.append(tagging_particles_location)
    else:
        tagging_particle_containers.append(make_has_rich_long_kaons)
        tagging_particle_containers.append(make_has_rich_up_kaons)

    ss_tagging_kaons = make_run2_sameside_tagging_kaons(
        tagging_particle_containers=tagging_particle_containers
    )
    v2_pvs = make_pvs()
    sskaon_tagger = Run2SSKaonTagger(
        BCandidates=b_candidates, TaggingKaons=ss_tagging_kaons, PrimaryVertices=v2_pvs
    )
    return sskaon_tagger


def run2_ssproton_tagger(b_candidates, tagging_particles_locations=[]):
    tagging_particle_containers = []

    if tagging_particles_locations:
        for tagging_particles_location in tagging_particles_locations:
            if tagging_particles_location == "":
                tagging_particle_containers.append(
                    get_particles(tagging_particles_location)
                )
            else:
                tagging_particle_containers.append(tagging_particles_location)
    else:
        tagging_particle_containers.append(make_has_rich_long_protons)
        tagging_particle_containers.append(make_has_rich_up_protons)

    ss_tagging_protons = make_run2_sameside_tagging_protons(
        tagging_particle_containers=tagging_particle_containers
    )
    v2_pvs = make_pvs()
    ssproton_tagger = Run2SSProtonTagger(
        BCandidates=b_candidates,
        TaggingProtons=ss_tagging_protons,
        PrimaryVertices=v2_pvs,
    )
    return ssproton_tagger


def run2_oskaon_tagger(b_candidates, tagging_particles_locations=[]):
    tagging_particle_containers = []

    if tagging_particles_locations:
        for tagging_particles_location in tagging_particles_locations:
            if tagging_particles_location == "":
                tagging_particle_containers.append(
                    get_particles(tagging_particles_location)
                )
            else:
                tagging_particle_containers.append(tagging_particles_location)
    else:
        tagging_particle_containers.append(make_has_rich_long_kaons)
        tagging_particle_containers.append(make_has_rich_up_kaons)

    os_tagging_kaons = make_run2_oppositeside_tagging_kaons(
        tagging_particle_containers=tagging_particle_containers
    )
    v2_pvs = make_pvs()
    oskaon_tagger = Run2OSKaonTagger(
        BCandidates=b_candidates, TaggingKaons=os_tagging_kaons, PrimaryVertices=v2_pvs
    )
    return oskaon_tagger


def run2_oselectron_tagger(b_candidates, tagging_particles_locations=[]):
    tagging_particle_containers = []

    if tagging_particles_locations:
        for tagging_particles_location in tagging_particles_locations:
            if tagging_particles_location == "":
                tagging_particle_containers.append(
                    get_particles(tagging_particles_location)
                )
            else:
                tagging_particle_containers.append(tagging_particles_location)
    else:
        tagging_particle_containers.append(make_long_electrons_with_brem)

    os_tagging_electrons = make_run2_oppositeside_tagging_electrons(
        tagging_particle_containers=tagging_particle_containers
    )
    v2_pvs = make_pvs()
    oselectron_tagger = Run2OSElectronTagger(
        BCandidates=b_candidates,
        TaggingElectrons=os_tagging_electrons,
        PrimaryVertices=v2_pvs,
    )
    return oselectron_tagger


def run2_osmuon_tagger(b_candidates, tagging_particles_locations=[]):
    tagging_particle_containers = []

    if tagging_particles_locations:
        for tagging_particles_location in tagging_particles_locations:
            if tagging_particles_location == "":
                tagging_particle_containers.append(
                    get_particles(tagging_particles_location)
                )
            else:
                tagging_particle_containers.append(tagging_particles_location)
    else:
        tagging_particle_containers.append(make_ismuon_long_muon)

    os_tagging_muons = make_run2_oppositeside_tagging_muons(
        tagging_particle_containers=tagging_particle_containers
    )
    v2_pvs = make_pvs()
    osmuon_tagger = Run2OSMuonTagger(
        BCandidates=b_candidates, TaggingMuons=os_tagging_muons, PrimaryVertices=v2_pvs
    )
    return osmuon_tagger


def run2_osvertexcharge_tagger(b_candidates, tagging_particles_locations=[]):
    tagging_particle_containers = []

    if tagging_particles_locations:
        for tagging_particles_location in tagging_particles_locations:
            if tagging_particles_location == "":
                tagging_particle_containers.append(
                    get_particles(tagging_particles_location)
                )
            else:
                tagging_particle_containers.append(tagging_particles_location)
    else:
        tagging_particle_containers.append(make_has_rich_long_pions)
        tagging_particle_containers.append(make_has_rich_up_pions)

    os_tagging_particles = make_run2_oppositeside_vertex_charge_tagging_particles(
        tagging_particle_containers=tagging_particle_containers
    )
    v2_pvs = make_pvs()
    osvertexcharge_tagger = Run2OSVertexChargeTagger(
        BCandidates=b_candidates,
        TaggingParticles=os_tagging_particles,
        PrimaryVertices=v2_pvs,
    )
    return osvertexcharge_tagger


def run2_os_taggers(b_candidates, tagging_particles_locations={}):
    if tagging_particles_locations:
        os_kaon_tagger = run2_oskaon_tagger(
            b_candidates, tagging_particles_locations["OSKaons"]
        )
        os_muon_tagger = run2_osmuon_tagger(
            b_candidates, tagging_particles_locations["OSMuons"]
        )
        os_electron_tagger = run2_oselectron_tagger(
            b_candidates, tagging_particles_locations["OSElectrons"]
        )
        os_vertexcharge_tagger = run2_osvertexcharge_tagger(
            b_candidates, tagging_particles_locations["OSVertexChargeParticles"]
        )

    else:
        os_kaon_tagger = run2_oskaon_tagger(b_candidates)
        os_muon_tagger = run2_osmuon_tagger(b_candidates)
        os_electron_tagger = run2_oselectron_tagger(b_candidates)
        os_vertexcharge_tagger = run2_osvertexcharge_tagger(b_candidates)

    os_taggers = FlavourTagsMerger(
        FlavourTagsList=[
            os_kaon_tagger,
            os_muon_tagger,
            os_electron_tagger,
            os_vertexcharge_tagger,
        ]
    )
    return os_taggers


def run2_ss_taggers(b_candidates, tagging_particles_locations={}):
    if tagging_particles_locations:
        ss_kaon_tagger = run2_sskaon_tagger(
            b_candidates, tagging_particles_locations["SSKaons"]
        )
        ss_pion_tagger = run2_sspion_tagger(
            b_candidates, tagging_particles_locations["SSPions"]
        )
        ss_proton_tagger = run2_ssproton_tagger(
            b_candidates, tagging_particles_locations["SSProtons"]
        )
    else:
        ss_kaon_tagger = run2_sskaon_tagger(b_candidates)
        ss_pion_tagger = run2_sspion_tagger(b_candidates)
        ss_proton_tagger = run2_ssproton_tagger(b_candidates)

    ss_taggers = FlavourTagsMerger(
        FlavourTagsList=[ss_kaon_tagger, ss_pion_tagger, ss_proton_tagger]
    )
    return ss_taggers


def run2_all_taggers(b_candidates, tagging_particles_locations={}):
    os_taggers = run2_os_taggers(b_candidates, tagging_particles_locations)
    ss_taggers = run2_ss_taggers(b_candidates, tagging_particles_locations)
    all_taggers = FlavourTagsMerger(FlavourTagsList=[os_taggers, ss_taggers])
    return all_taggers
