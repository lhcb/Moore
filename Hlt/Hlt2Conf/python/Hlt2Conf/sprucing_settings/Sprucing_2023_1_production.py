###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## Options used for Sprucing 7-8th July 2023 data

from Hlt2Conf.lines import sprucing_lines
from Hlt2Conf.lines.b_to_charmonia import sprucing_lines as spruce_b2cc_lines
from Hlt2Conf.lines.b_to_open_charm import sprucing_lines as spruce_b2oc_lines
from Hlt2Conf.lines.bandq import sprucing_lines as spruce_bandq_lines
from Hlt2Conf.lines.bnoc import sprucing_lines as spruce_bnoc_lines
from Hlt2Conf.lines.qee import sprucing_lines as spruce_qee_lines
from Hlt2Conf.lines.rd import sprucing_lines as spruce_rd_lines
from Moore import Options, run_moore
from Moore.lines import PassLine
from Moore.persistence.hlt2_tistos import list_of_full_stream_lines
from Moore.stream_writers import allow_rawbank_same_source_id_only_for_bad_2023_data
from Moore.streams import DETECTORS, Stream, Streams
from RecoConf.global_tools import stateProvider_with_simplified_geom

public_tools = [stateProvider_with_simplified_geom()]

#######################################################
##Should only need to change the following dictionaries.

# Note that wg_lines are themselves dict types here
fulllinedict = {
    "b2oc": spruce_b2oc_lines,
    "bandq": spruce_bandq_lines,
    "b2cc": spruce_b2cc_lines,
    "rd": spruce_rd_lines,
    # No sl lines ran in FULL
    "qee": spruce_qee_lines,
    "bnoc": spruce_bnoc_lines,
    # No IFT Sprucing lines for 2023
}

turbolinedict = {
    "b2oc": ["Hlt2B2OC.*Decision"],
    "bandq": [
        "Hlt2BandQ.*Decision",
        "Hlt2_JpsiToMuMuDecision",
        "Hlt2_Psi2SToMuMuDecision",
        "Hlt2_DiMuonPsi2STightDecision",
        "Hlt2_DiMuonJPsiTightDecision",
    ],
    "b2cc": ["Hlt2B2CC.*Decision"],
    "sl": ["Hlt2SLB.*Decision"],
    "charm": ["Hlt2Charm.*Decision"],
    "qee": ["Hlt2QEE.*Decision", "Hlt2.*DiMuonNoIP.*Decision"],
    "rd": ["Hlt2RD.*Decision"],
    "bnoc": ["Hlt2BnoC.*Decision"],
}

# For Turcal make 2 sets of line dict which will create 6 streams
# The first 3 streams will contain all events but no det rawbanks
# The second 3 streams will have a 10% prescale but have all det rawbanks
turcallinedict = {
    "pid": ["Hlt2PID.*Decision"],
    "trackeff": ["Hlt2TrackEff.*Decision", "Hlt2HadInt.*Decision"],
    "monitoring": ["Hlt2Monitoring.*Decision"],
    "pid_raw": ["Hlt2PID.*Decision"],
    "trackeff_raw": ["Hlt2TrackEff.*Decision", "Hlt2HadInt.*Decision"],
    "monitoring_raw": ["Hlt2Monitoring.*Decision"],
}

from Hlt2Conf.sprucing_settings.fixed_line_configs import (
    lines_for_TISTOS_78July_2023 as lines_for_TISTOS,
)

#######################################################


def lines_running(linedict):
    """Return the full list of lines to be run"""

    return [
        item
        for sublist in [list(linedict[wg].keys()) for wg in linedict]
        for item in sublist
    ]


def lines_not_running(all_lines, lines_to_run):
    """Return the list of lines that are declared in the framework but that are not set to run"""
    return [item for item in list(all_lines) if item not in lines_to_run]


def excl_spruce_production(options: Options):
    spruce_b2oc_lines.pop("SpruceB2OC_BdToDsmK_DsmToHHH_FEST")

    lines_to_run = lines_running(fulllinedict)
    missing_lines = lines_not_running(sprucing_lines, lines_to_run)

    def make_streams():
        streams = [
            Stream(
                wg,
                lines=[builder() for builder in fulllinedict[wg].values()],
                detectors=[],
            )
            for wg in fulllinedict
        ]

        return Streams(streams=streams)

    print(f"Running {len(lines_to_run)} lines")
    for wg in fulllinedict.keys():
        lines = list(fulllinedict[wg].keys())
        print(f"Stream {wg} will contain the lines: {lines} \n")

    print(
        f"The following lines exist but are not appended to any stream : {missing_lines} \n end of missing lines."
    )
    with (
        list_of_full_stream_lines.bind(lines=lines_for_TISTOS),
        allow_rawbank_same_source_id_only_for_bad_2023_data.bind(allow=True),
    ):
        config = run_moore(options, make_streams, public_tools=[])
        return config


def pass_spruce_production(options: Options):
    def make_streams():
        streams = [
            Stream(
                wg,
                lines=[
                    PassLine(
                        name="Pass" + wg,
                        hlt2_filter_code=turbolinedict[wg],
                    )
                ],
                detectors=[],
            )
            for wg in turbolinedict
        ]

        return Streams(streams=streams)

    for wg in turbolinedict.keys():
        print(
            f"Stream {wg} will contain the lines matching the regex : {turbolinedict[wg]} \n"
        )

    config = run_moore(options, make_streams, public_tools=[])
    return config


def turcal_spruce_production(options: Options):
    def make_streams():
        streams = [
            Stream(
                name=wg,
                lines=[PassLine(name="Pass" + wg, hlt2_filter_code=turcallinedict[wg])],
                detectors=[],
            )
            for wg in turcallinedict
            if "raw" not in wg
        ]

        streams += [
            Stream(
                name=wg,
                lines=[
                    PassLine(
                        name="Pass" + wg,
                        hlt2_filter_code=turcallinedict[wg],
                        prescale=0.1,
                    )
                ],
                detectors=DETECTORS,
            )
            for wg in turcallinedict
            if "raw" in wg
        ]

        return Streams(streams=streams)

    for wg in turcallinedict.keys():
        print(
            f"Stream {wg} will contain the lines matching the regex : {turcallinedict[wg]} \n"
        )

    config = run_moore(options, make_streams, public_tools=[])
    return config
