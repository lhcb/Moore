###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Options for Sprucing data from the 2024 pp reference run.

from Hlt2Conf.lines import sprucing_lines
from Hlt2Conf.lines.b_to_charmonia import sprucing_lines as spruce_b2cc_lines
from Hlt2Conf.lines.bandq import hlt2_full_lines as bandq_full_lines
from Hlt2Conf.lines.bandq import sprucing_lines as spruce_bandq_lines
from Hlt2Conf.lines.charmonium_to_dimuon import (
    full_lines as charmonium_to_dimuon_prompt_full_lines,
)
from Hlt2Conf.lines.charmonium_to_dimuon_detached import (
    all_lines as charmonium_to_dimuon_detached_lines,
)
from Hlt2Conf.lines.ift.hlt2_ift_pp_ref_2024 import ift_full_lines
from Hlt2Conf.lines.ift.spruce_ift_pp_ref_2024 import sprucing_lines as spruce_ift_lines
from Hlt2Conf.lines.inclusive_detached_dilepton import (
    all_lines as inclusive_detached_dilepton_lines,
)
from Hlt2Conf.lines.qee.hlt2_qee_pp_ref_2024 import full_lines as qee_full_lines
from Hlt2Conf.lines.qee.spruce_qee_pp_ref_2024 import sprucing_lines as spruce_qee_lines
from Hlt2Conf.lines.semileptonic import all_lines as semileptonic_lines
from Hlt2Conf.lines.semileptonic import sprucing_lines as spruce_sl_lines
from Hlt2Conf.lines.topological_b import all_lines as topological_b_lines
from Moore import Options, run_moore
from Moore.lines import PassLine, SpruceLine
from Moore.persistence.hlt2_tistos import list_of_full_stream_lines
from Moore.streams import DETECTORS, Stream, Streams
from PyConf.reading import get_particles
from RecoConf.global_tools import stateProvider_with_simplified_geom

public_tools = [stateProvider_with_simplified_geom()]

###################### EXCLUSIVE SPRUCING ON FULL STREAM ####################

fulllinedict = {
    "bandq": spruce_bandq_lines,
    "b2cc": spruce_b2cc_lines,
    "sl": spruce_sl_lines,
    "qee": spruce_qee_lines,
    "ift": spruce_ift_lines,
}

## Following configures FULL HLT2 lines that need TISTOS info in exclusive Sprucing

# Pick out SL lines to include.
semileptonic_lines_to_keep = [
    "Hlt2SLB_LbToLcMuNu_LcToPKPi",
    "Hlt2SLB_LbToLcMuNu_LcToPKPi_FakeMuon",
    "Hlt2SLB_LbToLcENu_LcToPKPi",
    "Hlt2SLB_LbToLcENu_LcToPKPi_FakeElectron",
    "Hlt2SLB_BcToJpsiMuNu_JpsiToMuMu",
    "Hlt2SLB_BcToJpsiMuNu_JpsiToMuMu_FakeMuon",
    "Hlt2SLB_BcToJpsiENu_JpsiToMuMu",
    "Hlt2SLB_BcToJpsiENu_JpsiToMuMu_FakeElectron",
    "Hlt2SLB_BuToD0MuNu_D0ToKPi",
    "Hlt2SLB_BuToD0MuNu_D0ToKPi_FakeMuon",
    "Hlt2SLB_BuToD0ENu_D0ToKPi",
    "Hlt2SLB_BuToD0ENu_D0ToKPi_FakeElectron",
    "Hlt2SLB_B0ToDpMuNu_DpToKPiPi",
    "Hlt2SLB_B0ToDpMuNu_DpToKPiPi_FakeMuon",
    "Hlt2SLB_B0ToDpENu_DpToKPiPi",
    "Hlt2SLB_B0ToDpENu_DpToKPiPi_FakeElectron",
    "Hlt2SLB_B0ToDpMuNu_DpToKKPi",
    "Hlt2SLB_B0ToDpMuNu_DpToKKPi_FakeMuon",
    "Hlt2SLB_B0ToDpENu_DpToKKPi",
    "Hlt2SLB_B0ToDpENu_DpToKKPi_FakeElectron",
    "Hlt2SLB_BsToDsMuNu_DsToKKPi",
    "Hlt2SLB_BsToDsMuNu_DsToKKPi_FakeMuon",
    "Hlt2SLB_BsToDsENu_DsToKKPi",
    "Hlt2SLB_BsToDsENu_DsToKKPi_FakeElectron",
    "Hlt2SLB_Xib0ToXicplusMuNu_XicplusTopKPi",
    "Hlt2SLB_Xib0ToXicplusMuNu_XicplusTopKPi_FakeMuon",
    "Hlt2SLB_XibminusToXic0MuNu_Xic0TopKKPi",
    "Hlt2SLB_XibminusToXic0MuNu_Xic0TopKKPi_FakeMuon",
    "Hlt2SLB_OmegabToOmegacMuNu_OmegacToPKKPi",
    "Hlt2SLB_OmegabToOmegacMuNu_OmegacToPKKPi_FakeMuon",
]
semileptonic_lines_filtered = {
    name: maker
    for name, maker in semileptonic_lines.items()
    if name in semileptonic_lines_to_keep
}

# Pick out B&Q FULL lines to include.
bandq_full_lines_to_keep = [
    "Hlt2BandQ_DiMuonSameSignHighMassFull",
    "Hlt2BandQ_DiMuonIncFull",
    "Hlt2BandQ_DiMuonSameSignIncFull",
    "Hlt2BandQ_DiMuonIncHighPTFull",
    "Hlt2BandQ_DiMuonSameSignIncHighPTFull",
    "Hlt2BandQ_DiMuonSoftFull",
    "Hlt2BandQ_DiMuonUpsilonFull",
    "Hlt2BandQ_DoubleCharmSameSignFull",
    "Hlt2BandQ_DoubleCharmOppositeSignFull",
    "Hlt2BandQ_DoubleCharm_D0ToHHFull",
]
bandq_full_lines_filtered = {
    name: maker
    for name, maker in bandq_full_lines.items()
    if name in bandq_full_lines_to_keep
}

full_modules_for_TISTOS = [
    topological_b_lines,
    inclusive_detached_dilepton_lines,
    charmonium_to_dimuon_detached_lines,
    charmonium_to_dimuon_prompt_full_lines,
    qee_full_lines,
    semileptonic_lines_filtered,
    ift_full_lines,
    bandq_full_lines_filtered,
]


def lines_for_TISTOS(full_modules_for_TISTOS):
    """Configure FULL lines for TISTOS persistency in exclusive Sprucing
    Note there will hopefully be nicer ways to configure this in the future"""

    full_lines_for_TISTOS = [
        linename for module in full_modules_for_TISTOS for linename in module.keys()
    ]

    return full_lines_for_TISTOS


def lines_running(linedict):
    """Return the full list of lines to be run"""

    return [
        item
        for sublist in [list(linedict[wg].keys()) for wg in linedict]
        for item in sublist
    ]


def lines_not_running(all_lines, lines_to_run):
    """Return the list of lines that are declared in the framework but that are not set to run"""
    return [item for item in list(all_lines) if item not in lines_to_run]


def make_excl_spruce_prod_streams():
    lines_to_run = lines_running(fulllinedict)
    missing_lines = lines_not_running(sprucing_lines, lines_to_run)

    print(f"Running {len(lines_to_run)} lines")
    for wg in fulllinedict.keys():
        lines = list(fulllinedict[wg].keys())
        print(f"Stream {wg} will contain the lines: {lines} \n")

    print(
        f"The following lines exist but are not appended to any stream : {missing_lines} \n end of missing lines."
    )

    streams = [
        Stream(
            wg, lines=[builder() for builder in fulllinedict[wg].values()], detectors=[]
        )
        for wg in fulllinedict
    ]

    return Streams(streams=streams)


def excl_spruce_production(options: Options):
    with list_of_full_stream_lines.bind(
        lines=lines_for_TISTOS(full_modules_for_TISTOS)
    ):
        config = run_moore(options, make_excl_spruce_prod_streams, public_tools=[])
        return config


###################### PASSTHROUGH SPRUCING ON TURBO STREAM ####################

turbolinedict = {
    "b2oc": ["Hlt2B2OC.*Decision"],
    "bandq": [
        "^(?!.*Full)Hlt2BandQ.*Decision",
        "^(?!.*Full)Hlt2_JpsiToMuMu.*Decision",
        "^(?!.*Full)Hlt2_Psi2SToMuMu.*Decision",
    ],
    "b2cc": ["Hlt2B2CC.*Decision"],
    "charm": ["Hlt2Charm.*Decision", "Hlt2HadInt.*Decision"],
    "qee": ["^(?!.*Full)Hlt2QEE.*Decision"],
    "rd": ["^(?!.*Gamma.*Incl)Hlt2RD.*Decision"],
    "bnoc": ["^(?!.*Full)Hlt2BnoC.*Decision"],
    "ift": ["Hlt2IFTTurbo.*Decision"],
    "trackeff": ["Hlt2TurboVelo2Long.*Decision", "Hlt2TrackEff_.*_FullEventDecision"],
}


def make_turbo_spruce_prod_streams():
    streams = [
        Stream(
            wg,
            lines=[
                PassLine(
                    name="Pass" + wg,
                    hlt2_filter_code=turbolinedict[wg],
                )
            ],
            detectors=DETECTORS,
        )
        for wg in turbolinedict
    ]

    for wg in turbolinedict.keys():
        print(
            f"Stream {wg} will contain the lines matching the regex : {turbolinedict[wg]} \n"
        )

    return Streams(streams=streams)


def turbo_spruce_production(options: Options):
    config = run_moore(options, make_turbo_spruce_prod_streams, public_tools=[])
    return config


###################### MDST SPRUCING ON TURCAL STREAM ####################

nominal_lines = [
    "Hlt2PID_BToJpsiK_JpsiToPpPmTagged",
    "Hlt2PID_BToJpsiK_JpsiToPmPpTagged",
    "Hlt2PID_DsToPhiPi_PhiToMuMupTagged_Detached",
    "Hlt2PID_DsToPhiPi_PhiToMuMumTagged_Detached",
    "Hlt2PID_DstToD0Pi_D0ToKPiPiPi",
    "Hlt2PID_KsToPiPi_LL",
    "Hlt2PID_LbToLmdJpsi_DD",
    "Hlt2PID_LbToLmdJpsi_LL",
    "Hlt2PID_LbToLcMuNu_LcToPKPi",
    "Hlt2PID_LcToLmdPi_DD",
    "Hlt2PID_LcToLmdPi_LL",
    "Hlt2PID_LbToLcPi_LcToPKPi",
    "Hlt2PID_LcToPKPi",
    "Hlt2PID_OmegaToL0K_L0ToPPi_LLL",
    "Hlt2PID_PhiToKK_Unbiased_Detached",
    "Hlt2PID_PhiToKmKpTagged_Detached",
    "Hlt2PID_KsToPiPi_DD",
    "Hlt2PID_L0ToPPi_DD_LowPT",
    "Hlt2PID_L0ToPPi_DD_MidPT",
    "Hlt2PID_OmegaToL0K_L0ToPPi_DDD",
    "Hlt2PID_L0ToPPi_DD_HighPT",
    "Hlt2PID_PhiToKpKmTagged_Detached",
    "Hlt2PID_L0ToPPi_DD_VeryHighPT",
    "Hlt2PID_SMOG2Dst2KPi",
    "Hlt2PID_SMOG2KS2PiPiDD",
    "Hlt2PID_SMOG2KS2PiPiLLHighPT",
    "Hlt2PID_SMOG2KS2PiPiLLLowPT",
    "Hlt2PID_SMOG2Lambda02PPiDD",
    "Hlt2PID_SMOG2Lambda02PPiLLLowPT",
    "Hlt2PID_SMOG2Lambda02PPiLLHighPT",
    "Hlt2PID_SMOG2PIDJpsi2MuMumTagged",
    "Hlt2PID_SMOG2PIDJpsi2MuMupTagged",
    "Hlt2PID_SMOG2Phi2kkHighPT_Kmprobe",
    "Hlt2PID_SMOG2Phi2kkHighPT_Kpprobe",
    "Hlt2PID_SMOG2Phi2kk_Kmprobe",
    "Hlt2PID_SMOG2Phi2kk_Kpprobe",
]

persist_reco_lines = [
    "Hlt2PID_BdToKstG",
    "Hlt2PID_Bs2PhiG",
    "Hlt2PID_EtaMuMuG",
    "Hlt2PID_Dsst2DsG",
    "Hlt2PID_D2EtapPi",
    "Hlt2PID_DstToD0Pi_D0ToKPiPi0Resolved",
    "Hlt2PID_DstToD0Pi_D0ToKPiPi0Merged",
]

rawbank_lines = [
    "Hlt2PID_BToJpsiK_JpsiToMuMumTagged_Detached",
    "Hlt2PID_BToJpsiK_JpsiToMuMupTagged_Detached",
    "Hlt2PID_JpsiToMuMumTagged_Detached",
    "Hlt2PID_JpsiToMuMupTagged_Detached",
    "Hlt2PID_BToJpsiK_JpsiToEmbremEpbremTagged_noPIDe",
    "Hlt2PID_BToJpsiK_JpsiToEpbremEmbremTagged_noPIDe",
    "Hlt2PID_BToJpsiK_JpsiToEpbremEmbremTagged",
    "Hlt2PID_BToJpsiK_JpsiToEmbremEpbremTagged",
    "Hlt2PID_DstToD0Pi_D0ToKPi",
    "Hlt2PID_L0ToPPi_LL_LowPT",
    "Hlt2PID_L0ToPPi_LL_MidPT",
    "Hlt2PID_L0ToPPi_LL_HighPT",
    "Hlt2PID_L0ToPPi_LL_VeryHighPT",
    "Hlt2TrackEff_DiMuon_SeedMuon_mup_Tag",
    "Hlt2TrackEff_DiMuon_SeedMuon_mum_Tag",
    "Hlt2TrackEff_DiMuon_SeedMuon_mum_Match",
    "Hlt2TrackEff_DiMuon_SeedMuon_mup_Match",
    "Hlt2TrackEff_DiMuon_VeloMuon_mup_Tag",
    "Hlt2TrackEff_DiMuon_VeloMuon_mum_Tag",
    "Hlt2TrackEff_DiMuon_VeloMuon_mum_Match",
    "Hlt2TrackEff_DiMuon_VeloMuon_mup_Match",
    "Hlt2TrackEff_DiMuon_Downstream_mup_Tag",
    "Hlt2TrackEff_DiMuon_Downstream_mum_Tag",
    "Hlt2TrackEff_DiMuon_Downstream_mup_Match",
    "Hlt2TrackEff_DiMuon_Downstream_mum_Match",
    "Hlt2TrackEff_DiMuon_MuonUT_mup_Tag",
    "Hlt2TrackEff_DiMuon_MuonUT_mum_Tag",
    "Hlt2TrackEff_DiMuon_MuonUT_mup_Match",
    "Hlt2TrackEff_DiMuon_MuonUT_mum_Match",
    "Hlt2TrackEff_SMOG2DiMuon_SeedMuon_mum_Match",
    "Hlt2TrackEff_SMOG2DiMuon_SeedMuon_mum_Tag",
    "Hlt2TrackEff_SMOG2DiMuon_SeedMuon_mup_Match",
    "Hlt2TrackEff_SMOG2DiMuon_SeedMuon_mup_Tag",
    "Hlt2TrackEff_SMOG2DiMuon_VeloMuon_mum_Match",
    "Hlt2TrackEff_SMOG2DiMuon_VeloMuon_mum_Tag",
    "Hlt2TrackEff_SMOG2DiMuon_VeloMuon_mup_Match",
    "Hlt2TrackEff_SMOG2DiMuon_VeloMuon_mup_Tag",
    "Hlt2TrackEff_SMOG2KshortVeloLong_HighPT",
    "Hlt2TrackEff_SMOG2KshortVeloLong_LowPT",
    "Hlt2TrackEff_SMOG2KshortVeloLong_VeryHighPT",
    "Hlt2TrackEff_TurCalVelo2Long_KshortVSoft",
    "Hlt2TrackEff_TurCalVelo2Long_KshortHard",
    "Hlt2TrackEff_TurCalVelo2Long_Kshort",
    "Hlt2TrackEff_TurCalVelo2Long_KshortSoft",
    "Hlt2TrackEff_TurCalVelo2Long_KshortVHard",
]

persist_reco_rawbank_lines = [
    "Hlt2TrackEff_ZToMuMu_SeedMuon_mup_Tag",
    "Hlt2TrackEff_ZToMuMu_SeedMuon_mum_Tag",
    "Hlt2TrackEff_ZToMuMu_SeedMuon_mum_Match",
    "Hlt2TrackEff_ZToMuMu_SeedMuon_mup_Match",
    "Hlt2TrackEff_ZToMuMu_VeloMuon_mup_Tag",
    "Hlt2TrackEff_ZToMuMu_VeloMuon_mum_Tag",
    "Hlt2TrackEff_ZToMuMu_VeloMuon_mum_Match",
    "Hlt2TrackEff_ZToMuMu_VeloMuon_mup_Match",
    "Hlt2TrackEff_ZToMuMu_Downstream_mup_Tag",
    "Hlt2TrackEff_ZToMuMu_Downstream_mum_Tag",
    "Hlt2TrackEff_ZToMuMu_Downstream_mup_Match",
    "Hlt2TrackEff_ZToMuMu_Downstream_mum_Match",
    "Hlt2TrackEff_ZToMuMu_MuonUT_mup_Tag",
    "Hlt2TrackEff_ZToMuMu_MuonUT_mum_Tag",
    "Hlt2TrackEff_ZToMuMu_MuonUT_mup_Match",
    "Hlt2TrackEff_ZToMuMu_MuonUT_mum_Match",
    "Hlt2TrackEff_Velo2Long_B2JpsiK_MuonProbe_VELO",
    "Hlt2TrackEff_Velo2Long_B2JpsiK_ElectronProbe_VELO",
]


def make_turcal_spruceline(hlt2_linename, persist_reco=False, prescale=1.0):
    filter = f"{hlt2_linename}Decision"
    location = f"/Event/HLT2/{hlt2_linename}/Particles"
    spruce_linename = hlt2_linename.replace("Hlt2", "SpruceTurCal")
    print(f"{filter} {location} {spruce_linename}")
    hlt2_particles = get_particles(location)
    turcal_spruce_line = SpruceLine(
        name=spruce_linename,
        hlt2_filter_code=filter,
        algs=[hlt2_particles],
        persistreco=persist_reco,
        prescale=prescale,
    )
    return turcal_spruce_line


def make_turcal_spruce_prod_streams():
    streams = [
        Stream(
            "Turcal_mDST",
            lines=[make_turcal_spruceline(line) for line in nominal_lines],
            detectors=[],
        ),
        Stream(
            "Turcal_persistreco",
            lines=[
                make_turcal_spruceline(line, persist_reco=True)
                for line in persist_reco_lines
            ],
            detectors=[],
        ),
        Stream(
            "Turcal_rawbanks",
            lines=[make_turcal_spruceline(line) for line in rawbank_lines],
            detectors=DETECTORS,
        ),
        Stream(
            "Turcal_persistrecorawbanks",
            lines=[
                make_turcal_spruceline(line, persist_reco=True)
                for line in persist_reco_rawbank_lines
            ],
            detectors=DETECTORS,
        ),
    ]
    print(streams)
    return Streams(streams=streams)


def turcal_spruce_production(options: Options):
    config = run_moore(options, make_turcal_spruce_prod_streams, public_tools=[])
    return config


### NOTE: THE FOLLOWING STREAMS ARE NOT FOR USAGE BY ANALYSTS
### THEY ARE "TECHNICAL" STREAMS FOR USE BY RTA/DPA CORE TEAM

###################### PASSTHROUGH SPRUCING ON NOBIAS STREAM ####################

# NoBias stream only has the HLT2NoBias line


def make_nobias_spruce_prod_streams():
    """Pass all rawbanks"""
    streams = [
        Stream(
            "nobias",
            lines=[
                PassLine(
                    name="Passnobias", hlt2_filter_code="Hlt2NoBias_NoBiasDecision"
                )
            ],
            detectors=DETECTORS,
        ),
    ]

    return Streams(streams=streams)


def nobias_spruce_production(options: Options):
    config = run_moore(options, make_nobias_spruce_prod_streams, public_tools=[])
    return config


###################### PASSTHROUGH SPRUCING ON HLT2CALIB STREAM ####################
###WIP###WIP###WIP###WIP###WIP###WIP###WIP###WIP###WIP###WIP###WIP###WIP###WIP###WIP

hlt2caliblinedict = {
    "beamgas": ["Hlt2.*BeamGas.*Decision"],
}


def make_hlt2calib_spruce_prod_streams():
    """beamgas lines go to own stream in sprucing - pass all rawbanks and include physics filter"""
    streams = [
        Stream(
            wg,
            lines=[
                PassLine(
                    name="Pass" + wg,
                    hlt2_filter_code=hlt2caliblinedict[wg],
                )
            ],
            detectors=DETECTORS,
        )
        for wg in hlt2caliblinedict
    ]
    for wg in hlt2caliblinedict.keys():
        print(
            f"Stream {wg} will contain the lines matching the regex : {hlt2caliblinedict[wg]} \n"
        )

    return Streams(streams=streams)


def hlt2calib_spruce_production(options: Options):
    config = run_moore(options, make_hlt2calib_spruce_prod_streams, public_tools=[])
    return config


###################### PASSTHROUGH SPRUCING ON LUMI STREAM ####################


##Note: `Hlt2LumiCalibration` passes `Hlt1ODIN1kHzLumiDecision` and `Hlt2LumiDefaultRawBanks` passes `Hlt1ODINLumiDecision`
def make_lumi_spruce_prod_streams():
    """Do not pass detector rawbanks"""
    streams = [
        Stream(
            "lumi",
            lines=[
                PassLine(
                    name="Passlumi",
                )
            ],
            detectors=[],
        ),
    ]

    return Streams(streams=streams)


def lumi_spruce_production(options: Options):
    config = run_moore(options, make_lumi_spruce_prod_streams, public_tools=[])
    return config
