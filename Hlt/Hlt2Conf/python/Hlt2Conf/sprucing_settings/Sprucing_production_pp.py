###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## Options for Sprucing 2024 data - this is a living script and is NOT necessarily what ran in production.
## What runs in production is defined in the SprucingConfig data pkg
## https://gitlab.cern.ch/lhcb-datapkg/SprucingConfig
## and the tag used on a particular dataset can be found in the twiki
## https://twiki.cern.ch/twiki/bin/view/LHCbInternal/OperationSummary2024
## What is in this script is compared to the output of hlt2_pp_2024 and should be updated by line authors when prompted to do so

from Hlt2Conf.lines import sprucing_lines
from Hlt2Conf.lines.b_to_charmonia import sprucing_lines as spruce_b2cc_lines
from Hlt2Conf.lines.b_to_open_charm import (
    all_calib_lines as b_to_open_charm_calib_lines,
)
from Hlt2Conf.lines.b_to_open_charm import sprucing_lines as spruce_b2oc_lines
from Hlt2Conf.lines.bandq import hlt2_full_lines as bandq_full_lines
from Hlt2Conf.lines.bandq import sprucing_lines as spruce_bandq_lines
from Hlt2Conf.lines.bnoc import full_lines as bnoc_full_lines
from Hlt2Conf.lines.bnoc import sprucing_lines as spruce_bnoc_lines
from Hlt2Conf.lines.charmonium_to_dimuon import (
    full_lines as charmonium_to_dimuon_prompt_full_lines,
)
from Hlt2Conf.lines.charmonium_to_dimuon_detached import (
    all_lines as charmonium_to_dimuon_detached_lines,
)
from Hlt2Conf.lines.ift import ift_full_lines
from Hlt2Conf.lines.ift import sprucing_lines as spruce_ift_lines
from Hlt2Conf.lines.inclusive_detached_dilepton import (
    all_lines as inclusive_detached_dilepton_lines,
)
from Hlt2Conf.lines.qee import hlt2_full_lines as qee_full_lines
from Hlt2Conf.lines.qee import sprucing_lines as spruce_qee_lines
from Hlt2Conf.lines.rd import full_lines as rd_full_lines
from Hlt2Conf.lines.rd import sprucing_lines as spruce_rd_lines
from Hlt2Conf.lines.semileptonic import all_lines as semileptonic_lines
from Hlt2Conf.lines.semileptonic import sprucing_lines as spruce_sl_lines
from Hlt2Conf.lines.topological_b import all_lines as topological_b_lines
from Moore import Options, run_moore
from Moore.lines import PassLine, SpruceLine
from Moore.persistence.hlt2_tistos import list_of_full_stream_lines
from Moore.streams import DETECTORS, Stream, Streams
from PyConf.reading import get_particles

###################### EXCLUSIVE SPRUCING ON FULL STREAM ####################

fulllinedict = {
    "b2oc": spruce_b2oc_lines,
    "bandq": spruce_bandq_lines,
    "b2cc": spruce_b2cc_lines,
    "rd": spruce_rd_lines,
    "sl": spruce_sl_lines,
    "qee": spruce_qee_lines,
    "bnoc": spruce_bnoc_lines,
    "ift": spruce_ift_lines,
}

## Following configures FULL HLT2 lines that need TISTOS info in exclusive Sprucing
full_modules_for_TISTOS = [
    topological_b_lines,
    inclusive_detached_dilepton_lines,
    charmonium_to_dimuon_detached_lines,
    charmonium_to_dimuon_prompt_full_lines,
    b_to_open_charm_calib_lines,
    rd_full_lines,
    qee_full_lines,
    semileptonic_lines,
    bnoc_full_lines,
    ift_full_lines,
    bandq_full_lines,
]


def lines_for_TISTOS(full_modules_for_TISTOS):
    """Configure FULL lines for TISTOS persistency in exclusive Sprucing
    Note there will hopefully be nicer ways to configure this in the future"""

    full_lines_for_TISTOS = [
        linename for module in full_modules_for_TISTOS for linename in module.keys()
    ]

    return full_lines_for_TISTOS


def lines_running(linedict):
    """Return the full list of lines to be run"""

    return [
        item
        for sublist in [list(linedict[wg].keys()) for wg in linedict]
        for item in sublist
    ]


def lines_not_running(all_lines, lines_to_run):
    """Return the list of lines that are declared in the framework but that are not set to run"""
    return [item for item in list(all_lines) if item not in lines_to_run]


def make_excl_spruce_prod_streams():
    spruce_b2oc_lines.pop("SpruceB2OC_BdToDsmK_DsmToHHH_FEST")
    lines_to_run = lines_running(fulllinedict)
    missing_lines = lines_not_running(sprucing_lines, lines_to_run)

    print(f"Running {len(lines_to_run)} lines")
    for wg in fulllinedict.keys():
        lines = list(fulllinedict[wg].keys())
        print(f"Stream {wg} will contain the lines: {lines} \n")

    print(
        f"The following lines exist but are not appended to any stream : {missing_lines} \n end of missing lines."
    )

    streams = [
        Stream(
            wg, lines=[builder() for builder in fulllinedict[wg].values()], detectors=[]
        )
        for wg in fulllinedict
    ]

    return Streams(streams=streams)


def excl_spruce_production(options: Options):
    with list_of_full_stream_lines.bind(
        lines=lines_for_TISTOS(full_modules_for_TISTOS)
    ):
        config = run_moore(options, make_excl_spruce_prod_streams, public_tools=[])
        return config


###################### PASSTHROUGH SPRUCING ON TURBO STREAM ####################

turbolinedict = {
    "b2oc": ["Hlt2B2OC.*"],
    "bandq": [
        "^(?!.*Full)Hlt2BandQ.*",
        "^(?!.*Full)Hlt2_JpsiToMuMu.*",
        "^(?!.*Full)Hlt2_Psi2SToMuMu.*",
    ],
    "b2cc": ["Hlt2B2CC.*"],
    "charm": ["Hlt2Charm.*", "Hlt2HadInt.*"],
    "qee": ["^(?!.*Full)Hlt2QEE.*"],
    "rd": ["^(?!.*Gamma.*Incl)Hlt2RD.*"],
    "bnoc": ["^(?!.*Full)Hlt2BnoC.*"],
    "ift": ["Hlt2IFTTurbo.*"],
    "trackeff": ["Hlt2TurboVelo2Long.*", "Hlt2TrackEff_.*_FullEvent"],
}

from Moore.production import turbo_spruce


def make_turbo_spruce_prod_streams(line_config):
    return turbo_spruce(line_config, use_regex=True, spruce_streams=turbolinedict)


def turbo_spruce_production(options: Options):
    import json

    input_line_config = options.input_streams_attributes_file
    stream = "turbo"
    with open(input_line_config, "r") as json_file:
        line_config = json.load(json_file)[stream]

    def make_streams():
        return make_turbo_spruce_prod_streams(line_config)

    config = run_moore(options, make_streams, public_tools=[])
    return config


###################### MDST SPRUCING ON TURCAL STREAM ####################

nominal_lines = [
    "Hlt2PID_BToJpsiK_JpsiToPpPmTagged",
    "Hlt2PID_BToJpsiK_JpsiToPmPpTagged",
    "Hlt2PID_DsToPhiPi_PhiToMuMupTagged_Detached",
    "Hlt2PID_DsToPhiPi_PhiToMuMumTagged_Detached",
    "Hlt2PID_DstToD0Pi_D0ToKPiPiPi",
    "Hlt2PID_KsToPiPi_LL",
    "Hlt2PID_LbToLmdJpsi_DD",
    "Hlt2PID_LbToLmdJpsi_LL",
    "Hlt2PID_LbToLcMuNu_LcToPKPi",
    "Hlt2PID_LcToLmdPi_DD",
    "Hlt2PID_LcToLmdPi_LL",
    "Hlt2PID_LbToLcPi_LcToPKPi",
    "Hlt2PID_LcToPKPi",
    "Hlt2PID_OmegaToL0K_L0ToPPi_LLL",
    "Hlt2PID_PhiToKK_Unbiased_Detached",
    "Hlt2PID_PhiToKmKpTagged_Detached",
    "Hlt2PID_KsToPiPi_DD",
    "Hlt2PID_L0ToPPi_DD_LowPT",
    "Hlt2PID_L0ToPPi_DD_MidPT",
    "Hlt2PID_OmegaToL0K_L0ToPPi_DDD",
    "Hlt2PID_L0ToPPi_DD_HighPT",
    "Hlt2PID_PhiToKpKmTagged_Detached",
    "Hlt2PID_L0ToPPi_DD_VeryHighPT",
    "Hlt2PID_SMOG2Dst2KPi",
    "Hlt2PID_SMOG2KS2PiPiDD",
    "Hlt2PID_SMOG2KS2PiPiLLHighPT",
    "Hlt2PID_SMOG2KS2PiPiLLLowPT",
    "Hlt2PID_SMOG2Lambda02PPiDD",
    "Hlt2PID_SMOG2Lambda02PPiLLLowPT",
    "Hlt2PID_SMOG2Lambda02PPiLLHighPT",
    "Hlt2PID_SMOG2PIDJpsi2MuMumTagged",
    "Hlt2PID_SMOG2PIDJpsi2MuMupTagged",
    "Hlt2PID_SMOG2Phi2kkHighPT_Kmprobe",
    "Hlt2PID_SMOG2Phi2kkHighPT_Kpprobe",
    "Hlt2PID_SMOG2Phi2kk_Kmprobe",
    "Hlt2PID_SMOG2Phi2kk_Kpprobe",
]

persist_reco_lines = [
    "Hlt2PID_BdToKstG",
    "Hlt2PID_Bs2PhiG",
    "Hlt2PID_EtaMuMuG",
    "Hlt2PID_Dsst2DsG",
    "Hlt2PID_D2EtapPi",
    "Hlt2PID_DstToD0Pi_D0ToKPiPi0Resolved",
    "Hlt2PID_DstToD0Pi_D0ToKPiPi0Merged",
]

rawbank_lines = [
    "Hlt2PID_BToJpsiK_JpsiToMuMumTagged_Detached",
    "Hlt2PID_BToJpsiK_JpsiToMuMupTagged_Detached",
    "Hlt2PID_JpsiToMuMumTagged_Detached",
    "Hlt2PID_JpsiToMuMupTagged_Detached",
    "Hlt2PID_BToJpsiK_JpsiToEmbremEpbremTagged_noPIDe",
    "Hlt2PID_BToJpsiK_JpsiToEpbremEmbremTagged_noPIDe",
    "Hlt2PID_BToJpsiK_JpsiToEpbremEmbremTagged",
    "Hlt2PID_BToJpsiK_JpsiToEmbremEpbremTagged",
    "Hlt2PID_DstToD0Pi_D0ToKPi",
    "Hlt2PID_L0ToPPi_LL_LowPT",
    "Hlt2PID_L0ToPPi_LL_MidPT",
    "Hlt2PID_L0ToPPi_LL_HighPT",
    "Hlt2PID_L0ToPPi_LL_VeryHighPT",
    "Hlt2TrackEff_DiMuon_SeedMuon_mup_Tag",
    "Hlt2TrackEff_DiMuon_SeedMuon_mum_Tag",
    "Hlt2TrackEff_DiMuon_SeedMuon_mum_Match",
    "Hlt2TrackEff_DiMuon_SeedMuon_mup_Match",
    "Hlt2TrackEff_DiMuon_VeloMuon_mup_Tag",
    "Hlt2TrackEff_DiMuon_VeloMuon_mum_Tag",
    "Hlt2TrackEff_DiMuon_VeloMuon_mum_Match",
    "Hlt2TrackEff_DiMuon_VeloMuon_mup_Match",
    "Hlt2TrackEff_DiMuon_Downstream_mup_Tag",
    "Hlt2TrackEff_DiMuon_Downstream_mum_Tag",
    "Hlt2TrackEff_DiMuon_Downstream_mup_Match",
    "Hlt2TrackEff_DiMuon_Downstream_mum_Match",
    "Hlt2TrackEff_DiMuon_MuonUT_mup_Tag",
    "Hlt2TrackEff_DiMuon_MuonUT_mum_Tag",
    "Hlt2TrackEff_DiMuon_MuonUT_mup_Match",
    "Hlt2TrackEff_DiMuon_MuonUT_mum_Match",
    "Hlt2TrackEff_SMOG2DiMuon_SeedMuon_mum_Match",
    "Hlt2TrackEff_SMOG2DiMuon_SeedMuon_mum_Tag",
    "Hlt2TrackEff_SMOG2DiMuon_SeedMuon_mup_Match",
    "Hlt2TrackEff_SMOG2DiMuon_SeedMuon_mup_Tag",
    "Hlt2TrackEff_SMOG2DiMuon_VeloMuon_mum_Match",
    "Hlt2TrackEff_SMOG2DiMuon_VeloMuon_mum_Tag",
    "Hlt2TrackEff_SMOG2DiMuon_VeloMuon_mup_Match",
    "Hlt2TrackEff_SMOG2DiMuon_VeloMuon_mup_Tag",
    "Hlt2TrackEff_SMOG2KshortVeloLong_HighPT",
    "Hlt2TrackEff_SMOG2KshortVeloLong_LowPT",
    "Hlt2TrackEff_SMOG2KshortVeloLong_VeryHighPT",
    "Hlt2TrackEff_TurCalVelo2Long_KshortVSoft",
    "Hlt2TrackEff_TurCalVelo2Long_KshortHard",
    "Hlt2TrackEff_TurCalVelo2Long_Kshort",
    "Hlt2TrackEff_TurCalVelo2Long_KshortSoft",
    "Hlt2TrackEff_TurCalVelo2Long_KshortVHard",
]

persist_reco_rawbank_lines = [
    "Hlt2TrackEff_ZToMuMu_SeedMuon_mup_Tag",
    "Hlt2TrackEff_ZToMuMu_SeedMuon_mum_Tag",
    "Hlt2TrackEff_ZToMuMu_SeedMuon_mum_Match",
    "Hlt2TrackEff_ZToMuMu_SeedMuon_mup_Match",
    "Hlt2TrackEff_ZToMuMu_VeloMuon_mup_Tag",
    "Hlt2TrackEff_ZToMuMu_VeloMuon_mum_Tag",
    "Hlt2TrackEff_ZToMuMu_VeloMuon_mum_Match",
    "Hlt2TrackEff_ZToMuMu_VeloMuon_mup_Match",
    "Hlt2TrackEff_ZToMuMu_Downstream_mup_Tag",
    "Hlt2TrackEff_ZToMuMu_Downstream_mum_Tag",
    "Hlt2TrackEff_ZToMuMu_Downstream_mup_Match",
    "Hlt2TrackEff_ZToMuMu_Downstream_mum_Match",
    "Hlt2TrackEff_ZToMuMu_MuonUT_mup_Tag",
    "Hlt2TrackEff_ZToMuMu_MuonUT_mum_Tag",
    "Hlt2TrackEff_ZToMuMu_MuonUT_mup_Match",
    "Hlt2TrackEff_ZToMuMu_MuonUT_mum_Match",
    "Hlt2TrackEff_Velo2Long_B2JpsiK_MuonProbe_VELO",
    "Hlt2TrackEff_Velo2Long_B2JpsiK_ElectronProbe_VELO",
]


def make_turcal_spruceline(hlt2_linename, persist_reco=False, prescale=1.0):
    filter = f"{hlt2_linename}Decision"
    location = f"/Event/HLT2/{hlt2_linename}/Particles"
    spruce_linename = hlt2_linename.replace("Hlt2", "SpruceTurCal")
    print(f"{filter} {location} {spruce_linename}")
    hlt2_particles = get_particles(location)
    turcal_spruce_line = SpruceLine(
        name=spruce_linename,
        hlt2_filter_code=filter,
        algs=[hlt2_particles],
        persistreco=persist_reco,
        prescale=prescale,
    )
    return turcal_spruce_line


def make_turcal_spruce_prod_streams():
    streams = [
        Stream(
            "Turcal_mDST",
            lines=[make_turcal_spruceline(line) for line in nominal_lines],
            detectors=[],
        ),
        Stream(
            "Turcal_persistreco",
            lines=[
                make_turcal_spruceline(line, persist_reco=True)
                for line in persist_reco_lines
            ],
            detectors=[],
        ),
        Stream(
            "Turcal_rawbanks",
            lines=[make_turcal_spruceline(line) for line in rawbank_lines],
            detectors=DETECTORS,
        ),
        Stream(
            "Turcal_persistrecorawbanks",
            lines=[
                make_turcal_spruceline(line, persist_reco=True)
                for line in persist_reco_rawbank_lines
            ],
            detectors=DETECTORS,
        ),
    ]
    print(streams)
    return Streams(streams=streams)


def turcal_spruce_production(options: Options):
    config = run_moore(options, make_turcal_spruce_prod_streams, public_tools=[])
    return config


### NOTE: THE FOLLOWING STREAMS ARE NOT FOR USAGE BY ANALYSTS
### THEY ARE "TECHNICAL" STREAMS FOR USE BY RTA/DPA CORE TEAM

###################### PASSTHROUGH SPRUCING ON NOBIAS STREAM ####################

# NoBias stream only has the HLT2NoBias line


def make_nobias_spruce_prod_streams():
    """Pass all rawbanks"""
    streams = [
        Stream(
            "nobias",
            lines=[
                PassLine(
                    name="Passnobias", hlt2_filter_code="Hlt2NoBias_NoBiasDecision"
                )
            ],
            detectors=DETECTORS,
        ),
    ]

    return Streams(streams=streams)


def nobias_spruce_production(options: Options):
    config = run_moore(options, make_nobias_spruce_prod_streams, public_tools=[])
    return config


###################### PASSTHROUGH SPRUCING ON HLT2CALIB STREAM ####################
###WIP###WIP###WIP###WIP###WIP###WIP###WIP###WIP###WIP###WIP###WIP###WIP###WIP###WIP

hlt2caliblinedict = {
    "beamgas": ["Hlt2.*BeamGas.*Decision"],
}


def make_hlt2calib_spruce_prod_streams():
    """beamgas lines go to own stream in sprucing - pass all rawbanks and include physics filter"""
    streams = [
        Stream(
            wg,
            lines=[
                PassLine(
                    name="Pass" + wg,
                    hlt2_filter_code=hlt2caliblinedict[wg],
                )
            ],
            detectors=DETECTORS,
        )
        for wg in hlt2caliblinedict
    ]
    for wg in hlt2caliblinedict.keys():
        print(
            f"Stream {wg} will contain the lines matching the regex : {hlt2caliblinedict[wg]} \n"
        )

    return Streams(streams=streams)


def hlt2calib_spruce_production(options: Options):
    config = run_moore(options, make_hlt2calib_spruce_prod_streams, public_tools=[])
    return config


###################### PASSTHROUGH SPRUCING ON LUMI STREAM ####################


##Note: `Hlt2LumiCalibration` passes `Hlt1ODIN1kHzLumiDecision` and `Hlt2LumiDefaultRawBanks` passes `Hlt1ODINLumiDecision`
def make_lumi_spruce_prod_streams():
    """Do not pass detector rawbanks"""
    streams = [
        Stream(
            "lumi",
            lines=[
                PassLine(
                    name="Passlumi",
                )
            ],
            detectors=[],
        ),
    ]

    return Streams(streams=streams)


def lumi_spruce_production(options: Options):
    config = run_moore(options, make_lumi_spruce_prod_streams, public_tools=[])
    return config
