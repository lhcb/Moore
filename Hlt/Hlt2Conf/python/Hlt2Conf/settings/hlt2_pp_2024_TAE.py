###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for Muon TAE and lumi lines only"""

from typing import Callable

from Moore.streams import Stream, Streams

from Hlt2Conf.lines.calibration.calibration import hlt2_tae_line
from Hlt2Conf.lines.luminosity import (
    calibration_lines as lumi_calibration_lines,
)
from Hlt2Conf.lines.luminosity import (
    lumi_nanofy_line,
)


def _make_streams() -> Streams:
    """Defines which trigger lines are run, which stream they belong to, and if the stream should persist raw banks."""
    full_lines = [
        hlt2_tae_line(prescale=1),
        lumi_nanofy_line(),
    ]

    lumi_lines = [b() for b in lumi_calibration_lines.values()]

    return Streams(
        streams=[
            Stream("hlt2calib", lines=full_lines, detectors=[]),
            Stream("lumi", lines=lumi_lines, detectors=[]),
        ]
    )


def make_streams(real_make_streams: Callable = _make_streams) -> Streams:
    """Wrapper around the stream maker used to apply the necessary reconstruction binds."""
    from .hlt2_binds import config_pp_2024

    with config_pp_2024():
        return real_make_streams()
