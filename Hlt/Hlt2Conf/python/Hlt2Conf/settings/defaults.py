###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.config import filter_lines as remove_lines
from PyConf import configurable

from Hlt2Conf.lines import all_lines

HLT1_PHYS_PP_LINES = r"Hlt1((?!Passthrough)(?!TAEPassthrough)(?!PassthroughLargeEvent)(?!ErrorBank)(?!Velo)(?!BeamGas)(?!BGIPseudo)(?!RICH)(?!Material)(?!ODINCalib)(?!ODINee)).*Decision"


@configurable
def get_default_hlt1_filter_code_for_hlt2(code=""):
    """Helper function to make default Hlt1 decision filter for Hlt2 lines configurable

    When set to a non-empty string a Hlt1 decision filter corresponding to the given regex is applied to every Hlt2 line, if the line does not explicitly define a filter.
    """
    return code


@configurable
def make_all_lines(
    make_lines=None, lines_to_remove=[], remove_lines_with_hlt1_filter_code=False
):
    """Helper function to build all lines for tests after removing problematic lines.

    lines_to_remove additionally removes any lines matching the regex.
    remove_lines_with_hlt1_filter_code should be enabled only for input files that are not processed by hlt1
    """
    lines = []
    if make_lines:
        lines = make_lines()
    else:
        trunc_lines = all_lines

        for l in lines_to_remove:
            trunc_lines = remove_lines(trunc_lines, l)

        lines = [builder() for builder in trunc_lines.values()]

    ## list of lines that require hlt1_filter_code
    if remove_lines_with_hlt1_filter_code:
        return list(filter(lambda line: (line.hlt1_filter_code == ""), lines))
    else:
        return lines
