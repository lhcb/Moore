###############################################################################
# (c) Copyright 2024-2025 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import sys
from contextlib import contextmanager

from Moore.lines import optimize_controlflow
from PyConf.Algorithms import (
    PrKalmanFilter,
    PrKalmanFilter_Downstream,
    PrKalmanFilter_noUT,
    PrKalmanFilter_Seed,
    PrKalmanFilter_Upstream,
    PrKalmanFilter_Velo,
)
from RecoConf.decoders import default_VeloCluster_source
from RecoConf.event_filters import require_gec
from RecoConf.hlt2_global_reco import (
    make_fastest_reconstruction,
    make_light_reco_pr_kf,
    make_light_reco_pr_kf_without_UT,
    reconstruction,
)
from RecoConf.hlt2_tracking import (
    convert_tracks_to_v3_from_v1,
    get_default_out_track_types_for_light_reco,
    get_GhostProbabilityTools,
    make_PrHybridSeeding_tracks,
    make_PrKalmanFilter_Downstream_tracks,
    make_PrKalmanFilter_noUT_tracks,
    make_PrKalmanFilter_Seed_tracks,
    make_PrKalmanFilter_tracks,
    make_PrKalmanFilter_Upstream_tracks,
    make_PrKalmanFilter_Velo_tracks,
    make_TrackBestTrackCreator_tracks,
)
from RecoConf.rich_add_reconstruction_monitoring_checking import add_hlt2_rich
from RecoConf.track_data_monitoring import get_monitoring_track_types_for_light_reco

from Hlt2Conf.settings.defaults import (
    HLT1_PHYS_PP_LINES,
    get_default_hlt1_filter_code_for_hlt2,
)

#####################
### Default binds ###
#####################


@contextmanager
def config_pp_2025():
    """ContextManagerGenerator for default 2025 proton-proton HLT2 configuration binds.

    Depending on the actual data and control flow, using this convenience function
    may emits some warnings about something not called within a bind. This is harmless
    if the corresponding component is not configured on purpose - like a default HLT1
    filter code which is not used when only the reconstruction is run.

    Use this function like this::

        with config_pp_2024():
            run_moore()
    """
    with config_pp_2025_nominal():
        yield


@contextmanager
def config_pp_2024():
    """ContextManagerGenerator for default 2024 proton-proton HLT2 configuration binds.

    Depending on the actual data and control flow, using this convenience function
    may emits some warnings about something not called within a bind. This is harmless
    if the corresponding component is not configured on purpose - like a default HLT1
    filter code which is not used when only the reconstruction is run.

    Use this function like this::

        with config_pp_2024():
            run_moore()
    """
    with config_pp_2024_with_UT():
        yield


@contextmanager
def config_pp_ref_2024():
    """ContextManagerGenerator for 2024 proton-proton reference run HLT2 configuration binds."""

    with (
        config_pp_2024(),
        config_pp_ref_2024_jet_prescales(),
        config_pp_ref_2024_charm_prescales(),
    ):
        yield


#############################
### 2025 HLT2 pp settings ###
#############################


@contextmanager
def config_pp_2025_nominal():
    """ContextManagerGenerator for nominal 2025 proton-proton HLT2 configuration binds.

    Depending on the actual data and control flow, using this convenience function
    may emits some warnings about something not called within a bind. This is harmless
    if the corresponding component is not configured on purpose - like a default HLT1
    filter code which is not used when only the reconstruction is run.

    Use this function like this::

        with config_pp_2025():
            run_moore()
    """
    # NOTE: the TBTC does not apply a chi2 cut because it is applied by the PrKF
    # an example how some of these binds are found is given in
    # https://indico.cern.ch/event/1370628/contributions/5810158/attachments/2799786/4884138/Reco_Config_Expected2024.pdf
    with (
        reconstruction.bind(make_reconstruction=make_light_reco_pr_kf),
        default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),
        require_gec.bind(skipUT=True),
        make_TrackBestTrackCreator_tracks.bind(
            max_ghost_prob=0.7, max_chi2ndof=sys.float_info.max
        ),
        make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=5.0),
        make_PrKalmanFilter_tracks.bind(max_chi2ndof=4.0),
        make_PrKalmanFilter_Downstream_tracks.bind(max_chi2ndof=6.0),
        make_PrKalmanFilter_Upstream_tracks.bind(max_chi2ndof=3.0),
        make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=6.0),
        get_default_hlt1_filter_code_for_hlt2.bind(code=HLT1_PHYS_PP_LINES),
        optimize_controlflow.bind(optimization="default"),
        make_PrHybridSeeding_tracks.bind(FasterCloneRemoval=True),
    ):
        yield


#############################
### 2024 HLT2 pp settings ###
#############################


@contextmanager
def config_pp_2024_with_UT():
    """ContextManagerGenerator for 2024 proton-proton HLT2 with UT configuration binds.

    Depending on the actual data and control flow, using this convenience function
    may emits some warnings about something not called within a bind. This is harmless
    if the corresponding component is not configured on purpose - like a default HLT1
    filter code which is not used when only the reconstruction is run.

    Use this function like this::

        with config_pp_2024_with_UT():
            run_moore()
    """
    # NOTE: the TBTC does not apply a chi2 cut because it is applied by the PrKF
    # an example how some of these binds are found is given in
    # https://indico.cern.ch/event/1370628/contributions/5810158/attachments/2799786/4884138/Reco_Config_Expected2024.pdf
    with (
        reconstruction.bind(make_reconstruction=make_light_reco_pr_kf),
        default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),
        require_gec.bind(skipUT=True),
        make_TrackBestTrackCreator_tracks.bind(
            max_ghost_prob=0.7, max_chi2ndof=sys.float_info.max
        ),
        make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=5.0),
        make_PrKalmanFilter_tracks.bind(max_chi2ndof=4.0),
        make_PrKalmanFilter_Downstream_tracks.bind(max_chi2ndof=6.0),
        make_PrKalmanFilter_Upstream_tracks.bind(max_chi2ndof=3.0),
        make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=6.0),
        get_default_hlt1_filter_code_for_hlt2.bind(code=HLT1_PHYS_PP_LINES),
        optimize_controlflow.bind(optimization="default"),
        make_PrHybridSeeding_tracks.bind(FasterCloneRemoval=True),
    ):
        yield


@contextmanager
def config_pp_2024_without_UT():
    """ContextManagerGenerator for 2024 proton-proton HLT2 w/o UT configuration binds.

    Depending on the actual data and control flow, using this convenience function
    may emits some warnings about something not called within a bind. This is harmless
    if the corresponding component is not configured on purpose - like a default HLT1
    filter code which is not used when only the reconstruction is run.

    Use this function like this::

        with config_pp_2024_without_UT():
            run_moore()
    """
    # NOTE: the TBTC does not apply a chi2 cut because it is applied by the PrKF
    # an example how some of these binds are found is given in
    # https://indico.cern.ch/event/1370628/contributions/5810158/attachments/2799786/4884138/Reco_Config_Expected2024.pdf
    with (
        reconstruction.bind(make_reconstruction=make_light_reco_pr_kf_without_UT),
        default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),
        require_gec.bind(skipUT=True),
        make_TrackBestTrackCreator_tracks.bind(
            max_ghost_prob=0.5, max_chi2ndof=sys.float_info.max
        ),
        make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=5.0),
        make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=4.0),
        make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=6.0),
        get_default_hlt1_filter_code_for_hlt2.bind(code=HLT1_PHYS_PP_LINES),
        optimize_controlflow.bind(optimization="default"),
        make_PrHybridSeeding_tracks.bind(FasterCloneRemoval=True),
    ):
        yield


@contextmanager
def config_pp_2024_with_monitoring():
    """ContextManagerGenerator for default 2024 proton-proton HLT2 configuration binds with monitoring.

    Depending on the actual data and control flow, using this convenience function
    may emits some warnings about something not called within a bind. This is harmless
    if the corresponding component is not configured on purpose - like a default HLT1
    filter code which is not used when only the reconstruction is run.

    Use this function like this::

        with config_pp_2024_with_monitoring():
            run_reconstruction()
    """
    with (
        config_pp_2024_with_UT(),
        add_hlt2_rich.bind(with_UT=True, track_types=["Long"]),
        get_default_out_track_types_for_light_reco.bind(skip_UT=False),
        get_monitoring_track_types_for_light_reco.bind(skip_UT=False),
        convert_tracks_to_v3_from_v1.bind(
            track_types=["Long", "Upstream", "Downstream", "Ttrack"]
        ),
        PrKalmanFilter.bind(FillFitResult=True),
        PrKalmanFilter_Downstream.bind(FillFitResult=True),
        PrKalmanFilter_Upstream.bind(FillFitResult=True),
        PrKalmanFilter_Seed.bind(FillFitResult=True),
        PrKalmanFilter_Velo.bind(FillFitResult=True),
    ):
        yield


###############################
### 2024 HLT2 PbPb settings ###
###############################


@contextmanager
def config_PbPb_2024_lowSMOG():
    """ContextManagerGenerator for 2024 Pb-Pb HLT2 with UT configuration binds and default reco and low prescale on the PBSMOG lines

    Depending on the actual data and control flow, using this convenience function
    may emits some warnings about something not called within a bind. This is harmless
    if the corresponding component is not configured on purpose - like a default HLT1
    filter code which is not used when only the reconstruction is run.

    Use this function like this::

        with config_PbPb_2024_lowSMOG():
            run_moore()
    """

    with config_PbPb_2024(), config_PbSMOG_prescaled():
        yield


@contextmanager
def config_PbPb_retina_2024():
    """ContextManagerGenerator for 2024 Pb-Pb HLT2 with UT configuration binds and default reco.

    Depending on the actual data and control flow, using this convenience function
    may emits some warnings about something not called within a bind. This is harmless
    if the corresponding component is not configured on purpose - like a default HLT1
    filter code which is not used when only the reconstruction is run.

    Use this function like this::

        with config_PbPb_retina_2024():
            run_moore()
    """
    # NOTE: the TBTC does not apply a chi2 cut because it is applied by the PrKF
    # an example how some of these binds are found is given in
    # https://indico.cern.ch/event/1370628/contributions/5810158/attachments/2799786/4884138/Reco_Config_Expected2024.pdf

    from PyConf.packing import persistreco_writing_version

    from Hlt2Conf.lines.trackeff.SMOG2_Ks_trackeff import (
        kshort_velo_long_line_high,
        kshort_velo_long_line_low,
        kshort_velo_long_line_veryhigh,
    )

    with (
        reconstruction.bind(make_reconstruction=make_light_reco_pr_kf),
        default_VeloCluster_source.bind(bank_type="VPRetinaCluster"),
        require_gec.bind(skipUT=True),
        get_GhostProbabilityTools.bind(collisiontype="PbPb"),
        make_TrackBestTrackCreator_tracks.bind(
            max_ghost_prob=0.25, max_chi2ndof=sys.float_info.max
        ),
        make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=5.0),
        make_PrKalmanFilter_tracks.bind(max_chi2ndof=4.0),
        make_PrKalmanFilter_Downstream_tracks.bind(max_chi2ndof=6.0),
        make_PrKalmanFilter_Upstream_tracks.bind(max_chi2ndof=3.0),
        make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=6.0),
        optimize_controlflow.bind(optimization="default"),
        persistreco_writing_version.bind(version=1.1),
        make_PrHybridSeeding_tracks.bind(FasterCloneRemoval=True),
        kshort_velo_long_line_low.bind(persistreco=False),
        kshort_velo_long_line_high.bind(persistreco=False),
        kshort_velo_long_line_veryhigh.bind(persistreco=False),
    ):
        yield


@contextmanager
def config_PbPb_2024():
    """ContextManagerGenerator for 2024 Pb-Pb HLT2 with UT configuration binds and default reco.

    Depending on the actual data and control flow, using this convenience function
    may emits some warnings about something not called within a bind. This is harmless
    if the corresponding component is not configured on purpose - like a default HLT1
    filter code which is not used when only the reconstruction is run.

    Use this function like this::

        with config_PbPb_2024():
            run_moore()
    """
    # NOTE: the TBTC does not apply a chi2 cut because it is applied by the PrKF
    # an example how some of these binds are found is given in
    # https://indico.cern.ch/event/1370628/contributions/5810158/attachments/2799786/4884138/Reco_Config_Expected2024.pdf

    from PyConf.packing import persistreco_writing_version

    from Hlt2Conf.lines.iftPbPb.SMOG2_Ks_trackeff import (
        kshort_velo_long_line_high,
        kshort_velo_long_line_veryhigh,
    )

    with (
        reconstruction.bind(make_reconstruction=make_light_reco_pr_kf),
        default_VeloCluster_source.bind(bank_type="VP"),
        require_gec.bind(skipUT=True),
        get_GhostProbabilityTools.bind(collisiontype="PbPb"),
        make_TrackBestTrackCreator_tracks.bind(
            max_ghost_prob=0.25, max_chi2ndof=sys.float_info.max
        ),
        make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=5.0),
        make_PrKalmanFilter_tracks.bind(max_chi2ndof=4.0),
        make_PrKalmanFilter_Downstream_tracks.bind(max_chi2ndof=6.0),
        make_PrKalmanFilter_Upstream_tracks.bind(max_chi2ndof=3.0),
        make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=6.0),
        optimize_controlflow.bind(optimization="default"),
        persistreco_writing_version.bind(version=1.1),
        make_PrHybridSeeding_tracks.bind(FasterCloneRemoval=True),
        kshort_velo_long_line_high.bind(persistreco=False),
        kshort_velo_long_line_veryhigh.bind(persistreco=False),
    ):
        yield


@contextmanager
def config_PbPb_2024_without_UT():
    """ContextManagerGenerator for 2024 Pb-Pb HLT2 with UT configuration binds.

    Depending on the actual data and control flow, using this convenience function
    may emits some warnings about something not called within a bind. This is harmless
    if the corresponding component is not configured on purpose - like a default HLT1
    filter code which is not used when only the reconstruction is run.

    Use this function like this::

        config_PbPb_2024_without_UT_fastreco():
            run_moore()
    """
    # NOTE: the TBTC does not apply a chi2 cut because it is applied by the PrKF
    # an example how some of these binds are found is given in
    # https://indico.cern.ch/event/1370628/contributions/5810158/attachments/2799786/4884138/Reco_Config_Expected2024.pdf

    from PyConf.packing import persistreco_writing_version

    from Hlt2Conf.lines.iftPbPb.SMOG2_Ks_trackeff import (
        kshort_velo_long_line_high,
        kshort_velo_long_line_veryhigh,
    )

    with (
        reconstruction.bind(make_reconstruction=make_light_reco_pr_kf_without_UT),
        default_VeloCluster_source.bind(bank_type="VP"),
        require_gec.bind(skipUT=True),
        get_GhostProbabilityTools.bind(without_UT=True, collisiontype="PbPb"),
        make_TrackBestTrackCreator_tracks.bind(
            max_ghost_prob=0.4, max_chi2ndof=sys.float_info.max
        ),
        make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=5.0),
        make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=4.0),
        make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=6.0),
        optimize_controlflow.bind(optimization="default"),
        persistreco_writing_version.bind(version=1.1),
        make_PrHybridSeeding_tracks.bind(FasterCloneRemoval=True),
        kshort_velo_long_line_high.bind(persistreco=False),
        kshort_velo_long_line_veryhigh.bind(persistreco=False),
    ):
        yield


@contextmanager
def config_PbPb_2024_without_UT_fastreco():
    """ContextManagerGenerator for 2024 Pb-Pb HLT2 with UT configuration binds.

    Depending on the actual data and control flow, using this convenience function
    may emits some warnings about something not called within a bind. This is harmless
    if the corresponding component is not configured on purpose - like a default HLT1
    filter code which is not used when only the reconstruction is run.

    Use this function like this::

        config_PbPb_2024_without_UT_fastreco():
            run_moore()
    """
    # NOTE: the TBTC does not apply a chi2 cut because it is applied by the PrKF
    # an example how some of these binds are found is given in
    # https://indico.cern.ch/event/1370628/contributions/5810158/attachments/2799786/4884138/Reco_Config_Expected2024.pdf

    from PyConf.packing import persistreco_writing_version

    from Hlt2Conf.lines.iftPbPb.SMOG2_Ks_trackeff import (
        kshort_velo_long_line_high,
        kshort_velo_long_line_veryhigh,
    )

    with (
        reconstruction.bind(make_reconstruction=make_fastest_reconstruction),
        make_fastest_reconstruction.bind(skipUT=True),
        default_VeloCluster_source.bind(bank_type="VP"),
        require_gec.bind(skipUT=True),
        make_TrackBestTrackCreator_tracks.bind(
            max_ghost_prob=0.4, max_chi2ndof=sys.float_info.max
        ),
        make_PrKalmanFilter_Velo_tracks.bind(max_chi2ndof=5.0),
        make_PrKalmanFilter_noUT_tracks.bind(max_chi2ndof=4.0),
        make_PrKalmanFilter_Seed_tracks.bind(max_chi2ndof=6.0),
        optimize_controlflow.bind(optimization="default"),
        persistreco_writing_version.bind(version=1.1),
        make_PrHybridSeeding_tracks.bind(FasterCloneRemoval=True),
        kshort_velo_long_line_high.bind(persistreco=False),
        kshort_velo_long_line_veryhigh.bind(persistreco=False),
    ):
        yield


@contextmanager
def config_pp_2024_without_UT_with_monitoring():
    """ContextManagerGenerator for default 2024 proton-proton HLT2 configuration binds with monitoring.

    Depending on the actual data and control flow, using this convenience function
    may emits some warnings about something not called within a bind. This is harmless
    if the corresponding component is not configured on purpose - like a default HLT1
    filter code which is not used when only the reconstruction is run.

    Use this function like this::

        with config_pp_2024_without_UT_with_monitoring():
            run_reconstruction()
    """
    with (
        config_pp_2024_without_UT(),
        add_hlt2_rich.bind(with_UT=False, track_types=["Long"]),
        get_default_out_track_types_for_light_reco.bind(skip_UT=True),
        get_monitoring_track_types_for_light_reco.bind(skip_UT=True),
        convert_tracks_to_v3_from_v1.bind(track_types=["Long", "Ttrack"]),
        PrKalmanFilter_noUT.bind(FillFitResult=True),
        PrKalmanFilter_Seed.bind(FillFitResult=True),
        PrKalmanFilter_Velo.bind(FillFitResult=True),
    ):
        yield


@contextmanager
def config_PbPb_2024_with_monitoring():
    """ContextManagerGenerator for default 2024 Pb-Pb HLT2 configuration binds with monitoring.

    Depending on the actual data and control flow, using this convenience function
    may emits some warnings about something not called within a bind. This is harmless
    if the corresponding component is not configured on purpose - like a default HLT1
    filter code which is not used when only the reconstruction is run.

    Use this function like this::

        with config_PbPb_2024_with_monitoring():
            run_reconstruction()
    """
    with (
        config_PbPb_2024(),
        add_hlt2_rich.bind(with_UT=True, track_types=["Long"]),
        get_default_out_track_types_for_light_reco.bind(skip_UT=False),
        get_monitoring_track_types_for_light_reco.bind(skip_UT=False),
        convert_tracks_to_v3_from_v1.bind(
            track_types=["Long", "Upstream", "Downstream", "Ttrack"]
        ),
        PrKalmanFilter.bind(FillFitResult=True),
        PrKalmanFilter_Downstream.bind(FillFitResult=True),
        PrKalmanFilter_Upstream.bind(FillFitResult=True),
        PrKalmanFilter_Seed.bind(FillFitResult=True),
        PrKalmanFilter_Velo.bind(FillFitResult=True),
    ):
        yield


@contextmanager
def config_PbPb_2024_without_UT_with_monitoring():
    """ContextManagerGenerator for default 2024 proton-proton HLT2 configuration binds with monitoring.

    Depending on the actual data and control flow, using this convenience function
    may emits some warnings about something not called within a bind. This is harmless
    if the corresponding component is not configured on purpose - like a default HLT1
    filter code which is not used when only the reconstruction is run.

    Use this function like this::

        with config_PbPb_2024_without_UT_with_monitoring():
            run_reconstruction()
    """
    with (
        config_PbPb_2024_without_UT(),
        add_hlt2_rich.bind(with_UT=False, track_types=["Long"]),
        get_default_out_track_types_for_light_reco.bind(skip_UT=True),
        get_monitoring_track_types_for_light_reco.bind(skip_UT=True),
        convert_tracks_to_v3_from_v1.bind(track_types=["Long", "Ttrack"]),
        PrKalmanFilter_noUT.bind(FillFitResult=True),
        PrKalmanFilter_Seed.bind(FillFitResult=True),
        PrKalmanFilter_Velo.bind(FillFitResult=True),
    ):
        yield


@contextmanager
def config_PbPb_2024_fastreco_without_UT_with_monitoring():
    """ContextManagerGenerator for fastreco 2024 Pb-Pb HLT2 configuration binds with monitoring.

    Depending on the actual data and control flow, using this convenience function
    may emits some warnings about something not called within a bind. This is harmless
    if the corresponding component is not configured on purpose - like a default HLT1
    filter code which is not used when only the reconstruction is run.

    Use this function like this::

        with config_PbPb_2024_fastreco_without_UT_with_monitoring():
            run_reconstruction()
    """
    with (
        config_PbPb_2024_without_UT_fastreco(),
        add_hlt2_rich.bind(with_UT=False, track_types=["Long"]),
        get_default_out_track_types_for_light_reco.bind(skip_UT=True),
        get_monitoring_track_types_for_light_reco.bind(skip_UT=True),
        convert_tracks_to_v3_from_v1.bind(
            track_types=["Long", "Upstream", "Downstream", "Ttrack"]
        ),
        PrKalmanFilter_noUT.bind(FillFitResult=True),
        PrKalmanFilter_Seed.bind(FillFitResult=True),
        PrKalmanFilter_Velo.bind(FillFitResult=True),
    ):
        yield


@contextmanager
def config_pp_ref_2024_jet_prescales():
    from Hlt2Conf.lines.qee.jets import (
        IncDiJet25GeV_line,
        IncDiJet30GeV_line,
        IncJet15GeV_line,
        IncJet25GeV_line,
        IncJet35GeV_line,
        diSVTagJet10GeV_line,
        diSVTagJet15GeV_line,
        diSVTagJet20GeV_line,
        diSVTagJet25GeV_line,
        diSVTagJet30GeV_line,
    )

    with (
        IncJet15GeV_line.bind(prescale=0.01),
        IncJet25GeV_line.bind(prescale=0.1),
        IncJet35GeV_line.bind(prescale=1),
        IncDiJet25GeV_line.bind(prescale=1),
        IncDiJet30GeV_line.bind(prescale=1),
        diSVTagJet10GeV_line.bind(prescale=0.01),
        diSVTagJet15GeV_line.bind(prescale=0.05),
        diSVTagJet20GeV_line.bind(prescale=0.1),
        diSVTagJet25GeV_line.bind(prescale=0.5),
        diSVTagJet30GeV_line.bind(prescale=1),
    ):
        yield


@contextmanager
def config_pp_ref_2024_charm_prescales():
    from Hlt2Conf.lines.charm.prod_xsec import (
        dplus2kkpi_line,
        dplus2kpipi_line,
        dstar2dzeropi_dzero2ksDDkk_line,
        dstar2dzeropi_dzero2ksDDpipi_line,
        dstar2dzeropi_dzero2ksLLkk_line,
        dstar2dzeropi_dzero2ksLLpipi_line,
        dstarp2dzeropip_dzero2k3pi_line,
        dstarp2dzeropip_dzero2kmpip_line,
        dzero2k3pi_line,
        dzero2kpi_line,
        dzero2ksDDkk_line,
        dzero2ksDDpipi_line,
        dzero2ksLLkk_line,
        dzero2ksLLpipi_line,
        lcp2pkpi_line,
        xic02pkkpi_line,
        xicp2pkpi_line,
    )

    with (
        dzero2kpi_line.bind(prescale=1),
        dstarp2dzeropip_dzero2kmpip_line.bind(prescale=1),
        dzero2k3pi_line.bind(prescale=1),
        dstarp2dzeropip_dzero2k3pi_line.bind(prescale=1),
        dplus2kpipi_line.bind(prescale=1),
        dplus2kkpi_line.bind(prescale=1),
        lcp2pkpi_line.bind(prescale=1),
        xicp2pkpi_line.bind(prescale=1),
        xic02pkkpi_line.bind(prescale=1),
        dzero2ksLLpipi_line.bind(prescale=1),
        dzero2ksDDpipi_line.bind(prescale=1),
        dzero2ksLLkk_line.bind(prescale=1),
        dzero2ksDDkk_line.bind(prescale=1),
        dstar2dzeropi_dzero2ksLLpipi_line.bind(prescale=1),
        dstar2dzeropi_dzero2ksDDpipi_line.bind(prescale=1),
        dstar2dzeropi_dzero2ksLLkk_line.bind(prescale=1),
        dstar2dzeropi_dzero2ksDDkk_line.bind(prescale=1),
    ):
        yield


@contextmanager
def config_PbSMOG_prescaled():
    from Hlt2Conf.lines.iftPbPb.hlt2_ift_smog2Pb_ChargedPID import (
        smog2_ks2pipi_dd_line,
        smog2_ks2pipi_ll_highpt_line,
        smog2_ks2pipi_ll_lowpt_line,
        smog2_L02ppi_dd_line,
        smog2_L02ppi_ll_highpt_line,
        smog2_L02ppi_ll_line,
        smog2_phi2kk_kmProbe_highpt_line,
        smog2_phi2kk_kmProbe_line,
        smog2_phi2kk_kpProbe_highpt_line,
        smog2_phi2kk_kpProbe_line,
    )
    from Hlt2Conf.lines.iftPbPb.hlt2_ift_smog2Pb_charm import (
        D02kkline,
        D02kpiline,
        D02pipiline,
        Dpm2kpipiline,
        Ds2kkpiline,
        Etac2ppbarline,
        Hiddencharm2pi2k_line,
        Hiddencharm4k_line,
        Hiddencharm4piline,
        Lc2pkpiline,
        Omegac02pkkpiline,
        Xic02pkkpiline,
        Xicp2pkpiline,
    )
    from Hlt2Conf.lines.iftPbPb.hlt2_ift_smog2Pb_muons import (
        smog2_DY2mumu_line,
        smog2_DY2mumu_SS_line,
        smog2_DY2mumuExcludeCCBarHigh_line,
        smog2_DY2mumuExcludeCCBarIntermediate_line,
        smog2_DY2mumuExcludeCCBarLow_line,
        smog2_Jpsi2mumu_line,
        smog2_Jpsi2mumu_SS_line,
        smog2_lowdimuon_line,
        smog2_lowdimuon_SS_line,
        smog2_Ups2mumu_line,
        smog2_Ups2mumu_SS_line,
    )
    from Hlt2Conf.lines.iftPbPb.hlt2_ift_smog2Pb_omegas import (
        smog2_omega2lambdak_lll_line,
    )
    from Hlt2Conf.lines.iftPbPb.hlt2_ift_smog2Pb_xis import smog2_xi2lambda0pi_lll_line
    from Hlt2Conf.lines.iftPbPb.hlt2_iftPbPb import (
        hlt2_PbSMOG_hadronic_line,
        hlt2_PbSMOG_minbias_line,
    )
    from Hlt2Conf.lines.pid.SMOG2_JpsiToMuMuTagged import (
        smog2_Jpsi2mumumtagged_line,
        smog2_Jpsi2mumuptagged_line,
    )
    from Hlt2Conf.lines.trackeff.SMOG2_Jpsi_trackeff import (
        LineWithMatching,
        LineWithTagging,
    )
    from Hlt2Conf.lines.trackeff.SMOG2_Ks_trackeff import (
        kshort_velo_long_line_high,
        kshort_velo_long_line_low,
        kshort_velo_long_line_veryhigh,
    )

    @contextmanager
    def charm_bind():
        with (
            D02kpiline.bind(prescale=0.5),
            D02kkline.bind(prescale=0.025),
            D02pipiline.bind(prescale=0.0025),
            Etac2ppbarline.bind(prescale=0.5),
            Dpm2kpipiline.bind(prescale=0.5),
            Ds2kkpiline.bind(prescale=0.5),
            Lc2pkpiline.bind(prescale=0.5),
            Xicp2pkpiline.bind(prescale=0.5),
            Xic02pkkpiline.bind(prescale=0.5),
            Omegac02pkkpiline.bind(prescale=0.5),
            Hiddencharm4piline.bind(prescale=0.0005),
            Hiddencharm2pi2k_line.bind(prescale=0.0005),
            Hiddencharm4k_line.bind(prescale=0.0005),
        ):
            yield

    @contextmanager
    def dimuon_bind():
        with (
            smog2_lowdimuon_line.bind(prescale=0.2),
            smog2_lowdimuon_SS_line.bind(prescale=0.2),
            smog2_Jpsi2mumu_line.bind(prescale=1.0),
            smog2_Jpsi2mumu_SS_line.bind(prescale=1.0),
            smog2_Ups2mumu_line.bind(prescale=1.0),
            smog2_Ups2mumu_SS_line.bind(prescale=1.0),
            smog2_DY2mumu_line.bind(prescale=1.0),
            smog2_DY2mumu_SS_line.bind(prescale=1.0),
            smog2_DY2mumuExcludeCCBarLow_line.bind(prescale=1),
            smog2_DY2mumuExcludeCCBarIntermediate_line.bind(prescale=1),
            smog2_DY2mumuExcludeCCBarHigh_line.bind(prescale=1),
        ):
            yield

    @contextmanager
    def omega_xi_bind():
        with (
            smog2_omega2lambdak_lll_line.bind(prescale=0.5),
            smog2_xi2lambda0pi_lll_line.bind(prescale=0.5),
        ):
            yield

    @contextmanager
    def minbias_bind():
        with (
            hlt2_PbSMOG_hadronic_line.bind(prescale=0.025),
            hlt2_PbSMOG_minbias_line.bind(prescale=0.5),
        ):
            yield

    @contextmanager
    def calib_bind():
        with (
            LineWithTagging.bind(prescale=0.5),
            LineWithMatching.bind(prescale=0.5),
            kshort_velo_long_line_high.bind(prescale=0.1),
            kshort_velo_long_line_low.bind(prescale=0.005),
            kshort_velo_long_line_veryhigh.bind(prescale=0.5),
            smog2_ks2pipi_ll_lowpt_line.bind(prescale=0.01),
            smog2_ks2pipi_ll_highpt_line.bind(prescale=0.5),
            smog2_ks2pipi_dd_line.bind(prescale=0.005),
            smog2_L02ppi_ll_line.bind(prescale=0.01),
            smog2_L02ppi_ll_highpt_line.bind(prescale=0.5),
            smog2_L02ppi_dd_line.bind(prescale=0.01),
            smog2_phi2kk_kmProbe_line.bind(prescale=0.05),
            smog2_phi2kk_kpProbe_line.bind(prescale=0.05),
            smog2_phi2kk_kmProbe_highpt_line.bind(prescale=0.5),
            smog2_phi2kk_kpProbe_highpt_line.bind(prescale=0.5),
            smog2_Jpsi2mumumtagged_line.bind(prescale=0.5),
            smog2_Jpsi2mumuptagged_line.bind(prescale=0.5),
        ):
            yield

    with charm_bind(), dimuon_bind(), omega_xi_bind(), minbias_bind(), calib_bind():
        yield
