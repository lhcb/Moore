###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore.streams import DETECTORS, Stream, Streams

# hlt2calib
from Hlt2Conf.lines.calibration import all_lines as hlt2_calibration_lines

# pidline PbAr (PbPb is done offline)
from Hlt2Conf.lines.iftPbPb.hlt2_ift_smog2Pb_ChargedPID import all_lines as chargedPid
from Hlt2Conf.lines.iftPbPb.hlt2_ift_smog2Pb_charm import all_lines as charm_lines
from Hlt2Conf.lines.iftPbPb.hlt2_ift_smog2Pb_generic import all_lines as generic_lines
from Hlt2Conf.lines.iftPbPb.hlt2_ift_smog2Pb_muons import all_lines as muon_lines
from Hlt2Conf.lines.iftPbPb.hlt2_ift_smog2Pb_omegas import all_lines as omega_lines
from Hlt2Conf.lines.iftPbPb.hlt2_ift_smog2Pb_xis import all_lines as xi_lines
from Hlt2Conf.lines.iftPbPb.hlt2_iftPbPb import ion_lines, ionraw_lines

# Tracking line PbAr (PbPb is done offline)
from Hlt2Conf.lines.iftPbPb.SMOG2_Jpsi_trackeff import all_lines as trackingeff_lines
from Hlt2Conf.lines.iftPbPb.SMOG2_JpsiToMuMuTagged import all_lines as jpsiPID
from Hlt2Conf.lines.iftPbPb.SMOG2_Ks_trackeff import all_lines as trackingeff_lines_Ks
from Hlt2Conf.lines.luminosity import (
    calibration_lines as lumi_calibration_lines,
)
from Hlt2Conf.lines.luminosity import (
    lumi_nanofy_line,
)

# DQ monitoring lines
from Hlt2Conf.lines.monitoring.data_quality_PbPb import all_lines as monitoring_lines
from Hlt2Conf.lines.nobias.nobias_PbPb import all_lines as hlt2_no_bias_lines
from Hlt2Conf.settings.hlt2_binds import config_PbPb_2024
from Hlt2Conf.settings.hlt2_PbPb_default import _make_lines_and_require_gec

from .hlt2_pp_2024 import _make_lines

all_lines_ionraw = {}
all_lines_ionraw.update(ionraw_lines)

all_lines_ion = {}
all_lines_ion.update(ion_lines)
all_lines_ion.update(charm_lines)
all_lines_ion.update(muon_lines)
all_lines_ion.update(omega_lines)
all_lines_ion.update(xi_lines)
all_lines_ion.update(generic_lines)
# calib lines
all_lines_ion.update(trackingeff_lines)
all_lines_ion.update(trackingeff_lines_Ks)
all_lines_ion.update(chargedPid)
all_lines_ion.update(jpsiPID)


def _make_streams():
    ion_lines = _make_lines_and_require_gec(
        all_lines_ion
    ) + _make_lines_and_require_gec(monitoring_lines)

    ionraw_lines = _make_lines(all_lines_ionraw)

    lumi_nano_line = lumi_nanofy_line()

    hlt2calib_lines = _make_lines(hlt2_calibration_lines) + [
        lumi_nano_line,
    ]

    lumi_lines = _make_lines(lumi_calibration_lines)

    no_bias_lines = _make_lines(hlt2_no_bias_lines) + [
        lumi_nano_line,
        next((l for l in lumi_lines if l.name == "Hlt2LumiCounters")),
    ]

    return Streams(
        streams=[
            Stream("ion", lines=[lumi_nano_line] + ion_lines, detectors=[]),
            Stream(
                "ionraw", lines=[lumi_nano_line] + ionraw_lines, detectors=DETECTORS
            ),
            Stream("hlt2calib", lines=hlt2calib_lines, detectors=[]),
            Stream("lumi", lines=lumi_lines, detectors=[]),
            Stream("no_bias", lines=no_bias_lines, detectors=[]),
        ]
    )


from typing import Callable


def make_streams(real_make_streams: Callable = _make_streams) -> Streams:
    with config_PbPb_2024():
        return real_make_streams()
