###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from typing import Callable

from Moore.config import filter_lines
from Moore.lines import Hlt2Line
from Moore.streams import DETECTORS, Stream, Streams

from Hlt2Conf.lines.b_to_charmonia import all_lines as b_to_charmonia_lines
from Hlt2Conf.lines.b_to_open_charm import (
    all_calib_lines as b_to_open_charm_calib_lines,
)
from Hlt2Conf.lines.b_to_open_charm import (
    all_lines as b_to_open_charm_lines,
)
from Hlt2Conf.lines.bandq import (
    hlt2_full_lines as bandq_full_lines,
)
from Hlt2Conf.lines.bandq import (
    hlt2_turbo_lines as bandq_turbo_lines,
)
from Hlt2Conf.lines.bnoc import (
    full_lines as bnoc_full_lines,
)
from Hlt2Conf.lines.bnoc import (
    hlt2_lines as bnoc_turbo_lines,
)
from Hlt2Conf.lines.calibration import all_lines as hlt2_calibration_lines
from Hlt2Conf.lines.charm import all_lines as charm_lines
from Hlt2Conf.lines.charmonium_to_dimuon import (
    full_lines as charmonium_to_dimuon_prompt_full_lines,
)
from Hlt2Conf.lines.charmonium_to_dimuon import (
    turbo_lines as charmonium_to_dimuon_prompt_turbo_lines,
)
from Hlt2Conf.lines.charmonium_to_dimuon_detached import (
    all_lines as charmonium_to_dimuon_detached_lines,
)
from Hlt2Conf.lines.ift import ift_full_lines, ift_turbo_lines
from Hlt2Conf.lines.inclusive_detached_dilepton import (
    all_lines as inclusive_detached_dilepton_lines,
)
from Hlt2Conf.lines.luminosity import (
    calibration_lines as lumi_calibration_lines,
)
from Hlt2Conf.lines.luminosity import (
    lumi_nanofy_line,
)
from Hlt2Conf.lines.monitoring import all_lines as monitoring_lines
from Hlt2Conf.lines.nobias import all_lines as hlt2_no_bias_lines
from Hlt2Conf.lines.pid import all_lines as pid_lines
from Hlt2Conf.lines.qee import (
    hlt2_full_lines as qee_full_lines,
)
from Hlt2Conf.lines.qee import (
    hlt2_turbo_lines as qee_turbo_lines,
)
from Hlt2Conf.lines.rd import (
    full_lines as rd_full_lines,
)
from Hlt2Conf.lines.rd import (
    turbo_lines as rd_turbo_lines,
)
from Hlt2Conf.lines.semileptonic import all_lines as semileptonic_lines
from Hlt2Conf.lines.topological_b import all_lines as topological_b_lines
from Hlt2Conf.lines.trackeff import (
    turbo_lines as trackeff_turbo_lines,
)
from Hlt2Conf.lines.trackeff import (
    turcal_lines as trackeff_turcal_lines,
)


def _make_lines(*lines_dicts: dict[str, Callable]) -> list[Hlt2Line]:
    """Helper to remove unsupported lines and invoke the line builder.

    Args:
        lines_dicts (Tuple[dict[str, Callable], ...]): Line to dictionaries to build lines from.

    Returns:
        list[Hlt2Line]: List of HLT2 trigger lines.
    """

    # for removing lines that are incompatible with running conditions
    to_remove = []

    all_lines = []
    for lines in lines_dicts:
        trunc_lines = lines
        for remove in to_remove:
            trunc_lines = filter_lines(trunc_lines, remove)
        if len(lines) != len(trunc_lines):
            from logging import getLogger

            getLogger(__name__).info(
                "Incompatible lines have been removed: %s",
                lines.keys() - trunc_lines.keys(),
            )
        all_lines += [builder() for builder in trunc_lines.values()]
    return all_lines


# TODO: It's a temporary solution to stream these events to hlt2calb.
# They should never enter any stream but be moved somewhere directly from the buffer.
# See discussion in https://gitlab.cern.ch/lhcb/Moore/-/issues/735
def _hlt2_large_event_line(name: str = "Hlt2LargeEvent", prescale: float = 1):
    """Passthrough for events too large for HLT1 processing."""
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code="Hlt1PassthroughLargeEventDecision",
        raw_banks=["VP", "UT", "FT", "Rich", "Muon", "Calo", "Plume"],
        prescale=prescale,
    )


def _make_streams() -> Streams:
    """Defines which trigger lines are run, which stream they belong to, and if the stream should persist raw banks.

    Raises:
        RuntimeError: If lines are misconfigured. Currently only checks for persistreco config in Full and Turcal.

    Returns:
        Streams: Object containing all streams with their corresponding trigger lines and raw banks.
    """
    lumi_nano_line = lumi_nanofy_line()

    full_lines = _make_lines(
        topological_b_lines,
        inclusive_detached_dilepton_lines,
        charmonium_to_dimuon_detached_lines,
        charmonium_to_dimuon_prompt_full_lines,
        b_to_open_charm_calib_lines,
        rd_full_lines,
        qee_full_lines,
        semileptonic_lines,
        bnoc_full_lines,
        ift_full_lines,
        bandq_full_lines,
    ) + [lumi_nano_line]

    turbo_lines = _make_lines(
        b_to_open_charm_lines,
        rd_turbo_lines,
        bandq_turbo_lines,
        qee_turbo_lines,
        charm_lines,
        b_to_charmonia_lines,
        charmonium_to_dimuon_prompt_turbo_lines,
        bnoc_turbo_lines,
        ift_turbo_lines,
        trackeff_turbo_lines,
    ) + [lumi_nano_line]

    turcal_lines = _make_lines(
        pid_lines,
        trackeff_turcal_lines,
        monitoring_lines,
    ) + [lumi_nano_line]

    lumi_lines = _make_lines(lumi_calibration_lines)

    # Hlt2LumiCounters can only be invoked once, so we take it from
    # the list of lumi lines
    # FIXME: Hlt2LumiCounters triggers on Hlt1ODINLumiDecision but
    # nobias uses Hlt1ODIN1kHzLumiDecision
    # https://gitlab.cern.ch/lhcb/Moore/-/issues/745
    no_bias_lines = _make_lines(hlt2_no_bias_lines) + [
        lumi_nano_line,
        next((l for l in lumi_lines if l.name == "Hlt2LumiCounters")),
    ]

    hlt2calib_lines = _make_lines(hlt2_calibration_lines) + [
        lumi_nano_line,
        _hlt2_large_event_line(),
    ]

    # make sure persistreco is true for full and turcal lines
    problems = []
    for l in full_lines:
        if isinstance(l, Hlt2Line) and not l.persistreco:
            problems.append(
                f"Line {l.name} is in the FULL stream"
                + "and must have persistreco=True"
            )
    for l in turcal_lines:
        if isinstance(l, Hlt2Line) and not l.persistreco:
            problems.append(
                f"Line {l.name} is in the TURCAL stream"
                + "and must have persistreco=True"
            )
    if problems:
        raise RuntimeError("Misconfigured lines found:\n" + "\n".join(problems))
    # NOTE: hlt2calib configures raw bank persistency by line
    return Streams(
        streams=[
            Stream("full", lines=full_lines, detectors=[]),
            Stream(
                "turbo", lines=turbo_lines, detectors=[]
            ),  # Note any entry in detectors here needs to be discussed with DPA WP1
            Stream("turcal", lines=turcal_lines, detectors=DETECTORS),
            Stream("lumi", lines=lumi_lines, detectors=[]),
            Stream("no_bias", lines=no_bias_lines, detectors=[]),
            Stream("hlt2calib", lines=hlt2calib_lines, detectors=[]),
        ]
    )


def make_streams(real_make_streams: Callable = _make_streams) -> Streams:
    """Wrapper around the stream maker used to apply the necessary reconstruction binds.

    Args:
        real_make_streams (Callable, optional): Function defining the streams. Defaults to _make_streams.

    Returns:
        Streams: Object containing all streams with their corresponding trigger lines and raw banks.
    """
    from .hlt2_binds import config_pp_2024

    with config_pp_2024():
        return real_make_streams()
