###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Options for the van der Meer scan in Nov. 2022"""

from Moore.lines import Hlt2Line
from Moore.streams import DETECTORS, Stream, Streams

from Hlt2Conf.lines.luminosity import (
    calibration_lines as lumi_calibration_lines,
)
from Hlt2Conf.lines.luminosity import (
    lumi_nanofy_line,
)


def _hlt2_beamgas_line(name="Hlt2BeamGas"):
    """
    - Hlt1ODIN*Lumi is removed (the nanofication line will select these events)
    """
    return Hlt2Line(
        name=name,
        algs=[],
        hlt1_filter_code=r"^Hlt1(?!ODINLumi|ODIN1kHzLumi).*Decision",
    )


def make_streams():
    lumi_line = lumi_nanofy_line()
    beamgas_lines = [
        _hlt2_beamgas_line(),
        lumi_line,
    ]
    lumi_lines = [b() for b in lumi_calibration_lines.values()]

    beamgas_stream = Stream(
        name="beamgas",
        lines=beamgas_lines,
        detectors=["VP"],
    )
    lumi_stream = Stream(
        name="lumi",
        lines=lumi_lines,
        detectors=DETECTORS,
    )

    return Streams(streams=[beamgas_stream, lumi_stream])
