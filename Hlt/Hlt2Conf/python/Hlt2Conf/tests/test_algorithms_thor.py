###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test ThOr selection algorithm wrappers correctly validate input types."""

import Functors as F
import pytest
from PyConf.packing import persistreco_version
from RecoConf import algorithms_thor as thor
from RecoConf.standard_particles import make_long_cb_pions

persistreco_version.global_bind(version=0.0)

from Moore import options
from PyConf.application import configure_input

# minimum config to be able to configure
options.simulation = True
options.input_type = "Root"
options.input_files = ["fake"]
options.dddb_tag = "v1"
options.conddb_tag = "v1"


def test_v1_filter_v2_inputs():
    """A v1 filter cannot be instantiated with v2 input."""
    configure_input(options)
    v2_pions = make_long_cb_pions()
    with thor.thor_backend.bind(particle_api=thor.PARTICLE_V1):
        with pytest.raises(TypeError):
            thor.ParticleFilter(v2_pions, F.ALL)


def test_v1_combiner_v2_inputs():
    """A v1 combiner cannot be instantiated with v2 input."""
    configure_input(options)
    v2_pions = make_long_cb_pions()
    with thor.thor_backend.bind(particle_api=thor.PARTICLE_V1):
        with pytest.raises(TypeError):
            thor.ParticleCombiner(
                [v2_pions, v2_pions],
                DecayDescriptor="KS0 -> pi+ pi-",
                CombinationCut=F.ALL,
                CompositeCut=F.ALL,
            )
