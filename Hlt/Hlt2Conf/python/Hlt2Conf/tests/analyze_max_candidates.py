#!/usr/bin/env python
###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import argparse
import json
import re

import pandas as pd


def parseAlgorithmCounters(log_fn, AlgToLines):
    """
    Parse the log file to extract algorithm counters and their associated data.

    This function reads a log file and extracts relevant counters for algorithms with
    "Too many composites/combinations" warnings. The counters collect are:
    - Trigger lines associated with the algorithm
    - Algorithm name
    - Number of times the algorithm was executed
    - Number of times the algorithm passed
    - Number of successful combinations
    - Number of successful composites
    - Number of composite warnings
    - Number of combination warnings

    Args:
        log_fn (str): The filename of the log file to parse.
        AlgToLines (dict): A dictionary mapping algorithms to their associated trigger lines.

    Returns:
        list: A list of lists, where each inner list contains the extracted data for an algorithm.
    """
    with open(log_fn, "r") as log_file:
        lines = log_file.readlines()

    # Find the line numbers of all algorithm counters and the number of counters
    counter_indices = [
        [i, re.findall(r"\b\d+\b", line)[-1]]
        for i, line in enumerate(lines)
        if " Number of counters" in line
    ]

    data = []
    for i_count, [line_no, n_counters] in enumerate(counter_indices):
        saveValues = False
        values = {}
        # Loops over all the counters of the algorithm starting from 'Number of counters' line and ending at the last counter,
        # +2 in range() because of the heading of the counter table
        for i in range(line_no, line_no + int(n_counters) + 2):
            bits = lines[i].split("|")
            bits = [bit.strip() for bit in bits]
            if len(bits) > 1:
                name = bits[1]
                if "passed" in bits[1]:
                    name = bits[1].replace('*"#', "").replace(" ", "").replace('"', "")
                if "Counter" in bits[1]:
                    continue
                values[name] = bits[2:-1]

            if "Too many composites" in lines[i]:
                saveValues = True
            if "Too many combinations" in lines[i]:
                saveValues = True

        if saveValues is False:
            continue
        if "passed" not in values.keys():
            continue
        algorithm = lines[line_no].split(" ")[0]

        trigLines = AlgToLines.get(algorithm, "Unknown")

        output = [
            ", ".join(trigLines) if type(trigLines) == list else trigLines,
            algorithm,
            int(values["passed"][0]),  # n_executed
            int(values["passed"][1]),  # n_passed
            int(values["passedCombinationCut"][0]),
            int(values["passedCompositeCut"][0]),
        ]
        for too_many in ("composites", "combinations"):
            try:
                output.append(
                    int(values[f'"Too many {too_many}. Stopping the combination."'][0])
                )
            except KeyError:
                output.append(0)

        data.append(output)

    return data


def get_number_of_events(log_fn):
    with open(log_fn, "r") as log_file:
        file_string = log_file.read()

    tot_num = [
        match
        for match in re.finditer(
            r"LAZY_AND: moore.*#=([\d.]+).*Sum=([\d.]+)", file_string
        )
    ]
    if len(tot_num) < 1:
        print(f"Cannot obtain total number of events in {log_file}.")
        return 0, 0
    if len(tot_num) > 1:
        print(f"More than one `LAZY_AND: moore` detected in {log_file}.")
        return 0, 0
    tot_evt = float(tot_num[0].group(1))
    inc_evt = float(tot_num[0].group(2))

    return tot_evt, inc_evt


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--log_file", type=str)
    args = parser.parse_args()
    if args.log_file:
        log_file = args.log_file
    else:
        log_file = "MaxCombinerTest.log"

    with open("AlgToLines.json", "r") as f:
        AlgToLines = json.load(f)

    data = parseAlgorithmCounters(log_file, AlgToLines)

    df = pd.DataFrame(
        data,
        columns=[
            "Lines",
            "Combiner",
            "N_executed",
            "N_passed",
            "passedCombinationCut",
            "passedCompositeCut",
            "NCompositeWarnings",
            "NCombinationWarnings",
        ],
    )

    df.sort_values(
        by="NCompositeWarnings",
        key=df["NCombinationWarnings"].add,
        inplace=True,
        ascending=False,
    )

    df.to_csv("MaxCombinerTest.csv")
    df.to_html("MaxCombinerTest.html", index=False)

    tot_evt, inc_evt = get_number_of_events(log_file)

    with open("jobSettings.json", "r") as f:
        settings = json.load(f)

    settings["total_events"] = tot_evt
    settings["inc_events"] = inc_evt
    if tot_evt != 0:
        settings["total_rate"] = inc_evt / tot_evt

    with open("jobSettings.json", "w") as f:
        json.dump(settings, f)
