###############################################################################
# (c) Copyright 2023-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Takes the config file describing the Hlt2 job's input file.
Generates a metadata yaml about the FULL stream output of the Hlt2 job.
This metadata can then be used as a config file for a Spruce job's input file.
"""

import argparse
import json

import yaml
from Hlt2Conf.tests.bandwidth.bandwidth_helpers import (
    FULL_STREAM_LINES_KEY,
    FileNameHelper,
    parse_yaml,
)
from PRConfig.TestFileDB import test_file_db


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c",
        "--config",
        type=str,
        required=True,
        help="Path to yaml config file defining the input.",
    )
    parser.add_argument("--stream-config", type=str, required=True)
    parser.add_argument("--stream", type=str, required=True)
    args = parser.parse_args()
    fname_helper = FileNameHelper(process="hlt2", stream_config=args.stream_config)
    config = parse_yaml(args.config)

    opts = {"input_raw_format": config["input_raw_format"]}
    for optional in ["velo_radial_opening", "nu", "mu"]:
        if optional in config.keys():
            opts[optional] = config[optional]
    opts["filtering_info_links"] = [
        FileNameHelper.gitlab_config_webdir + "/" + args.config.split("/")[-1]
    ]
    opts["input_type"] = "MDF"  # We always write MDF out of HLT2

    try:
        conds = test_file_db[config["testfiledb_key"]].qualifiers
        opts["dddb_tag"] = conds["DDDB"]
        opts["conddb_tag"] = conds["CondDB"]
        opts["simulation"] = conds["Simulation"]
        opts["geometry_version"] = conds["GeometryVersion"]
        opts["conditions_version"] = conds["ConditionsVersion"]
        opts["data_type"] = conds["DataType"]
    except KeyError:
        # input wasn't in testfileDB
        for k in [
            "geometry_version",
            "dddb_tag",
            "conddb_tag",
            "conditions_version",
            "simulation",
            "data_type",
        ]:
            if k in config.keys():
                opts[k] = config[k]

    ifile = fname_helper.event_no_fname(stream=args.stream)
    with open(ifile, "r") as f:
        # json = {stream: [evt_numbers]}
        n_triggered = len(json.load(f)[args.stream])

    n_hlt2_input = int(parse_yaml(fname_helper.input_info_json())["n_evts"])
    input_rate = config["input_rate"]
    opts["input_rate"] = round((input_rate * n_triggered / n_hlt2_input), 4)

    # Load up the streaming configuration and therefore get the list of full-stream lines
    # Needed for TISTOS
    if args.stream == "full":
        with open(fname_helper.stream_config_json_path(full_path=True), "r") as f:
            opts[FULL_STREAM_LINES_KEY] = json.load(f)["full"]

    # Now split into outputs for two files - one to allow reading of HLT2 output that lives on eos, and one for reading from local disk
    # Metadata with 'eos' file paths.
    # Use local path for manifest file and download it in spruce_latest jobs
    # Can be reverted once TurboPass sprucing can access non-local input_manifests
    opts["input_files"] = [
        fname_helper.mdfdst_fname_for_reading(
            stream=args.stream, ext=".mdf", on_eos=True, full_path=False
        )
    ]
    opts["input_manifest_file"] = fname_helper.tck(
        on_eos=False, full_path=True
    )  # will be downloaded to here
    spruce_config_to_put_on_eos = fname_helper.config_file_path(
        stream=args.stream, send_to_eos=True
    )
    with open(spruce_config_to_put_on_eos, "w") as f:
        yaml.dump(opts, f, default_flow_style=False)

    # Metadata with 'local' file paths.
    local_opts = opts
    local_opts["input_files"] = [
        fname_helper.mdfdst_fname_for_reading(
            stream=args.stream, ext=".mdf", on_eos=False, full_path=True
        )
    ]
    local_opts["input_manifest_file"] = fname_helper.tck(on_eos=False, full_path=True)
    spruce_config_for_local_use = fname_helper.config_file_path(
        stream=args.stream, send_to_eos=False
    )

    with open(spruce_config_for_local_use, "w") as f:
        yaml.dump(local_opts, f, default_flow_style=False)


if __name__ == "__main__":
    main()
