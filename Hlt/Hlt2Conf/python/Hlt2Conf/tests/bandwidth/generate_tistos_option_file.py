###############################################################################
# (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse

from Hlt2Conf.tests.bandwidth.bandwidth_helpers import (
    FULL_STREAM_LINES_KEY,
    FileNameHelper,
    parse_yaml,
)


def full_stream_content(config):
    # get list of lines from yaml config
    tistos_content = ""
    if "fixed_list_of_full_stream_lines" in config.keys():
        tistos_content += f"from Hlt2Conf.sprucing_settings.fixed_line_configs import {config['fixed_list_of_full_stream_lines']} as lines_for_TISTOS"
    elif FULL_STREAM_LINES_KEY in config.keys():
        tistos_content += f"lines_for_TISTOS = {config[FULL_STREAM_LINES_KEY]}"
    else:
        raise ValueError(
            f"Expected either fixed_list_of_full_stream_lines or {FULL_STREAM_LINES_KEY} in the config file."
        )

    return [
        tistos_content,
        "from Moore.persistence.hlt2_tistos import list_of_full_stream_lines",
        "list_of_full_stream_lines.global_bind(lines=lines_for_TISTOS)",
    ]


def main():
    """
    Generate the option file for TISTOS for the Spruce job.
    Read input config: either get a fixed key for a list of lines
    stored in Moore, or we get a list of lines in the file.
    """
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument(
        "-c",
        "--config",
        type=str,
        required=True,
        help="Path to yaml config file defining the input.",
    )
    parser.add_argument("--stream-config", type=str, required=True)
    args = parser.parse_args()
    config = parse_yaml(args.config)

    if args.stream_config == "full":
        print("Setting list of full stream lines as stream-config == full.")
        content = full_stream_content(config)
    else:
        print("Not setting a list of full stream lines as stream-config != full.")
        content = []

    output_fname_helper = FileNameHelper("spruce", args.stream_config)
    ofile = output_fname_helper.tistos_option_file()
    with open(ofile, "w") as f:
        f.write("\n".join(content))


if __name__ == "__main__":
    main()
