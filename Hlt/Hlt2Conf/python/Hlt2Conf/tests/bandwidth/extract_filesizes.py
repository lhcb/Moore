###############################################################################
# (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import json
import os
import subprocess

from Hlt2Conf.tests.bandwidth.bandwidth_helpers import (
    KNOWN_STREAM_CONFIGS,
    FileNameHelper,
)


def extract_filesizes(process, stream_config, streams):
    """
    Extracts the compression scale factor per output mdf file.
        scale_factor range: [0, 1]
        scale_factor = compressed_size / uncompressed_size
    """
    fname_helper = FileNameHelper(process, stream_config)
    filesizes = {}
    compression_cmd = "zstd -f {fpath}"
    fsize = lambda fpath: os.stat(fpath).st_size  # in Bytes
    for stream in streams:
        fpath = fname_helper.mdfdst_fname_for_reading(
            stream=stream,
            full_path=True,
            ext=".mdf" if process in ["hlt1", "hlt2"] else ".dst",
        )
        filesizes[stream] = {"default": fsize(fpath)}

        # Unsure if Hlt1 is compressed by data movers so for now we do nothing.
        # This doesn't affect Rate anyway which is the important parameter at Hlt1 in this test.
        if process == "hlt2":
            # .mdf requires compression by data movers
            sp_compress = subprocess.run(
                compression_cmd.format(fpath=fpath), shell=True
            )
            if sp_compress.returncode != 0:
                raise RuntimeError(
                    f"Returned {sp_compress.returncode} during compressing {fpath}."
                )
            filesizes[stream]["compressed"] = fsize(fpath + ".zst")

    with open(fname_helper.filesize_path(), "w") as ofile:
        json.dump(filesizes, ofile, indent=2)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-p",
        "--process",
        type=str,
        help="Compute for Hlt1, Hlt2 or Sprucing output",
        choices=["hlt1", "hlt2", "spruce"],
        required=True,
    )
    parser.add_argument(
        "--stream-config", type=str, choices=KNOWN_STREAM_CONFIGS, required=True
    )
    parser.add_argument("--streams", nargs="+", type=str, required=True)

    args = parser.parse_args()

    extract_filesizes(
        process=args.process, stream_config=args.stream_config, streams=args.streams
    )
