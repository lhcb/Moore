#!/usr/bin/env python
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os

import yaml

KNOWN_WORKING_GROUPS = [
    "B2CC",
    "B2OC",
    "BandQ",
    "BnoC",
    "Calib",
    "Calo",
    "Charm",
    "DPA",
    "HLT",
    "IFT",
    "Luminosity",
    "PID",
    "QCD",
    "QEE",
    "RD",
    "RTA",
    "SLB",
    "Topo",
    "Tagging",
    "TrackEff",
    "Monitoring",
    "CutBasedDiLep",
    "InclDetDiLep",
    "TurCalPID",
]

CUSTOM_WGS = {
    "Topo": ["Hlt2Topo"],
    "CutBasedDiLep": ["Hlt2CutBasedIncl"],
    "InclDetDiLep": ["Hlt2_InclDet"],
    "BandQ": [
        "Hlt2_DiMuonJPsiTight",
        "Hlt2_DiMuonPsi2STight",
        "Hlt2_JpsiToMuMu",
        "Hlt2_Psi2SToMuMu",
    ],
    "Charm": ["Hlt2HadInt"],
    "TrackEff": [
        "Hlt2TurboVelo2Long_Kshort",
        "Hlt2TurCalVelo2Long_Kshort",
        "Hlt2Tracking",
        "Hlt2TrackEff",
        "Hlt2TurCalTrackEff",
    ],
    "IFT": ["Hlt2IFTFull", "Hlt2IFTTurbo"],
}

FULL_STREAM_LINES_KEY = "full_stream_lines"

KNOWN_STREAM_CONFIGS_BY_STAGE = {
    "hlt1": ["streamless"],
    "hlt2": ["production"],
    "spruce": ["full", "turbo", "turcal", "no_bias", "hlt2calib", "lumi"],
}
KNOWN_STREAM_CONFIGS = [
    sc for sc_list in KNOWN_STREAM_CONFIGS_BY_STAGE.values() for sc in sc_list
]
# to be able to cope with large events
# https://gitlab.cern.ch/lhcb-datapkg/PRConfig/-/issues/35#note_8723098
IOALG_KNOWN_BUFFER_EVENTS = os.getenv("IOALG_BUFFER_EVENTS", default=300)


def guess_wg(line_name, process):
    # First, expect it follows naming convention e.g. <Hlt2,Spruce><WG>_MyLine
    line_prefix = line_name.split("_")[0].removeprefix(process.capitalize())
    if line_prefix in KNOWN_WORKING_GROUPS:
        return line_prefix

    for custom_wg, guesses in CUSTOM_WGS.items():
        for guess in guesses:
            if line_name.startswith(guess):
                return custom_wg

    return "Other"  # If you made it here, all guesses failed


def parse_yaml(file_path):
    with open(os.path.expandvars(file_path), "r") as f:
        return yaml.safe_load(f)


class FileNameHelper(object):
    # See: Moore_bandwidth_tests.sh in PRConfig for an explanation of the environment variables needed to use this class
    base_dir = os.getenv("BASEDIR", default="tmp")
    mdf_subdir = "MDF"
    output_subdir = "Output"
    inter_subsubdir = "Inter"
    to_eos_subdir = "to_eos"
    gitlab_config_webdir = "https://gitlab.cern.ch/lhcb/Moore/-/blob/master/Hlt/Hlt2Conf/tests/options/bandwidth"
    current_hlt2_output_dir = "root://eoslhcb.cern.ch//eos/lhcb/storage/lhcbpr/www/UpgradeRateTest/current_hlt2_output"

    def __init__(self, process, stream_config):
        self.process = process
        self.stream_config = stream_config

    @staticmethod
    def _join(*bits):
        return "__".join(bits)

    def _file_pfx(self):
        return f"{self.process}_bw_testing"

    def _incomplete_mdfdst_fname(self, ext=".mdf", full_path=True):
        fname = self._join(self._file_pfx(), self.stream_config, "{stream_bit}") + ext
        return (
            os.path.join(self.base_dir, self.mdf_subdir, fname) if full_path else fname
        )

    def _prwww_path(self, fname, starts_mdf):
        baseurl = (
            f"mdf:{self.current_hlt2_output_dir}"
            if starts_mdf
            else self.current_hlt2_output_dir
        )
        return os.path.join(baseurl, fname)

    @staticmethod
    def input_type_to_file_ext(input_type):
        return {"ROOT": ".dst", "MDF": ".mdf"}[input_type]

    def make_tmp_dirs(self):
        mdf_dir = os.path.join(self.base_dir, self.mdf_subdir)
        output_dir = os.path.join(self.base_dir, self.output_subdir)
        for diro in (mdf_dir, output_dir):
            if not os.path.exists(diro):
                os.system(f"mkdir -p {diro}")

    def mdfdst_fname_for_Moore(self, ext=".mdf"):
        return self._incomplete_mdfdst_fname(ext).format(stream_bit="{stream}")

    def mdfdst_fname_for_reading(
        self, stream, ext=".mdf", on_eos=False, full_path=True
    ):
        basic_path = self._incomplete_mdfdst_fname(ext, full_path=full_path).format(
            stream_bit=stream
        )
        if on_eos:
            return self._prwww_path(basic_path, starts_mdf=("mdf" in ext))
        else:
            return basic_path

    def get_stream_from_bw_path(self, bw_file_path):
        # useful for globbing
        # all the BW files share the same separator, so should work for all
        stream_and_ext_bit = bw_file_path.split("__")[-1]
        return stream_and_ext_bit.split(".")[0]

    def tck(self, on_eos=False, full_path=True):
        fname = self._join(self._file_pfx(), self.stream_config) + ".tck.json"
        basic_path = (
            os.path.join(self.base_dir, self.mdf_subdir, fname) if full_path else fname
        )
        if on_eos:
            assert not full_path, "File paths on eos dont have directory structure"
            return self._prwww_path(basic_path, starts_mdf=False)
        else:
            return basic_path

    def streams_attributes_file(self, full_path=True):
        fname = self._join(self._file_pfx(), "streams_attributes") + ".json"
        return (
            os.path.join(self.base_dir, self.mdf_subdir, fname) if full_path else fname
        )

    def stream_config_json_path(self, full_path=True):
        fname = self._join(self._file_pfx(), "streaming", self.stream_config) + ".json"
        return (
            os.path.join(self.base_dir, self.mdf_subdir, fname) if full_path else fname
        )

    def line_descriptor_json_path(self, full_path=True):
        fname = (
            self._join(self._file_pfx(), self.stream_config, "line_descriptives")
            + ".json"
        )
        return (
            os.path.join(self.base_dir, self.mdf_subdir, fname) if full_path else fname
        )

    def config_file_path(self, stream: str, send_to_eos: bool):
        assert self.process == "hlt2"
        fname = f"spruce_bandwidth_latest_input__{stream}.yaml"
        if send_to_eos:
            return os.path.join(self.base_dir, self.to_eos_subdir, fname)
        else:
            return os.path.join(self.base_dir, fname)

    def tistos_option_file(self, full_path=True):
        fname = (
            self._join(self.process, "tistos_option_file", self.stream_config) + ".py"
        )
        return (
            os.path.join(self.base_dir, self.mdf_subdir, fname) if full_path else fname
        )

    def input_info_json(self):
        return (
            os.path.join(
                self.base_dir,
                self.mdf_subdir,
                self._join(self._file_pfx(), self.stream_config, "input_info"),
            )
            + ".json"
        )

    def filesize_path(self):
        fname = self._join(self.process, self.stream_config, "filesizes") + ".json"
        return os.path.join(self.base_dir, self.output_subdir, fname)

    def line_descr_path(self, full_path=True):
        fname = (
            self._join(self.process, self.stream_config, "line_descriptives") + ".html"
        )
        return (
            os.path.join(self.base_dir, self.output_subdir, fname)
            if full_path
            else fname
        )

    def jaccard_similarities_path(self):
        return os.path.join(
            self.base_dir,
            self.output_subdir,
            self._join(self.process, self.stream_config, "jaccard_similarity_matrix")
            + ".html",
        )

    def overlap_matrix_path(self):
        return os.path.join(
            self.base_dir,
            self.output_subdir,
            self._join(self.process, self.stream_config, "overlap_matrix") + ".html",
        )

    def intra_stream_jaccard_similarities_path(self, stream):
        return os.path.join(
            self.base_dir,
            self.output_subdir,
            self._join(
                self.process, self.stream_config, stream, "jaccard_similarity_matrix"
            )
            + ".html",
        )

    def intra_stream_overlap_matrix_path(self, stream):
        return os.path.join(
            self.base_dir,
            self.output_subdir,
            self._join(self.process, self.stream_config, stream, "overlap_matrix")
            + ".html",
        )

    def event_no_fname(self, stream):
        return os.path.join(
            self.base_dir,
            self.output_subdir,
            self._join(self.process, "event_numbers", self.stream_config, stream)
            + ".json",
        )

    def intra_stream_event_no_fname(self, stream):
        return os.path.join(
            self.base_dir,
            self.output_subdir,
            self._join(
                self.process, "event_numbers_intra_stream", self.stream_config, stream
            )
            + ".json",
        )

    def _tmp_rate_table_path(self, stream, line_or_stream):
        return os.path.join(
            self.base_dir,
            self.output_subdir,
            self.inter_subsubdir,
            self._join(
                self.process, f"rates_per_{line_or_stream}", self.stream_config, stream
            )
            + ".csv",
        )

    def tmp_rate_table_per_line_path(self, stream):
        return self._tmp_rate_table_path(stream, "line")

    def tmp_rate_table_per_stream_path(self, stream):
        return self._tmp_rate_table_path(stream, "stream")

    def tmp_rate_table_intra_stream_path(self, stream):
        return os.path.join(
            self.base_dir,
            self.output_subdir,
            self.inter_subsubdir,
            self._join(
                self.process, f"rates_wgs_within_{stream}", self.stream_config, stream
            )
            + ".csv",
        )

    def final_rate_table_all_lines_path(self, ext="html", full_path=True):
        fname = (
            self._join(self.process, self.stream_config, "rates_for_all_lines")
            + f".{ext}"
        )
        return (
            os.path.join(self.base_dir, self.output_subdir, fname)
            if full_path
            else fname
        )

    def final_rate_table_all_lines_split_by_stream_path(self, full_path=True):
        fname = (
            self._join(
                self.process,
                self.stream_config,
                "rates_for_all_lines_split_by_stream",
            )
            + ".html"
        )
        return (
            os.path.join(self.base_dir, self.output_subdir, fname)
            if full_path
            else fname
        )

    def final_rate_table_all_streams_path(self, ext="html", full_path=True):
        fname = (
            self._join(self.process, self.stream_config, "rates_for_all_streams")
            + f".{ext}"
        )
        return (
            os.path.join(self.base_dir, self.output_subdir, fname)
            if full_path
            else fname
        )

    def final_rate_table_all_lines_split_by_stream_by_wg_path(self, full_path=True):
        fname = (
            self._join(
                self.process,
                self.stream_config,
                "rates_for_all_lines_split_by_stream_by_wg",
            )
            + ".html"
        )
        return (
            os.path.join(self.base_dir, self.output_subdir, fname)
            if full_path
            else fname
        )

    ### Below helpers only used in make_bandwidth_test_page.py
    @staticmethod
    def base_html_path(running_locally: bool):
        return (
            "."
            if running_locally
            else "https://cern.ch/lhcbpr-hlt/UpgradeRateTest/$$dirname$$"
        )

    @classmethod
    def _full_path(cls, fname):
        return os.path.join(cls.base_dir, cls.output_subdir, fname)

    def _plot_path(self, fname, ext="png", full_path=False):
        fname = self._join(self.process, self.stream_config, fname) + f".{ext}"
        return self._full_path(fname) if full_path else fname

    def pie_chart_path(self, full_path=False):
        return self._plot_path("lines_per_wg", full_path=full_path)

    def hist_path(self, hist_suffix, full_path=False):
        return self._plot_path(f"hist__{hist_suffix}", full_path=full_path)

    def bar_chart_path(self, stream, metric, full_path=False):
        return self._plot_path(f"{stream}__bar_chart__{metric}", full_path=full_path)

    def headline_bar_chart_path(self, full_path=False):
        return self.bar_chart_path("all", "bandwidth", full_path=full_path)

    def to_disk_bar_chart_path(self, full_path=False):
        return self.bar_chart_path("all_to_disk", "bandwidth", full_path=full_path)

    def _extra_html_page_path(self, page_name, full_path):
        fname = self._join(self.process, self.stream_config, page_name) + ".html"
        return self._full_path(fname) if full_path else fname

    def other_lines_html_page_path(self, full_path=False):
        return self._extra_html_page_path("other_lines", full_path)

    def all_rates_html_page_path(self, full_path=False):
        return self._extra_html_page_path("all_rates", full_path)

    def sim_matrices_html_page_path(self, full_path=False):
        return self._extra_html_page_path("similarity_matrices", full_path)

    def extra_bar_charts_html_page_path(self, full_path=False):
        return self._extra_html_page_path("extra_bar_charts", full_path)

    def extra_sim_matrices_html_page_path(self, full_path=False):
        return self._extra_html_page_path("extra_similarity_matrices", full_path)

    def comparison_str(self):
        return self._join(self.process, self.stream_config, "comparison")

    def index_html_page_path(self, full_path=False):
        return self._extra_html_page_path("index", full_path)

    @classmethod
    def top_level_index_html_path(cls):
        return cls._full_path("index.html")

    @classmethod
    def message_path(cls):
        return cls._full_path("message.json")


def bandwidth_qmtest_validator():
    import json

    causes = []
    message_fpath = FileNameHelper.message_path()
    try:
        with open(message_fpath, "r") as f:
            messages = json.load(f)
    except:
        causes.append("The tests failed to complete, indicates a serious error.")

    for subprocess in messages:
        if subprocess in KNOWN_STREAM_CONFIGS_BY_STAGE:
            for stream_config, code in messages[subprocess].items():
                if subprocess == "hlt2" and code["code"] == 1:
                    # "Misordered large cluster" errors, although ignored, cause a bad status code here.
                    # We therefore must allow this bad error code until the errors go away, unfortunately.
                    continue

                if code["code"] != 0:
                    causes.append(
                        f"The {subprocess}.{stream_config} subjob returned {code['code']}; Please check the failing subjob in the log."
                    )
        else:
            # Should be the make_html_page subprocess
            if messages[subprocess]["code"] != 0:
                causes.append(
                    f"The {subprocess} subjob returned {messages[subprocess]['code']}; Please check the failing subjob in the log."
                )
    return causes
