###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Compare event numbers extracted from mdf outputs of different streams to
quantify the overlap between the streams by Jaccard similarity index.
Writes similarity matrix to console and out to .html for usage in BW test page.
"""

import argparse
import json

import pandas as pd
from Hlt2Conf.tests.bandwidth.bandwidth_helpers import (
    KNOWN_STREAM_CONFIGS,
    FileNameHelper,
)
from sklearn.metrics.pairwise import pairwise_distances


def get_all_event_numbers(args):
    fname_helper = FileNameHelper(args.process, args.stream_config)
    ret = {}
    for stream in args.streams:
        with open(fname_helper.event_no_fname(stream), "r") as f:
            ret.update(json.load(f))
    return ret


def get_event_number_matrix(event_numbers):
    all_event_numbers = set(
        [
            event_info
            for event_info_list in event_numbers.values()
            for event_info in event_info_list
        ]
    )
    print(
        f"Found {len(all_event_numbers)} unique event numbers across {len(event_numbers.keys())} categories/streams."
    )

    df = pd.DataFrame(
        False, index=list(all_event_numbers), columns=event_numbers.keys()
    )
    for stream, event_info_list in event_numbers.items():
        for event_info in event_info_list:
            df[stream][event_info] = True

    return df


def calculate_similarity_matrix(df):
    if df.empty:
        # return a dummy matrix so things dont fail
        jaccard = [[0 for _ in range(len(df.columns))] for _ in range(len(df.columns))]
    else:
        jaccard = (
            1 - pairwise_distances(df.T.to_numpy(), metric="jaccard")
        )  # .T bcuz pairwise_distance must expect the fields to take similarity between to be rows rather than columns
    jaccard_sim_matrix_df = pd.DataFrame(jaccard, columns=df.columns, index=df.columns)

    return jaccard_sim_matrix_df


def calculate_overlap_matrix(df):
    cond_prob_per_stream = {stream: [] for stream in df.columns}
    for target_stream in df.columns:
        for comparison_stream in df.columns:
            cond_prob_per_stream[target_stream].append(
                sum(df[comparison_stream] * df[target_stream]) / sum(df[target_stream])
                if sum(df[target_stream]) > 0
                else 0
            )
    overlap_matrix_df = pd.DataFrame(
        cond_prob_per_stream, columns=df.columns, index=df.columns
    )
    return overlap_matrix_df


def save(df, htmlpath):
    # Generate HTML table for similarity matrix
    html = df.to_html(float_format=lambda x: f"{x:.1%}")
    with open(htmlpath, "w") as f:
        f.write(html)


def main():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help="Mode of execution", dest="mode")
    inter_stream_parser = subparsers.add_parser("inter_stream")
    inter_stream_parser.add_argument("--streams", nargs="+", type=str, required=True)

    intra_stream_parser = subparsers.add_parser("intra_stream")
    intra_stream_parser.add_argument("--stream", type=str, required=True)
    for sp in [inter_stream_parser, intra_stream_parser]:
        sp.add_argument(
            "-p",
            "--process",
            type=str,
            choices=["hlt1", "hlt2", "spruce"],
            required=True,
        )
        sp.add_argument(
            "--stream-config", type=str, choices=KNOWN_STREAM_CONFIGS, required=True
        )
    args = parser.parse_args()

    fname_helper = FileNameHelper(args.process, args.stream_config)

    if args.mode == "inter_stream":
        similarities_path = fname_helper.jaccard_similarities_path()
        overlaps_path = fname_helper.overlap_matrix_path()
        event_numbers = get_all_event_numbers(args)
    elif args.mode == "intra_stream":
        similarities_path = fname_helper.intra_stream_jaccard_similarities_path(
            args.stream
        )
        overlaps_path = fname_helper.intra_stream_overlap_matrix_path(args.stream)
        with open(fname_helper.intra_stream_event_no_fname(args.stream), "r") as f:
            event_numbers = json.load(f)

    df = get_event_number_matrix(event_numbers)

    sim_matrix = calculate_similarity_matrix(df)
    print(
        f"Calculated similarity matrix. Printing and saving to html at {similarities_path}."
    )
    print(sim_matrix)
    save(sim_matrix, similarities_path)

    overlap_matrix = calculate_overlap_matrix(df)
    print(f"Calculated overlap matrix. Printing and saving to html at {overlaps_path}.")
    print(overlap_matrix)
    save(overlap_matrix, overlaps_path)


if __name__ == "__main__":
    main()
