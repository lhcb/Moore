###############################################################################
# (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Simple script that returns if a line has persist reco
and/or extra output flag enabled. Saves to .html.
Relies upon this information being saved to JSON during the Moore job.
"""

import argparse
import json

import pandas as pd
from Hlt2Conf.tests.bandwidth.bandwidth_helpers import (
    KNOWN_STREAM_CONFIGS,
    FileNameHelper,
)


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "-p",
        "--process",
        type=str,
        help="Compute for Hlt2 or Sprucing lines. Not applicable to HLT1",
        choices=["hlt2", "spruce"],
        required=True,
    )
    parser.add_argument(
        "--stream-config", type=str, choices=KNOWN_STREAM_CONFIGS, required=True
    )
    parser.add_argument("--streams", type=str, nargs="+", required=True)
    args = parser.parse_args()

    fname_helper = FileNameHelper(args.process, args.stream_config)
    with open(fname_helper.line_descriptor_json_path(), "r") as f:
        line_descriptives = json.load(f)

    dfs = {}
    for stream in args.streams:
        print(f'Tabulating line descriptives for "{stream}" stream.')
        df = pd.DataFrame(
            index=line_descriptives[stream].keys(),
            columns=["PersistReco", "ExtraOutputs"],
        )
        for line in df.index.to_list():
            for descr in ["PersistReco", "ExtraOutputs"]:
                df[descr][line] = line_descriptives[stream][line][descr]
        dfs[stream] = df

    html_page = """
    <html>
    <head>
    <body>
    <p> Persist reco and extra outputs for each line in each stream </p>
    <p>Jump to:\n<ul>
    """
    for stream in args.streams:
        html_page += f'<li><a href="#{stream}_label">{stream.upper()}</a></li>'
    html_page += "</ul>\n</p>"

    for stream in args.streams:
        html_page += f"<head>{stream.upper()}</head>"
        html_page += f'<a id="{stream}_label">'
        html_page += dfs[stream].to_html(justify="left")
        html_page += "</a>"
        html_page += "<br/><br/>"

    html_page += """
    </body>
    </html>
    """

    with open(fname_helper.line_descr_path(), "w") as f:
        f.write(html_page)
    print(f"Written line descriptives to {fname_helper.line_descr_path()}.")


if __name__ == "__main__":
    main()
