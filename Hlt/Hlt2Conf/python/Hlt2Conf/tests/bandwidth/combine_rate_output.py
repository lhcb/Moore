###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Each method works the same: reads in all relevant CSV files
and combines into single dataframe.
It writes out CSV and HTML for each dataframe.
"""

import argparse
import glob

import pandas as pd
from Hlt2Conf.tests.bandwidth.bandwidth_helpers import (
    KNOWN_STREAM_CONFIGS,
    FileNameHelper,
    guess_wg,
)

COLUMNS_PER_STREAM = [
    "Stream",
    "Total Retention (%)",
    "Rate (kHz)",
    "Avg Total Event Size (kB)",
    "Total Bandwidth (GB/s)",
    "Avg DstData Size (kB)",
    "DstData Bandwidth (GB/s)",
]


def _columns_per_line(process):
    tols = {
        # Tolerances per process.
        "hlt1": (1e3, None, 150, 0, 0),
        "hlt2": (1, 1e3, 0.2, 1e3, 0.2),
        "spruce": (1, 1e3, 0.2, 1e3, 0.2),
    }[process]
    return {
        # col_name, threshold for turning it red to catch the reader's eye
        "Line": None,
        "Total Retention (%)": None,
        "Rate (kHz)": tols[0],
        "Exclusive Retention(%)": None,
        "Exclusive Rate (kHz)": None,
        "Avg Total Event Size (kB)": tols[1],
        "Total Bandwidth (GB/s)": tols[2],
        "Avg DstData Size (kB)": tols[3],
        "DstData Bandwidth (GB/s)": tols[4],
    }


def _sorted_df_by_bandwidth(df):
    return df.sort_values(by=["Total Bandwidth (GB/s)"], ascending=False).reset_index(
        drop=True
    )


def _read_csv(file):
    try:
        return pd.read_csv(file, header=None)
    except pd.errors.EmptyDataError:
        print(f"Empty file: {file} - likely no triggers.")
        return pd.DataFrame()


def table_per_line(fname_helper: FileNameHelper):
    """Make 1 enormous table with rate/bw info per line for all lines in all streams (i.e. n_rows = n_lines).
    Saves to .csv and .html.
    """

    frames = []

    for file in glob.glob(fname_helper.tmp_rate_table_per_line_path("*")):
        df = _read_csv(file)
        if not df.empty:
            frames.append(df)

    try:
        df = pd.concat(frames)
        df.columns = _columns_per_line(fname_helper.process).keys()
        df = _sorted_df_by_bandwidth(df)
    except ValueError:
        print("No triggers found for any lines. Saving empty .csv and .html.")
        df = pd.DataFrame(columns=_columns_per_line(fname_helper.process).keys())

    df.to_csv(fname_helper.final_rate_table_all_lines_path("csv"))

    def highlight_vals(val, threshold, color="red"):
        return f"background-color: {color}" if val > threshold else ""

    styler = None
    for column, threshold in _columns_per_line(fname_helper.process).items():
        # Make cell red if column value greater than threshold
        if threshold:
            if styler:
                styler = styler.applymap(
                    highlight_vals, subset=[column], threshold=threshold
                )
            else:
                styler = df.style.applymap(
                    highlight_vals, subset=[column], threshold=threshold
                )

    html = (
        styler.format("{:.3g}", subset=df.columns[df.columns != "Line"])
        .set_table_attributes("border=1")
        .to_html()
    )
    with open(fname_helper.final_rate_table_all_lines_path("html"), "w") as f:
        f.write(html)

    return


def table_per_line_per_stream(fname_helper: FileNameHelper):
    """Makes (1 table with rate/bw info per line in the streamed mdf) for all <stream_config> streams (i.e. n_tables = n_streams).
    Puts them all on 1 html page, adds hyperlinks to jump to the different streams on the page.
    Saves to .html page only.
    """

    with open(fname_helper.final_rate_table_all_lines_split_by_stream_path(), "w") as f:
        files = glob.glob(fname_helper.tmp_rate_table_per_line_path("*"))
        files_by_stream = {
            fname_helper.get_stream_from_bw_path(file): file for file in files
        }
        f.write("<head></head>\n<body>\n")
        f.write("""
        <p>
            Rates, event sizes and bandwidths of all lines in each stream, listed descending in bandwidth. <br>
            Exclusive retentions/rates are calculated by counting those events in which only that line fired. <br>
            Bandwidths are inclusive: they are calculated by summing raw bank sizes for those events in which the trigger line fired. <br>
        </p>
        """)
        f.write("<p>Jump to:\n<ul>")
        for stream in files_by_stream.keys():
            f.write(f'<li><a href="#{stream}_label"> {stream.upper()}</a></li>')
        f.write("</ul>\n</p>")

        for stream, file in files_by_stream.items():
            f.write(f"<head>{stream.upper()}</head>")
            f.write(f'<a id="{stream}_label">')
            df = _read_csv(file)
            if not df.empty:
                df.columns = _columns_per_line(fname_helper.process).keys()
                df = _sorted_df_by_bandwidth(df)
                f.write(
                    df.style.format("{:.3g}", subset=df.columns[df.columns != "Line"])
                    .set_table_attributes("border=1")
                    .to_html()
                )
            f.write("</a>")
            f.write("<br/><br/>")

    return


def table_per_stream(fname_helper: FileNameHelper):
    """Makes 1 table with rate/bw info integrated over the whole streamed mdf for all <fname_helper.stream_config> streams (i.e. a table with n_rows = n_streams).
    Saves to .html and .csv.
    """

    frames = []
    for file in glob.glob(fname_helper.tmp_rate_table_per_stream_path("*")):
        df = _read_csv(file)
        frames.append(df)

    try:
        df = pd.concat(frames)
        df.columns = COLUMNS_PER_STREAM
        df = _sorted_df_by_bandwidth(df)
    except ValueError:
        print("No triggers found for any streams. Saving empty .csv and .html.")
        df = pd.DataFrame(columns=COLUMNS_PER_STREAM)

    df.to_csv(fname_helper.final_rate_table_all_streams_path(ext="csv"))

    html = (
        df.style.format("{:.3g}", subset=df.columns[df.columns != "Stream"])
        .set_table_attributes("border=1")
        .to_html()
    )
    with open(fname_helper.final_rate_table_all_streams_path(ext="html"), "w") as f:
        f.write(html)

    return


def table_per_line_per_wg_per_stream(fname_helper: FileNameHelper):
    """Makes html page with 1 table per stream and per WG, with rows being per line within that WG and stream giving rate/bw,
    Saves to .html.
    """

    files = glob.glob(fname_helper.tmp_rate_table_per_line_path("*"))
    files_by_stream = {
        fname_helper.get_stream_from_bw_path(file): file for file in files
    }
    htmls_per_stream_and_per_wg = {stream: {} for stream in files_by_stream.keys()}
    for stream, file in files_by_stream.items():
        stream_df = _read_csv(file)
        if stream_df.empty:
            continue
        stream_df.columns = _columns_per_line(fname_helper.process)
        stream_df["WG"] = stream_df.apply(
            lambda row: guess_wg(row["Line"], fname_helper.process), axis=1
        )
        for wg in sorted(stream_df["WG"].unique()):
            wg_df = stream_df.loc[stream_df["WG"] == wg]
            wg_df = _sorted_df_by_bandwidth(wg_df)
            wg_df = wg_df.drop(columns="WG")

            htmls_per_stream_and_per_wg[stream][wg] = (
                wg_df.style.format(
                    "{:.3g}", subset=wg_df.columns[~wg_df.columns.isin(["Line", "WG"])]
                )
                .set_table_attributes("border=1")
                .to_html()
            )

    with open(
        fname_helper.final_rate_table_all_lines_split_by_stream_by_wg_path(), "w"
    ) as f:
        f.write("<head></head>\n<body>\n")
        f.write("""
        <p>
            Rates, event sizes and bandwidths of all lines in each stream, split also by WG, listed descending in bandwidth. <br>
            Exclusive retentions/rates are calculated by counting those events in which only that line fired. <br>
            Bandwidths are inclusive: they are calculated by summing raw bank sizes for those events in which the trigger line fired. <br>
        </p>
        """)
        f.write("<p>Jump to:\n<ul>")
        for stream, htmls in htmls_per_stream_and_per_wg.items():
            f.write(f'<li><a href="#{stream}_label"> {stream.upper()}</a></li>')
            f.write("<ul>")
            for wg in htmls.keys():
                f.write(f'<li><a href="#{stream}_{wg}_label"> {wg}</a></li>')
            f.write("</ul>")
        f.write("</ul>\n</p>")

        for stream, htmls in htmls_per_stream_and_per_wg.items():
            f.write("<p>")
            f.write(f"<head>{stream.upper()}</head>")
            f.write(f'<a id="{stream}_label">')
            f.write("<p/>")

            for wg, html in htmls.items():
                f.write(f"<head>{wg}</head>")
                f.write(f'<a id="{stream}_{wg}_label">')
                f.write(html)
                f.write("</a>")
                f.write("<br/><br/>")

            f.write("</a>")
            f.write("<br/><br/>")

    return


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Combine output to make final rate tables per line, per stream, per stream and WG etc."
    )
    parser.add_argument(
        "-p",
        "--process",
        type=str,
        help="Compute for Hlt1, Hlt2 or Sprucing lines",
        choices=["hlt1", "hlt2", "spruce"],
        required=True,
    )
    parser.add_argument(
        "--stream-config", type=str, choices=KNOWN_STREAM_CONFIGS, required=True
    )
    args = parser.parse_args()

    fname_helper = FileNameHelper(args.process, args.stream_config)
    for func in [
        table_per_line,
        table_per_stream,
        table_per_line_per_stream,
        table_per_line_per_wg_per_stream,
    ]:
        func(fname_helper)
