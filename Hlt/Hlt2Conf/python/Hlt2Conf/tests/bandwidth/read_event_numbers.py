###############################################################################
# (c) Copyright 2022-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import argparse
import json

from Gaudi.Main import gaudimain
from Hlt2Conf.tests.bandwidth.bandwidth_helpers import (
    IOALG_KNOWN_BUFFER_EVENTS,
    KNOWN_STREAM_CONFIGS,
    FileNameHelper,
)
from PyConf.Algorithms import EventNumberCollectAlg
from PyConf.application import ApplicationOptions, configure, configure_input, make_odin
from PyConf.control_flow import CompositeNode


def _count_evts_mdf(filename):
    """
    This method scans an MDF file to count the number of events
    It uses knowledge of the structure of the MDF to jump from event to event
    """
    with open(filename, "rb") as f:
        n = 0
        while True:
            # try to read event size
            s = int.from_bytes(f.read(4), byteorder="little")
            if s == 0:
                break  # end of file
            # skip to next event
            if len(f.read(s - 4)) != s - 4:
                break  # end of file, event was cut
            # increase counter, the event was complete
            n += 1
    return n


def _extract_nevts_root(filename):
    import ROOT

    with ROOT.TFile.Open(filename, "READ") as f:
        tree = f.Get("Event")
        return tree.GetEntriesFast()


def _count_evts(ifiles, file_type, evt_max):
    """
    Count all events in a list of MDF/ROOT files

    HACK
    Dont know how to easily read the number of events in a .raw file,
    Hack it for now - use evt_max (will only currently be .raw for spruce
    nominal, which has a capped EVTMAX).
    Once we can actually count these, remove evt_max as arg here.
    """
    if file_type == "RAW":
        return evt_max

    if file_type == "MDF":
        count_method = _count_evts_mdf
    else:
        count_method = _extract_nevts_root
    count = 0
    for f in ifiles:
        count += count_method(f)
    return count


def _extract_evt_numbers(ifiles, file_type, stream=None, evt_max=-1):
    """
    Extract all event numbers saved to a single MDF/DST.
    """
    # configure input - the tags are arbitrary in this use case, but must be provided
    options = ApplicationOptions(_enabled=False)
    options.simulation = True
    options.dddb_tag = "dddb-20171126"
    options.conddb_tag = "sim-20171127-vc-md100"
    options.geometry_version = "run3/2024.Q1.2-v00.00"
    options.conditions_version = "master"
    options.input_type = file_type
    options.input_files = ifiles
    options.evt_max = evt_max
    options.ioalg_buffer_nb_events = IOALG_KNOWN_BUFFER_EVENTS
    if stream:
        options.input_stream = stream
    config = configure_input(options)
    # configure data flow

    odin = make_odin()
    collector = EventNumberCollectAlg(
        name="EventNumberCollectAlg",
        ODINLocation=odin,
        JSONFileName="EventNumbers.json",
    )
    cf_node = CompositeNode("TopAlg", [odin, collector])
    config.update(configure(options, cf_node))
    # run the application counting events
    gaudimain().run(False)
    # get back the count from the json file
    data = json.load(open("EventNumbers.json"))
    return data


def main():
    """
    For a given stream's MDF/DST output, finds all event_numbers and saves them in a json file for later use.
    Useful for similarity between streams later.
    """
    parser = argparse.ArgumentParser(description=main.__doc__)
    subparsers = parser.add_subparsers(help="Mode of execution", dest="mode")
    event_numbers_parser = subparsers.add_parser("store_output_event_numbers")
    event_numbers_parser.add_argument(
        "-s", "--stream", type=str, required=True, help="Name of the stream"
    )

    rate_denom_parser = subparsers.add_parser("count_input_events")
    rate_denom_parser.add_argument(
        "-n",
        "--evt-max",
        required=True,
        type=lambda x: int(round(float(x))),
        help="maximum nb of events to process per job",
    )
    for sp in [event_numbers_parser, rate_denom_parser]:
        sp.add_argument(
            "-p",
            "--process",
            type=str,
            required=True,
            choices=["hlt1", "hlt2", "spruce"],
        )
        sp.add_argument(
            "-sc",
            "--stream-config",
            type=str,
            required=True,
            choices=KNOWN_STREAM_CONFIGS,
            help="Name of the stream config",
        )
        sp.add_argument("--file-type", choices=("ROOT", "MDF", "RAW"), required=True)

    args = parser.parse_args()

    fname_helper = FileNameHelper(args.process, args.stream_config)
    if args.mode == "store_output_event_numbers":
        file_ext = fname_helper.input_type_to_file_ext(args.file_type)
        ifile = fname_helper.mdfdst_fname_for_reading(args.stream, ext=file_ext)
        event_numbers = _extract_evt_numbers(
            ifiles=[ifile],
            file_type=args.file_type,
            stream=args.stream if args.process == "spruce" else None,
        )
        ofile = fname_helper.event_no_fname(args.stream)
        with open(ofile, "w") as f:
            json.dump({args.stream: event_numbers}, f)
        print(
            f"Found {len(event_numbers)} event numbers for {args.stream} stream. Saved list to {ofile}."
        )

    else:
        # Load up the filenames
        with open(fname_helper.input_info_json()) as f:
            input_info = json.load(f)

        # Wont ever be sprucing output as this is for working out how many events in *input* files
        n_evts_in_file = _count_evts(
            input_info["fnames"], file_type=args.file_type, evt_max=args.evt_max
        )
        rate_denom = (
            min(n_evts_in_file, args.evt_max) if args.evt_max > 0 else n_evts_in_file
        )

        output_info = input_info
        output_info["n_evts"] = rate_denom
        with open(fname_helper.input_info_json(), "w") as f:
            json.dump(output_info, f)
        print(
            f"Found {n_evts_in_file} events in input files (max of {args.evt_max}), therefore n_evts in denominator of rates: {rate_denom}"
        )


if __name__ == "__main__":
    main()
