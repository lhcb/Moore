Monte Carlo association
=======================

The ``Moore.truth_matching`` module defines configuration for creating
``LHCb::ProtoParticle``-to-``LHCb::MCParticle`` association tables.

API
---

.. automodule:: Moore.persistence.truth_matching
  :members:
