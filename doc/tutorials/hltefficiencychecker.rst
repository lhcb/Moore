Studying HLT efficiencies, rates and overlaps
=============================================

Since the ``HltEfficiencyChecker`` package now lives in ``DaVinci``, this
tutorial has been moved to the `DaVinci documentation
<https://lhcb-davinci.docs.cern.ch/guide/hltefficiencychecker.html>`_.
