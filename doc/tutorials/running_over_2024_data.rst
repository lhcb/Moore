Running HLT2 and Sprucing over 2024 HLT1-filtered real data
===========================================================

Now that we have real data, we can emulate production and run HLT2 over it using
example options very similar to the examples in the previous tutorial that were
applicable for ``expected_2024`` MC.

This time round you will need a ``DD4HEP`` stack to run over real data. The
default ``lb-stack-setup`` platform will be with ``DD4HEP``. If you plan to be
running over MC and over real data, it is probably a good idea to have two
stacks built and up-to-date, one with ``DD4HEP`` and one with ``detdesc``.

Your stack should be targeting the ``2024-patches`` branch.

As always, there is a danger of the example below becoming out of date and
diverging from the options used in HLT2 in the pit. If you spot an error, please
edit this tutorial, or at least report the issue in the relevant Mattermost
channels.

.. tip::
    If lots of the configuration options here are unfamiliar to you, please go
    back to earlier parts of the documentation.

HLT2
----

.. literalinclude:: ../../Hlt/Hlt2Conf/options/hlt2_pp_realdata_with_UT.py

.. note::
    Here we must pick some HLT2 lines that we'd like to see the output of. All
    trigger lines in HLT2 could be run, but this would be computationally
    expensive and inefficient. Here we pick the same semileptonic lines used in
    the previous tutorial semi-arbitrarily. They are full-stream lines, and so
    the output should be spruced as shown in the next example. In your case you
    may have Turbo lines (or perhaps Turbo *and* Full lines) that are relevant,
    and this example shows how to add a second stream to the job if required.
    Turbo output should have a ``SprucingPass`` job run over it; see the
    :ref:`spruce_pass_data` example.

Sprucing
--------

*For use on the output of Full-stream lines only*

.. literalinclude:: ../../Hlt/Hlt2Conf/options/sprucing/spruce_pp_early2024_realdata.py

.. _spruce_pass_data:

SprucingPass
------------

*For use on the output of Turbo-stream lines only*

.. literalinclude:: ../../Hlt/Hlt2Conf/options/sprucing/sprucepass_pp_early2024_realdata.py

.. note::
    This example **should not be used** on the output of the HLT2 example above,
    as the example above runs Full-stream lines. Use it only on the output of
    Turbo lines, modified appropriately for your use case.
