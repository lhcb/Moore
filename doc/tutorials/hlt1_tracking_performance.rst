HLT1 tracking performance check
===============================

This part is delicated to introduce how to run the applications and plotting scripts for
the tracking level performance of HLT1 reconstruction.
Generally, you need to run these options files in /Moore/Hlt/RecoConf/options with the description
in :doc:`running`, like::

    ./Moore/run gaudirun.py some_options_file.py

Then, run these corresponding python scripts in /Moore/Hlt/Recoconf/scripts to
draw the plots. You can find more details below.

Tracking efficiency check
-------------------------

To check the tracking efficiency, firstly::

    ./Moore/run gaudirun.py ./Moore/Hlt/Moore/tests/options/default_input_and_conds_hlt1.py ./Moore/Hlt/RecoConf/options/hlt1_reco_baseline_with_mcchecking.py

Secondly::

    python ./Moore/Hlt/RecoConf/scripts/PrCheckerEfficiency_HLT1.py --plotstyle ./Moore/Hlt/RecoConf/scripts

Then you will have a root file ``efficiency_plots_baseline_MiniBias.root``, including all the tracking efficiency
distribution along with momentum, transverse momentum, eta, etc...
Note that the tracking efficiency is defined as the reconstructed divided by reconstructible tracks. The definition
of reconstructible particles could be found in the LHCb-Note-XXX (will release soon, it is the conventional definition used in LHCb).


IP resolution check
-------------------

To check the IP resolution, the input tracks will be velo tracks. Run as follows::

    ./Moore/run gaudirun.py ./Moore/Hlt/Moore/tests/options/default_input_and_conds_hlt1.py ./Moore/Hlt/RecoConf/options/hlt1_reco_IPresolution.py
    python ./Moore/Hlt/RecoConf/scripts/PrCheckerIPresolution.py --plotstyle ./Moore/Hlt/RecoConf/scripts

You will see some output of IPx and IPy resolution in different ``1/pT`` and ``eta`` regions, you
will also have a root file ``IPResolution_plots.root``, saving the resolution distributions along with ``1/pT`` and ``eta``.


Tracking resolution check
-------------------------

To check the tracking resolution, the input tracks will be forward tracks. Run in two steps::

    ./Moore/run gaudirun.py ./Moore/Hlt/Moore/tests/options/default_input_and_conds_hlt1.py ./Moore/Hlt/RecoConf/options/hlt1_reco_trackresolution.py
    python ./Moore/Hlt/RecoConf/scripts/PrCheckerTrackResolution.py --plotstyle ./Moore/Hlt/RecoConf/scripts

You will see the tracking resolution outputs in different momentum ``p`` and ``eta`` regions, you
will also have a root file ``TrackResolution_plots.root``, saving the resolution distributions along with ``p`` and ``eta``.


Muon ID efficiency check
-------------------------

To check the muon ID efficiency, run as follows::

    ./Moore/run gaudirun.py ./Moore/Hlt/Moore/tests/options/default_input_and_conds_hlt1.py ./Moore/Hlt/RecoConf/options/hlt1_reco_muonid_efficiency.py
    python ./Moore/Hlt/RecoConf/scripts/PrCheckerMuonIDEff.py --plotstyle ./Moore/Hlt/RecoConf/scripts

The resultant root file ``muonIDeff_plots_MiniBias.root`` will include the muonID efficiency distributions
along with momentum, transverse momentum, eta, etc..
Note that the muonID efficiency is defined on these reconstructed forward tracks.


.. note::
   The input file used here is for SciFi decoding version 4, if you would like to run for SciFi decoding version 6, you can change the input file to ``default_input_and_conds_hlt1_FT6.py``. If you would like to run for other MC samples, you may change your input files refering to the ``TestFileDB.py`` in PRConfig repository.
