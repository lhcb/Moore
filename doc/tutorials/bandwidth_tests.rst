Bandwidth tests
===============
A significant set of constraints on data-taking are the operational constraints placed upon bandwidth, i.e. the size of the data stored per second.
These constraints were set in the `Upgrade Computing Model TDR`_ for each selection stage (HLT1, HLT2, Sprucing).
The bandwidth tests emulate a stage(s) of selections over a minimum bias sample and generate summaries, pages, plots and tables reporting relevant parameters of interest such as rate and bandwidth.
These tests run periodically using the result of the nightly builds and can also be evaluated using the changes within a merge request.
The output of these tests are published on the `Upgrade Rate Test dashboard`_.

There are five bandwidth tests in total, each differing in input file or selection stage(s).
To run any of these from within a merge request, the appropriate Gitlab label must be included. These usually start with `PR/`, for example `PR/Moore_hlt2_and_spruce_bandwidth`.
When ran from a merge request, a gitlab bot will summarise the test and post links to the newly created pages.

The six tests are:

1. **AllenInMoore_hlt1_bandwidth**: test of HLT1.
2. **Moore_hlt2_bandwidth**: test of HLT2.
3. **Moore_spruce_latest_bandwidth**: test of Sprucing, using the output from the latest successful `hlt2_bandwidth` test on ``lhcb-master`` as input.
4. **Moore_hlt2_and_spruce_bandwidth**: a chained test of HLT2 then Sprucing. Effectively `hlt2_bandwidth` then `spruce_latest_bandwidth` within a single job.
5. **Moore_hlt2_and_spruce_bandwidth_SMOG2**: Effectively `hlt2_and_spruce_bandwidth` with a special simulated pp+pAr input file thus accounting for SMOG2's contribution to the Bandwidth.
6. **Moore_spruce_bandwidth**: test of Sprucing, using a set of static HLT2-filtered files as input.

The tests are defined and configured across four repositories:

 - `Moore`_: option and configuration files stored in `Hlt/Hlt2Conf/tests/options/bandwidth/` and python scripts analysing the output of the tests in `Hlt/Hlt2Conf/python/Hlt2Conf/tests/bandwidth/`.
 - `lhcb-datapkg/PRConfig`_: bash scripts defining the tests in `scripts/benchmark-scripts/`.
 - `lhcb-core/LHCbPR2HD`_: Handler to publish results of the test in `handlers/`.
 - `lhcb-core/LHCbNightlyConf`_: Defining the schedule of the tests.

.. note::
    The webpages are intended to be self-consistent and explanatory.
    If you find this is not the case feel free to reach out to the experts to suggest improvements.
    Alternatively if you aren't sure who are responsible for these tests you could also reach out to Mattermost (`Upgrade HLT2`_) or RTA WP3 meetings.
Which Sprucing test to use?
---------------------------

| If interested in End-of-Year Sprucing evaluations, use latest suggestions from `DPA WP1 Sprucing`_.
| `hlt2_and_spruce_bandwidth` is sensitive to indirect sprucing changes, for instance when a change in an HLT2 full-stream line could cause a sprucing line's output to change. As `hlt2_and_spruce_bandwidth` generates the hlt2 output within the job, the sprucing input is the most up-to-date it can be.
| Alternatively, `spruce_latest_bandwidth`, which uses the hlt2 output from the most recent successful `lhcb-nightly` `hlt2_bandwidth` job, is only sensitive to direct sprucing changes, for example adding/removing a Sprucing line. The benefit of `spruce_latest_bandwidth` is that it avoids the computationally expensive (and lengthy) running of the HLT2 stage.
| Thus when only direct changes to sprucing are to be evaluated, `spruce_latest_bandwidth` is more appropriate, while `hlt2_and_spruce_bandwidth` has greater coverage but greater cost.


Updating input files
--------------------
The files that configure the tests are in `yaml` format, and defined in `Hlt/Hlt2Conf/tests/options/bandwidth/`.
Take, as an example, the input used for the `Moore_hlt2_bandwidth` and `Moore_hlt2_and_spruce_bandwidth` tests, shown below.

.. literalinclude:: ../../Hlt/Hlt2Conf/tests/options/bandwidth/hlt2_bandwidth_input_2024.yaml

It is possible, and preferred, to use inputs defined within the `test file database`_ via `testfiledb_key`.
However if this is not possible, paths to the `input_files` (as a yaml list) and other necessary configuration (e.g. `input_raw_format`, `conditions_version` etc.) must be provided within the configuration file.
Once the input configuration is changed, the bandwidth test can be tested locally (see next section) or via opening a merge request to Moore with the changes, adding the relevant bandwidth test label, and asking for a CI test.
If the latter, pay special attention to the summaries, webpages and potential errors that will be posted on the merge request.

.. note::
    The tests can also evaluate simulation, be aware of the :doc:`running_over_mc` guidelines.
    This necessitates different configuration information to be provided than data (e.g. `conddb_tag`, `dddb_tag` etc.)

.. warning::
    Since late 2024, `Moore` jobs will fail at runtime if the number of events in the file are less than the specified EVTMAX.
    Therefore, {HLT1, HLT2, SPRUCE}_EVTMAX provided by the scripts must either be a) less than or equal to the number of events present in the input files, or b) set to `-1` (process all events in all files).

Running tests locally
---------------------

The tests are typically scheduled to run on LHCbPR machines from nightly or merge-request builds, but you can also be run them on your local machine.
Assuming you have a locally built stack, having followed the instructions in :doc:`developing`, and, when evaluating simulation, also followed the guidance for evaluating simulation in :doc:`running_over_mc`.
Then the test can be run with::

    Moore/run DBASE/PRConfig/scripts/benchmark-scripts/Moore_hlt2_bandwidth.sh

.. note::
    The tests are not strict on error messages to ensure that the jobs finish and print the appropriate error messages in the automation.
    Thus when running locally it is important to check the `message.json` that is generated at the end of the job for error codes.


Each selection stage being evaluated corresponds to running a `Moore` job and then several scripts analysing the data generating the plots, summaries, pages, etc.
There are several environment variables that can be configured, see one of the wrapper scripts like `Moore_hlt2_bandwidth.sh` for an up-to-date view.
These can configure information like the number of events processed in the Moore jobs (`EVTMAX`), or the directory to write to (`BASEDIR`).
For running locally it is important to at least flag `BUILD_PAGES_LOCALLY`, otherwise the output html links to plots and pages will not work.

Within the `BASEDIR`, the output will have a directory structure of the form::

    <BASEDIR>
    ├── MDF
    ├── Output
    └── to_eos


Each directory has it's purpose, explained below:

 - `MDF`: Output of the `Moore` processing stage, i.e. `.tck.json`, `.mdf`, `.dst` output files.
 - `Output`: Html, json and csv files from analysing the output from the processing stage.
 - `to_eos`: Related to the test handler, irrelevant for local testing.

The webpages require only the files from `Output` to function, thus to investigate the pages (with `BUILD_PAGES_LOCALLY=true`), it is enough to have the `Output` directory and open (`Output/index.html`), then navigating via the links provided.

Running the test handler locally (for developing the handler only)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The test handler's main functions are to:
 - Write results to: `/eos/lhcb/storage/lhcbpr/www/UpgradeRateTest/` thus accessible via the `Upgrade Rate Test dashboard`_,
 - Generate and post messages to Mattermost (`LHCbPR Throughput`_) and Gitlab merge requests,
 - Generate comparison tables between builds and thus create comparison pages, when applicable,
 - Make memory usage plots via PRMon.

.. note::
    The handler's docstring includes necessary information on how to run it, if you notice that is outdated please update any relevant information or inform relevant experts.
    Alternatively if you aren't sure who are responsible for these tests you could also reach out to Mattermost (`Upgrade HLT2`_) or RTA WP3 meetings.

The ability to test the handler is limited, specifically it's not able to test the publishing files stage.
In a successful test on an LHCbPR machine, the log will include lines such as::

    2024-09-20 12:34:47,508 - handlers.BandwidthTestHandler - INFO - Found file <fname>.html to upload.
    2024-09-20 12:34:47,508 - handlers.utils.publish - INFO - Uploading output/<fname>.html to root://eoslhcb.cern.ch/<fpath>/<fname.html>

While in a successful local test these publish attempts will fail with::

    2024-09-20 12:34:47,508 - handlers.BandwidthTestHandler - INFO - Found file <fname>.html to upload.
    2024-09-20 12:34:47,508 - handlers.utils.publish - ERROR -

.. Warning::
    Regardless of the above known publishing error, the handler test will still accurately report if it passes or fails.
    Therefore it remains important to read the log and ensure a passing local test when developing the handler.

.. _`Upgrade Computing Model TDR`: https://cds.cern.ch/record/2319756
.. _`Upgrade Rate Test dashboard`: https://lhcbpr-hlt.web.cern.ch/UpgradeRateTest/
.. _`Moore`: https://gitlab.cern.ch/lhcb/Moore
.. _`lhcb-datapkg/PRConfig`: https://gitlab.cern.ch/lhcb-datapkg/PRConfig
.. _`lhcb-core/LHCbPR2HD`: https://gitlab.cern.ch/lhcb-core/LHCbPR2HD
.. _`lhcb-core/LHCbNightlyConf`: https://gitlab.cern.ch/lhcb-core/LHCbNightlyConf
.. _`test file database`: https://gitlab.cern.ch/lhcb-datapkg/PRConfig/-/blob/master/python/PRConfig/TestFileDB.py
.. _`Upgrade HLT2`: https://mattermost.web.cern.ch/lhcb/channels/upgrade-hlt2
.. _`DPA WP1 Sprucing`: https://mattermost.web.cern.ch/lhcb/channels/dpa-wp1-sprucingtesla
.. _`LHCbPR throughput`: https://mattermost.web.cern.ch/lhcb/channels/lhcbpr-new-results
