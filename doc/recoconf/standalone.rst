Standalone reconstruction options
=================================

Member functions of the ``RecoConf.standalone`` module are documented.

The functions in ``RecoConf.standalone`` allow to run predefined reconstruction data flows, including MC efficiency checking, without being included in a trigger line. Use cases are testing of the reconstruction, monitoring and (in future) writing of files.

Members of ``RecoConf.standalone``
-------------------------------------

.. automodule:: RecoConf.standalone
  :members:
