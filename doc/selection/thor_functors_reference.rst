=======================
ThOr functors reference
=======================

Introduction
------------

This page aims to be a comprehensive list of all :doc:`thor_functors`
currently in the source tree, alongside their parameters, C++ source
files, and even matching LoKi functors!


Contributing
^^^^^^^^^^^^

Want to improve functor documentation? This page is automatically
generated from the `Functors` python package in the `lhcb/Rec project <https://gitlab.cern.ch/lhcb/Rec/-/blob/master/Phys/FunctorCore/python/Functors/__init__.py>`_.
Additionally, any Sphinx-flavoured reStructuredText, if used in these
docstrings, will be rendered on this page.


To make it easier to move from LoKi to ThOr, each ThOr functor entry
on this page will list LoKi functors with known equivalent behaviour.
If an entry is missing a LoKi functor equivalent that you
know of, why not `add it here? <https://gitlab.cern.ch/lhcb/Moore/-/blob/master/doc/selection/standard_loki_thor_table.csv>`_


.. toctree::
   :maxdepth: 2


.. Please take note!
.. The sections following this text are automatically generated!
.. If you would like to change the behaviour of the autogeneration,
.. please look instead at doc/make_functor_docs.py.
..
.. Thanks for contributing!
.. include:: thor_functors_reference.generated.rst
