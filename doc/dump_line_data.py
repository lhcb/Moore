###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import importlib
import inspect
import os
import sys
import textwrap
from collections import defaultdict

from Hlt2Conf.lines import all_lines
from Moore import options, run_moore
from RecoConf.global_tools import (
    stateProvider_with_simplified_geom,
    trackMasterExtrapolator_with_simplified_geom,
)
from RecoConf.hlt2_global_reco import make_fastest_reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction
from RecoConf.reconstruction_objects import reconstruction


def format_cutstring(cutstr):
    return cutstr


def generate_combiner_doc(bldr, line, node, doc_line):
    combiners = []

    if not hasattr(node, "typename"):
        print("no typename!!")
        print(dir(node))
        return

    # try to resurrect the last combiner for parsing ranges in case the top alg is a merger
    if node.typename == "ParticleContainerMerger":
        for dalg in reversed(node.__dict__["_inputs"]["InputContainers"]):
            if "BodyCombiner" in dalg.producer.typename:
                combiners.append(dalg.producer)
    if "BodyCombiner" in node.typename:
        combiners = [node]

    if combiners == []:
        doc_line(f"{node.typename} ``{node.name}``: unable to document.", lvl=1)
        doc_line("")
        doc_line("")
        return

    for combiner in combiners:
        assert "BodyCombiner" in combiner.typename

        if "Merger" in node.typename:
            doc_line(
                f"{combiner.typename} ``{combiner.name}`` (via merger ``{node.name}``)",
                lvl=1,
            )
        else:
            doc_line(f"{combiner.typename} ``{combiner.name}``", lvl=1)

        prop_dict = combiner.__dict__["_properties"]
        decay_desc = prop_dict["DecayDescriptor"]
        comp_cutstring = (
            ""
            if "CompositeCut" not in prop_dict
            else prop_dict["CompositeCut"].code_repr()
        )
        comb_cutstring = (
            ""
            if "CombinationCut" not in prop_dict
            else prop_dict["CombinationCut"].code_repr()
        )
        producer_output = (
            node.OutputContainer
            if node.typename == "ParticleContainerMerger"
            else combiner.OutputParticles
        )

        doc_line("Decay descriptor", lvl=2)
        doc_line(f"``{decay_desc}``", lvl=3)
        doc_line("")

        doc_line("Output particles", lvl=2)
        doc_line(f"``{producer_output}``", lvl=3)
        doc_line("")

        doc_line("..  code-block:: py", lvl=2)
        doc_line(":caption: CombinationCut\n", lvl=3)
        doc_line(format_cutstring(comb_cutstring), lvl=3)
        doc_line("")

        doc_line("..  code-block:: py", lvl=2)
        doc_line(":caption: CompositeCut\n", lvl=3)
        doc_line(format_cutstring(comp_cutstring), lvl=3)
        doc_line("")

        doc_line("")


def generate_line_doc(path, bldr, heading=True, autofunction=True):
    line = bldr()
    filename = path.format(line.name)

    with open(filename, "w") as f:

        def doc_line(l: str, lvl: int = 0):
            print(textwrap.indent(l, " " * 4 * lvl, lambda _: True), file=f)

        if heading:
            doc_line(f"{line.name}")
            doc_line(f"{'^' * len(line.name)}\n")

        fqualname = bldr.__qualname__
        bldr_doc = inspect.getdoc(bldr)
        gitlab = "https://gitlab.cern.ch/lhcb/Moore/-/blob/master"
        file_location = os.path.relpath(inspect.getfile(bldr), os.getcwd())
        _, lineno = inspect.getsourcelines(bldr)
        mod = inspect.getmodule(bldr).__name__

        try:
            mod__ = importlib.import_module(f"{mod}")
            importable = hasattr(mod__, fqualname)
        except ModuleNotFoundError:
            importable = False

        if not importable:
            doc_line(f".. py:function::  {mod}.{fqualname}{inspect.signature(bldr)}\n")
            doc_line("")

            doc_line(".. note::", lvl=1)
            doc_line(
                "The builder function for this line is not importable. It is probably nested inside another builder. ",
                lvl=2,
            )
            doc_line("")

        elif autofunction:
            doc_line(f".. autofunction:: {mod}.{fqualname}\n")
            doc_line("")
        else:
            doc_line(f".. py:function::  {mod}.{fqualname}{inspect.signature(bldr)}\n")
            if bldr_doc:
                doc_line(bldr_doc, lvl=1)
                doc_line("")

        doc_line("Line Name", lvl=1)
        doc_line(f"``{line.name}``", lvl=2)
        doc_line("")

        doc_line("Module", lvl=1)
        doc_line(f"``{mod}``", lvl=2)
        doc_line("")

        doc_line("File location", lvl=1)
        doc_line(
            f"``{file_location}`` at line {lineno} (`go to this file on gitlab <{gitlab}/{file_location}#L{lineno}>`_).",
            lvl=2,
        )
        doc_line("")

        persistence = []

        if line.persistreco:
            persistence += ["PersistReco"]
        if len(line.extra_outputs) > 0:
            persistence += ["Selective Persistence (extra_outputs)"]
            ## TODO: details of persisted objects

        if len(persistence):
            persistence = ", ".join(persistence)
        else:
            persistence = (
                "None configured. (May not take into account global configuration)"
            )

        doc_line("Persistence", lvl=1)
        doc_line(persistence, lvl=2)
        doc_line("")

        picked_nodes = []
        doc_line("Algorithms", lvl=1)
        for node in line.node.children:
            try:
                node_name, node_combine, _, _ = node.represent()

                doc_line(f"{node_name} (``{node_combine}``)", lvl=2)
                for i, node_ in enumerate(node.children):
                    if node_.name.startswith("Monitor_"):
                        continue
                    if hasattr(node_, "typename"):
                        if (
                            "ParticleContainerMerger" in node_.typename
                            or "BodyCombiner" in node_.typename
                        ):
                            picked_nodes.append(node_)
                    doc_line(
                        f"{i + 1}. {node_.typename if hasattr(node_, 'typename') else ''} ``{node_.name}``",
                        lvl=3,
                    )
                doc_line("")
            except AttributeError:
                doc_line(
                    f'{node} - this node doesnt have a "represent" method, so dont know how to display it.',
                    lvl=2,
                )
        doc_line("")

        doc_line("\n")

        for n__ in picked_nodes:
            generate_combiner_doc(bldr, line, n__, doc_line)

    return line.name


def make_lines():
    grouped_lines = defaultdict(lambda: defaultdict(set))
    for groupname, line_builders in all_lines.items():
        for bldr in all_lines.values():
            mod = inspect.getmodule(bldr).__name__
            mod_parts = mod.split(".", 3)

            if len(mod_parts) == 4:
                _, __, wg, group = mod_parts
            else:
                _, __, group = mod_parts
                wg = "NoWG"
            grouped_lines[wg][group].add(bldr)

        # from IPython import embed; embed()

    os.makedirs("doc/selection/hlt2_lines/", exist_ok=True)

    wg_includes = []
    for wg, groups in grouped_lines.items():
        groupname_includes = []
        for groupname, lines in groups.items():
            module_includes = []
            for bldr in lines:
                line_name = generate_line_doc(
                    "doc/selection/hlt2_lines/{}.generated.rst", bldr
                )
                module_includes.append(f"hlt2_lines/{line_name}.generated")

            # write toctree for this module
            with open(f"doc/selection/index_{wg}_{groupname}.generated.rst", "w") as f:
                print(f"{groupname}", file=f)
                print(f"{'-' * len(groupname)}\n", file=f)
                print(".. toctree::\n    " + "\n    ".join(module_includes), file=f)
            groupname_includes.append(f"index_{wg}_{groupname}.generated")

        # write toctree for each wg
        with open(f"doc/selection/index_{wg}.generated.rst", "w") as f:
            print(f"{wg}", file=f)
            print(f"{'=' * len(wg)}\n", file=f)

            print(""".. toctree::\n    """ + "\n    ".join(groupname_includes), file=f)

        wg_includes.append(f"index_{wg}.generated")

    # write final toctree
    with open("doc/selection/hlt2_line_reference.generated.rst", "w") as f:
        print(""".. toctree::\n    """ + "\n    ".join(wg_includes), file=f)

    sys.exit(0)


public_tools = [
    trackMasterExtrapolator_with_simplified_geom(),
    stateProvider_with_simplified_geom(),
]

# Dummy settings to make sure Moore configures properly
options.set_input_and_conds_from_testfiledb("UpgradeHLT1FilteredMinbias")
options.scheduler_legacy_mode = False

with (
    reconstruction.bind(from_file=False),
    hlt2_reconstruction.bind(make_reconstruction=make_fastest_reconstruction),
):
    config = run_moore(options, make_lines, public_tools)
