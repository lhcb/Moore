HLT1 Reconstruction
===================

HLT1 reconstructs the trajectories of charged particles traversing the full LHCb tracking system,
called *long tracks*, with certain kinematic requirements. In addition, a precise reconstruction of
the *primary vertices* (PVs) is performed.
Tight timing constraints in HLT1 mean that most particle-identification algorithms cannot be
executed. The exception is *muon identification*, which due to a clean signature produced by muons
in the LHCb detector can be performed already in HLT1.


Track and vertex reconstruction in HLT1
---------------------------------------

The pattern recognition deployed in HLT1 consists of three main steps: reconstructing the *VELO
tracks*, extrapolating them to the UT to form *upstream tracks*, and finally extending them
further to the SciFi to produce *long tracks*. Next, the long tracks are fitted and the
fake trajectories are rejected. The set of VELO tracks is used to determine
the positions of the PVs.

A detailed documentation of the configuration in Moore can be found here: :doc:`Configuration of the track reconstruction in HLT1 <../recoconf/hlt1_tracking>`.

Pattern recognition of high-momentum tracks
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The hits in the VELO are combined to form straight lines loosely pointing back towards the beam line.
Next, hits in the UT are required in a small region around a straight-line extrapolation
from the VELO to form so-called upstream tracks. The UT is located in the fringe field of the
LHCb dipole magnet, which allows the momentum to be determined with a coarse resolution.
The momentum estimate is used twofold: first, to define a search window for hits around
the track extrapolations in the SciFi. With the SciFi hit information, the upstream track
is extended to form so-called long tracks. These particular long tracks are often called forward tracks.
Second, the momentum estimate is used to reject upstream tracks with low transverse momentum.
Because of their high abundance and the larger search window they open up, the number of track-hit
combinations grows too large to fulfill the timing constraints in HLT1.

Track fitting and fake-track rejection
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Subsequently, all tracks are fitted with a Kalman filter to obtain the optimal parameter estimate.

.. todo:: fill with more information

Primary vertex reconstruction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Many LHCb analyses require a precise knowledge of the PV position and this information is used
early in the selection of displaced particles. The full set of VELO tracks is available in HLT1.
Therefore, the PVs are reconstructed using VELO tracks only, neglecting the additional
momentum information on long tracks which is only available later. This does not result in a
degradation in resolution compared to using a mixture of VELO and long tracks. Furthermore,
this approach produces a consistent PV position from the beginning to the end of the analysis chain
which reduces systematic effects.

Muon identification
-------------------

.. todo:: Please fill

Data flow in HLT1
-----------------

.. graphviz:: ../graphviz/hlt1_example_data_flow.gv


Further reading
---------------

`Expression of Interest for an LHCb Upgrade <https://cds.cern.ch/record/1100545>`_

`Letter of Intent for the LHCb Upgrade <https://cds.cern.ch/record/1333091>`_

`Framework TDR for the LHCb Upgrade <https://cds.cern.ch/record/1443882>`_

`LHCb VELO Upgrade Technical Design Report  <https://cds.cern.ch/record/1624070>`_

`LHCb PID Upgrade Technical Design Report <https://cds.cern.ch/record/1624074>`_

`LHCb Tracker Upgrade Technical Design Report  <https://cds.cern.ch/record/1647400>`_

`LHCb Trigger and Online Upgrade Technical Design Report <https://cds.cern.ch/record/1701361>`_

`Upgrade Software and Computing <https://cds.cern.ch/record/2310827>`_

`Computing Model of the Upgrade LHCb experiment <https://cds.cern.ch/record/2319756>`_

`LHCb SMOG Upgrade <https://cds.cern.ch/record/2673690>`_
