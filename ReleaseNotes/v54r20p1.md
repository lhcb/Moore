2023-09-27 Moore v54r20p1
===

This version uses
Allen [v3r20p1](../../../../Allen/-/tags/v3r20p1),
Rec [v35r19](../../../../Rec/-/tags/v35r19),
Lbcom [v34r18](../../../../Lbcom/-/tags/v34r18),
LHCb [v54r18](../../../../LHCb/-/tags/v54r18),
Gaudi [v36r16](../../../../Gaudi/-/tags/v36r16),
Detector [v1r21](../../../../Detector/-/tags/v1r21) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Moore [v54r20](/../../tags/v54r20), with the following changes:

### New features ~"new feature"

- Upstream project highlights :star:


### Fixes ~"bug fix" ~workaround

- Upstream project highlights :star:


### Enhancements ~enhancement

- Upstream project highlights :star:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Upstream project highlights :star:


### Documentation ~Documentation

### Other
