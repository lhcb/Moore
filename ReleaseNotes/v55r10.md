2024-06-13 Moore v55r10
===

This version uses
Allen [v4r10](../../../../Allen/-/tags/v4r10),
Rec [v36r10](../../../../Rec/-/tags/v36r10),
Lbcom [v35r9](../../../../Lbcom/-/tags/v35r9),
LHCb [v55r10](../../../../LHCb/-/tags/v55r10),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1),
Detector [v1r33](../../../../Detector/-/tags/v1r33) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `2024-patches` branch.
Built relative to Moore [v55r9](/../../tags/v55r9), with the following changes:

### New features ~"new feature"

- Upstream project highlights :star:


### Fixes ~"bug fix" ~workaround

- Remove unneccesary ProbNN options, !3431 (@mveghel)
- Upstream project highlights :star:


### Enhancements ~enhancement

- ~Tracking | Hlt2_pp_2024 enable faster clone removal in PrHybridSeeding, !3507 (@gunther) [#773]
- Upstream project highlights :star:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~UT | Stringent UT hits comparison between HLT1 vs HLT2, !3547 (@dtou)
- Update to test_hlt2_standard_particles, !3524 (@msaur)
- Upstream project highlights :star:


### Documentation ~Documentation

### Other

- ~selection | Enlarge psi2S mass window to prepare for early 2024 withUT data investigation, !3423 (@mengzhen)
- ~selection | Bug fix for BandQ, B2CC/BnoC dimuon builder: lifetime resolution bias, !3339 (@mengzhen)
- ~selection | B&Q: loosen HHHH selection and add 3Kpi modes in prompt D0 anti-D0 B&Q double charm lines, !3260 (@pnaik)
- ~selection ~hlt2 | [Charm] Tune doubly charmed baryon hadronic lines, !3300 (@axu)
- ~selection ~hlt2 | Optimise charm neutral lines, !3286 (@tpajero)
- ~selection ~hlt2 | Add isolation information for charm SL lines (See Moore!2262), !3237 (@yangjie)
- ~hlt2 | Add new trigger lines for the Lambda_c+ -> Sigma + pi pi, !3454 (@tork)
- ~Configuration | Adapt calibration lines for post-TS June 2024, !3552 (@decianm)
- ~Configuration | Remove redundant bind for Sprucing, !3512 (@nskidmor)
- ~Tracking | Tighten ghostprob for throughput improvement in HLT2, !3498 (@mveghel)
- ~Tracking | Ghost probability for with UT, !3480 (@mveghel)
- ~UT | Update HLT1 vs HLT2 UT hits comparison, !3192 (@dtou)
- ~Jets | Move topobits.py from lines, !3390 (@rangel)
- ~PID | Adding upstream electrons to di-electron combiners for B2CC, RD and PID, !3396 (@mveghel)
- ~PID | Neutral pid calib v1, !3183 (@desahoo)
- ~PID | Adding ProbNNs for Downstream, Upstream and ProbNNghost, !3528 (@mveghel)
- Update References for: Allen!1198, Allen!559 based on lhcb-2024-patches-mr/748, !3566 (@msaur)
- Update References for: Allen!1633 based on lhcb-2024-patches-mr/734, !3562 (@msaur)
- Update References for: LHCb!4557, Rec!3811 based on lhcb-2024-patches-mr/728, !3559 (@msaur)
- Update References for: Allen!1525, Allen!1663, Moore!3547, Moore!3557 based on lhcb-2024-patches-mr/719, !3558 (@msaur)
- Fix the ci-test for matching with ut, !3557 (@jzhuo)
- Update References for: Allen!1603, Moore!3385 based on lhcb-2024-patches-mr/702, !3553 (@lhcbsoft)
- Update References for: Rec!3964, Moore!3548 based on lhcb-2024-patches-mr/687, !3549 (@msaur)
- No longer need to deduce subdetectors for HLT1 configuration, !3385 (@raaij)
- Simplify MC cloning configuration, !3548 (@graven)
- Update References for: Rec!3922, Moore!3479 based on lhcb-2024-patches-mr/673, !3546 (@lhcbsoft)
- Update References for: Allen!1444, Moore!3192, Moore!3539 based on lhcb-2024-patches-mr/646, !3543 (@msaur)
- Warning exclusion due to kalman fit allen1444, !3539 (@msaur)
- Change in exclusion message following LHCb!4565, !3506 (@msaur)
- Update RecSummary with UT and Muon info, !3479 (@mveghel)
- Tune Ks tracking efficiency lines, !3461 (@sstahl)
- [RTA/DPA BW tests] Add NoBias, Calib & Lumi streams to bandwidth tests, !3448 (@rjhunter)
- Remove BPVFDCHI2 cut from D02KSKS LLLL line, !3462 (@lpica)
- SLB-WG MRs targeting JuneTS milestone, !3457 (@groberts)
- TrackEff Electron line - Reduce PIDe cut to increase electron statistics and align with muonic cut., !3426 (@gpietrzy)
- RD developments towards June TS, !3394 (@mramospe) [#662,#704,#705,#710,#737,#756,#761,gitlab.cern.ch/lhcb/Moore/-/merge_requests/3425#0]
- Inclusive Detached Dilepton Trigger reduction of combinations, !3380 (@lecarus) [#756]
- Add dimuon displaced dimuon produced in smog2 for DM search, !3299 (@sbelin)
- Update References for: Rec!3902, Moore!3392, Moore!3260, Moore!3476,..., !3534 (@msaur)
- Removing hardcoded expected warnings in test_spruce_alllines_realtime_gaudirun, !3532 (@msaur)
- B2cc btojpsix-jpsi2pp combinations, !3475 (@jbaptist)
- QEE: Lower Hlt2DiMuonNoIP lines rates by filtering on Hlt1DiMuonNoIP, !3443 (@acasaisv)
- B2OC: merge request to collect updates for the May 15th Moore deadline, !3435 (@abertoli)
- Add Dst->D0(->KsPipPim)Pip NoBias Line, !3388 (@lpica)
- [QEE] Collection MR for May 15th Deadline, !3354 (@lugrazet)
- Re-implement vertex monitoring for B2OC and B2CC, !3319 (@shunan)
- Update References for: Moore!3507 based on lhcb-2024-patches-mr/615, !3531 (@msaur)
- Adapt criteria of smog2 etac line to HLT1, !3476 (@oboenteg)
- Including upstream electrons in the dst_to_dee module for the UT scenario, !3458 (@cacochat)
- Bug fix for charmxsec line Ds->KKpi, !3445 (@peilian)
- BnoC HLT2/Spruce lines, May selections deadline, !3392 (@fswystun) [#717,#766]
- Update References for: LHCb!4385, Rec!3659, Moore!2784 based on lhcb-2024-patches-mr/603, !3529 (@msaur)
- Update References for: Moore!3307, Moore!3524 based on lhcb-2024-patches-mr/598, !3527 (@lhcbsoft)
- Update References for: LHCb!4570, !3923, Moore!3480, MooreOnline!451 based on lhcb-2024-patches-mr/586, !3523 (@msaur)
- Update References for: Rec!3853, Moore!3268 based on lhcb-2024-patches-mr/578, !3522 (@msaur)
- Enable muon ID for T tracks, !3307 (@isanders)
- T tracks updates for PrKalmanFiltered step for MVA filtered tracks and calorimeter reconstruction, !2784 (@isanders)
- Update References for: Allen!1619 based on lhcb-2024-patches-mr/569, !3518 (@lhcbsoft)
- Add UTError to PrStoreUTHit, and update default properties, !3268 (@hawu)
