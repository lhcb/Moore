2021-05-27 Moore v52r1
===

This version uses
Allen [v1r3](../../../../Allen/-/tags/v1r3),
Phys [v32r1](../../../../Phys/-/tags/v32r1),
Rec [v32r1](../../../../Rec/-/tags/v32r1),
Lbcom [v32r1](../../../../Lbcom/-/tags/v32r1),
LHCb [v52r1](../../../../LHCb/-/tags/v52r1),
Gaudi [v35r4](../../../../Gaudi/-/tags/v35r4) and
LCG [100](http://lcginfo.cern.ch/release/100/) with ROOT 6.24.00.

This version is released on `master` branch.
Built relative to Moore [v52r0](/../../tags/v52r0), with the following changes:

### New features ~"new feature"

- ~selection ~hlt2 | Add onia sprucing lines, !835 (@ngrieser)
- ~selection ~hlt2 | B+/Bc+ -> psi(')h+ exclusive lines and combiners for the B&Q, !776 (@gcavalle)
- ~selection ~hlt2 | Porting a set of B2OC lines to the ThOr framework., !675 (@vrenaudi)
- ~selection ~hlt2 ~Calo | Add electrons with brem correction, !669 (@cmarinbe) [#214]
- ~Configuration | Stream to multiple files, !833 (@nskidmor) ['LHCb__MDFWriter/LHCb__MDFWriter#1,'LHCb__MDFWriter/LHCb__MDFWriter#2,'RawEventSimpleCombiner/RawEventSimpleCombiner#1,'RawEventSimpleCombiner/RawEventSimpleCombiner#2,['LoKi__HDRFilter/LoKi__HDRFilter#1,['LoKi__HDRFilter/LoKi__HDRFilter#2]
- ~Configuration ~Persistency | Add Sprucing support, prototype lines and tests, !763 (@nskidmor) :star:
- ~Decoding | Add calo geometry for Allen calo decoding, !780 (@raaij)
- ~Calo | Inclusion of selective bremsstrahlung matching algorithm, !707 (@mveghel)
- Upstream project highlights :star:
  - ~selection ~Calo ~"Event model" | Add electrons with brem, Phys!820 (@cmarinbe)
  - ~"Event model" | Add a Generic Track SOA Container, LHCb!2920 (@agilman) [#97]


### Fixes ~"bug fix" ~workaround

- ~Configuration | Do not use AssertionError for control flow, !794 (@rmatev)


### Enhancements ~enhancement

- ~hlt2 ~Configuration ~Tracking | Configure cut values on new quality NN for PrForwardTracking, !784 (@gunther)
- ~hlt2 ~Configuration ~Tracking | Adapt PrForwardTracking configuration  to redesigned algorithm, !754 (@gunther)
- ~Configuration | Follow LHCb!2964, !810 (@nnolte)
- ~Configuration | Cache expensive, data-flow-traversing functions., !791 (@apearce)
- ~Configuration | Make it possible to configure which velo algorithm is used, !781 (@ahennequ)
- ~Configuration | Use new lockOption functionality in download_mdf script, !761 (@chasse)
- ~Persistency | Support MDF output and always write to DAQ/RawEvent, !770 (@sesen) [#228,#241,#264]
- ~Persistency | Force P2PV relation creation in HLT2, !762 (@apearce) [#255]
- ~Persistency ~"MC checking" | Support truth matching of arbitrary containers of particles, !778 (@cprouve)
- ~"MC checking" | Refactoring PrimaryVertexChecker, !388 (@twojton)
- ~Monitoring | Enable resolution checking with fullDetail for monitoring, !783 (@ausachov) [challenge#18]
- ~Build | CI: only fail docs jobs on sphinx runs, !790 (@rmatev)
- Upstream project highlights :star:
  - ~Tracking | PrForwardTracking redesign, Rec!2352 (@gunther) [#157,#162,#38]


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~hlt2 | Add hlt2_lead_lead_light_reco_pr_kf test, !758 (@cmarinbe)
- ~hlt2 ~"MC checking" | Add VeloHits, VeloTracks, UTHits and FThits to PrMultiplicityChecker, !751 (@rlitvino)
- ~Decoding | Reduce verbosity of hlt1_velo_decoding test, !789 (@rmatev)
- ~Tracking | Update refs for Rec!2414, !808 (@chasse)
- ~Tracking | Update refs for Rec!2397using lhcb-master-mr/2218, !803 (@jbaptist)
- ~Tracking | Reference update for Rec!2352 (PrForwardTracking), !782 (@msaur)
- ~Tracking | Update references to Moore for Rec!2339, !775 (@fsouzade)
- ~Calo | Update refs for Rec!2267 using lhcb-master-mr/2192, !787 (@jbaptist)
- ~Calo | Update references to Moore for Rec!2351, !773 (@fsouzade)
- ~Calo | Get zero suppression parameters from DB, !752 (@jmarchan)
- ~Persistency | Update refs files for LHCb!2989 using lhcb-master-mr/2176, !786 (@jbaptist)
- ~"MC checking" | Ignore warnings due to detailed resolution checking (follow up !783), !797 (@rmatev)
- ~"MC checking" | Update references for Moore!388, !777 (@fsouzade)
- ~"MC checking" | Update references for Moore!751, !765 (@cgobelbu)
- ~Monitoring | Update refs for Moore!783 using lhcb-master-mr/2204, !793 (@jbaptist)
- Update refs from lhcb-master/1356 (follow up !758), !779 (@msaur)
- Allen UTDecoding test, !714 (@mstahl)
- Upstream project highlights :star:


### Documentation ~Documentation

- ~hlt2 | Update ganga tutorials, !829 (@shunan) [#274]
- Add ThOr transition guide for HLT2 lines, !840 (@apearce) [#284]
- Add entry about IO optimization for MDF inputs in the documentation, !805 (@lmeyerga)
- Add SLB templates for issues and MRs, !798 (@bldelane)
- Expand Moore docs for different input types, !796 (@lmeyerga)
- Add note to Ganga tutorial on using a released version of Moore, !768 (@apearce)


### Other

- ~selection | WP1 preparations for June FEST: add B2OC sprucing lines, !850 (@abertoli)
- ~selection | Add options to get and test all sprucing lines, !841 (@shunan) [lhcb-dpa/project#122]
- ~selection ~hlt2 | B2OC: new hlt2/spruce lines booking scheme for b_to_dh.py and b_to_dhh.py, extra_outputs for Bs2DsPi, !830 (@abertoli)
- ~selection ~hlt2 | Double dimuon dps line and combiner for B&Q, !811 (@lan)
- ~selection ~hlt2 | B2OC sprucing, !795 (@abertoli)
- ~selection ~hlt2 | Tunable topo. BDT cut value for B2OC Hlt2 lines, !772 (@abertoli)
- ~selection ~hlt2 ~Configuration | Add LHCb::Particle-compatible ThOr algorithm configuration and tests, !821 (@apearce)
- ~selection ~hlt2 ~Jets | Include jet tagging with DeltaR using flight direction, !818 (@rangel)
- ~selection ~Configuration ~Persistency | Gather all outputs of LHCb::Particle producers, !774 (@apearce) [#259]
- ~hlt2 ~Calo ~Monitoring | Add ParticleMassMonitors to bremsstrahlung test, !845 (@cmarinbe)
- ~hlt2 ~Persistency | First code changes to allow eventual Moore streaming, !800 (@nskidmor)
- ~Configuration ~Persistency | Conditionally produce HLT1 line outputs, !759 (@apearce) [#145]
- Update reference files from lhcb-head 2950, !854 (@axu)
- Add info where to find the log files of a test to developing.rst, !842 (@sstahl)
- Update reference files from lhcb-master-mr 2317, !838 (@axu)
- Update reference files from nightly slot lhcb-head, build ID 2938. To be tested with Rec!2404, !826 (@ascarabo)
- Update HLT2 analysis tutorial for latest Moore output format, !749 (@apearce) [#250]
