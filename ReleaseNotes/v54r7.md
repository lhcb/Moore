2023-04-19 Moore v54r7
===

This version uses
Allen [v3r7](../../../../Allen/-/tags/v3r7),
Rec [v35r7](../../../../Rec/-/tags/v35r7),
Lbcom [v34r7](../../../../Lbcom/-/tags/v34r7),
LHCb [v54r7](../../../../LHCb/-/tags/v54r7),
Detector [v1r11](../../../../Detector/-/tags/v1r11),
Gaudi [v36r12](../../../../Gaudi/-/tags/v36r12) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Moore [v54r6](/../../tags/v54r6), with the following changes:

### New features ~"new feature"

- Upstream project highlights :star:


### Fixes ~"bug fix" ~workaround

- ~Persistency | Reenable packing checks, !2086 (@sesen)
- Improve flavourtag test: only unpack what is needed, !2226 (@graven)
- Bug in Standart LambdaLL/DD, !2198 (@ipolyako)
- Upstream project highlights :star:


### Enhancements ~enhancement

- ~Build | Fix limit of number of functor cache compilation jobs, !2213 (@rmatev)
- Upstream project highlights :star:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Add tests to check if hlt2 and sprucing configuration is deterministic, !1851 (@sesen) [#490]
- Upstream project highlights :star:


### Documentation ~Documentation

- Documentation for overlap tool of HltEfficiencyChecker, !2116 (@jagoodin)
### Other

- ~selection ~hlt2 | B&Q lines: Xib -> J/psi hhh and bbaryon -> Lc Ds h(h), !2132 (@chuangxi)
- ~hlt2 | Add Electron Momentum variable to std particle and rdbuilder_thor builders, !2220 (@rmwillia)
- ~hlt2 | Hlt2 btoxll lines retuning, !2070 (@rmwillia)
- ~hlt2 | Add SLB D02K3pi muonic lines, !2045 (@masmith)
- ~hlt2 ~Configuration | Adapt options for TrackBestTrackCreator path to work without UT, !2209 (@decianm)
- ~Configuration | Add missing options.simulation setting as required, !2228 (@jonrob)
- ~Jets | Remove Jets.py and related code used only to test jets algorithms, !2214 (@helder)
- ~Monitoring | Moore option file for DC tests over real 2022 data, !2135 (@msaur)
- ~Luminosity | Incorporating lumi counter and FSR creation into Sprucing workflow, !2153 (@nskidmor)
- Update References for: Moore!2226 based on lhcb-master-mr/7628, !2230 (@lhcbsoft)
- Implement case-insensitive stream name checker for output file names, !2223 (@jzhuo)
- Update References for: Moore!1974 based on lhcb-master-mr/7572, !2216 (@lhcbsoft)
- New HLT2 trigger lines for dark photon search from charm decays, !2207 (@cacochat)
- New tuning for the topological beauty trigger, !1974 (@nschulte)
- Update References for: LHCb!3977, Moore!2086 based on lhcb-master-mr/7566, !2217 (@lhcbsoft)
- Update References for: Moore!2212 based on lhcb-master-mr/7548, !2215 (@lhcbsoft)
- Give explicit names for RecoMon/Monet, !2212 (@mstahl)
- Update References to fix previous bad updates, !2211 (@lhcbsoft)
- Fix line with input_samples, !2199 (@mmulder)
- Editorial: fixing Dps -> Dsp in introductory comment, !2210 (@smaccoli)
- Update References for: LHCb!3969, Rec!3310, Allen!1160 based on lhcb-master-mr/7503, !2206 (@lhcbsoft)
- Update References for: LHCb!4051, Rec!3364, Moore!2195, Alignment!360, MooreOnline!214, MooreAnalysis!115, Boole!455 based on lhcb-master-mr/7477, !2201 (@lhcbsoft)
- Follow changes in LHCb!4051 and Rec!3364, !2195 (@graven)
- B2OC: add new Hlt2 lines and a speed up strategy for the 3- and 4-body builders, !2194 (@abertoli)
- Add detached dimuon Jpsi and psi2s lines to full stream, !2174 (@peilian)
- Standard particles: Remove ProbNN, GhostProb cuts. Fix decaydescriptor clones and PID cut issue., !2101 (@rmwillia)
- Update References for: Allen!1162 based on lhcb-master-mr/7453, !2197 (@lhcbsoft)
- Update References for: Detector!371 based on lhcb-master-mr/7448, !2196 (@lhcbsoft)
- Update References for: LHCb!3728, Rec!3160, Moore!1836, Moore!2189 based on lhcb-master-mr/7422, !2192 (@lhcbsoft)
- Allow MEP files when downloading locally from TestFileDB, !2191 (@raaij)
- Add monitoring histos to BdToDsmK_DsmToHHH_spucing_FEST_line, !2190 (@eleckste) [[lhcb-dpa/project#211]
- Ramalric/EMXCharm_hlt2_kshh -  Fix for self-conjugated D0 decays, !2186 (@reamalri)
- Update References for: Rec!3322, DaVinci!853 based on lhcb-master-mr/7437, !2144 (@lhcbsoft)
- Flavour tagging particles and tests, !1836 (@dtou)
