2024-09-29 Moore v55r13
===

This version uses
Allen [v4r13](../../../../Allen/-/tags/v4r13),
Rec [v36r13](../../../../Rec/-/tags/v36r13),
Lbcom [v35r13](../../../../Lbcom/-/tags/v35r13),
LHCb [v55r13](../../../../LHCb/-/tags/v55r13),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1),
Detector [v1r36](../../../../Detector/-/tags/v1r36) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.0.

This version is released on the `2024-patches` branch.
Built relative to Moore [v55r12p3](/../../tags/v55r12p3), with the following changes:

### New features ~"new feature"

- Upstream project highlights :star:
- Add HLT2 BuSca Lines, !3846 (@jzhuo)


### Fixes ~"bug fix" ~workaround

- ~selection | Fix bug in hyperons.py, !3845 (@mstahl)
- ~selection | Applying tighter cuts to bnoc.bbaryon_to_phhh.Lb0ToPpPipPimPim_NoPID, !3856 (@msaur)
- ~Tracking ~"PV finding" ~Persistency | Fix Velo track duplication (in persistreco), !3855 (@mveghel)
- Upstream project highlights :star:
- ~"Flavour tagging" | Fix the configuration of the OSVertexCharge tagger, !3783 (@cprouve)
- Hotfix Xic builders to match corresponding Lc builders PID cuts, !3818 (@anmorris) [#830]


### Enhancements ~enhancement

- Upstream project highlights :star:
- ~selection | Add sanity cuts to B&Q double charm lines, !3883 (@tevans)
- ~selection | Disable all charm cross-section lines, !3873 (@tpajero) [#840]
- ~selection | Reduce bandwidth of D2KSHH lowbias HLT2 lines, !3871 (@mamartin)
- ~selection | Tune d24h lines, !3870 (@tevans)
- ~selection | Tighten cuts on B2CC JpsiToEE lines, !3817 (@yimingli) [#829]
- ~RICH ~"MC checking" | Rich ref index update support, !3812 (@jonrob)
- Reduce rate of SLB lines in full stream, !3882 (@gligorov)
- Tighten Bc2BsPi/K lines, !3879 (@gligorov)
- BnoC BdsToKstzKstzb line reduce bandwidth, !3872 (@mmonk)
- [Charm] BW reduction of doubly charmed baryon lines Sep 2024, !3869 (@axu) [#840]
- Introduce filters on HLT1 lines for D2HHGamma, !3868 (@gligorov) [#854]
- Remove BdToPipPim_NoPID Turbo line, !3866 (@avilla)
- Tighten Xic0ToLmdKmPip_TT_DD line, !3863 (@ziyiw)
- RD hotfix for #840, !3859 (@mramospe) [#840]
- Add low-k Lc-p femtoscopy line, !3867 (@thboettc)
- Prescale D02KSKS DDDD DDLD Loose and Tight lines, !3864 (@lpica)
- Tighten D2hhPi0 resolved lines, !3862 (@gligorov)
- Prescale and hlt1_filter for two high-bandwidth QEE full lines, !3857 (@rjhunter)
- Prescale Hlt2BandQ_EMDM_B2D3Pi_D2KS3Pi_TT, !3854 (@mengzhen)
- Further prescale the muon detector shower line to reduce bandwidth, !3850 (@rjhunter)
- Higher m(eegamma) mass threshold for enable eta mass fit, !3811 (@fevolle)
- SLB: MRs targeting the SeptMD Deadline, !3793 (@groberts)
- Updated B2CC lines for SeptMD, !3789 (@yimingli)
- RD developments towards September 2024 MD, !3750 (@matzeni)
- BandQ development towards September deadline, !3742 (@yajing)
- Charm changes for Sept 2024 deadline, !3741 (@tpajero)
- BnoC MRs for the September selections deadline, !3727 (@fswystun)
- QEE: Prescale Hlt2QEE_SexaquarkProtonToLambda0Kp_DDdDecision, !3816 (@rjhunter)
- B2OC: updates to hlt2 and spruce lines for RTA/2024.10.07-SeptMD, !3794 (@abertoli)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Updating HLT1 sequence in allen_hlt1_pp_default_with_dec_logger, !3759 (@msaur)
- Enabling RecoConf.allen_gaudi_downstream_with_mcchecking, !3757 (@msaur) [#698]
- Upstream project highlights :star:
- Update qm test for the new delectronsoft line, !3825 (@tmombach)
- Update test_lbexec_spruce_pp_2024_mc.qmt, !3810 (@nskidmor)


### Documentation ~Documentation

### Other

- Update References for: Moore!3873, DaVinci!1151 based on lhcb-2024-patches-mr/1571, !3887 (@lhcbsoft)
- Update References for: Rec!4085, Moore!3855 based on lhcb-2024-patches-mr/1563, !3886 (@lhcbsoft)
- Update References for: Rec!4068 based on lhcb-2024-patches-mr/1537, !3874 (@lhcbsoft)
- Follows https://gitlab.cern.ch/lhcb/Rec/-/merge_requests/4070, !3830 (@samarian)
- Update References for: Allen!1735 based on lhcb-2024-patches-mr/1421, !3853 (@lhcbsoft)
- Update References for: Moore!3750 based on lhcb-2024-patches-mr/1512, !3848 (@lhcbsoft)
- Update References for: Rec!4033 based on lhcb-2024-patches-mr/1490, !3841 (@lhcbsoft)
- Update References for: Allen!1134, Moore!3825 based on lhcb-2024-patches-mr/1483, !3838 (@lhcbsoft)
- Update References for: Rec!4041, Rec!4053 based on lhcb-2024-patches-mr/1470, !3828 (@lhcbsoft)
- Update References for: Rec!4025, Moore!3783, DaVinci!1144, LHCbIntegrationTests!78 based on lhcb-2024-patches-mr/1431, !3820 (@lhcbsoft)
