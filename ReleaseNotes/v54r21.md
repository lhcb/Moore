2023-10-06 Moore v54r21
===

This version uses
Allen [v3r21](../../../../Allen/-/tags/v3r21),
Rec [v35r20](../../../../Rec/-/tags/v35r20),
Lbcom [v34r19](../../../../Lbcom/-/tags/v34r19),
LHCb [v54r19](../../../../LHCb/-/tags/v54r19),
Detector [v1r22](../../../../Detector/-/tags/v1r22),
Gaudi [v36r16](../../../../Gaudi/-/tags/v36r16) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Moore [v54r20p1](/../../tags/v54r20p1), with the following changes:

### New features ~"new feature"

- Upstream project highlights :star:


### Fixes ~"bug fix" ~workaround

- Upstream project highlights :star:


### Enhancements ~enhancement

- Upstream project highlights :star:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Fix hlt2_check_output qmtest (follow up !2436), !2687 (@graven)
- Upstream project highlights :star:


### Documentation ~Documentation

### Other

- ~hlt2 | Fix B->Xll sprucing lines filter codes functions, !2673 (@jagoodin)
- ~Persistency | Options for neutral pid integration test, !2636 (@sesen)
- ~Persistency | Lumi line that "nanofies" events, !2436 (@rmatev)
- Check for exact line names in allen_*_write tests, !2682 (@rmatev)
- Update References for: Allen!1345 based on lhcb-master-mr/9377, !2681 (@lhcbsoft)
- Update References for: LHCb!4301, Rec!3596 based on lhcb-master-mr/9309, !2680 (@lhcbsoft)
- Update References for: LHCb!4292, Moore!2436 based on lhcb-master-mr/9242, !2655 (@lhcbsoft)
- Changes to BW test options files to go with PRConfig!349, !2651 (@rjhunter)
- Fix errors caused by DD4Hep future upgrades refactor, !2638 (@emmuhamm)
