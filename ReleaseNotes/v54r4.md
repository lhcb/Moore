2023-01-27 Moore v54r4
===

This version uses
Allen [v3r4](../../../../Allen/-/tags/v3r4),
Rec [v35r4](../../../../Rec/-/tags/v35r4),
Lbcom [v34r4](../../../../Lbcom/-/tags/v34r4),
LHCb [v54r4](../../../../LHCb/-/tags/v54r4),
Detector [v1r8](../../../../Detector/-/tags/v1r8),
Gaudi [v36r9](../../../../Gaudi/-/tags/v36r9) and
LCG [101a](http://lcginfo.cern.ch/release/101a_LHCB_7/) with ROOT 6.24.08.

This version is released on the `master` branch.
Built relative to Moore [v54r3](/../../tags/v54r3), with the following changes:


### Fixes ~"bug fix" ~workaround

- Fix typo in translation table, !1993 (@tfulghes)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration | Speed up HLT2 python configuration, !1831 (@rmatev)
- ~Decoding ~Calo | Clean up Calo decoding configuration, !1975 (@cmarinbe) [#491]


### Documentation ~Documentation

- ~Functors | Update the LoKi-ThOr conversion table, !1992 (@tfulghes)
- Fix typo in translation table, !1993 (@tfulghes)

### Other

- ~selection | Release comment-out BandQ lines, !2007 (@mengzhen)
- ~selection ~hlt2 | HLT2 lines: LbToL0pipill, LbToPKpipill, LbToPpPimll, LbToPpKSPimll, OmbToXimll, !1738 (@msaur)
- ~hlt2 | Fix registration of HLT2 IFT lines in rate tests, !2003 (@oboenteg)
- ~hlt2 | Include the mass and decay time monitoring variables in interesting charm lines, !1971 (@lutoscan)
- ~hlt2 | HLT2 lines for charmonium + open charm, !1897 (@yajing) [TDR-018]
- ~hlt2 | Include SMOG2 HLT2 lines in rate tests, !1972 (@kmattiol)
- ~Configuration ~RICH | Add Min/Max momentum cut options to RICH reco configuration, !2015 (@jonrob)
- ~Calo ~"Event model" | Add track-cluster match info to neutral pp, !1977 (@mveghel)
- ~Persistency | Split packing into streams, !2006 (@sesen) [#509]
- Update References for: Rec!3283, Analysis!948 based on lhcb-master-mr/6811, !2029 (@lhcbsoft)
- Update References for: LHCb!3937 based on lhcb-master-mr/6787, !2020 (@lhcbsoft)
- Run all lines for hlt2_analytics and remove the qmtest, !2019 (@shunan)
- Update References for: Rec!3268 based on lhcb-master-mr/6772, !2017 (@lhcbsoft)
- Update References for: Allen!1101 based on lhcb-master-mr/6747, !2014 (@lhcbsoft)
- Update References for: Rec!3273 based on lhcb-master-mr/6743, !2008 (@lhcbsoft)
- Add possibility for 5- and 6-body combinations (in ThOR), !2000 (@ipolyako) [PAPER-2016]
- Update References for: LHCb!3721, Moore!1782 based on lhcb-master-mr/6741, !2004 (@lhcbsoft)
- B&Q: Xibc/Tbc -> J/psi + hadrons, !2001 (@ipolyako)
- Update References for: Moore!1844 based on lhcb-master-mr/6714, !1994 (@lhcbsoft)
- Add fitted Upstream particles with PID, !1844 (@cprouve)
- Follow LHCb!3721, !1782 (@graven)
- Update References for: LHCb!3845, Rec!3115, Analysis!935, DaVinci!771 based on lhcb-master-mr/6696, !1990 (@lhcbsoft)
- Update References for: Rec!3252 based on lhcb-master-mr/6673, !1986 (@lhcbsoft)
- Fix cmake for sanitizer builds, !1983 (@rmatev)
- Update References for: Allen!1082 based on lhcb-master-mr/6648, !1982 (@lhcbsoft)
- Update References for: LHCb!3913, Rec!3255, Moore!1977 based on lhcb-master-mr/6647, !1981 (@lhcbsoft)
- Workarounds for failing tests in master, !1980 (@rmatev) [LHCb#151]
- Allen simple best track creator, !1967 (@dcampora)
