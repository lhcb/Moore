2024-08-29 Moore v55r12p1
===

This version uses
Allen [v4r12p1](../../../../Allen/-/tags/v4r12p1),
Rec [v36r12p1](../../../../Rec/-/tags/v36r12p1),
Lbcom [v35r12](../../../../Lbcom/-/tags/v35r12),
LHCb [v55r12](../../../../LHCb/-/tags/v55r12),
Detector [v1r35](../../../../Detector/-/tags/v1r35),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `2024-patches` branch.
Built relative to Moore [v55r12](/../../tags/v55r12), with the following changes:

- ~Monitoring | Hlt2 Lines for OfflineDQ, !2908 (@jessicma)
- Update References for: Rec!4014, Moore!2908 based on lhcb-2024-patches-mr/1356, !3797 (@lhcbsoft)
- Update References for: Rec!4047, Rec!4050 based on lhcb-2024-patches-mr/1333, !3790 (@lhcbsoft)
- First commit for sprucing MC, !3724 (@nskidmor)
