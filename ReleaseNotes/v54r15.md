2023-08-04 Moore v54r15
===

This version uses Allen [v3r15](../../../../Allen/-/tags/v3r15),
Rec [v35r14](../../../../Rec/-/tags/v35r14),
Lbcom [v34r14](../../../../Lbcom/-/tags/v34r14),
LHCb [v54r14](../../../../LHCb/-/tags/v54r14),
Gaudi [v36r16](../../../../Gaudi/-/tags/v36r16),
Detector [v1r18](../../../../Detector/-/tags/v1r18) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.


This version is released on the `master` branch.
Built relative to Moore [v54r14](/../../tags/v54r14), with the following changes:

### New features ~"new feature"

- Upstream project highlights :star:


### Fixes ~"bug fix" ~workaround

- ~selection ~hlt2 | Charm: bugfix for Moore!2377, !2402 (@mamartin)
- Upstream project highlights :star:


### Enhancements ~enhancement

- ~Monitoring | Add monitoring histograms to multiple lines, for RTA PromptDQ, !2535 (@gciezare)
- Turcal processing, new tests and cleanup, !2483 (@nskidmor)
- Upstream project highlights :star:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Persistency | Adding additional test for RawBanks SP, !2405 (@msaur) [#570]
- Remove no longer needed `load_manifest` calls, !2509 (@graven)
- Add dedicated dd4hep refs for allen_gaudi tests, !2548 (@rmatev)
- Upstream project highlights :star:


### Other

- ~selection | Remove GhostProb cuts from standard particles, !2161 (@alobosal)
- ~selection ~hlt2 | BnoC: HLT2 / Spruce lines: 2-/3-/4-/5-body baryonic modes, !2203 (@msaur)
- ~selection ~hlt2 | MR for  upsilon -> ll lines, !2110 (@nraja)
- ~selection ~hlt2 | Update d0_to_hh selections, !1907 (@nkleijne) [#3]
- ~selection ~hlt2 | Added XTauTau electronic lines, !1451 (@htilquin)
- ~selection ~hlt2 | RD: XTauTau and XTauMu Hlt2 and Spruce lines, !1079 (@htilquin)
- ~hlt2 | Remove SDOCA for LL particles combination in RD, !2395 (@tfulghes)
- ~hlt2 | Prescale prompt B2CC  lines, !2383 (@oozcelik)
- ~hlt2 | Detached dilepton trigger 2023 tuning, !2449 (@lecarus)
- ~hlt2 | Fix too high rates for multilepton builders, !2387 (@tmombach)
- ~hlt2 | RD: prefer ParticleFilters to ParticleCombiners for rdbuilders, remove duplicates in B2XTauL, !2355 (@tfulghes)
- ~hlt2 ~Muon ~Calo | Setting persistreco to true to Velo2Long_B2JpsiK_ElectronProbe and Velo2Long_B2JpsiK_MuonProbe (V2), !2493 (@gpietrzy)
- Update References for: Moore!2509 based on lhcb-master-mr/8764, !2553 (@lhcbsoft)
- Prep for sprucing productions, !2547 (@nskidmor)
- Update References for: LHCb!4168, Rec!3496, Allen!1283 based on lhcb-master-mr/8727, !2551 (@lhcbsoft)
- Fix Sprucing BW test options, !2545 (@shunan)
- Update documentation, !2519 (@mwaterla)
- Update References for: Detector!394, LHCb!4151, Rec!3451, Allen!1240, Moore!2411 based on lhcb-master-mr/8672, !2544 (@lhcbsoft)
- Adapt to Detector!394, !2411 (@lohenry)
