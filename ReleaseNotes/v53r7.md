2022-08-03 Moore v53r7
===

This version uses
  Gaudi: [v36r7](../../../../Gaudi/-/tags/v36r7),
  Detector: [v1r3](../../../../Detector/-/tags/v1r3),
  LHCb: [v53r10](../../../../LHCb/-/tags/v53r10),
  Lbcom: [v33r10](../../../../Lbcom/-/tags/v33r10),
  Rec: [v34r4](../../../../Rec/-/tags/v34r4),
  Allen : [v2r2](../../../../Allen/-/tags/v2r2)

This version is released on `master` branch.
Built relative to Moore [v53r6](/../../tags/v53r6), with the following changes:

### New features ~"new feature"

- ~selection ~hlt2 | Charm HLT2 lines related to detection asymmetries, !1281 (@ldufour)
- ~selection ~hlt2 | Add B2ll sprucing lines and fix B2llgamma, !1458 (@tmombach)
- ~Tracking | PrKalmanFilter and PrKalmanFilterTool returning v3::Tracks, ExtraInfo and TrackFitResult, !1642 (@ausachov)
- Create Allen run configuration for Allen wrappers, !609 (@dcampora)
- Add b_to_v0ll lines to __init__.py file (follow up !1209), !1269 (@fevolle)
- Upstream project highlights :star:


### Fixes ~"bug fix" ~workaround

- ~Persistency | Sorted object types dictionary, !1543 (@sesen)
- ~Build | Reintroduce hlt2_pp_default functor cache, !1522 (@chasse) [#420]
- Fix(doc) functor repr now prints @ instead of chain, !1546 (@chasse)
- Upstream project highlights :star:


### Enhancements ~enhancement

- ~selection ~hlt2 | Add PV filter to V0 monitoring lines, !1487 (@rmatev)
- ~selection ~hlt2 | Radiative inclusive hh(h)g lines to ThOr, !1331 (@alobosal)
- ~UT | Modify configuration of sequences without UT to call the PrIgnoreUTHitsTool, !1504 (@decianm)
- ~Calo | Updated brem method, !1460 (@mveghel)
- ~Calo ~"MC checking" | Add pi0_cluster_efficiency counters, !1498 (@conrad)
- Refactor(testing) remove a few old exclusions that were quite costly, !1668 (@chasse)
- Upstream project highlights :star:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~selection ~hlt2 | Drop charm builders, !1476 (@mstahl)
- ~Configuration | Replace '${PARAMFILESROOT}' with 'paramfile:/', !1483 (@graven)
- ~Tracking | Use PrUTHits for GhostProb, !1512 (@decianm)
- ~"PV finding" | Add test for PV reconstruction with PatPV3DFuture, !1651 (@decianm)
- ~Muon ~PID | Change MuonPIDConverter name, !1678 (@rvazquez)
- ~Functors | Follow Rec!2683, !1497 (@chasse)
- ~Persistency | Update test_hlt2_check_packed_data_check following lhcb/LHCb!3524, !1517 (@rmatev)
- ~Monitoring | Update test for new JSONSink, !1693 (@jheuel)
- ~Build | Update hlt2_all_lines_analytics test, !1632 (@shunan)
- ~Build | Mitigate very slow ref validation in hlt2_protoparticles test, !1577 (@rmatev) [#434]
- Test running Allen sequence on XDST, !1542 (@raaij)
- Update dump test to properly use a reference, !1592 (@raaij) [#428]
- Fixed exception in validator, !1541 (@lohenry)
- Update to test Hlt1Conf.persistency.mdf_read_decs_allen, !1515 (@spradlin)
- Remove explicit configuration of FT DecodingVersion, !1318 (@raaij)
- Upstream project highlights :star:


### Documentation ~Documentation

- Update docs for MooreAnalysis!81, !1545 (@rjhunter)
- Added line to documentation about how to run HLT2 on HLT1 output, !1598 (@jedavies)
- Fix(doc) functor repr now prints @ instead of chain, !1546 (@chasse)
- Update hlt2_analysis.rst, !1513 (@sesen)
- Fix bug in new Moore documentation section, !1501 (@gligorov)
### Other

- ~selection | Add an option file to run Sprucing throughput test, !1630 (@shunan) [lhcb-dpa/project#25,lhcb-dpa/project#90]
- ~selection | Lines with charged hyperons as long tracks, !1530 (@mstahl)
- ~selection | Add sprucings for detached Jpsi, psi2S, tight prompt Jpsi, psi2S, !1584 (@mengzhen) [TDR-018]
- ~selection | Move spruce_all_lines_analytics test to nightlies, !1507 (@shunan) [lhcb-dpa/project#90]
- ~selection ~hlt2 | BNV and LFV lines, !1195 (@fdevelli)
- ~selection ~hlt2 | Early Measurement Charm production XSec update line cuts, !1523 (@gunther)
- ~selection ~hlt2 | Lc+ -> p K- pi+,  Xic+ -> p K- pi+, Xic0 -> p K- K- pi+ spectroscopy lines, !788 (@roneil)
- ~selection ~hlt2 | Update d2hhh lines, !1442 (@fsouzade)
- ~selection ~hlt2 | Bs2PhiGamma and Bd2KstGamma HLT2 and Sprucing lines, !1240 (@isanders)
- ~selection ~hlt2 | Update D0->KsKs HLT2 lines using the new flag to select LD candidates, !1607 (@gtuci)
- ~selection ~hlt2 | Reduce the rates of double-charm lines in B&Q wg, !1591 (@hmu)
- ~selection ~hlt2 | DiMuonNoIP lines, !1419 (@kaaricha)
- ~selection ~hlt2 | Add RD HLT2 lines: Omega to Xi X, !1222 (@chuangxi)
- ~selection ~hlt2 | Updating b->sl+l- sprucing lines, !1533 (@fevolle)
- ~selection ~hlt2 | RD fixes, !1532 (@msaur)
- ~hlt2 | Add option to exclude incompatible lines in run_moore, !1581 (@poluekt)
- ~hlt2 | New HLT2 sequences for PbPb data taking with and without UT, !1603 (@oboenteg)
- ~hlt2 | BnoC HLT2 Lines for Early Measurements, !1565 (@oozcelik)
- ~hlt2 | Fix rates of strange lines, !1527 (@mramospe)
- ~hlt2 | Fix Hlt2RD radinclusive line persistance, !1528 (@alobosal)
- ~hlt2 | Add Muon pid HLT2 lines for calibration, !1490 (@fdordei)
- ~hlt2 ~Tracking | Add dimuon track efficiency lines to hlt2_pp_thor, !1567 (@peilian)
- ~hlt2 ~Calo ~Monitoring | Hlt2 Monitoring pi0 to gamma gamma with calo only reco, !1098 (@alobosal)
- ~hlt2 ~PID | Add Hadronic PID HLT2 Lines, !1261 (@dcervenk)
- ~hlt2 ~Monitoring | Default monitoring histograms for Hlt2 lines, !1636 (@mstahl) [#398]
- ~Configuration | Add light reco sequence with PrKalman and no UT, for HLT2 lines reco, !1535 (@decianm)
- ~Configuration | Support running with lbexec, !1464 (@cburr)
- ~Tracking | Script to run PrDebugMatchToolNN, !1520 (@sesen)
- ~VP | RetinaCluster v1, !1280 (@gbassi)
- ~Calo | Add TAE spectrum to configuration, !1680 (@nuvallsc)
- ~Calo | Add GraphClustering to resolution scripts, !1540 (@nuvallsc)
- ~Calo | Modifications needed for the new Calo Digit MC linker, !1525 (@jmarchan)
- ~Calo ~PID | Obsolete calo code cleanup, !1526 (@mveghel)
- ~Calo ~"MC checking" | Add calo efficiency checkers to all reco sequences, !1676 (@cmarinbe)
- ~Calo ~Monitoring | Optional split clusters monitoring, !1683 (@cmarinbe)
- ~Calo ~Monitoring | Add Calo monitoring plots of ADCs RMS, !1604 (@alobosal)
- ~RICH | Add new RICH data monitor 'TrackGeometry', !1538 (@jonrob)
- ~Jets | Include Jet energy calibration option in FastJetBuilder, !1586 (@rangel)
- ~Monitoring | Adding track resolution checkers to options file, !1682 (@sklaver)
- ~Monitoring | Removing number of events from hlt1_reco_allen_track_reconstruction, !1669 (@msaur)
- Update References for: Rec!2960 based on lhcb-master-mr/5286, !1705 (@lhcbsoft)
- Update References for: Moore!1651 based on lhcb-master-mr/5260, !1702 (@lhcbsoft)
- Update References for: Rec!3034, Moore!1678 based on lhcb-master-mr/5245, !1701 (@lhcbsoft)
- Update References for: LHCb!3154 based on lhcb-master-mr/5248, !1700 (@lhcbsoft)
- Fix barrier config, !1684 (@nnolte)
- Add refs for !1624, !1697 (@rmatev)
- Update References for: LHCb!3452, Boole!398, Rec!2958, Allen!874, Moore!1635 based on lhcb-master-mr/5214, !1696 (@lhcbsoft)
- Apply correct db tags to qmtest, !1691 (@cprouve)
- Update References for: Lbcom!621, Moore!1676 based on lhcb-master-mr/5182, !1690 (@lhcbsoft)
- Add velo-only tracking for Allen in Gaudi, !1635 (@gbassi)
- Tests for tracking only sequence, !1624 (@decianm)
- Follow up from starterkit, !1435 (@mstahl)
- Update References for: Allen!861 based on lhcb-master-mr/5177, !1687 (@lhcbsoft)
- Update References for: Moore!1682 based on lhcb-master-mr/5154, !1686 (@lhcbsoft)
- Update References for: LHCb!3622, Rec!2908, Rec!2951, Moore!1567 based on lhcb-master-mr/5123, !1681 (@chasse)
- Make chi2 of TBTC configurable, !1679 (@sstahl)
- KstG/PhiG high-PT lines for photon pid, !1625 (@bdey) [INT-2021]
- Avoid potentially huge (and inefficient) regex, !1677 (@graven)
- Update References for: Detector!244 based on lhcb-master-mr/5076, !1671 (@lhcbsoft)
- Update standard loki-thor functor table to add CL, !1664 (@johndan)
- Fix for Z+jet sprucing lines, !1663 (@lsestini)
- Update to new functor syntax, !1661 (@johndan)
- Add writing and reading of RecSummary, !1649 (@sstahl)
- Update References for: LHCb!3671, Rec!3007, Moore!1649 based on lhcb-master-mr/5054, !1667 (@lhcbsoft)
- Speed up qm test by 10 minutes, !1665 (@sstahl)
- Update References for: Rec!2994 based on lhcb-master-mr/5022, !1662 (@lhcbsoft)
- Update References for: LHCb!3427, LHCb!3366, Rec!2741, Moore!1642, Alignment!294 based on lhcb-master-mr/5009, !1659 (@lhcbsoft)
- Update References for: Moore!1636 based on lhcb-master-mr/4977, !1654 (@lhcbsoft)
- New e+ e- gamma hlt2 line, !1365 (@johndan)
- Update References for: Moore!1655 based on lhcb-master-mr/4989, !1658 (@lhcbsoft)
- Update References for: Rec!2952, Moore!1604, MooreOnline!115 based on lhcb-master-mr/4994, !1657 (@lhcbsoft)
- Update References for: Moore!1571 based on lhcb-master-mr/4997, !1656 (@lhcbsoft)
- Unify persistreco checks, !1655 (@sesen)
- Update References for: LHCb!3589, Lbcom!613, Rec!2937, Allen!896, Moore!1650 based on lhcb-master-mr/4971, !1653 (@lhcbsoft)
- Make Calo event model types SOACollections, !1650 (@ahennequ)
- Update References for: Allen!838 based on lhcb-master-mr/4964, !1648 (@lhcbsoft)
- Add muon monitoring in reconstruction, !1600 (@rvazquez)
- Add test without UT without MC checking or monitoring, !1571 (@decianm)
- Add hooks to be able to set the new 'Panels' property of the RICH decoder, !1646 (@jonrob)
- Update References for: LHCb!3664 based on lhcb-master-mr/4932, !1645 (@lhcbsoft)
- Update References for: LHCb!3536, Rec!2983, Alignment!291 based on lhcb-master-mr/4920, !1643 (@lhcbsoft)
- Add test for running two recos, !1641 (@sstahl)
- Update References for: Allen!691 based on lhcb-master-mr/4905, !1640 (@lhcbsoft)
- Update References for: LHCb!3659, Moore!1634 based on lhcb-master-mr/4917, !1639 (@lhcbsoft)
- Updating reco_decode_retina tests to use VPRetinaFullClusterDecoder, !1634 (@dpassaro)
- Hlt1 hlt2 comparison options, !1495 (@valukash)
- Dump Rich hits in phoenix json files, !1637 (@sponce)
- Fixed bad ref file for hlt2_protoparticles_fastest, !1633 (@sponce)
- Fix option files that include GetFlavourTaggingParticles, !1631 (@cprouve)
- Update References for: Rec!2904, Moore!1626 based on lhcb-master-mr/4840, !1628 (@lhcbsoft)
- Rename TrackPV2HalfAlignMonitor to TrackPV2HalfAlignMonitor, !1626 (@jcobbled)
- Update References for: LHCb!3616, Rec!2942 based on lhcb-master-mr/4787, !1622 (@lhcbsoft)
- Easier configuration of Allen sequence by name, !1615 (@spradlin)
- Fixed phoenix file checking for inclusion of time, !1623 (@sponce)
- Fix file names, !1621 (@decianm)
- Add information how to select non-default reco sequences, !1620 (@decianm)
- Update References for: Moore!1612 based on lhcb-master-mr/4774, !1619 (@lhcbsoft)
- Fix import of GEC in fastest protoparticle test, !1612 (@decianm)
- Update References for: LHCb!3629 based on lhcb-master-mr/4748, !1611 (@lhcbsoft)
- Follow changes to SelTools::LifetimeFitter, !1601 (@chasse)
- New test for additional info expected in Allen SelReports., !1550 (@spradlin) [#252]
- Follow up of moving functions out of hlt1_tracking, !1486 (@sstahl)
- Fixed hlt1_reco_velo_only test for the DD4hep branch, !1450 (@bcouturi)
- Update References for: LHCb!3562, Moore!1580, DaVinci!691 based on lhcb-master-mr/4668, !1597 (@lhcbsoft)
- Update References for: Rec!2920, Moore!1590 based on lhcb-master-mr/4609, !1596 (@lhcbsoft)
- Update References for: Moore!1540 based on lhcb-master-mr/4613, !1595 (@lhcbsoft)
- Disable RecoConf.hlt2_protoparticles_baseline test in dbg and clang builds, !1593 (@sponce)
- Deactivate CatBoost in Hlt1, !1590 (@rvazquez)
- Allow for missing reco objects in persistency, !1589 (@sesen)
- Changes to checking scripts due to changes in reading helpers in LHCb, !1580 (@nskidmor)
- Use the new {UT,VP}MeasurementProvider instead of MeasurementProviderT, !1551 (@clemenci)
- Update References for: Moore!1538 based on lhcb-master-mr/4573, !1594 (@lhcbsoft)
- Fix tests for Allen!875, !1588 (@samarian) [Allen#324]
- Update References for: LHCb!3564, Rec!2821 based on lhcb-master-mr/4553, !1587 (@lhcbsoft)
- Added muon hits to phoenix dumps, !1583 (@sponce)
- Fix allen tests, !1576 (@raaij)
- Update References for: Moore!1560 based on lhcb-master-mr/4505, !1582 (@lhcbsoft)
- Revert "Merge branch 'sponce_addMuonHits' into 'master'", !1579 (@sponce)
- Added Muon hits to JSON dumps, !1578 (@sponce)
- Change phoenix export config to use the HLT2 without UT config, !1575 (@bcouturi)
- Allow PrimaryVertices to be filtered with ThOr functors, !1570 (@samarian)
- Add cell by cell histograms for calo Time Alignment, !1563 (@nuvallsc)
- Removing creation of UT hits in GhostProb and MC checking, !1560 (@decianm)
- Update References for: LHCb!3577, Rec!2898, Moore!1559 based on lhcb-master-mr/4504, !1574 (@lhcbsoft)
- Update References for: Rec!2873 based on lhcb-master-mr/4492, !1573 (@lhcbsoft)
- Follow update for DD4HEP compatibility of Allen, !1569 (@raaij)
- Follow Rec!2909, !1566 (@chasse)
- Script to run PrDebugMatchToolNN, !1559 (@sponce)
- Fix tests for Allen/602, !1548 (@samarian)
- Charmonium + neutral lines with vertex cuts, !1506 (@chefdevi)
- Update References for: LHCb!3540, Rec!2871, Moore!1526 based on lhcb-master-mr/4484, !1572 (@lhcbsoft)
- Update References for: Rec!2847 based on lhcb-master-mr/4449, !1562 (@chasse)
- Do not make chi2 and dof mandatory in phoenix tracks, !1561 (@sponce)
- Update References for: Rec!2866 based on lhcb-master-mr/4432, !1558 (@lhcbsoft)
- Revert "Merge branch 'ref_bot_Rec2861_Moore1520' into 'master'", !1557 (@sponce)
- Revert "Merge branch 'sevda-matching' into 'master'", !1556 (@sponce)
- Remove Brunelesque reconstruction qm tests, !1555 (@decianm)
- Example config to dump protoparticles to Phoenix JSON format, !1537 (@bcouturi)
- Update References for: Rec!2861, Moore!1520 based on lhcb-master-mr/4406, !1553 (@lhcbsoft)
- Update References for: Detector!165, LHCb!3385, Rec!2695, Allen!748, Moore!1280 based on lhcb-master-mr/4367, !1547 (@rmatev)
- Update refs from lhcb-master/1677, !1544 (@rmatev)
- Update References for: Detector!166, LHCb!3502, Rec!2836, Moore!1538 based on lhcb-master-mr/4358, !1539 (@lhcbsoft)
- Update References for: Detector!169, LHCb!2942, Boole!379, Allen!849, Moore!1318 based on lhcb-master-mr/4321, !1524 (@lhcbsoft)
- Restore Sprucing check test, !1521 (@nskidmor)
- Update References for: Rec!2856, Moore!1512 based on lhcb-master-mr/4304, !1519 (@lhcbsoft)
- Update refs for Moore!1504, !1518 (@rmatev)
- Update References for: Lbcom!605, Moore!1498 based on lhcb-master-mr/4260, !1516 (@lhcbsoft)
- Update References for: LHCb!3496, Rec!2817, Moore!1460 based on lhcb-master-mr/4238, !1511 (@lhcbsoft)
