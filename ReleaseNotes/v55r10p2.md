2024-06-22 Moore v55r10p2
===

This version uses
Allen [v4r10p2](../../../../Allen/-/tags/v4r10p2),
Rec [v36r10p2](../../../../Rec/-/tags/v36r10p2),
Lbcom [v35r10p1](../../../../Lbcom/-/tags/v35r10p1),
LHCb [v55r10p1](../../../../LHCb/-/tags/v55r10p1),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1),
Detector [v1r33](../../../../Detector/-/tags/v1r33) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `2024-patches` branch.
Built relative to Moore [v55r10p1](/../../tags/v55r10p1), with the following changes:

### New features ~"new feature"

- Upstream project highlights :star:


### Fixes ~"bug fix" ~workaround

- Upstream project highlights :star:


### Enhancements ~enhancement

- Upstream project highlights :star:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Extend timeout for test_hlt2_check_packed_data_checksums, !3584 (@jonrob)
- Update refs based on lhcb-2024-patches/72, !3581 (@jonrob)
- Disable all tests for cuda platforms, !3542 (@rmatev)
- Upstream project highlights :star:


### Documentation ~Documentation

### Other

- Update References for: Allen!1687 based on lhcb-2024-patches-mr/853, !3597 (@msaur)
- [RTA/DPA BW tests] Maintenance on the bandwidth tests, !3496 (@rjhunter)
- Add option to use full VELO tracking, !3161 (@freiss)
