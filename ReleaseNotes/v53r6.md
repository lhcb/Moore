2022-04-19 Moore v53r6
===

This version uses
Allen [v2r1](../../../../Allen/-/tags/v2r1),
Rec [v34r2](../../../../Rec/-/tags/v34r2),
Lbcom [v33r8](../../../../Lbcom/-/tags/v33r8),
LHCb [v53r8](../../../../LHCb/-/tags/v53r8),
Gaudi [v36r5](../../../../Gaudi/-/tags/v36r5),
Detector [v1r1](../../../../Detector/-/tags/v1r1) and
LCG [101](http://lcginfo.cern.ch/release/101/) with ROOT 6.24.06.

This version is released on `master` branch.
Built relative to Moore [v53r5](/../../tags/v53r5), with the following changes:

### New features ~"new feature"

- ~selection | B->3lnu lines, !1283 (@vlisovsk)
- ~selection ~hlt2 | Add Electron PID HLT2 lines for Calib Stream, !1402 (@sstanisl)
- ~selection ~hlt2 | Charm HLT2 lines: LcTophh update - Xicp, !1358 (@msaur)
- ~selection ~hlt2 | Charm HLT2 lines: Xic0 to pk, !1277 (@msaur)
- ~selection ~hlt2 | Charm HLT2 lines: D2HH0 with H0->eegamma and H0->eeee, !1271 (@thadaviz)
- ~selection ~hlt2 | B - > X l l - HLT2 lines, !1187 (@rmwillia)
- ~hlt1 | Implement Hlt1 filter, !1424 (@nskidmor)
- ~hlt2 | SLB - Development of Lambda_b and Omega_b semileptonic HLT2 lines, !1343 (@mferrill) [#373]
- ~hlt2 | Add prompt multilepton lines, !1282 (@vlisovsk)
- ~hlt2 ~Configuration | HLT2 event sizes and clean up of Sprucing event sizes scripts, !1478 (@nskidmor)
- ~hlt2 ~Persistency | New persistency configuration, !1085 (@sesen)
- ~Tracking | Remove UT hits from PrKalmanFilter config without UT, !1494 (@gunther)
- Upstream project highlights :star:
  - ~Persistency | New persistency model for packing, LHCb!3268 (@sesen) [#151,Moore#248,Moore#354,lhcb-dpa/project#120]


### Fixes ~"bug fix" ~workaround

- ~selection ~hlt2 | Use muons with PID for charm lines (bugfix), !1485 (@tpajero)


### Enhancements ~enhancement

- ~selection ~hlt2 | RD HLT2 lines: qqbar_to_ll fix, !1454 (@msaur)
- ~Tracking | Adapt PrKalmanFilter config to flexible input, !1456 (@gunther)
- ~Tracking ~Monitoring | Adapt configuration of track monitoring to use PrKalmanFilter, !1466 (@jkubat)
- ~"PV finding" ~"Event model" | Adapt to change in PV finder output, !1310 (@wouter)
- ~Calo | Change data dependencies, !908 (@canorman)
- Inherit default qmtest exclusions from Rec project, !1470 (@chasse)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~selection ~Calo | Add generic Pi0ToGammaGamma ThOr line, !1459 (@cmarinbe) [#407]
- ~hlt2 ~Muon | Change name of Hlt1 MuonID alg, add track type to algorithm in Hlt2, !1455 (@decianm)
- ~Configuration | Clean up of usage of hlt1_tracking in Hlt2 lines, !1443 (@sstahl)
- ~Functors | Dummy line for neutral functors, !1434 (@tnanut)
- ~Build | FunctorCache cleanup, !1467 (@chasse)
- Remove LoKi inclusive dilepton trigger file, !1482 (@lecarus)
- Fix logical conflict between !1466 and !1494, !1509 (@rmatev)


### Documentation ~Documentation

- Add trigger design documentation, !1433 (@gligorov)


### Other

- Update References for: LHCb!3499, Rec!2413, Moore!908 based on lhcb-master-mr/4230, !1502 (@lhcbsoft)
- Update References for: LHCb!3500, Rec!2834 based on lhcb-master-mr/4217, !1500 (@rmatev)
- Update References for: LHCb!3268, Moore!1085, DaVinci!634, MooreAnalysis!69 based on lhcb-master-mr/4215, !1496 (@lhcbsoft)
- Update References for: LHCb!3468, Rec!2785, Moore!1455 based on lhcb-master-mr/4197, !1493 (@lhcbsoft)
- Update References for: LHCb!3473, Rec!2809, Allen!818, Moore!1456 based on lhcb-master-mr/4199, !1492 (@lhcbsoft)
- Update References for: LHCb!3459, Rec!2512, Moore!1310, DaVinci!667, Alignment!250, MooreAnalysis!75 based on lhcb-master-mr/4168, !1488 (@lhcbsoft)
- Update References for: Rec!2718, Rec!2814, Moore!1434, Moore!1459 based on lhcb-master-mr/4144, !1479 (@rmatev)
