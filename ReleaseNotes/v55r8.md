2024-05-08 Moore v55r8
===

This version uses
Allen [v4r8](../../../../Allen/-/tags/v4r8),
Rec [v36r8](../../../../Rec/-/tags/v36r8),
Lbcom [v35r8](../../../../Lbcom/-/tags/v35r8),
LHCb [v55r8](../../../../LHCb/-/tags/v55r8),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1),
Detector [v1r32](../../../../Detector/-/tags/v1r32) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `2024-patches` branch.
Built relative to Moore [v55r7](/../../tags/v55r7), with the following changes:

### New features ~"new feature"

- ~selection ~hlt2 | Add MVA-based lines for some b2xll modes, !3308 (@acampove)
- ~selection ~hlt2 | HLT2 MVA based lines for Lc decays in the Charm WG, !3295 (@acampove)
- ~hlt2 | Optimized control flow, !2757 (@dmagdali)


### Fixes ~"bug fix" ~workaround

- ~Tracking ~Monitoring | Update track monitoring with UT, !3245 (@mveghel)
- ~Monitoring | Fix directories of mass histograms (follow up on !3119), !3375 (@rmatev)
- Do not save raw data for all events in NOBIAS, !3368 (@rmatev)
- Fix wrong daughter_index in lb0_to_pkpipiee_ll, Hlt2RD_LbToPpKS0PimEE_LL line, !3340 (@ldufour)


### Enhancements ~enhancement

- ~UT ~Monitoring | RecoMon monitoring for including UT hits in correlation hists if available, !3302 (@tmombach)
- Add make_pvs argument in particle maker, !3208 (@ldufour)
- ~selection ~hlt2 | Reduce Hlt2Charm_LcpToL0PimPipPip_LLDecision rate, !3172 (@mireboll)
- ~hlt2 | RD developments towards April, !3157 (@mramospe) [#710]
- ~PID | ProbNN evaluation for ProtoParticle makers, !2768 (@mveghel)
- ~PID | Fix for bug in one of the PIDe lines and added extra cuts, !3294 (@pvidrier)
- Ignore too many composite warnings in QM tests, !3420 (@jonrob)
- Fix Moore references for x86_64_v3, !3412 (@lhcbsoft)
- B2OC: add 2 very low rate Spruce lines to read back the Calib lines going to tape/full, !3405 (@abertoli)
- SL: Adding in track monitoring for Hlt2 and sprucing lines, !3306 (@groberts)
- SLB: Add MVA-based isolation, !2981 (@anmorris)
- Remove ProbNN calls in line selections, !3379 (@ldufour)
- Update inclusive radiative lines, !3372 (@alobosal)
- Topological Trigger Update 2024 April, !3309 (@nschulte) [#7]
- SL: Tuning of charm builder, !3304 (@groberts)
- [RTA/DPA BW Tests]: Save persistency info -> json in Moore options files, !3329 (@rjhunter)
- BdsToJpsiX_JpsiTopp tunning, !3291 (@jbaptist)
- BnoC HLT2 lines, !3170 (@zewen)
- Hotfix: Avoid touching the combination limits in RD builder for vertex isolation, !3015 (@tfulghes)
- Muon detector showers lines. Requires Rec!3756, !2975 (@lohenry) [#10,#7]
- Update HLT2 VDM configuration (cherry-pick !2799), !2800 (@rmatev)
- QEE concurrent BW reduction, !3315 (@dzuliani)
- SLB: update electronic line selections, !3284 (@chinghua)
- SL: Remove both topo and prescale from background lines, !3159 (@anmorris)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Fix miscellaneous tests, !3408 (@jonrob)
- Don't set DetectorList in HLT1 HLT2 vertex comparison test, !3367 (@raaij)
- Update test script to follow changes to BeamSpotMonitor, !3251 (@spradlin)
- [RTA/DPA BW tests]: Add options for sprucing jobs on Turbo and TurCal in the bandwidth tests, !3285 (@rjhunter)
- Increase hlt2_reco_calo_efficiency timeout to 1200, !3365 (@jonrob)
- ~Luminosity | Add function to run only 2and3bodytopo in hlt2  + only test spruce lines during sprucing for FSR integrationtest, !3199 (@eleckste)


### Documentation ~Documentation

- Add documentation on running HLT2 and sprucing over 2024 data, !3360 (@rjhunter)
- Documentation: For now, recommend producing .dst files for HLT1 and HLT2 when running over MC, !3357 (@mmulder)


### Other

- Update References for: Allen!1618 based on lhcb-2024-patches-mr/395, !3434 (@lhcbsoft)
- Update refs for new 2024 ProbNNs, !3419 (@jonrob)
- Update References for: Allen!1597 based on lhcb-2024-patches-mr/331, !3404 (@lhcbsoft)
- Update References for: Moore!3309 based on lhcb-2024-patches-mr/317, !3397 (@lhcbsoft)
- Update References for: Rec!3858, Moore!3302, MooreOnline!390 based on lhcb-2024-patches-mr/298, !3391 (@lhcbsoft)
- Update References for: Rec!3845, Moore!3245, Alignment!495 based on lhcb-2024-patches-mr/265, !3374 (@lhcbsoft)
- Update References for: Allen!1587 based on lhcb-2024-patches-mr/223, !3356 (@lhcbsoft)
- Update References for: Rec!3840, Moore!3251, MooreOnline!358 based on lhcb-2024-patches-mr/243, !3364 (@lhcbsoft)
