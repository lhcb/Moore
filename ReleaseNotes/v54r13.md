2023-07-16 Moore v54r13
===

This version uses
Allen [v3r13](../../../../Allen/-/tags/v3r13),
Rec [v35r12](../../../../Rec/-/tags/v35r12),
Lbcom [v34r12](../../../../Lbcom/-/tags/v34r12),
LHCb [v54r12](../../../../LHCb/-/tags/v54r12),
Detector [v1r16](../../../../Detector/-/tags/v1r16),
Gaudi [v36r14](../../../../Gaudi/-/tags/v36r14) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Moore [v54r12](/../../tags/v54r12), with the following changes:

### New features ~"new feature"

- Upstream project highlights :star:


### Fixes ~"bug fix" ~workaround

- Upstream project highlights :star:


### Enhancements ~enhancement

- ~"PV finding" | Loose PatPV3DFuture options for PV reconstruction in 2023 data taking, !2476 (@mgiza) [#7]
- ~Monitoring | Fix default monitoring for lines with MVA-based filters, !2512 (@mstahl)
- Upstream project highlights :star:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration | Remove redundant no-subdet tests, !2492 (@jonrob)
- ~Configuration | Global muon geometry change and cleaned up calo_raw_bank in option files, !2424 (@nschulte)
- ~Persistency | Adding additional functions to check the output of 2022 data sprucing, !2460 (@tizhou)
- Fix pp thor data 2022 test, !2455 (@decianm)
- Cleanup: remove spruce and hlt2 analytics tests, !2422 (@shunan)
- Upstream project highlights :star:


### Documentation ~Documentation

### Other

- ~selection | SLB remove duplicated lines (new), !2347 (@jugarcia)
- ~selection | SL: rename SLB lines to fit naming convention, !2430 (@anmorris)
- ~selection | SL: Bc2BsX lines, !2415 (@sklaver) [[#288]
- ~selection ~hlt2 | Semileptonic baryon lines with Kshorts and Lambda0, !2068 (@amathad)
- ~selection ~hlt2 | Trigger lines for KSVeloLong tag and probe method, !2374 (@pherrero)
- ~selection ~hlt2 | Register cut-based inclusive dilepton lines and include in hlt2_pp_commissioning and bandwidth tests, !2356 (@jagoodin)
- ~selection ~hlt2 ~PID | Changes to L0 PID lines, !2314 (@imackay)
- ~hlt2 | Updates in SMOG lines for 2022 HLT2 reprocessing, !2472 (@oboenteg)
- Update References for: Rec!3460, Moore!2502 based on lhcb-master-mr/8562, !2516 (@lhcbsoft)
- Follow lhcb/Rec!3431, !2515 (@rmatev)
- Use new inputs for sprucing BW test, !2510 (@shunan)
- Update refs for LHCb!4189, !2508 (@rmatev)
- Switch to IP-based PV association for persistency, !2502 (@decianm)
- Force all lines going to full/turcal stream to have persistreco=True, !2358 (@sesen) [#582]
- Update References for: Moore!2476, MooreOnline!262 based on lhcb-master-mr/8522, !2505 (@lhcbsoft)
- Update References for: Rec!3464, Moore!2496 based on lhcb-master-mr/8517, !2504 (@lhcbsoft)
- Revert "Merge branch 'ref_bot_LHCb4179' into 'master'", !2501 (@rmatev)
- Update References for: LHCb!4135 based on lhcb-master-mr/8484, !2499 (@lhcbsoft)
- Updating the persistreco test to >=20 entries, !2496 (@mgiza)
- [QEE] Adding require_pvs() prefilter to spruce_qee_jets, !2489 (@lugrazet)
- Update References for: Detector!355, LHCb!4163, Rec!3371, Moore!2204 based on lhcb-master-mr/8505, !2498 (@lhcbsoft)
- Update References for: LHCb!4179 based on lhcb-master-mr/8475, !2497 (@lhcbsoft)
- Update References for: Allen!1268 based on lhcb-master-mr/8467, !2491 (@lhcbsoft)
- Update References for: Rec!3326, Moore!2133 based on lhcb-master-mr/8486, !2488 (@lhcbsoft)
- Pick up production streaming configuration from hlt2_pp_commissioning for HLT2 BW test, !2470 (@enoomen)
- FT pseudo efficiency, !2204 (@zexu)
- Add options for VP hit efficiency, !2133 (@peilian)
- Update References for: Allen!1242, Allen!1268, Moore!2425 based on lhcb-master-mr/8457, !2487 (@lhcbsoft)
- Use sequence with no lepton ID in the allen SV TOS test, !2425 (@thboettc)
