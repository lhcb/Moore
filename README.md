# Moore

Moore is the application for the LHCb high level trigger (HLT). This repository
contains the application configuration code, including the definition of the
HLT1 and HLT2 reconstruction and physics selections (lines). Reconstruction-
and selection-specific algorithms are mostly defined in the
[Rec](https://gitlab.cern.ch/lhcb/Rec) and
[Phys](https://gitlab.cern.ch/lhcb/Phys) projects.

Information on contributing to Moore is given in the [CONTRIBUTING
file](CONTRIBUTING.md).

Documentation can be found [here](https://lhcbdoc.web.cern.ch/lhcbdoc/moore/master/index.html).
